//Maya ASCII 2016 scene
//Name: clouds.ma
//Last modified: Fri, Feb 10, 2017 02:31:25 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "122549FD-B84F-7135-0FD0-1984CE539020";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -17.61238452405718 6.7078762977532431 -1.9133172425891909 ;
	setAttr ".r" -type "double3" -20.738352729532764 623.79999999980589 2.5444437451708134e-14 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "2FF859B5-6947-407F-95CC-2CA78DE908E0";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 18.943401909900629;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "D0F1DAE0-D74F-FE72-6B0E-D59B52536ED8";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "95A54D81-1F41-CD07-366B-CBBBCFFB6350";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "A91FCFDC-BE40-7837-4A40-67B353D85517";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "B452DA0D-114A-9679-528E-CC80DFBB8A39";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "76745929-404C-9513-7F50-139C58E0B00E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C4892F7E-EB4E-8DB6-3FC9-709DEE5E5513";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pSphere1";
	rename -uid "723F56FB-2F4F-939C-C0B6-6AA9BC617E19";
	setAttr ".s" -type "double3" 1.2252833194650632 0.79218636839076906 2.2766764893243474 ;
createNode mesh -n "pSphereShape1" -p "pSphere1";
	rename -uid "02E82465-A845-BF57-31C6-488D45E33C99";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.66666668653488159 0.66666668653488159 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 37 ".pt";
	setAttr ".pt[31]" -type "float3" 0 0 -0.14311554 ;
	setAttr ".pt[46]" -type "float3" 0 0 -0.045773715 ;
	setAttr ".pt[49]" -type "float3" 0.27209049 0 0.12085004 ;
	setAttr ".pt[52]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[54]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[55]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[56]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[57]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[58]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[59]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[60]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[61]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[62]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[63]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[64]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[65]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[66]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[67]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[68]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[69]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[70]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[71]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[72]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[73]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[74]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[75]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[76]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[81]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[83]" -type "float3" 0 -0.45567769 0 ;
	setAttr ".pt[84]" -type "float3" 0 -0.024785215 0 ;
	setAttr ".pt[85]" -type "float3" 0 -0.086214565 0 ;
	setAttr ".pt[86]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[87]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[89]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[90]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[91]" -type "float3" 0 -0.41716599 0 ;
	setAttr ".pt[92]" -type "float3" 0 -0.41716599 0 ;
createNode transform -n "pSphere2";
	rename -uid "46BA21FD-6748-2BD3-4679-2FA5A7E8CD3A";
	setAttr ".t" -type "double3" 0 0 4.5097992312681798 ;
	setAttr ".s" -type "double3" 1 0.75003072937461679 2.2660557916773256 ;
createNode mesh -n "pSphereShape2" -p "pSphere2";
	rename -uid "872C418B-1247-EF9B-C147-CFB0F6BA1BD3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.61111113429069519 0.50000001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt";
	setAttr ".pt[9]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[10]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[14]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[15]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[18]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[19]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[20]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[23]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[24]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[25]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[28]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[29]" -type "float3" 0 -0.33959535 0 ;
	setAttr ".pt[31]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[32]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[33]" -type "float3" 0 0 -0.15904099 ;
	setAttr ".pt[34]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[40]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[41]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[43]" -type "float3" 0 0.42344308 0 ;
createNode transform -n "pSphere3";
	rename -uid "B8ED54E7-BA4C-C64E-6B0E-2B9A7FCFED8C";
	setAttr ".t" -type "double3" 0 0 -6.9688945317396849 ;
	setAttr ".s" -type "double3" 1 0.75003072937461679 2.2660557916773256 ;
createNode transform -n "transform1" -p "pSphere3";
	rename -uid "8F538480-9B49-3FA8-9DB8-B899E8A708DF";
	setAttr ".v" no;
createNode mesh -n "pSphereShape3" -p "transform1";
	rename -uid "4234963C-F14E-53E3-C665-7AA40B72C95A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.41666667908430099 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 68 ".uvst[0].uvsp[0:67]" -type "float2" 0 0.16666667 0.11111111
		 0.16666667 0.22222222 0.16666667 0.33333334 0.16666667 0.44444445 0.16666667 0.55555558
		 0.16666667 0.66666669 0.16666667 0.77777779 0.16666667 0.8888889 0.16666667 1 0.16666667
		 0 0.33333334 0.11111111 0.33333334 0.22222222 0.33333334 0.33333334 0.33333334 0.44444445
		 0.33333334 0.55555558 0.33333334 0.66666669 0.33333334 0.77777779 0.33333334 0.8888889
		 0.33333334 1 0.33333334 0 0.5 0.11111111 0.5 0.22222222 0.5 0.33333334 0.5 0.44444445
		 0.5 0.55555558 0.5 0.66666669 0.5 0.77777779 0.5 0.8888889 0.5 1 0.5 0 0.66666669
		 0.11111111 0.66666669 0.22222222 0.66666669 0.33333334 0.66666669 0.44444445 0.66666669
		 0.55555558 0.66666669 0.66666669 0.66666669 0.77777779 0.66666669 0.8888889 0.66666669
		 1 0.66666669 0 0.83333337 0.11111111 0.83333337 0.22222222 0.83333337 0.33333334
		 0.83333337 0.44444445 0.83333337 0.55555558 0.83333337 0.66666669 0.83333337 0.77777779
		 0.83333337 0.8888889 0.83333337 1 0.83333337 0.055555556 0 0.16666667 0 0.27777779
		 0 0.3888889 0 0.5 0 0.6111111 0 0.72222221 0 0.83333331 0 0.94444448 0 0.055555556
		 1 0.16666667 1 0.27777779 1 0.3888889 1 0.5 1 0.6111111 1 0.72222221 1 0.83333331
		 1 0.94444448 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 29 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 0.21883546 ;
	setAttr ".pt[9]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[10]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.21883546 ;
	setAttr ".pt[14]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[15]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[18]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[19]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[20]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[23]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[24]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[25]" -type "float3" 0 0.42344299 0 ;
	setAttr ".pt[28]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[29]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[31]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[32]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[33]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[34]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[35]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[36]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[37]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[38]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[39]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[40]" -type "float3" 0 0.42344314 0 ;
	setAttr ".pt[41]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[43]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[44]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[46]" -type "float3" 0 -1.1920929e-07 0 ;
	setAttr -s 47 ".vt[0:46]"  0.38302234 -0.86602539 -0.32139379 0.086824179 -0.86602539 -0.49240392
		 -0.24999999 -0.86602539 -0.43301278 -0.46984634 -0.86602539 -0.17101014 -0.46984637 -0.86602539 0.17101005
		 -0.25000003 -0.86602539 0.43301269 0.086824059 -0.86602539 0.49240389 0.38302222 -0.86602539 0.32139382
		 0.5 -0.86602539 0 0.66341418 -0.49999997 -0.55667043 0.15038389 -0.49999997 -0.85286868
		 -0.43301269 -0.49999997 -0.75000018 -0.81379777 -0.49999997 -0.29619825 -0.81379783 -0.49999997 0.2961981
		 -0.43301278 -0.49999997 0.75 0.1503837 -0.49999997 0.85286862 0.663414 -0.49999997 0.55667043
		 0.86602545 -0.49999997 0 0.76604468 0 -0.64278758 0.17364836 0 -0.98480785 -0.49999997 0 -0.86602557
		 -0.93969268 0 -0.34202027 -0.93969274 0 0.34202009 -0.50000006 0 0.86602539 0.17364812 0 0.98480779
		 0.76604444 0 0.64278764 1 0 0 0.66341418 0.49999997 -0.55667043 0.15038389 0.49999997 -0.85286868
		 -0.43301269 0.49999997 -0.75000018 -0.81379777 0.49999997 -0.29619825 -0.81379783 0.49999997 0.2961981
		 -0.43301278 0.49999997 0.75 0.1503837 0.49999997 0.85286862 0.663414 0.49999997 0.55667043
		 0.86602545 0.49999997 0 0.38302234 0.86602539 -0.32139379 0.086824179 0.86602539 -0.49240392
		 -0.24999999 0.86602539 -0.43301278 -0.46984634 0.86602539 -0.17101014 -0.46984637 0.86602539 0.17101005
		 -0.25000003 0.86602539 0.43301269 0.086824059 0.86602539 0.49240389 0.38302222 0.86602539 0.32139382
		 0.5 0.86602539 0 0 -1 0 0 1 0;
	setAttr -s 99 ".ed[0:98]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 0 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 9 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 18 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 27 0 36 37 0 37 38 0 38 39 0 39 40 0
		 40 41 0 41 42 0 42 43 0 43 44 0 44 36 0 0 9 0 1 10 0 2 11 0 3 12 0 4 13 0 5 14 0
		 6 15 0 7 16 0 8 17 0 9 18 0 10 19 0 11 20 0 12 21 0 13 22 0 14 23 0 15 24 0 16 25 0
		 17 26 0 18 27 0 19 28 0 20 29 0 21 30 0 22 31 0 23 32 0 24 33 0 25 34 0 26 35 0 27 36 0
		 28 37 0 29 38 0 30 39 0 31 40 0 32 41 0 33 42 0 34 43 0 35 44 0 45 0 0 45 1 0 45 2 0
		 45 3 0 45 4 0 45 5 0 45 6 0 45 7 0 45 8 0 36 46 0 37 46 0 38 46 0 39 46 0 40 46 0
		 41 46 0 42 46 0 43 46 0 44 46 0;
	setAttr -s 54 -ch 198 ".fc[0:53]" -type "polyFaces" 
		f 4 0 46 -10 -46
		mu 0 4 0 1 11 10
		f 4 1 47 -11 -47
		mu 0 4 1 2 12 11
		f 4 2 48 -12 -48
		mu 0 4 2 3 13 12
		f 4 3 49 -13 -49
		mu 0 4 3 4 14 13
		f 4 4 50 -14 -50
		mu 0 4 4 5 15 14
		f 4 5 51 -15 -51
		mu 0 4 5 6 16 15
		f 4 6 52 -16 -52
		mu 0 4 6 7 17 16
		f 4 7 53 -17 -53
		mu 0 4 7 8 18 17
		f 4 8 45 -18 -54
		mu 0 4 8 9 19 18
		f 4 9 55 -19 -55
		mu 0 4 10 11 21 20
		f 4 10 56 -20 -56
		mu 0 4 11 12 22 21
		f 4 11 57 -21 -57
		mu 0 4 12 13 23 22
		f 4 12 58 -22 -58
		mu 0 4 13 14 24 23
		f 4 13 59 -23 -59
		mu 0 4 14 15 25 24
		f 4 14 60 -24 -60
		mu 0 4 15 16 26 25
		f 4 15 61 -25 -61
		mu 0 4 16 17 27 26
		f 4 16 62 -26 -62
		mu 0 4 17 18 28 27
		f 4 17 54 -27 -63
		mu 0 4 18 19 29 28
		f 4 18 64 -28 -64
		mu 0 4 20 21 31 30
		f 4 19 65 -29 -65
		mu 0 4 21 22 32 31
		f 4 20 66 -30 -66
		mu 0 4 22 23 33 32
		f 4 21 67 -31 -67
		mu 0 4 23 24 34 33
		f 4 22 68 -32 -68
		mu 0 4 24 25 35 34
		f 4 23 69 -33 -69
		mu 0 4 25 26 36 35
		f 4 24 70 -34 -70
		mu 0 4 26 27 37 36
		f 4 25 71 -35 -71
		mu 0 4 27 28 38 37
		f 4 26 63 -36 -72
		mu 0 4 28 29 39 38
		f 4 27 73 -37 -73
		mu 0 4 30 31 41 40
		f 4 28 74 -38 -74
		mu 0 4 31 32 42 41
		f 4 29 75 -39 -75
		mu 0 4 32 33 43 42
		f 4 30 76 -40 -76
		mu 0 4 33 34 44 43
		f 4 31 77 -41 -77
		mu 0 4 34 35 45 44
		f 4 32 78 -42 -78
		mu 0 4 35 36 46 45
		f 4 33 79 -43 -79
		mu 0 4 36 37 47 46
		f 4 34 80 -44 -80
		mu 0 4 37 38 48 47
		f 4 35 72 -45 -81
		mu 0 4 38 39 49 48
		f 3 -1 -82 82
		mu 0 3 1 0 50
		f 3 -2 -83 83
		mu 0 3 2 1 51
		f 3 -3 -84 84
		mu 0 3 3 2 52
		f 3 -4 -85 85
		mu 0 3 4 3 53
		f 3 -5 -86 86
		mu 0 3 5 4 54
		f 3 -6 -87 87
		mu 0 3 6 5 55
		f 3 -7 -88 88
		mu 0 3 7 6 56
		f 3 -8 -89 89
		mu 0 3 8 7 57
		f 3 -9 -90 81
		mu 0 3 9 8 58
		f 3 36 91 -91
		mu 0 3 40 41 59
		f 3 37 92 -92
		mu 0 3 41 42 60
		f 3 38 93 -93
		mu 0 3 42 43 61
		f 3 39 94 -94
		mu 0 3 43 44 62
		f 3 40 95 -95
		mu 0 3 44 45 63
		f 3 41 96 -96
		mu 0 3 45 46 64
		f 3 42 97 -97
		mu 0 3 46 47 65
		f 3 43 98 -98
		mu 0 3 47 48 66
		f 3 44 90 -99
		mu 0 3 48 49 67;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere4";
	rename -uid "8F004C4C-7642-CD0E-F3C7-0CB17C42EF3A";
	setAttr ".t" -type "double3" 0 0.61882253064402004 -8.0866778567051583 ;
	setAttr ".s" -type "double3" 1 0.75003072937461679 2.2660557916773256 ;
createNode transform -n "transform2" -p "pSphere4";
	rename -uid "1BBE37A1-074E-2646-59E2-C58101B34E88";
	setAttr ".v" no;
createNode mesh -n "pSphereShape4" -p "transform2";
	rename -uid "DD71B011-8E4C-E830-DE5F-278011B5187D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:53]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.41666667908430099 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 68 ".uvst[0].uvsp[0:67]" -type "float2" 0 0.16666667 0.11111111
		 0.16666667 0.22222222 0.16666667 0.33333334 0.16666667 0.44444445 0.16666667 0.55555558
		 0.16666667 0.66666669 0.16666667 0.77777779 0.16666667 0.8888889 0.16666667 1 0.16666667
		 0 0.33333334 0.11111111 0.33333334 0.22222222 0.33333334 0.33333334 0.33333334 0.44444445
		 0.33333334 0.55555558 0.33333334 0.66666669 0.33333334 0.77777779 0.33333334 0.8888889
		 0.33333334 1 0.33333334 0 0.5 0.11111111 0.5 0.22222222 0.5 0.33333334 0.5 0.44444445
		 0.5 0.55555558 0.5 0.66666669 0.5 0.77777779 0.5 0.8888889 0.5 1 0.5 0 0.66666669
		 0.11111111 0.66666669 0.22222222 0.66666669 0.33333334 0.66666669 0.44444445 0.66666669
		 0.55555558 0.66666669 0.66666669 0.66666669 0.77777779 0.66666669 0.8888889 0.66666669
		 1 0.66666669 0 0.83333337 0.11111111 0.83333337 0.22222222 0.83333337 0.33333334
		 0.83333337 0.44444445 0.83333337 0.55555558 0.83333337 0.66666669 0.83333337 0.77777779
		 0.83333337 0.8888889 0.83333337 1 0.83333337 0.055555556 0 0.16666667 0 0.27777779
		 0 0.3888889 0 0.5 0 0.6111111 0 0.72222221 0 0.83333331 0 0.94444448 0 0.055555556
		 1 0.16666667 1 0.27777779 1 0.3888889 1 0.5 1 0.6111111 1 0.72222221 1 0.83333331
		 1 0.94444448 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 29 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 0.21883546 ;
	setAttr ".pt[9]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[10]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.21883546 ;
	setAttr ".pt[14]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[15]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[18]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[19]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[20]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[23]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[24]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[25]" -type "float3" 0 0.42344299 0 ;
	setAttr ".pt[28]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[29]" -type "float3" 0 -0.33959535 0.21883546 ;
	setAttr ".pt[31]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[32]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[33]" -type "float3" 0 -1.7881393e-07 -0.15904099 ;
	setAttr ".pt[34]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[35]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[36]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[37]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[38]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[39]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[40]" -type "float3" 0 0.42344314 0 ;
	setAttr ".pt[41]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[43]" -type "float3" 0 0.42344308 0 ;
	setAttr ".pt[44]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[46]" -type "float3" 0 -1.1920929e-07 0 ;
	setAttr -s 47 ".vt[0:46]"  0.38302234 -0.86602539 -0.32139379 0.086824179 -0.86602539 -0.49240392
		 -0.24999999 -0.86602539 -0.43301278 -0.46984634 -0.86602539 -0.17101014 -0.46984637 -0.86602539 0.17101005
		 -0.25000003 -0.86602539 0.43301269 0.086824059 -0.86602539 0.49240389 0.38302222 -0.86602539 0.32139382
		 0.5 -0.86602539 0 0.66341418 -0.49999997 -0.55667043 0.15038389 -0.49999997 -0.85286868
		 -0.43301269 -0.49999997 -0.75000018 -0.81379777 -0.49999997 -0.29619825 -0.81379783 -0.49999997 0.2961981
		 -0.43301278 -0.49999997 0.75 0.1503837 -0.49999997 0.85286862 0.663414 -0.49999997 0.55667043
		 0.86602545 -0.49999997 0 0.76604468 0 -0.64278758 0.17364836 0 -0.98480785 -0.49999997 0 -0.86602557
		 -0.93969268 0 -0.34202027 -0.93969274 0 0.34202009 -0.50000006 0 0.86602539 0.17364812 0 0.98480779
		 0.76604444 0 0.64278764 1 0 0 0.66341418 0.49999997 -0.55667043 0.15038389 0.49999997 -0.85286868
		 -0.43301269 0.49999997 -0.75000018 -0.81379777 0.49999997 -0.29619825 -0.81379783 0.49999997 0.2961981
		 -0.43301278 0.49999997 0.75 0.1503837 0.49999997 0.85286862 0.663414 0.49999997 0.55667043
		 0.86602545 0.49999997 0 0.38302234 0.86602539 -0.32139379 0.086824179 0.86602539 -0.49240392
		 -0.24999999 0.86602539 -0.43301278 -0.46984634 0.86602539 -0.17101014 -0.46984637 0.86602539 0.17101005
		 -0.25000003 0.86602539 0.43301269 0.086824059 0.86602539 0.49240389 0.38302222 0.86602539 0.32139382
		 0.5 0.86602539 0 0 -1 0 0 1 0;
	setAttr -s 99 ".ed[0:98]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 0 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 9 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 18 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 27 0 36 37 0 37 38 0 38 39 0 39 40 0
		 40 41 0 41 42 0 42 43 0 43 44 0 44 36 0 0 9 0 1 10 0 2 11 0 3 12 0 4 13 0 5 14 0
		 6 15 0 7 16 0 8 17 0 9 18 0 10 19 0 11 20 0 12 21 0 13 22 0 14 23 0 15 24 0 16 25 0
		 17 26 0 18 27 0 19 28 0 20 29 0 21 30 0 22 31 0 23 32 0 24 33 0 25 34 0 26 35 0 27 36 0
		 28 37 0 29 38 0 30 39 0 31 40 0 32 41 0 33 42 0 34 43 0 35 44 0 45 0 0 45 1 0 45 2 0
		 45 3 0 45 4 0 45 5 0 45 6 0 45 7 0 45 8 0 36 46 0 37 46 0 38 46 0 39 46 0 40 46 0
		 41 46 0 42 46 0 43 46 0 44 46 0;
	setAttr -s 54 -ch 198 ".fc[0:53]" -type "polyFaces" 
		f 4 0 46 -10 -46
		mu 0 4 0 1 11 10
		f 4 1 47 -11 -47
		mu 0 4 1 2 12 11
		f 4 2 48 -12 -48
		mu 0 4 2 3 13 12
		f 4 3 49 -13 -49
		mu 0 4 3 4 14 13
		f 4 4 50 -14 -50
		mu 0 4 4 5 15 14
		f 4 5 51 -15 -51
		mu 0 4 5 6 16 15
		f 4 6 52 -16 -52
		mu 0 4 6 7 17 16
		f 4 7 53 -17 -53
		mu 0 4 7 8 18 17
		f 4 8 45 -18 -54
		mu 0 4 8 9 19 18
		f 4 9 55 -19 -55
		mu 0 4 10 11 21 20
		f 4 10 56 -20 -56
		mu 0 4 11 12 22 21
		f 4 11 57 -21 -57
		mu 0 4 12 13 23 22
		f 4 12 58 -22 -58
		mu 0 4 13 14 24 23
		f 4 13 59 -23 -59
		mu 0 4 14 15 25 24
		f 4 14 60 -24 -60
		mu 0 4 15 16 26 25
		f 4 15 61 -25 -61
		mu 0 4 16 17 27 26
		f 4 16 62 -26 -62
		mu 0 4 17 18 28 27
		f 4 17 54 -27 -63
		mu 0 4 18 19 29 28
		f 4 18 64 -28 -64
		mu 0 4 20 21 31 30
		f 4 19 65 -29 -65
		mu 0 4 21 22 32 31
		f 4 20 66 -30 -66
		mu 0 4 22 23 33 32
		f 4 21 67 -31 -67
		mu 0 4 23 24 34 33
		f 4 22 68 -32 -68
		mu 0 4 24 25 35 34
		f 4 23 69 -33 -69
		mu 0 4 25 26 36 35
		f 4 24 70 -34 -70
		mu 0 4 26 27 37 36
		f 4 25 71 -35 -71
		mu 0 4 27 28 38 37
		f 4 26 63 -36 -72
		mu 0 4 28 29 39 38
		f 4 27 73 -37 -73
		mu 0 4 30 31 41 40
		f 4 28 74 -38 -74
		mu 0 4 31 32 42 41
		f 4 29 75 -39 -75
		mu 0 4 32 33 43 42
		f 4 30 76 -40 -76
		mu 0 4 33 34 44 43
		f 4 31 77 -41 -77
		mu 0 4 34 35 45 44
		f 4 32 78 -42 -78
		mu 0 4 35 36 46 45
		f 4 33 79 -43 -79
		mu 0 4 36 37 47 46
		f 4 34 80 -44 -80
		mu 0 4 37 38 48 47
		f 4 35 72 -45 -81
		mu 0 4 38 39 49 48
		f 3 -1 -82 82
		mu 0 3 1 0 50
		f 3 -2 -83 83
		mu 0 3 2 1 51
		f 3 -3 -84 84
		mu 0 3 3 2 52
		f 3 -4 -85 85
		mu 0 3 4 3 53
		f 3 -5 -86 86
		mu 0 3 5 4 54
		f 3 -6 -87 87
		mu 0 3 6 5 55
		f 3 -7 -88 88
		mu 0 3 7 6 56
		f 3 -8 -89 89
		mu 0 3 8 7 57
		f 3 -9 -90 81
		mu 0 3 9 8 58
		f 3 36 91 -91
		mu 0 3 40 41 59
		f 3 37 92 -92
		mu 0 3 41 42 60
		f 3 38 93 -93
		mu 0 3 42 43 61
		f 3 39 94 -94
		mu 0 3 43 44 62
		f 3 40 95 -95
		mu 0 3 44 45 63
		f 3 41 96 -96
		mu 0 3 45 46 64
		f 3 42 97 -97
		mu 0 3 46 47 65
		f 3 43 98 -98
		mu 0 3 47 48 66
		f 3 44 90 -99
		mu 0 3 48 49 67;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere5";
	rename -uid "B6D06981-2E4D-CD10-6F2A-ACA8040E4E1B";
createNode mesh -n "pSphere5Shape" -p "pSphere5";
	rename -uid "59EE253D-C14D-767B-299B-25AF8644F857";
	setAttr -k off ".v";
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "992B647C-7F47-5FC5-AD9B-2E99DF198A2B";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "4B9E4092-3240-F6BD-09C2-15905E6FA2FE";
createNode displayLayer -n "defaultLayer";
	rename -uid "CEBA084F-D742-B563-099A-59A4EA94DBC8";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A6820D57-9640-119D-00D0-FD844988EAE2";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "56A24F9F-294B-6100-0DDB-26835399E05B";
	setAttr ".g" yes;
createNode polySphere -n "polySphere1";
	rename -uid "AEAD559F-BA40-E903-671A-01BCE66AD0EE";
	setAttr ".sa" 9;
	setAttr ".sh" 6;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "93C5C8D4-EA43-8364-CC74-8F827F5C9298";
	setAttr ".ics" -type "componentList" 2 "f[9:10]" "f[18:19]";
	setAttr ".ix" -type "matrix" 1.2252833194650632 0 0 0 0 0.79218636839076906 0 0 0 0 2.2766764893243474 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.30552465 0 -1.6195092 ;
	setAttr ".rs" 1985405560;
	setAttr ".lt" -type "double3" 0 -8.3266726846886741e-17 1.0662451058194795 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.61264162321624305 -0.39609316058639099 -1.971660047667332 ;
	setAttr ".cbx" -type "double3" 1.223690917156026 0.39609316058639099 -1.2673584741870179 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "F205E428-2A47-AC99-8B2F-0EB1A184B1DE";
	setAttr ".uopa" yes;
	setAttr -s 23 ".tk";
	setAttr ".tk[1]" -type "float3" 0.33528617 0 0 ;
	setAttr ".tk[2]" -type "float3" 0.33528617 0 0 ;
	setAttr ".tk[9]" -type "float3" 0.33528617 0 -0.12042215 ;
	setAttr ".tk[10]" -type "float3" 0 0 0.067274921 ;
	setAttr ".tk[12]" -type "float3" 0.33528617 0 0 ;
	setAttr ".tk[14]" -type "float3" 0 0 -0.13545796 ;
	setAttr ".tk[19]" -type "float3" 0 0 0.22375639 ;
	setAttr ".tk[21]" -type "float3" 0.33528617 0 0 ;
	setAttr ".tk[22]" -type "float3" 0 -0.18114905 0 ;
	setAttr ".tk[23]" -type "float3" 0 0 -0.13545796 ;
	setAttr ".tk[24]" -type "float3" 0 0 -0.13545796 ;
	setAttr ".tk[28]" -type "float3" 0 0 0.17001963 ;
	setAttr ".tk[30]" -type "float3" 0.33528617 0 0 ;
	setAttr ".tk[32]" -type "float3" 0 0 -0.13545796 ;
	setAttr ".tk[34]" -type "float3" -0.1801993 0 0 ;
	setAttr ".tk[36]" -type "float3" 0.22190571 0 0 ;
	setAttr ".tk[39]" -type "float3" 0 -0.2622146 0 ;
	setAttr ".tk[41]" -type "float3" 0 -0.18114905 0 ;
	setAttr ".tk[42]" -type "float3" 0 -0.18114905 0 ;
	setAttr ".tk[43]" -type "float3" -0.057591658 -0.18114905 0 ;
	setAttr ".tk[44]" -type "float3" 0 -0.2622146 0 ;
	setAttr ".tk[46]" -type "float3" 0 -0.2622146 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "56C7F7D5-5848-480F-4E28-D4889EA9C692";
	setAttr ".ics" -type "componentList" 1 "f[48:52]";
	setAttr ".ix" -type "matrix" 1.2252833194650632 0 0 0 0 0.79218636839076906 0 0 0 0 2.2766764893243474 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.018473372 0.58219206 0.3658548 ;
	setAttr ".rs" 568899971;
	setAttr ".lt" -type "double3" 7.3942588163511402e-17 5.2735593669694936e-16 0.76918680843927623 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.57569491722069543 0.47833067470787438 -0.38933472351805215 ;
	setAttr ".cbx" -type "double3" 0.6126416597325316 0.68605350724469649 1.1210443023804224 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "65D5B3A4-7A40-9ABC-1CC0-448810610C15";
	setAttr ".uopa" yes;
	setAttr -s 15 ".tk";
	setAttr ".tk[46]" -type "float3" -0.2827788 0 0 ;
	setAttr ".tk[47]" -type "float3" 0 -0.11501355 0 ;
	setAttr ".tk[48]" -type "float3" 0 0 -0.11364768 ;
	setAttr ".tk[49]" -type "float3" 0.04011213 0 0 ;
	setAttr ".tk[50]" -type "float3" 0.16261287 0 0 ;
	setAttr ".tk[51]" -type "float3" 0.18429135 0 0 ;
	setAttr ".tk[52]" -type "float3" 0 -0.37177467 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.32315403 0 ;
	setAttr ".tk[54]" -type "float3" 0.17437458 -0.42353195 0 ;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "0F875D87-B545-0D5A-AD71-D5B799BD628C";
	setAttr ".ics" -type "componentList" 3 "f[48:52]" "f[63]" "f[68]";
createNode polyTweak -n "polyTweak3";
	rename -uid "09609E7B-D64B-E861-5182-47A7A5ABB2D9";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk";
	setAttr ".tk[45]" -type "float3" 0 0 -0.24592957 ;
	setAttr ".tk[55]" -type "float3" 0.14411682 0 0.21505283 ;
	setAttr ".tk[57]" -type "float3" 0 0.10322655 -0.050415773 ;
	setAttr ".tk[61]" -type "float3" -0.18743795 0 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "0013520F-6846-9A10-88E0-B286FFE8F85B";
	setAttr ".ics" -type "componentList" 3 "f[27:28]" "f[58]" "f[61]";
	setAttr ".ix" -type "matrix" 1.2252833194650632 0 0 0 0 0.79218636839076906 0 0 0 0 2.2766764893243474 0
		 0 0 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.32158813 0.54107332 -1.6771951 ;
	setAttr ".rs" 379264472;
	setAttr ".lt" -type "double3" 1.2490009027033011e-16 -4.163336342344337e-16 0.53357745114466704 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.53056323122236249 0.39609316058639099 -2.6226804508143005 ;
	setAttr ".cbx" -type "double3" 1.1737394827544754 0.68605350724469649 -0.73170968063774044 ;
createNode polyTweak -n "polyTweak4";
	rename -uid "D4AFC992-754E-29FB-74FB-6C84919A1EF5";
	setAttr ".uopa" yes;
	setAttr -s 30 ".tk";
	setAttr ".tk[4]" -type "float3" 0 0.27335063 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[56]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[57]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[58]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[59]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[60]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[61]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[62]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[63]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[64]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[65]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[66]" -type "float3" 0 0.0044951756 0.013985261 ;
	setAttr ".tk[67]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[68]" -type "float3" 0 0.0044951756 0.013985261 ;
	setAttr ".tk[69]" -type "float3" 0 0.0044952352 0.013985261 ;
	setAttr ".tk[70]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[71]" -type "float3" 0 0.0044951756 0.013985261 ;
	setAttr ".tk[72]" -type "float3" 0 0.0044951756 0.013985261 ;
	setAttr ".tk[73]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[74]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[75]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[76]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[77]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[78]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[80]" -type "float3" 0 -0.26885545 0.013985261 ;
	setAttr ".tk[81]" -type "float3" 0 -0.26885545 0.013985261 ;
createNode polySphere -n "polySphere2";
	rename -uid "B8395645-624D-A2F2-8FA3-E5B12B61504E";
	setAttr ".sa" 9;
	setAttr ".sh" 6;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "DAAE6818-6C42-1DCA-B8BB-1EA0EBC613D0";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1251\n                -height 660\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1251\n            -height 660\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1251\\n    -height 660\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1251\\n    -height 660\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "320C30A5-9343-CE53-ABED-50B0A0FC4A55";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode lambert -n "cloud";
	rename -uid "F31CD469-A04D-36B8-757D-A6A9FF37B88D";
createNode shadingEngine -n "lambert2SG";
	rename -uid "20FE3840-BD4B-B28C-2899-06BEF157DE86";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "8EDB4773-EC48-6B15-E449-0FB0D99CD549";
createNode polyUnite -n "polyUnite1";
	rename -uid "3ABD6A9B-E147-09D6-E340-8DADF130D4E9";
	setAttr -s 2 ".ip";
	setAttr -s 2 ".im";
createNode groupId -n "groupId1";
	rename -uid "15D3CACB-B242-69EC-BAD6-25B8DA4C890A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId2";
	rename -uid "5A3AAA7B-9549-3B51-7B78-0EB8414238D5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "998EA3D6-6F48-38A4-44E9-B4943E874120";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "1726E4C3-2842-F3A5-B0CA-2AA5AE10A0FB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "4C8B8CEC-0F47-926E-C7D7-38A764A1A8CE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "58E6CC45-4046-DA6C-35EF-F499294513DA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:107]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "93F1EFEA-A044-EEE2-B93E-8BAAD26D470B";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -742.85711333865333 -278.57141750199497 ;
	setAttr ".tgi[0].vh" -type "double2" 708.33330518669663 290.4761789337042 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -58.571430206298828;
	setAttr ".tgi[0].ni[1].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyExtrudeFace3.out" "pSphereShape1.i";
connectAttr "polySphere2.out" "pSphereShape2.i";
connectAttr "groupId3.id" "pSphereShape3.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape3.iog.og[0].gco";
connectAttr "groupId4.id" "pSphereShape3.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pSphereShape4.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape4.iog.og[0].gco";
connectAttr "groupId2.id" "pSphereShape4.ciog.cog[0].cgid";
connectAttr "groupParts1.og" "pSphere5Shape.i";
connectAttr "groupId5.id" "pSphere5Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphere5Shape.iog.og[0].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyTweak1.out" "polyExtrudeFace1.ip";
connectAttr "pSphereShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polySphere1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace2.ip";
connectAttr "pSphereShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak2.ip";
connectAttr "polyTweak3.out" "polySubdFace1.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak3.ip";
connectAttr "polyTweak4.out" "polyExtrudeFace3.ip";
connectAttr "pSphereShape1.wm" "polyExtrudeFace3.mp";
connectAttr "polySubdFace1.out" "polyTweak4.ip";
connectAttr "cloud.oc" "lambert2SG.ss";
connectAttr "pSphereShape2.iog" "lambert2SG.dsm" -na;
connectAttr "pSphereShape1.iog" "lambert2SG.dsm" -na;
connectAttr "pSphereShape4.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape4.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape3.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape3.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphere5Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "groupId1.msg" "lambert2SG.gn" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "groupId3.msg" "lambert2SG.gn" -na;
connectAttr "groupId4.msg" "lambert2SG.gn" -na;
connectAttr "groupId5.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "cloud.msg" "materialInfo1.m";
connectAttr "pSphereShape4.o" "polyUnite1.ip[0]";
connectAttr "pSphereShape3.o" "polyUnite1.ip[1]";
connectAttr "pSphereShape4.wm" "polyUnite1.im[0]";
connectAttr "pSphereShape3.wm" "polyUnite1.im[1]";
connectAttr "polyUnite1.out" "groupParts1.ig";
connectAttr "groupId5.id" "groupParts1.gi";
connectAttr "cloud.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "cloud.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of clouds.ma
