//Maya ASCII 2016 scene
//Name: house_snow.ma
//Last modified: Fri, Dec 30, 2016 08:12:27 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "E4F1301D-9D48-A323-4DE5-B0A990C5773C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -17.034927674994353 8.6448241238077053 16.370283154656963 ;
	setAttr ".r" -type "double3" -18.338352728948124 1398.2000000002356 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "1AFF202F-F34D-E1FE-CC38-D3B09672C29E";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 23.689538116804336;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -2.0469673322053907 1.1914341784315359 -0.3928382513547497 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "C45CB695-FA4D-7EE1-325C-478E79E74DAA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.9875457767751574 100.3147477530563 -0.24253212957665848 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "71B0F6B7-4F4A-2335-AC4C-ACA6E71EE46E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 20.15718272646993;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "1E0EBC0F-1240-9769-1CA5-A9BCA8D17B3F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.19557496485237968 0.77928066747495217 100.15565311894609 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "FD1344E4-3F4F-E4CB-9BB1-2CA16CB1A314";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 6.2644140873238063;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "8BB7ADE1-C148-FED9-5773-88B961CD1024";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.17257686716712 1.4757526892495809 -0.87669962950139535 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "C44454B7-E54F-D79F-836A-91A02F5865A0";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.9335353488088272;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "B38AF396-A84F-8429-EA09-D2B1361B1990";
	setAttr ".t" -type "double3" -2.0180923814611966 1.1445117574956996 -2.9720763210511745 ;
	setAttr ".s" -type "double3" 0.11776151926431713 2.1980142178410844 0.11776151926431713 ;
createNode transform -n "transform116" -p "pCube1";
	rename -uid "EA012246-7548-EEE6-647C-68BCB60C4292";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform116";
	rename -uid "A36AF995-914C-BFAC-372A-3B832DF3BEE8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "32EDA322-5C49-CD42-B365-95A4903FEE55";
	setAttr ".t" -type "double3" 2.0180923814611971 1.1445117574956996 -2.9720763210511745 ;
	setAttr ".s" -type "double3" 0.11776151926431713 2.1980142178410844 0.11776151926431713 ;
createNode transform -n "transform115" -p "pCube2";
	rename -uid "6E68B7FF-2C46-62A3-20E8-6CAC7E29298C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform115";
	rename -uid "5F683431-074C-96E1-E6A1-8EA7B535FEC5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "06003429-B947-914A-E796-CF9FA883C0C6";
	setAttr ".t" -type "double3" 2.0180923814611971 1.1445117574956996 1.9862553960183025 ;
	setAttr ".s" -type "double3" 0.11776151926431713 2.1980142178410844 0.11776151926431713 ;
createNode transform -n "transform114" -p "pCube3";
	rename -uid "540C26A0-4E4C-6F56-4C25-0FB247FF6E44";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform114";
	rename -uid "50B48E4B-A64A-DAA5-7F66-9BABB3181D85";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4";
	rename -uid "BAF5385A-8F4D-A292-AB07-3497BA28AB95";
	setAttr ".t" -type "double3" -2.0180923814611966 1.1445117574956996 1.9862553960183025 ;
	setAttr ".s" -type "double3" 0.11776151926431713 2.1980142178410844 0.11776151926431713 ;
createNode transform -n "transform113" -p "pCube4";
	rename -uid "F44F3F2E-1C41-AF58-C65F-9998B71C47E0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform113";
	rename -uid "3BA97A29-C84E-B8C2-7D3C-8BB0BC418C87";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder1";
	rename -uid "F713F668-234D-432D-BC2E-0EB82BA8BE48";
	setAttr ".t" -type "double3" 2.0116594718686129 0 -0.45127988658416673 ;
	setAttr -av ".tx";
	setAttr -av ".tz";
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
	setAttr -av ".sx";
	setAttr -av ".sy";
	setAttr -av ".sz";
createNode transform -n "transform112" -p "pCylinder1";
	rename -uid "79759769-F14F-CC1A-8872-DC93E286E32F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform112";
	rename -uid "040DB700-8E4E-5C6B-5A0F-2D816AAD4912";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "28D16703-7049-ECC6-EB45-5BAC2A900057";
	setAttr ".t" -type "double3" 2.0116594718686129 0.21384499639817467 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform111" -p "pCylinder2";
	rename -uid "4666D74B-164E-9C03-6743-C794EBB3C6F9";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform111";
	rename -uid "D3EB431E-254A-3F92-B672-DF84F629049D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder3";
	rename -uid "CF3E0A96-8C43-8E79-8F91-A68E5B915D04";
	setAttr ".t" -type "double3" 2.0116594718686129 0.42834872320517037 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform110" -p "pCylinder3";
	rename -uid "B7E59601-3F49-29AA-81A9-A6A93F3AB727";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape3" -p "transform110";
	rename -uid "5DA0440C-8444-1C07-798C-399F55B51E1B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder4";
	rename -uid "7E663DEF-B243-CDD4-8EDA-77A206A8B812";
	setAttr ".t" -type "double3" 2.0116594718686129 0.65000257423906627 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform109" -p "pCylinder4";
	rename -uid "3023B071-6D40-B8FF-4139-89803552F435";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape4" -p "transform109";
	rename -uid "56891C8A-A94E-6EC3-C7C4-A4A2E95B72B8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder5";
	rename -uid "332CBDA8-BC46-D7BC-C8EB-FC96A5651352";
	setAttr ".t" -type "double3" 2.0116594718686129 0.85020605259226234 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform108" -p "pCylinder5";
	rename -uid "1C60FC26-4941-D81D-658D-82AE190B0DA9";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape5" -p "transform108";
	rename -uid "57B04A6B-9E43-1920-C9FB-3686232BC39A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder6";
	rename -uid "14797E5D-B744-8028-FD02-399A13114F1F";
	setAttr ".t" -type "double3" 2.0116594718686129 1.0575596551723581 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform107" -p "pCylinder6";
	rename -uid "166BB107-4E4A-6C59-DAA7-67955E52E799";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape6" -p "transform107";
	rename -uid "DEDC8D52-A147-1B81-981E-3AB3FBBC96C6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder7";
	rename -uid "E5E733FE-ED41-435D-0674-B4A2B03A33CF";
	setAttr ".t" -type "double3" 2.0116594718686129 1.2706013916399015 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform106" -p "pCylinder7";
	rename -uid "93C91C23-854C-D201-AD77-B29396BEB1C0";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape7" -p "transform106";
	rename -uid "15F85764-6A4D-BAEB-7C7C-F086DD042153";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder8";
	rename -uid "A8B18004-0746-CF0C-33CF-E1BA8774D4A0";
	setAttr ".t" -type "double3" 2.0116594718686129 1.4757526934234615 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform105" -p "pCylinder8";
	rename -uid "ABFB816D-EB46-03BF-C22A-96931A747098";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape8" -p "transform105";
	rename -uid "D7087675-0844-E73D-D7BD-3C8A2E015477";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder9";
	rename -uid "F919A23E-0746-8AAA-E8FF-6A96D5A01308";
	setAttr ".t" -type "double3" 2.0116594718686129 1.6987471506717415 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform104" -p "pCylinder9";
	rename -uid "98144682-FD4F-6785-283A-BB9A1F0ACE61";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape9" -p "transform104";
	rename -uid "F59FC415-E74D-92B1-6E25-129E858187A1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder10";
	rename -uid "D484F7FB-214D-2403-B17F-569A386BD9C5";
	setAttr ".t" -type "double3" 2.0116594718686129 1.9149842001246189 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform103" -p "pCylinder10";
	rename -uid "2FE144DC-7046-D246-7D2C-92B49BDBAF29";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape10" -p "transform103";
	rename -uid "DDE1CD84-8541-2EFE-C45D-80BA3B5F0657";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder11";
	rename -uid "A0D0649D-464F-3CDA-DD5A-50A2086E9540";
	setAttr ".t" -type "double3" 2.0116594718686129 2.1244638417820942 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform102" -p "pCylinder11";
	rename -uid "9ABE63F4-594A-A5FE-9B5C-EB80D8B40F88";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape11" -p "transform102";
	rename -uid "2BCBE38B-8E41-2E43-0843-76B077EEDD47";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 3 ".iog[0].og";
	setAttr ".iog[0].og[2].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".ciog[0].cog";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder12";
	rename -uid "9903012F-DF4D-C869-9BA9-D1888810025A";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.1244638417820942 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.1370154227055558 0.14005219103648944 ;
createNode transform -n "transform101" -p "pCylinder12";
	rename -uid "76F1BF5D-0F4A-3C94-223F-BAB15627464F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape12" -p "transform101";
	rename -uid "E4B63A11-C245-907A-0D00-FCBAA7391C34";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder13";
	rename -uid "97206425-AB4D-6C4D-1B74-9686C9450685";
	setAttr ".t" -type "double3" -0.0031231368343787036 1.8668233487980483 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform100" -p "pCylinder13";
	rename -uid "39FB445F-E448-C2DB-7438-8F8AB7F0142F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape13" -p "transform100";
	rename -uid "08ACB201-E04D-C36C-C2C9-2E830EBDF157";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder14";
	rename -uid "C35265D7-B342-F9F8-9554-FDA7BBD3CC99";
	setAttr ".t" -type "double3" -0.0031231368343787036 1.6134477909779568 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform99" -p "pCylinder14";
	rename -uid "B0F624EB-F44F-7504-ECC7-B79CE483F1DB";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape14" -p "transform99";
	rename -uid "0FE49A92-534D-148C-4959-B89FD5A1D5A4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder15";
	rename -uid "379056E8-4F4C-9E55-5D48-F5B37982028C";
	setAttr ".t" -type "double3" -0.0031231368343787036 1.3698174469201767 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform98" -p "pCylinder15";
	rename -uid "95E8C155-6841-606E-2644-03AD150EC0C8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape15" -p "transform98";
	rename -uid "A8BE9422-C249-65F6-14D9-8BA2368C3BAA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder16";
	rename -uid "83660983-114A-78EE-E316-87A652F1CCD1";
	setAttr ".t" -type "double3" -0.0031231368343787036 1.1164418891000856 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform97" -p "pCylinder16";
	rename -uid "BB7CAB63-5947-EB94-747F-5D93BE0B3E07";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape16" -p "transform97";
	rename -uid "05073AF7-0547-CC5A-8940-5BA169F3B70B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder17";
	rename -uid "19457F33-344C-CF1F-006C-65BAFEFF414B";
	setAttr ".t" -type "double3" -0.0031231368343787036 0.88255675880461659 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform96" -p "pCylinder17";
	rename -uid "565EF298-5D4A-6A4B-415D-688010AD12B1";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape17" -p "transform96";
	rename -uid "6605B275-AD44-4C25-0205-99B247E7CA3B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder18";
	rename -uid "F49B0EE6-2140-6480-C6F6-F88FCBDD7A20";
	setAttr ".t" -type "double3" -0.0031231368343787036 0.62918120098452501 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform95" -p "pCylinder18";
	rename -uid "2FDB2D40-3B4F-C0ED-CCBE-E2B5FD963FF6";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape18" -p "transform95";
	rename -uid "20368596-FC4F-3214-4C3A-EF8A5157B7DE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder19";
	rename -uid "39FD488B-E04D-4798-1D70-E2AF25C0BA65";
	setAttr ".t" -type "double3" -0.0031231368343787036 0.42453171197598971 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform94" -p "pCylinder19";
	rename -uid "CF4FCBC4-8F4B-CBFB-46E6-648229E4E787";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape19" -p "transform94";
	rename -uid "77B16F80-DC4C-BE75-D5CE-6C82E9697F2F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder20";
	rename -uid "25697805-8644-BED2-DF1E-159227458E02";
	setAttr ".t" -type "double3" -0.0031231368343787036 0.21988222296745441 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform93" -p "pCylinder20";
	rename -uid "CADEEB60-DA41-C3B3-6E36-7C83957D811D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape20" -p "transform93";
	rename -uid "CE030C7A-5C4C-5BDE-7539-CBA3B12DD902";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder21";
	rename -uid "B3875138-4243-88C3-6BBF-DE8982A74934";
	setAttr ".t" -type "double3" -0.0031231368343787036 0.034723161483541265 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.4996473300328241 0.14005219103648944 ;
createNode transform -n "transform92" -p "pCylinder21";
	rename -uid "D886D49B-964F-3F85-5BC5-288B1E9AA13D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape21" -p "transform92";
	rename -uid "FA308E60-D74B-C75A-0FC5-0D86F29D414F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder22";
	rename -uid "623E7803-0A43-54E9-D69E-F7B5F41B9AC9";
	setAttr ".t" -type "double3" 0.48112732831911131 0.88611015922214031 1.9530934845034313 ;
	setAttr ".s" -type "double3" 0.1016342229405259 0.97099556615882965 0.1016342229405259 ;
createNode transform -n "transform91" -p "pCylinder22";
	rename -uid "B79947D3-9C42-9687-91C2-2794F94F3BAD";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape22" -p "transform91";
	rename -uid "FBA07E25-1440-341C-F9DD-A2ABCC4AE228";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder23";
	rename -uid "BE78DA0D-BE46-B9F7-9742-199000D1314B";
	setAttr ".t" -type "double3" -0.50538883670887491 0.88611015922214031 1.9530934845034313 ;
	setAttr ".s" -type "double3" 0.1016342229405259 0.97099556615882965 0.1016342229405259 ;
createNode transform -n "transform90" -p "pCylinder23";
	rename -uid "3074058D-9048-2EBD-5679-17A58893FE4C";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape23" -p "transform90";
	rename -uid "3145408E-4543-438F-8024-4E996FE81FAD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder24";
	rename -uid "AA21AC0F-0447-E49C-2363-E5977CD4629A";
	setAttr ".t" -type "double3" -0.0016359013754350116 1.8201520601528935 1.9530934845034313 ;
	setAttr -av ".tz";
	setAttr ".s" -type "double3" 0.1016342229405259 0.60590626573147077 0.1016342229405259 ;
	setAttr -av ".sy";
createNode transform -n "transform89" -p "pCylinder24";
	rename -uid "CC2D6390-8D4F-E27A-B601-D4841FA07AB3";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape24" -p "transform89";
	rename -uid "34586FD4-8648-7D78-9165-02A648671436";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder25";
	rename -uid "D68EE875-1942-98D1-E7DA-45A3D79AF22D";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.1244638417820942 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.0986430979999353 0.14005219103648944 ;
createNode transform -n "transform88" -p "pCylinder25";
	rename -uid "02FB2097-F948-5A88-1B38-5D991D90F2DC";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape25" -p "transform88";
	rename -uid "B5E8D4AC-4D48-A867-82E2-7C9D623D2CF8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder26";
	rename -uid "D3527058-5B42-E8DF-728B-0EB23D1C1E8C";
	setAttr ".t" -type "double3" -0.0031231368343787036 1.895432314677171 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.0986430979999353 0.14005219103648944 ;
createNode transform -n "transform87" -p "pCylinder26";
	rename -uid "F0EFBB09-7E4C-7C4C-DFEE-C3B311E59D25";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape26" -p "transform87";
	rename -uid "719C30C7-014A-3D9D-9928-80AE437BE506";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder27";
	rename -uid "F8D0D1FF-9749-AF98-A7DB-AF935976725C";
	setAttr ".t" -type "double3" 1.5370833029269519 1.6978664608694696 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform86" -p "pCylinder27";
	rename -uid "83FAB582-A34C-C310-085E-7195BA9E124D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape27" -p "transform86";
	rename -uid "9B7788DC-EE4C-263D-EDE1-689DFA75BCA4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder28";
	rename -uid "95A6F427-744A-BFA5-89BD-A6B5CA53CD61";
	setAttr ".t" -type "double3" 1.5370833029269519 1.4608349201379105 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform85" -p "pCylinder28";
	rename -uid "372CC6DC-9648-9E2E-7A3F-27A41B20A6B0";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape28" -p "transform85";
	rename -uid "9F13F8BB-7046-C32B-E535-FCA6A89E97D3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder29";
	rename -uid "9D732B76-E943-3934-1995-739FDBF4E3CE";
	setAttr ".t" -type "double3" 1.5370833029269519 1.2525344752526011 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform84" -p "pCylinder29";
	rename -uid "72D53A60-604F-5B41-3181-348B4F8239AA";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape29" -p "transform84";
	rename -uid "9F27172D-0C45-6941-5EB2-D9A9A5D56BCE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder30";
	rename -uid "88F21A8E-1345-E19A-11B3-19ADC3D398F1";
	setAttr ".t" -type "double3" 1.5370833029269519 1.0442340303672917 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform83" -p "pCylinder30";
	rename -uid "0B4C3848-7146-6AA3-67DD-E3901FA3DBDE";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape30" -p "transform83";
	rename -uid "FCEF9965-5741-190D-92E1-A88AB02B9A4E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder31";
	rename -uid "D96D6645-4546-2923-6CCA-A381917C668D";
	setAttr ".t" -type "double3" 1.5370833029269519 0.83593358548198227 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform82" -p "pCylinder31";
	rename -uid "7DAC427A-AD48-BE49-1CF8-57B2DE97F419";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape31" -p "transform82";
	rename -uid "032995E5-EC42-EFC6-7695-FCBAC483B8A9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder32";
	rename -uid "9DE414F2-CF4C-122B-DEC9-E4A0856FE382";
	setAttr ".t" -type "double3" 1.5370833029269519 0.63704936808436652 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform81" -p "pCylinder32";
	rename -uid "D37E1F03-A849-FB6E-2560-6B9EA686B1FA";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape32" -p "transform81";
	rename -uid "F50B9511-C042-93BA-21D9-30995E41C7AF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder33";
	rename -uid "39D2E584-734C-7FEA-C3EE-9990A31E7742";
	setAttr ".t" -type "double3" 1.5370833029269519 0.43816515068675121 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform80" -p "pCylinder33";
	rename -uid "F0CA2B3F-9D41-3C97-FC57-AA8C93BF847F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape33" -p "transform80";
	rename -uid "EB8987C9-0049-B5C8-5208-EF9619D688C7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder34";
	rename -uid "4FFC1CB9-F34A-75B6-92FF-C78A6673998F";
	setAttr ".t" -type "double3" 1.5370833029269519 0.21442040611443369 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform79" -p "pCylinder34";
	rename -uid "6DC4DDE6-B849-917F-2228-C2A1B53E78EB";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape34" -p "transform79";
	rename -uid "D72B1FA7-EC42-5A38-940C-64A621CA26AD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder35";
	rename -uid "125E1ECA-BD4B-B052-C171-A5ABADD45841";
	setAttr ".t" -type "double3" 1.5370833029269519 -0.0010374960663163124 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform78" -p "pCylinder35";
	rename -uid "404E553F-FA4E-A607-8261-DD96A8908685";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape35" -p "transform78";
	rename -uid "FBF0A0FF-CB40-C9BA-B883-82A307E02C33";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder36";
	rename -uid "03933995-8048-C934-8C0B-3FA93B5588B8";
	setAttr ".t" -type "double3" -1.5531909268844348 1.6978664608694696 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform77" -p "pCylinder36";
	rename -uid "B16E2042-FE43-0D09-81C0-24A9590524A6";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape36" -p "transform77";
	rename -uid "407E47EB-744D-3EFA-B041-C0A17EF10B34";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder37";
	rename -uid "92C87131-664A-6B90-1094-D99ACD459306";
	setAttr ".t" -type "double3" -1.5531909268844348 1.4608349201379105 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform76" -p "pCylinder37";
	rename -uid "17821E2A-F543-AA76-525E-299712D75930";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape37" -p "transform76";
	rename -uid "D3F68643-564C-28B5-8EC0-F7AD74CE9DEA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder38";
	rename -uid "5A4A6CD3-8F4F-4B6D-DC77-C8AA80925C76";
	setAttr ".t" -type "double3" -1.5531909268844348 1.2525344752526011 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform75" -p "pCylinder38";
	rename -uid "359A99C0-D349-2FF4-655D-ACB2693F200D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape38" -p "transform75";
	rename -uid "1DCF974A-784B-D9E7-03AE-2592143A6A35";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder39";
	rename -uid "E6367675-3648-58B6-88AB-CFA2E32D4008";
	setAttr ".t" -type "double3" -1.5531909268844348 1.0442340303672917 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform74" -p "pCylinder39";
	rename -uid "E2F0C28E-FD4C-7A8E-9BC8-5ABE5ED762A4";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape39" -p "transform74";
	rename -uid "928DB563-A34C-8746-7D9C-D18441F1EF70";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder40";
	rename -uid "27BF86E0-FE4C-1400-F6EF-379280A4FD91";
	setAttr ".t" -type "double3" -1.5531909268844348 0.83593358548198227 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform73" -p "pCylinder40";
	rename -uid "49CD629C-0343-742E-1568-5C84854BD10B";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape40" -p "transform73";
	rename -uid "006F5EF0-B64E-D598-F901-5A847826F0DE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder41";
	rename -uid "8BD767C0-E44D-D68C-5D6E-F1B3C7E8FA39";
	setAttr ".t" -type "double3" -1.5531909268844348 0.63704936808436652 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform72" -p "pCylinder41";
	rename -uid "EE4B20E4-2746-BB7D-BA33-8D96FA36F99D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape41" -p "transform72";
	rename -uid "C3A546FF-E740-49B3-57D8-F2896325F810";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder42";
	rename -uid "1A2E940C-5540-BEE1-4F6F-4ABDD9FFFB61";
	setAttr ".t" -type "double3" -1.5531909268844348 0.43816515068675121 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform71" -p "pCylinder42";
	rename -uid "8260AFA2-3A42-58B2-7C19-DA82115E3BC0";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape42" -p "transform71";
	rename -uid "2CF15A2F-944C-3AC1-A0F9-C7B0698E87CF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder43";
	rename -uid "E94546E7-C544-8C68-0819-0CB001C8E47B";
	setAttr ".t" -type "double3" -1.5531909268844348 0.21442040611443369 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform70" -p "pCylinder43";
	rename -uid "8ECABB96-8146-EA2B-9B7A-808E14E18CE4";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape43" -p "transform70";
	rename -uid "0F4BB946-824D-D135-8F49-78A9267F888D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder44";
	rename -uid "50C238E2-8745-1B6F-98E9-EDAE5E8B9754";
	setAttr ".t" -type "double3" -1.5531909268844348 -0.0010374960663163124 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.9941062797118565 0.14005219103648944 ;
createNode transform -n "transform69" -p "pCylinder44";
	rename -uid "BEC93063-C844-7F33-F9FF-12B04E9E66F4";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape44" -p "transform69";
	rename -uid "3803FDC4-1145-357D-F000-F08CC9215256";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder45";
	rename -uid "A8D7FC1D-DE45-6C2F-8386-558A7A83E883";
	setAttr ".t" -type "double3" -1.9912347569635682 2.1244638417820942 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform68" -p "pCylinder45";
	rename -uid "A4AE7206-494A-BBFC-0DEE-158EB0FF1FB3";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape45" -p "transform68";
	rename -uid "ACA5ABF3-F24D-0BD7-CD43-3B8C48CD732B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder46";
	rename -uid "13DF170A-C149-005B-2E21-20B34746A8D9";
	setAttr ".t" -type "double3" -1.9912347569635682 1.9149842001246189 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform67" -p "pCylinder46";
	rename -uid "2387DB0B-004F-8DBC-5EC0-69AFA4857468";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape46" -p "transform67";
	rename -uid "D7FBFF60-424B-FAE8-1A5D-B188AA7B3EA4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder47";
	rename -uid "069BC7B8-A04C-789C-A280-14B667345F47";
	setAttr ".t" -type "double3" -1.9912347569635682 1.6987471506717415 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform66" -p "pCylinder47";
	rename -uid "5F428401-3D47-5A0D-812D-BD878FA2CB69";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape47" -p "transform66";
	rename -uid "C037256E-E846-EA9E-4B81-D19C364FE4DA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder49";
	rename -uid "7C025D47-9E49-C6A2-2314-E099E1D18592";
	setAttr ".t" -type "double3" -1.9912347569635682 1.4757526934234615 1.3432834554199531 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.97710500280885826 0.14005219103648944 ;
createNode transform -n "transform65" -p "pCylinder49";
	rename -uid "0EDD1DAF-D348-1395-2DC1-5EAF242978A3";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape49" -p "transform65";
	rename -uid "2DFDDB6B-364D-37C2-25B2-62AD5CBDC748";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder53";
	rename -uid "58C1DD01-2140-5496-0403-FDB79F4CF4DA";
	setAttr ".t" -type "double3" -1.9912347569635682 0.42834872320517037 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform64" -p "pCylinder53";
	rename -uid "B477FA7A-2942-84ED-0600-74B9EEDC9D41";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape53" -p "transform64";
	rename -uid "95300934-EF4A-C2D2-2C1C-DCA29849E9B8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder54";
	rename -uid "FCA1A7E5-CB42-AC88-BCDD-05BDC089334E";
	setAttr ".t" -type "double3" -1.9912347569635682 0.21384499639817467 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform63" -p "pCylinder54";
	rename -uid "9657C5AA-E145-B6B7-0B3E-CEBC664C978F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape54" -p "transform63";
	rename -uid "9ED96A04-ED4B-BFD5-74C3-8EBD13E8BF88";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder55";
	rename -uid "E91DDEA0-6B42-C0D9-15A4-45B01488C6EB";
	setAttr ".t" -type "double3" -1.9912347569635682 0 -0.45127988658416673 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 2.7837800441802347 0.14005219103648944 ;
createNode transform -n "transform62" -p "pCylinder55";
	rename -uid "FA8DA58F-0A40-C0C4-CCAA-DD9CCBE7967E";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape55" -p "transform62";
	rename -uid "0BC642CE-5C4D-FCF4-F7AB-A093064AA8AC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder56";
	rename -uid "10C1BB6D-234D-A650-FFE8-3893DA8D87F7";
	setAttr ".t" -type "double3" -1.9912347569635682 1.4757526934234615 -2.1132005393689548 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1245609066317679 0.14005219103648944 ;
createNode transform -n "transform61" -p "pCylinder56";
	rename -uid "AA229BDE-C544-EA84-9CFE-8280C229268B";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape56" -p "transform61";
	rename -uid "C8C37979-EA44-00E4-B293-AABD538DFAB7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder57";
	rename -uid "BD71641F-2048-8D00-B218-168A56634963";
	setAttr ".t" -type "double3" -1.9912347569635682 1.239015033315553 -2.1132005393689548 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1245609066317679 0.14005219103648944 ;
createNode transform -n "transform60" -p "pCylinder57";
	rename -uid "1FC39299-F646-3B50-9B92-4EB07DCAA41F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape57" -p "transform60";
	rename -uid "EB98878D-6346-99CD-F3DE-9D8B0D85C28E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder58";
	rename -uid "BA86D02D-EE4F-33E3-8544-D2B0B993D21E";
	setAttr ".t" -type "double3" -1.9912347569635682 1.239015033315553 1.3432834554199531 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.97710500280885826 0.14005219103648944 ;
createNode transform -n "transform59" -p "pCylinder58";
	rename -uid "820946D7-2E4E-436C-0BDD-51860E890194";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape58" -p "transform59";
	rename -uid "4666EA6C-3B44-B783-37F9-EBB4168D63A3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder59";
	rename -uid "B10FAB99-F641-4FCD-0BD8-BD9ABFF75640";
	setAttr ".t" -type "double3" -1.9912347569635682 1.0432799892251288 -2.1132005393689548 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1245609066317679 0.14005219103648944 ;
createNode transform -n "transform58" -p "pCylinder59";
	rename -uid "BF737C52-BC40-B59F-E4C1-E1B214BCFE6B";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape59" -p "transform58";
	rename -uid "432CA72E-3042-456E-D735-46A4B623E8FB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder60";
	rename -uid "4B40E56B-8041-8EDB-7F82-A583E3F3120E";
	setAttr ".t" -type "double3" -1.9912347569635682 1.0432799892251288 1.3432834554199531 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.97710500280885826 0.14005219103648944 ;
createNode transform -n "transform57" -p "pCylinder60";
	rename -uid "D480886E-ED49-2092-889C-2282FE9400C9";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape60" -p "transform57";
	rename -uid "B81F1ABE-4A48-DF4D-152A-9891D1C9E7B6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder61";
	rename -uid "07EA51C9-1143-63CB-2D61-9DA4BA0F8BDD";
	setAttr ".t" -type "double3" -1.9912347569635682 0.8146384639155746 -2.1132005393689548 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1245609066317679 0.14005219103648944 ;
createNode transform -n "transform56" -p "pCylinder61";
	rename -uid "896B43A7-C049-3DDA-5A85-3992046315C2";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape61" -p "transform56";
	rename -uid "89BFF95E-EF43-A2B1-60EE-4880777D79E0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder62";
	rename -uid "013C1806-8D47-06F7-60B5-82AEED79D840";
	setAttr ".t" -type "double3" -1.9912347569635682 0.8146384639155746 1.3432834554199531 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.97710500280885826 0.14005219103648944 ;
createNode transform -n "transform55" -p "pCylinder62";
	rename -uid "303057FB-B54A-57E8-7270-3D9471E09ED6";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape62" -p "transform55";
	rename -uid "6A8338BA-4942-7095-DB70-DFA5F438A18A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder63";
	rename -uid "9C763E40-1144-AD68-2B3C-AE82757B729F";
	setAttr ".t" -type "double3" -1.9912347569635682 0.63627465648044046 -2.1132005393689548 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1245609066317679 0.14005219103648944 ;
createNode transform -n "transform54" -p "pCylinder63";
	rename -uid "13D9220E-8645-C415-44B9-A49F16C1C012";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape63" -p "transform54";
	rename -uid "74C6C13A-A643-C171-8991-8EB59AB27C94";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder64";
	rename -uid "727A6D6E-0D4D-CE38-C88D-C785CA57ABB8";
	setAttr ".t" -type "double3" -1.9912347569635682 0.63627465648044046 1.3432834554199531 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.97710500280885826 0.14005219103648944 ;
createNode transform -n "transform53" -p "pCylinder64";
	rename -uid "A54CE553-7342-4F5D-6D06-D186A9E1248A";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape64" -p "transform53";
	rename -uid "B8E9E297-9D43-8008-E7B9-71A721BBCBE6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder65";
	rename -uid "C34FBBD1-4140-5DDD-DA06-CF823A75EA3C";
	setAttr ".t" -type "double3" -2.0302475774537632 0.88611015922214031 -0.30222830402394751 ;
	setAttr ".s" -type "double3" 0.04237711302045654 0.7083998071507519 0.04237711302045654 ;
createNode transform -n "transform52" -p "pCylinder65";
	rename -uid "0320DE4F-4240-4F6B-00E6-28908DA5C0E0";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape65" -p "transform52";
	rename -uid "17374D97-A24B-9D20-1A1B-00B5794F74BD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder66";
	rename -uid "3381EFAC-0044-06F5-BF8B-E582D35EDF0E";
	setAttr ".t" -type "double3" -2.0302475774537632 1.0591666658113292 -0.30222830402394751 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.058798163099557633 0.7083998071507519 0.04237711302045654 ;
createNode transform -n "transform51" -p "pCylinder66";
	rename -uid "87C25214-7048-FF54-6DC2-AD931DF48B93";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape66" -p "transform51";
	rename -uid "D325AF57-854D-887A-8419-33BD06B215CB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder67";
	rename -uid "2FFDFBEC-AA4D-7F3B-434B-BBA159DDF0D3";
	setAttr ".t" -type "double3" -2.1430273768067982 1.6461505528206788 -0.30222830402394751 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.090764115594402159 0.68811422283818946 0.090764115594402159 ;
createNode transform -n "transform50" -p "pCylinder67";
	rename -uid "2ECED3C6-A845-B792-E6DF-6095C3B5B3E8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape67" -p "transform50";
	rename -uid "B2C2ED6B-7443-00EE-4B10-F7A705267E56";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder68";
	rename -uid "F4DD4FB0-1D49-87B4-CB2C-659288ABBB14";
	setAttr ".t" -type "double3" -2.1430273768067982 0.52551237359399439 -0.30222830402394751 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.12593503643654452 0.68811422283818946 0.090764115594402159 ;
createNode transform -n "transform49" -p "pCylinder68";
	rename -uid "0BDC7200-B644-D780-1F38-46A71193001F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape68" -p "transform49";
	rename -uid "B64A372E-F84F-C17C-432D-758AF4E9C6F4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder69";
	rename -uid "12578607-1348-4528-A3D9-75BD4CDF828E";
	setAttr ".t" -type "double3" -2.1430273768067982 1.0776972985546238 -1.0488386802320715 ;
	setAttr ".s" -type "double3" 0.12593503643654452 0.62338968798691408 0.090764115594402159 ;
createNode transform -n "transform48" -p "pCylinder69";
	rename -uid "753A4346-4A4A-8DFD-4EC2-A6AAFC9A8CF2";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape69" -p "transform48";
	rename -uid "3077BE35-9E46-A94D-0F79-5BB1672DFF9A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder70";
	rename -uid "D270217E-4343-3995-7C02-9DAD741C4096";
	setAttr ".t" -type "double3" -2.1430273768067982 1.0776972985546238 0.3929649403886466 ;
	setAttr ".s" -type "double3" 0.12593503643654452 0.62338968798691408 0.090764115594402159 ;
createNode transform -n "transform47" -p "pCylinder70";
	rename -uid "BB97D17A-D14E-552E-8CA0-92A67B12C076";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape70" -p "transform47";
	rename -uid "F498C23A-9841-6201-254F-BEA0B672463E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "0A002F6A-AC42-7A49-A40C-CFB8E17CD685";
	setAttr ".t" -type "double3" 1.1379903273136318 3.2035582360766242 -0.50997986027897024 ;
	setAttr ".r" -type "double3" 0 0 -45 ;
	setAttr ".s" -type "double3" 3.5173646606857316 0.40681624089535573 6.0678184710707432 ;
createNode transform -n "transform46" -p "pCube5";
	rename -uid "C3D76B0F-0945-FF8C-58D1-A78081ADA3A9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform46";
	rename -uid "772FD815-A047-7202-729D-059B0F940D2B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.46875 2 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 922 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[3]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[10]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[46]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[47]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[48]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[50]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[55]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[56]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[57]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[58]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[59]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[60]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[65]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[92]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[118]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[119]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[120]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[121]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[122]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[128]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[130]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[131]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[132]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[133]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[134]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[135]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[136]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[137]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[145]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[158]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[160]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[162]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[180]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[181]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[201]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[254]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[255]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[256]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[257]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[258]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[271]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[272]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[273]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[274]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[275]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[276]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[277]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[278]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[279]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[280]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[281]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[282]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[283]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[284]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[292]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[293]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[294]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[318]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[319]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[326]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[340]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[341]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[342]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[343]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[364]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[365]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[366]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[367]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[400]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[401]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[402]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[403]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[408]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[409]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[411]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[412]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[413]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[414]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[415]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[432]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[433]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[435]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[528]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[529]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[530]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[531]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[532]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[533]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[534]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[535]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[552]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[553]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[554]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[555]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[556]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[557]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[558]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[559]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[560]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[563]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[564]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[565]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[566]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[567]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[568]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[570]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[571]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[573]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[575]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[589]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[590]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[591]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[592]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[594]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[605]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[606]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[607]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[635]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[672]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[673]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[674]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[675]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[676]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[677]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[679]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[680]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[720]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[721]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[722]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[723]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[724]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[725]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[726]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[727]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[728]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[729]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[730]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[792]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[793]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[794]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[795]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[796]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[797]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[805]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[806]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[808]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[810]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[811]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[812]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[813]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[814]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[815]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[824]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[841]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[842]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[843]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[866]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[867]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[891]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[892]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[893]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[960]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[961]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[962]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[963]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[964]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[966]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[967]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[971]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[983]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[984]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[985]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[986]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[987]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[988]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[989]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[990]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[991]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[992]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[993]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[994]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[995]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[996]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[997]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[999]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1002]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1013]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1014]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1015]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1052]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1062]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1082]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1083]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1085]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1118]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1119]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1120]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1121]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1122]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1123]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1158]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1160]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1161]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[1166]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1190]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1191]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1196]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1197]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1199]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1201]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1205]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[1206]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1209]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1226]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1227]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1228]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1238]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1239]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1240]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1262]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1263]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1264]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1268]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1271]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1272]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1273]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[1278]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1286]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1287]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1299]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1311]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1312]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1358]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1359]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1360]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1363]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1365]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1366]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1376]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1377]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1378]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1379]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1380]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1381]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1382]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1383]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1385]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1386]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[1387]" -type "float3" 0 0 -0.038085718 ;
	setAttr ".pt[1391]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1403]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1404]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1466]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1467]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1468]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1469]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[1502]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1503]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1504]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1505]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1506]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1507]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".pt[1510]" -type "float3" 0 0 -0.038085714 ;
	setAttr ".dr" 1;
createNode transform -n "pCube6";
	rename -uid "93FAE67A-4240-9232-71E2-5FA68A7386A5";
	setAttr ".t" -type "double3" -1.1070652586112344 3.2035582360766242 -0.50997986027897024 ;
	setAttr ".r" -type "double3" 0 0 45 ;
	setAttr ".s" -type "double3" 3.5173646606857316 0.40681624089535573 6.0678184710707432 ;
createNode mesh -n "polySurfaceShape1" -p "pCube6";
	rename -uid "9E8BB28E-2A43-7B11-A9FE-5BB6DA977F54";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[0:3]" -type "float3"  0 2.9802322e-08 0 0 2.9802322e-08 
		0 0 -2.9802322e-08 0 0 -2.9802322e-08 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "transform45" -p "pCube6";
	rename -uid "34B65AEB-A04E-09C5-C6CF-9C8285018FD4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape6" -p "transform45";
	rename -uid "DC6BFABD-1749-B541-7C35-EEB14B69E839";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" -0.4375 1.59375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 1040 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[2]" -type "float3" 0 0 0.038792409 ;
	setAttr ".pt[3]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[4]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[5]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[6]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[7]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[10]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[12]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[13]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[18]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[20]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[21]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[26]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[27]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[28]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[29]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[30]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[31]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[34]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[36]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[37]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[38]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[40]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[42]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[43]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[44]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[45]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[46]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[47]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[48]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[50]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[51]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[52]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[54]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[55]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[56]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[57]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[58]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[59]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[60]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[61]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[63]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[67]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[69]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[70]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[71]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[72]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[73]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[74]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[75]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[76]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[77]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[78]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[80]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[81]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[82]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[83]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[84]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[85]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[86]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[87]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[88]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[97]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[98]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[99]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[100]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[101]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[102]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[106]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[107]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[108]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[109]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[110]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[111]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[112]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[116]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[118]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[119]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[120]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[121]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[122]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[123]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[124]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[125]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[126]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[127]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[128]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[129]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[130]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[131]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[132]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[133]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[134]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[135]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[136]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[137]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[138]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[139]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[140]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[142]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[143]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[144]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[145]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[146]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[147]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[151]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[152]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[156]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[161]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[163]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[164]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[165]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[166]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[167]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[168]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[170]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[171]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[172]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[173]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[174]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[175]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[176]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[177]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[179]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[181]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[182]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[183]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[184]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[186]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[187]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[190]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[191]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[193]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[194]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[195]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[196]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[208]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[209]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[210]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[211]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[216]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[218]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[219]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[220]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[225]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[227]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[228]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[229]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[230]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[231]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[232]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[233]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[234]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[235]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[236]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[237]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[238]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[239]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[240]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[241]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[242]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[244]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[245]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[246]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[247]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[248]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[249]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[251]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[252]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[253]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[254]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[255]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[256]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[257]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[263]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[264]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[265]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[271]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[272]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[273]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[274]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[275]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[276]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[277]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[278]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[279]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[280]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[281]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[282]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[283]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[284]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[285]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[286]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[287]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[288]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[289]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[290]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[292]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[293]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[298]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[299]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[300]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[301]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[302]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[303]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[304]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[305]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[306]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[307]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[308]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[309]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[313]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[314]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[315]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[316]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[317]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[320]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[321]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[322]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[323]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[328]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[329]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[330]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[331]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[332]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[333]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[334]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[335]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[336]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[337]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[338]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[339]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[340]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[341]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[342]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[343]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[344]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[345]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[346]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[347]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[348]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[349]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[350]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[351]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[352]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[353]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[354]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[355]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[358]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[364]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[365]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[366]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[367]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[369]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[370]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[371]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[375]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[381]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[382]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[383]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[384]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[386]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[387]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[388]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[389]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[392]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[393]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[394]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[395]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[396]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[398]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[400]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[401]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[402]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[403]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[404]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[405]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[406]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[407]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[408]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[409]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[410]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[411]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[412]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[413]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[414]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[415]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[416]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[417]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[418]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[419]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[420]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[421]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[422]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[423]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[424]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[425]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[436]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[437]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[438]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[439]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[440]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[441]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[443]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[444]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[445]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[446]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[447]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[448]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[450]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[452]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[453]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[454]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[455]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[461]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[462]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[463]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[475]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[480]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[481]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[482]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[483]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[488]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[490]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[491]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[492]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[493]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[495]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[496]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[497]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[499]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[500]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[501]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[502]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[503]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[504]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[505]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[506]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[507]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[508]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[509]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[510]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[513]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[516]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[517]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[518]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[519]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[520]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[521]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[522]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[523]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[524]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[525]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[526]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[527]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[528]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[529]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[530]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[531]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[532]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[540]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[541]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[542]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[543]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[544]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[552]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[553]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[554]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[555]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[556]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[557]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[558]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[559]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[560]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[561]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[562]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[563]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[564]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[565]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[566]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[567]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[568]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[569]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[570]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[571]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[572]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[573]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[574]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[575]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[577]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[578]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[579]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[580]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[581]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[582]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[583]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[584]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[588]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[589]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[590]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[591]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[598]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[599]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[600]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[601]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[602]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[603]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[604]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[605]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[608]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[609]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[610]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[611]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[612]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[613]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[614]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[615]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[616]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[622]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[623]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[624]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[625]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[626]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[627]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[628]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[629]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[630]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[631]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[632]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[636]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[637]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[638]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[639]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[640]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[641]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[642]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[648]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[649]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[650]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[651]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[652]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[653]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[654]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[655]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[656]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[657]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[658]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[659]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[660]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[661]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[662]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[663]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[664]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[665]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[666]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[667]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[668]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[669]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[670]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[671]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[672]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[673]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[674]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[675]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[676]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[677]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[678]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[679]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[680]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[681]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[682]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[683]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[684]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[685]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[686]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[687]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[688]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[689]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[692]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[693]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[694]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[695]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[696]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[697]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[698]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[699]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[700]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[701]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[702]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[703]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[704]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[708]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[709]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[710]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[711]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[721]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[722]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[723]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[724]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[725]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[726]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[727]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[728]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[733]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[734]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[735]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[736]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[737]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[738]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[739]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[744]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[745]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[746]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[747]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[756]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[757]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[758]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[759]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[760]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[761]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[762]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[763]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[764]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[768]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[769]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[770]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[771]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[772]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[773]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[777]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[778]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[779]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[780]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[781]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[782]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[783]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[784]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[785]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[790]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[792]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[793]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[794]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[795]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[796]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[797]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[798]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[799]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[800]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[801]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[802]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[803]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[804]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[805]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[806]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[807]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[808]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[809]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[810]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[811]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[812]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[813]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[814]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[815]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[816]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[817]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[818]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[822]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[823]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[824]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[825]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[826]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[831]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[832]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[833]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[837]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[838]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[839]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[844]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[846]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[847]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[848]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[849]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[850]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[851]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[853]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[854]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[855]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[856]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[857]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[860]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[861]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[862]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[863]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[870]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[872]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[873]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[874]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[875]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[879]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[880]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[882]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[883]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[884]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[885]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[886]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[900]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[901]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[902]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[903]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[910]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[912]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[913]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[914]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[915]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[917]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[920]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[921]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[924]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[925]" -type "float3" 0 0 0.038792409 ;
	setAttr ".pt[926]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[927]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[928]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[929]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[930]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[931]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[932]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[933]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[934]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[935]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[936]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[937]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[938]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[939]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[940]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[942]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[943]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[946]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[947]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[948]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[949]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[950]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[951]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[952]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[953]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[954]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[956]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[957]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[958]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[959]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[960]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[961]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[962]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[963]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[964]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[972]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[973]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[974]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[975]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[983]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[984]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[985]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[986]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[987]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[988]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[989]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[990]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[991]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[992]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[993]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[994]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[995]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[996]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[997]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[998]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[999]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1000]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1001]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1002]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1003]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1004]" -type "float3" 0 0 0.038792409 ;
	setAttr ".pt[1005]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1006]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1007]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1008]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1009]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1011]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1012]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1013]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1014]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1015]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1020]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1021]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1023]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1024]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1025]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1026]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1027]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1029]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1030]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1031]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1032]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1033]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1034]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1035]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1036]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1037]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1041]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1043]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1044]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1045]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1046]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1047]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1048]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1049]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1053]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1054]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1055]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1056]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1057]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1060]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1064]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1065]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1066]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1067]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1068]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1069]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1071]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1072]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1073]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1074]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1075]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1076]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1077]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1078]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1079]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1080]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1081]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1082]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1083]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1084]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1085]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1086]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1087]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1088]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1089]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1090]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1091]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1092]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1093]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1094]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1095]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1096]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1097]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1098]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1099]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1100]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1101]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1102]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1103]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1104]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1105]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1109]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1118]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1119]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1120]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1121]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1122]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1123]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1126]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1127]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1128]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1129]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1135]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1144]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1145]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1146]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1147]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1148]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1149]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1152]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1153]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1154]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1156]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1158]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1159]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1160]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1161]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1162]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1163]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1167]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1168]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1169]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1170]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1171]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1173]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1178]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1180]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1181]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1183]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1184]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1185]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1187]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1188]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1189]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1190]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1193]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1196]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1197]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1198]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1199]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1200]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1201]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1202]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1203]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1205]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1208]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1209]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1210]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1211]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1214]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1215]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1217]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1218]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1220]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1221]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1222]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1223]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1224]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1225]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1226]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1227]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1228]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[1229]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1230]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1231]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1232]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1233]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1235]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1238]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1239]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1241]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1242]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1244]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1247]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1248]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1250]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1251]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1252]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1255]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1256]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1257]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1258]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1262]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1263]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1264]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1265]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1266]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1267]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[1268]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1269]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1270]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1271]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1272]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1273]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1274]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1277]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1278]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1279]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1282]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1284]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1285]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1289]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1290]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1291]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1292]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1293]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1294]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1295]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1296]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1297]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1302]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1303]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1305]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1307]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1308]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1309]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1316]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1317]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1322]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1323]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1324]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1326]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1328]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1331]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1332]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1333]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1334]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1335]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1336]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1337]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1338]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1339]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1340]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1341]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1342]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1345]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1347]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1348]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1349]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1350]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1351]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1352]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1353]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1354]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1355]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1356]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1358]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1359]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1360]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1367]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[1368]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[1369]" -type "float3" 0 0 0.030281028 ;
	setAttr ".pt[1376]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1377]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1378]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1379]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1380]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1381]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1382]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1383]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1384]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1385]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1386]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1387]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1388]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1389]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1390]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1391]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1392]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1393]" -type "float3" 0 0 0.032219544 ;
	setAttr ".pt[1394]" -type "float3" 0 0 0.038792409 ;
	setAttr ".pt[1395]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1396]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1397]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1398]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1399]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1400]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1401]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1403]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1404]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1405]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1409]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1412]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1413]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1414]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1416]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1417]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1418]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1419]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1420]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1421]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1422]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1423]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1427]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1428]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1430]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1431]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1432]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1433]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1434]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1435]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1439]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1440]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1441]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1444]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1447]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1448]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1449]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1450]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1451]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1452]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1453]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1455]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1456]" -type "float3" 0 0 0.04641572 ;
	setAttr ".pt[1457]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1458]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1459]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1460]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1461]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1462]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1463]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1464]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1465]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1466]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1467]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1468]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1469]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1470]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1471]" -type "float3" 0 0 0.028010676 ;
	setAttr ".pt[1472]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1473]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1474]" -type "float3" 0 0 0.066803068 ;
	setAttr ".pt[1475]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1476]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1477]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1479]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1480]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1481]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1482]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1483]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1484]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1485]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1486]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1487]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1488]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1489]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1493]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1502]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1503]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1504]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1505]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1506]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1507]" -type "float3" 0 0 0.0042088712 ;
	setAttr ".pt[1511]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1512]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1513]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1514]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1516]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1520]" -type "float3" 0 0 0.038792394 ;
	setAttr ".pt[1529]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1530]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1531]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1532]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1533]" -type "float3" 0 0 0.076696739 ;
	setAttr ".pt[1534]" -type "float3" 0 0 0.076696739 ;
	setAttr ".dr" 1;
createNode transform -n "pCylinder71";
	rename -uid "7920D72E-5444-A553-A0FB-1D868041A73E";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.3523251479725937 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.8151897978321612 0.14005219103648944 ;
createNode transform -n "transform44" -p "pCylinder71";
	rename -uid "B089CBE1-9743-1CA4-790A-2490C5C6F96D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape71" -p "transform44";
	rename -uid "67F2EB47-2848-46FF-26E8-4BAE24AD5501";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder73";
	rename -uid "DB76FF8A-D44E-66C4-9A6D-CBA3E68E3BF3";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.8094895410183272 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.5619972336538588 0.14005219103648944 ;
createNode transform -n "transform43" -p "pCylinder73";
	rename -uid "0C124DE8-EF46-2E1C-3E25-EA905DA8760B";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape73" -p "transform43";
	rename -uid "085318CE-EF4B-5B74-F0E3-F394ECAB7E6B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder75";
	rename -uid "DA04E69E-C64B-280B-7CC6-D1A1D66F5DFC";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.2870076699779616 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.90961385162423258 0.14005219103648944 ;
createNode transform -n "transform42" -p "pCylinder75";
	rename -uid "918CD0C4-D34E-FDE3-E93C-7F997147960F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape75" -p "transform42";
	rename -uid "FA93B7CB-D646-D284-3E4D-01B4282A86D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder77";
	rename -uid "47CC21BC-0D44-AD65-B05C-9C8F3A417B51";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.8349965340770868 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.63414461003906819 0.14005219103648944 ;
createNode transform -n "transform41" -p "pCylinder77";
	rename -uid "B3E8315D-3E4B-6BA3-4611-2294987C4096";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape77" -p "transform41";
	rename -uid "7049A49A-8E46-86B3-5E52-D1A25B6FEBA4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder78";
	rename -uid "DD4C08BB-4D40-6831-AAB3-4E805A8F902E";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.8349965340770868 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.63414461003906819 0.14005219103648944 ;
createNode transform -n "transform40" -p "pCylinder78";
	rename -uid "7FCCB437-974B-C56F-1D7A-2F8F7701B6A8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape78" -p "transform40";
	rename -uid "7531187B-584D-A141-4B72-71BE09B98F06";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder79";
	rename -uid "65C26A29-414B-1FB0-825D-8FA5940AEE91";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.8094895410183272 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.5619972336538588 0.14005219103648944 ;
createNode transform -n "transform39" -p "pCylinder79";
	rename -uid "67B2D591-6945-409F-9836-8D92EB3CBEA8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape79" -p "transform39";
	rename -uid "3D5034D0-9C48-91E9-69DE-76B84C3E7DC9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder82";
	rename -uid "BCEE9215-F149-DD7B-0594-B99DDCA6D4B7";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.2870076699779616 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.90961385162423258 0.14005219103648944 ;
createNode transform -n "transform38" -p "pCylinder82";
	rename -uid "8D14A753-D34A-ED83-7FB4-F6B263EA010D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape82" -p "transform38";
	rename -uid "3BE5F67A-8040-59FD-8B13-FDA773B4BAB5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder84";
	rename -uid "1FF1B92F-3947-6005-4F04-179FFABA78B0";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.3523251479725937 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.8151897978321612 0.14005219103648944 ;
createNode transform -n "transform37" -p "pCylinder84";
	rename -uid "B630C163-FC41-5A16-504E-69B36586CF73";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape84" -p "transform37";
	rename -uid "30616110-D541-6258-BFDA-CDB26F77D82B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder85";
	rename -uid "945AE993-1942-FBB5-FA3B-E4BDAAE0C940";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.8349965340770868 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.63414461003906819 0.14005219103648944 ;
createNode transform -n "transform36" -p "pCylinder85";
	rename -uid "AEB6265C-3347-F763-DF7D-6881317EE05D";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape85" -p "transform36";
	rename -uid "D9917C6D-1C47-D217-D8B1-36830DF3F0BF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder86";
	rename -uid "7E1BCC25-7C41-BD2F-A2A6-36B5D88A54F3";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.8094895410183272 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.5619972336538588 0.14005219103648944 ;
createNode transform -n "transform35" -p "pCylinder86";
	rename -uid "B7C1C967-EB40-502D-775C-09A1A402A5C3";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape86" -p "transform35";
	rename -uid "D1C87880-9741-2D56-4966-42B96A88AB47";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder87";
	rename -uid "C452EDA6-CA4A-FB72-D2A0-08956E924E79";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.5751365687163088 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.8151897978321612 0.14005219103648944 ;
createNode transform -n "transform34" -p "pCylinder87";
	rename -uid "C50EDA34-C944-2543-88E3-90BDCA12EBD8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape87" -p "transform34";
	rename -uid "83932ACC-4B4C-373F-A4C8-8D81A4EA4977";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder88";
	rename -uid "88166767-D140-F826-FAE0-37A8F33F826E";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.5476733719545228 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.63414461003906819 0.14005219103648944 ;
createNode transform -n "transform33" -p "pCylinder88";
	rename -uid "561CF27B-D84F-984E-EA80-4FA67307CE25";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape88" -p "transform33";
	rename -uid "BCAFCF87-4D4C-09B0-552C-D4AD372B2159";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder89";
	rename -uid "F72842E5-6C4C-64D7-31E4-0090069F03A2";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.2870076699779616 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.90961385162423258 0.14005219103648944 ;
createNode transform -n "transform32" -p "pCylinder89";
	rename -uid "A5D48DF4-8F46-9A56-25AE-99AF32FEF06E";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape89" -p "transform32";
	rename -uid "5776205B-4F43-CA64-A5DE-5E8CA8CF25D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder90";
	rename -uid "0F0ECB98-424C-36A9-8E57-61B7BEF74B5B";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.0408444711236493 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.183710184525079 0.14005219103648944 ;
createNode transform -n "transform31" -p "pCylinder90";
	rename -uid "189E7E93-E848-EAF8-DCDC-35B7FB30461F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape90" -p "transform31";
	rename -uid "9E859AE2-A542-3F5B-4B4C-D8BB1CA5D3EC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder91";
	rename -uid "86B91B94-AB46-0375-E378-378F08CEE8C4";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.3523251479725937 1.8155016156668049 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.8151897978321612 0.14005219103648944 ;
createNode transform -n "transform30" -p "pCylinder91";
	rename -uid "CD80E92C-174F-AA99-0041-26AE427F2C24";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape91" -p "transform30";
	rename -uid "A2C03ADA-9948-2FAD-A3A7-BAAD4959A62F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder92";
	rename -uid "F1B52D25-D841-DCEF-37E8-6CAE699BB92A";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.3511181079193695 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.9155673463375094 0.14005219103648944 ;
createNode transform -n "transform29" -p "pCylinder92";
	rename -uid "2FD281ED-624F-D2A1-6987-E0976EE80B74";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape92" -p "transform29";
	rename -uid "9BADEA28-1D48-4EBF-D364-5A859170DA20";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder93";
	rename -uid "77C5E2D8-EB44-7F9A-671A-60ACAEE02F8B";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.5994444912892187 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.7801841420136932 0.14005219103648944 ;
createNode transform -n "transform28" -p "pCylinder93";
	rename -uid "03EDCDC3-9A46-CA92-0EFB-26B92149CA55";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape93" -p "transform28";
	rename -uid "81DE3484-3E4D-DB0B-F6D5-F8B67C72A280";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder94";
	rename -uid "185467F6-8E48-0AE7-A0DF-BD89D7E18793";
	setAttr ".t" -type "double3" -0.0031231368343787036 2.8262064451895879 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.555747097212403 0.14005219103648944 ;
createNode transform -n "transform27" -p "pCylinder94";
	rename -uid "03300DBC-3442-C7D9-3399-70847040BCBC";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape94" -p "transform27";
	rename -uid "7D0379D7-D143-2F23-32D9-0A8E6E65F27F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder95";
	rename -uid "99EA33D1-C34D-1056-BBB7-94B9BFF47A50";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.0637454813662077 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.3179640135844435 0.14005219103648944 ;
createNode transform -n "transform26" -p "pCylinder95";
	rename -uid "60E85122-0947-0B1A-472F-29918C26CD1C";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape95" -p "transform26";
	rename -uid "ED3652CB-D244-2285-F217-8583D0323351";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder96";
	rename -uid "7C3B67DF-5F4F-B5C3-B632-68B2550B7033";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.2907245958159721 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 1.1048195755838741 0.14005219103648944 ;
createNode transform -n "transform25" -p "pCylinder96";
	rename -uid "5FDA1935-594B-8CFD-FB03-F3B8D7DDC59C";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape96" -p "transform25";
	rename -uid "7B42A53D-0148-4C3B-FF8B-F2841D665E38";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder97";
	rename -uid "04A979B8-F14B-D816-7D6D-C08D437FACF7";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.5070437084712451 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.65787463165200089 0.14005219103648944 ;
createNode transform -n "transform24" -p "pCylinder97";
	rename -uid "A38F04CF-984F-8667-4A49-4CBF689A28A8";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape97" -p "transform24";
	rename -uid "180BB80B-0D4F-068B-3313-2D8AC315779D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder98";
	rename -uid "33A663D2-6D4F-7FBD-64E9-28B1EAB6720C";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.7342161192226917 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.62403329316265632 0.14005219103648944 ;
createNode transform -n "transform23" -p "pCylinder98";
	rename -uid "B35B5B78-1D44-4795-99C2-B4AD072168DD";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape98" -p "transform23";
	rename -uid "6B3832FC-AB45-FB6F-025F-7B8787F7625F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder99";
	rename -uid "DA48B2E4-374B-3FA3-65D5-E794FCFCE5B7";
	setAttr ".t" -type "double3" -0.0031231368343787036 3.9830367594202767 -2.8516673252467282 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.14005219103648944 0.34312404558220988 0.14005219103648944 ;
createNode transform -n "transform22" -p "pCylinder99";
	rename -uid "DA3AC72C-2045-B0C3-148A-12AE845D63E9";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape99" -p "transform22";
	rename -uid "36C6AE7C-D645-8A64-1F12-87B360FBB2B1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:23]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "16B342AE-2E45-F14B-AA4B-2A9D5AFA5AAC";
	setAttr ".t" -type "double3" 0.29533894431683372 4.9801850934109417 0.83724570369921736 ;
	setAttr ".s" -type "double3" 0.7817866766055056 0.36812305756785801 0.7817866766055056 ;
createNode transform -n "transform21" -p "pCube7";
	rename -uid "E196C4F4-E448-B260-22D8-9FA0E2603D8F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape7" -p "transform21";
	rename -uid "8393D0DB-E746-928E-0CB1-3D962803CB1C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 2.0000004768371582 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pPlane2";
	rename -uid "3DCACBC8-3147-0AAA-E832-56AAAC847F53";
	setAttr ".t" -type "double3" 0 0 -0.54783480589718403 ;
	setAttr ".s" -type "double3" 4.1760478178768876 4.1760478178768876 4.820902130096238 ;
createNode transform -n "transform20" -p "pPlane2";
	rename -uid "ECACBFAD-2D4C-E076-8641-E38A370838CB";
	setAttr ".v" no;
createNode mesh -n "pPlaneShape2" -p "transform20";
	rename -uid "9F9D5FB8-854C-8D01-7F0A-43896CB2C438";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCone1";
	rename -uid "6AAF5DD6-1B47-A379-F4FD-6AAA5A83D1F5";
	setAttr ".t" -type "double3" -2.3714455201612781 1.8266508812773823 0 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.27304227071525594 0.039714048413513933 ;
createNode transform -n "transform19" -p "pCone1";
	rename -uid "E3FB64F7-9845-0E38-2DE5-68AC6DDBD474";
	setAttr ".v" no;
createNode mesh -n "pConeShape1" -p "transform19";
	rename -uid "3C1C3FC5-E241-85D7-EA3D-029431AC7527";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCone2";
	rename -uid "04E4617C-F14B-0FD2-D53C-16A37142970D";
	setAttr ".t" -type "double3" -2.3714455201612781 1.7498152913864924 2.200403580632762 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform18" -p "pCone2";
	rename -uid "D27F867F-9540-CAA0-88FE-06939CB3FEB3";
	setAttr ".v" no;
createNode mesh -n "pConeShape2" -p "transform18";
	rename -uid "AE3AA7FC-E341-826D-957A-4186E04937FB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone3";
	rename -uid "6A35F4C3-D849-3C5E-EB69-31A43E094B3F";
	setAttr ".t" -type "double3" -2.3714455201612781 1.8266508812773823 1.9870933760690908 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.27304227071525594 0.039714048413513933 ;
createNode transform -n "transform17" -p "pCone3";
	rename -uid "0B1314A5-E243-5404-FD9A-6CB1BD1E5ED7";
	setAttr ".v" no;
createNode mesh -n "pConeShape3" -p "transform17";
	rename -uid "CE2327DC-D440-3204-2F3E-F38402097410";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone4";
	rename -uid "027EE535-4745-68C1-FF61-B6A1BF684F27";
	setAttr ".t" -type "double3" -2.3714455201612781 1.8266508812773823 -2.4034994894432855 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.27304227071525594 0.039714048413513933 ;
createNode transform -n "transform16" -p "pCone4";
	rename -uid "0E887C80-254A-D1F0-D622-DA89325DABFA";
	setAttr ".v" no;
createNode mesh -n "pConeShape4" -p "transform16";
	rename -uid "5BC700E6-F54E-AD86-659E-63A1B339A062";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone5";
	rename -uid "55F88608-D346-3CE9-C764-DC814A0E46F9";
	setAttr ".t" -type "double3" -2.3714455201612781 1.7498152913864924 0.15413615188875474 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform15" -p "pCone5";
	rename -uid "823C49C8-D14E-772F-FE6D-36BEFD04EC56";
	setAttr ".v" no;
createNode mesh -n "pConeShape5" -p "transform15";
	rename -uid "111778A3-174A-67D5-1C4B-E0B42B0E8FFD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone6";
	rename -uid "F1D43B8D-D349-6384-5069-CAA9C7E14ED9";
	setAttr ".t" -type "double3" -2.3714455201612781 1.7498152913864924 -2.2118500917383068 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform14" -p "pCone6";
	rename -uid "50F6D1EF-9340-EA06-7B81-3BB40F2F7767";
	setAttr ".v" no;
createNode mesh -n "pConeShape6" -p "transform14";
	rename -uid "E5D81411-594B-4A09-B927-128FE85F2042";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone7";
	rename -uid "DEF4054C-5E4E-F47A-2F15-CEA0380FA5D3";
	setAttr ".t" -type "double3" -0.14561835129066436 4.9491956319182053 1.0898085061926635 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform13" -p "pCone7";
	rename -uid "B61C1F05-004C-78AE-49D1-A296E6AA2DFE";
	setAttr ".v" no;
createNode mesh -n "pConeShape7" -p "transform13";
	rename -uid "9A565B0D-924E-E6AD-A4C9-B483BA1AD1AD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone8";
	rename -uid "21EC0B83-C348-4AA0-821B-03B6A2E8D0D1";
	setAttr ".t" -type "double3" -0.14561835129066436 4.9864163538214727 0.88745969058749652 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.17683950155837427 0.039714048413513933 ;
createNode transform -n "transform12" -p "pCone8";
	rename -uid "65276D06-E649-902D-9CF8-C3A6D0A628A3";
	setAttr ".v" no;
createNode mesh -n "pConeShape8" -p "transform12";
	rename -uid "49042670-AE4B-60E6-41B4-FE8A8CF87D1A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone9";
	rename -uid "AE6B4D7B-0446-FC06-7EFA-F19682816846";
	setAttr ".t" -type "double3" -0.14561835129066436 4.9864163538214727 0.32804811437439541 ;
	setAttr ".r" -type "double3" 180 90 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.17683950155837427 0.039714048413513933 ;
createNode transform -n "transform11" -p "pCone9";
	rename -uid "4D12EA03-CE47-F39E-F2C2-0999C3CF7B23";
	setAttr ".v" no;
createNode mesh -n "pConeShape9" -p "transform11";
	rename -uid "E1E0A7A4-4F46-A350-D90C-F08DC7AE892C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone10";
	rename -uid "8B9C1D23-DE49-91C1-D5CD-4C9A6B835CD3";
	setAttr ".t" -type "double3" 0.67320287215818375 4.9491956319182053 1.3398100726723805 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform10" -p "pCone10";
	rename -uid "30D3B5C9-2A40-BCC4-F231-2FAB0D60BD02";
	setAttr ".v" no;
createNode mesh -n "pConeShape10" -p "transform10";
	rename -uid "370BAACE-3845-53D9-7F70-119D67D36452";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone11";
	rename -uid "1A29F0F9-374D-0095-9540-39B861404A2E";
	setAttr ".t" -type "double3" 0.26483739175423981 4.9491956319182053 1.3398100726723805 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform9" -p "pCone11";
	rename -uid "F5A8C0DE-C042-081D-1113-819ABFC71087";
	setAttr ".v" no;
createNode mesh -n "pConeShape11" -p "transform9";
	rename -uid "12EF607A-5544-8C04-EEBF-B795AD97D8AA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone12";
	rename -uid "78EF0279-3D4F-F7BF-79D2-68B2FC6A88F5";
	setAttr ".t" -type "double3" 0.67320287215818375 3.23104315162115 2.1731326121256749 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform8" -p "pCone12";
	rename -uid "EBA889BD-9041-5A66-F79A-79996B1C362B";
	setAttr ".v" no;
createNode mesh -n "pConeShape12" -p "transform8";
	rename -uid "35B48007-4A49-B5D6-1A2D-8EA68F1D0273";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone13";
	rename -uid "955EBB0B-C441-6A97-48CC-4097D04F3D2D";
	setAttr ".t" -type "double3" 1.1942327762273663 2.6170202944292118 2.1731326121256749 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform7" -p "pCone13";
	rename -uid "5B2D5A21-AD4F-FE00-A49E-D1B98E9236A5";
	setAttr ".v" no;
createNode mesh -n "pConeShape13" -p "transform7";
	rename -uid "0E2729A9-764D-FC67-8444-BB917033A482";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCone14";
	rename -uid "34B2C723-B640-4076-DFC9-2BBEF7AB0B76";
	setAttr ".t" -type "double3" 1.5893105471016096 2.0281361944870273 2.1731326121256749 ;
	setAttr ".r" -type "double3" 180 0 0 ;
	setAttr ".s" -type "double3" 0.140814286978859 0.38478193600705696 0.039714048413513933 ;
createNode transform -n "transform6" -p "pCone14";
	rename -uid "80BE441B-1C48-8BE2-D523-7CBAB2CB17D9";
	setAttr ".v" no;
createNode mesh -n "pConeShape14" -p "transform6";
	rename -uid "AB2CD285-5B47-15E4-9C1C-958404A85586";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:6]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.62500006 0.033493668
		 0.37500003 0.033493623 0.25 0.24999997 0.37499997 0.46650636 0.625 0.46650636 0.75
		 0.25 0.25 0.5 0.33333334 0.5 0.41666669 0.5 0.5 0.5 0.58333331 0.5 0.66666663 0.5
		 0.74999994 0.5 0.5 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".vt[0:6]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0 1 0;
	setAttr -s 12 ".ed[0:11]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 0 6 0
		 1 6 0 2 6 0 3 6 0 4 6 0 5 6 0;
	setAttr -s 7 -ch 24 ".fc[0:6]" -type "polyFaces" 
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 3 0 7 -7
		mu 0 3 6 7 13
		f 3 1 8 -8
		mu 0 3 7 8 13
		f 3 2 9 -9
		mu 0 3 8 9 13
		f 3 3 10 -10
		mu 0 3 9 10 13
		f 3 4 11 -11
		mu 0 3 10 11 13
		f 3 5 6 -12
		mu 0 3 11 12 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8";
	rename -uid "8BE0FB9D-0F40-01BD-365A-CD9BCCBD9224";
	setAttr ".t" -type "double3" 0 0.33750337445418022 1.9551337289128292 ;
	setAttr ".s" -type "double3" 1 0.88808620463670462 0.065725549712966846 ;
createNode transform -n "transform5" -p "pCube8";
	rename -uid "61E86C23-4E4B-C975-6D8E-E795453B0C50";
	setAttr ".v" no;
createNode mesh -n "pCubeShape8" -p "transform5";
	rename -uid "928250F1-0C47-C634-E245-699A0888BEF1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube9";
	rename -uid "A9D6E225-7B4D-D4F0-7AE7-43894E697AD7";
	setAttr ".t" -type "double3" 0 1.1914341784315359 1.9467217197950824 ;
	setAttr ".s" -type "double3" 1 1.2511617677574083 0.024315359930148554 ;
createNode transform -n "transform4" -p "pCube9";
	rename -uid "C40101A8-3240-1E85-44D0-0892CE6F9BC7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape9" -p "transform4";
	rename -uid "94FAEDB4-6A44-EF6B-1713-208F2734C7CC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "15677122-C047-E78E-3A7C-93BC140C365C";
	setAttr ".t" -type "double3" 0 1.2512685316593595 1.9512759667876569 ;
	setAttr ".s" -type "double3" 0.044426656590290457 1.0459929684992317 0.065725549712966846 ;
createNode transform -n "transform3" -p "pCube10";
	rename -uid "245D8ADA-E249-22D3-6136-0D8C973070DF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape10" -p "transform3";
	rename -uid "A25D74F0-2B4E-1861-C029-9EAC83CA7D81";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11";
	rename -uid "D930E7DA-B640-BC35-5746-1DAA894F3957";
	setAttr ".t" -type "double3" 0 1.2512685316593595 1.9512759667876569 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.044426656590290457 1.0459929684992317 0.065725549712966846 ;
createNode transform -n "transform2" -p "pCube11";
	rename -uid "54345D1D-8548-FD81-8AE8-6FB50F3DE2FB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape11" -p "transform2";
	rename -uid "CC3985A7-F148-5650-F7C1-A9B9DF36160A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube12";
	rename -uid "46E09EDD-0641-F4B7-C94C-4EA9E1C1C0F1";
	setAttr ".t" -type "double3" -2.0476294080066069 1.1914341784315359 -0.3928382513547497 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1.6018303309505793 1.2511617677574083 0.024315359930148554 ;
createNode transform -n "transform1" -p "pCube12";
	rename -uid "84EAB8E1-C743-813D-161D-AEBF2947A337";
	setAttr ".v" no;
createNode mesh -n "pCubeShape12" -p "transform1";
	rename -uid "483D0F41-9B46-8B87-C6F0-04B79AC1B2DB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube13";
	rename -uid "EE26770B-B145-4BFD-E9DD-459040E3A5DD";
createNode mesh -n "pCube13Shape" -p "pCube13";
	rename -uid "E3AD3044-3341-06DC-1093-4D89E92502F3";
	setAttr -k off ".v";
	setAttr -s 16 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "4DA79A75-284E-3EEA-3B25-7BB56EADB663";
	setAttr -s 11 ".lnk";
	setAttr -s 11 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "D468A3A2-C44E-7980-A154-EDA35C992A0C";
createNode displayLayer -n "defaultLayer";
	rename -uid "B3F835C4-D346-E4E9-299F-F687CCC27B61";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "7458EB10-5D42-5CC0-24FA-868949DF785B";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "E515723F-D34E-8CD5-44E2-5686AB3641B8";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "BCAE78C6-1141-C113-1A97-60897EBCD486";
	setAttr ".cuv" 1;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "C6C3770D-8945-D31D-8FEA-7D9CEB7CEEBF";
	setAttr ".cuv" 3;
createNode animCurveTL -n "pCylinder1_translateX";
	rename -uid "F10D5B16-AB47-7E15-3C4C-2DA76CE9603D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2.0971386141191664;
createNode animCurveTL -n "pCylinder1_translateY";
	rename -uid "8B306B94-594D-1CC1-729C-2FB2B490B058";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pCylinder1_translateZ";
	rename -uid "99D96579-AD4E-3093-FBE9-91B179E85F85";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder1_axisX";
	rename -uid "4708D05E-AF46-2581-77F0-E4AE752A34DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder1_axisY";
	rename -uid "3EE05F55-F14A-7B0A-BD56-3DB00C8F3AB7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "polyCylinder1_axisZ";
	rename -uid "737FEE66-B548-7F2C-009B-C1B8EE284970";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder1_radius";
	rename -uid "582F5561-4D4E-4ABD-DAB6-FA81C9906CA7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "polyCylinder1_height";
	rename -uid "99A224C0-ED44-E860-A8D9-C2BB0981236B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2;
createNode animCurveTU -n "polyCylinder1_subdivisionsAxis";
	rename -uid "D7960B49-1B40-896C-9943-7BA9BC4A5315";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 8;
createNode animCurveTU -n "polyCylinder1_subdivisionsHeight";
	rename -uid "7FF40392-4F44-5361-9A6F-92949836DCF8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "polyCylinder1_subdivisionsCaps";
	rename -uid "592F198F-6D48-81B2-D73E-A5A3E3ABDE41";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder1_visibility";
	rename -uid "9C05A323-2B4A-35C9-5632-1691BFAB903D";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pCylinder1_rotateX";
	rename -uid "4604F9C2-8042-5C15-80D7-BA9DF09D610A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 90;
createNode animCurveTA -n "pCylinder1_rotateY";
	rename -uid "50F2FADA-A94D-C5BC-C34B-41B81E9A12ED";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder1_rotateZ";
	rename -uid "9FD4FC4C-D14B-EC4D-3BA4-EEB4930B09D7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTU -n "pCylinder1_scaleX";
	rename -uid "A5CB9F6D-E34F-64B2-CC99-EEA806077C0B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder1_scaleY";
	rename -uid "1EE11A75-8649-F630-1DE6-B998C768DE76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder1_scaleZ";
	rename -uid "7C1EB188-2C43-7810-0036-A6B139DD855D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "990BFF02-E846-E279-DBC1-8DB809EF0B68";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 100\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 100\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 381\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 381\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 381\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 381\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 662\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 662\n            -height 347\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 431\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 431\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 14 50 -ps 2 86 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 100\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 100\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 662\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 662\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 381\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 381\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 381\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 381\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "9CBE9C01-2D4E-7E10-8304-58A0A0626581";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode animCurveTL -n "pCylinder24_translateX";
	rename -uid "8165376B-B94B-9581-D473-5197CFCB9CBE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 -0.0016359013754350116;
createNode animCurveTL -n "pCylinder24_translateY";
	rename -uid "98421197-CD48-346F-DA60-CE80B440D462";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.8201520601528935;
createNode animCurveTL -n "pCylinder24_translateZ";
	rename -uid "4D2393F3-2545-F075-2B6B-5E94E2E518BC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1.8425826553526738;
createNode animCurveTU -n "pCylinder24_visibility";
	rename -uid "695CCA01-654D-62D7-ED79-338F104F9152";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pCylinder24_rotateX";
	rename -uid "AD865366-BD4F-A0A9-2316-F3BF6B01DEB7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder24_rotateY";
	rename -uid "6E0C531E-0042-2EC9-D1AD-39899AA2AC62";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder24_rotateZ";
	rename -uid "505D6E67-AA44-1532-F2E1-9BB006794876";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 90;
createNode animCurveTU -n "pCylinder24_scaleX";
	rename -uid "43CE89C1-DB4C-AFB4-FA1B-21BED3EBCC89";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.1016342229405259;
createNode animCurveTU -n "pCylinder24_scaleY";
	rename -uid "E082243A-F243-7B96-CB8A-45BF6478D612";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.5632723519180981;
createNode animCurveTU -n "pCylinder24_scaleZ";
	rename -uid "6C676BF8-BB45-E4B2-E3A3-A8B5F52C289E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0.1016342229405259;
createNode polySubdFace -n "polySubdFace6";
	rename -uid "6D3C1AC3-F140-B7CD-FDA9-E68AD70A4EA9";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode polySubdFace -n "polySubdFace5";
	rename -uid "3C836F5F-DF41-A104-E6AC-B49117E7589F";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace4";
	rename -uid "94CD34B8-7C41-C396-D7AE-4DB44A781967";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyCube -n "polyCube2";
	rename -uid "9BE58FDD-C54B-6B7E-28CB-8B98ED1EEE63";
	setAttr ".cuv" 1;
createNode polySubdFace -n "polySubdFace7";
	rename -uid "87241CD1-F04F-8E32-26E1-7EA1520DC94B";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polySubdFace -n "polySubdFace8";
	rename -uid "061ACD01-AE4A-6C77-020C-CBAFF5C39706";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace9";
	rename -uid "606EBBA0-A14C-BB5C-6C54-DBB2330AB4F8";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode polySmoothFace -n "polySmoothFace1";
	rename -uid "7D7FC232-7C41-D52E-016B-67AF54A0B9B7";
	setAttr ".ics" -type "componentList" 1 "f[0:383]";
	setAttr ".sdt" 2;
	setAttr ".suv" yes;
	setAttr ".ps" 0.10000000149011612;
	setAttr ".ro" 1;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode polySmoothFace -n "polySmoothFace2";
	rename -uid "5A5047BD-D34F-A939-91DE-BAA7744BABE9";
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".sdt" 2;
	setAttr ".suv" yes;
	setAttr ".ps" 0.10000000149011612;
	setAttr ".ro" 1;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode polyCube -n "polyCube3";
	rename -uid "37173EA3-914B-EB02-775C-5CAEF6213A6E";
	setAttr ".cuv" 1;
createNode polyBevel3 -n "polyBevel1";
	rename -uid "9220C727-F241-E78B-4EA1-B385D05AE99A";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 3 "e[0]" "e[3]" "e[10:11]";
	setAttr ".ix" -type "matrix" 1.1237785147662891 0 0 0 0 0.52915813899650588 0 0 0 0 1.1237785147662891 0
		 0.88560715839690918 5.6592967643774204 0 1;
	setAttr ".ws" yes;
	setAttr ".oaf" yes;
	setAttr ".at" 180;
	setAttr ".fn" yes;
	setAttr ".mv" yes;
	setAttr ".mvt" 0.0001;
	setAttr ".sa" 30;
	setAttr ".ma" 180;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "85EC1339-094B-F756-D049-64A87E9CB2C9";
	setAttr ".ics" -type "componentList" 1 "f[7]";
	setAttr ".ix" -type "matrix" 1.1237785147662891 0 0 0 0 0.52915813899650588 0 0 0 0 1.1237785147662891 0
		 0.88560715839690918 5.6592967643774204 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.88560718 5.3947186 0 ;
	setAttr ".rs" 358162387;
	setAttr ".lt" -type "double3" 0 4.9303806576313238e-32 1.0345586243489775 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.45599414607519662 5.3947187041682207 -0.42961307930413173 ;
	setAttr ".cbx" -type "double3" 1.3152202711922505 5.3947187041682207 0.42961307930413173 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "5B3CDEAC-1048-78B3-780F-7AB803293502";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 0.7817866766055056 0 0 0 0 0.36812305756785801 0 0 0 0 0.7817866766055056 0
		 0.29533894431683372 4.9801850934109417 0.83724570369921736 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.29533902 5.1642466 0.8372457 ;
	setAttr ".rs" 760850743;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.095554277490626227 5.1642466221948711 0.44635236539646456 ;
	setAttr ".cbx" -type "double3" 0.68623232921770372 5.1642466221948711 1.2281390420019702 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "1513880C-A04F-A9CF-DCB0-A5AAF87930EE";
	setAttr ".uopa" yes;
	setAttr -s 6 ".tk";
	setAttr ".tk[12]" -type "float3" 0 -1.2394391 0 ;
	setAttr ".tk[13]" -type "float3" 0 -1.2394391 0 ;
	setAttr ".tk[14]" -type "float3" 0 -1.2394391 0 ;
	setAttr ".tk[15]" -type "float3" 0 -1.2394391 0 ;
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "BF39717C-1449-046E-76BE-EEA3DBB634AE";
	setAttr ".ics" -type "componentList" 1 "f[0]";
	setAttr ".ix" -type "matrix" 0.7817866766055056 0 0 0 0 0.36812305756785801 0 0 0 0 0.7817866766055056 0
		 0.29533894431683372 4.9801850934109417 0.83724570369921736 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.29533902 5.1642466 0.8372457 ;
	setAttr ".rs" 978679378;
	setAttr ".lt" -type "double3" 0 4.1959574082554642e-17 0.18896912220281781 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.24961634333474414 5.1642466221948711 0.29229022965517093 ;
	setAttr ".cbx" -type "double3" 0.840294371762763 5.1642466221948711 1.3822011777432639 ;
createNode polyTweak -n "polyTweak2";
	rename -uid "C659AE1C-DD4E-EAFF-903C-E5A3B4416DFC";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[16]" -type "float3" -0.19706409 0 0.19706416 ;
	setAttr ".tk[17]" -type "float3" 0.19706409 0 0.19706416 ;
	setAttr ".tk[18]" -type "float3" 0.19706409 0 -0.19706416 ;
	setAttr ".tk[19]" -type "float3" -0.19706409 0 -0.19706416 ;
createNode polySubdFace -n "polySubdFace10";
	rename -uid "F83AB906-EC41-0C2A-4B67-359B7444CD35";
	setAttr ".ics" -type "componentList" 1 "f[0]";
createNode polySubdFace -n "polySubdFace11";
	rename -uid "FE37007F-1249-1AF6-E522-B18A6310B715";
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[22:24]";
createNode polySubdFace -n "polySubdFace12";
	rename -uid "F3F62493-D742-E66B-6C1A-F6B1677E8188";
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[22:36]";
createNode polySmoothFace -n "polySmoothFace3";
	rename -uid "0F49F4CF-F846-3114-3D4B-6F93CDDE693F";
	setAttr ".ics" -type "componentList" 2 "f[0]" "f[22:84]";
	setAttr ".sdt" 2;
	setAttr ".suv" yes;
	setAttr ".ps" 0.10000000149011612;
	setAttr ".ro" 1;
	setAttr ".ma" yes;
	setAttr ".m08" yes;
createNode lambert -n "house_snow";
	rename -uid "853132C4-2248-DC02-3148-F18B72EEFE24";
createNode shadingEngine -n "lambert2SG";
	rename -uid "1369D01A-2242-7880-7BC5-D5A138CB3F61";
	setAttr ".ihi" 0;
	setAttr -s 6 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 6 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "E248A281-4441-189C-F851-C78AF83348B5";
createNode lambert -n "house_body";
	rename -uid "6DFFEAB7-A04E-B989-3C86-76B1878A8BAD";
	setAttr ".c" -type "float3" 0.21882963 0.1556115 0.10728619 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "0B67B81C-8A43-CAA8-3B43-E095F4AE9C83";
	setAttr ".ihi" 0;
	setAttr -s 108 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 107 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "E0889F9B-C141-F6CC-32B3-1FBE5FF9D924";
createNode lambert -n "house_details_tronk";
	rename -uid "6BC038FF-1D4F-CAFE-9549-55AF17711567";
	setAttr ".c" -type "float3" 0.37396809 0.29898527 0.24135195 ;
createNode shadingEngine -n "lambert4SG";
	rename -uid "CEDA58B3-C64C-9AAD-A4E7-84960F27E34F";
	setAttr ".ihi" 0;
	setAttr -s 65 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 66 ".gn";
createNode materialInfo -n "materialInfo3";
	rename -uid "D128BCE5-7744-CF6A-CBCE-53B35EF11B3E";
createNode lambert -n "house_chimney_snow";
	rename -uid "E09B62F7-D84D-20B9-3495-D192E5A4F85E";
createNode shadingEngine -n "lambert5SG";
	rename -uid "61DFBE4C-C447-135B-DE83-F7A6D2F0921D";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "B5B238F6-E941-01AB-4502-D0A3ECE1A38F";
createNode groupId -n "groupId1";
	rename -uid "EF25D4CC-1441-3E2B-EA1E-80849DFBB605";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "04A4C774-AB48-1D0C-253A-AAA8BE2D252F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[7]" "f[14]" "f[17]";
	setAttr ".irc" -type "componentList" 4 "f[0:6]" "f[8:13]" "f[15:16]" "f[18:276]";
createNode groupId -n "groupId2";
	rename -uid "47FB7047-0F47-24FC-9125-309CE4378A10";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "2CFE2370-5448-2F32-B392-1B993B491509";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "8CD56E6E-B04A-5CC0-4A70-BAA26900562F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[0]" "f[15:16]" "f[18:276]";
createNode groupId -n "groupId4";
	rename -uid "09AA8FB0-BA44-2DFE-F15A-8DBC30458926";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "02D59A40-1449-4761-2F84-4F8D9F419809";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1:6]" "f[8:13]";
createNode groupId -n "groupId6";
	rename -uid "983751F1-614C-1C84-1F8F-E18C5BEE35E1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "19DE6C59-764D-F249-8340-34AA2F07ADB2";
	setAttr ".ihi" 0;
createNode polyPlane -n "polyPlane2";
	rename -uid "7C1EE4AF-644F-75D1-F2A0-3697EBFE1062";
	setAttr ".cuv" 2;
createNode lambert -n "snow_house_floor";
	rename -uid "084A68C6-7C4D-3049-1F08-F5BF3A771AFE";
	setAttr ".c" -type "float3" 0 0 0 ;
createNode shadingEngine -n "lambert6SG";
	rename -uid "525BB918-D04D-1BF8-29DD-29A47ECB3E92";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo5";
	rename -uid "5F1AE9E4-0C49-5482-C9E4-6294649A353F";
createNode polyCone -n "polyCone1";
	rename -uid "F686F2AA-8C4E-6EEA-3745-A1A05F1B6808";
	setAttr ".sa" 6;
	setAttr ".cuv" 3;
createNode polyCube -n "polyCube4";
	rename -uid "DF552C65-A540-26A0-65BE-4C96F048BB77";
	setAttr ".cuv" 1;
createNode lambert -n "house_snow_door_window";
	rename -uid "72F9A5E9-A143-A80D-C2DF-448FEF40CD58";
	setAttr ".c" -type "float3" 0.068400003 0.054699998 0.044199999 ;
createNode shadingEngine -n "lambert7SG";
	rename -uid "D29517F8-BD43-4F30-816E-DE8A522350A7";
	setAttr ".ihi" 0;
	setAttr -s 20 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 19 ".gn";
createNode materialInfo -n "materialInfo6";
	rename -uid "C85410C0-3041-D143-9D7F-0AABFD774193";
createNode lambert -n "snow_door_house";
	rename -uid "F3283374-1D48-56DA-FA24-B8BACD7A60D6";
	setAttr ".c" -type "float3" 0.29159999 0.14569999 0.091499999 ;
createNode shadingEngine -n "lambert8SG";
	rename -uid "CF548A83-8144-A9BA-DD5F-B88F2E0C67C9";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo7";
	rename -uid "0F1BD04A-4D41-474C-3DF0-F2859E3FDCC9";
createNode lambert -n "house_ice";
	rename -uid "7B39C2C0-EB40-A474-A945-E8AEA125DAF5";
createNode shadingEngine -n "lambert9SG";
	rename -uid "0224C9EC-3D40-45AF-8313-099467C1F21D";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo8";
	rename -uid "285FEE6E-6D41-A0C0-648C-9FA0701D118B";
createNode lambert -n "house_glass";
	rename -uid "2FE5DE38-514D-E408-8583-90BAFAE63A05";
	setAttr ".c" -type "float3" 1 0.92984807 0.40700001 ;
createNode shadingEngine -n "lambert10SG";
	rename -uid "DAC49A1B-B64F-99F2-E5A3-869460D32486";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo9";
	rename -uid "91DC6E8C-E341-92B0-A344-D1BFD4E8D7F2";
createNode polyUnite -n "polyUnite1";
	rename -uid "0347AE6F-C04D-F76F-98D1-42992AC4DE81";
	setAttr -s 116 ".ip";
	setAttr -s 116 ".im";
createNode groupId -n "groupId10";
	rename -uid "EACC1431-AE40-AFE0-2D51-6E8AF33D44E2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "17821F7F-624A-7209-A2EB-AD9CBDBAACAD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId11";
	rename -uid "BA8DAC2B-1F43-F602-B64A-E9B869281DB8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "C62D5F35-844A-3BC5-015B-F9932AB78657";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "F84E63CD-1747-2536-9274-E6B14CD740A5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "F58B0011-394C-8AAE-8034-DDAB2B2D5A57";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "BFB5E884-E540-CA98-C44F-EFB9AD55A22F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "0D82973E-6F47-75ED-8085-1894FC7B2CD4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "53BE327D-FB4A-6465-BE16-CD8CBF44845E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "DE824D0D-A74D-6D48-7DE1-A6A5C6386EA2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "6BED1453-DF44-71E5-BCF8-6DAA6C8B9C46";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:23]";
createNode groupId -n "groupId19";
	rename -uid "DF49D5B0-5A44-5526-7D98-A280E2B72CFD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "BB7533BE-F449-5E17-2CF6-C0BA385B806A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "574D50AE-9447-8467-4D42-4ABC5E548ED8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "66E73EEB-7741-BF7D-44C0-4F843F102B87";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "C3DD0F8E-2948-81CB-C95E-52B0953EF903";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "977EB04C-E941-C5F5-7094-FAA8B148012A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "0651D53D-E94D-F405-B01E-B9AC99D53C05";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "AC7BDBD4-C240-7937-5A0B-E1963708AE7D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "8B164835-6442-131F-93AE-47AE56B24716";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "EF6A4EB9-F944-5769-88F0-8689730DE6AB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "D6B412D0-CC44-0AE6-E32C-85B07CD96D58";
	setAttr ".ihi" 0;
createNode groupId -n "groupId30";
	rename -uid "9630C0D7-4540-27CF-0E93-278A97F12422";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "C374F19C-3640-8A9E-8D16-9D804CAA0D94";
	setAttr ".ihi" 0;
createNode groupId -n "groupId32";
	rename -uid "2A9C3389-2842-35BB-C8A6-10A6CB08B8E8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "B1C2C788-664E-7EBA-C516-2F9F520E27DE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId34";
	rename -uid "35426535-444C-D78E-9DB2-86A11AF8ECC8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "20F14474-2D43-9ADF-F98D-97A66F3C63E5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId36";
	rename -uid "305E1D18-FB42-F677-D1A0-F08191E8B292";
	setAttr ".ihi" 0;
createNode groupId -n "groupId37";
	rename -uid "EF9B5555-8F47-B67F-AED4-13880D11A8FD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId38";
	rename -uid "4115628C-E142-EEAC-268E-08BD24B7F40B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId39";
	rename -uid "80AF9008-D84E-7631-05AF-1FAA40AC5CAC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId40";
	rename -uid "83BB9910-AE45-D413-A09D-AFB9C2B833BD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "DA1176E9-FB45-AE70-452D-05BEC5BD9A0A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "A989F37B-2347-8474-197A-6F9271433F2A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "4467A179-6D47-4849-7CBC-089FBDCB7738";
	setAttr ".ihi" 0;
createNode groupId -n "groupId44";
	rename -uid "793526BB-5149-8E22-9FE5-3080E262CE8A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "8F34FF47-E24E-4ACB-A059-47B631F5F00D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId46";
	rename -uid "3F3639EF-3640-45B2-1FB1-8A93666BAF23";
	setAttr ".ihi" 0;
createNode groupId -n "groupId47";
	rename -uid "87D10318-7749-CA22-88A7-D98CADF9C0E8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId48";
	rename -uid "02029D09-BB48-C1BF-3640-83A1A20FB7B8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "E932E6C8-5B4D-A710-47A6-349C7D729841";
	setAttr ".ihi" 0;
createNode groupId -n "groupId50";
	rename -uid "00805372-804C-3320-26B0-0595DC9E6EE9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId51";
	rename -uid "9DC06406-4940-5E5E-06F1-C2A63E4470CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId52";
	rename -uid "9C82364F-3E42-B4FD-2A08-C6BA20A701E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId53";
	rename -uid "5DB29548-2943-A676-D87E-24A85315BEC0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId54";
	rename -uid "E340767F-1342-8859-5FE6-DDAE6FDC77FE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId55";
	rename -uid "CC958107-4D4C-76E8-6633-9CBA62893FA1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId56";
	rename -uid "51E49F22-F340-0528-3585-3DAF992C395D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId57";
	rename -uid "CE945D98-D54F-93E0-0E24-11B329EA0768";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "36618F5B-4047-A294-8E37-528FE25CC1E4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "5DD12D36-9742-F42B-9AD3-34BC82EC94F1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "0817B753-CB40-3A80-2FC5-3ABA439E6D8E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "F18B2F6F-3248-58C2-9B55-0891BB323C76";
	setAttr ".ihi" 0;
createNode groupId -n "groupId62";
	rename -uid "79260BC7-134A-5FD2-DA33-FC9EFD44C458";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "DF498153-D54D-6A1C-3632-03AE315977D3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId64";
	rename -uid "E4A41F02-6A45-B464-4E7D-C693C44758F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId65";
	rename -uid "49B8ADA2-684F-C3D5-BF10-2E86986F580F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId66";
	rename -uid "11B641C9-AB43-B0E2-D094-F78484AAB988";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "6791C37D-DD4B-AAA9-A9C1-E8AFFEE69A26";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "3A5ED430-A548-9CAC-B1ED-C8B013021655";
	setAttr ".ihi" 0;
createNode groupId -n "groupId69";
	rename -uid "1F963857-594C-DBAC-CBC1-178E9F46BB92";
	setAttr ".ihi" 0;
createNode groupId -n "groupId70";
	rename -uid "25A2F8A0-8B46-3EC1-C8EB-EE8567BC1777";
	setAttr ".ihi" 0;
createNode groupId -n "groupId71";
	rename -uid "28F386BC-4E4D-4465-5180-9DAD9A285F94";
	setAttr ".ihi" 0;
createNode groupId -n "groupId72";
	rename -uid "8405CCBB-EC4B-0A27-EDD0-28BE1DE0C0E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId73";
	rename -uid "906B51A0-B44A-93AA-AD32-AEA176C759F2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId74";
	rename -uid "216ACD5A-C74A-F406-580C-228DEDA286AD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId75";
	rename -uid "95A875DD-C540-F030-88FA-17878424B205";
	setAttr ".ihi" 0;
createNode groupId -n "groupId76";
	rename -uid "4C9C39BB-004C-6ACC-4641-E7A3BFD5950B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId77";
	rename -uid "55D5BA32-9F4F-90D0-4CFD-28A7DDAD4925";
	setAttr ".ihi" 0;
createNode groupId -n "groupId78";
	rename -uid "D4814EDE-1D49-D12E-4A6D-148815A8D75F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId79";
	rename -uid "584A75F2-DE4D-EEF6-E8F8-5BB19A27293E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId80";
	rename -uid "54BF8149-E94A-15AB-FB1A-D0B506E89A13";
	setAttr ".ihi" 0;
createNode groupId -n "groupId81";
	rename -uid "0412D8D5-0045-8B24-6B98-A89B32E9E888";
	setAttr ".ihi" 0;
createNode groupId -n "groupId82";
	rename -uid "5FBC608E-E94C-107D-DB5C-9986003EB545";
	setAttr ".ihi" 0;
createNode groupId -n "groupId83";
	rename -uid "B2B5F086-5049-F7BC-7C5D-87BE97B9EF6C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId84";
	rename -uid "F094FAEB-8440-46EF-ED9A-0F84C83579EF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId85";
	rename -uid "A14B2006-0C4E-4D14-CAD8-088443ECAC98";
	setAttr ".ihi" 0;
createNode groupId -n "groupId86";
	rename -uid "D0D48E7A-DD4F-DD9A-25D5-63A7CF7F5312";
	setAttr ".ihi" 0;
createNode groupId -n "groupId87";
	rename -uid "52C57F52-CC41-8D40-E92C-B29A4675CAEC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId88";
	rename -uid "BD55B696-FF4E-F929-EEEE-AAB9BA4D3AB3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId89";
	rename -uid "9F5E77B0-974D-A207-327A-E19AB423B4EB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId90";
	rename -uid "A569B1D8-C348-5864-DC8F-B298B54284CE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId91";
	rename -uid "2B49FBE7-A848-D2D4-EF56-42BF410E6559";
	setAttr ".ihi" 0;
createNode groupId -n "groupId92";
	rename -uid "10A04D4A-8048-D237-F647-348F689E0F34";
	setAttr ".ihi" 0;
createNode groupId -n "groupId93";
	rename -uid "08C6E7ED-D946-6C3E-DC81-1F8D8B69A912";
	setAttr ".ihi" 0;
createNode groupId -n "groupId94";
	rename -uid "707FBCE6-E642-2920-6C31-FE929F0C6CE5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId95";
	rename -uid "3C87BC31-3B49-7C81-16F7-6E9EDF381090";
	setAttr ".ihi" 0;
createNode groupId -n "groupId96";
	rename -uid "030927BC-DF4A-E306-4E1C-87ACF581D78A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId97";
	rename -uid "6420CD05-BB43-73E5-0575-0DA7343495B7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId98";
	rename -uid "7F27EC9F-5D4B-1249-8FBB-AF82B38FE533";
	setAttr ".ihi" 0;
createNode groupId -n "groupId99";
	rename -uid "68833D2B-564E-84EA-92DB-13B95D6E7476";
	setAttr ".ihi" 0;
createNode groupId -n "groupId100";
	rename -uid "25755748-1D49-4B8D-1F0A-14A23213E0A2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId101";
	rename -uid "E4C0966C-C142-26B5-EFEA-9B8734BDD99C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId102";
	rename -uid "53BA802C-6F4B-BACE-5217-8DA890CEDF87";
	setAttr ".ihi" 0;
createNode groupId -n "groupId103";
	rename -uid "3EADCAAB-CC45-F015-3262-FAA4AF816526";
	setAttr ".ihi" 0;
createNode groupId -n "groupId104";
	rename -uid "1C2D17B1-2148-4617-A22C-5CBD2BB0336D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId105";
	rename -uid "1FEDC7E4-7446-38A8-630A-37B9A984C3E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId106";
	rename -uid "75B2B8A4-4348-FB98-27F8-F0A5CEDEF183";
	setAttr ".ihi" 0;
createNode groupId -n "groupId107";
	rename -uid "A6136511-BB45-AF4D-D44A-B7BED4424F73";
	setAttr ".ihi" 0;
createNode groupId -n "groupId108";
	rename -uid "DB161DDB-0F42-6060-A874-2EB0DC5E8E4B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId109";
	rename -uid "8AC458FE-B34D-78C7-DF4E-F6AF9D6A5E1C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId110";
	rename -uid "DDE25606-CD43-A189-8555-FA900CA5EF6E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId111";
	rename -uid "2D377A9D-1541-D199-9BA3-3FAC4F2E94D0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId112";
	rename -uid "6DF64985-984C-72C4-8A9E-3EA0E8CCAF53";
	setAttr ".ihi" 0;
createNode groupId -n "groupId113";
	rename -uid "05D598D3-6A4F-7D1A-5A8F-62BFF3D7C124";
	setAttr ".ihi" 0;
createNode groupId -n "groupId114";
	rename -uid "F89DEC89-994A-D9D6-9F6F-D2A15DF81332";
	setAttr ".ihi" 0;
createNode groupId -n "groupId115";
	rename -uid "8E82A33D-FA4A-8732-5ED3-E5B5359D9442";
	setAttr ".ihi" 0;
createNode groupId -n "groupId116";
	rename -uid "73953EA5-8E45-A580-A7D5-DC84F128F7E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId117";
	rename -uid "9AE6284A-3743-0EEC-5C89-FBAC8D5DDCEC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId118";
	rename -uid "30DC6C21-6B43-B894-FBCA-1DA93A7446A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId119";
	rename -uid "AA7A7554-6449-97EA-32EB-A4A8B338B94F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId120";
	rename -uid "3E58EE63-3244-9583-3C6A-50992A2218DA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId121";
	rename -uid "8ACE6984-0247-3E27-BB0A-C894F91670E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId122";
	rename -uid "D402A85F-624A-E702-A245-A3A680891657";
	setAttr ".ihi" 0;
createNode groupId -n "groupId123";
	rename -uid "E29D2CB2-2B4C-4B8D-9D2E-338CE60361B9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId124";
	rename -uid "741CC5E3-8740-29F7-073A-FE8A2A6D261F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId125";
	rename -uid "560062A0-3C4C-2389-0705-9B81CFD50C4B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId126";
	rename -uid "B78967C8-DA4E-5ED3-B632-5892171AF019";
	setAttr ".ihi" 0;
createNode groupId -n "groupId127";
	rename -uid "671698DB-D647-EF5F-C106-CFB363D49C73";
	setAttr ".ihi" 0;
createNode groupId -n "groupId128";
	rename -uid "4EE6F68B-3642-2609-0ABB-54A4A55CD4E0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId129";
	rename -uid "AB2393A5-E748-130F-7460-45AFEC177FF3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId130";
	rename -uid "D8BFAEA3-3F40-7683-0F13-CCA71F0A72CB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId131";
	rename -uid "E18195A1-024B-FAFD-4B43-748598F47BB9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId132";
	rename -uid "DC9697A3-9A47-9693-EB8F-BFAFA8CFF5E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId133";
	rename -uid "A8F433FA-3343-FFE4-93B2-F48880F1F07D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId134";
	rename -uid "D4825B29-0F46-451E-B99A-E298299C0651";
	setAttr ".ihi" 0;
createNode groupId -n "groupId135";
	rename -uid "A94D6007-074E-6445-3C54-00B1C841D330";
	setAttr ".ihi" 0;
createNode groupId -n "groupId136";
	rename -uid "6AE2461F-8347-03EB-9D08-79AC1A3B3579";
	setAttr ".ihi" 0;
createNode groupId -n "groupId137";
	rename -uid "01DE331F-194D-B487-A27A-CB8D8C60E3A7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId138";
	rename -uid "D3A677D3-8449-370D-DFFD-DB82878FC969";
	setAttr ".ihi" 0;
createNode groupId -n "groupId139";
	rename -uid "0DB64C14-5B41-EC56-C830-148E20FBF529";
	setAttr ".ihi" 0;
createNode groupId -n "groupId140";
	rename -uid "0E1811E0-8640-85F1-F7BE-A0B016CB9D1B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId141";
	rename -uid "60E7DEEC-C942-0356-93FB-6AB63AA7E71D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId142";
	rename -uid "A808FBCB-4F46-151B-F836-28A1DFE0893D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId143";
	rename -uid "A07336CB-B14A-3693-FAFF-0EA0EDD7EE53";
	setAttr ".ihi" 0;
createNode groupId -n "groupId144";
	rename -uid "B0D3DCB4-9F40-7DD3-3EFA-119D50FDFDD3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId145";
	rename -uid "8503D57A-F844-FE9E-9FE6-989D43EF6B72";
	setAttr ".ihi" 0;
createNode groupId -n "groupId146";
	rename -uid "CDD6878D-8D4C-3B00-EE2E-D8B1339AEEDF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId147";
	rename -uid "E2E93134-904A-319E-3BB0-C2B7597EBFEB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId148";
	rename -uid "7059B7D0-6D43-DBA6-980E-7EA6B57C3CF2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId149";
	rename -uid "5CE2981C-2045-68BE-248A-FC8B5A2347A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId150";
	rename -uid "6EE670FC-EC40-7025-442A-71A37041803E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "A23AF93C-E947-6E53-28C5-9EA2063E5FBE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1535]";
createNode groupId -n "groupId151";
	rename -uid "18D51E63-EE4A-0BD9-1C84-D3B8021F7A55";
	setAttr ".ihi" 0;
createNode groupId -n "groupId152";
	rename -uid "6F7495DF-E145-68C9-EC65-F68308451753";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "03A677E8-4048-8FD8-B37B-F294E57CB5C3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1535]";
createNode groupId -n "groupId153";
	rename -uid "409B4614-2B4A-C496-1545-21BE809C001A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId154";
	rename -uid "79A856DA-A940-BB62-2785-308242EF4B93";
	setAttr ".ihi" 0;
createNode groupId -n "groupId155";
	rename -uid "1AB7BBE6-0448-58A4-8FBE-9A96A9B102A7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId156";
	rename -uid "BFD38F6E-144F-8CF2-817B-879F36A27CC2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId157";
	rename -uid "86BB1B17-5346-758A-5890-49922922598F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId158";
	rename -uid "5D798AA6-C04C-C9E6-CF22-7D8179EC9140";
	setAttr ".ihi" 0;
createNode groupId -n "groupId159";
	rename -uid "BDF32D8E-7945-CB35-A6D0-3283BFF4CD32";
	setAttr ".ihi" 0;
createNode groupId -n "groupId160";
	rename -uid "2B9F2C9D-C045-FD91-BA52-ED8C152BDD17";
	setAttr ".ihi" 0;
createNode groupId -n "groupId161";
	rename -uid "7AEAD2D8-6B4C-B651-FDC0-4087595088C3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId162";
	rename -uid "58AC1879-FB4E-F48C-4296-DBBA1C7B9DC5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId163";
	rename -uid "A94D04E8-2145-4ED0-6A60-3CB4BA87EF6A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId164";
	rename -uid "30440BDE-3C43-8380-552E-0D86D6E89B86";
	setAttr ".ihi" 0;
createNode groupId -n "groupId165";
	rename -uid "567B5003-8F4A-287A-E586-AA82F62B140A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId166";
	rename -uid "E5C5CC9A-DE4B-0823-87EA-E0B47F87BC2E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId167";
	rename -uid "0DFABB3C-744F-493D-19E4-B09582736441";
	setAttr ".ihi" 0;
createNode groupId -n "groupId168";
	rename -uid "FFBB32D1-314E-AB9A-1221-63A579EB3E3E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId169";
	rename -uid "E2379E36-3A4F-D697-A9C3-7CAB06E87E16";
	setAttr ".ihi" 0;
createNode groupId -n "groupId170";
	rename -uid "9A2F9303-064F-C010-3837-76AFB787C2BF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId171";
	rename -uid "4C5E051A-5144-B230-0099-A09B5F3BB08B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId172";
	rename -uid "736B802D-C34C-2210-23A6-20BBCB4392FC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId173";
	rename -uid "C64C4CAB-7841-7FDD-F505-ABB96EFF1B25";
	setAttr ".ihi" 0;
createNode groupId -n "groupId174";
	rename -uid "8D820FAD-A346-5830-2C3A-B68363F771A8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId175";
	rename -uid "8CD2ED99-DF41-269D-6F1D-AAA7D24D6870";
	setAttr ".ihi" 0;
createNode groupId -n "groupId176";
	rename -uid "C973C2B8-DB40-8F60-9BA1-F0B785FC7A51";
	setAttr ".ihi" 0;
createNode groupId -n "groupId177";
	rename -uid "6669849A-664F-3631-3497-0BB67F89B816";
	setAttr ".ihi" 0;
createNode groupId -n "groupId178";
	rename -uid "3A7438CE-2444-F4D8-A9F7-20AAEDAD9C57";
	setAttr ".ihi" 0;
createNode groupId -n "groupId179";
	rename -uid "FDA29AF8-7545-38B0-A54A-3EA9FF526757";
	setAttr ".ihi" 0;
createNode groupId -n "groupId180";
	rename -uid "CFE9935C-624A-81C4-3674-558256011E00";
	setAttr ".ihi" 0;
createNode groupId -n "groupId181";
	rename -uid "0C231C15-2A4A-4E04-172D-FA9DC18277CD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId182";
	rename -uid "ED76CD52-7640-DE83-1A6E-5A8D7FE08041";
	setAttr ".ihi" 0;
createNode groupId -n "groupId183";
	rename -uid "A795E2F7-8A49-0EC1-BD14-BCA7743166F1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId184";
	rename -uid "9A5FE1B8-D74E-C238-2128-67824AF2F715";
	setAttr ".ihi" 0;
createNode groupId -n "groupId185";
	rename -uid "3D838041-8544-DECD-BB56-A1AB11F7AF4F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId186";
	rename -uid "09999B26-D248-83A5-A469-0EB61E80BB11";
	setAttr ".ihi" 0;
createNode groupId -n "groupId187";
	rename -uid "4FEBCA33-BE41-EA2C-83C8-0387AEBBE811";
	setAttr ".ihi" 0;
createNode groupId -n "groupId188";
	rename -uid "5872D5E7-574D-2828-754A-3CB2B3673F97";
	setAttr ".ihi" 0;
createNode groupId -n "groupId189";
	rename -uid "51D926DA-E44D-2BB3-7D88-1AAD05591104";
	setAttr ".ihi" 0;
createNode groupId -n "groupId190";
	rename -uid "66AB5E02-2441-2131-D25F-08A95607F9EF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId191";
	rename -uid "7984AE30-5B47-0AC8-1041-E98E28505779";
	setAttr ".ihi" 0;
createNode groupId -n "groupId192";
	rename -uid "169DBC1F-BB44-AA2C-4EC3-C081E39CF38D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId193";
	rename -uid "3D97FF6E-7C4E-E83F-29F8-CAAC60C23B33";
	setAttr ".ihi" 0;
createNode groupId -n "groupId194";
	rename -uid "395BDFEA-6E48-F63B-4DC8-748E7F6BB3D1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId195";
	rename -uid "7117196C-E849-BBC7-99EF-EDAEAE231924";
	setAttr ".ihi" 0;
createNode groupId -n "groupId196";
	rename -uid "D9EA7B05-9649-F06D-13F7-F5B8171232AE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId197";
	rename -uid "E4A28AA0-5748-03FD-428A-B3864ACAB0A7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId198";
	rename -uid "3B6AC029-6446-537B-A824-51957387E1D6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId199";
	rename -uid "CACF5E42-2B43-B92D-56B0-C4B60D475A72";
	setAttr ".ihi" 0;
createNode groupId -n "groupId200";
	rename -uid "13027D04-C349-2D76-29FF-CD9D34FDFC4A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "1741CB21-3741-8C05-FC3F-B4A47C06727D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:99]";
createNode groupId -n "groupId201";
	rename -uid "C9755DAC-ED4D-C8F6-3199-1FBEA2AD2F56";
	setAttr ".ihi" 0;
createNode groupId -n "groupId202";
	rename -uid "7FF131BA-8E42-75D0-88DB-6C945E27B06A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "2E93E978-EB47-47A0-7D5A-7E8A8C2A3E0D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:6]";
createNode groupId -n "groupId203";
	rename -uid "37EA75B6-7C47-4672-EE36-1EAF828F1391";
	setAttr ".ihi" 0;
createNode groupId -n "groupId204";
	rename -uid "E440C4FB-6F41-D2E8-5AEE-E48CE11F19BC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId205";
	rename -uid "96E9AACC-AB46-02F9-EE80-45AFDAD20DEE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId206";
	rename -uid "4FBE814E-F947-513C-0C4B-7AA7561E0C7D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId207";
	rename -uid "666A6231-0347-7361-DC91-94A13811608D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId208";
	rename -uid "D70805C4-0A46-FEBC-2727-10BAAF02FCC4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId209";
	rename -uid "1406F321-D744-BD16-BD84-92A5D49776EA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId210";
	rename -uid "60641D6A-9246-5950-85E2-4491B62764B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId211";
	rename -uid "77E88397-0248-A60C-A7B1-7D8E207C616D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId212";
	rename -uid "556F92B0-EE4B-307A-10C8-EF8BC60C1EC0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId213";
	rename -uid "2ACC7AB7-BB42-F427-38C7-6585342037E8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId214";
	rename -uid "CE4A6A77-4842-8DF1-8C80-42A027327EDF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId215";
	rename -uid "D36D47B0-2A4E-2110-6872-FC81F366667F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId216";
	rename -uid "BD08B67B-DC43-ACF4-F1CE-6C982D0355E8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId217";
	rename -uid "313CECCF-4745-7D22-CB02-0FBCC2B68120";
	setAttr ".ihi" 0;
createNode groupId -n "groupId218";
	rename -uid "3833F9D6-7045-39A0-23FA-B3912FE9784C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId219";
	rename -uid "C8D461B8-7D4A-D7A2-057F-1795498F254B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId220";
	rename -uid "DE276E1C-BC4D-125F-6923-EC95B47F33AC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId221";
	rename -uid "1E8AAE5F-814C-FFA4-2849-5DAF08D65B83";
	setAttr ".ihi" 0;
createNode groupId -n "groupId222";
	rename -uid "109AA072-EC44-F2A6-3CEE-1787AF56A043";
	setAttr ".ihi" 0;
createNode groupId -n "groupId223";
	rename -uid "D463164D-5149-2A73-7219-C0A4671BFBCC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId224";
	rename -uid "E1E1EE5F-D143-95F1-470D-6E91F10EDD3D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId225";
	rename -uid "0074629A-8B4B-FB86-5C0E-1883ED375E68";
	setAttr ".ihi" 0;
createNode groupId -n "groupId226";
	rename -uid "9B3A7A44-8745-29BC-9924-C58769EC3BCC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId227";
	rename -uid "2A1587F7-034A-2A06-616E-318681C48C5A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId228";
	rename -uid "B3507D5F-884F-4CAC-FE1B-F39ECCEF54F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId229";
	rename -uid "8DFB2A5D-6D47-0861-44C9-478DE4BAA2AF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId230";
	rename -uid "CD35AE49-994A-62F1-1F56-96A3B53B22CA";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "CF5BF735-504D-136D-7F12-87BB03A87CA3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId231";
	rename -uid "2EB00D2A-7D49-8794-208E-AFA68110DB21";
	setAttr ".ihi" 0;
createNode groupId -n "groupId232";
	rename -uid "673330E5-4548-1632-F4B6-D8AAA69FE584";
	setAttr ".ihi" 0;
createNode groupId -n "groupId233";
	rename -uid "83EA3CC3-8546-421E-551D-FAB620614CE1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId234";
	rename -uid "15BB2E89-864D-E17C-7F2A-33B4FD71A1C5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId235";
	rename -uid "6FBF14DC-EF43-FAA7-9F05-50A59008881D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId236";
	rename -uid "6D562E20-614B-77A1-037F-E488504541A3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId237";
	rename -uid "75270302-254D-96B1-FB1C-A38602C3731B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId238";
	rename -uid "41C7211B-D244-6F00-F0EA-248DA48AD849";
	setAttr ".ihi" 0;
createNode groupId -n "groupId239";
	rename -uid "85F4DB9D-1740-352C-1436-808E21F31231";
	setAttr ".ihi" 0;
createNode groupId -n "groupId240";
	rename -uid "2A472DA3-B54E-693F-53C7-9397433585F6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "B82F4A6A-DB41-5DCF-9F80-4D8775AB97DF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 32 "f[0:23]" "f[48:71]" "f[96:119]" "f[144:167]" "f[192:215]" "f[240:263]" "f[288:311]" "f[336:359]" "f[384:407]" "f[432:455]" "f[480:503]" "f[624:647]" "f[672:695]" "f[720:743]" "f[768:791]" "f[816:839]" "f[888:911]" "f[936:959]" "f[984:1007]" "f[1032:1055]" "f[1080:1127]" "f[1152:1175]" "f[1200:1223]" "f[1248:1271]" "f[1320:1367]" "f[1416:1463]" "f[4680:4919]" "f[4968:4991]" "f[5016:5231]" "f[5239]" "f[5246]" "f[5249]";
createNode groupId -n "groupId241";
	rename -uid "C50B5F6A-F845-A0CC-9E1C-AEA582073615";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "F952B8E3-D44F-E323-9D51-95BA10B65E09";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 28 "f[24:47]" "f[72:95]" "f[120:143]" "f[168:191]" "f[216:239]" "f[264:287]" "f[312:335]" "f[360:383]" "f[408:431]" "f[456:479]" "f[504:527]" "f[600:623]" "f[648:671]" "f[696:719]" "f[744:767]" "f[792:815]" "f[840:887]" "f[912:935]" "f[960:983]" "f[1008:1031]" "f[1056:1079]" "f[1128:1151]" "f[1176:1199]" "f[1224:1247]" "f[1272:1319]" "f[1368:1415]" "f[4920:4967]" "f[4992:5015]";
createNode groupId -n "groupId242";
	rename -uid "4A48826B-C746-2745-071A-48B63C1C9C9E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "F2940DF1-8F42-455C-97E8-259B04AAC5A4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[528:599]" "f[1464:1607]" "f[5233:5238]" "f[5240:5245]";
createNode groupId -n "groupId243";
	rename -uid "CA4B3293-7041-F1A3-D0F5-8F9F9B43B738";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "E187D7E5-734E-5C67-8B6E-20A08551354C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 4 "f[1608:4679]" "f[5232]" "f[5247:5248]" "f[5250:5508]";
createNode groupId -n "groupId244";
	rename -uid "5C172DB2-3B49-4C83-D267-549441C6CAE7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "5ED1B67C-A245-F512-221D-2B95AA7E5613";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[5509:5608]";
createNode groupId -n "groupId245";
	rename -uid "D810C81A-5146-F19B-EBCC-1BB13DA5723C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "6702C95C-ED41-0EBE-BCE6-0B92192DEFE4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[5609:5706]";
createNode groupId -n "groupId246";
	rename -uid "D869E7D8-1142-41DB-3BEA-CFBF9E7F513C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "4037EB20-854E-CFA5-8EAC-62BFF43C6133";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[5707:5712]" "f[5719:5730]";
createNode groupId -n "groupId247";
	rename -uid "10399E51-EF4F-D61C-0F6F-D58BAF528DA5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "2DACD6C2-AE4D-7F86-F42D-8DAF8A9B470F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[5713:5718]" "f[5731:5736]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "5FF07F85-0149-118B-A781-B583FFED9147";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -739.88092298072604 -114.88094781599366 ;
	setAttr ".tgi[0].vh" -type "double2" 706.54759097194028 191.07142097893245 ;
	setAttr -s 18 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 262.85714721679688;
	setAttr ".tgi[0].ni[0].y" -71.428573608398438;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[1].y" -10;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[2].y" -10;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 582.85711669921875;
	setAttr ".tgi[0].ni[3].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" 1161.4285888671875;
	setAttr ".tgi[0].ni[4].y" -10;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" 1422.857177734375;
	setAttr ".tgi[0].ni[5].y" -71.428573608398438;
	setAttr ".tgi[0].ni[5].nvs" 1923;
	setAttr ".tgi[0].ni[6].x" 262.85714721679688;
	setAttr ".tgi[0].ni[6].y" -58.571430206298828;
	setAttr ".tgi[0].ni[6].nvs" 1923;
	setAttr ".tgi[0].ni[7].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[7].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[7].nvs" 1923;
	setAttr ".tgi[0].ni[8].x" 262.85714721679688;
	setAttr ".tgi[0].ni[8].y" -67.142860412597656;
	setAttr ".tgi[0].ni[8].nvs" 1923;
	setAttr ".tgi[0].ni[9].x" 844.28570556640625;
	setAttr ".tgi[0].ni[9].y" -67.142860412597656;
	setAttr ".tgi[0].ni[9].nvs" 1923;
	setAttr ".tgi[0].ni[10].x" 1161.4285888671875;
	setAttr ".tgi[0].ni[10].y" -10;
	setAttr ".tgi[0].ni[10].nvs" 1923;
	setAttr ".tgi[0].ni[11].x" 1422.857177734375;
	setAttr ".tgi[0].ni[11].y" -71.428573608398438;
	setAttr ".tgi[0].ni[11].nvs" 1923;
	setAttr ".tgi[0].ni[12].x" 2327.142822265625;
	setAttr ".tgi[0].ni[12].y" -10;
	setAttr ".tgi[0].ni[12].nvs" 1923;
	setAttr ".tgi[0].ni[13].x" 2588.571533203125;
	setAttr ".tgi[0].ni[13].y" -71.428573608398438;
	setAttr ".tgi[0].ni[13].nvs" 1923;
	setAttr ".tgi[0].ni[14].x" 2318.571533203125;
	setAttr ".tgi[0].ni[14].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[14].nvs" 1923;
	setAttr ".tgi[0].ni[15].x" 2580;
	setAttr ".tgi[0].ni[15].y" -58.571430206298828;
	setAttr ".tgi[0].ni[15].nvs" 1923;
	setAttr ".tgi[0].ni[16].x" 1161.4285888671875;
	setAttr ".tgi[0].ni[16].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[16].nvs" 1923;
	setAttr ".tgi[0].ni[17].x" 1422.857177734375;
	setAttr ".tgi[0].ni[17].y" -58.571430206298828;
	setAttr ".tgi[0].ni[17].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 11 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 13 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 29 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 29 ".gn";
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId10.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts4.og" "pCubeShape1.i";
connectAttr "groupId11.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId12.id" "pCubeShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId13.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId14.id" "pCubeShape3.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId15.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId16.id" "pCubeShape4.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupId17.id" "pCubeShape4.ciog.cog[0].cgid";
connectAttr "pCylinder1_translateX.o" "pCylinder1.tx";
connectAttr "pCylinder1_translateY.o" "pCylinder1.ty";
connectAttr "pCylinder1_translateZ.o" "pCylinder1.tz";
connectAttr "pCylinder1_visibility.o" "pCylinder1.v";
connectAttr "pCylinder1_rotateX.o" "pCylinder1.rx";
connectAttr "pCylinder1_rotateY.o" "pCylinder1.ry";
connectAttr "pCylinder1_rotateZ.o" "pCylinder1.rz";
connectAttr "pCylinder1_scaleX.o" "pCylinder1.sx";
connectAttr "pCylinder1_scaleY.o" "pCylinder1.sy";
connectAttr "pCylinder1_scaleZ.o" "pCylinder1.sz";
connectAttr "groupId18.id" "pCylinderShape1.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts5.og" "pCylinderShape1.i";
connectAttr "groupId19.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupId20.id" "pCylinderShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "groupId21.id" "pCylinderShape2.ciog.cog[0].cgid";
connectAttr "groupId22.id" "pCylinderShape3.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape3.iog.og[0].gco";
connectAttr "groupId23.id" "pCylinderShape3.ciog.cog[0].cgid";
connectAttr "groupId24.id" "pCylinderShape4.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape4.iog.og[0].gco";
connectAttr "groupId25.id" "pCylinderShape4.ciog.cog[0].cgid";
connectAttr "groupId26.id" "pCylinderShape5.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape5.iog.og[0].gco";
connectAttr "groupId27.id" "pCylinderShape5.ciog.cog[0].cgid";
connectAttr "groupId28.id" "pCylinderShape6.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape6.iog.og[0].gco";
connectAttr "groupId29.id" "pCylinderShape6.ciog.cog[0].cgid";
connectAttr "groupId30.id" "pCylinderShape7.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape7.iog.og[0].gco";
connectAttr "groupId31.id" "pCylinderShape7.ciog.cog[0].cgid";
connectAttr "groupId32.id" "pCylinderShape8.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape8.iog.og[0].gco";
connectAttr "groupId33.id" "pCylinderShape8.ciog.cog[0].cgid";
connectAttr "groupId34.id" "pCylinderShape9.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape9.iog.og[2].gco";
connectAttr "groupId35.id" "pCylinderShape9.ciog.cog[1].cgid";
connectAttr "groupId36.id" "pCylinderShape10.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape10.iog.og[0].gco";
connectAttr "groupId37.id" "pCylinderShape10.ciog.cog[0].cgid";
connectAttr "groupId38.id" "pCylinderShape11.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape11.iog.og[2].gco";
connectAttr "groupId39.id" "pCylinderShape11.ciog.cog[1].cgid";
connectAttr "groupId40.id" "pCylinderShape12.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape12.iog.og[0].gco";
connectAttr "groupId41.id" "pCylinderShape12.ciog.cog[0].cgid";
connectAttr "groupId42.id" "pCylinderShape13.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape13.iog.og[0].gco";
connectAttr "groupId43.id" "pCylinderShape13.ciog.cog[0].cgid";
connectAttr "groupId44.id" "pCylinderShape14.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape14.iog.og[0].gco";
connectAttr "groupId45.id" "pCylinderShape14.ciog.cog[0].cgid";
connectAttr "groupId46.id" "pCylinderShape15.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape15.iog.og[0].gco";
connectAttr "groupId47.id" "pCylinderShape15.ciog.cog[0].cgid";
connectAttr "groupId48.id" "pCylinderShape16.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape16.iog.og[0].gco";
connectAttr "groupId49.id" "pCylinderShape16.ciog.cog[0].cgid";
connectAttr "groupId50.id" "pCylinderShape17.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape17.iog.og[0].gco";
connectAttr "groupId51.id" "pCylinderShape17.ciog.cog[0].cgid";
connectAttr "groupId52.id" "pCylinderShape18.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape18.iog.og[0].gco";
connectAttr "groupId53.id" "pCylinderShape18.ciog.cog[0].cgid";
connectAttr "groupId54.id" "pCylinderShape19.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape19.iog.og[0].gco";
connectAttr "groupId55.id" "pCylinderShape19.ciog.cog[0].cgid";
connectAttr "groupId56.id" "pCylinderShape20.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape20.iog.og[0].gco";
connectAttr "groupId57.id" "pCylinderShape20.ciog.cog[0].cgid";
connectAttr "groupId58.id" "pCylinderShape21.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape21.iog.og[0].gco";
connectAttr "groupId59.id" "pCylinderShape21.ciog.cog[0].cgid";
connectAttr "groupId60.id" "pCylinderShape22.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape22.iog.og[0].gco";
connectAttr "groupId61.id" "pCylinderShape22.ciog.cog[0].cgid";
connectAttr "groupId62.id" "pCylinderShape23.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape23.iog.og[0].gco";
connectAttr "groupId63.id" "pCylinderShape23.ciog.cog[0].cgid";
connectAttr "pCylinder24_translateX.o" "pCylinder24.tx";
connectAttr "pCylinder24_translateY.o" "pCylinder24.ty";
connectAttr "pCylinder24_translateZ.o" "pCylinder24.tz";
connectAttr "pCylinder24_visibility.o" "pCylinder24.v";
connectAttr "pCylinder24_rotateX.o" "pCylinder24.rx";
connectAttr "pCylinder24_rotateY.o" "pCylinder24.ry";
connectAttr "pCylinder24_rotateZ.o" "pCylinder24.rz";
connectAttr "pCylinder24_scaleX.o" "pCylinder24.sx";
connectAttr "pCylinder24_scaleY.o" "pCylinder24.sy";
connectAttr "pCylinder24_scaleZ.o" "pCylinder24.sz";
connectAttr "groupId64.id" "pCylinderShape24.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape24.iog.og[0].gco";
connectAttr "groupId65.id" "pCylinderShape24.ciog.cog[0].cgid";
connectAttr "groupId66.id" "pCylinderShape25.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape25.iog.og[0].gco";
connectAttr "groupId67.id" "pCylinderShape25.ciog.cog[0].cgid";
connectAttr "groupId68.id" "pCylinderShape26.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape26.iog.og[0].gco";
connectAttr "groupId69.id" "pCylinderShape26.ciog.cog[0].cgid";
connectAttr "groupId70.id" "pCylinderShape27.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape27.iog.og[0].gco";
connectAttr "groupId71.id" "pCylinderShape27.ciog.cog[0].cgid";
connectAttr "groupId72.id" "pCylinderShape28.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape28.iog.og[0].gco";
connectAttr "groupId73.id" "pCylinderShape28.ciog.cog[0].cgid";
connectAttr "groupId74.id" "pCylinderShape29.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape29.iog.og[0].gco";
connectAttr "groupId75.id" "pCylinderShape29.ciog.cog[0].cgid";
connectAttr "groupId76.id" "pCylinderShape30.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape30.iog.og[0].gco";
connectAttr "groupId77.id" "pCylinderShape30.ciog.cog[0].cgid";
connectAttr "groupId78.id" "pCylinderShape31.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape31.iog.og[0].gco";
connectAttr "groupId79.id" "pCylinderShape31.ciog.cog[0].cgid";
connectAttr "groupId80.id" "pCylinderShape32.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape32.iog.og[0].gco";
connectAttr "groupId81.id" "pCylinderShape32.ciog.cog[0].cgid";
connectAttr "groupId82.id" "pCylinderShape33.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape33.iog.og[0].gco";
connectAttr "groupId83.id" "pCylinderShape33.ciog.cog[0].cgid";
connectAttr "groupId84.id" "pCylinderShape34.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape34.iog.og[0].gco";
connectAttr "groupId85.id" "pCylinderShape34.ciog.cog[0].cgid";
connectAttr "groupId86.id" "pCylinderShape35.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape35.iog.og[0].gco";
connectAttr "groupId87.id" "pCylinderShape35.ciog.cog[0].cgid";
connectAttr "groupId88.id" "pCylinderShape36.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape36.iog.og[0].gco";
connectAttr "groupId89.id" "pCylinderShape36.ciog.cog[0].cgid";
connectAttr "groupId90.id" "pCylinderShape37.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape37.iog.og[0].gco";
connectAttr "groupId91.id" "pCylinderShape37.ciog.cog[0].cgid";
connectAttr "groupId92.id" "pCylinderShape38.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape38.iog.og[0].gco";
connectAttr "groupId93.id" "pCylinderShape38.ciog.cog[0].cgid";
connectAttr "groupId94.id" "pCylinderShape39.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape39.iog.og[0].gco";
connectAttr "groupId95.id" "pCylinderShape39.ciog.cog[0].cgid";
connectAttr "groupId96.id" "pCylinderShape40.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape40.iog.og[0].gco";
connectAttr "groupId97.id" "pCylinderShape40.ciog.cog[0].cgid";
connectAttr "groupId98.id" "pCylinderShape41.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape41.iog.og[0].gco";
connectAttr "groupId99.id" "pCylinderShape41.ciog.cog[0].cgid";
connectAttr "groupId100.id" "pCylinderShape42.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape42.iog.og[0].gco";
connectAttr "groupId101.id" "pCylinderShape42.ciog.cog[0].cgid";
connectAttr "groupId102.id" "pCylinderShape43.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape43.iog.og[0].gco";
connectAttr "groupId103.id" "pCylinderShape43.ciog.cog[0].cgid";
connectAttr "groupId104.id" "pCylinderShape44.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape44.iog.og[0].gco";
connectAttr "groupId105.id" "pCylinderShape44.ciog.cog[0].cgid";
connectAttr "groupId106.id" "pCylinderShape45.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape45.iog.og[0].gco";
connectAttr "groupId107.id" "pCylinderShape45.ciog.cog[0].cgid";
connectAttr "groupId108.id" "pCylinderShape46.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape46.iog.og[0].gco";
connectAttr "groupId109.id" "pCylinderShape46.ciog.cog[0].cgid";
connectAttr "groupId110.id" "pCylinderShape47.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape47.iog.og[0].gco";
connectAttr "groupId111.id" "pCylinderShape47.ciog.cog[0].cgid";
connectAttr "groupId112.id" "pCylinderShape49.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape49.iog.og[0].gco";
connectAttr "groupId113.id" "pCylinderShape49.ciog.cog[0].cgid";
connectAttr "groupId114.id" "pCylinderShape53.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape53.iog.og[0].gco";
connectAttr "groupId115.id" "pCylinderShape53.ciog.cog[0].cgid";
connectAttr "groupId116.id" "pCylinderShape54.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape54.iog.og[0].gco";
connectAttr "groupId117.id" "pCylinderShape54.ciog.cog[0].cgid";
connectAttr "groupId118.id" "pCylinderShape55.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape55.iog.og[0].gco";
connectAttr "groupId119.id" "pCylinderShape55.ciog.cog[0].cgid";
connectAttr "groupId120.id" "pCylinderShape56.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape56.iog.og[0].gco";
connectAttr "groupId121.id" "pCylinderShape56.ciog.cog[0].cgid";
connectAttr "groupId122.id" "pCylinderShape57.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape57.iog.og[0].gco";
connectAttr "groupId123.id" "pCylinderShape57.ciog.cog[0].cgid";
connectAttr "groupId124.id" "pCylinderShape58.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape58.iog.og[0].gco";
connectAttr "groupId125.id" "pCylinderShape58.ciog.cog[0].cgid";
connectAttr "groupId126.id" "pCylinderShape59.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape59.iog.og[0].gco";
connectAttr "groupId127.id" "pCylinderShape59.ciog.cog[0].cgid";
connectAttr "groupId128.id" "pCylinderShape60.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape60.iog.og[0].gco";
connectAttr "groupId129.id" "pCylinderShape60.ciog.cog[0].cgid";
connectAttr "groupId130.id" "pCylinderShape61.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape61.iog.og[0].gco";
connectAttr "groupId131.id" "pCylinderShape61.ciog.cog[0].cgid";
connectAttr "groupId132.id" "pCylinderShape62.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape62.iog.og[0].gco";
connectAttr "groupId133.id" "pCylinderShape62.ciog.cog[0].cgid";
connectAttr "groupId134.id" "pCylinderShape63.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape63.iog.og[0].gco";
connectAttr "groupId135.id" "pCylinderShape63.ciog.cog[0].cgid";
connectAttr "groupId136.id" "pCylinderShape64.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape64.iog.og[0].gco";
connectAttr "groupId137.id" "pCylinderShape64.ciog.cog[0].cgid";
connectAttr "groupId138.id" "pCylinderShape65.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape65.iog.og[0].gco";
connectAttr "groupId139.id" "pCylinderShape65.ciog.cog[0].cgid";
connectAttr "groupId140.id" "pCylinderShape66.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape66.iog.og[0].gco";
connectAttr "groupId141.id" "pCylinderShape66.ciog.cog[0].cgid";
connectAttr "groupId142.id" "pCylinderShape67.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape67.iog.og[0].gco";
connectAttr "groupId143.id" "pCylinderShape67.ciog.cog[0].cgid";
connectAttr "groupId144.id" "pCylinderShape68.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape68.iog.og[0].gco";
connectAttr "groupId145.id" "pCylinderShape68.ciog.cog[0].cgid";
connectAttr "groupId146.id" "pCylinderShape69.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape69.iog.og[0].gco";
connectAttr "groupId147.id" "pCylinderShape69.ciog.cog[0].cgid";
connectAttr "groupId148.id" "pCylinderShape70.iog.og[0].gid";
connectAttr "lambert7SG.mwc" "pCylinderShape70.iog.og[0].gco";
connectAttr "groupId149.id" "pCylinderShape70.ciog.cog[0].cgid";
connectAttr "groupId150.id" "pCubeShape5.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupParts6.og" "pCubeShape5.i";
connectAttr "groupId151.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "groupId152.id" "pCubeShape6.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape6.iog.og[0].gco";
connectAttr "groupParts7.og" "pCubeShape6.i";
connectAttr "groupId153.id" "pCubeShape6.ciog.cog[0].cgid";
connectAttr "groupId154.id" "pCylinderShape71.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape71.iog.og[0].gco";
connectAttr "groupId155.id" "pCylinderShape71.ciog.cog[0].cgid";
connectAttr "groupId156.id" "pCylinderShape73.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape73.iog.og[0].gco";
connectAttr "groupId157.id" "pCylinderShape73.ciog.cog[0].cgid";
connectAttr "groupId158.id" "pCylinderShape75.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape75.iog.og[0].gco";
connectAttr "groupId159.id" "pCylinderShape75.ciog.cog[0].cgid";
connectAttr "groupId160.id" "pCylinderShape77.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape77.iog.og[0].gco";
connectAttr "groupId161.id" "pCylinderShape77.ciog.cog[0].cgid";
connectAttr "groupId162.id" "pCylinderShape78.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape78.iog.og[0].gco";
connectAttr "groupId163.id" "pCylinderShape78.ciog.cog[0].cgid";
connectAttr "groupId164.id" "pCylinderShape79.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape79.iog.og[0].gco";
connectAttr "groupId165.id" "pCylinderShape79.ciog.cog[0].cgid";
connectAttr "groupId166.id" "pCylinderShape82.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape82.iog.og[0].gco";
connectAttr "groupId167.id" "pCylinderShape82.ciog.cog[0].cgid";
connectAttr "groupId168.id" "pCylinderShape84.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape84.iog.og[0].gco";
connectAttr "groupId169.id" "pCylinderShape84.ciog.cog[0].cgid";
connectAttr "groupId170.id" "pCylinderShape85.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape85.iog.og[0].gco";
connectAttr "groupId171.id" "pCylinderShape85.ciog.cog[0].cgid";
connectAttr "groupId172.id" "pCylinderShape86.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape86.iog.og[0].gco";
connectAttr "groupId173.id" "pCylinderShape86.ciog.cog[0].cgid";
connectAttr "groupId174.id" "pCylinderShape87.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape87.iog.og[0].gco";
connectAttr "groupId175.id" "pCylinderShape87.ciog.cog[0].cgid";
connectAttr "groupId176.id" "pCylinderShape88.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape88.iog.og[0].gco";
connectAttr "groupId177.id" "pCylinderShape88.ciog.cog[0].cgid";
connectAttr "groupId178.id" "pCylinderShape89.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape89.iog.og[0].gco";
connectAttr "groupId179.id" "pCylinderShape89.ciog.cog[0].cgid";
connectAttr "groupId180.id" "pCylinderShape90.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape90.iog.og[0].gco";
connectAttr "groupId181.id" "pCylinderShape90.ciog.cog[0].cgid";
connectAttr "groupId182.id" "pCylinderShape91.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape91.iog.og[0].gco";
connectAttr "groupId183.id" "pCylinderShape91.ciog.cog[0].cgid";
connectAttr "groupId184.id" "pCylinderShape92.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape92.iog.og[0].gco";
connectAttr "groupId185.id" "pCylinderShape92.ciog.cog[0].cgid";
connectAttr "groupId186.id" "pCylinderShape93.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape93.iog.og[0].gco";
connectAttr "groupId187.id" "pCylinderShape93.ciog.cog[0].cgid";
connectAttr "groupId188.id" "pCylinderShape94.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape94.iog.og[0].gco";
connectAttr "groupId189.id" "pCylinderShape94.ciog.cog[0].cgid";
connectAttr "groupId190.id" "pCylinderShape95.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape95.iog.og[0].gco";
connectAttr "groupId191.id" "pCylinderShape95.ciog.cog[0].cgid";
connectAttr "groupId192.id" "pCylinderShape96.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape96.iog.og[0].gco";
connectAttr "groupId193.id" "pCylinderShape96.ciog.cog[0].cgid";
connectAttr "groupId194.id" "pCylinderShape97.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape97.iog.og[0].gco";
connectAttr "groupId195.id" "pCylinderShape97.ciog.cog[0].cgid";
connectAttr "groupId196.id" "pCylinderShape98.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape98.iog.og[0].gco";
connectAttr "groupId197.id" "pCylinderShape98.ciog.cog[0].cgid";
connectAttr "groupId198.id" "pCylinderShape99.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape99.iog.og[0].gco";
connectAttr "groupId199.id" "pCylinderShape99.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape7.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape7.iog.og[0].gco";
connectAttr "groupId3.id" "pCubeShape7.iog.og[1].gid";
connectAttr "lambert2SG.mwc" "pCubeShape7.iog.og[1].gco";
connectAttr "groupId4.id" "pCubeShape7.iog.og[2].gid";
connectAttr "lambert7SG.mwc" "pCubeShape7.iog.og[2].gco";
connectAttr "groupParts3.og" "pCubeShape7.i";
connectAttr "groupId2.id" "pCubeShape7.ciog.cog[0].cgid";
connectAttr "groupId200.id" "pPlaneShape2.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "pPlaneShape2.iog.og[0].gco";
connectAttr "groupParts8.og" "pPlaneShape2.i";
connectAttr "groupId201.id" "pPlaneShape2.ciog.cog[0].cgid";
connectAttr "groupId202.id" "pConeShape1.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape1.iog.og[0].gco";
connectAttr "groupParts9.og" "pConeShape1.i";
connectAttr "groupId203.id" "pConeShape1.ciog.cog[0].cgid";
connectAttr "groupId204.id" "pConeShape2.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape2.iog.og[0].gco";
connectAttr "groupId205.id" "pConeShape2.ciog.cog[0].cgid";
connectAttr "groupId206.id" "pConeShape3.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape3.iog.og[0].gco";
connectAttr "groupId207.id" "pConeShape3.ciog.cog[0].cgid";
connectAttr "groupId208.id" "pConeShape4.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape4.iog.og[0].gco";
connectAttr "groupId209.id" "pConeShape4.ciog.cog[0].cgid";
connectAttr "groupId210.id" "pConeShape5.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape5.iog.og[0].gco";
connectAttr "groupId211.id" "pConeShape5.ciog.cog[0].cgid";
connectAttr "groupId212.id" "pConeShape6.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape6.iog.og[0].gco";
connectAttr "groupId213.id" "pConeShape6.ciog.cog[0].cgid";
connectAttr "groupId214.id" "pConeShape7.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape7.iog.og[0].gco";
connectAttr "groupId215.id" "pConeShape7.ciog.cog[0].cgid";
connectAttr "groupId216.id" "pConeShape8.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape8.iog.og[0].gco";
connectAttr "groupId217.id" "pConeShape8.ciog.cog[0].cgid";
connectAttr "groupId218.id" "pConeShape9.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape9.iog.og[0].gco";
connectAttr "groupId219.id" "pConeShape9.ciog.cog[0].cgid";
connectAttr "groupId220.id" "pConeShape10.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape10.iog.og[0].gco";
connectAttr "groupId221.id" "pConeShape10.ciog.cog[0].cgid";
connectAttr "groupId222.id" "pConeShape11.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape11.iog.og[0].gco";
connectAttr "groupId223.id" "pConeShape11.ciog.cog[0].cgid";
connectAttr "groupId224.id" "pConeShape12.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape12.iog.og[0].gco";
connectAttr "groupId225.id" "pConeShape12.ciog.cog[0].cgid";
connectAttr "groupId226.id" "pConeShape13.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape13.iog.og[0].gco";
connectAttr "groupId227.id" "pConeShape13.ciog.cog[0].cgid";
connectAttr "groupId228.id" "pConeShape14.iog.og[0].gid";
connectAttr ":initialShadingGroup.mwc" "pConeShape14.iog.og[0].gco";
connectAttr "groupId229.id" "pConeShape14.ciog.cog[0].cgid";
connectAttr "groupId230.id" "pCubeShape8.iog.og[0].gid";
connectAttr "lambert8SG.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupParts10.og" "pCubeShape8.i";
connectAttr "groupId231.id" "pCubeShape8.ciog.cog[0].cgid";
connectAttr "groupId232.id" "pCubeShape9.iog.og[0].gid";
connectAttr "lambert10SG.mwc" "pCubeShape9.iog.og[0].gco";
connectAttr "groupId233.id" "pCubeShape9.ciog.cog[0].cgid";
connectAttr "groupId234.id" "pCubeShape10.iog.og[0].gid";
connectAttr "lambert8SG.mwc" "pCubeShape10.iog.og[0].gco";
connectAttr "groupId235.id" "pCubeShape10.ciog.cog[0].cgid";
connectAttr "groupId236.id" "pCubeShape11.iog.og[0].gid";
connectAttr "lambert8SG.mwc" "pCubeShape11.iog.og[0].gco";
connectAttr "groupId237.id" "pCubeShape11.ciog.cog[0].cgid";
connectAttr "groupId238.id" "pCubeShape12.iog.og[0].gid";
connectAttr "lambert10SG.mwc" "pCubeShape12.iog.og[0].gco";
connectAttr "groupId239.id" "pCubeShape12.ciog.cog[0].cgid";
connectAttr "groupParts18.og" "pCube13Shape.i";
connectAttr "groupId240.id" "pCube13Shape.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCube13Shape.iog.og[0].gco";
connectAttr "groupId241.id" "pCube13Shape.iog.og[1].gid";
connectAttr "lambert4SG.mwc" "pCube13Shape.iog.og[1].gco";
connectAttr "groupId242.id" "pCube13Shape.iog.og[2].gid";
connectAttr "lambert7SG.mwc" "pCube13Shape.iog.og[2].gco";
connectAttr "groupId243.id" "pCube13Shape.iog.og[3].gid";
connectAttr "lambert2SG.mwc" "pCube13Shape.iog.og[3].gco";
connectAttr "groupId244.id" "pCube13Shape.iog.og[4].gid";
connectAttr "lambert6SG.mwc" "pCube13Shape.iog.og[4].gco";
connectAttr "groupId245.id" "pCube13Shape.iog.og[5].gid";
connectAttr ":initialShadingGroup.mwc" "pCube13Shape.iog.og[5].gco";
connectAttr "groupId246.id" "pCube13Shape.iog.og[6].gid";
connectAttr "lambert8SG.mwc" "pCube13Shape.iog.og[6].gco";
connectAttr "groupId247.id" "pCube13Shape.iog.og[7].gid";
connectAttr "lambert10SG.mwc" "pCube13Shape.iog.og[7].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert8SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert9SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert10SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert8SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert9SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert10SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder1_axisX.o" "polyCylinder1.axx";
connectAttr "polyCylinder1_axisY.o" "polyCylinder1.axy";
connectAttr "polyCylinder1_axisZ.o" "polyCylinder1.axz";
connectAttr "polyCylinder1_radius.o" "polyCylinder1.r";
connectAttr "polyCylinder1_height.o" "polyCylinder1.h";
connectAttr "polyCylinder1_subdivisionsAxis.o" "polyCylinder1.sa";
connectAttr "polyCylinder1_subdivisionsHeight.o" "polyCylinder1.sh";
connectAttr "polyCylinder1_subdivisionsCaps.o" "polyCylinder1.sc";
connectAttr "polySubdFace5.out" "polySubdFace6.ip";
connectAttr "polySubdFace4.out" "polySubdFace5.ip";
connectAttr "polyCube2.out" "polySubdFace4.ip";
connectAttr "polySurfaceShape1.o" "polySubdFace7.ip";
connectAttr "polySubdFace7.out" "polySubdFace8.ip";
connectAttr "polySubdFace8.out" "polySubdFace9.ip";
connectAttr "polySubdFace9.out" "polySmoothFace1.ip";
connectAttr "polySubdFace6.out" "polySmoothFace2.ip";
connectAttr "polyCube3.out" "polyBevel1.ip";
connectAttr "pCubeShape7.wm" "polyBevel1.mp";
connectAttr "polyBevel1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polyExtrudeFace3.ip";
connectAttr "pCubeShape7.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace2.out" "polyTweak2.ip";
connectAttr "polyExtrudeFace3.out" "polySubdFace10.ip";
connectAttr "polySubdFace10.out" "polySubdFace11.ip";
connectAttr "polySubdFace11.out" "polySubdFace12.ip";
connectAttr "polySubdFace12.out" "polySmoothFace3.ip";
connectAttr "house_snow.oc" "lambert2SG.ss";
connectAttr "pCubeShape7.iog.og[1]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape6.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape6.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[3]" "lambert2SG.dsm" -na;
connectAttr "groupId3.msg" "lambert2SG.gn" -na;
connectAttr "groupId150.msg" "lambert2SG.gn" -na;
connectAttr "groupId151.msg" "lambert2SG.gn" -na;
connectAttr "groupId152.msg" "lambert2SG.gn" -na;
connectAttr "groupId153.msg" "lambert2SG.gn" -na;
connectAttr "groupId243.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "house_snow.msg" "materialInfo1.m";
connectAttr "house_body.oc" "lambert3SG.ss";
connectAttr "pCubeShape7.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape7.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape1.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape4.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape4.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape6.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape6.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape8.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape8.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape10.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape10.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape12.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape12.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape14.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape14.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape16.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape16.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape18.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape18.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape20.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape20.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape26.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape26.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape28.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape28.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape30.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape30.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape32.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape32.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape34.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape34.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape37.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape37.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape39.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape39.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape41.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape41.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape43.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape43.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape45.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape45.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape46.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape46.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape49.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape49.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape54.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape54.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape56.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape56.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape59.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape59.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape60.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape60.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape63.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape63.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape64.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape64.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape71.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape71.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape73.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape73.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape75.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape75.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape77.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape77.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape78.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape78.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape79.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape79.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape82.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape82.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape84.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape84.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape85.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape85.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape86.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape86.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape89.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape89.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape91.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape91.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape92.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape92.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape93.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape93.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape94.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape94.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape95.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape95.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape96.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape96.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape97.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape97.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape98.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape98.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape99.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape99.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "groupId1.msg" "lambert3SG.gn" -na;
connectAttr "groupId2.msg" "lambert3SG.gn" -na;
connectAttr "groupId10.msg" "lambert3SG.gn" -na;
connectAttr "groupId11.msg" "lambert3SG.gn" -na;
connectAttr "groupId12.msg" "lambert3SG.gn" -na;
connectAttr "groupId13.msg" "lambert3SG.gn" -na;
connectAttr "groupId14.msg" "lambert3SG.gn" -na;
connectAttr "groupId15.msg" "lambert3SG.gn" -na;
connectAttr "groupId16.msg" "lambert3SG.gn" -na;
connectAttr "groupId17.msg" "lambert3SG.gn" -na;
connectAttr "groupId20.msg" "lambert3SG.gn" -na;
connectAttr "groupId21.msg" "lambert3SG.gn" -na;
connectAttr "groupId24.msg" "lambert3SG.gn" -na;
connectAttr "groupId25.msg" "lambert3SG.gn" -na;
connectAttr "groupId28.msg" "lambert3SG.gn" -na;
connectAttr "groupId29.msg" "lambert3SG.gn" -na;
connectAttr "groupId32.msg" "lambert3SG.gn" -na;
connectAttr "groupId33.msg" "lambert3SG.gn" -na;
connectAttr "groupId36.msg" "lambert3SG.gn" -na;
connectAttr "groupId37.msg" "lambert3SG.gn" -na;
connectAttr "groupId40.msg" "lambert3SG.gn" -na;
connectAttr "groupId41.msg" "lambert3SG.gn" -na;
connectAttr "groupId44.msg" "lambert3SG.gn" -na;
connectAttr "groupId45.msg" "lambert3SG.gn" -na;
connectAttr "groupId48.msg" "lambert3SG.gn" -na;
connectAttr "groupId49.msg" "lambert3SG.gn" -na;
connectAttr "groupId52.msg" "lambert3SG.gn" -na;
connectAttr "groupId53.msg" "lambert3SG.gn" -na;
connectAttr "groupId56.msg" "lambert3SG.gn" -na;
connectAttr "groupId57.msg" "lambert3SG.gn" -na;
connectAttr "groupId68.msg" "lambert3SG.gn" -na;
connectAttr "groupId69.msg" "lambert3SG.gn" -na;
connectAttr "groupId72.msg" "lambert3SG.gn" -na;
connectAttr "groupId73.msg" "lambert3SG.gn" -na;
connectAttr "groupId76.msg" "lambert3SG.gn" -na;
connectAttr "groupId77.msg" "lambert3SG.gn" -na;
connectAttr "groupId80.msg" "lambert3SG.gn" -na;
connectAttr "groupId81.msg" "lambert3SG.gn" -na;
connectAttr "groupId84.msg" "lambert3SG.gn" -na;
connectAttr "groupId85.msg" "lambert3SG.gn" -na;
connectAttr "groupId90.msg" "lambert3SG.gn" -na;
connectAttr "groupId91.msg" "lambert3SG.gn" -na;
connectAttr "groupId94.msg" "lambert3SG.gn" -na;
connectAttr "groupId95.msg" "lambert3SG.gn" -na;
connectAttr "groupId98.msg" "lambert3SG.gn" -na;
connectAttr "groupId99.msg" "lambert3SG.gn" -na;
connectAttr "groupId102.msg" "lambert3SG.gn" -na;
connectAttr "groupId103.msg" "lambert3SG.gn" -na;
connectAttr "groupId106.msg" "lambert3SG.gn" -na;
connectAttr "groupId107.msg" "lambert3SG.gn" -na;
connectAttr "groupId108.msg" "lambert3SG.gn" -na;
connectAttr "groupId109.msg" "lambert3SG.gn" -na;
connectAttr "groupId112.msg" "lambert3SG.gn" -na;
connectAttr "groupId113.msg" "lambert3SG.gn" -na;
connectAttr "groupId116.msg" "lambert3SG.gn" -na;
connectAttr "groupId117.msg" "lambert3SG.gn" -na;
connectAttr "groupId120.msg" "lambert3SG.gn" -na;
connectAttr "groupId121.msg" "lambert3SG.gn" -na;
connectAttr "groupId126.msg" "lambert3SG.gn" -na;
connectAttr "groupId127.msg" "lambert3SG.gn" -na;
connectAttr "groupId128.msg" "lambert3SG.gn" -na;
connectAttr "groupId129.msg" "lambert3SG.gn" -na;
connectAttr "groupId134.msg" "lambert3SG.gn" -na;
connectAttr "groupId135.msg" "lambert3SG.gn" -na;
connectAttr "groupId136.msg" "lambert3SG.gn" -na;
connectAttr "groupId137.msg" "lambert3SG.gn" -na;
connectAttr "groupId154.msg" "lambert3SG.gn" -na;
connectAttr "groupId155.msg" "lambert3SG.gn" -na;
connectAttr "groupId156.msg" "lambert3SG.gn" -na;
connectAttr "groupId157.msg" "lambert3SG.gn" -na;
connectAttr "groupId158.msg" "lambert3SG.gn" -na;
connectAttr "groupId159.msg" "lambert3SG.gn" -na;
connectAttr "groupId160.msg" "lambert3SG.gn" -na;
connectAttr "groupId161.msg" "lambert3SG.gn" -na;
connectAttr "groupId162.msg" "lambert3SG.gn" -na;
connectAttr "groupId163.msg" "lambert3SG.gn" -na;
connectAttr "groupId164.msg" "lambert3SG.gn" -na;
connectAttr "groupId165.msg" "lambert3SG.gn" -na;
connectAttr "groupId166.msg" "lambert3SG.gn" -na;
connectAttr "groupId167.msg" "lambert3SG.gn" -na;
connectAttr "groupId168.msg" "lambert3SG.gn" -na;
connectAttr "groupId169.msg" "lambert3SG.gn" -na;
connectAttr "groupId170.msg" "lambert3SG.gn" -na;
connectAttr "groupId171.msg" "lambert3SG.gn" -na;
connectAttr "groupId172.msg" "lambert3SG.gn" -na;
connectAttr "groupId173.msg" "lambert3SG.gn" -na;
connectAttr "groupId178.msg" "lambert3SG.gn" -na;
connectAttr "groupId179.msg" "lambert3SG.gn" -na;
connectAttr "groupId182.msg" "lambert3SG.gn" -na;
connectAttr "groupId183.msg" "lambert3SG.gn" -na;
connectAttr "groupId184.msg" "lambert3SG.gn" -na;
connectAttr "groupId185.msg" "lambert3SG.gn" -na;
connectAttr "groupId186.msg" "lambert3SG.gn" -na;
connectAttr "groupId187.msg" "lambert3SG.gn" -na;
connectAttr "groupId188.msg" "lambert3SG.gn" -na;
connectAttr "groupId189.msg" "lambert3SG.gn" -na;
connectAttr "groupId190.msg" "lambert3SG.gn" -na;
connectAttr "groupId191.msg" "lambert3SG.gn" -na;
connectAttr "groupId192.msg" "lambert3SG.gn" -na;
connectAttr "groupId193.msg" "lambert3SG.gn" -na;
connectAttr "groupId194.msg" "lambert3SG.gn" -na;
connectAttr "groupId195.msg" "lambert3SG.gn" -na;
connectAttr "groupId196.msg" "lambert3SG.gn" -na;
connectAttr "groupId197.msg" "lambert3SG.gn" -na;
connectAttr "groupId198.msg" "lambert3SG.gn" -na;
connectAttr "groupId199.msg" "lambert3SG.gn" -na;
connectAttr "groupId240.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "house_body.msg" "materialInfo2.m";
connectAttr "house_details_tronk.oc" "lambert4SG.ss";
connectAttr "pCylinderShape1.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape3.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape3.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape5.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape5.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape7.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape7.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape9.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape9.ciog.cog[1]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape11.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape11.ciog.cog[1]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape13.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape13.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape15.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape15.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape17.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape17.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape19.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape19.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape21.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape21.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape25.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape25.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape27.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape27.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape29.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape29.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape31.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape31.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape33.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape33.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape35.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape35.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape36.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape36.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape38.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape38.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape40.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape40.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape42.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape42.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape44.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape44.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape47.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape47.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape53.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape53.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape55.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape55.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape57.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape57.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape58.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape58.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape61.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape61.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape62.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape62.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape87.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape87.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape88.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape88.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape90.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape90.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[1]" "lambert4SG.dsm" -na;
connectAttr "groupId4.msg" "lambert4SG.gn" -na;
connectAttr "groupId18.msg" "lambert4SG.gn" -na;
connectAttr "groupId19.msg" "lambert4SG.gn" -na;
connectAttr "groupId22.msg" "lambert4SG.gn" -na;
connectAttr "groupId23.msg" "lambert4SG.gn" -na;
connectAttr "groupId26.msg" "lambert4SG.gn" -na;
connectAttr "groupId27.msg" "lambert4SG.gn" -na;
connectAttr "groupId30.msg" "lambert4SG.gn" -na;
connectAttr "groupId31.msg" "lambert4SG.gn" -na;
connectAttr "groupId34.msg" "lambert4SG.gn" -na;
connectAttr "groupId35.msg" "lambert4SG.gn" -na;
connectAttr "groupId38.msg" "lambert4SG.gn" -na;
connectAttr "groupId39.msg" "lambert4SG.gn" -na;
connectAttr "groupId42.msg" "lambert4SG.gn" -na;
connectAttr "groupId43.msg" "lambert4SG.gn" -na;
connectAttr "groupId46.msg" "lambert4SG.gn" -na;
connectAttr "groupId47.msg" "lambert4SG.gn" -na;
connectAttr "groupId50.msg" "lambert4SG.gn" -na;
connectAttr "groupId51.msg" "lambert4SG.gn" -na;
connectAttr "groupId54.msg" "lambert4SG.gn" -na;
connectAttr "groupId55.msg" "lambert4SG.gn" -na;
connectAttr "groupId58.msg" "lambert4SG.gn" -na;
connectAttr "groupId59.msg" "lambert4SG.gn" -na;
connectAttr "groupId66.msg" "lambert4SG.gn" -na;
connectAttr "groupId67.msg" "lambert4SG.gn" -na;
connectAttr "groupId70.msg" "lambert4SG.gn" -na;
connectAttr "groupId71.msg" "lambert4SG.gn" -na;
connectAttr "groupId74.msg" "lambert4SG.gn" -na;
connectAttr "groupId75.msg" "lambert4SG.gn" -na;
connectAttr "groupId78.msg" "lambert4SG.gn" -na;
connectAttr "groupId79.msg" "lambert4SG.gn" -na;
connectAttr "groupId82.msg" "lambert4SG.gn" -na;
connectAttr "groupId83.msg" "lambert4SG.gn" -na;
connectAttr "groupId86.msg" "lambert4SG.gn" -na;
connectAttr "groupId87.msg" "lambert4SG.gn" -na;
connectAttr "groupId88.msg" "lambert4SG.gn" -na;
connectAttr "groupId89.msg" "lambert4SG.gn" -na;
connectAttr "groupId92.msg" "lambert4SG.gn" -na;
connectAttr "groupId93.msg" "lambert4SG.gn" -na;
connectAttr "groupId96.msg" "lambert4SG.gn" -na;
connectAttr "groupId97.msg" "lambert4SG.gn" -na;
connectAttr "groupId100.msg" "lambert4SG.gn" -na;
connectAttr "groupId101.msg" "lambert4SG.gn" -na;
connectAttr "groupId104.msg" "lambert4SG.gn" -na;
connectAttr "groupId105.msg" "lambert4SG.gn" -na;
connectAttr "groupId110.msg" "lambert4SG.gn" -na;
connectAttr "groupId111.msg" "lambert4SG.gn" -na;
connectAttr "groupId114.msg" "lambert4SG.gn" -na;
connectAttr "groupId115.msg" "lambert4SG.gn" -na;
connectAttr "groupId118.msg" "lambert4SG.gn" -na;
connectAttr "groupId119.msg" "lambert4SG.gn" -na;
connectAttr "groupId122.msg" "lambert4SG.gn" -na;
connectAttr "groupId123.msg" "lambert4SG.gn" -na;
connectAttr "groupId124.msg" "lambert4SG.gn" -na;
connectAttr "groupId125.msg" "lambert4SG.gn" -na;
connectAttr "groupId130.msg" "lambert4SG.gn" -na;
connectAttr "groupId131.msg" "lambert4SG.gn" -na;
connectAttr "groupId132.msg" "lambert4SG.gn" -na;
connectAttr "groupId133.msg" "lambert4SG.gn" -na;
connectAttr "groupId174.msg" "lambert4SG.gn" -na;
connectAttr "groupId175.msg" "lambert4SG.gn" -na;
connectAttr "groupId176.msg" "lambert4SG.gn" -na;
connectAttr "groupId177.msg" "lambert4SG.gn" -na;
connectAttr "groupId180.msg" "lambert4SG.gn" -na;
connectAttr "groupId181.msg" "lambert4SG.gn" -na;
connectAttr "groupId241.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "house_details_tronk.msg" "materialInfo3.m";
connectAttr "house_chimney_snow.oc" "lambert5SG.ss";
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "house_chimney_snow.msg" "materialInfo4.m";
connectAttr "polySmoothFace3.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "groupParts2.og" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "snow_house_floor.oc" "lambert6SG.ss";
connectAttr "pPlaneShape2.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "pPlaneShape2.ciog.cog[0]" "lambert6SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[4]" "lambert6SG.dsm" -na;
connectAttr "groupId200.msg" "lambert6SG.gn" -na;
connectAttr "groupId201.msg" "lambert6SG.gn" -na;
connectAttr "groupId244.msg" "lambert6SG.gn" -na;
connectAttr "lambert6SG.msg" "materialInfo5.sg";
connectAttr "snow_house_floor.msg" "materialInfo5.m";
connectAttr "house_snow_door_window.oc" "lambert7SG.ss";
connectAttr "pCubeShape7.iog.og[2]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape22.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape22.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape23.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape23.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape24.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape24.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape65.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape65.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape66.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape66.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape67.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape67.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape68.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape68.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape69.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape69.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape70.iog.og[0]" "lambert7SG.dsm" -na;
connectAttr "pCylinderShape70.ciog.cog[0]" "lambert7SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[2]" "lambert7SG.dsm" -na;
connectAttr "groupId60.msg" "lambert7SG.gn" -na;
connectAttr "groupId61.msg" "lambert7SG.gn" -na;
connectAttr "groupId62.msg" "lambert7SG.gn" -na;
connectAttr "groupId63.msg" "lambert7SG.gn" -na;
connectAttr "groupId64.msg" "lambert7SG.gn" -na;
connectAttr "groupId65.msg" "lambert7SG.gn" -na;
connectAttr "groupId138.msg" "lambert7SG.gn" -na;
connectAttr "groupId139.msg" "lambert7SG.gn" -na;
connectAttr "groupId140.msg" "lambert7SG.gn" -na;
connectAttr "groupId141.msg" "lambert7SG.gn" -na;
connectAttr "groupId142.msg" "lambert7SG.gn" -na;
connectAttr "groupId143.msg" "lambert7SG.gn" -na;
connectAttr "groupId144.msg" "lambert7SG.gn" -na;
connectAttr "groupId145.msg" "lambert7SG.gn" -na;
connectAttr "groupId146.msg" "lambert7SG.gn" -na;
connectAttr "groupId147.msg" "lambert7SG.gn" -na;
connectAttr "groupId148.msg" "lambert7SG.gn" -na;
connectAttr "groupId149.msg" "lambert7SG.gn" -na;
connectAttr "groupId242.msg" "lambert7SG.gn" -na;
connectAttr "lambert7SG.msg" "materialInfo6.sg";
connectAttr "house_snow_door_window.msg" "materialInfo6.m";
connectAttr "snow_door_house.oc" "lambert8SG.ss";
connectAttr "pCubeShape8.iog.og[0]" "lambert8SG.dsm" -na;
connectAttr "pCubeShape8.ciog.cog[0]" "lambert8SG.dsm" -na;
connectAttr "pCubeShape10.iog.og[0]" "lambert8SG.dsm" -na;
connectAttr "pCubeShape10.ciog.cog[0]" "lambert8SG.dsm" -na;
connectAttr "pCubeShape11.iog.og[0]" "lambert8SG.dsm" -na;
connectAttr "pCubeShape11.ciog.cog[0]" "lambert8SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[6]" "lambert8SG.dsm" -na;
connectAttr "groupId230.msg" "lambert8SG.gn" -na;
connectAttr "groupId231.msg" "lambert8SG.gn" -na;
connectAttr "groupId234.msg" "lambert8SG.gn" -na;
connectAttr "groupId235.msg" "lambert8SG.gn" -na;
connectAttr "groupId236.msg" "lambert8SG.gn" -na;
connectAttr "groupId237.msg" "lambert8SG.gn" -na;
connectAttr "groupId246.msg" "lambert8SG.gn" -na;
connectAttr "lambert8SG.msg" "materialInfo7.sg";
connectAttr "snow_door_house.msg" "materialInfo7.m";
connectAttr "house_ice.oc" "lambert9SG.ss";
connectAttr "lambert9SG.msg" "materialInfo8.sg";
connectAttr "house_ice.msg" "materialInfo8.m";
connectAttr "house_glass.oc" "lambert10SG.ss";
connectAttr "pCubeShape9.iog.og[0]" "lambert10SG.dsm" -na;
connectAttr "pCubeShape9.ciog.cog[0]" "lambert10SG.dsm" -na;
connectAttr "pCubeShape12.iog.og[0]" "lambert10SG.dsm" -na;
connectAttr "pCubeShape12.ciog.cog[0]" "lambert10SG.dsm" -na;
connectAttr "pCube13Shape.iog.og[7]" "lambert10SG.dsm" -na;
connectAttr "groupId232.msg" "lambert10SG.gn" -na;
connectAttr "groupId233.msg" "lambert10SG.gn" -na;
connectAttr "groupId238.msg" "lambert10SG.gn" -na;
connectAttr "groupId239.msg" "lambert10SG.gn" -na;
connectAttr "groupId247.msg" "lambert10SG.gn" -na;
connectAttr "lambert10SG.msg" "materialInfo9.sg";
connectAttr "house_glass.msg" "materialInfo9.m";
connectAttr "pCubeShape1.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape3.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape4.o" "polyUnite1.ip[3]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[4]";
connectAttr "pCylinderShape2.o" "polyUnite1.ip[5]";
connectAttr "pCylinderShape3.o" "polyUnite1.ip[6]";
connectAttr "pCylinderShape4.o" "polyUnite1.ip[7]";
connectAttr "pCylinderShape5.o" "polyUnite1.ip[8]";
connectAttr "pCylinderShape6.o" "polyUnite1.ip[9]";
connectAttr "pCylinderShape7.o" "polyUnite1.ip[10]";
connectAttr "pCylinderShape8.o" "polyUnite1.ip[11]";
connectAttr "pCylinderShape9.o" "polyUnite1.ip[12]";
connectAttr "pCylinderShape10.o" "polyUnite1.ip[13]";
connectAttr "pCylinderShape11.o" "polyUnite1.ip[14]";
connectAttr "pCylinderShape12.o" "polyUnite1.ip[15]";
connectAttr "pCylinderShape13.o" "polyUnite1.ip[16]";
connectAttr "pCylinderShape14.o" "polyUnite1.ip[17]";
connectAttr "pCylinderShape15.o" "polyUnite1.ip[18]";
connectAttr "pCylinderShape16.o" "polyUnite1.ip[19]";
connectAttr "pCylinderShape17.o" "polyUnite1.ip[20]";
connectAttr "pCylinderShape18.o" "polyUnite1.ip[21]";
connectAttr "pCylinderShape19.o" "polyUnite1.ip[22]";
connectAttr "pCylinderShape20.o" "polyUnite1.ip[23]";
connectAttr "pCylinderShape21.o" "polyUnite1.ip[24]";
connectAttr "pCylinderShape22.o" "polyUnite1.ip[25]";
connectAttr "pCylinderShape23.o" "polyUnite1.ip[26]";
connectAttr "pCylinderShape24.o" "polyUnite1.ip[27]";
connectAttr "pCylinderShape25.o" "polyUnite1.ip[28]";
connectAttr "pCylinderShape26.o" "polyUnite1.ip[29]";
connectAttr "pCylinderShape27.o" "polyUnite1.ip[30]";
connectAttr "pCylinderShape28.o" "polyUnite1.ip[31]";
connectAttr "pCylinderShape29.o" "polyUnite1.ip[32]";
connectAttr "pCylinderShape30.o" "polyUnite1.ip[33]";
connectAttr "pCylinderShape31.o" "polyUnite1.ip[34]";
connectAttr "pCylinderShape32.o" "polyUnite1.ip[35]";
connectAttr "pCylinderShape33.o" "polyUnite1.ip[36]";
connectAttr "pCylinderShape34.o" "polyUnite1.ip[37]";
connectAttr "pCylinderShape35.o" "polyUnite1.ip[38]";
connectAttr "pCylinderShape36.o" "polyUnite1.ip[39]";
connectAttr "pCylinderShape37.o" "polyUnite1.ip[40]";
connectAttr "pCylinderShape38.o" "polyUnite1.ip[41]";
connectAttr "pCylinderShape39.o" "polyUnite1.ip[42]";
connectAttr "pCylinderShape40.o" "polyUnite1.ip[43]";
connectAttr "pCylinderShape41.o" "polyUnite1.ip[44]";
connectAttr "pCylinderShape42.o" "polyUnite1.ip[45]";
connectAttr "pCylinderShape43.o" "polyUnite1.ip[46]";
connectAttr "pCylinderShape44.o" "polyUnite1.ip[47]";
connectAttr "pCylinderShape45.o" "polyUnite1.ip[48]";
connectAttr "pCylinderShape46.o" "polyUnite1.ip[49]";
connectAttr "pCylinderShape47.o" "polyUnite1.ip[50]";
connectAttr "pCylinderShape49.o" "polyUnite1.ip[51]";
connectAttr "pCylinderShape53.o" "polyUnite1.ip[52]";
connectAttr "pCylinderShape54.o" "polyUnite1.ip[53]";
connectAttr "pCylinderShape55.o" "polyUnite1.ip[54]";
connectAttr "pCylinderShape56.o" "polyUnite1.ip[55]";
connectAttr "pCylinderShape57.o" "polyUnite1.ip[56]";
connectAttr "pCylinderShape58.o" "polyUnite1.ip[57]";
connectAttr "pCylinderShape59.o" "polyUnite1.ip[58]";
connectAttr "pCylinderShape60.o" "polyUnite1.ip[59]";
connectAttr "pCylinderShape61.o" "polyUnite1.ip[60]";
connectAttr "pCylinderShape62.o" "polyUnite1.ip[61]";
connectAttr "pCylinderShape63.o" "polyUnite1.ip[62]";
connectAttr "pCylinderShape64.o" "polyUnite1.ip[63]";
connectAttr "pCylinderShape65.o" "polyUnite1.ip[64]";
connectAttr "pCylinderShape66.o" "polyUnite1.ip[65]";
connectAttr "pCylinderShape67.o" "polyUnite1.ip[66]";
connectAttr "pCylinderShape68.o" "polyUnite1.ip[67]";
connectAttr "pCylinderShape69.o" "polyUnite1.ip[68]";
connectAttr "pCylinderShape70.o" "polyUnite1.ip[69]";
connectAttr "pCubeShape5.o" "polyUnite1.ip[70]";
connectAttr "pCubeShape6.o" "polyUnite1.ip[71]";
connectAttr "pCylinderShape71.o" "polyUnite1.ip[72]";
connectAttr "pCylinderShape73.o" "polyUnite1.ip[73]";
connectAttr "pCylinderShape75.o" "polyUnite1.ip[74]";
connectAttr "pCylinderShape77.o" "polyUnite1.ip[75]";
connectAttr "pCylinderShape78.o" "polyUnite1.ip[76]";
connectAttr "pCylinderShape79.o" "polyUnite1.ip[77]";
connectAttr "pCylinderShape82.o" "polyUnite1.ip[78]";
connectAttr "pCylinderShape84.o" "polyUnite1.ip[79]";
connectAttr "pCylinderShape85.o" "polyUnite1.ip[80]";
connectAttr "pCylinderShape86.o" "polyUnite1.ip[81]";
connectAttr "pCylinderShape87.o" "polyUnite1.ip[82]";
connectAttr "pCylinderShape88.o" "polyUnite1.ip[83]";
connectAttr "pCylinderShape89.o" "polyUnite1.ip[84]";
connectAttr "pCylinderShape90.o" "polyUnite1.ip[85]";
connectAttr "pCylinderShape91.o" "polyUnite1.ip[86]";
connectAttr "pCylinderShape92.o" "polyUnite1.ip[87]";
connectAttr "pCylinderShape93.o" "polyUnite1.ip[88]";
connectAttr "pCylinderShape94.o" "polyUnite1.ip[89]";
connectAttr "pCylinderShape95.o" "polyUnite1.ip[90]";
connectAttr "pCylinderShape96.o" "polyUnite1.ip[91]";
connectAttr "pCylinderShape97.o" "polyUnite1.ip[92]";
connectAttr "pCylinderShape98.o" "polyUnite1.ip[93]";
connectAttr "pCylinderShape99.o" "polyUnite1.ip[94]";
connectAttr "pCubeShape7.o" "polyUnite1.ip[95]";
connectAttr "pPlaneShape2.o" "polyUnite1.ip[96]";
connectAttr "pConeShape1.o" "polyUnite1.ip[97]";
connectAttr "pConeShape2.o" "polyUnite1.ip[98]";
connectAttr "pConeShape3.o" "polyUnite1.ip[99]";
connectAttr "pConeShape4.o" "polyUnite1.ip[100]";
connectAttr "pConeShape5.o" "polyUnite1.ip[101]";
connectAttr "pConeShape6.o" "polyUnite1.ip[102]";
connectAttr "pConeShape7.o" "polyUnite1.ip[103]";
connectAttr "pConeShape8.o" "polyUnite1.ip[104]";
connectAttr "pConeShape9.o" "polyUnite1.ip[105]";
connectAttr "pConeShape10.o" "polyUnite1.ip[106]";
connectAttr "pConeShape11.o" "polyUnite1.ip[107]";
connectAttr "pConeShape12.o" "polyUnite1.ip[108]";
connectAttr "pConeShape13.o" "polyUnite1.ip[109]";
connectAttr "pConeShape14.o" "polyUnite1.ip[110]";
connectAttr "pCubeShape8.o" "polyUnite1.ip[111]";
connectAttr "pCubeShape9.o" "polyUnite1.ip[112]";
connectAttr "pCubeShape10.o" "polyUnite1.ip[113]";
connectAttr "pCubeShape11.o" "polyUnite1.ip[114]";
connectAttr "pCubeShape12.o" "polyUnite1.ip[115]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape3.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape4.wm" "polyUnite1.im[3]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[4]";
connectAttr "pCylinderShape2.wm" "polyUnite1.im[5]";
connectAttr "pCylinderShape3.wm" "polyUnite1.im[6]";
connectAttr "pCylinderShape4.wm" "polyUnite1.im[7]";
connectAttr "pCylinderShape5.wm" "polyUnite1.im[8]";
connectAttr "pCylinderShape6.wm" "polyUnite1.im[9]";
connectAttr "pCylinderShape7.wm" "polyUnite1.im[10]";
connectAttr "pCylinderShape8.wm" "polyUnite1.im[11]";
connectAttr "pCylinderShape9.wm" "polyUnite1.im[12]";
connectAttr "pCylinderShape10.wm" "polyUnite1.im[13]";
connectAttr "pCylinderShape11.wm" "polyUnite1.im[14]";
connectAttr "pCylinderShape12.wm" "polyUnite1.im[15]";
connectAttr "pCylinderShape13.wm" "polyUnite1.im[16]";
connectAttr "pCylinderShape14.wm" "polyUnite1.im[17]";
connectAttr "pCylinderShape15.wm" "polyUnite1.im[18]";
connectAttr "pCylinderShape16.wm" "polyUnite1.im[19]";
connectAttr "pCylinderShape17.wm" "polyUnite1.im[20]";
connectAttr "pCylinderShape18.wm" "polyUnite1.im[21]";
connectAttr "pCylinderShape19.wm" "polyUnite1.im[22]";
connectAttr "pCylinderShape20.wm" "polyUnite1.im[23]";
connectAttr "pCylinderShape21.wm" "polyUnite1.im[24]";
connectAttr "pCylinderShape22.wm" "polyUnite1.im[25]";
connectAttr "pCylinderShape23.wm" "polyUnite1.im[26]";
connectAttr "pCylinderShape24.wm" "polyUnite1.im[27]";
connectAttr "pCylinderShape25.wm" "polyUnite1.im[28]";
connectAttr "pCylinderShape26.wm" "polyUnite1.im[29]";
connectAttr "pCylinderShape27.wm" "polyUnite1.im[30]";
connectAttr "pCylinderShape28.wm" "polyUnite1.im[31]";
connectAttr "pCylinderShape29.wm" "polyUnite1.im[32]";
connectAttr "pCylinderShape30.wm" "polyUnite1.im[33]";
connectAttr "pCylinderShape31.wm" "polyUnite1.im[34]";
connectAttr "pCylinderShape32.wm" "polyUnite1.im[35]";
connectAttr "pCylinderShape33.wm" "polyUnite1.im[36]";
connectAttr "pCylinderShape34.wm" "polyUnite1.im[37]";
connectAttr "pCylinderShape35.wm" "polyUnite1.im[38]";
connectAttr "pCylinderShape36.wm" "polyUnite1.im[39]";
connectAttr "pCylinderShape37.wm" "polyUnite1.im[40]";
connectAttr "pCylinderShape38.wm" "polyUnite1.im[41]";
connectAttr "pCylinderShape39.wm" "polyUnite1.im[42]";
connectAttr "pCylinderShape40.wm" "polyUnite1.im[43]";
connectAttr "pCylinderShape41.wm" "polyUnite1.im[44]";
connectAttr "pCylinderShape42.wm" "polyUnite1.im[45]";
connectAttr "pCylinderShape43.wm" "polyUnite1.im[46]";
connectAttr "pCylinderShape44.wm" "polyUnite1.im[47]";
connectAttr "pCylinderShape45.wm" "polyUnite1.im[48]";
connectAttr "pCylinderShape46.wm" "polyUnite1.im[49]";
connectAttr "pCylinderShape47.wm" "polyUnite1.im[50]";
connectAttr "pCylinderShape49.wm" "polyUnite1.im[51]";
connectAttr "pCylinderShape53.wm" "polyUnite1.im[52]";
connectAttr "pCylinderShape54.wm" "polyUnite1.im[53]";
connectAttr "pCylinderShape55.wm" "polyUnite1.im[54]";
connectAttr "pCylinderShape56.wm" "polyUnite1.im[55]";
connectAttr "pCylinderShape57.wm" "polyUnite1.im[56]";
connectAttr "pCylinderShape58.wm" "polyUnite1.im[57]";
connectAttr "pCylinderShape59.wm" "polyUnite1.im[58]";
connectAttr "pCylinderShape60.wm" "polyUnite1.im[59]";
connectAttr "pCylinderShape61.wm" "polyUnite1.im[60]";
connectAttr "pCylinderShape62.wm" "polyUnite1.im[61]";
connectAttr "pCylinderShape63.wm" "polyUnite1.im[62]";
connectAttr "pCylinderShape64.wm" "polyUnite1.im[63]";
connectAttr "pCylinderShape65.wm" "polyUnite1.im[64]";
connectAttr "pCylinderShape66.wm" "polyUnite1.im[65]";
connectAttr "pCylinderShape67.wm" "polyUnite1.im[66]";
connectAttr "pCylinderShape68.wm" "polyUnite1.im[67]";
connectAttr "pCylinderShape69.wm" "polyUnite1.im[68]";
connectAttr "pCylinderShape70.wm" "polyUnite1.im[69]";
connectAttr "pCubeShape5.wm" "polyUnite1.im[70]";
connectAttr "pCubeShape6.wm" "polyUnite1.im[71]";
connectAttr "pCylinderShape71.wm" "polyUnite1.im[72]";
connectAttr "pCylinderShape73.wm" "polyUnite1.im[73]";
connectAttr "pCylinderShape75.wm" "polyUnite1.im[74]";
connectAttr "pCylinderShape77.wm" "polyUnite1.im[75]";
connectAttr "pCylinderShape78.wm" "polyUnite1.im[76]";
connectAttr "pCylinderShape79.wm" "polyUnite1.im[77]";
connectAttr "pCylinderShape82.wm" "polyUnite1.im[78]";
connectAttr "pCylinderShape84.wm" "polyUnite1.im[79]";
connectAttr "pCylinderShape85.wm" "polyUnite1.im[80]";
connectAttr "pCylinderShape86.wm" "polyUnite1.im[81]";
connectAttr "pCylinderShape87.wm" "polyUnite1.im[82]";
connectAttr "pCylinderShape88.wm" "polyUnite1.im[83]";
connectAttr "pCylinderShape89.wm" "polyUnite1.im[84]";
connectAttr "pCylinderShape90.wm" "polyUnite1.im[85]";
connectAttr "pCylinderShape91.wm" "polyUnite1.im[86]";
connectAttr "pCylinderShape92.wm" "polyUnite1.im[87]";
connectAttr "pCylinderShape93.wm" "polyUnite1.im[88]";
connectAttr "pCylinderShape94.wm" "polyUnite1.im[89]";
connectAttr "pCylinderShape95.wm" "polyUnite1.im[90]";
connectAttr "pCylinderShape96.wm" "polyUnite1.im[91]";
connectAttr "pCylinderShape97.wm" "polyUnite1.im[92]";
connectAttr "pCylinderShape98.wm" "polyUnite1.im[93]";
connectAttr "pCylinderShape99.wm" "polyUnite1.im[94]";
connectAttr "pCubeShape7.wm" "polyUnite1.im[95]";
connectAttr "pPlaneShape2.wm" "polyUnite1.im[96]";
connectAttr "pConeShape1.wm" "polyUnite1.im[97]";
connectAttr "pConeShape2.wm" "polyUnite1.im[98]";
connectAttr "pConeShape3.wm" "polyUnite1.im[99]";
connectAttr "pConeShape4.wm" "polyUnite1.im[100]";
connectAttr "pConeShape5.wm" "polyUnite1.im[101]";
connectAttr "pConeShape6.wm" "polyUnite1.im[102]";
connectAttr "pConeShape7.wm" "polyUnite1.im[103]";
connectAttr "pConeShape8.wm" "polyUnite1.im[104]";
connectAttr "pConeShape9.wm" "polyUnite1.im[105]";
connectAttr "pConeShape10.wm" "polyUnite1.im[106]";
connectAttr "pConeShape11.wm" "polyUnite1.im[107]";
connectAttr "pConeShape12.wm" "polyUnite1.im[108]";
connectAttr "pConeShape13.wm" "polyUnite1.im[109]";
connectAttr "pConeShape14.wm" "polyUnite1.im[110]";
connectAttr "pCubeShape8.wm" "polyUnite1.im[111]";
connectAttr "pCubeShape9.wm" "polyUnite1.im[112]";
connectAttr "pCubeShape10.wm" "polyUnite1.im[113]";
connectAttr "pCubeShape11.wm" "polyUnite1.im[114]";
connectAttr "pCubeShape12.wm" "polyUnite1.im[115]";
connectAttr "polyCube1.out" "groupParts4.ig";
connectAttr "groupId10.id" "groupParts4.gi";
connectAttr "polyCylinder1.out" "groupParts5.ig";
connectAttr "groupId18.id" "groupParts5.gi";
connectAttr "polySmoothFace2.out" "groupParts6.ig";
connectAttr "groupId150.id" "groupParts6.gi";
connectAttr "polySmoothFace1.out" "groupParts7.ig";
connectAttr "groupId152.id" "groupParts7.gi";
connectAttr "polyPlane2.out" "groupParts8.ig";
connectAttr "groupId200.id" "groupParts8.gi";
connectAttr "polyCone1.out" "groupParts9.ig";
connectAttr "groupId202.id" "groupParts9.gi";
connectAttr "polyCube4.out" "groupParts10.ig";
connectAttr "groupId230.id" "groupParts10.gi";
connectAttr "polyUnite1.out" "groupParts11.ig";
connectAttr "groupId240.id" "groupParts11.gi";
connectAttr "groupParts11.og" "groupParts12.ig";
connectAttr "groupId241.id" "groupParts12.gi";
connectAttr "groupParts12.og" "groupParts13.ig";
connectAttr "groupId242.id" "groupParts13.gi";
connectAttr "groupParts13.og" "groupParts14.ig";
connectAttr "groupId243.id" "groupParts14.gi";
connectAttr "groupParts14.og" "groupParts15.ig";
connectAttr "groupId244.id" "groupParts15.gi";
connectAttr "groupParts15.og" "groupParts16.ig";
connectAttr "groupId245.id" "groupParts16.gi";
connectAttr "groupParts16.og" "groupParts17.ig";
connectAttr "groupId246.id" "groupParts17.gi";
connectAttr "groupParts17.og" "groupParts18.ig";
connectAttr "groupId247.id" "groupParts18.gi";
connectAttr "lambert4SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "house_body.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "house_details_tronk.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "snow_house_floor.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "house_chimney_snow.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "lambert5SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "house_snow.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "lambert6SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "house_snow_door_window.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "lambert7SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "snow_door_house.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[12].dn"
		;
connectAttr "lambert8SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[13].dn"
		;
connectAttr "house_ice.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[14].dn"
		;
connectAttr "lambert9SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[15].dn"
		;
connectAttr "house_glass.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[16].dn"
		;
connectAttr "lambert10SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[17].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "lambert7SG.pa" ":renderPartition.st" -na;
connectAttr "lambert8SG.pa" ":renderPartition.st" -na;
connectAttr "lambert9SG.pa" ":renderPartition.st" -na;
connectAttr "lambert10SG.pa" ":renderPartition.st" -na;
connectAttr "house_snow.msg" ":defaultShaderList1.s" -na;
connectAttr "house_body.msg" ":defaultShaderList1.s" -na;
connectAttr "house_details_tronk.msg" ":defaultShaderList1.s" -na;
connectAttr "house_chimney_snow.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_house_floor.msg" ":defaultShaderList1.s" -na;
connectAttr "house_snow_door_window.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_door_house.msg" ":defaultShaderList1.s" -na;
connectAttr "house_ice.msg" ":defaultShaderList1.s" -na;
connectAttr "house_glass.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pConeShape1.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape1.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape2.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape2.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape3.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape3.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape4.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape4.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape5.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape5.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape6.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape6.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape7.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape7.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape8.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape8.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape9.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape9.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape10.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape10.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape11.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape11.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape12.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape12.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape13.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape13.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape14.iog.og[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pConeShape14.ciog.cog[0]" ":initialShadingGroup.dsm" -na;
connectAttr "pCube13Shape.iog.og[5]" ":initialShadingGroup.dsm" -na;
connectAttr "groupId202.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId203.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId204.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId205.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId206.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId207.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId208.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId209.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId210.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId211.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId212.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId213.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId214.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId215.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId216.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId217.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId218.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId219.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId220.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId221.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId222.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId223.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId224.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId225.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId226.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId227.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId228.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId229.msg" ":initialShadingGroup.gn" -na;
connectAttr "groupId245.msg" ":initialShadingGroup.gn" -na;
// End of house_snow.ma
