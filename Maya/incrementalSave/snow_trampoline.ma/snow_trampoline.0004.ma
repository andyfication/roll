//Maya ASCII 2016 scene
//Name: snow_trampoline.ma
//Last modified: Mon, Feb 13, 2017 05:03:18 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "890F292C-7A40-82F7-B175-159E0217E6CE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 18.01909286127156 37.059237520710525 3.1907978793594935 ;
	setAttr ".r" -type "double3" -68.138352729732588 -290.99999999996891 -8.8751091873687916e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "CDF65AD2-C646-436E-4E08-9D899C261AD9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 46.270873226178175;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.9337553058156707 -5.8840990382489355 -2.9837846995755495 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "B4507139-8F4D-52CE-E265-49BE798C6DA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "5FB03A9B-CF45-0725-C8CB-3F9288A7772E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 80.204382140779018;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "EC194BEF-0649-7E86-6889-B3AE2189827C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9CFEB074-C54D-F0E6-5573-32910634C192";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 43.915359887041596;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "F17E7F4C-F24E-04A6-B7B1-098657EA308A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.3214435090854 -5.8840990382489355 -2.9837846995755277 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "BD51643B-0C47-3248-955F-4290B5859188";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 71.380820536136042;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "planarTrimmedSurface1";
	rename -uid "678F8D49-7A46-B4F7-5A0E-E9A6C09D2C83";
createNode nurbsSurface -n "planarTrimmedSurfaceShape1" -p "planarTrimmedSurface1";
	rename -uid "ACDEBE1E-F543-6967-FA4B-B581EAA41153";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "planarTrimmedSurface2";
	rename -uid "AAE1AF1B-AA4D-215B-F706-2FBD91FAF685";
createNode nurbsSurface -n "planarTrimmedSurfaceShape2" -p "planarTrimmedSurface2";
	rename -uid "E464E624-024C-7929-B4F5-7F90377A127B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "planarTrimmedSurface3";
	rename -uid "1959C060-BD43-DF05-108A-3789FACCC815";
createNode nurbsSurface -n "planarTrimmedSurfaceShape3" -p "planarTrimmedSurface3";
	rename -uid "8921F572-3249-E1AC-AA58-E0BA6306107F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "curve1";
	rename -uid "D787C0BD-AD4E-BF10-B264-898890ABB262";
createNode nurbsCurve -n "curveShape1" -p "curve1";
	rename -uid "B808D3FA-2D48-6A3D-7200-33B3E10A2269";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		0 6.9665836600455675 12.961085879154533
		0 6.9519474739810416 12.283215501457114
		0 6.9226751018519419 10.927474746062192
		0 6.3645527400708977 9.0417205439104329
		0 5.5302901343890731 7.7310363471160208
		0 3.9818068496536916 6.3052106562053449
		0 2.4556859140350165 5.1537135127747398
		0 0.77667110825445929 4.1865414026651431
		0 -1.0907957187447908 2.9854057645400403
		0 -4.3845560489549342 1.3693014760323745
		0 -6.4506812615980236 0.091705011571878325
		0 -8.1127276775527495 -1.3472889459454169
		0 -8.7303986340814532 -2.8680333316572351
		0 -9.263659308507517 -5.4557088170328258
		0 -8.9851805111842022 -6.0957279457984139
		0 -8.8459411125225351 -6.4157375101812031
		;
createNode transform -n "pCube1";
	rename -uid "7479EABD-194E-37E6-C726-D4B47A4CE300";
	setAttr ".t" -type "double3" 0 6.9748495298193829 7.3310560806761078 ;
	setAttr ".s" -type "double3" 5.8861002675555181 0.22012785228015991 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "40529CAB-D14A-A0CE-3C5A-08A8B61B781A";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.49999997019767761 2 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2578 ".pt";
	setAttr ".pt[92]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[93]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[95]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[97]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[99]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[154]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[305]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[359]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[365]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[377]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[395]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[404]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[405]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[413]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[425]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[434]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[440]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[449]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[465]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[479]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[484]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[485]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[489]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[591]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[593]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[594]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[595]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[596]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[598]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[599]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[600]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[601]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[603]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[604]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[605]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[606]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[608]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[609]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[610]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[611]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[613]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[614]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[615]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[616]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[618]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[619]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[620]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[621]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[623]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[624]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[625]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[626]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[628]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[629]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[630]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[631]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[633]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[634]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[635]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[636]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[638]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[639]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[640]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[641]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[643]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[644]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[645]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[646]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[648]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[649]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[650]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[651]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[653]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[654]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[656]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[664]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[665]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[674]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[679]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[800]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[829]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[835]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[836]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[837]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[839]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[841]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[842]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[843]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[844]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[858]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[859]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[860]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[862]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[863]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[864]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[866]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[867]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[868]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[870]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[871]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[874]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[878]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[879]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[880]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[882]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[883]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[884]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[886]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[887]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[888]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[890]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[891]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[892]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[894]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[895]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[896]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[898]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[899]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[900]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[902]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[903]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[904]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[906]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[907]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[908]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[910]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[911]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[912]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[914]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[915]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[916]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[918]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[919]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[920]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[922]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[923]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[924]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[926]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[927]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[928]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[930]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[931]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[932]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[934]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[935]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[936]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[938]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[939]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[940]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[942]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[943]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[944]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[946]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[947]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[948]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[952]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[954]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[955]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[956]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[958]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[959]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[960]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[962]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[963]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[964]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[966]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[967]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[968]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[970]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[971]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[972]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[974]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[975]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[976]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[978]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[979]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[980]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[995]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1004]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1169]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1285]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1422]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1457]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1477]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1482]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1485]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1487]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1491]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1492]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1493]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1495]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1496]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1497]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1498]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1500]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1501]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1502]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1503]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1505]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1506]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1507]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1508]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1510]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1511]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1512]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1513]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1515]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1516]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1517]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1518]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1520]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1521]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1522]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1523]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1525]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1526]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1527]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1528]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1530]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1531]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1532]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1533]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1535]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1536]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1537]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1538]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1540]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1541]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1542]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1543]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1545]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1546]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1547]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1548]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1550]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1551]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1552]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1553]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1555]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1556]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1557]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1558]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1560]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1561]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1562]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1563]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1565]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1566]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1567]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1568]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1570]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1571]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1710]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1746]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1755]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1873]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1882]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1883]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1885]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1886]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1892]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1893]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1894]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1895]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1896]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1897]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1898]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1900]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1901]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1902]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1903]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1904]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1905]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1906]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1907]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1908]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1909]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1910]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1911]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1912]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1913]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1915]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1916]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1917]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1918]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1919]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1920]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1921]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1922]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1923]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1924]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1925]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1926]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1927]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1928]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1930]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1931]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1932]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1933]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1934]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1935]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1936]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1937]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1938]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1939]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1940]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1941]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1942]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1943]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1945]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1946]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1947]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1948]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1949]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1985]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1993]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1995]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1997]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2001]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2005]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2009]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2013]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2023]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2024]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2025]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2026]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2027]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2028]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2029]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2030]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2031]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2033]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2041]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2042]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2043]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2044]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2045]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2046]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2047]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2048]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2052]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2058]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2066]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2071]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2072]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2087]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2090]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2110]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2129]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2135]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2137]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2141]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2143]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2145]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2147]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2149]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2151]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2153]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2155]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2157]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2161]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2582]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2583]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2584]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2585]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2586]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2587]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2588]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2590]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2591]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2592]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2593]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2594]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2595]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2596]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2597]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2598]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2599]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2600]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2601]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2602]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2603]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2605]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2606]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2607]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2608]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2609]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2610]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2611]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2612]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2613]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2614]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2615]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2616]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2617]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2618]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2620]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2621]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2622]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2623]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2624]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2625]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2626]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2627]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2628]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2629]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2630]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2631]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2632]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2633]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2635]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2636]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2637]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2638]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2639]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2640]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2641]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2642]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2643]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2644]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2645]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2646]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2647]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2648]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2650]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2651]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2652]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2653]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2654]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2656]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2657]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2662]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2665]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2667]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2674]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2704]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2710]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2714]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2719]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2734]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2744]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2746]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2749]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2755]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2759]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2764]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2772]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2773]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2774]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2775]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2776]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2777]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2778]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2779]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2780]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2781]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2782]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2783]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2785]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2786]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2787]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2788]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2789]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2791]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2795]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2796]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2797]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2798]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2800]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2801]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2805]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2806]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2807]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2808]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2809]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2818]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2820]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2823]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2827]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2830]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2833]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2835]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2836]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2837]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2838]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2839]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2840]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2841]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2842]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2843]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2845]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2846]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2847]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2848]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2849]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2850]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2851]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2852]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2853]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2854]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2855]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2856]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2857]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2858]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2860]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2861]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2862]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2863]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2864]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2865]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2866]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2867]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2868]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2869]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2870]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2871]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2873]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2877]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2878]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2879]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2880]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2881]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3317]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3318]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3325]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3330]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3331]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3333]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3334]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3335]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3336]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3337]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3338]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3339]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3340]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3342]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3343]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3344]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3345]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3346]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3347]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3348]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3349]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3351]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3352]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3353]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3355]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3356]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3357]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3358]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3360]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3361]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3363]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3365]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3373]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3378]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3382]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3383]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3384]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3385]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3387]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3388]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3389]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3390]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3392]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3397]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3402]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3405]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3407]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3412]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3417]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3425]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3427]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3434]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3437]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3457]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3462]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3465]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3657]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3662]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3664]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3667]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3679]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3694]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3704]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3709]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3714]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3724]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3734]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3739]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3744]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3749]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3754]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3759]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3769]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3772]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3774]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3775]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3776]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3777]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3778]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3779]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3780]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3781]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3783]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3784]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3785]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3786]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3787]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3788]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3789]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3795]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3796]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3797]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3798]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3799]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3801]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3805]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3806]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3807]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3808]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3814]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3820]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3823]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3829]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3830]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3833]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4019]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4034]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4052]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4064]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4071]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4090]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4109]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4294]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4314]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4318]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4323]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4330]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4331]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4332]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4333]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4334]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4335]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4336]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4338]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4339]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4340]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4341]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4342]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4343]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4344]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4346]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4347]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4348]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4349]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4350]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4351]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4352]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4353]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4355]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4356]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4358]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4359]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4360]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4361]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4363]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4368]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4373]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4378]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4382]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4383]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4384]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4386]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4387]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4388]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4389]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4390]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4392]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4395]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4402]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4404]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4407]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4412]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4413]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4422]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4427]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4434]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4440]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4449]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4462]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4467]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4479]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4482]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4484]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4487]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4489]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4491]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4492]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4493]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4494]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4495]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4496]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4498]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4499]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4500]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4501]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4502]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4503]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4504]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4506]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4507]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4508]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4509]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4510]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4511]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4512]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4513]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4514]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4515]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4516]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4518]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4519]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4520]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4521]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4522]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4523]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4524]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4526]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4527]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4528]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4529]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4530]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4531]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4532]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4533]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4534]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4535]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4536]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4538]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4539]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4540]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4541]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4542]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4543]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4544]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4546]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4547]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4548]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4549]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4550]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4551]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4552]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4553]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4554]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4555]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4556]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4558]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4559]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4560]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4561]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4562]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4563]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4564]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4566]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4567]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4568]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4569]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4952]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4953]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4954]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4955]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4956]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4958]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4959]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4960]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4961]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4962]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4963]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4964]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4965]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4966]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4967]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4968]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4969]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4970]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4971]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4972]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4973]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4974]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4975]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4977]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4978]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4979]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4980]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4981]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4985]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4989]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4993]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4997]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5001]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5004]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5005]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5009]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5013]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5019]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5023]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5024]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5025]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5026]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5027]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5028]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5029]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5030]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5031]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5034]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5041]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5042]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5043]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5044]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5045]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5046]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5047]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5048]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5049]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5058]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5064]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5066]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5072]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5079]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5087]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5094]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5110]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5129]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5135]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5137]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5139]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5141]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5143]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5145]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5149]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5151]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5153]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5154]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5155]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5157]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5161]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5169]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5285]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5294]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5305]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5314]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5317]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5323]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5325]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5694]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5739]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5744]" -type "float3" 0 -2.2206724 0 ;
createNode transform -n "pCylinder1";
	rename -uid "4A74B9A7-F347-7CFC-D860-43B1347215A8";
	setAttr ".t" -type "double3" 1.9337552942071801 -0.057208167182503988 9.1550017696453612 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.8132921129123734 0.19475815342054875 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "AE4074FC-8B49-2E92-DB5A-B19FC144A315";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "595BDDFA-F248-0595-685E-4E9CF66B4469";
	setAttr ".t" -type "double3" -2.0269654550987832 -0.057208167182503988 9.1550017696453612 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.8132921129123734 0.19475815342054875 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "C42E3BB9-BF46-5DED-8619-63A75685839B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder3";
	rename -uid "DAD96CB3-EF4F-F20E-0CF9-F8978D87A2F7";
	setAttr ".t" -type "double3" 1.9337552942071801 -1.1685638241239915 3.1929022652345722 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.3098451179437944 0.19475815342054875 ;
createNode mesh -n "pCylinderShape3" -p "pCylinder3";
	rename -uid "17C4F54D-894D-1FE0-83C3-4383AA00A5E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder4";
	rename -uid "E54BC5A8-9C45-FE5F-2692-44AB5EF7FFA1";
	setAttr ".t" -type "double3" 1.9337552942071801 -6.0029091910474603 -2.9837847053797946 ;
	setAttr ".s" -type "double3" 0.19475815342054875 4.8715954096983323 0.19475815342054875 ;
createNode mesh -n "pCylinderShape4" -p "pCylinder4";
	rename -uid "3D2E3B5E-9E47-0B5E-6DF1-2180FB5A5CF8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder5";
	rename -uid "5EF9195B-F14C-FD54-8B2D-0A96F32078AB";
	setAttr ".t" -type "double3" -2.011450240889876 -6.0029091910474603 -2.9837847053797946 ;
	setAttr ".s" -type "double3" 0.19475815342054875 4.8715954096983323 0.19475815342054875 ;
createNode mesh -n "pCylinderShape5" -p "pCylinder5";
	rename -uid "77E79B07-6D4F-8EBD-5ADA-5296F7F4E976";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder6";
	rename -uid "9B0E0B89-6549-E9D2-7C42-3295F7103D1B";
	setAttr ".t" -type "double3" -2.011450240889876 -1.1685638241239915 3.1929022652345722 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.3098451179437944 0.19475815342054875 ;
createNode mesh -n "pCylinderShape6" -p "pCylinder6";
	rename -uid "97844B1C-B346-138F-2BAF-BD8E40EF3DFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "BCBE420E-8F4B-7A6E-DAEE-5E91212962D4";
	setAttr -s 2 ".lnk";
	setAttr -s 2 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "2FB8E638-1546-7676-1E6B-FDB6486B4A36";
createNode displayLayer -n "defaultLayer";
	rename -uid "85E6004B-7048-FA52-9008-638657AE40DD";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "441366CA-B844-9C0F-4EBD-DBB6F9F017A3";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "CAF88FAB-7746-DA90-AE5E-51849734B421";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C3645EE5-AA4A-89D8-B47A-B1BCD96FAAFF";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 632\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 632\n            -height 308\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 632\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 632\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 632\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 632\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 632\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 632\n            -height 308\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 632\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "2963DE28-BE44-280B-7E80-B8A509C3897D";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube1";
	rename -uid "525A47CA-EF47-6290-5C5F-1287EFFFE993";
	setAttr ".cuv" 1;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "6AEF3C34-8441-39AD-F26A-B19EBC9939FE";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 5.8861002675555181 0 0 0 0 0.22012785228015991 0 0 0 0 1 0
		 0 6.9748495298193829 13.716054312747051 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 6.9748497 13.216054 ;
	setAttr ".rs" 1462052984;
	setAttr ".d" 20;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.943050133777759 6.8647856036793033 13.216054312747051 ;
	setAttr ".cbx" -type "double3" 2.943050133777759 7.0849134559594624 13.216054312747051 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "DD7EB9C7-1C49-BA96-3B6D-E7A8B1D64653";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 5.8861002675555181 0 0 0 0 0.22012785228015991 0 0 0 0 1 0
		 0 6.9748495298193829 13.716054312747051 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -8.8459415 -6.4157376 ;
	setAttr ".rs" 2064043369;
	setAttr ".lt" -type "double3" 0 1.1102230246251565e-16 2.2209522923561109 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.943050133777759 -8.9505722817411844 -6.449888833004902 ;
	setAttr ".cbx" -type "double3" 2.943050133777759 -8.7413104207044086 -6.3815866784638864 ;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "A2B4152A-A644-1DB4-5240-2E87AD9B4177";
	setAttr ".ics" -type "componentList" 1 "f[0:89]";
createNode polyTweak -n "polyTweak1";
	rename -uid "3DA993E4-704D-756B-5C0B-A7899281E175";
	setAttr ".uopa" yes;
	setAttr -s 92 ".tk[0:91]" -type "float3"  0 14.65748405 1.71203077 0
		 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07
		 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 1.1920929e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07
		 0 0 1.1920929e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode polySubdFace -n "polySubdFace2";
	rename -uid "2BAF41DC-304A-0186-360D-7587A56CD58C";
	setAttr ".ics" -type "componentList" 1 "f[0:359]";
createNode polySubdFace -n "polySubdFace3";
	rename -uid "38DA677C-FA4C-4163-C1C0-678933AFAD3C";
	setAttr ".ics" -type "componentList" 1 "f[0:1439]";
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7CAACB1F-6643-E45B-FEDD-42BECEDE40FE";
	setAttr ".sa" 8;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 4 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 10 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polySubdFace3.out" "pCubeShape1.i";
connectAttr "polyCylinder1.out" "pCylinderShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "curveShape1.ws" "polyExtrudeFace1.ipc";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak1.out" "polySubdFace1.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak1.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polySubdFace3.ip";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "planarTrimmedSurfaceShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "planarTrimmedSurfaceShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "planarTrimmedSurfaceShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape3.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape4.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape5.iog" ":initialShadingGroup.dsm" -na;
connectAttr "pCylinderShape6.iog" ":initialShadingGroup.dsm" -na;
// End of snow_trampoline.ma
