//Maya ASCII 2016 scene
//Name: snow_trampoline.ma
//Last modified: Mon, Feb 13, 2017 05:22:44 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "890F292C-7A40-82F7-B175-159E0217E6CE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 22.946853257771448 34.140250806737669 14.987736685704839 ;
	setAttr ".r" -type "double3" -48.938352729888393 398.59999999988202 -4.0697010426539683e-15 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "CDF65AD2-C646-436E-4E08-9D899C261AD9";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 56.891818596230316;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.36788126672221988 -8.7563657805964166 -14.218126811169595 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "B4507139-8F4D-52CE-E265-49BE798C6DA9";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1049071086633 -0.9925675164835831 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "5FB03A9B-CF45-0725-C8CB-3F9288A7772E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 41.840222352480168;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "EC194BEF-0649-7E86-6889-B3AE2189827C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "9CFEB074-C54D-F0E6-5573-32910634C192";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 49.244838645714516;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "F17E7F4C-F24E-04A6-B7B1-098657EA308A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 101.87961627832311 -0.5592103275079614 3.0856085379370501 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "BD51643B-0C47-3248-955F-4290B5859188";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 24.532580646741106;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "planarTrimmedSurface1";
	rename -uid "678F8D49-7A46-B4F7-5A0E-E9A6C09D2C83";
createNode nurbsSurface -n "planarTrimmedSurfaceShape1" -p "planarTrimmedSurface1";
	rename -uid "ACDEBE1E-F543-6967-FA4B-B581EAA41153";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "planarTrimmedSurface2";
	rename -uid "AAE1AF1B-AA4D-215B-F706-2FBD91FAF685";
createNode nurbsSurface -n "planarTrimmedSurfaceShape2" -p "planarTrimmedSurface2";
	rename -uid "E464E624-024C-7929-B4F5-7F90377A127B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "planarTrimmedSurface3";
	rename -uid "1959C060-BD43-DF05-108A-3789FACCC815";
createNode nurbsSurface -n "planarTrimmedSurfaceShape3" -p "planarTrimmedSurface3";
	rename -uid "8921F572-3249-E1AC-AA58-E0BA6306107F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 2;
	setAttr ".dvv" 2;
	setAttr ".cpr" 4;
	setAttr ".cps" 16;
createNode transform -n "curve1";
	rename -uid "D787C0BD-AD4E-BF10-B264-898890ABB262";
createNode nurbsCurve -n "curveShape1" -p "curve1";
	rename -uid "B808D3FA-2D48-6A3D-7200-33B3E10A2269";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 13 0 no 3
		18 0 0 0 1 2 3 4 5 6 7 8 9 10 11 12 13 13 13
		16
		0 6.9665836600455675 12.961085879154533
		0 6.9519474739810416 12.283215501457114
		0 6.9226751018519419 10.927474746062192
		0 6.3645527400708977 9.0417205439104329
		0 5.5302901343890731 7.7310363471160208
		0 3.9818068496536916 6.3052106562053449
		0 2.4556859140350165 5.1537135127747398
		0 0.77667110825445929 4.1865414026651431
		0 -1.0907957187447908 2.9854057645400403
		0 -4.3845560489549342 1.3693014760323745
		0 -6.4506812615980236 0.091705011571878325
		0 -8.1127276775527495 -1.3472889459454169
		0 -8.7303986340814532 -2.8680333316572351
		0 -9.263659308507517 -5.4557088170328258
		0 -8.9851805111842022 -6.0957279457984139
		0 -8.8459411125225351 -6.4157375101812031
		;
createNode transform -n "pCube1";
	rename -uid "7479EABD-194E-37E6-C726-D4B47A4CE300";
	setAttr ".t" -type "double3" 0 6.9748495298193829 7.3310560806761078 ;
	setAttr ".s" -type "double3" 5.8861002675555181 0.22012785228015991 1 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "40529CAB-D14A-A0CE-3C5A-08A8B61B781A";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.1875 2.5000001192092896 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 2583 ".pt";
	setAttr ".pt[92]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[93]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[95]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[97]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[99]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[154]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[305]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[359]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[365]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[377]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[395]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[404]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[405]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[413]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[425]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[434]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[440]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[449]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[465]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[479]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[484]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[485]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[489]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[591]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[593]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[594]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[595]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[596]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[598]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[599]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[600]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[601]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[603]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[604]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[605]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[606]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[608]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[609]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[610]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[611]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[613]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[614]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[615]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[616]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[618]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[619]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[620]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[621]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[623]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[624]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[625]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[626]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[628]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[629]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[630]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[631]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[633]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[634]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[635]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[636]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[638]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[639]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[640]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[641]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[643]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[644]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[645]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[646]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[648]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[649]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[650]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[651]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[653]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[654]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[656]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[664]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[665]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[674]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[679]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[800]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[829]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[835]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[836]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[837]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[839]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[841]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[842]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[843]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[844]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[858]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[859]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[860]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[862]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[863]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[864]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[866]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[867]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[868]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[870]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[871]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[874]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[878]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[879]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[880]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[882]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[883]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[884]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[886]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[887]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[888]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[890]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[891]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[892]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[894]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[895]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[896]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[898]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[899]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[900]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[902]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[903]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[904]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[906]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[907]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[908]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[910]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[911]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[912]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[914]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[915]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[916]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[918]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[919]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[920]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[922]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[923]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[924]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[926]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[927]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[928]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[930]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[931]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[932]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[934]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[935]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[936]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[938]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[939]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[940]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[942]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[943]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[944]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[946]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[947]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[948]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[952]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[954]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[955]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[956]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[958]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[959]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[960]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[962]" -type "float3" -2.9802322e-08 -2.2206724 0 ;
	setAttr ".pt[963]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[964]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[966]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[967]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[968]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[970]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[971]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[972]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[974]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[975]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[976]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[978]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[979]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[980]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[995]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1004]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1169]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1285]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1422]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1457]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1477]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1482]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1485]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1487]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1491]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1492]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1493]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1495]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1496]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1497]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1498]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1500]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1501]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1502]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1503]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1505]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1506]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1507]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1508]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1510]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1511]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1512]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1513]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1515]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1516]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1517]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1518]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1520]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1521]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1522]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1523]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1525]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1526]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1527]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1528]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1530]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1531]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1532]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1533]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1535]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1536]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1537]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1538]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1540]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1541]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1542]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1543]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1545]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1546]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1547]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1548]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1550]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1551]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1552]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1553]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1555]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1556]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1557]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1558]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1560]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1561]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1562]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1563]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1565]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1566]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1567]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1568]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1570]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1571]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1710]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1746]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1755]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1873]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1882]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1883]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1885]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1886]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1892]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1893]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1894]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1895]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1896]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1897]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1898]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1900]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1901]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1902]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1903]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1904]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1905]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1906]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1907]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1908]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1909]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1910]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1911]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1912]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1913]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1915]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1916]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1917]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1918]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1919]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1920]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1921]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1922]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1923]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1924]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1925]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1926]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1927]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1928]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1930]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1931]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1932]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1933]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1934]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1935]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1936]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1937]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1938]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1939]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1940]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1941]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1942]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1943]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1945]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1946]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1947]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1948]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1949]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1985]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1993]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1995]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1997]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[1999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2001]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2005]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2009]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2013]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2023]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2024]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2025]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2026]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2027]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2028]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2029]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2030]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2031]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2033]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2041]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2042]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2043]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2044]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2045]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2046]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2047]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2048]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2052]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2058]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2066]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2071]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2072]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2087]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2090]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2110]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2129]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2135]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2137]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2141]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2143]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2145]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2147]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2149]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2151]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2153]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2155]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2157]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2161]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2166]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2181]" -type "float3" -2.9802322e-08 -2.2206724 0 ;
	setAttr ".pt[2182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2582]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2583]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2584]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2585]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2586]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2587]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2588]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2590]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2591]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2592]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2593]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2594]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2595]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2596]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2597]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2598]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2599]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2600]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2601]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2602]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2603]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2605]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2606]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2607]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2608]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2609]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2610]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2611]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2612]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2613]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2614]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2615]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2616]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2617]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2618]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2620]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2621]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2622]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2623]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2624]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2625]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2626]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2627]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2628]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2629]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2630]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2631]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2632]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2633]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2635]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2636]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2637]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2638]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2639]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2640]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2641]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2642]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2643]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2644]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2645]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2646]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2647]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2648]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2650]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2651]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2652]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2653]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2654]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2656]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2657]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2662]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2665]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2667]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2674]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2683]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2704]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2710]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2714]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2719]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2734]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2744]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2746]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2749]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2755]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2759]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2764]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2772]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2773]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2774]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2775]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2776]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2777]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2778]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2779]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2780]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2781]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2782]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2783]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2785]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2786]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2787]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2788]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2789]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2791]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2795]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2796]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2797]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2798]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2800]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2801]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2805]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2806]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2807]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2808]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2809]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2818]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2820]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2823]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2827]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2830]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2833]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2835]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2836]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2837]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2838]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2839]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2840]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2841]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2842]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2843]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2845]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2846]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2847]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2848]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2849]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2850]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2851]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2852]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2853]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2854]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2855]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2856]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2857]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2858]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2860]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2861]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2862]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2863]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2864]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2865]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2866]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2867]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2868]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2869]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2870]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2871]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2872]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2873]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2875]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2876]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2877]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2878]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2879]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2880]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[2881]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3185]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3317]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3318]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3325]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3330]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3331]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3333]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3334]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3335]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3336]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3337]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3338]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3339]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3340]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3342]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3343]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3344]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3345]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3346]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3347]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3348]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3349]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3351]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3352]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3353]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3355]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3356]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3357]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3358]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3360]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3361]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3363]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3365]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3373]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3378]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3382]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3383]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3384]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3385]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3387]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3388]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3389]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3390]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3392]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3397]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3402]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3405]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3407]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3412]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3417]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3425]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3427]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3433]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3434]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3437]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3445]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3457]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3462]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3465]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3655]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3657]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3658]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3659]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3660]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3661]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3662]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3663]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3664]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3666]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3667]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3668]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3669]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3670]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3671]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3672]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3673]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3675]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3676]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3677]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3678]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3679]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3680]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3681]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3682]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3684]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3685]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3686]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3687]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3688]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3689]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3694]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3697]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3704]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3709]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3711]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3712]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3713]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3714]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3715]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3716]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3717]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3718]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3720]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3721]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3722]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3723]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3724]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3725]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3726]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3734]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3739]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3744]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3745]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3747]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3748]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3749]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3750]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3751]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3752]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3753]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3754]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3756]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3757]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3758]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3759]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3760]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3761]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3762]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3763]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3765]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3766]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3767]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3768]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3769]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3770]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3771]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3772]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3774]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3775]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3776]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3777]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3778]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3779]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3780]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3781]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3783]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3784]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3785]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3786]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3787]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3788]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3789]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3790]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3792]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3793]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3794]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3795]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3796]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3797]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3798]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3799]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3801]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3802]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3803]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3804]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3805]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3806]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3807]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3808]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3810]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3811]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3812]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3813]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3814]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3815]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3816]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3817]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3819]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3820]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3821]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3822]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3823]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3824]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3825]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3826]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3828]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3829]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3830]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3831]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3832]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3833]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[3834]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4014]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4019]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4034]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4052]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4064]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4071]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4090]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4109]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4128]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4204]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4223]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4242]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4261]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4280]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4294]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4299]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4314]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4318]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4323]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4330]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4331]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4332]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4333]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4334]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4335]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4336]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4338]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4339]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4340]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4341]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4342]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4343]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4344]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4346]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4347]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4348]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4349]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4350]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4351]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4352]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4353]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4354]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4355]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4356]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4358]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4359]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4360]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4361]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4362]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4363]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4364]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4366]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4367]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4368]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4369]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4370]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4371]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4372]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4373]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4374]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4375]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4376]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4378]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4379]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4380]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4381]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4382]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4383]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4384]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4386]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4387]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4388]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4389]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4390]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4391]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4392]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4393]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4394]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4395]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4396]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4398]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4399]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4400]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4401]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4402]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4403]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4404]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4406]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4407]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4408]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4409]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4410]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4411]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4412]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4413]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4414]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4415]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4416]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4418]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4419]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4420]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4421]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4422]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4423]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4424]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4426]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4427]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4428]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4429]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4430]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4431]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4432]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4433]" -type "float3" -2.9802322e-08 -2.2206724 0 ;
	setAttr ".pt[4434]" -type "float3" -2.9802322e-08 -2.2206724 0 ;
	setAttr ".pt[4435]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4436]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4438]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4439]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4440]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4441]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4442]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4443]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4444]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4446]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4447]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4448]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4449]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4450]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4451]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4452]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4453]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4454]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4455]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4456]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4458]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4459]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4460]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4461]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4462]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4463]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4464]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4466]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4467]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4468]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4469]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4470]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4471]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4472]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4473]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4474]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4475]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4476]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4478]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4479]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4480]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4481]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4482]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4483]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4484]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4486]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4487]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4488]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4489]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4490]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4491]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4492]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4493]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4494]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4495]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4496]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4498]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4499]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4500]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4501]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4502]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4503]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4504]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4506]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4507]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4508]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4509]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4510]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4511]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4512]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4513]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4514]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4515]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4516]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4518]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4519]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4520]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4521]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4522]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4523]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4524]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4526]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4527]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4528]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4529]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4530]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4531]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4532]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4533]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4534]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4535]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4536]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4538]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4539]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4540]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4541]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4542]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4543]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4544]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4546]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4547]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4548]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4549]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4550]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4551]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4552]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4553]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4554]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4555]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4556]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4558]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4559]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4560]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4561]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4562]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4563]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4564]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4566]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4567]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4568]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4569]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4950]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4951]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4952]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4953]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4954]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4955]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4956]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4958]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4959]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4960]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4961]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4962]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4963]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4964]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4965]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4966]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4967]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4968]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4969]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4970]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4971]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4972]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4973]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4974]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4975]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4977]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4978]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4979]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4980]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4981]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4982]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4983]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4984]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4985]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4986]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4987]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4988]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4989]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4990]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4991]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4992]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4993]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4994]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4996]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4997]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4998]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[4999]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5000]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5001]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5002]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5003]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5004]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5005]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5006]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5007]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5008]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5009]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5010]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5011]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5012]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5013]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5015]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5016]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5017]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5018]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5019]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5020]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5021]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5022]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5023]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5024]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5025]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5026]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5027]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5028]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5029]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5030]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5031]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5032]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5034]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5035]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5036]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5037]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5038]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5039]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5040]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5041]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5042]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5043]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5044]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5045]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5046]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5047]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5048]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5049]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5050]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5051]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5053]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5054]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5055]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5056]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5057]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5058]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5059]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5060]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5061]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5062]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5063]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5064]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5065]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5066]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5067]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5068]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5069]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5070]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5072]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5073]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5074]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5075]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5076]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5077]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5078]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5079]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5080]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5081]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5082]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5083]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5084]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5085]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5086]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5087]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5088]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5089]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5091]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5092]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5093]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5094]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5095]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5096]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5097]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5098]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5099]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5100]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5101]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5102]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5103]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5104]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5105]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5106]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5107]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5108]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5110]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5111]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5112]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5113]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5114]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5115]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5116]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5117]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5118]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5119]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5120]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5121]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5122]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5123]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5124]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5125]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5126]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5127]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5129]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5130]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5131]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5132]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5133]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5134]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5135]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5136]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5137]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5138]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5139]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5140]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5141]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5142]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5143]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5144]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5145]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5146]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5148]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5149]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5150]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5151]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5152]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5153]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5154]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5155]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5156]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5157]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5158]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5159]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5160]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5161]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5162]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5163]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5164]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5165]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5167]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5168]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5169]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5170]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5171]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5172]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5173]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5174]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5175]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5176]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5177]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5178]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5179]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5180]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5181]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5182]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5183]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5184]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5186]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5187]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5188]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5189]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5190]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5191]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5192]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5193]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5194]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5195]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5196]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5197]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5198]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5199]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5200]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5201]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5202]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5203]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5205]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5206]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5207]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5208]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5209]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5210]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5211]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5212]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5213]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5214]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5215]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5216]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5217]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5218]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5219]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5220]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5221]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5222]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5224]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5225]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5226]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5227]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5228]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5229]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5230]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5231]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5232]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5233]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5234]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5235]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5236]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5237]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5238]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5239]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5240]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5241]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5243]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5244]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5245]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5246]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5247]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5248]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5249]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5250]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5251]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5252]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5253]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5254]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5255]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5256]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5257]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5258]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5259]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5260]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5262]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5263]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5264]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5265]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5266]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5267]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5268]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5269]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5270]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5271]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5272]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5273]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5274]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5275]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5276]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5277]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5278]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5279]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5281]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5282]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5283]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5284]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5285]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5286]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5287]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5288]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5289]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5290]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5291]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5292]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5293]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5294]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5295]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5296]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5297]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5298]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5300]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5301]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5302]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5303]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5304]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5305]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5306]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5307]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5308]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5309]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5310]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5311]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5312]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5313]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5314]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5315]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5316]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5317]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5319]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5320]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5321]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5322]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5323]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5324]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5325]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5326]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5327]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5328]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5329]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5690]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5691]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5692]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5693]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5694]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5695]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5696]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5698]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5699]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5700]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5701]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5702]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5703]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5705]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5706]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5707]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5708]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5727]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5728]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5729]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5730]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5731]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5732]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5733]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5735]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5736]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5737]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5738]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5739]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5740]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5741]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5742]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5743]" -type "float3" 0 -2.2206724 0 ;
	setAttr ".pt[5744]" -type "float3" 0 -2.2206724 0 ;
createNode transform -n "pCylinder1";
	rename -uid "4A74B9A7-F347-7CFC-D860-43B1347215A8";
	setAttr ".t" -type "double3" 1.9337552942071801 -0.057208167182503988 9.1550017696453612 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.8132921129123734 0.19475815342054875 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "AE4074FC-8B49-2E92-DB5A-B19FC144A315";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "595BDDFA-F248-0595-685E-4E9CF66B4469";
	setAttr ".t" -type "double3" -2.0269654550987832 -0.057208167182503988 9.1550017696453612 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.8132921129123734 0.19475815342054875 ;
createNode mesh -n "pCylinderShape2" -p "pCylinder2";
	rename -uid "C42E3BB9-BF46-5DED-8619-63A75685839B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder3";
	rename -uid "DAD96CB3-EF4F-F20E-0CF9-F8978D87A2F7";
	setAttr ".t" -type "double3" 1.9337552942071801 -1.1685638241239915 3.1929022652345722 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.3098451179437944 0.19475815342054875 ;
createNode mesh -n "pCylinderShape3" -p "pCylinder3";
	rename -uid "17C4F54D-894D-1FE0-83C3-4383AA00A5E7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder4";
	rename -uid "E54BC5A8-9C45-FE5F-2692-44AB5EF7FFA1";
	setAttr ".t" -type "double3" 1.9337552942071801 -6.0029091910474603 -2.9837847053797946 ;
	setAttr ".s" -type "double3" 0.19475815342054875 4.8715954096983323 0.19475815342054875 ;
createNode mesh -n "pCylinderShape4" -p "pCylinder4";
	rename -uid "3D2E3B5E-9E47-0B5E-6DF1-2180FB5A5CF8";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder5";
	rename -uid "5EF9195B-F14C-FD54-8B2D-0A96F32078AB";
	setAttr ".t" -type "double3" -2.011450240889876 -6.0029091910474603 -2.9837847053797946 ;
	setAttr ".s" -type "double3" 0.19475815342054875 4.8715954096983323 0.19475815342054875 ;
createNode mesh -n "pCylinderShape5" -p "pCylinder5";
	rename -uid "77E79B07-6D4F-8EBD-5ADA-5296F7F4E976";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder6";
	rename -uid "9B0E0B89-6549-E9D2-7C42-3295F7103D1B";
	setAttr ".t" -type "double3" -2.011450240889876 -1.1685638241239915 3.1929022652345722 ;
	setAttr ".s" -type "double3" 0.19475815342054875 9.3098451179437944 0.19475815342054875 ;
createNode mesh -n "pCylinderShape6" -p "pCylinder6";
	rename -uid "97844B1C-B346-138F-2BAF-BD8E40EF3DFB";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 36 ".uvst[0].uvsp[0:35]" -type "float2" 0.61048543 0.04576458
		 0.5 1.4901161e-08 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5
		 0.3125 0.61048543 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125
		 0.46875 0.3125 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125
		 0.375 0.68843985 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985
		 0.53125 0.68843985 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543
		 0.73326457 0.5 0.6875 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543
		 0.5 1 0.61048543 0.95423543 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 18 ".vt[0:17]"  0.70710671 -1 -0.70710671 0 -1 -0.99999988
		 -0.70710671 -1 -0.70710671 -0.99999988 -1 0 -0.70710671 -1 0.70710671 0 -1 0.99999994
		 0.70710677 -1 0.70710677 1 -1 0 0.70710671 1 -0.70710671 0 1 -0.99999988 -0.70710671 1 -0.70710671
		 -0.99999988 1 0 -0.70710671 1 0.70710671 0 1 0.99999994 0.70710677 1 0.70710677 1 1 0
		 0 -1 0 0 1 0;
	setAttr -s 40 ".ed[0:39]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 16 0 1 16 1 1 16 2 1 16 3 1 16 4 1 16 5 1 16 6 1
		 16 7 1 8 17 1 9 17 1 10 17 1 11 17 1 12 17 1 13 17 1 14 17 1 15 17 1;
	setAttr -s 24 -ch 80 ".fc[0:23]" -type "polyFaces" 
		f 4 0 17 -9 -17
		mu 0 4 8 9 18 17
		f 4 1 18 -10 -18
		mu 0 4 9 10 19 18
		f 4 2 19 -11 -19
		mu 0 4 10 11 20 19
		f 4 3 20 -12 -20
		mu 0 4 11 12 21 20
		f 4 4 21 -13 -21
		mu 0 4 12 13 22 21
		f 4 5 22 -14 -22
		mu 0 4 13 14 23 22
		f 4 6 23 -15 -23
		mu 0 4 14 15 24 23
		f 4 7 16 -16 -24
		mu 0 4 15 16 25 24
		f 3 -1 -25 25
		mu 0 3 1 0 34
		f 3 -2 -26 26
		mu 0 3 2 1 34
		f 3 -3 -27 27
		mu 0 3 3 2 34
		f 3 -4 -28 28
		mu 0 3 4 3 34
		f 3 -5 -29 29
		mu 0 3 5 4 34
		f 3 -6 -30 30
		mu 0 3 6 5 34
		f 3 -7 -31 31
		mu 0 3 7 6 34
		f 3 -8 -32 24
		mu 0 3 0 7 34
		f 3 8 33 -33
		mu 0 3 32 31 35
		f 3 9 34 -34
		mu 0 3 31 30 35
		f 3 10 35 -35
		mu 0 3 30 29 35
		f 3 11 36 -36
		mu 0 3 29 28 35
		f 3 12 37 -37
		mu 0 3 28 27 35
		f 3 13 38 -38
		mu 0 3 27 26 35
		f 3 14 39 -39
		mu 0 3 26 33 35
		f 3 15 32 -40
		mu 0 3 33 32 35;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube2";
	rename -uid "0360D175-4C42-1E77-7468-959C89E6E5B4";
	setAttr ".t" -type "double3" 1.8949690667315511 0 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "F52CFCEF-A841-4336-EA6B-968990924616";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube3";
	rename -uid "84C017C5-4447-FCE7-2C3E-8DAC33D8DFB4";
	setAttr ".t" -type "double3" 1.8949690667315511 5.7327591312699377 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "AB9DC513-DA4A-1048-7AD6-AC8748F78FE5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4";
	rename -uid "5BA751D2-6243-E07B-79F5-84A2974255F0";
	setAttr ".t" -type "double3" 1.8949690667315511 -6.4615237023016974 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape4" -p "pCube4";
	rename -uid "E6BF04CC-C04C-B4A3-5FC0-32A49B8F2C8D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "CC454ABE-6F49-4BC2-2783-C6B2E473B9BA";
	setAttr ".t" -type "double3" 1.8949690667315511 -3.2725453346441666 0.037885759905309069 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape5" -p "pCube5";
	rename -uid "7B9A8CFE-704C-4B28-DFD2-1EB8DB039C7D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube6";
	rename -uid "F7A7EC9C-5C4A-1605-50D1-6990D0C7583F";
	setAttr ".t" -type "double3" 1.8949690667315511 2.9869020440710035 1.1561452193881077 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 3.8173463513787711 ;
createNode mesh -n "pCubeShape6" -p "pCube6";
	rename -uid "93B8E124-4A4E-0284-767E-B8AE80D58433";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "196D5EA3-8847-EEC4-0704-91888C68A386";
	setAttr ".t" -type "double3" -1.9753427544649131 5.7327591312699377 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape7" -p "pCube7";
	rename -uid "D06347A2-4340-47E6-D7D4-27AF55AF78D7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8";
	rename -uid "B3E54BDC-E645-4671-6E7A-78B33CF5DBE1";
	setAttr ".t" -type "double3" -1.9753427544649131 0 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape8" -p "pCube8";
	rename -uid "36E94A11-EC47-63DB-0931-468126B6DE50";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "170F3FF9-1C4C-ABF8-88D9-CBB7D33A336B";
	setAttr ".t" -type "double3" -1.9753427544649131 -6.4615237023016974 6.2374927026187308 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape9" -p "pCube9";
	rename -uid "91F35738-9A46-9FF9-FDB8-A3A4BD32302B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "5A55F51A-D644-6255-35B5-8EA05452209A";
	setAttr ".t" -type "double3" -1.9753427544649131 -3.2725453346441666 0.037885759905309069 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 6.0274602572042779 ;
createNode mesh -n "pCubeShape10" -p "pCube10";
	rename -uid "55F7D33C-8E45-CFDE-1C7A-04B43820E316";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11";
	rename -uid "5B418B45-CC45-9C79-176B-0DB8D30E6CD6";
	setAttr ".t" -type "double3" -1.9753427544649131 2.9869020440710035 1.1561452193881077 ;
	setAttr ".s" -type "double3" 0.048656812225524766 0.52960018265967834 3.8173463513787711 ;
createNode mesh -n "pCubeShape11" -p "pCube11";
	rename -uid "9BB86EC7-1F44-E14E-19A8-5B9CA4F0022F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "BCBE420E-8F4B-7A6E-DAEE-5E91212962D4";
	setAttr -s 6 ".lnk";
	setAttr -s 6 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "2FB8E638-1546-7676-1E6B-FDB6486B4A36";
createNode displayLayer -n "defaultLayer";
	rename -uid "85E6004B-7048-FA52-9008-638657AE40DD";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "441366CA-B844-9C0F-4EBD-DBB6F9F017A3";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "CAF88FAB-7746-DA90-AE5E-51849734B421";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C3645EE5-AA4A-89D8-B47A-B1BCD96FAAFF";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 623\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 623\n            -height 308\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 622\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 622\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 623\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 623\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 622\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 622\n            -height 308\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 434\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 434\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "2963DE28-BE44-280B-7E80-B8A509C3897D";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube1";
	rename -uid "525A47CA-EF47-6290-5C5F-1287EFFFE993";
	setAttr ".cuv" 1;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "6AEF3C34-8441-39AD-F26A-B19EBC9939FE";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 5.8861002675555181 0 0 0 0 0.22012785228015991 0 0 0 0 1 0
		 0 6.9748495298193829 13.716054312747051 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 6.9748497 13.216054 ;
	setAttr ".rs" 1462052984;
	setAttr ".d" 20;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.943050133777759 6.8647856036793033 13.216054312747051 ;
	setAttr ".cbx" -type "double3" 2.943050133777759 7.0849134559594624 13.216054312747051 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "DD7EB9C7-1C49-BA96-3B6D-E7A8B1D64653";
	setAttr ".ics" -type "componentList" 1 "f[2]";
	setAttr ".ix" -type "matrix" 5.8861002675555181 0 0 0 0 0.22012785228015991 0 0 0 0 1 0
		 0 6.9748495298193829 13.716054312747051 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 -8.8459415 -6.4157376 ;
	setAttr ".rs" 2064043369;
	setAttr ".lt" -type "double3" 0 1.1102230246251565e-16 2.2209522923561109 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -2.943050133777759 -8.9505722817411844 -6.449888833004902 ;
	setAttr ".cbx" -type "double3" 2.943050133777759 -8.7413104207044086 -6.3815866784638864 ;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "A2B4152A-A644-1DB4-5240-2E87AD9B4177";
	setAttr ".ics" -type "componentList" 1 "f[0:89]";
createNode polyTweak -n "polyTweak1";
	rename -uid "3DA993E4-704D-756B-5C0B-A7899281E175";
	setAttr ".uopa" yes;
	setAttr -s 92 ".tk[0:91]" -type "float3"  0 14.65748405 1.71203077 0
		 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07
		 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 2.3841858e-07
		 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07 0 0 1.1920929e-07
		 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405
		 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 14.65748405 1.71203077
		 0 14.65748405 1.71203077 0 14.65748405 1.71203077 0 0 1.1920929e-07 0 0 1.1920929e-07
		 0 0 1.1920929e-07 0 0 2.3841858e-07 0 0 2.3841858e-07 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode polySubdFace -n "polySubdFace2";
	rename -uid "2BAF41DC-304A-0186-360D-7587A56CD58C";
	setAttr ".ics" -type "componentList" 1 "f[0:359]";
createNode polySubdFace -n "polySubdFace3";
	rename -uid "38DA677C-FA4C-4163-C1C0-678933AFAD3C";
	setAttr ".ics" -type "componentList" 1 "f[0:1439]";
createNode polyCylinder -n "polyCylinder1";
	rename -uid "7CAACB1F-6643-E45B-FEDD-42BECEDE40FE";
	setAttr ".sa" 8;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyCube -n "polyCube2";
	rename -uid "5CE8F3E1-E34C-7D51-C916-49BB917380CD";
	setAttr ".cuv" 1;
createNode lambert -n "snow_trampoline_main";
	rename -uid "21BA56CC-9C48-3D7F-EB8D-F7838937F9D6";
	setAttr ".c" -type "float3" 0.79833674 0.79833674 0.79833674 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "B63CF29C-724F-9718-53AA-68844EC77E3B";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 2 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "CA5C88D3-6F40-6787-352B-0E8C75C94DBB";
createNode lambert -n "snow_trampoline_details_side";
	rename -uid "C39098C1-7F44-ADB6-C112-D2BD20C22023";
	setAttr ".c" -type "float3" 0.1657435 0.1657435 0.1657435 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "C5B348E8-064A-D341-7736-9BAD166435E5";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "F399D195-2F47-00CF-F432-F2BE1EC14EDC";
createNode lambert -n "snow_tramplonine_details_centre";
	rename -uid "68ACBFCC-1C49-FA0C-56BA-1696FD07CE4F";
	setAttr ".c" -type "float3" 0.6631 0.3556 0.31200001 ;
createNode shadingEngine -n "lambert4SG";
	rename -uid "9025A3EF-A445-759E-36BD-DA9CF328D5C0";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
	rename -uid "25EDFBC0-6842-93B0-F309-25825FC858F8";
createNode lambert -n "snow_trampoline_structure";
	rename -uid "060B0D50-CC4F-1B39-91F0-4997699CB160";
	setAttr ".c" -type "float3" 0.5 0.21633524 0.155 ;
createNode shadingEngine -n "lambert5SG";
	rename -uid "A995BB4E-344E-DB83-2A14-EE8542318153";
	setAttr ".ihi" 0;
	setAttr -s 16 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
	rename -uid "861D3FF2-8844-B73E-27C7-22947B340EAE";
createNode groupId -n "groupId1";
	rename -uid "2C386806-2B43-B2D7-FF89-33A026CD9FEC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "0B55FDD8-064C-7025-911F-F5936FE9CF26";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 544 "f[0:1]" "f[3]" "f[6:8]" "f[10:18]" "f[20:24]" "f[46:48]" "f[50]" "f[52:58]" "f[60:64]" "f[90:96]" "f[98:101]" "f[108:115]" "f[118:123]" "f[126:133]" "f[136:138]" "f[141:146]" "f[150:165]" "f[167]" "f[228:236]" "f[238:243]" "f[246:253]" "f[256:258]" "f[261:266]" "f[270:285]" "f[287]" "f[348]" "f[350]" "f[354]" "f[356]" "f[361:362]" "f[364:365]" "f[368]" "f[370:371]" "f[379:380]" "f[382:383]" "f[385:386]" "f[391:392]" "f[394:395]" "f[398]" "f[400:401]" "f[403:404]" "f[407]" "f[409:410]" "f[412:413]" "f[415:416]" "f[418]" "f[421:422]" "f[424:425]" "f[427:428]" "f[430:431]" "f[433:434]" "f[437]" "f[499:500]" "f[502:503]" "f[505:506]" "f[511:512]" "f[514:515]" "f[517:518]" "f[520:521]" "f[523:524]" "f[527]" "f[529:530]" "f[532:533]" "f[535:536]" "f[538]" "f[541:542]" "f[544:545]" "f[547:548]" "f[550:551]" "f[553:554]" "f[557]" "f[620]" "f[626]" "f[630]" "f[632]" "f[634:636]" "f[638:639]" "f[641]" "f[643:645]" "f[647]" "f[653]" "f[657]" "f[659]" "f[661:663]" "f[665]" "f[684]" "f[686]" "f[688:690]" "f[692:693]" "f[695]" "f[697:699]" "f[701:702]" "f[704]" "f[707]" "f[710]" "f[715:717]" "f[719:720]" "f[722]" "f[724:726]" "f[728:729]" "f[731]" "f[740]" "f[742:744]" "f[746:747]" "f[749]" "f[751:753]" "f[755:756]" "f[758]" "f[764]" "f[767]" "f[769:771]" "f[773:774]" "f[776]" "f[778]" "f[780]" "f[783]" "f[785]" "f[787:789]" "f[791:792]" "f[794]" "f[796:798]" "f[800:801]" "f[810]" "f[812]" "f[814:816]" "f[818:819]" "f[821]" "f[823:825]" "f[827:828]" "f[830]" "f[832:834]" "f[836:837]" "f[839]" "f[841:843]" "f[845:846]" "f[848]" "f[851]" "f[854]" "f[860]" "f[1044]" "f[1046]" "f[1048:1050]" "f[1052:1053]" "f[1055]" "f[1057:1059]" "f[1061:1062]" "f[1064]" "f[1067:1068]" "f[1070]" "f[1075:1077]" "f[1080]" "f[1082]" "f[1084:1086]" "f[1088:1089]" "f[1091]" "f[1100]" "f[1102:1104]" "f[1106:1107]" "f[1109]" "f[1111:1113]" "f[1115:1116]" "f[1118]" "f[1124]" "f[1127]" "f[1129:1131]" "f[1133:1134]" "f[1136]" "f[1138]" "f[1140]" "f[1143]" "f[1145]" "f[1147:1149]" "f[1151:1152]" "f[1154]" "f[1156:1158]" "f[1160:1161]" "f[1170]" "f[1172]" "f[1174:1176]" "f[1178:1179]" "f[1181]" "f[1183:1185]" "f[1187:1188]" "f[1190]" "f[1192:1194]" "f[1196:1197]" "f[1199]" "f[1201:1203]" "f[1205:1206]" "f[1208]" "f[1211]" "f[1214]" "f[1220]" "f[1409]" "f[1427]" "f[1442]" "f[1445]" "f[1451]" "f[1460]" "f[1463]" "f[1466]" "f[1472]" "f[1475]" "f[1478]" "f[1481]" "f[1484]" "f[1487]" "f[1490]" "f[1493]" "f[1496]" "f[1502]" "f[1505]" "f[1508]" "f[1511]" "f[1514]" "f[1580]" "f[1583]" "f[1586]" "f[1592]" "f[1595]" "f[1598]" "f[1601]" "f[1604]" "f[1607]" "f[1610]" "f[1613]" "f[1616]" "f[1622]" "f[1625]" "f[1628]" "f[1631]" "f[1634]" "f[1710:1712]" "f[1715:1721]" "f[1724:1728]" "f[1734]" "f[1737:1739]" "f[1742:1745]" "f[1764:1766]" "f[1769:1775]" "f[1778:1784]" "f[1787]" "f[1796:1802]" "f[1805:1811]" "f[1820]" "f[1823:1829]" "f[1832:1838]" "f[1850:1856]" "f[1860:1861]" "f[1863:1865]" "f[1868:1874]" "f[1877:1882]" "f[1890:1892]" "f[1895:1901]" "f[1904:1910]" "f[1913:1919]" "f[1922:1928]" "f[1931:1932]" "f[1935]" "f[1941]" "f[2124:2126]" "f[2129:2135]" "f[2138:2144]" "f[2147:2150]" "f[2156:2162]" "f[2165:2171]" "f[2180]" "f[2183:2189]" "f[2192:2198]" "f[2210:2216]" "f[2220:2221]" "f[2223:2225]" "f[2228:2234]" "f[2237:2242]" "f[2250:2252]" "f[2255:2261]" "f[2264:2270]" "f[2273:2279]" "f[2282:2288]" "f[2291:2292]" "f[2294:2295]" "f[2301]" "f[2484]" "f[2490]" "f[2502]" "f[2508]" "f[2521:2528]" "f[2530:2537]" "f[2541:2542]" "f[2544]" "f[2548:2555]" "f[2575:2582]" "f[2584:2591]" "f[2593:2600]" "f[2611:2618]" "f[2620:2627]" "f[2630]" "f[2632:2636]" "f[2638:2645]" "f[2647:2654]" "f[2661:2663]" "f[2665:2672]" "f[2674:2681]" "f[2683:2690]" "f[2692]" "f[2694]" "f[2701:2708]" "f[2710:2717]" "f[2719:2726]" "f[2728:2735]" "f[2737:2744]" "f[2748:2749]" "f[2751]" "f[2935:2942]" "f[2944:2951]" "f[2953:2960]" "f[2962]" "f[2971:2978]" "f[2980:2984]" "f[2987]" "f[2989:2990]" "f[2992:2996]" "f[2998:3005]" "f[3007:3014]" "f[3021:3023]" "f[3025:3032]" "f[3034:3041]" "f[3043:3050]" "f[3052]" "f[3054]" "f[3061:3068]" "f[3070:3077]" "f[3079:3086]" "f[3088:3095]" "f[3097:3104]" "f[3108:3109]" "f[3111]" "f[3297:3298]" "f[3300]" "f[3315:3316]" "f[3318]" "f[3332]" "f[3334:3338]" "f[3340:3347]" "f[3350]" "f[3352:3356]" "f[3359]" "f[3361:3365]" "f[3367:3374]" "f[3377]" "f[3379:3383]" "f[3390:3391]" "f[3396:3397]" "f[3399]" "f[3408:3409]" "f[3413]" "f[3415:3419]" "f[3421:3428]" "f[3431]" "f[3433:3437]" "f[3494]" "f[3496:3500]" "f[3502:3509]" "f[3512]" "f[3514:3518]" "f[3521]" "f[3523:3527]" "f[3529:3536]" "f[3539]" "f[3541:3545]" "f[3548]" "f[3550:3554]" "f[3561:3563]" "f[3568:3572]" "f[3583:3590]" "f[3593]" "f[3595:3599]" "f[3602]" "f[3604:3608]" "f[3610:3617]" "f[3620]" "f[3622:3626]" "f[3629]" "f[3631:3635]" "f[3656]" "f[3658:3662]" "f[3664:3671]" "f[3674]" "f[3676:3680]" "f[3683]" "f[3685:3689]" "f[3691:3698]" "f[3701]" "f[3703:3707]" "f[3710]" "f[3712:3716]" "f[3723:3724]" "f[3730]" "f[3732]" "f[3739:3743]" "f[3745:3752]" "f[3755]" "f[3757:3761]" "f[3764]" "f[3766:3770]" "f[3772]" "f[3774]" "f[3791]" "f[3793:3797]" "f[3799:3806]" "f[3809]" "f[3811:3815]" "f[3818]" "f[3820:3824]" "f[3826:3833]" "f[3836]" "f[3838:3842]" "f[3872]" "f[3874:3878]" "f[3880:3887]" "f[3890]" "f[3892:3896]" "f[3899]" "f[3901:3905]" "f[3907:3914]" "f[3917]" "f[3919:3923]" "f[3926]" "f[3928:3932]" "f[3934:3941]" "f[3944]" "f[3946:3950]" "f[3953]" "f[3955:3959]" "f[3961:3968]" "f[3971]" "f[3973:3977]" "f[3980]" "f[3982:3986]" "f[3990:3991]" "f[3993:3994]" "f[4000]" "f[4002:4003]" "f[4011:4012]" "f[4017:4018]" "f[4020]" "f[4029:4030]" "f[4574]" "f[4576:4580]" "f[4582:4589]" "f[4592]" "f[4594:4598]" "f[4601]" "f[4603:4607]" "f[4609:4616]" "f[4619]" "f[4621:4625]" "f[4628]" "f[4630:4634]" "f[4637:4643]" "f[4646]" "f[4648:4652]" "f[4663:4670]" "f[4673]" "f[4676]" "f[4678:4679]" "f[4682]" "f[4684:4688]" "f[4690:4697]" "f[4700]" "f[4702:4706]" "f[4709]" "f[4711:4712]" "f[4714:4715]" "f[4736]" "f[4738:4742]" "f[4744:4751]" "f[4754]" "f[4756:4760]" "f[4763]" "f[4765:4769]" "f[4771:4778]" "f[4781]" "f[4783:4787]" "f[4790]" "f[4792:4796]" "f[4803:4804]" "f[4810]" "f[4812]" "f[4819:4823]" "f[4825:4832]" "f[4835]" "f[4837:4841]" "f[4844]" "f[4846:4850]" "f[4852:4854]" "f[4871]" "f[4873:4877]" "f[4879:4886]" "f[4889]" "f[4891:4895]" "f[4898]" "f[4900:4904]" "f[4906:4913]" "f[4916]" "f[4918:4922]" "f[4952]" "f[4954:4958]" "f[4960:4967]" "f[4970]" "f[4972:4976]" "f[4979]" "f[4981:4985]" "f[4987:4994]" "f[4997]" "f[4999:5003]" "f[5006]" "f[5008:5012]" "f[5014:5021]" "f[5024]" "f[5026:5030]" "f[5033]" "f[5035:5039]" "f[5041:5048]" "f[5051]" "f[5053:5057]" "f[5060]" "f[5062:5066]" "f[5070:5071]" "f[5073:5075]" "f[5078]" "f[5080:5084]" "f[5091:5092]" "f[5097:5098]" "f[5100]" "f[5109:5110]" "f[5658:5659]" "f[5664:5665]" "f[5667]" "f[5676:5677]" "f[5712:5713]" "f[5718:5719]" "f[5721]" "f[5730:5731]";
	setAttr ".irc" -type "componentList" 544 "f[2]" "f[4:5]" "f[9]" "f[19]" "f[25:45]" "f[49]" "f[51]" "f[59]" "f[65:89]" "f[97]" "f[102:107]" "f[116:117]" "f[124:125]" "f[134:135]" "f[139:140]" "f[147:149]" "f[166]" "f[168:227]" "f[237]" "f[244:245]" "f[254:255]" "f[259:260]" "f[267:269]" "f[286]" "f[288:347]" "f[349]" "f[351:353]" "f[355]" "f[357:360]" "f[363]" "f[366:367]" "f[369]" "f[372:378]" "f[381]" "f[384]" "f[387:390]" "f[393]" "f[396:397]" "f[399]" "f[402]" "f[405:406]" "f[408]" "f[411]" "f[414]" "f[417]" "f[419:420]" "f[423]" "f[426]" "f[429]" "f[432]" "f[435:436]" "f[438:498]" "f[501]" "f[504]" "f[507:510]" "f[513]" "f[516]" "f[519]" "f[522]" "f[525:526]" "f[528]" "f[531]" "f[534]" "f[537]" "f[539:540]" "f[543]" "f[546]" "f[549]" "f[552]" "f[555:556]" "f[558:619]" "f[621:625]" "f[627:629]" "f[631]" "f[633]" "f[637]" "f[640]" "f[642]" "f[646]" "f[648:652]" "f[654:656]" "f[658]" "f[660]" "f[664]" "f[666:683]" "f[685]" "f[687]" "f[691]" "f[694]" "f[696]" "f[700]" "f[703]" "f[705:706]" "f[708:709]" "f[711:714]" "f[718]" "f[721]" "f[723]" "f[727]" "f[730]" "f[732:739]" "f[741]" "f[745]" "f[748]" "f[750]" "f[754]" "f[757]" "f[759:763]" "f[765:766]" "f[768]" "f[772]" "f[775]" "f[777]" "f[779]" "f[781:782]" "f[784]" "f[786]" "f[790]" "f[793]" "f[795]" "f[799]" "f[802:809]" "f[811]" "f[813]" "f[817]" "f[820]" "f[822]" "f[826]" "f[829]" "f[831]" "f[835]" "f[838]" "f[840]" "f[844]" "f[847]" "f[849:850]" "f[852:853]" "f[855:859]" "f[861:1043]" "f[1045]" "f[1047]" "f[1051]" "f[1054]" "f[1056]" "f[1060]" "f[1063]" "f[1065:1066]" "f[1069]" "f[1071:1074]" "f[1078:1079]" "f[1081]" "f[1083]" "f[1087]" "f[1090]" "f[1092:1099]" "f[1101]" "f[1105]" "f[1108]" "f[1110]" "f[1114]" "f[1117]" "f[1119:1123]" "f[1125:1126]" "f[1128]" "f[1132]" "f[1135]" "f[1137]" "f[1139]" "f[1141:1142]" "f[1144]" "f[1146]" "f[1150]" "f[1153]" "f[1155]" "f[1159]" "f[1162:1169]" "f[1171]" "f[1173]" "f[1177]" "f[1180]" "f[1182]" "f[1186]" "f[1189]" "f[1191]" "f[1195]" "f[1198]" "f[1200]" "f[1204]" "f[1207]" "f[1209:1210]" "f[1212:1213]" "f[1215:1219]" "f[1221:1408]" "f[1410:1426]" "f[1428:1441]" "f[1443:1444]" "f[1446:1450]" "f[1452:1459]" "f[1461:1462]" "f[1464:1465]" "f[1467:1471]" "f[1473:1474]" "f[1476:1477]" "f[1479:1480]" "f[1482:1483]" "f[1485:1486]" "f[1488:1489]" "f[1491:1492]" "f[1494:1495]" "f[1497:1501]" "f[1503:1504]" "f[1506:1507]" "f[1509:1510]" "f[1512:1513]" "f[1515:1579]" "f[1581:1582]" "f[1584:1585]" "f[1587:1591]" "f[1593:1594]" "f[1596:1597]" "f[1599:1600]" "f[1602:1603]" "f[1605:1606]" "f[1608:1609]" "f[1611:1612]" "f[1614:1615]" "f[1617:1621]" "f[1623:1624]" "f[1626:1627]" "f[1629:1630]" "f[1632:1633]" "f[1635:1709]" "f[1713:1714]" "f[1722:1723]" "f[1729:1733]" "f[1735:1736]" "f[1740:1741]" "f[1746:1763]" "f[1767:1768]" "f[1776:1777]" "f[1785:1786]" "f[1788:1795]" "f[1803:1804]" "f[1812:1819]" "f[1821:1822]" "f[1830:1831]" "f[1839:1849]" "f[1857:1859]" "f[1862]" "f[1866:1867]" "f[1875:1876]" "f[1883:1889]" "f[1893:1894]" "f[1902:1903]" "f[1911:1912]" "f[1920:1921]" "f[1929:1930]" "f[1933:1934]" "f[1936:1940]" "f[1942:2123]" "f[2127:2128]" "f[2136:2137]" "f[2145:2146]" "f[2151:2155]" "f[2163:2164]" "f[2172:2179]" "f[2181:2182]" "f[2190:2191]" "f[2199:2209]" "f[2217:2219]" "f[2222]" "f[2226:2227]" "f[2235:2236]" "f[2243:2249]" "f[2253:2254]" "f[2262:2263]" "f[2271:2272]" "f[2280:2281]" "f[2289:2290]" "f[2293]" "f[2296:2300]" "f[2302:2483]" "f[2485:2489]" "f[2491:2501]" "f[2503:2507]" "f[2509:2520]" "f[2529]" "f[2538:2540]" "f[2543]" "f[2545:2547]" "f[2556:2574]" "f[2583]" "f[2592]" "f[2601:2610]" "f[2619]" "f[2628:2629]" "f[2631]" "f[2637]" "f[2646]" "f[2655:2660]" "f[2664]" "f[2673]" "f[2682]" "f[2691]" "f[2693]" "f[2695:2700]" "f[2709]" "f[2718]" "f[2727]" "f[2736]" "f[2745:2747]" "f[2750]" "f[2752:2934]" "f[2943]" "f[2952]" "f[2961]" "f[2963:2970]" "f[2979]" "f[2985:2986]" "f[2988]" "f[2991]" "f[2997]" "f[3006]" "f[3015:3020]" "f[3024]" "f[3033]" "f[3042]" "f[3051]" "f[3053]" "f[3055:3060]" "f[3069]" "f[3078]" "f[3087]" "f[3096]" "f[3105:3107]" "f[3110]" "f[3112:3296]" "f[3299]" "f[3301:3314]" "f[3317]" "f[3319:3331]" "f[3333]" "f[3339]" "f[3348:3349]" "f[3351]" "f[3357:3358]" "f[3360]" "f[3366]" "f[3375:3376]" "f[3378]" "f[3384:3389]" "f[3392:3395]" "f[3398]" "f[3400:3407]" "f[3410:3412]" "f[3414]" "f[3420]" "f[3429:3430]" "f[3432]" "f[3438:3493]" "f[3495]" "f[3501]" "f[3510:3511]" "f[3513]" "f[3519:3520]" "f[3522]" "f[3528]" "f[3537:3538]" "f[3540]" "f[3546:3547]" "f[3549]" "f[3555:3560]" "f[3564:3567]" "f[3573:3582]" "f[3591:3592]" "f[3594]" "f[3600:3601]" "f[3603]" "f[3609]" "f[3618:3619]" "f[3621]" "f[3627:3628]" "f[3630]" "f[3636:3655]" "f[3657]" "f[3663]" "f[3672:3673]" "f[3675]" "f[3681:3682]" "f[3684]" "f[3690]" "f[3699:3700]" "f[3702]" "f[3708:3709]" "f[3711]" "f[3717:3722]" "f[3725:3729]" "f[3731]" "f[3733:3738]" "f[3744]" "f[3753:3754]" "f[3756]" "f[3762:3763]" "f[3765]" "f[3771]" "f[3773]" "f[3775:3790]" "f[3792]" "f[3798]" "f[3807:3808]" "f[3810]" "f[3816:3817]" "f[3819]" "f[3825]" "f[3834:3835]" "f[3837]" "f[3843:3871]" "f[3873]" "f[3879]" "f[3888:3889]" "f[3891]" "f[3897:3898]" "f[3900]" "f[3906]" "f[3915:3916]" "f[3918]" "f[3924:3925]" "f[3927]" "f[3933]" "f[3942:3943]" "f[3945]" "f[3951:3952]" "f[3954]" "f[3960]" "f[3969:3970]" "f[3972]" "f[3978:3979]" "f[3981]" "f[3987:3989]" "f[3992]" "f[3995:3999]" "f[4001]" "f[4004:4010]" "f[4013:4016]" "f[4019]" "f[4021:4028]" "f[4031:4573]" "f[4575]" "f[4581]" "f[4590:4591]" "f[4593]" "f[4599:4600]" "f[4602]" "f[4608]" "f[4617:4618]" "f[4620]" "f[4626:4627]" "f[4629]" "f[4635:4636]" "f[4644:4645]" "f[4647]" "f[4653:4662]" "f[4671:4672]" "f[4674:4675]" "f[4677]" "f[4680:4681]" "f[4683]" "f[4689]" "f[4698:4699]" "f[4701]" "f[4707:4708]" "f[4710]" "f[4713]" "f[4716:4735]" "f[4737]" "f[4743]" "f[4752:4753]" "f[4755]" "f[4761:4762]" "f[4764]" "f[4770]" "f[4779:4780]" "f[4782]" "f[4788:4789]" "f[4791]" "f[4797:4802]" "f[4805:4809]" "f[4811]" "f[4813:4818]" "f[4824]" "f[4833:4834]" "f[4836]" "f[4842:4843]" "f[4845]" "f[4851]" "f[4855:4870]" "f[4872]" "f[4878]" "f[4887:4888]" "f[4890]" "f[4896:4897]" "f[4899]" "f[4905]" "f[4914:4915]" "f[4917]" "f[4923:4951]" "f[4953]" "f[4959]" "f[4968:4969]" "f[4971]" "f[4977:4978]" "f[4980]" "f[4986]" "f[4995:4996]" "f[4998]" "f[5004:5005]" "f[5007]" "f[5013]" "f[5022:5023]" "f[5025]" "f[5031:5032]" "f[5034]" "f[5040]" "f[5049:5050]" "f[5052]" "f[5058:5059]" "f[5061]" "f[5067:5069]" "f[5072]" "f[5076:5077]" "f[5079]" "f[5085:5090]" "f[5093:5096]" "f[5099]" "f[5101:5108]" "f[5111:5657]" "f[5660:5663]" "f[5666]" "f[5668:5675]" "f[5678:5711]" "f[5714:5717]" "f[5720]" "f[5722:5729]" "f[5732:5759]";
createNode groupId -n "groupId2";
	rename -uid "26216646-BB49-D920-5FD7-F4B4EBC03000";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "ACA84502-9E43-3958-8442-3386B6493824";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "61FEB270-1244-DF3C-7940-77AED5694F63";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 565 "f[2]" "f[4:5]" "f[25:45]" "f[65:89]" "f[97]" "f[102:107]" "f[166]" "f[168:227]" "f[286]" "f[288:347]" "f[349]" "f[351:353]" "f[355]" "f[357:360]" "f[363]" "f[366:367]" "f[369]" "f[372:378]" "f[381]" "f[384]" "f[387]" "f[390]" "f[393]" "f[396]" "f[399]" "f[402]" "f[405]" "f[408]" "f[411]" "f[414]" "f[417]" "f[420]" "f[423]" "f[426]" "f[429]" "f[432]" "f[435:436]" "f[438:498]" "f[501]" "f[504]" "f[507]" "f[510]" "f[513]" "f[516]" "f[519]" "f[522]" "f[525]" "f[528]" "f[531]" "f[534]" "f[537]" "f[540]" "f[543]" "f[546]" "f[549]" "f[552]" "f[555:556]" "f[558:619]" "f[621:625]" "f[627:629]" "f[631]" "f[633]" "f[637]" "f[640]" "f[642]" "f[646]" "f[648:652]" "f[654:656]" "f[658]" "f[660]" "f[664]" "f[666:683]" "f[685]" "f[687]" "f[691]" "f[694]" "f[696]" "f[700]" "f[703]" "f[705]" "f[709]" "f[712]" "f[714]" "f[718]" "f[721]" "f[723]" "f[727]" "f[730]" "f[732]" "f[736]" "f[739]" "f[741]" "f[745]" "f[748]" "f[750]" "f[754]" "f[757]" "f[759]" "f[763]" "f[766]" "f[768]" "f[772]" "f[775]" "f[777]" "f[781]" "f[784]" "f[786]" "f[790]" "f[793]" "f[795]" "f[799]" "f[802]" "f[804]" "f[808]" "f[811]" "f[813]" "f[817]" "f[820]" "f[822]" "f[826]" "f[829]" "f[831]" "f[835]" "f[838]" "f[840]" "f[844]" "f[847]" "f[849:850]" "f[852:853]" "f[855:859]" "f[861:1043]" "f[1045]" "f[1047]" "f[1051]" "f[1054]" "f[1056]" "f[1060]" "f[1063]" "f[1065]" "f[1069]" "f[1072]" "f[1074]" "f[1078]" "f[1081]" "f[1083]" "f[1087]" "f[1090]" "f[1092]" "f[1096]" "f[1099]" "f[1101]" "f[1105]" "f[1108]" "f[1110]" "f[1114]" "f[1117]" "f[1119]" "f[1123]" "f[1126]" "f[1128]" "f[1132]" "f[1135]" "f[1137]" "f[1141]" "f[1144]" "f[1146]" "f[1150]" "f[1153]" "f[1155]" "f[1159]" "f[1162]" "f[1164]" "f[1168]" "f[1171]" "f[1173]" "f[1177]" "f[1180]" "f[1182]" "f[1186]" "f[1189]" "f[1191]" "f[1195]" "f[1198]" "f[1200]" "f[1204]" "f[1207]" "f[1209:1210]" "f[1212:1213]" "f[1215:1219]" "f[1221:1408]" "f[1410:1426]" "f[1428:1441]" "f[1443:1444]" "f[1446:1450]" "f[1452:1459]" "f[1461:1462]" "f[1464:1465]" "f[1467:1468]" "f[1470:1471]" "f[1473:1474]" "f[1476:1477]" "f[1479:1480]" "f[1482:1483]" "f[1485:1486]" "f[1488:1489]" "f[1491:1492]" "f[1494:1495]" "f[1497:1498]" "f[1500:1501]" "f[1503:1504]" "f[1506:1507]" "f[1509:1510]" "f[1512:1513]" "f[1515:1579]" "f[1581:1582]" "f[1584:1585]" "f[1587:1588]" "f[1590:1591]" "f[1593:1594]" "f[1596:1597]" "f[1599:1600]" "f[1602:1603]" "f[1605:1606]" "f[1608:1609]" "f[1611:1612]" "f[1614:1615]" "f[1617:1618]" "f[1620:1621]" "f[1623:1624]" "f[1626:1627]" "f[1629:1630]" "f[1632:1633]" "f[1635:1709]" "f[1713:1714]" "f[1722:1723]" "f[1729:1733]" "f[1735:1736]" "f[1740:1741]" "f[1746:1763]" "f[1767:1768]" "f[1776:1777]" "f[1785:1786]" "f[1794:1795]" "f[1803:1804]" "f[1812:1813]" "f[1821:1822]" "f[1830:1831]" "f[1839:1840]" "f[1848:1849]" "f[1857:1858]" "f[1866:1867]" "f[1875:1876]" "f[1884:1885]" "f[1893:1894]" "f[1902:1903]" "f[1911:1912]" "f[1920:1921]" "f[1929:1930]" "f[1933:1934]" "f[1936:1940]" "f[1942:2123]" "f[2127:2128]" "f[2136:2137]" "f[2145:2146]" "f[2154:2155]" "f[2163:2164]" "f[2172:2173]" "f[2181:2182]" "f[2190:2191]" "f[2199:2200]" "f[2208:2209]" "f[2217:2218]" "f[2226:2227]" "f[2235:2236]" "f[2244:2245]" "f[2253:2254]" "f[2262:2263]" "f[2271:2272]" "f[2280:2281]" "f[2289:2290]" "f[2293]" "f[2296:2300]" "f[2302:2483]" "f[2485:2489]" "f[2491:2501]" "f[2503:2507]" "f[2509:2520]" "f[2529]" "f[2538:2540]" "f[2543]" "f[2545:2547]" "f[2556:2574]" "f[2583]" "f[2592]" "f[2601]" "f[2610]" "f[2619]" "f[2628]" "f[2637]" "f[2646]" "f[2655]" "f[2664]" "f[2673]" "f[2682]" "f[2691]" "f[2700]" "f[2709]" "f[2718]" "f[2727]" "f[2736]" "f[2745:2747]" "f[2750]" "f[2752:2934]" "f[2943]" "f[2952]" "f[2961]" "f[2970]" "f[2979]" "f[2988]" "f[2997]" "f[3006]" "f[3015]" "f[3024]" "f[3033]" "f[3042]" "f[3051]" "f[3060]" "f[3069]" "f[3078]" "f[3087]" "f[3096]" "f[3105:3107]" "f[3110]" "f[3112:3296]" "f[3299]" "f[3301:3314]" "f[3317]" "f[3319:3331]" "f[3333]" "f[3339]" "f[3348:3349]" "f[3351]" "f[3357:3358]" "f[3360]" "f[3366]" "f[3375:3376]" "f[3378]" "f[3384:3389]" "f[3392:3395]" "f[3398]" "f[3400:3407]" "f[3410:3412]" "f[3414]" "f[3420]" "f[3429:3430]" "f[3432]" "f[3438:3493]" "f[3495]" "f[3501]" "f[3510:3511]" "f[3513]" "f[3519:3520]" "f[3522]" "f[3528]" "f[3537:3538]" "f[3540]" "f[3546:3547]" "f[3549]" "f[3555]" "f[3564:3565]" "f[3567]" "f[3573:3574]" "f[3576]" "f[3582]" "f[3591:3592]" "f[3594]" "f[3600:3601]" "f[3603]" "f[3609]" "f[3618:3619]" "f[3621]" "f[3627:3628]" "f[3630]" "f[3636]" "f[3645:3646]" "f[3648]" "f[3654:3655]" "f[3657]" "f[3663]" "f[3672:3673]" "f[3675]" "f[3681:3682]" "f[3684]" "f[3690]" "f[3699:3700]" "f[3702]" "f[3708:3709]" "f[3711]" "f[3717]" "f[3726:3727]" "f[3729]" "f[3735:3736]" "f[3738]" "f[3744]" "f[3753:3754]" "f[3756]" "f[3762:3763]" "f[3765]" "f[3771]" "f[3780:3781]" "f[3783]" "f[3789:3790]" "f[3792]" "f[3798]" "f[3807:3808]" "f[3810]" "f[3816:3817]" "f[3819]" "f[3825]" "f[3834:3835]" "f[3837]" "f[3843:3844]" "f[3846]" "f[3852]" "f[3861:3862]" "f[3864]" "f[3870:3871]" "f[3873]" "f[3879]" "f[3888:3889]" "f[3891]" "f[3897:3898]" "f[3900]" "f[3906]" "f[3915:3916]" "f[3918]" "f[3924:3925]" "f[3927]" "f[3933]" "f[3942:3943]" "f[3945]" "f[3951:3952]" "f[3954]" "f[3960]" "f[3969:3970]" "f[3972]" "f[3978:3979]" "f[3981]" "f[3987:3989]" "f[3992]" "f[3995:3999]" "f[4001]" "f[4004:4010]" "f[4013:4016]" "f[4019]" "f[4021:4028]" "f[4031:4573]" "f[4575]" "f[4581]" "f[4590:4591]" "f[4593]" "f[4599:4600]" "f[4602]" "f[4608]" "f[4617:4618]" "f[4620]" "f[4626:4627]" "f[4629]" "f[4635]" "f[4644:4645]" "f[4647]" "f[4653:4654]" "f[4656]" "f[4662]" "f[4671:4672]" "f[4674]" "f[4680:4681]" "f[4683]" "f[4689]" "f[4698:4699]" "f[4701]" "f[4707:4708]" "f[4710]" "f[4716]" "f[4725:4726]" "f[4728]" "f[4734:4735]" "f[4737]" "f[4743]" "f[4752:4753]" "f[4755]" "f[4761:4762]" "f[4764]" "f[4770]" "f[4779:4780]" "f[4782]" "f[4788:4789]" "f[4791]" "f[4797]" "f[4806:4807]" "f[4809]" "f[4815:4816]" "f[4818]" "f[4824]" "f[4833:4834]" "f[4836]" "f[4842:4843]" "f[4845]" "f[4851]" "f[4860:4861]" "f[4863]" "f[4869:4870]" "f[4872]" "f[4878]" "f[4887:4888]" "f[4890]" "f[4896:4897]" "f[4899]" "f[4905]" "f[4914:4915]" "f[4917]" "f[4923:4924]" "f[4926]" "f[4932]" "f[4941:4942]" "f[4944]" "f[4950:4951]" "f[4953]" "f[4959]" "f[4968:4969]" "f[4971]" "f[4977:4978]" "f[4980]" "f[4986]" "f[4995:4996]" "f[4998]" "f[5004:5005]" "f[5007]" "f[5013]" "f[5022:5023]" "f[5025]" "f[5031:5032]" "f[5034]" "f[5040]" "f[5049:5050]" "f[5052]" "f[5058:5059]" "f[5061]" "f[5067:5069]" "f[5072]" "f[5076:5077]" "f[5079]" "f[5085:5090]" "f[5093:5096]" "f[5099]" "f[5101:5108]" "f[5111:5657]" "f[5660:5663]" "f[5666]" "f[5668:5675]" "f[5678:5711]" "f[5714:5717]" "f[5720]" "f[5722:5729]" "f[5732:5759]";
	setAttr ".irc" -type "componentList" 66 "f[96]" "f[98]" "f[164:165]" "f[167]" "f[285]" "f[287]" "f[348]" "f[350]" "f[354]" "f[356]" "f[368]" "f[437]" "f[557]" "f[620]" "f[626]" "f[653]" "f[851]" "f[860]" "f[1220]" "f[1409]" "f[1427]" "f[1728]" "f[1734]" "f[1932]" "f[1935]" "f[1941]" "f[2292]" "f[2295]" "f[2301]" "f[2484]" "f[2490]" "f[2502]" "f[2508]" "f[2541:2542]" "f[2544]" "f[2748:2749]" "f[2751]" "f[3108:3109]" "f[3111]" "f[3297:3298]" "f[3300]" "f[3315:3316]" "f[3318]" "f[3390:3391]" "f[3396:3397]" "f[3399]" "f[3408:3409]" "f[3990:3991]" "f[4003]" "f[4011:4012]" "f[4017:4018]" "f[4020]" "f[4029:4030]" "f[5070]" "f[5091:5092]" "f[5097:5098]" "f[5100]" "f[5109:5110]" "f[5658:5659]" "f[5664:5665]" "f[5667]" "f[5676:5677]" "f[5712:5713]" "f[5718:5719]" "f[5721]" "f[5730:5731]";
createNode groupId -n "groupId4";
	rename -uid "FF344F1D-C049-9A08-DCE0-93983D4543AB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "D1AF0989-0444-0398-5622-419AC221C971";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 123 "f[9]" "f[19]" "f[49]" "f[51]" "f[59]" "f[116:117]" "f[124:125]" "f[134:135]" "f[139:140]" "f[147:149]" "f[237]" "f[244:245]" "f[254:255]" "f[259:260]" "f[267:269]" "f[388:389]" "f[397]" "f[406]" "f[419]" "f[508:509]" "f[526]" "f[539]" "f[706]" "f[708]" "f[711]" "f[713]" "f[733:735]" "f[737:738]" "f[760:762]" "f[765]" "f[779]" "f[782]" "f[803]" "f[805:807]" "f[809]" "f[1066]" "f[1071]" "f[1073]" "f[1079]" "f[1093:1095]" "f[1097:1098]" "f[1120:1122]" "f[1125]" "f[1139]" "f[1142]" "f[1163]" "f[1165:1167]" "f[1169]" "f[1469]" "f[1499]" "f[1589]" "f[1619]" "f[1788:1793]" "f[1814:1819]" "f[1841:1847]" "f[1859]" "f[1862]" "f[1883]" "f[1886:1889]" "f[2151:2153]" "f[2174:2179]" "f[2201:2207]" "f[2219]" "f[2222]" "f[2243]" "f[2246:2249]" "f[2602:2609]" "f[2629]" "f[2631]" "f[2656:2660]" "f[2693]" "f[2695:2699]" "f[2963:2969]" "f[2985:2986]" "f[2991]" "f[3016:3020]" "f[3053]" "f[3055:3059]" "f[3556:3560]" "f[3566]" "f[3575]" "f[3577:3581]" "f[3637:3644]" "f[3647]" "f[3649:3653]" "f[3718:3722]" "f[3725]" "f[3728]" "f[3731]" "f[3733:3734]" "f[3737]" "f[3773]" "f[3775:3779]" "f[3782]" "f[3784:3788]" "f[3845]" "f[3847:3851]" "f[3853:3860]" "f[3863]" "f[3865:3869]" "f[4636]" "f[4655]" "f[4657:4661]" "f[4675]" "f[4677]" "f[4713]" "f[4717:4724]" "f[4727]" "f[4729:4733]" "f[4798:4802]" "f[4805]" "f[4808]" "f[4811]" "f[4813:4814]" "f[4817]" "f[4855:4859]" "f[4862]" "f[4864:4868]" "f[4925]" "f[4927:4931]" "f[4933:4940]" "f[4943]" "f[4945:4949]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "88F5377F-B648-AEE0-386A-409965EE8B22";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -266.17646600224276 -317.50699253178425 ;
	setAttr ".tgi[0].vh" -type "double2" 759.03358927500324 84.173668470284184 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 297.14285278320312;
	setAttr ".tgi[0].ni[0].y" -62.857143402099609;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[1].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[1].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 6 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 8 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupId3.id" "pCubeShape1.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pCubeShape1.iog.og[1].gco";
connectAttr "groupId4.id" "pCubeShape1.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCubeShape1.iog.og[2].gco";
connectAttr "groupParts3.og" "pCubeShape1.i";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "polyCylinder1.out" "pCylinderShape1.i";
connectAttr "polyCube2.out" "pCubeShape2.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "curveShape1.ws" "polyExtrudeFace1.ipc";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak1.out" "polySubdFace1.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak1.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polySubdFace3.ip";
connectAttr "snow_trampoline_main.oc" "lambert2SG.ss";
connectAttr "pCubeShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "groupId1.msg" "lambert2SG.gn" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "snow_trampoline_main.msg" "materialInfo1.m";
connectAttr "snow_trampoline_details_side.oc" "lambert3SG.ss";
connectAttr "pCubeShape1.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "groupId3.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "snow_trampoline_details_side.msg" "materialInfo2.m";
connectAttr "snow_tramplonine_details_centre.oc" "lambert4SG.ss";
connectAttr "pCubeShape1.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "groupId4.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "snow_tramplonine_details_centre.msg" "materialInfo3.m";
connectAttr "snow_trampoline_structure.oc" "lambert5SG.ss";
connectAttr "pCubeShape4.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape1.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape3.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape4.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape6.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape5.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape2.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape3.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape5.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape10.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape9.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape8.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape6.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape7.iog" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape2.iog" "lambert5SG.dsm" -na;
connectAttr "pCubeShape11.iog" "lambert5SG.dsm" -na;
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "snow_trampoline_structure.msg" "materialInfo4.m";
connectAttr "polySubdFace3.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "groupParts2.og" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "snow_trampoline_details_side.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "snow_trampoline_main.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_trampoline_details_side.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_tramplonine_details_centre.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_trampoline_structure.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "planarTrimmedSurfaceShape1.iog" ":initialShadingGroup.dsm" -na;
connectAttr "planarTrimmedSurfaceShape2.iog" ":initialShadingGroup.dsm" -na;
connectAttr "planarTrimmedSurfaceShape3.iog" ":initialShadingGroup.dsm" -na;
// End of snow_trampoline.ma
