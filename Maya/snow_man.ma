//Maya ASCII 2016 scene
//Name: snow_man.ma
//Last modified: Wed, Dec 14, 2016 09:37:21 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "9B968D24-4A4B-3A90-CF72-67953EC56B26";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 5.2312999811065648 4.2090244817525733 3.9420672585206047 ;
	setAttr ".r" -type "double3" -19.538352729581831 52.999999999995978 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "9AA0D2A9-F349-58EC-B43F-7F90B3F83B15";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 6.9505233755868829;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 3.7881858816479763e-08 1.884506936106471 -9.9961899868716841e-10 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "E4A4F8C1-764E-8651-4B44-61BD7945FCEA";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "FA42FF16-5F41-1559-95B1-76B39AA93F01";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.9389611111467879;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "DC79FCA4-754F-830D-3C1C-82B947746895";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.98177136213532334 1.4564971002846983 100.11540969706235 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "FD4F6521-7548-95CB-C99B-C3911B242A50";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 3.8181243358625983;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "80961A34-3B43-74E9-D04F-DAB26C2C76B7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10902583547137 1.5554190983436396 0.68361017729328832 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "D5D8488D-4D40-3EAD-E48C-88A658379827";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 2.6364201903596527;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pSphere1";
	rename -uid "CDCD2883-EB4C-EF00-5806-9E960C770243";
createNode transform -n "transform22" -p "pSphere1";
	rename -uid "0BC61F36-254A-3DDC-4C88-1994B32775DF";
	setAttr ".v" no;
createNode mesh -n "pSphereShape1" -p "transform22";
	rename -uid "288F418C-5B4E-542D-7032-A6B441DAABD0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere2";
	rename -uid "8B72CC77-824F-4EAB-3979-948244159E06";
	setAttr ".t" -type "double3" 0 1.2280934506830887 0 ;
	setAttr ".s" -type "double3" 0.738799096743881 0.738799096743881 0.738799096743881 ;
createNode mesh -n "polySurfaceShape1" -p "pSphere2";
	rename -uid "89E6B68D-794D-358A-0BB4-63961CBF9F67";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.68181824684143066 0.64285716414451599 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 94 ".uvst[0].uvsp[0:93]" -type "float2" 0 0.14285715 0.090909094
		 0.14285715 0.18181819 0.14285715 0.27272728 0.14285715 0.36363637 0.14285715 0.45454547
		 0.14285715 0.54545456 0.14285715 0.63636363 0.14285715 0.72727275 0.14285715 0.81818187
		 0.14285715 0.909091 0.14285715 1.000000119209 0.14285715 0 0.2857143 0.090909094
		 0.2857143 0.18181819 0.2857143 0.27272728 0.2857143 0.36363637 0.2857143 0.45454547
		 0.2857143 0.54545456 0.2857143 0.63636363 0.2857143 0.72727275 0.2857143 0.81818187
		 0.2857143 0.909091 0.2857143 1.000000119209 0.2857143 0 0.42857146 0.090909094 0.42857146
		 0.18181819 0.42857146 0.27272728 0.42857146 0.36363637 0.42857146 0.45454547 0.42857146
		 0.54545456 0.42857146 0.63636363 0.42857146 0.72727275 0.42857146 0.81818187 0.42857146
		 0.909091 0.42857146 1.000000119209 0.42857146 0 0.5714286 0.090909094 0.5714286 0.18181819
		 0.5714286 0.27272728 0.5714286 0.36363637 0.5714286 0.45454547 0.5714286 0.54545456
		 0.5714286 0.63636363 0.5714286 0.72727275 0.5714286 0.81818187 0.5714286 0.909091
		 0.5714286 1.000000119209 0.5714286 0 0.71428573 0.090909094 0.71428573 0.18181819
		 0.71428573 0.27272728 0.71428573 0.36363637 0.71428573 0.45454547 0.71428573 0.54545456
		 0.71428573 0.63636363 0.71428573 0.72727275 0.71428573 0.81818187 0.71428573 0.909091
		 0.71428573 1.000000119209 0.71428573 0 0.85714287 0.090909094 0.85714287 0.18181819
		 0.85714287 0.27272728 0.85714287 0.36363637 0.85714287 0.45454547 0.85714287 0.54545456
		 0.85714287 0.63636363 0.85714287 0.72727275 0.85714287 0.81818187 0.85714287 0.909091
		 0.85714287 1.000000119209 0.85714287 0.045454547 0 0.13636364 0 0.22727273 0 0.31818181
		 0 0.40909094 0 0.5 0 0.59090912 0 0.68181819 0 0.77272731 0 0.86363637 0 0.9545455
		 0 0.045454547 1 0.13636364 1 0.22727273 1 0.31818181 1 0.40909094 1 0.5 1 0.59090912
		 1 0.68181819 1 0.77272731 1 0.86363637 1 0.9545455 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  0.3650063 -0.90096885 -0.23457517 0.18024193 -0.90096885 -0.39467448
		 -0.061747987 -0.90096885 -0.42946744 -0.28413334 -0.90096885 -0.32790753 -0.4163084 -0.90096885 -0.12223926
		 -0.41630843 -0.90096885 0.12223913 -0.28413346 -0.90096885 0.32790744 -0.061748128 -0.90096885 0.42946744
		 0.18024181 -0.90096885 0.39467457 0.36500624 -0.90096885 0.23457527 0.43388376 -0.90096885 0
		 0.6577186 -0.6234898 -0.42268986 0.32478473 -0.6234898 -0.71117884 -0.11126602 -0.6234898 -0.77387357
		 -0.51199061 -0.6234898 -0.59086895 -0.75016177 -0.6234898 -0.22026753 -0.75016189 -0.6234898 0.2202673
		 -0.51199079 -0.6234898 0.59086877 -0.11126628 -0.6234898 0.77387357 0.32478449 -0.6234898 0.71117896
		 0.65771848 -0.6234898 0.42269003 0.7818315 -0.6234898 0 0.82016164 -0.22252086 -0.5270856
		 0.40499991 -0.22252086 -0.88682544 -0.13874646 -0.22252086 -0.96500456 -0.6384418 -0.22252086 -0.73680145
		 -0.93543643 -0.22252086 -0.27466911 -0.93543655 -0.22252086 0.27466881 -0.63844204 -0.22252086 0.73680133
		 -0.13874678 -0.22252086 0.96500456 0.40499964 -0.22252086 0.88682562 0.82016152 -0.22252086 0.52708584
		 0.9749279 -0.22252086 0 0.82016164 0.22252086 -0.5270856 0.40499991 0.22252086 -0.88682544
		 -0.13874646 0.22252086 -0.96500456 -0.6384418 0.22252086 -0.73680145 -0.93543643 0.22252086 -0.27466911
		 -0.93543655 0.22252086 0.27466881 -0.63844204 0.22252086 0.73680133 -0.13874678 0.22252086 0.96500456
		 0.40499964 0.22252086 0.88682562 0.82016152 0.22252086 0.52708584 0.9749279 0.22252086 0
		 0.6577186 0.6234898 -0.42268986 0.32478473 0.6234898 -0.71117884 -0.11126602 0.6234898 -0.77387357
		 -0.51199061 0.6234898 -0.59086895 -0.75016177 0.6234898 -0.22026753 -0.75016189 0.6234898 0.2202673
		 -0.51199079 0.6234898 0.59086877 -0.11126628 0.6234898 0.77387357 0.32478449 0.6234898 0.71117896
		 0.65771848 0.6234898 0.42269003 0.7818315 0.6234898 0 0.3650063 0.90096885 -0.23457517
		 0.18024193 0.90096885 -0.39467448 -0.061747987 0.90096885 -0.42946744 -0.28413334 0.90096885 -0.32790753
		 -0.4163084 0.90096885 -0.12223926 -0.41630843 0.90096885 0.12223913 -0.28413346 0.90096885 0.32790744
		 -0.061748128 0.90096885 0.42946744 0.18024181 0.90096885 0.39467457 0.36500624 0.90096885 0.23457527
		 0.43388376 0.90096885 0 0 -1 0 0 1 0;
	setAttr -s 143 ".ed[0:142]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 0 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 11 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 22 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0
		 40 41 0 41 42 0 42 43 0 43 33 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 44 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0
		 62 63 0 63 64 0 64 65 0 65 55 0 0 11 0 1 12 0 2 13 0 3 14 0 4 15 0 5 16 0 6 17 0
		 7 18 0 8 19 0 9 20 0 10 21 0 11 22 0 12 23 0 13 24 0 14 25 0 15 26 0 16 27 0 17 28 0
		 18 29 0 19 30 0 20 31 0 21 32 0 22 33 0 23 34 0 24 35 0 25 36 0 26 37 0 27 38 0 28 39 0
		 29 40 0 30 41 0 31 42 0 32 43 0 33 44 0 34 45 0 35 46 0 36 47 0 37 48 0 38 49 0 39 50 0
		 40 51 0 41 52 0 42 53 0 43 54 0 44 55 0 45 56 0 46 57 0 47 58 0 48 59 0 49 60 0 50 61 0
		 51 62 0 52 63 0 53 64 0 54 65 0 66 0 0 66 1 0 66 2 0 66 3 0 66 4 0 66 5 0 66 6 0
		 66 7 0 66 8 0 66 9 0 66 10 0 55 67 0 56 67 0 57 67 0 58 67 0 59 67 0 60 67 0 61 67 0
		 62 67 0 63 67 0 64 67 0 65 67 0;
	setAttr -s 77 -ch 286 ".fc[0:76]" -type "polyFaces" 
		f 4 0 67 -12 -67
		mu 0 4 0 1 13 12
		f 4 1 68 -13 -68
		mu 0 4 1 2 14 13
		f 4 2 69 -14 -69
		mu 0 4 2 3 15 14
		f 4 3 70 -15 -70
		mu 0 4 3 4 16 15
		f 4 4 71 -16 -71
		mu 0 4 4 5 17 16
		f 4 5 72 -17 -72
		mu 0 4 5 6 18 17
		f 4 6 73 -18 -73
		mu 0 4 6 7 19 18
		f 4 7 74 -19 -74
		mu 0 4 7 8 20 19
		f 4 8 75 -20 -75
		mu 0 4 8 9 21 20
		f 4 9 76 -21 -76
		mu 0 4 9 10 22 21
		f 4 10 66 -22 -77
		mu 0 4 10 11 23 22
		f 4 11 78 -23 -78
		mu 0 4 12 13 25 24
		f 4 12 79 -24 -79
		mu 0 4 13 14 26 25
		f 4 13 80 -25 -80
		mu 0 4 14 15 27 26
		f 4 14 81 -26 -81
		mu 0 4 15 16 28 27
		f 4 15 82 -27 -82
		mu 0 4 16 17 29 28
		f 4 16 83 -28 -83
		mu 0 4 17 18 30 29
		f 4 17 84 -29 -84
		mu 0 4 18 19 31 30
		f 4 18 85 -30 -85
		mu 0 4 19 20 32 31
		f 4 19 86 -31 -86
		mu 0 4 20 21 33 32
		f 4 20 87 -32 -87
		mu 0 4 21 22 34 33
		f 4 21 77 -33 -88
		mu 0 4 22 23 35 34
		f 4 22 89 -34 -89
		mu 0 4 24 25 37 36
		f 4 23 90 -35 -90
		mu 0 4 25 26 38 37
		f 4 24 91 -36 -91
		mu 0 4 26 27 39 38
		f 4 25 92 -37 -92
		mu 0 4 27 28 40 39
		f 4 26 93 -38 -93
		mu 0 4 28 29 41 40
		f 4 27 94 -39 -94
		mu 0 4 29 30 42 41
		f 4 28 95 -40 -95
		mu 0 4 30 31 43 42
		f 4 29 96 -41 -96
		mu 0 4 31 32 44 43
		f 4 30 97 -42 -97
		mu 0 4 32 33 45 44
		f 4 31 98 -43 -98
		mu 0 4 33 34 46 45
		f 4 32 88 -44 -99
		mu 0 4 34 35 47 46
		f 4 33 100 -45 -100
		mu 0 4 36 37 49 48
		f 4 34 101 -46 -101
		mu 0 4 37 38 50 49
		f 4 35 102 -47 -102
		mu 0 4 38 39 51 50
		f 4 36 103 -48 -103
		mu 0 4 39 40 52 51
		f 4 37 104 -49 -104
		mu 0 4 40 41 53 52
		f 4 38 105 -50 -105
		mu 0 4 41 42 54 53
		f 4 39 106 -51 -106
		mu 0 4 42 43 55 54
		f 4 40 107 -52 -107
		mu 0 4 43 44 56 55
		f 4 41 108 -53 -108
		mu 0 4 44 45 57 56
		f 4 42 109 -54 -109
		mu 0 4 45 46 58 57
		f 4 43 99 -55 -110
		mu 0 4 46 47 59 58
		f 4 44 111 -56 -111
		mu 0 4 48 49 61 60
		f 4 45 112 -57 -112
		mu 0 4 49 50 62 61
		f 4 46 113 -58 -113
		mu 0 4 50 51 63 62
		f 4 47 114 -59 -114
		mu 0 4 51 52 64 63
		f 4 48 115 -60 -115
		mu 0 4 52 53 65 64
		f 4 49 116 -61 -116
		mu 0 4 53 54 66 65
		f 4 50 117 -62 -117
		mu 0 4 54 55 67 66
		f 4 51 118 -63 -118
		mu 0 4 55 56 68 67
		f 4 52 119 -64 -119
		mu 0 4 56 57 69 68
		f 4 53 120 -65 -120
		mu 0 4 57 58 70 69
		f 4 54 110 -66 -121
		mu 0 4 58 59 71 70
		f 3 -1 -122 122
		mu 0 3 1 0 72
		f 3 -2 -123 123
		mu 0 3 2 1 73
		f 3 -3 -124 124
		mu 0 3 3 2 74
		f 3 -4 -125 125
		mu 0 3 4 3 75
		f 3 -5 -126 126
		mu 0 3 5 4 76
		f 3 -6 -127 127
		mu 0 3 6 5 77
		f 3 -7 -128 128
		mu 0 3 7 6 78
		f 3 -8 -129 129
		mu 0 3 8 7 79
		f 3 -9 -130 130
		mu 0 3 9 8 80
		f 3 -10 -131 131
		mu 0 3 10 9 81
		f 3 -11 -132 121
		mu 0 3 11 10 82
		f 3 55 133 -133
		mu 0 3 60 61 83
		f 3 56 134 -134
		mu 0 3 61 62 84
		f 3 57 135 -135
		mu 0 3 62 63 85
		f 3 58 136 -136
		mu 0 3 63 64 86
		f 3 59 137 -137
		mu 0 3 64 65 87
		f 3 60 138 -138
		mu 0 3 65 66 88
		f 3 61 139 -139
		mu 0 3 66 67 89
		f 3 62 140 -140
		mu 0 3 67 68 90
		f 3 63 141 -141
		mu 0 3 68 69 91
		f 3 64 142 -142
		mu 0 3 69 70 92
		f 3 65 132 -143
		mu 0 3 70 71 93;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform21" -p "pSphere2";
	rename -uid "F88E57A5-FF4A-B642-62B2-13B3FA455595";
	setAttr ".v" no;
createNode mesh -n "pSphereShape2" -p "transform21";
	rename -uid "D87D827D-7F44-2E29-0410-C4BF012E68BE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.68181824684143066 0.64285716414451599 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere3";
	rename -uid "3314E3EA-A142-5E03-4D7F-95A17ED84CF6";
	setAttr ".t" -type "double3" 0 2.2070084203577522 0 ;
	setAttr ".s" -type "double3" 0.43432532116688216 0.43432532116688216 0.43432532116688216 ;
createNode mesh -n "polySurfaceShape2" -p "pSphere3";
	rename -uid "C87DC605-0748-3A06-377D-4D9286B9DA86";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 94 ".uvst[0].uvsp[0:93]" -type "float2" 0 0.14285715 0.090909094
		 0.14285715 0.18181819 0.14285715 0.27272728 0.14285715 0.36363637 0.14285715 0.45454547
		 0.14285715 0.54545456 0.14285715 0.63636363 0.14285715 0.72727275 0.14285715 0.81818187
		 0.14285715 0.909091 0.14285715 1.000000119209 0.14285715 0 0.2857143 0.090909094
		 0.2857143 0.18181819 0.2857143 0.27272728 0.2857143 0.36363637 0.2857143 0.45454547
		 0.2857143 0.54545456 0.2857143 0.63636363 0.2857143 0.72727275 0.2857143 0.81818187
		 0.2857143 0.909091 0.2857143 1.000000119209 0.2857143 0 0.42857146 0.090909094 0.42857146
		 0.18181819 0.42857146 0.27272728 0.42857146 0.36363637 0.42857146 0.45454547 0.42857146
		 0.54545456 0.42857146 0.63636363 0.42857146 0.72727275 0.42857146 0.81818187 0.42857146
		 0.909091 0.42857146 1.000000119209 0.42857146 0 0.5714286 0.090909094 0.5714286 0.18181819
		 0.5714286 0.27272728 0.5714286 0.36363637 0.5714286 0.45454547 0.5714286 0.54545456
		 0.5714286 0.63636363 0.5714286 0.72727275 0.5714286 0.81818187 0.5714286 0.909091
		 0.5714286 1.000000119209 0.5714286 0 0.71428573 0.090909094 0.71428573 0.18181819
		 0.71428573 0.27272728 0.71428573 0.36363637 0.71428573 0.45454547 0.71428573 0.54545456
		 0.71428573 0.63636363 0.71428573 0.72727275 0.71428573 0.81818187 0.71428573 0.909091
		 0.71428573 1.000000119209 0.71428573 0 0.85714287 0.090909094 0.85714287 0.18181819
		 0.85714287 0.27272728 0.85714287 0.36363637 0.85714287 0.45454547 0.85714287 0.54545456
		 0.85714287 0.63636363 0.85714287 0.72727275 0.85714287 0.81818187 0.85714287 0.909091
		 0.85714287 1.000000119209 0.85714287 0.045454547 0 0.13636364 0 0.22727273 0 0.31818181
		 0 0.40909094 0 0.5 0 0.59090912 0 0.68181819 0 0.77272731 0 0.86363637 0 0.9545455
		 0 0.045454547 1 0.13636364 1 0.22727273 1 0.31818181 1 0.40909094 1 0.5 1 0.59090912
		 1 0.68181819 1 0.77272731 1 0.86363637 1 0.9545455 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  0.3650063 -0.90096885 -0.23457517 0.18024193 -0.90096885 -0.39467448
		 -0.061747987 -0.90096885 -0.42946744 -0.28413334 -0.90096885 -0.32790753 -0.4163084 -0.90096885 -0.12223926
		 -0.41630843 -0.90096885 0.12223913 -0.28413346 -0.90096885 0.32790744 -0.061748128 -0.90096885 0.42946744
		 0.18024181 -0.90096885 0.39467457 0.36500624 -0.90096885 0.23457527 0.43388376 -0.90096885 0
		 0.6577186 -0.6234898 -0.42268986 0.32478473 -0.6234898 -0.71117884 -0.11126602 -0.6234898 -0.77387357
		 -0.51199061 -0.6234898 -0.59086895 -0.75016177 -0.6234898 -0.22026753 -0.75016189 -0.6234898 0.2202673
		 -0.51199079 -0.6234898 0.59086877 -0.11126628 -0.6234898 0.77387357 0.32478449 -0.6234898 0.71117896
		 0.65771848 -0.6234898 0.42269003 0.7818315 -0.6234898 0 0.82016164 -0.22252086 -0.5270856
		 0.40499991 -0.22252086 -0.88682544 -0.13874646 -0.22252086 -0.96500456 -0.6384418 -0.22252086 -0.73680145
		 -0.93543643 -0.22252086 -0.27466911 -0.93543655 -0.22252086 0.27466881 -0.63844204 -0.22252086 0.73680133
		 -0.13874678 -0.22252086 0.96500456 0.40499964 -0.22252086 0.88682562 0.82016152 -0.22252086 0.52708584
		 0.9749279 -0.22252086 0 0.82016164 0.22252086 -0.5270856 0.40499991 0.22252086 -0.88682544
		 -0.13874646 0.22252086 -0.96500456 -0.6384418 0.22252086 -0.73680145 -0.93543643 0.22252086 -0.27466911
		 -0.93543655 0.22252086 0.27466881 -0.63844204 0.22252086 0.73680133 -0.13874678 0.22252086 0.96500456
		 0.40499964 0.22252086 0.88682562 0.82016152 0.22252086 0.52708584 0.9749279 0.22252086 0
		 0.6577186 0.6234898 -0.42268986 0.32478473 0.6234898 -0.71117884 -0.11126602 0.6234898 -0.77387357
		 -0.51199061 0.6234898 -0.59086895 -0.75016177 0.6234898 -0.22026753 -0.75016189 0.6234898 0.2202673
		 -0.51199079 0.6234898 0.59086877 -0.11126628 0.6234898 0.77387357 0.32478449 0.6234898 0.71117896
		 0.65771848 0.6234898 0.42269003 0.7818315 0.6234898 0 0.3650063 0.90096885 -0.23457517
		 0.18024193 0.90096885 -0.39467448 -0.061747987 0.90096885 -0.42946744 -0.28413334 0.90096885 -0.32790753
		 -0.4163084 0.90096885 -0.12223926 -0.41630843 0.90096885 0.12223913 -0.28413346 0.90096885 0.32790744
		 -0.061748128 0.90096885 0.42946744 0.18024181 0.90096885 0.39467457 0.36500624 0.90096885 0.23457527
		 0.43388376 0.90096885 0 0 -1 0 0 1 0;
	setAttr -s 143 ".ed[0:142]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 0 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 11 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 22 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0
		 40 41 0 41 42 0 42 43 0 43 33 0 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 44 0 55 56 0 56 57 0 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0
		 62 63 0 63 64 0 64 65 0 65 55 0 0 11 0 1 12 0 2 13 0 3 14 0 4 15 0 5 16 0 6 17 0
		 7 18 0 8 19 0 9 20 0 10 21 0 11 22 0 12 23 0 13 24 0 14 25 0 15 26 0 16 27 0 17 28 0
		 18 29 0 19 30 0 20 31 0 21 32 0 22 33 0 23 34 0 24 35 0 25 36 0 26 37 0 27 38 0 28 39 0
		 29 40 0 30 41 0 31 42 0 32 43 0 33 44 0 34 45 0 35 46 0 36 47 0 37 48 0 38 49 0 39 50 0
		 40 51 0 41 52 0 42 53 0 43 54 0 44 55 0 45 56 0 46 57 0 47 58 0 48 59 0 49 60 0 50 61 0
		 51 62 0 52 63 0 53 64 0 54 65 0 66 0 0 66 1 0 66 2 0 66 3 0 66 4 0 66 5 0 66 6 0
		 66 7 0 66 8 0 66 9 0 66 10 0 55 67 0 56 67 0 57 67 0 58 67 0 59 67 0 60 67 0 61 67 0
		 62 67 0 63 67 0 64 67 0 65 67 0;
	setAttr -s 77 -ch 286 ".fc[0:76]" -type "polyFaces" 
		f 4 0 67 -12 -67
		mu 0 4 0 1 13 12
		f 4 1 68 -13 -68
		mu 0 4 1 2 14 13
		f 4 2 69 -14 -69
		mu 0 4 2 3 15 14
		f 4 3 70 -15 -70
		mu 0 4 3 4 16 15
		f 4 4 71 -16 -71
		mu 0 4 4 5 17 16
		f 4 5 72 -17 -72
		mu 0 4 5 6 18 17
		f 4 6 73 -18 -73
		mu 0 4 6 7 19 18
		f 4 7 74 -19 -74
		mu 0 4 7 8 20 19
		f 4 8 75 -20 -75
		mu 0 4 8 9 21 20
		f 4 9 76 -21 -76
		mu 0 4 9 10 22 21
		f 4 10 66 -22 -77
		mu 0 4 10 11 23 22
		f 4 11 78 -23 -78
		mu 0 4 12 13 25 24
		f 4 12 79 -24 -79
		mu 0 4 13 14 26 25
		f 4 13 80 -25 -80
		mu 0 4 14 15 27 26
		f 4 14 81 -26 -81
		mu 0 4 15 16 28 27
		f 4 15 82 -27 -82
		mu 0 4 16 17 29 28
		f 4 16 83 -28 -83
		mu 0 4 17 18 30 29
		f 4 17 84 -29 -84
		mu 0 4 18 19 31 30
		f 4 18 85 -30 -85
		mu 0 4 19 20 32 31
		f 4 19 86 -31 -86
		mu 0 4 20 21 33 32
		f 4 20 87 -32 -87
		mu 0 4 21 22 34 33
		f 4 21 77 -33 -88
		mu 0 4 22 23 35 34
		f 4 22 89 -34 -89
		mu 0 4 24 25 37 36
		f 4 23 90 -35 -90
		mu 0 4 25 26 38 37
		f 4 24 91 -36 -91
		mu 0 4 26 27 39 38
		f 4 25 92 -37 -92
		mu 0 4 27 28 40 39
		f 4 26 93 -38 -93
		mu 0 4 28 29 41 40
		f 4 27 94 -39 -94
		mu 0 4 29 30 42 41
		f 4 28 95 -40 -95
		mu 0 4 30 31 43 42
		f 4 29 96 -41 -96
		mu 0 4 31 32 44 43
		f 4 30 97 -42 -97
		mu 0 4 32 33 45 44
		f 4 31 98 -43 -98
		mu 0 4 33 34 46 45
		f 4 32 88 -44 -99
		mu 0 4 34 35 47 46
		f 4 33 100 -45 -100
		mu 0 4 36 37 49 48
		f 4 34 101 -46 -101
		mu 0 4 37 38 50 49
		f 4 35 102 -47 -102
		mu 0 4 38 39 51 50
		f 4 36 103 -48 -103
		mu 0 4 39 40 52 51
		f 4 37 104 -49 -104
		mu 0 4 40 41 53 52
		f 4 38 105 -50 -105
		mu 0 4 41 42 54 53
		f 4 39 106 -51 -106
		mu 0 4 42 43 55 54
		f 4 40 107 -52 -107
		mu 0 4 43 44 56 55
		f 4 41 108 -53 -108
		mu 0 4 44 45 57 56
		f 4 42 109 -54 -109
		mu 0 4 45 46 58 57
		f 4 43 99 -55 -110
		mu 0 4 46 47 59 58
		f 4 44 111 -56 -111
		mu 0 4 48 49 61 60
		f 4 45 112 -57 -112
		mu 0 4 49 50 62 61
		f 4 46 113 -58 -113
		mu 0 4 50 51 63 62
		f 4 47 114 -59 -114
		mu 0 4 51 52 64 63
		f 4 48 115 -60 -115
		mu 0 4 52 53 65 64
		f 4 49 116 -61 -116
		mu 0 4 53 54 66 65
		f 4 50 117 -62 -117
		mu 0 4 54 55 67 66
		f 4 51 118 -63 -118
		mu 0 4 55 56 68 67
		f 4 52 119 -64 -119
		mu 0 4 56 57 69 68
		f 4 53 120 -65 -120
		mu 0 4 57 58 70 69
		f 4 54 110 -66 -121
		mu 0 4 58 59 71 70
		f 3 -1 -122 122
		mu 0 3 1 0 72
		f 3 -2 -123 123
		mu 0 3 2 1 73
		f 3 -3 -124 124
		mu 0 3 3 2 74
		f 3 -4 -125 125
		mu 0 3 4 3 75
		f 3 -5 -126 126
		mu 0 3 5 4 76
		f 3 -6 -127 127
		mu 0 3 6 5 77
		f 3 -7 -128 128
		mu 0 3 7 6 78
		f 3 -8 -129 129
		mu 0 3 8 7 79
		f 3 -9 -130 130
		mu 0 3 9 8 80
		f 3 -10 -131 131
		mu 0 3 10 9 81
		f 3 -11 -132 121
		mu 0 3 11 10 82
		f 3 55 133 -133
		mu 0 3 60 61 83
		f 3 56 134 -134
		mu 0 3 61 62 84
		f 3 57 135 -135
		mu 0 3 62 63 85
		f 3 58 136 -136
		mu 0 3 63 64 86
		f 3 59 137 -137
		mu 0 3 64 65 87
		f 3 60 138 -138
		mu 0 3 65 66 88
		f 3 61 139 -139
		mu 0 3 66 67 89
		f 3 62 140 -140
		mu 0 3 67 68 90
		f 3 63 141 -141
		mu 0 3 68 69 91
		f 3 64 142 -142
		mu 0 3 69 70 92
		f 3 65 132 -143
		mu 0 3 70 71 93;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform20" -p "pSphere3";
	rename -uid "BFCBB915-D044-9B01-112C-55BD561A5E47";
	setAttr ".v" no;
createNode mesh -n "pSphereShape3" -p "transform20";
	rename -uid "D1CC6C83-B04D-0001-B2A5-F0B901AA285F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere4";
	rename -uid "7B7D3F83-864A-98EA-960E-FC9A0CB39D50";
	setAttr ".t" -type "double3" 0.13473455936244605 0 0.93204157747027239 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode transform -n "transform19" -p "pSphere4";
	rename -uid "625C79EF-5342-0A93-F817-3CBFB8F18B5E";
	setAttr ".v" no;
createNode mesh -n "pSphereShape4" -p "transform19";
	rename -uid "EA93D604-134C-CD95-6378-658FA8948C7A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere5";
	rename -uid "55C1234F-9F45-C783-1571-569496F7B31E";
	setAttr ".t" -type "double3" 0.13473455936244605 0.46647455699033846 0.81542293822268785 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape3" -p "pSphere5";
	rename -uid "FC802CF6-074C-C854-BE90-2A92CA09270C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform18" -p "pSphere5";
	rename -uid "73869FCD-234D-0D88-2C53-24B23E277E14";
	setAttr ".v" no;
createNode mesh -n "pSphereShape5" -p "transform18";
	rename -uid "2DD425EB-3044-D553-542C-F5B67596803E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere6";
	rename -uid "4C3D501E-894E-7652-3BCE-AEB649DA8C79";
	setAttr ".t" -type "double3" 0.13473455936244605 0.70612494955005634 0.63387955564236953 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape4" -p "pSphere6";
	rename -uid "FDC4A5D2-264C-19EF-FE81-24B64C991608";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform17" -p "pSphere6";
	rename -uid "7F8DAF82-7345-7128-FEB1-75830299EB63";
	setAttr ".v" no;
createNode mesh -n "pSphereShape6" -p "transform17";
	rename -uid "ACB625A7-8E41-A27A-390B-23A1FB62E386";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere7";
	rename -uid "4098ED9B-3D4D-59D2-9E2B-308FE7382B84";
	setAttr ".t" -type "double3" 0.13473455936244605 1.2186290031889886 0.68361018141941043 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape5" -p "pSphere7";
	rename -uid "021B1A3C-674C-EDFD-6EC8-AD9E3A9E8EC7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform16" -p "pSphere7";
	rename -uid "DB9F97CD-6848-4443-56A2-54BEDBB55845";
	setAttr ".v" no;
createNode mesh -n "pSphereShape7" -p "transform16";
	rename -uid "373A6BB3-5845-1F9B-1D1F-C5AC27EAD652";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere8";
	rename -uid "B1BBBE25-A448-2ADB-D393-168646F82A08";
	setAttr ".t" -type "double3" 0.13473455936244605 1.5554190983436396 0.60625143021067984 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape6" -p "pSphere8";
	rename -uid "6C95A4C6-AF4D-349C-69F8-7F8223641107";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform15" -p "pSphere8";
	rename -uid "9E955D95-6C43-76F6-5152-9095054F346A";
	setAttr ".v" no;
createNode mesh -n "pSphereShape8" -p "transform15";
	rename -uid "A96FE558-8B47-F829-9EB3-E59545AE238D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pPyramid1";
	rename -uid "325F2861-964C-DED6-FC4E-A5A146C10DF5";
	setAttr ".t" -type "double3" 0.03226343432254053 2.2182113619116373 0.44463749970692168 ;
	setAttr ".r" -type "double3" 90 0 0 ;
createNode transform -n "transform14" -p "pPyramid1";
	rename -uid "7B073591-5D49-12E6-CF43-FE8CBBF4E0D7";
	setAttr ".v" no;
createNode mesh -n "pPyramidShape1" -p "transform14";
	rename -uid "48E51412-A34F-720A-800F-3B9C3A81317A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.25 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere9";
	rename -uid "EFD71D93-814B-30E2-08ED-34BD7B839A0A";
	setAttr ".t" -type "double3" 0.17329040310003202 2.3958275775381948 0.30179827784286145 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape7" -p "pSphere9";
	rename -uid "2DEAA349-4448-60D3-717B-96A7E3C3A42F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform13" -p "pSphere9";
	rename -uid "BED9F7EC-7D45-ADC1-2CEF-3C92975E6460";
	setAttr ".v" no;
createNode mesh -n "pSphereShape9" -p "transform13";
	rename -uid "28739DB1-9748-1331-EE94-35BAB790916E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere10";
	rename -uid "6906391A-2B4C-10F8-355E-B9A3DC2D8232";
	setAttr ".t" -type "double3" -0.12105998996510181 2.3958275775381948 0.32586899614382009 ;
	setAttr ".s" -type "double3" 0.069225215280235911 0.069225215280235911 0.069225215280235911 ;
createNode mesh -n "polySurfaceShape8" -p "pSphere10";
	rename -uid "5339BA39-254D-BFB9-8F98-D592D5D2D33D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform12" -p "pSphere10";
	rename -uid "6CF81EAE-E743-369D-3033-1F81EA0F8A2A";
	setAttr ".v" no;
createNode mesh -n "pSphereShape10" -p "transform12";
	rename -uid "60B573FB-4C46-12D1-6AE8-2A9577276D6D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere13";
	rename -uid "1DE1967D-D843-0399-1E75-1487021E7582";
	setAttr ".t" -type "double3" -0.051358644496463768 2.0677577057086887 0.39145812817617559 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape9" -p "pSphere13";
	rename -uid "AE6D136B-5746-A0D5-10D9-89800F4B3B40";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform11" -p "pSphere13";
	rename -uid "0CE7DB6D-A449-B46C-425E-529EAA5EA3D7";
	setAttr ".v" no;
createNode mesh -n "pSphereShape13" -p "transform11";
	rename -uid "B6052DA1-C845-C570-90CA-378121574217";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere14";
	rename -uid "D269D89E-D44A-5368-AC82-5B8864583B9D";
	setAttr ".t" -type "double3" -0.02089891494410713 2.0484385709121056 0.37839784297535367 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape10" -p "pSphere14";
	rename -uid "94F8AE54-1D4C-5A5C-C9BA-169B4A1922C0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform10" -p "pSphere14";
	rename -uid "0F9EA87E-AA41-8834-8D56-34A6629AF3EC";
	setAttr ".v" no;
createNode mesh -n "pSphereShape14" -p "transform10";
	rename -uid "C1663911-0F42-DDF3-20D1-5DB24C187A97";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere15";
	rename -uid "4D1A7092-1543-D513-4DB1-C1B7CFF56AC0";
	setAttr ".t" -type "double3" 0.019361835764145824 2.0418666689564016 0.37839784297535367 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape11" -p "pSphere15";
	rename -uid "60DE31CF-504D-B203-F02F-F8ADB89B4BB1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform9" -p "pSphere15";
	rename -uid "1E4F0866-1647-0555-4C99-5EB7DB093904";
	setAttr ".v" no;
createNode mesh -n "pSphereShape15" -p "transform9";
	rename -uid "0F5274FE-9249-BE63-9A89-489126744F40";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere16";
	rename -uid "A852B8F5-6A43-186F-750E-2F94C04D47D6";
	setAttr ".t" -type "double3" 0.062360114340774639 2.0418666689564016 0.37839784297535367 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape12" -p "pSphere16";
	rename -uid "26B0EB27-174E-C8CE-DE7E-F2ACB047DFCC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform8" -p "pSphere16";
	rename -uid "2EDDCE05-4444-883D-B0AB-C6A9490738C4";
	setAttr ".v" no;
createNode mesh -n "pSphereShape16" -p "transform8";
	rename -uid "5BE5DB1E-2F4B-8CD2-0D0C-CC991CDE01E9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere17";
	rename -uid "1114C4E6-5B4B-47BC-664C-1EA80E1859BE";
	setAttr ".t" -type "double3" 0.10476023688825296 2.0418666689564016 0.33011531449659332 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape13" -p "pSphere17";
	rename -uid "956EC4F4-7F49-5BB4-FF02-179047CC098A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform7" -p "pSphere17";
	rename -uid "070FEB31-004C-A92A-DAAC-28BB5FD75CBE";
	setAttr ".v" no;
createNode mesh -n "pSphereShape17" -p "transform7";
	rename -uid "80966E88-D844-E228-8182-E2B384D69764";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere19";
	rename -uid "0098B75C-E347-CEF9-3781-1A9DDFC0C0F9";
	setAttr ".t" -type "double3" 0.14869685164606439 2.0714353828889367 0.36471762205799357 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape14" -p "pSphere19";
	rename -uid "55E390E7-FB47-7C2A-DBCB-CE90194CBF50";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform6" -p "pSphere19";
	rename -uid "B4F6ECDA-6C43-8A7D-C182-ACA12F5B2DC1";
	setAttr ".v" no;
createNode mesh -n "pSphereShape19" -p "transform6";
	rename -uid "C3580C26-D44C-92D8-3AE1-1ABEE6E762AF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder1";
	rename -uid "5BBAC89D-0842-EC63-AC07-329884918A2C";
	setAttr ".t" -type "double3" 0 2.6054925825617645 0 ;
	setAttr ".s" -type "double3" 1.0985414096463331 1.0985414096463331 1.0985414096463331 ;
createNode transform -n "transform5" -p "pCylinder1";
	rename -uid "9E96F36A-BD4E-3E57-87A0-4C99875BA0BB";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform5";
	rename -uid "19F90606-D747-DFC9-CD01-76B7D8A92AAC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.609375 0.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "221A59EA-B947-3FDB-30BC-CD8A2C84A345";
	setAttr ".t" -type "double3" 0 2.8279590340780794 0 ;
	setAttr ".s" -type "double3" 1 0.79422201962146233 1 ;
createNode transform -n "transform4" -p "pCylinder2";
	rename -uid "083515E4-5D4F-18B5-5033-658A04AB3B71";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform4";
	rename -uid "EFA62375-0941-B403-EBAA-3895DAE2BB85";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.50070446729660034 0.8403962254524231 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder3";
	rename -uid "13572D10-F749-8885-AC04-FA834783E26B";
	setAttr ".t" -type "double3" 0.76518680883826495 1.2698037643652178 0.14455072808221947 ;
	setAttr -av ".tx";
	setAttr -av ".ty";
	setAttr -av ".tz";
	setAttr ".s" -type "double3" 0.020140796946054963 0.073822100570912805 0.020140796946054963 ;
	setAttr -av ".sx";
	setAttr -av ".sy";
	setAttr -av ".sz";
createNode transform -n "transform3" -p "pCylinder3";
	rename -uid "FC78AA0B-F947-FE36-9F32-72B5F0580934";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape3" -p "transform3";
	rename -uid "12FF4A85-094C-38A1-05E6-FD9ABB9C4884";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64623898267745972 0.16660803556442261 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "pCylinder4";
	rename -uid "1C150FAA-A048-6A9C-4670-E8875B3D7DA4";
	setAttr ".t" -type "double3" -0.59149403814710277 1.2698037643652178 0.14455072808221947 ;
	setAttr ".r" -type "double3" 180 0 90 ;
	setAttr ".s" -type "double3" 0.020140796946054963 0.073822100570912805 0.020140796946054963 ;
createNode mesh -n "polySurfaceShape15" -p "pCylinder4";
	rename -uid "86C6D54C-684E-1518-702B-FF95F22263AD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64623898267745972 0.16660803556442261 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4128 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.61048543 0.04576458 0.5 1.4901161e-08
		 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543
		 0.26673543 0.65625 0.15625 0.375 0.3125 0.40625 0.3125 0.4375 0.3125 0.46875 0.3125
		 0.5 0.3125 0.53125 0.3125 0.5625 0.3125 0.59375 0.3125 0.625 0.3125 0.375 0.68843985
		 0.40625 0.68843985 0.4375 0.68843985 0.46875 0.68843985 0.5 0.68843985 0.53125 0.68843985
		 0.5625 0.68843985 0.59375 0.68843985 0.625 0.68843985 0.61048543 0.73326457 0.5 0.6875
		 0.38951457 0.73326457 0.34375 0.84375 0.38951457 0.95423543 0.5 1 0.61048543 0.95423543
		 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.5 1.4901161e-08 0.61048543 0.04576458
		 0.38951457 0.04576458 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543
		 0.26673543 0.65625 0.15625 0.5 1.4901161e-08 0.61048543 0.04576458 0.38951457 0.04576458
		 0.34375 0.15625 0.38951457 0.26673543 0.5 0.3125 0.61048543 0.26673543 0.65625 0.15625
		 0.55524278 0.022882299 0.390625 0.3125 0.55524272 0.022882298 0.61048543 0.04576458
		 0.55524272 0.022882298 0.5 1.4901161e-08 0.44475725 0.022882301 0.421875 0.3125 0.44475728
		 0.022882298 0.44475728 0.022882298 0.38951457 0.04576458 0.36663231 0.10100731 0.453125
		 0.3125 0.36663228 0.10100729 0.36663228 0.10100729 0.34375 0.15625 0.36663231 0.21149272
		 0.484375 0.3125 0.36663228 0.21149272 0.36663228 0.21149272 0.38951457 0.26673543
		 0.44475725 0.28961772 0.515625 0.3125 0.44475728 0.28961772 0.44475728 0.28961772
		 0.5 0.3125 0.55524278 0.28961772 0.546875 0.3125 0.55524272 0.28961772 0.55524272
		 0.28961772 0.61048543 0.26673543 0.63336778 0.21149275 0.578125 0.3125 0.63336772
		 0.21149272 0.63336772 0.21149272 0.65625 0.15625 0.63336778 0.10100731 0.609375 0.3125
		 0.63336772 0.10100729 0.63336772 0.10100729 0.52762139 0.011441156 0.5 1.4901161e-08
		 0.3984375 0.3125 0.52762139 0.011441156 0.55524278 0.022882298 0.52762139 0.011441157
		 0.41713595 0.034323439 0.38951457 0.04576458 0.4296875 0.3125 0.41713592 0.034323439
		 0.44475728 0.022882299 0.41713589 0.034323439 0.35519114 0.12862866 0.34375 0.15625
		 0.4609375 0.3125 0.35519114 0.12862864 0.36663228 0.1010073 0.35519117 0.12862866
		 0.37807342 0.23911406 0.38951457 0.26673543 0.4921875 0.3125 0.37807342 0.23911408
		 0.36663228 0.21149272 0.37807345 0.23911408 0.47237867 0.30105889 0.5 0.3125 0.5234375
		 0.3125 0.47237864 0.30105886 0.44475728 0.28961772 0.47237861 0.30105886 0.58286411
		 0.27817658 0.61048543 0.26673543 0.5546875 0.3125 0.58286405 0.27817658 0.55524278
		 0.28961772 0.58286411 0.27817658 0.64480889 0.18387137 0.65625 0.15625 0.5859375
		 0.3125 0.64480889 0.18387136 0.63336778 0.21149273 0.64480889 0.18387137 0.62192661
		 0.073385939 0.61048543 0.04576458 0.6171875 0.3125 0.62192655 0.073385939 0.63336778
		 0.1010073 0.62192661 0.073385939 0.58286411 0.034323439 0.3828125 0.3125 0.58286405
		 0.034323439 0.58286411 0.034323439 0.58286411 0.034323439 0.61048543 0.04576458 0.58286405
		 0.034323439 0.55524278 0.022882298 0.52762139 0.011441157 0.52762139 0.011441156
		 0.5 1.4901161e-08 0.47237867 0.011441158 0.4140625 0.3125 0.47237864 0.011441156
		 0.47237861 0.011441158 0.47237867 0.011441158 0.47237864 0.011441156 0.44475728 0.022882299
		 0.41713595 0.034323439 0.41713592 0.034323439 0.38951457 0.04576458 0.37807342 0.073385946
		 0.4453125 0.3125 0.37807342 0.073385939 0.37807345 0.073385939 0.37807345 0.073385939
		 0.37807342 0.073385939 0.36663228 0.1010073 0.3551912 0.12862866 0.35519114 0.12862864
		 0.34375 0.15625 0.35519114 0.18387139 0.4765625 0.3125 0.35519114 0.18387136 0.35519117
		 0.18387136 0.35519111 0.18387136 0.35519114 0.18387136 0.36663228 0.21149272 0.37807345
		 0.23911411 0.37807342 0.23911408 0.38951457 0.26673543 0.41713595 0.27817658 0.5078125
		 0.3125 0.41713592 0.27817658 0.41713589 0.27817658 0.41713595 0.27817658 0.41713592
		 0.27817658 0.44475728 0.28961772 0.47237867 0.30105889 0.47237864 0.30105886 0.5
		 0.3125 0.52762139 0.30105886 0.5390625 0.3125 0.52762139 0.30105886 0.52762139 0.30105886
		 0.52762139 0.30105886 0.52762139 0.30105886 0.55524278 0.28961772 0.58286411 0.27817658
		 0.58286405 0.27817658 0.61048543 0.26673543 0.62192667 0.23911411 0.5703125 0.3125
		 0.62192655 0.23911408 0.62192661 0.23911409 0.62192661 0.23911409 0.62192655 0.23911408
		 0.63336778 0.21149273 0.64480895 0.18387139 0.64480889 0.18387136 0.65625 0.15625
		 0.64480895 0.12862866 0.6015625 0.3125 0.64480889 0.12862864 0.64480889 0.12862866
		 0.64480889 0.12862866 0.64480889 0.12862864 0.63336778 0.1010073 0.62192667 0.073385946
		 0.62192655 0.073385939 0.55524278 0.28961772 0.55524278 0.28961772 0.52762139 0.30105886
		 0.52762139 0.30105886 0.62192661 0.23911409 0.61048543 0.26673543 0.61048543 0.26673543
		 0.62192661 0.23911409 0.64480889 0.12862866 0.65625 0.15625 0.65625 0.15625 0.64480889
		 0.12862866 0.63336778 0.21149275 0.63336778 0.21149273 0.64480889 0.18387137 0.64480895
		 0.18387139 0.63336778 0.10100731 0.63336778 0.1010073 0.62192661 0.073385939 0.62192667
		 0.073385946 0.55524248 0.022882208 0.51381069 0.0057205856 0.54143202 0.017161727
		 0.56905341 0.028602868 0.59667474 0.04004401 0.61048543 0.04576458 0.55524272 0.022882298
		 0.5 1.4901161e-08 0.44475761 0.022882195 0.40332526 0.04004401;
	setAttr ".uvst[0].uvsp[250:499]" 0.43094659 0.028602868 0.45856798 0.017161727
		 0.48618931 0.0057205856 0.44475728 0.022882298 0.38951457 0.04576458 0.36663237 0.10100707
		 0.34947056 0.14243932 0.36091173 0.11481796 0.37235284 0.087196618 0.38379401 0.05957526
		 0.36663228 0.10100729 0.34375 0.15625 0.36663228 0.21149272 0.38379401 0.25292474
		 0.37235284 0.2253034 0.36091173 0.19768204 0.34947056 0.17006068 0.36663228 0.21149272
		 0.38951457 0.26673543 0.44475764 0.28961787 0.48618931 0.30677944 0.45856798 0.29533827
		 0.43094659 0.28389716 0.40332526 0.27245599 0.44475728 0.28961772 0.5 0.3125 0.55524272
		 0.28961772 0.59667474 0.27245599 0.56905341 0.28389716 0.54143202 0.29533827 0.51381069
		 0.30677944 0.55524272 0.28961772 0.61048543 0.26673543 0.6333676 0.21149296 0.65052944
		 0.17006068 0.63908827 0.19768204 0.62764716 0.2253034 0.61620599 0.25292474 0.63336772
		 0.21149272 0.65625 0.15625 0.6333676 0.10100704 0.61620599 0.05957526 0.62764716
		 0.087196618 0.63908827 0.11481796 0.65052944 0.14243932 0.63336772 0.10100729 0.51764572
		 0.007309109 0.5 1.4901161e-08 0.50690532 0.0028603002 0.53452659 0.014301397 0.52762127
		 0.011441112 0.4071607 0.038455337 0.38951457 0.04576458 0.39641991 0.042904295 0.42404145
		 0.031463102 0.41713607 0.034323387 0.3510052 0.1387344 0.34375 0.15625 0.34661028
		 0.14934465 0.35805148 0.1217232 0.35519117 0.12862854 0.38233879 0.24941148 0.38951457
		 0.26673543 0.38665429 0.25983009 0.37521315 0.23220873 0.37807342 0.23911408 0.48281693
		 0.30538255 0.5 0.3125 0.49309465 0.30963972 0.46547347 0.29819864 0.47237882 0.30105895
		 0.59330225 0.27385291 0.61048543 0.26673543 0.60358012 0.26959571 0.57595873 0.28103685
		 0.58286405 0.27817658 0.64907414 0.17357408 0.65625 0.15625 0.65338969 0.16315535
		 0.64194852 0.19077682 0.64480877 0.18387148 0.61774057 0.063279986 0.61048543 0.04576458
		 0.61334574 0.05266992 0.62478679 0.080291152 0.62192655 0.073385805 0.5412336 0.017079508
		 0.52071607 0.0085808709 0.53452671 0.014301442 0.54833722 0.020021968 0.55517942
		 0.022856079 0.54833734 0.020022012 0.56214809 0.025742583 0.56214797 0.025742538
		 0.5691303 0.028634747 0.57595873 0.031463154 0.58976936 0.037183724 0.57595861 0.031463109
		 0.59330225 0.038647082 0.60358012 0.042904295 0.58286393 0.034323394 0.58286393 0.034323372
		 0.61048543 0.04576458 0.58286405 0.034323439 0.5552426 0.022882253 0.52762121 0.011441133
		 0.52762139 0.011441156 0.5 1.4901161e-08 0.4307484 0.028684992 0.41023058 0.037183724
		 0.42404127 0.031463154 0.43785208 0.025742531 0.44469422 0.022908421 0.43785194 0.025742583
		 0.45166263 0.020022012 0.45166278 0.02002196 0.45864522 0.017129751 0.46547329 0.014301442
		 0.47928399 0.0085808709 0.46547347 0.01430139 0.48281693 0.0071174731 0.49309465
		 0.0028603002 0.47237879 0.011441105 0.47237885 0.011441078 0.47237864 0.011441156
		 0.44475746 0.022882245 0.41713601 0.034323413 0.41713592 0.034323439 0.38951457 0.04576458
		 0.36084357 0.11498255 0.35233086 0.13553399 0.35805142 0.1217233 0.36377203 0.10791252
		 0.36662143 0.10103335 0.363772 0.10791263 0.36949256 0.094101951 0.36949259 0.094101846
		 0.37240037 0.087082013 0.37521315 0.080291279 0.3809337 0.066480599 0.37521321 0.080291167
		 0.38233876 0.063088462 0.38665429 0.05266992 0.37807345 0.073385827 0.37807348 0.073385827
		 0.37807342 0.073385939 0.36663234 0.10100718 0.35519114 0.12862852 0.35519114 0.12862864
		 0.34375 0.15625 0.37240031 0.22541791 0.3809337 0.24601941 0.37521315 0.23220873
		 0.36949256 0.21839806 0.36662143 0.21146655 0.36949256 0.21839806 0.363772 0.20458737
		 0.363772 0.20458737 0.36084351 0.19751735 0.35805142 0.19077671 0.35233086 0.17696601
		 0.35805142 0.19077671 0.35100514 0.1737655 0.34661028 0.16315535 0.35519114 0.18387136
		 0.35519114 0.18387139 0.35519114 0.18387136 0.36663228 0.21149272 0.37807342 0.23911405
		 0.37807342 0.23911408 0.38951457 0.26673543 0.45864528 0.29537028 0.47928399 0.30391914
		 0.46547329 0.29819858 0.45166281 0.29247808 0.44469425 0.28959164 0.45166263 0.292478
		 0.43785194 0.28675744 0.43785211 0.28675753 0.43074846 0.28381506 0.42404127 0.28103685
		 0.41023058 0.2753163 0.42404145 0.28103691 0.40716073 0.27404469 0.39641991 0.26959571
		 0.4171361 0.27817667 0.41713604 0.27817664 0.41713592 0.27817658 0.44475746 0.28961778
		 0.47237891 0.30105898 0.47237864 0.30105886 0.5 0.3125 0.5691306 0.28386524 0.58976936
		 0.2753163 0.57595873 0.28103685 0.56214809 0.28675744 0.5551796 0.28964394 0.56214809
		 0.28675744 0.54833734 0.292478 0.54833734 0.292478 0.5412336 0.29542047 0.53452671
		 0.29819858 0.52071607 0.30391914 0.53452671 0.29819858 0.5176459 0.30519086 0.50690532
		 0.30963972 0.52762139 0.30105886 0.52762139 0.30105883 0.52762139 0.30105886 0.55524272
		 0.28961772 0.58286405 0.27817658 0.58286405 0.27817658 0.61048543 0.26673543 0.63913572
		 0.19756767 0.6476692 0.17696601 0.64194858 0.19077671 0.63622797 0.20458749 0.63335681
		 0.211519 0.63622797 0.20458737 0.63050747 0.21839806 0.63050735 0.21839818 0.62757885
		 0.22546823 0.62478685 0.23220873 0.61906624 0.24601941 0.62478679 0.23220885 0.61774057
		 0.24922004 0.61334574 0.25983009 0.62192655 0.2391142 0.62192655 0.2391142 0.62192655
		 0.23911408 0.63336766 0.21149284 0.64480883 0.18387149 0.64480889 0.18387136 0.65625
		 0.15625 0.62757885 0.087031797 0.61906624 0.066480599 0.62478685 0.080291279 0.63050735
		 0.094101831 0.63335681 0.10098098 0.63050747 0.094101951 0.63622797 0.10791263 0.63622797
		 0.1079125 0.63913572 0.11493235 0.64194858 0.1217233 0.6476692 0.13553399 0.64194852
		 0.12172318 0.64907414 0.13892591 0.65338969 0.14934465 0.64480877 0.12862852 0.64480883
		 0.12862851;
	setAttr ".uvst[0].uvsp[500:749]" 0.64480889 0.12862864 0.63336766 0.10100716
		 0.62192655 0.07338582 0.62192655 0.073385939 0.51131094 0.0046851397 0.51381063 0.0057205632
		 0.5 1.4901161e-08 0.50882286 0.0036545619 0.52263349 0.0093751103 0.40082568 0.041079398
		 0.40332532 0.040043984 0.38951457 0.04576458 0.39833763 0.042109959 0.41214839 0.036389362
		 0.34840688 0.1450073 0.34947059 0.14243928 0.34375 0.15625 0.3473776 0.1474922 0.35309818
		 0.13368148 0.38489848 0.25559124 0.38379401 0.25292474 0.38951457 0.26673543 0.38592666
		 0.25807345 0.38020611 0.24426278 0.48892385 0.30791211 0.48618942 0.30677947 0.5
		 0.3125 0.49140847 0.30894127 0.47759789 0.30322075 0.59940916 0.27132332 0.59667474
		 0.27245599 0.61048543 0.26673543 0.60189384 0.27029419 0.58808315 0.27601475 0.65163392
		 0.16739427 0.65052938 0.17006074 0.65625 0.15625 0.65266204 0.16491205 0.64694142
		 0.17872277 0.61514229 0.057007149 0.61620599 0.059575193 0.61048543 0.04576458 0.61411297
		 0.054522283 0.61983359 0.068332896 0.52752501 0.011401227 0.52416861 0.010010991
		 0.51726341 0.0071507283 0.53097486 0.012830189 0.53788006 0.015690453 0.54829425
		 0.020004159 0.54488462 0.018591847 0.54488468 0.01859187 0.55175841 0.021439046 0.55175829
		 0.021439023 0.56906492 0.02860768 0.56560069 0.027172703 0.57250607 0.030033011 0.57254452
		 0.030048952 0.56563914 0.027188644 0.59235269 0.038253728 0.5863167 0.035753559 0.60012746
		 0.041474152 0.59844118 0.040775687 0.58463043 0.035055093 0.59667474 0.040043984
		 0.59667468 0.040043987 0.61048543 0.04576458 0.59667468 0.040043976 0.58286393 0.034323383
		 0.54143196 0.01716171 0.55524266 0.022882275 0.54143202 0.017161727 0.52762127 0.011441145
		 0.5414319 0.017161693 0.41703969 0.034363322 0.41368335 0.035753556 0.40677792 0.038613867
		 0.42048949 0.03293436 0.42739493 0.030074047 0.43780905 0.025760392 0.43439934 0.0271727
		 0.43439925 0.027172726 0.44127309 0.024325501 0.44127315 0.024325475 0.45857975 0.017156869
		 0.45511538 0.018591844 0.46202064 0.015731584 0.46205926 0.015715595 0.455154 0.018575855
		 0.48186728 0.0075108339 0.47583139 0.010010988 0.48964196 0.0042904429 0.48795581
		 0.0049888864 0.4741452 0.010709432 0.4861894 0.0057205539 0.4861894 0.00572056 0.5
		 1.4901161e-08 0.48618942 0.0057205465 0.47237882 0.011441091 0.43094668 0.02860285
		 0.44475737 0.022882272 0.43094659 0.028602868 0.41713595 0.034323424 0.43094674 0.028602829
		 0.35516909 0.12868185 0.35376102 0.13208126 0.35090071 0.13898665 0.35658723 0.12525827
		 0.35944754 0.11835288 0.36376294 0.10793439 0.36234188 0.11136524 0.36234188 0.1113653
		 0.3651967 0.10447299 0.36519673 0.10447294 0.37235662 0.087187536 0.37092271 0.090649232
		 0.37378299 0.083743945 0.37380677 0.08368665 0.37094647 0.09059193 0.38198748 0.063936546
		 0.37950361 0.069933213 0.38522416 0.05612259 0.38449651 0.057879191 0.37877598 0.071689814
		 0.38379407 0.059575193 0.38379401 0.059575204 0.38951457 0.04576458 0.38379401 0.059575204
		 0.37807345 0.073385827 0.36091173 0.11481789 0.36663231 0.10100724 0.36091173 0.11481796
		 0.35519114 0.12862858 0.36091173 0.11481785 0.37807846 0.23912618 0.37950358 0.24256673
		 0.38236386 0.24947208 0.37666702 0.23571867 0.37380672 0.22881332 0.36949229 0.21839739
		 0.37092268 0.22185072 0.37092268 0.22185072 0.36805701 0.21493231 0.36805701 0.21493231
		 0.3609066 0.19766958 0.36234188 0.20113471 0.35948157 0.19422936 0.35944748 0.19414702
		 0.36230776 0.20105237 0.35129505 0.17446545 0.35376099 0.1804187 0.3480404 0.16660801
		 0.34880769 0.16846043 0.35452828 0.18227109 0.34947056 0.17006066 0.34947056 0.17006068
		 0.34375 0.15625 0.34947056 0.17006069 0.35519114 0.18387137 0.37235284 0.22530337
		 0.36663228 0.21149272 0.37235284 0.2253034 0.37807342 0.23911406 0.37235284 0.22530338
		 0.47236484 0.30105317 0.47583139 0.30248904 0.48273665 0.30534929 0.46896464 0.29964471
		 0.46205938 0.29678446 0.4516477 0.29247183 0.45511538 0.29390818 0.45511532 0.29390812
		 0.44817844 0.29103482 0.44817853 0.29103488 0.43093064 0.28389052 0.43439937 0.28532735
		 0.42749393 0.28246701 0.42739487 0.28242594 0.4343003 0.28528631 0.4077439 0.27428624
		 0.41368335 0.27674645 0.3998726 0.27102584 0.40179032 0.27182019 0.41560107 0.2775408
		 0.40332532 0.27245605 0.40332532 0.27245605 0.38951457 0.26673543 0.40332532 0.27245605
		 0.41713607 0.27817667 0.4585681 0.29533836 0.44475737 0.28961775 0.45856798 0.29533827
		 0.47237879 0.30105892 0.45856819 0.29533839 0.58285016 0.27818233 0.5863167 0.27674642
		 0.59322202 0.27388614 0.57945001 0.27959079 0.57254469 0.28245103 0.56213307 0.28676367
		 0.56560075 0.28532732 0.56560075 0.28532732 0.55866385 0.28820068 0.55866385 0.28820068
		 0.54141599 0.29534492 0.54488468 0.29390812 0.53797936 0.29676843 0.53788018 0.29680952
		 0.5447855 0.29394925 0.51822919 0.30494925 0.52416873 0.30248901 0.51035798 0.3082096
		 0.51227558 0.30741531 0.52608633 0.30169472 0.51381063 0.30677938 0.51381069 0.30677944
		 0.5 0.3125 0.51381069 0.30677941 0.52762139 0.30105883 0.56905341 0.28389713 0.55524272
		 0.28961772 0.56905341 0.28389716 0.58286405 0.27817658 0.56905341 0.28389716 0.6448139
		 0.18385938 0.64623898 0.18041876 0.64909935 0.17351335 0.64340246 0.18726684 0.64054215
		 0.19417225 0.63622773 0.20458812 0.63765812 0.20113477 0.63765812 0.20113471 0.63479239
		 0.20805319 0.63479239 0.20805325 0.62764198 0.22531591 0.62907726 0.22185078 0.62621701
		 0.22875607 0.62618285 0.22883847 0.6290431 0.22193322 0.61803049 0.24852005 0.62049639
		 0.24256679 0.6147759 0.2563774 0.61554313 0.25452507 0.62126368 0.24071445 0.61620593
		 0.25292486 0.61620599 0.2529248 0.61048543 0.26673543 0.61620599 0.2529248 0.62192655
		 0.2391142 0.63908827 0.19768211;
	setAttr ".uvst[0].uvsp[750:999]" 0.63336766 0.21149278 0.63908827 0.19768204
		 0.64480889 0.18387142 0.63908827 0.19768217 0.62190443 0.073332593 0.62049639 0.069933206
		 0.61763608 0.063027933 0.62332255 0.076756194 0.62618279 0.083661474 0.63049841 0.094080061
		 0.62907726 0.090649225 0.62907732 0.090649284 0.63193214 0.097541466 0.63193208 0.097541407
		 0.63909209 0.11482693 0.63765812 0.11136523 0.64051843 0.11827064 0.64054215 0.11832783
		 0.63768184 0.11142242 0.64872295 0.13807791 0.64623898 0.13208124 0.65195954 0.14589199
		 0.65123188 0.14413528 0.64551133 0.13032454 0.65052938 0.14243923 0.65052938 0.14243926
		 0.65625 0.15625 0.65052938 0.14243925 0.64480877 0.12862852 0.6276471 0.087196536
		 0.63336766 0.10100722 0.62764716 0.087196618 0.62192655 0.073385879 0.6276471 0.087196492
		 0.50626165 0.0025936682 0.5 1.4901161e-08 0.50345266 0.0014301576 0.51227552 0.0050847046
		 0.51822907 0.007550762 0.51035798 0.0042904429 0.52608615 0.010805253 0.53401637
		 0.014090045 0.54488456 0.018591803 0.5414319 0.01716166 0.39577639 0.043170858 0.38951457
		 0.04576458 0.39296722 0.044334438 0.40179032 0.040679816 0.4077439 0.038213771 0.3998726
		 0.041474152 0.41560107 0.034959219 0.42353144 0.031674352 0.43439955 0.027172647
		 0.43094683 0.02860279 0.34631702 0.15005264 0.34375 0.15625 0.34518015 0.15279733
		 0.34880775 0.14403953 0.35129511 0.13803451 0.3480404 0.14589199 0.35452834 0.1302288
		 0.35779285 0.12234762 0.36234194 0.11136514 0.36091179 0.11481781 0.38698682 0.2606329
		 0.38951457 0.26673543 0.38808441 0.26328278 0.38449654 0.25462079 0.38198745 0.24856338
		 0.38522416 0.2563774 0.37877595 0.2408101 0.37553239 0.23297949 0.37092271 0.22185072
		 0.37235284 0.2253034 0.49397036 0.31000245 0.5 0.3125 0.49654734 0.31106985 0.48795581
		 0.30751115 0.48186728 0.30498919 0.48964196 0.3082096 0.4741452 0.3017906 0.46634582
		 0.29855999 0.45511556 0.29390824 0.45856822 0.29533839 0.60445571 0.26923302 0.61048543
		 0.26673543 0.60703278 0.26816559 0.59844118 0.27172431 0.59235269 0.27424625 0.60012746
		 0.27102584 0.58463049 0.2774449 0.57683104 0.2806755 0.56560075 0.28532729 0.56905341
		 0.28389716 0.65372223 0.16235259 0.65625 0.15625 0.65481985 0.15970267 0.65123188
		 0.1683647 0.64872289 0.17442207 0.65195954 0.16660801 0.64551133 0.18217546 0.64226776
		 0.19000611 0.63765806 0.20113489 0.63908815 0.19768222 0.61305243 0.051961873 0.61048543
		 0.04576458 0.61191559 0.04921725 0.61554313 0.057974953 0.61803055 0.063979968 0.6147759
		 0.05612259 0.62126368 0.071785569 0.62452823 0.079666652 0.6290772 0.090649098 0.62764704
		 0.087196425 0.53432834 0.014219247 0.52416873 0.010011014 0.53107405 0.012871299
		 0.53788018 0.015690476 0.54141599 0.01715507 0.53797936 0.015731584 0.54478538 0.018550739
		 0.54825097 0.019986216 0.55178988 0.021452088 0.55517948 0.022856103 0.55179 0.021452155
		 0.55869544 0.02431244 0.55866373 0.024299331 0.56213301 0.025736338 0.56560075 0.027172726
		 0.55866373 0.024299309 0.55523723 0.022880035 0.5586952 0.024312373 0.57603568 0.031495057
		 0.57941139 0.032893296 0.5863167 0.035753582 0.57944983 0.032909237 0.5828501 0.034317669
		 0.59322202 0.038613867 0.57254446 0.030048929 0.56222701 0.025775293 0.56560051 0.027172659
		 0.60445577 0.043267 0.60703278 0.044334438 0.60189384 0.042205833 0.59940922 0.041176666
		 0.58808309 0.03648524 0.57683092 0.03182444 0.56905317 0.028602801 0.59667468 0.040043969
		 0.61048543 0.04576458 0.59667474 0.04004401 0.58286399 0.034323405 0.56905335 0.028602844
		 0.56905341 0.028602868 0.56905329 0.028602812 0.56905317 0.028602783 0.55524254 0.022882231
		 0.51381063 0.0057205795 0.51381069 0.0057205856 0.5 1.4901161e-08 0.51381063 0.0057205739
		 0.51381063 0.0057205688 0.52762127 0.011441123 0.5414319 0.017161679 0.42384297 0.031545307
		 0.41368324 0.035753582 0.42058861 0.032893296 0.42739484 0.030074073 0.43093061 0.028609479
		 0.42749393 0.030033011 0.43430024 0.027213762 0.43776596 0.025778236 0.44130486 0.024312362
		 0.44469419 0.022908445 0.44130462 0.02431244 0.44820994 0.021452155 0.44817841 0.021465216
		 0.45164767 0.020028211 0.45511532 0.01859187 0.4481785 0.02146519 0.44475225 0.022884419
		 0.44821018 0.021452077 0.46555048 0.014269492 0.46892595 0.012871299 0.47583133 0.010011014
		 0.46896461 0.012855311 0.47236481 0.011446877 0.48273665 0.0071507283 0.46205935
		 0.015715571 0.45174199 0.019989157 0.45511556 0.018591793 0.49397033 0.0024975892
		 0.49654734 0.0014301576 0.49140847 0.003558744 0.48892391 0.0045878985 0.47759786
		 0.0092792893 0.46634576 0.013940049 0.45856822 0.017161651 0.48618942 0.005720539
		 0.48618931 0.0057205856 0.47237873 0.011441117 0.45856798 0.017161695 0.45856798
		 0.017161727 0.45856816 0.017161662 0.45856819 0.017161626 0.44475752 0.022882219
		 0.40332526 0.040044002 0.40332526 0.04004401 0.38951457 0.04576458 0.40332529 0.040043995
		 0.40332532 0.040043991 0.41713604 0.034323402 0.4309468 0.028602812 0.35798326 0.12188793
		 0.35376102 0.13208131 0.35662127 0.12517597 0.35944748 0.11835293 0.36090654 0.11483037
		 0.35948157 0.11827064 0.36230779 0.11144754 0.36373091 0.10801177 0.36520219 0.10445979
		 0.36662143 0.1010334 0.36520213 0.10445996 0.36806244 0.097554624 0.36805701 0.097567648
		 0.36949229 0.094102569 0.37092268 0.090649284 0.36805701 0.097567603 0.36663079 0.10101091
		 0.3680625 0.09755446 0.37526065 0.080176733 0.3766433 0.076838613 0.37950355 0.069933265
		 0.37666702 0.076781303 0.3780784 0.073373809 0.38236386 0.063027933 0.37380677 0.08368659
		 0.36953142 0.09400823 0.3709228 0.090649121 0.38698679 0.05186709 0.38808441 0.04921725
		 0.38592666 0.054426521 0.38489851 0.056908708 0.38020611 0.068237141 0.37553245 0.079520367
		 0.3723529 0.087196454 0.38379404 0.059575215 0.38379401 0.05957526 0.37807345 0.073385879;
	setAttr ".uvst[0].uvsp[1000:1249]" 0.37235287 0.087196544 0.37235284 0.087196618
		 0.3723529 0.087196499 0.37235293 0.087196477 0.36663234 0.10100713 0.34947062 0.14243928
		 0.34947056 0.14243932 0.34375 0.15625 0.34947056 0.14243926 0.34947059 0.14243925
		 0.35519117 0.12862852 0.36091173 0.1148178 0.37526059 0.23232324 0.37950355 0.24256673
		 0.3766433 0.2356614 0.37380672 0.22881332 0.37235662 0.22531243 0.37378299 0.22875607
		 0.37094644 0.22190799 0.36953133 0.21849164 0.36806244 0.21494539 0.36662143 0.21146657
		 0.36806244 0.21494539 0.36520213 0.20804004 0.3651967 0.20802696 0.36376294 0.20456554
		 0.36234188 0.20113471 0.3651967 0.20802696 0.3666307 0.21148893 0.36520213 0.20804004
		 0.35798326 0.19061203 0.35662127 0.18732403 0.35376102 0.18041869 0.35658717 0.18724167
		 0.35516912 0.1838181 0.35090071 0.17351335 0.35944748 0.19414702 0.36373085 0.20448807
		 0.36234185 0.20113471 0.34631702 0.16244733 0.34518015 0.15970267 0.34737757 0.16500774
		 0.34840682 0.16749263 0.35309815 0.17881843 0.35779274 0.19015223 0.36091173 0.19768204
		 0.34947056 0.17006069 0.34947056 0.17006068 0.35519114 0.18387137 0.36091167 0.19768204
		 0.36091173 0.19768204 0.36091173 0.19768205 0.36091176 0.19768207 0.36663228 0.21149272
		 0.38379401 0.25292477 0.38379401 0.25292474 0.38951457 0.26673543 0.38379401 0.25292474
		 0.38379401 0.25292477 0.37807342 0.23911406 0.37235287 0.22530338 0.46555051 0.2982305
		 0.47583133 0.30248898 0.46892595 0.29962873 0.46205929 0.29678443 0.45857975 0.29534316
		 0.46202064 0.29676843 0.45515406 0.29392418 0.45174205 0.29251087 0.44821024 0.29104799
		 0.44469422 0.28959161 0.44820994 0.29104787 0.44130462 0.28818756 0.44127309 0.28817454
		 0.43780893 0.28673962 0.43439925 0.28532732 0.44127318 0.28817457 0.44475225 0.28961563
		 0.44130486 0.28818768 0.42384294 0.28095469 0.42058861 0.2796067 0.41368324 0.27674645
		 0.42048952 0.27956569 0.41703966 0.2781367 0.40677792 0.27388614 0.42739496 0.282426
		 0.43776599 0.2867218 0.43439955 0.28532737 0.39577639 0.26932916 0.39296722 0.26816559
		 0.39833766 0.27039006 0.40082565 0.27142066 0.41214842 0.27611068 0.42353147 0.28082567
		 0.43094689 0.28389728 0.40332529 0.27245605 0.40332526 0.27245599 0.41713598 0.27817661
		 0.43094671 0.28389719 0.43094659 0.28389716 0.43094677 0.28389722 0.43094686 0.28389725
		 0.44475755 0.28961784 0.48618945 0.3067795 0.48618931 0.30677944 0.5 0.3125 0.48618945
		 0.3067795 0.4861894 0.30677947 0.47237885 0.30105895 0.45856827 0.29533842 0.57603586
		 0.28100494 0.5863167 0.27674645 0.57941139 0.2796067 0.57254469 0.28245103 0.56906515
		 0.28389233 0.57250607 0.28246701 0.56563938 0.28531134 0.56222731 0.28672469 0.55869544
		 0.28818756 0.5551796 0.28964394 0.55869544 0.28818756 0.55179 0.29104787 0.55175847
		 0.29106098 0.54829431 0.29249585 0.54488468 0.29390812 0.55175847 0.29106098 0.55523741
		 0.28961989 0.55179 0.29104787 0.53432834 0.29828078 0.53107405 0.29962873 0.52416873
		 0.30248898 0.53097486 0.2996698 0.52752507 0.30109879 0.51726341 0.30534929 0.53788018
		 0.29680952 0.54825109 0.29251373 0.54488468 0.29390815 0.50626177 0.30990636 0.50345266
		 0.31106985 0.50882292 0.30884543 0.51131099 0.30781487 0.52263367 0.30312485 0.53401655
		 0.29840985 0.54143202 0.29533827 0.51381069 0.30677941 0.51381069 0.30677944 0.52762139
		 0.30105883 0.54143202 0.29533824 0.54143202 0.29533827 0.54143202 0.29533827 0.54143202
		 0.29533824 0.55524272 0.28961772 0.59667474 0.27245602 0.59667474 0.27245599 0.61048543
		 0.26673543 0.59667474 0.27245599 0.59667474 0.27245602 0.58286405 0.27817658 0.56905341
		 0.28389716 0.64199603 0.19066228 0.64623904 0.18041869 0.64337873 0.18732403 0.64054215
		 0.19417219 0.63909203 0.19767307 0.64051843 0.19422936 0.63768184 0.20107758 0.63626665
		 0.20449397 0.63479781 0.20804022 0.63335681 0.21151891 0.63479781 0.20804004 0.63193762
		 0.21494539 0.63193214 0.21495853 0.63049835 0.21841992 0.62907732 0.22185072 0.63193208
		 0.21495859 0.63336611 0.21149667 0.6319375 0.21494557 0.62471861 0.23237349 0.6233567
		 0.2356614 0.62049639 0.24256673 0.62332255 0.23574382 0.62190449 0.23916741 0.61763608
		 0.24947208 0.62618279 0.22883853 0.63046622 0.21849753 0.6290772 0.2218509 0.61305249
		 0.26053816 0.61191559 0.26328278 0.61411297 0.25797772 0.61514223 0.25549287 0.61983359
		 0.24416712 0.62452817 0.23283337 0.62764704 0.22530358 0.61620599 0.2529248 0.61620599
		 0.25292474 0.62192655 0.23911414 0.6276471 0.22530346 0.62764716 0.2253034 0.6276471
		 0.22530352 0.62764704 0.22530355 0.63336766 0.2114929 0.65052938 0.17006072 0.65052944
		 0.17006068 0.65625 0.15625 0.65052938 0.17006075 0.65052933 0.17006074 0.64480877
		 0.18387148 0.63908821 0.1976822 0.62471861 0.080126539 0.62049639 0.069933265 0.6233567
		 0.076838613 0.62618285 0.083661541 0.62764192 0.087184094 0.62621701 0.083743945
		 0.6290431 0.090566814 0.63046622 0.094002455 0.6319375 0.09755443 0.63335681 0.10098105
		 0.63193762 0.097554624 0.63479781 0.10445996 0.63479239 0.10444681 0.63622773 0.10791189
		 0.63765812 0.1113653 0.63479239 0.10444674 0.63336605 0.10100332 0.63479781 0.10445976
		 0.64199603 0.12183775 0.64337873 0.12517597 0.64623904 0.13208131 0.64340246 0.12523317
		 0.64481384 0.12864062 0.64909935 0.13898665 0.64054215 0.11832777 0.63626665 0.10800599
		 0.63765806 0.11136511 0.65372223 0.15014742 0.65481985 0.15279733 0.65266204 0.14758795
		 0.65163386 0.14510573 0.64694142 0.13377722 0.6422677 0.12249388 0.63908815 0.11481778
		 0.65052944 0.14243928 0.65052944 0.14243932 0.64480889 0.12862858 0.63908827 0.11481788
		 0.63908827 0.11481796 0.63908827 0.11481784 0.63908821 0.1148178 0.63336766 0.1010071;
	setAttr ".uvst[0].uvsp[1250:1499]" 0.61620605 0.059575222 0.61620599 0.05957526
		 0.61620599 0.0595752 0.61620599 0.059575174 0.62192655 0.073385812 0.6276471 0.087196447
		 0.51884419 0.0078055155 0.52512741 0.010408111 0.52071595 0.0085808374 0.51256078
		 0.0052028513 0.51697218 0.0070301248 0.40835896 0.037958995 0.41464221 0.035356373
		 0.4102307 0.037183687 0.4020755 0.040561691 0.40648705 0.03873438 0.35154191 0.13743867
		 0.35414469 0.13115501 0.35233086 0.1355339 0.34893873 0.14372328 0.35075253 0.13934439
		 0.38174301 0.24797326 0.37913978 0.24168843 0.3809337 0.24601941 0.38434625 0.25425798
		 0.3825523 0.24992701 0.48127314 0.30474305 0.47498834 0.30213985 0.47928411 0.3039192
		 0.48755664 0.30734581 0.48326087 0.30556643 0.59175837 0.27449244 0.5854736 0.27709568
		 0.58976936 0.2753163 0.59804195 0.27188966 0.59374619 0.27366903 0.64847845 0.17501225
		 0.6458751 0.18129712 0.64766908 0.1769661 0.65108168 0.1687275 0.6492877 0.17305851
		 0.61827731 0.064575724 0.62088007 0.07085935 0.61906624 0.066480502 0.61567414 0.058291171
		 0.61748791 0.062670022 0.5310232 0.012850239 0.53620332 0.014995925 0.5293476 0.012156194
		 0.52584684 0.010706109 0.53270257 0.01354584 0.54831684 0.020013493 0.55004776 0.020730495
		 0.54661095 0.019306907 0.54658943 0.019298002 0.5500263 0.020721592 0.56561142 0.02717714
		 0.56389356 0.026465591 0.56387436 0.026457621 0.5673328 0.02789019 0.56735206 0.027898163
		 0.584813 0.035130717 0.58029449 0.033259101 0.58113766 0.033608332 0.58933473 0.037003644
		 0.58849156 0.036654413 0.5897693 0.037183691 0.58286393 0.034323387 0.5897693 0.037183691
		 0.59667468 0.040043987 0.58976936 0.037183683 0.54833722 0.020021982 0.54833722 0.020021973
		 0.55524266 0.022882264 0.54833734 0.020021994 0.5414319 0.017161701 0.42053801 0.032914285
		 0.42571819 0.030768573 0.4188624 0.033608329 0.41536152 0.035058439 0.42221731 0.032218684
		 0.43783158 0.025751032 0.43956262 0.025034003 0.4361257 0.026457615 0.43610418 0.026466545
		 0.4395411 0.025042932 0.45512617 0.018587384 0.45340839 0.019298907 0.45338908 0.019306902
		 0.45684755 0.017874356 0.45686686 0.017866362 0.47432777 0.010633815 0.46980935 0.012505411
		 0.47065243 0.012156188 0.47884935 0.0087609105 0.47800624 0.0091101332 0.47928411
		 0.0085808272 0.47237879 0.011441099 0.47928411 0.0085808327 0.4861894 0.0057205567
		 0.47928411 0.0085808225 0.437852 0.025742546 0.43785208 0.025742538 0.4447574 0.022882259
		 0.43785203 0.025742561 0.43094671 0.028602839 0.35660729 0.12520979 0.35874951 0.12003803
		 0.35590625 0.12690222 0.35446507 0.13038155 0.35730833 0.12351736 0.36376706 0.1079244
		 0.36448437 0.10619272 0.36305696 0.10963888 0.36305243 0.10964982 0.36447984 0.10620366
		 0.37092689 0.090639219 0.37021953 0.092346892 0.37020767 0.092375539 0.37163967 0.088918388
		 0.37165153 0.088889733 0.37886593 0.071472764 0.37699461 0.075990491 0.37735841 0.075112194
		 0.38074553 0.066934884 0.38038173 0.06781318 0.38093376 0.06648051 0.37807345 0.073385827
		 0.38093373 0.066480517 0.38379404 0.0595752 0.38093376 0.06648051 0.36377206 0.10791254
		 0.36377203 0.10791251 0.36663234 0.10100721 0.36377203 0.10791257 0.36091173 0.11481787
		 0.3766472 0.23567091 0.37450993 0.23051102 0.37735838 0.23738773 0.37879103 0.24084646
		 0.37594259 0.23396975 0.36949199 0.21839674 0.36877477 0.21666518 0.37020761 0.22012439
		 0.37020749 0.22012407 0.36877465 0.21666485 0.3623372 0.2011233 0.36303988 0.20281987
		 0.36305696 0.20286104 0.36162424 0.19940215 0.36160719 0.19936097 0.35441595 0.18200003
		 0.35628986 0.1865239 0.35590619 0.1855977 0.35252804 0.17744207 0.35291165 0.17836827
		 0.35233086 0.17696603 0.35519114 0.18387136 0.35233086 0.17696601 0.34947056 0.17006066
		 0.35233086 0.17696601 0.36949253 0.21839805 0.36949256 0.21839805 0.36663228 0.21149272
		 0.36949256 0.21839803 0.37235284 0.22530338 0.46891606 0.29962459 0.46376643 0.29749155
		 0.47065243 0.30034384 0.47409812 0.3017711 0.46721211 0.29891881 0.45165148 0.29247341
		 0.44992065 0.29175648 0.45338911 0.29319313 0.45338154 0.29319 0.44991311 0.29175335
		 0.43438709 0.28532222 0.43607622 0.28602192 0.43612576 0.28604245 0.43266499 0.28460893
		 0.43261546 0.2845884 0.4152748 0.27740568 0.41982126 0.27928886 0.4188624 0.27889168
		 0.41071361 0.27551633 0.41167247 0.27591354 0.41023073 0.27531636 0.41713607 0.27817667
		 0.4102307 0.27531636 0.40332532 0.27245605 0.4102307 0.27531636 0.45166281 0.29247808
		 0.45166284 0.29247808 0.4447574 0.28961778 0.45166272 0.29247805 0.45856816 0.29533839
		 0.57940137 0.27961084 0.57425171 0.28174394 0.58113772 0.27889162 0.5845834 0.27746439
		 0.5776974 0.28031668 0.56213689 0.28676212 0.56040597 0.28747904 0.56387442 0.28604239
		 0.56386691 0.28604549 0.56039846 0.28748217 0.54487234 0.29391325 0.54656142 0.29321361
		 0.54661101 0.29319304 0.54315031 0.29462653 0.54310071 0.2946471 0.52576005 0.30182984
		 0.53030652 0.29994667 0.52934772 0.30034381 0.52119899 0.30371913 0.52215779 0.30332199
		 0.52071607 0.30391914 0.52762139 0.30105883 0.52071607 0.30391914 0.51381063 0.30677941
		 0.52071601 0.30391911 0.56214809 0.28675744 0.56214809 0.28675744 0.55524272 0.28961772
		 0.56214809 0.28675741 0.56905341 0.28389716 0.64338267 0.18731464 0.64124537 0.19247454
		 0.64409375 0.18559779 0.64552641 0.18213907 0.64267802 0.18901581 0.63622749 0.20458879
		 0.63551021 0.20632038 0.63694304 0.20286113 0.63694292 0.20286144 0.63551009 0.20632067
		 0.62907249 0.22186226 0.62977523 0.2201657 0.62979233 0.22012448 0.62835962 0.22358334
		 0.62834251 0.22362456 0.62115145 0.24098553 0.62302524 0.23646164 0.62264156 0.23738782
		 0.61926341 0.24554342 0.61964709 0.24461725 0.61906624 0.24601951 0.62192655 0.2391142
		 0.61906624 0.2460195 0.61620593 0.25292483;
	setAttr ".uvst[0].uvsp[1500:1749]" 0.61906624 0.24601953 0.63622797 0.20458747
		 0.63622797 0.2045875 0.63336766 0.21149281 0.63622797 0.20458744 0.63908827 0.19768214
		 0.62334257 0.076804623 0.62548482 0.081976309 0.62264156 0.075112179 0.62120044 0.071632899
		 0.62404358 0.078497037 0.63050246 0.094090007 0.63121974 0.095821619 0.62979233 0.092375532
		 0.6297878 0.092364639 0.63121521 0.095810734 0.63766223 0.11137518 0.6369549 0.10966746
		 0.63694304 0.10963886 0.6383751 0.11309607 0.63838696 0.11312467 0.64560127 0.13054164
		 0.64372993 0.12602386 0.64409375 0.12690221 0.64748096 0.13507958 0.64711714 0.13420123
		 0.64766902 0.13553387 0.64480877 0.12862852 0.64766908 0.1355339 0.65052938 0.14243925
		 0.64766908 0.13553387 0.63050741 0.094101854 0.63050735 0.094101831 0.63336766 0.10100719
		 0.63050735 0.094101876 0.6276471 0.087196514 0.50377077 0.0015619351 0.50441146 0.0018272884
		 0.5 1.4901161e-08 0.50313079 0.0012968415 0.50754225 0.0031241151 0.51194626 0.004948325
		 0.50959039 0.0039725024 0.50863165 0.0035753716 0.51429355 0.0059206025 0.51525229
		 0.0063177333 0.53488636 0.014450409 0.53030634 0.012553325 0.53970557 0.0164466 0.53945047
		 0.016340924 0.53005123 0.01244765 0.39328545 0.044202622 0.39392608 0.04393727 0.38951457
		 0.04576458 0.39264548 0.044467717 0.397057 0.04264041 0.40146101 0.04081624 0.39910513
		 0.041792057 0.39814627 0.042189226 0.40380824 0.039843962 0.4047671 0.039446793 0.4244014
		 0.031314045 0.41982126 0.03321116 0.4292205 0.029317874 0.42896551 0.029423499 0.41956627
		 0.033316784 0.3452979 0.15251304 0.3455638 0.1518711 0.34375 0.15625 0.34503353 0.15315132
		 0.3468473 0.14877242 0.34869418 0.14431362 0.347709 0.1466921 0.34732533 0.14761832
		 0.34966776 0.14196324 0.35005143 0.14103702 0.35818663 0.121397 0.35628992 0.125976
		 0.36019671 0.11654417 0.3600674 0.11685638 0.35616058 0.12628821 0.38798657 0.26304662
		 0.38772061 0.26240444 0.38951457 0.26673543 0.38825071 0.26368415 0.38645673 0.25935316
		 0.38458562 0.25483578 0.38557541 0.25722545 0.38593924 0.25810373 0.38360581 0.25247037
		 0.38324201 0.2515921 0.37510556 0.23194911 0.37699455 0.23650941 0.37306792 0.22702973
		 0.37322754 0.22741511 0.37715417 0.23689479 0.49634525 0.31098616 0.49570423 0.31072062
		 0.5 0.3125 0.4969852 0.31125122 0.49268943 0.30947185 0.48813722 0.30758631 0.49052525
		 0.30857545 0.49136829 0.30892467 0.48575461 0.30659938 0.48491156 0.30625015 0.46526775
		 0.29811347 0.46980935 0.29999462 0.46029451 0.29605344 0.46073067 0.29623413 0.47024551
		 0.30017531 0.60683066 0.26824927 0.60618961 0.26851481 0.61048543 0.26673543 0.60747057
		 0.26798421 0.60317481 0.26976359 0.59862268 0.27164915 0.60101068 0.27066001 0.60185379
		 0.27031076 0.59624004 0.27263606 0.59539694 0.27298528 0.57575303 0.28112206 0.58029461
		 0.27924088 0.57077974 0.28318208 0.57121587 0.28300139 0.5807308 0.27906018 0.65472203
		 0.15993886 0.65445602 0.16058102 0.65625 0.15625 0.65498614 0.1593013 0.65319216
		 0.16363232 0.65132099 0.16814968 0.65231079 0.16576003 0.65267462 0.16488168 0.65034121
		 0.17051503 0.64997739 0.17139339 0.64184099 0.19103646 0.64372993 0.18647614 0.63980329
		 0.19595586 0.63996291 0.1955705 0.64388955 0.18609078 0.61203337 0.049501501 0.6122992
		 0.050143432 0.61048543 0.04576458 0.61176896 0.048863225 0.61358273 0.05324208 0.61542964
		 0.057700906 0.61444443 0.055322438 0.61406082 0.054396257 0.61640322 0.060051277
		 0.61678684 0.060977459 0.62492192 0.080617309 0.62302524 0.076038361 0.62693202 0.085470125
		 0.62680268 0.085157871 0.62289596 0.075726107 0.52752358 0.011400614 0.52584547 0.010705531
		 0.5224424 0.0092959423 0.52924854 0.01211513 0.5326516 0.013524719 0.53796691 0.015726432
		 0.53620344 0.014995959 0.53625304 0.015016513 0.53969765 0.016443327 0.53964806 0.016422773
		 0.54829389 0.020004043 0.5465613 0.019286353 0.55006355 0.020737028 0.55002046 0.020719152
		 0.54651821 0.019268477 0.55175257 0.021436648 0.55004787 0.020730529 0.55006367 0.020737084
		 0.55348474 0.02215413 0.55346894 0.022147574 0.56213677 0.025737887 0.56040591 0.025020957
		 0.56387442 0.026457654 0.56386685 0.026454531 0.56039834 0.025017835 0.55868608 0.024308586
		 0.56040585 0.025020923 0.56042159 0.025027456 0.55696619 0.023596205 0.55695045 0.023589671
		 0.57598364 0.031473499 0.57425165 0.030756053 0.57768506 0.032178223 0.5777235 0.032194175
		 0.5742901 0.030772004 0.58630663 0.035749409 0.58460963 0.035046481 0.59149569 0.037898794
		 0.58803606 0.036465768 0.58114994 0.033613451 0.5690794 0.028613653 0.57425153 0.030756019
		 0.57077956 0.029317884 0.56391376 0.026473977 0.56738573 0.02791211 0.60337627 0.04281985
		 0.60101068 0.041839991 0.60530645 0.043619365 0.60574424 0.043800719 0.60144848 0.042021342
		 0.60556948 0.043728348 0.60618961 0.043985207 0.61048543 0.04576458 0.60494733 0.043470621
		 0.6006515 0.041691251 0.57920539 0.03280801 0.58547354 0.035404317 0.57595855 0.031463098
		 0.57294202 0.030213621 0.58245701 0.03415484 0.60358006 0.04290428 0.60358006 0.04290428
		 0.61048543 0.04576458 0.60358006 0.042904273 0.59667468 0.040043972 0.57595873 0.031463139
		 0.58286405 0.034323424 0.57595873 0.031463154 0.56905341 0.028602857 0.57595867 0.031463124
		 0.56214792 0.025742522 0.56214797 0.025742533 0.55524254 0.022882242 0.56214786 0.025742507
		 0.56905323 0.028602798 0.52071601 0.0085808681 0.52762133 0.011441151 0.52071607
		 0.0085808709 0.51381063 0.0057205828 0.52071595 0.0085808625 0.50690532 0.0028602933
		 0.50690532 0.0028602944 0.5 1.4901161e-08 0.50690532 0.0028602919 0.51381063 0.0057205716
		 0.53452659 0.014301394 0.52762127 0.011441117 0.53452659 0.014301386 0.5414319 0.017161669
		 0.53452659 0.014301401 0.41703808 0.034363963 0.41536003 0.035059042 0.41195691 0.036468655
		 0.4187631 0.033649445;
	setAttr ".uvst[0].uvsp[1750:1999]" 0.42216623 0.032239832 0.42748159 0.030038141
		 0.42571807 0.030768614 0.4257676 0.030748082 0.42921227 0.029321246 0.42916274 0.029341776
		 0.4378089 0.025760429 0.43607616 0.026478145 0.43957847 0.025027446 0.43953541 0.025045298
		 0.4360331 0.026495999 0.44126728 0.024327926 0.4395625 0.025034042 0.43957829 0.025027512
		 0.44299942 0.023610443 0.44298363 0.023616973 0.45165133 0.020026684 0.44992054 0.020743614
		 0.45338899 0.019306941 0.45338148 0.01931004 0.44991302 0.020746714 0.448201 0.02145589
		 0.44992065 0.020743575 0.44993648 0.020737018 0.44648123 0.022168249 0.44646537 0.022174805
		 0.46549842 0.01429107 0.46376628 0.015008518 0.46719962 0.01358637 0.46723822 0.013570395
		 0.46380487 0.014992543 0.47582135 0.01001516 0.47412431 0.01071809 0.48101032 0.0078657996
		 0.47755075 0.0092988033 0.47066471 0.012151094 0.45859432 0.017150816 0.4637664 0.015008481
		 0.46029451 0.016446592 0.45342878 0.019290475 0.45690066 0.017852364 0.49289083 0.00294474
		 0.49052525 0.0039245933 0.49482101 0.0021452289 0.49525884 0.0019638734 0.49096307
		 0.0037432378 0.49508414 0.0020362337 0.49570423 0.0017793794 0.5 1.4901161e-08 0.49446195
		 0.0022939567 0.49016619 0.0040733214 0.46872032 0.012956485 0.47498834 0.010360197
		 0.4654735 0.014301378 0.462457 0.01555085 0.47197181 0.01160967 0.49309471 0.0028602802
		 0.49309471 0.0028602807 0.5 1.4901161e-08 0.49309471 0.002860277 0.48618942 0.0057205427
		 0.46547329 0.014301424 0.47237867 0.011441137 0.46547329 0.014301442 0.45856798 0.017161712
		 0.46547335 0.014301406 0.45166281 0.02002194 0.45166281 0.020021953 0.44475749 0.022882232
		 0.45166284 0.020021923 0.45856816 0.017161645 0.41023064 0.037183717 0.41713595 0.034323432
		 0.41023058 0.037183724 0.40332526 0.040044006 0.41023061 0.037183713 0.39641991 0.042904288
		 0.39641994 0.042904288 0.38951457 0.04576458 0.39641994 0.042904288 0.40332532 0.040043995
		 0.42404145 0.031463094 0.41713607 0.034323394 0.42404145 0.031463087 0.43094683 0.028602801
		 0.42404142 0.031463109 0.35516304 0.1286965 0.35445905 0.13039613 0.35304594 0.13380766
		 0.35587215 0.12698463 0.35728526 0.12357309 0.35947666 0.11828253 0.35874945 0.12003811
		 0.3587665 0.11999696 0.36019406 0.11655051 0.36017701 0.11659165 0.36375579 0.10795172
		 0.36303991 0.10968003 0.36448711 0.10618615 0.36446655 0.10623579 0.36301935 0.10972966
		 0.36519489 0.10447747 0.36448437 0.10619281 0.36448705 0.10618629 0.36591178 0.10274668
		 0.36590907 0.1027532 0.36949226 0.09410271 0.36877477 0.095834799 0.37020761 0.092375621
		 0.37020749 0.092375927 0.36877465 0.095835105 0.36806098 0.097558223 0.3687748 0.095834725
		 0.36877754 0.095828153 0.36734664 0.099282682 0.3673439 0.099289253 0.37522689 0.080258198
		 0.37450996 0.081988961 0.37592822 0.078564942 0.37595198 0.078507677 0.37453371 0.081931695
		 0.37950823 0.06992203 0.37880036 0.071630955 0.38164878 0.064754263 0.38022113 0.068200871
		 0.37737271 0.075077556 0.37236637 0.087164074 0.37450999 0.081988879 0.373068 0.08547014
		 0.3702271 0.092328675 0.37166911 0.088847414 0.38655367 0.052912798 0.38557541 0.055274554
		 0.38736933 0.050943583 0.3875356 0.050542168 0.38574165 0.054873139 0.38746506 0.050712474
		 0.38772061 0.050095551 0.38951457 0.04576458 0.38720655 0.051336646 0.38541257 0.055667616
		 0.37653562 0.077098534 0.37913978 0.07081148 0.37521318 0.080291137 0.37394267 0.083358407
		 0.37786928 0.07387875 0.38665435 0.052669901 0.38665429 0.05266989 0.38951457 0.04576458
		 0.38665432 0.052669898 0.38379401 0.059575208 0.37521315 0.080291241 0.37807345 0.073385909
		 0.37521315 0.080291279 0.37235284 0.087196581 0.37521315 0.080291212 0.36949262 0.094101816
		 0.36949262 0.094101839 0.36663234 0.10100715 0.36949265 0.094101802 0.3723529 0.087196484
		 0.35233086 0.13553394 0.35519114 0.12862861 0.35233086 0.13553399 0.34947059 0.14243931
		 0.35233086 0.13553393 0.34661028 0.14934462 0.34661028 0.14934462 0.34375 0.15625
		 0.34661031 0.14934462 0.34947056 0.14243925 0.35805148 0.12172316 0.35519117 0.12862852
		 0.35805148 0.12172318 0.36091176 0.1148178 0.35805145 0.12172316 0.37808776 0.23914874
		 0.37880036 0.24086905 0.38021863 0.24429306 0.37738207 0.237445 0.37596381 0.23402095
		 0.37378797 0.22876805 0.37450993 0.23051102 0.37449807 0.2304824 0.37306982 0.22703424
		 0.37308168 0.22706288 0.36950582 0.2184301 0.3702195 0.22015303 0.36877751 0.21667174
		 0.36879689 0.21671852 0.3702389 0.22019982 0.36805955 0.21493852 0.36877477 0.21666518
		 0.36877751 0.21667174 0.36734194 0.21320598 0.36733922 0.21319944 0.36376622 0.20457338
		 0.36448437 0.20630717 0.36305696 0.20286104 0.36305243 0.20285013 0.36447984 0.20629625
		 0.36519992 0.20803471 0.36448437 0.20630717 0.36448705 0.2063137 0.36591643 0.20976448
		 0.36591369 0.20975795 0.35803178 0.19072931 0.35874945 0.19246186 0.35733634 0.18905038
		 0.35730225 0.18896803 0.35871536 0.19237953 0.35374433 0.18037841 0.35445902 0.18210384
		 0.35161579 0.17523968 0.35303491 0.17866573 0.35587814 0.18552989 0.36089423 0.19763984
		 0.35874945 0.19246186 0.36019665 0.19595571 0.36303633 0.20281139 0.36158916 0.19931754
		 0.34673068 0.16344593 0.347709 0.16580789 0.34589523 0.16142902 0.3457486 0.161075
		 0.34756237 0.16545388 0.3458201 0.16124761 0.34556377 0.16062887 0.34375 0.15625
		 0.3460784 0.16187131 0.34789219 0.16625018 0.35675567 0.18764856 0.35414463 0.1813449
		 0.35805142 0.19077671 0.35935223 0.19391713 0.35544544 0.18448533 0.34661031 0.16315535
		 0.34661028 0.16315535 0.34375 0.15625 0.34661028 0.16315535 0.34947056 0.17006069
		 0.35805145 0.19077672 0.35519114 0.18387136 0.35805142 0.19077671 0.3609117 0.19768204
		 0.35805142 0.19077671 0.36377198 0.20458737 0.363772 0.20458739 0.36663228 0.21149272
		 0.36377203 0.2045874;
	setAttr ".uvst[0].uvsp[2000:2249]" 0.36091173 0.19768205 0.38093367 0.24601936
		 0.37807342 0.23911408 0.3809337 0.24601941 0.38379401 0.25292474 0.3809337 0.24601942
		 0.38665429 0.25983009 0.38665429 0.25983009 0.38951457 0.26673543 0.38665429 0.25983012
		 0.38379401 0.25292474 0.37521318 0.23220873 0.37807342 0.23911408 0.37521315 0.23220873
		 0.37235284 0.22530338 0.37521315 0.23220873 0.47239101 0.30106395 0.47412431 0.30178192
		 0.47755766 0.30320406 0.47069091 0.30035973 0.46725756 0.29893762 0.46203297 0.29677355
		 0.46376628 0.29749149 0.46374696 0.2974835 0.46030021 0.29605579 0.46031952 0.29606378
		 0.45168993 0.29248935 0.45340842 0.29320115 0.44993651 0.29176304 0.44997615 0.29177943
		 0.45344806 0.29321754 0.44818664 0.29103822 0.44992054 0.29175639 0.44993627 0.29176295
		 0.44645208 0.29031974 0.44643635 0.29031321 0.43782988 0.28674832 0.4395625 0.28746599
		 0.43612558 0.28604239 0.43610409 0.28603345 0.43954101 0.28745708 0.44129464 0.28818345
		 0.43956265 0.28746605 0.43957847 0.28747261 0.44302857 0.28890166 0.44301271 0.2888951
		 0.42398363 0.28101298 0.42571807 0.2817314 0.42231494 0.28032178 0.42221576 0.28028071
		 0.42561889 0.2816903 0.4136281 0.27672356 0.41536003 0.27744099 0.40850425 0.27460122
		 0.41190881 0.27601141 0.41876459 0.27885121 0.43090084 0.28387821 0.42571819 0.28173146
		 0.4292205 0.28318214 0.43608278 0.28602457 0.43258047 0.28457391 0.3967396 0.26972812
		 0.39910513 0.27070796 0.39469355 0.26888067 0.39437181 0.26874739 0.39878336 0.27057469
		 0.3945477 0.26882023 0.39392611 0.26856273 0.38951457 0.26673543 0.39517009 0.26907805
		 0.39958167 0.27090538 0.4209457 0.27975464 0.41464227 0.27714366 0.42404151 0.28103697
		 0.42723918 0.28236148 0.41783994 0.27846819 0.39641994 0.26959574 0.39641994 0.26959574
		 0.38951457 0.26673543 0.39641994 0.26959574 0.40332532 0.27245605 0.42404127 0.28103688
		 0.41713595 0.27817661 0.42404127 0.28103685 0.43094665 0.28389716 0.42404133 0.28103691
		 0.43785217 0.28675753 0.43785211 0.2867575 0.44475752 0.28961781 0.4378522 0.28675753
		 0.43094683 0.28389722 0.47928405 0.30391917 0.47237873 0.30105889 0.47928399 0.30391914
		 0.48618937 0.30677947 0.47928411 0.3039192 0.49309471 0.30963975 0.49309474 0.30963975
		 0.5 0.3125 0.49309468 0.30963975 0.48618942 0.3067795 0.46547353 0.29819867 0.47237885
		 0.30105895 0.46547353 0.29819867 0.45856825 0.29533839 0.46547356 0.2981987 0.58287638
		 0.27817154 0.58460969 0.27745354 0.58804303 0.27603137 0.58117628 0.27887571 0.57774293
		 0.28029788 0.57251847 0.28246191 0.57425171 0.28174394 0.5742324 0.28175193 0.57078564
		 0.28317967 0.57080495 0.28317168 0.56217521 0.28674626 0.56389374 0.28603441 0.56042176
		 0.28747249 0.5604614 0.28745613 0.56393337 0.28601801 0.55867207 0.28819728 0.56040597
		 0.28747904 0.56042176 0.28747249 0.55693752 0.28891575 0.55692172 0.28892231 0.54831529
		 0.29248714 0.55004787 0.2917695 0.54661101 0.29319304 0.54658949 0.29320198 0.55002642
		 0.29177842 0.55177987 0.29105207 0.55004787 0.2917695 0.55006367 0.29176295 0.55351371
		 0.29033387 0.55349791 0.29034042 0.53446901 0.29822245 0.53620344 0.29750407 0.53280038
		 0.29891366 0.53270119 0.29895476 0.53610426 0.29754514 0.52411354 0.30251187 0.52584547
		 0.30179447 0.51898974 0.30463421 0.52239424 0.30322403 0.52924997 0.30038428 0.54138601
		 0.29535735 0.53620344 0.29750407 0.53970569 0.29605335 0.54656792 0.29321092 0.54306567
		 0.29466164 0.50722498 0.30950737 0.50959045 0.30852753 0.50517899 0.31035477 0.50485718
		 0.3104881 0.50926864 0.30866084 0.50503308 0.31041524 0.50441146 0.3106727 0.5 0.3125
		 0.50565553 0.31015742 0.51006699 0.30833015 0.5314309 0.29948089 0.52512753 0.30209184
		 0.53452671 0.29819858 0.53772426 0.29687405 0.52832508 0.30076736 0.50690538 0.30963972
		 0.50690532 0.30963969 0.5 0.3125 0.50690532 0.30963969 0.51381069 0.30677941 0.53452665
		 0.29819855 0.52762139 0.30105883 0.53452671 0.29819858 0.54143202 0.29533827 0.53452671
		 0.29819852 0.54833734 0.29247802 0.54833734 0.292478 0.55524272 0.28961772 0.54833734
		 0.29247797 0.54143202 0.29533827 0.58976936 0.2753163 0.58286405 0.27817658 0.58976936
		 0.2753163 0.59667474 0.27245599 0.58976936 0.2753163 0.60358018 0.26959571 0.60358012
		 0.26959571 0.61048543 0.26673543 0.60358012 0.26959574 0.59667474 0.27245599 0.57595873
		 0.28103688 0.58286405 0.27817658 0.57595873 0.28103685 0.56905341 0.28389716 0.57595873
		 0.28103685 0.64482325 0.18383671 0.64553583 0.18211642 0.64695412 0.17869234 0.64411753
		 0.18554048 0.64269924 0.18896456 0.64052343 0.19421737 0.64124537 0.19247445 0.6412335
		 0.19250304 0.6398052 0.19595122 0.63981712 0.19592263 0.6362412 0.20455545 0.6369549
		 0.20283253 0.63551289 0.20631385 0.63553226 0.20626709 0.63697422 0.20278578 0.63479495
		 0.20804694 0.63551021 0.20632029 0.63551289 0.2063137 0.63407731 0.20977947 0.63407457
		 0.20978606 0.63050157 0.21841209 0.6312198 0.21667829 0.62979239 0.22012439 0.6297878
		 0.22013533 0.63121521 0.21668923 0.6319353 0.2149509 0.63121974 0.21667838 0.63122243
		 0.21667188 0.63265181 0.21322112 0.63264906 0.21322763 0.62476712 0.23225619 0.62548482
		 0.2305236 0.62407178 0.23393506 0.62403762 0.23401745 0.62545073 0.23060599 0.6204797
		 0.24260706 0.62119436 0.24088162 0.61835116 0.24774575 0.61977029 0.24431974 0.62261355
		 0.23745561 0.62762958 0.22534573 0.62548482 0.23052369 0.62693202 0.22702987 0.62977171
		 0.22017422 0.62832451 0.22366804 0.61346608 0.25953954 0.61444443 0.25717759 0.61263067
		 0.26155645 0.61248404 0.26191047 0.61429781 0.25753161 0.61255544 0.26173785 0.6122992
		 0.26235658 0.61048543 0.26673543 0.61281383 0.26111415;
	setAttr ".uvst[0].uvsp[2250:2499]" 0.6146276 0.2567353 0.62349105 0.23533699
		 0.62088007 0.24164066 0.62478679 0.23220888 0.62608761 0.22906847 0.62218088 0.23850024
		 0.61334568 0.25983012 0.61334574 0.25983012 0.61048543 0.26673543 0.61334574 0.25983012
		 0.61620599 0.2529248 0.62478691 0.23220879 0.62192655 0.23911411 0.62478685 0.23220873
		 0.62764716 0.22530343 0.62478685 0.23220879 0.63050735 0.21839817 0.63050735 0.21839818
		 0.63336766 0.21149287 0.63050735 0.21839821 0.62764704 0.22530353 0.64766914 0.17696604
		 0.64480889 0.18387139 0.6476692 0.17696601 0.65052938 0.17006069 0.64766914 0.17696607
		 0.65338981 0.16315539 0.65338969 0.16315538 0.65625 0.15625 0.65338969 0.16315538
		 0.65052938 0.17006075 0.64194846 0.19077684 0.64480877 0.18387148 0.64194846 0.19077685
		 0.63908815 0.1976822 0.64194846 0.19077684 0.62189835 0.07331802 0.62119436 0.071618393
		 0.61978132 0.068206936 0.62260747 0.075029902 0.62402058 0.078441367 0.62621206 0.083732001
		 0.62548482 0.081976414 0.62550193 0.082017615 0.62692946 0.085464016 0.62691236 0.085422814
		 0.63049108 0.094062567 0.62977523 0.092334323 0.63122243 0.095828131 0.63120186 0.095778443
		 0.62975466 0.092284635 0.63193035 0.097537056 0.6312198 0.095821708 0.63122255 0.095828287
		 0.63264722 0.09926784 0.63264447 0.099261254 0.63622767 0.10791181 0.63551021 0.10617971
		 0.63694304 0.10963896 0.63694292 0.10963859 0.63551009 0.10617935 0.63479632 0.10445607
		 0.63551021 0.10617962 0.63551289 0.10618613 0.63408196 0.10273154 0.63407922 0.10272503
		 0.64196235 0.12175634 0.64124537 0.12002556 0.64266366 0.12344964 0.64268738 0.12350686
		 0.64126909 0.12008279 0.64624363 0.13209249 0.64553583 0.13038358 0.64838427 0.13726032
		 0.64695656 0.13381363 0.64410818 0.1269369 0.63910168 0.11485023 0.64124537 0.12002547
		 0.63980329 0.11654414 0.63696235 0.10968556 0.63840437 0.11316688 0.65328914 0.14910173
		 0.65231079 0.14673996 0.65410477 0.15107098 0.65427101 0.15147237 0.65247703 0.14714135
		 0.65420049 0.15130205 0.65445602 0.15191898 0.65625 0.15625 0.65394193 0.15067786
		 0.65214795 0.14634684 0.64327085 0.12491576 0.6458751 0.13120288 0.64194846 0.12172315
		 0.64067793 0.11865583 0.64460456 0.12813555 0.65338969 0.14934464 0.65338969 0.14934462
		 0.65625 0.15625 0.65338969 0.14934464 0.65052938 0.14243926 0.64194864 0.12172327
		 0.64480889 0.12862861 0.64194858 0.1217233 0.63908827 0.11481792 0.64194858 0.12172323
		 0.63622802 0.10791247 0.63622797 0.1079125 0.63336766 0.10100713 0.63622797 0.10791245
		 0.63908827 0.11481781 0.6190663 0.066480584 0.62192655 0.073385909 0.61906624 0.066480599
		 0.61620605 0.059575241 0.6190663 0.066480547 0.61334568 0.052669886 0.61334574 0.05266989
		 0.61048543 0.04576458 0.61334574 0.052669875 0.61620599 0.059575185 0.62478679 0.080291122
		 0.62192655 0.073385805 0.62478679 0.080291115 0.62764704 0.08719644 0.62478685 0.08029113
		 0.50627738 0.0026001786 0.50690532 0.0028602891 0.5 1.4901161e-08 0.50565547 0.0023425773
		 0.50503302 0.0020847647 0.51006687 0.0041698506 0.51510072 0.0062549459 0.51323426
		 0.0054818355 0.52013958 0.0083421096 0.39579204 0.043164387 0.39641994 0.04290428
		 0.38951457 0.04576458 0.39517012 0.043421991 0.3945477 0.043679796 0.39958167 0.041594677
		 0.40461561 0.03950955 0.40274918 0.040282648 0.40965456 0.037422352 0.34633523 0.15000875
		 0.34661031 0.14934464 0.34375 0.15625 0.34607846 0.15062866 0.34582007 0.15125237
		 0.34789222 0.14624974 0.34996474 0.14124624 0.3491914 0.1431133 0.35205168 0.13620794
		 0.38695204 0.26054901 0.38665429 0.25983009 0.38951457 0.26673543 0.38720652 0.26116335
		 0.38746509 0.26178753 0.38541257 0.25683236 0.38335744 0.25187081 0.38413274 0.25374246
		 0.38127244 0.24683714 0.49385658 0.3099553 0.49309471 0.30963975 0.5 0.3125 0.49446192
		 0.31020606 0.49508417 0.31046379 0.49016616 0.30842668 0.48523185 0.30638283 0.4871127
		 0.30716193 0.48020741 0.30430165 0.60434192 0.26928017 0.60358012 0.26959571 0.61048543
		 0.26673543 0.60494733 0.26902938 0.60556954 0.26877168 0.6006515 0.27080876 0.59571713
		 0.27285263 0.59759808 0.27207357 0.5906927 0.27493382 0.65368748 0.16243649 0.65338969
		 0.16315538 0.65625 0.15625 0.65394199 0.16182214 0.65420055 0.16119796 0.65214801
		 0.16615316 0.6500929 0.17111474 0.65086806 0.16924307 0.64800775 0.17614841 0.61307067
		 0.052005764 0.61334574 0.052669886 0.61048543 0.04576458 0.61281383 0.051385865 0.61255544
		 0.050762136 0.6146276 0.055764716 0.61670011 0.060768172 0.61592674 0.058901135 0.61878705
		 0.065806441 0.52067047 0.0085619958 0.51898968 0.0078657884 0.51553702 0.0064356569
		 0.52239418 0.0092759775 0.52411348 0.0099881375 0.51898974 0.0078657996 0.52924991
		 0.012115708 0.53438628 0.014243259 0.5361042 0.014954848 0.53955686 0.016384982 0.54486215
		 0.018582536 0.54315829 0.017876787 0.54315835 0.017876798 0.54658949 0.019298013
		 0.54831529 0.020012874 0.54661101 0.019306941 0.5500263 0.020721603 0.55173731 0.021430312
		 0.55346894 0.022147562 0.55346882 0.022147551 0.56905431 0.028603261 0.56732702 0.027887786
		 0.57077974 0.02931794 0.57078552 0.029320344 0.57251841 0.030038105 0.5742324 0.030748082
		 0.57080472 0.029328316 0.56909114 0.028618515 0.57083738 0.02934185 0.56738472 0.027911697
		 0.59383708 0.03886861 0.59149575 0.037898786 0.59840107 0.040759079 0.59624004 0.03986394
		 0.59862268 0.040850859 0.60185379 0.042189226 0.59539694 0.039514706 0.59219038 0.038186513
		 0.59587169 0.039711386 0.58896637 0.036851086 0.60358006 0.04290428 0.60358006 0.042904284
		 0.61048543 0.04576458 0.60358012 0.04290428 0.60358012 0.042904284 0.59667468 0.04004398
		 0.58976936 0.03718368 0.5897693 0.037183672 0.58286393 0.034323379 0.5483374 0.020022003
		 0.55524266 0.022882286 0.54833734 0.020022012;
	setAttr ".uvst[0].uvsp[2500:2749]" 0.54143202 0.01716172 0.53452671 0.014301436
		 0.53452671 0.014301442 0.53452659 0.014301428 0.53452659 0.01430142 0.52762127 0.01144114
		 0.53452659 0.014301414 0.4101851 0.037202574 0.40850431 0.037898783 0.40505159 0.03932894
		 0.41190881 0.036488593 0.41362813 0.035776433 0.40850425 0.037898794 0.41876459 0.033648841
		 0.42390099 0.031521261 0.42561895 0.030809676 0.42907166 0.029379521 0.43437684 0.027182039
		 0.43267298 0.027887784 0.43267292 0.027887797 0.43610415 0.02646656 0.43783 0.025751702
		 0.43612558 0.026457654 0.43954107 0.025042947 0.44125208 0.024334213 0.44298366 0.023616962
		 0.44298369 0.023616947 0.45856902 0.017161308 0.45684168 0.017876785 0.46029431 0.016446656
		 0.46030021 0.016444227 0.462033 0.015726471 0.46374696 0.015016513 0.46031952 0.016436232
		 0.45860595 0.017146014 0.46035224 0.016422674 0.45689961 0.017852802 0.48335165 0.0068959687
		 0.48101035 0.0078657866 0.48791564 0.0050055142 0.48575461 0.0059006382 0.4881373
		 0.0049137231 0.49136829 0.0035753716 0.48491156 0.0062498599 0.48170513 0.0075780409
		 0.48538637 0.0060531795 0.47848105 0.0089134527 0.49309465 0.0028602867 0.49309468
		 0.0028602874 0.5 1.4901161e-08 0.49309468 0.0028602844 0.49309471 0.0028602814 0.48618942
		 0.0057205502 0.47928414 0.0085808188 0.47928414 0.0085808123 0.47237885 0.011441085
		 0.43785203 0.025742568 0.44475734 0.022882285 0.43785194 0.025742583 0.43094665 0.028602859
		 0.4240413 0.031463142 0.42404127 0.031463154 0.42404133 0.031463139 0.42404133 0.031463124
		 0.41713598 0.034323417 0.42404139 0.03146312 0.35232282 0.13555329 0.35161579 0.13726029
		 0.35018563 0.14071298 0.35303491 0.13383424 0.35374433 0.13212158 0.35161579 0.13726032
		 0.35587817 0.12697005 0.35801199 0.12181858 0.35871542 0.12012041 0.36014557 0.11666772
		 0.36233774 0.11137521 0.3616268 0.1130916 0.3616268 0.11309163 0.36305243 0.10964984
		 0.36376619 0.10792659 0.36305696 0.10963896 0.36447984 0.10620369 0.36519346 0.10448084
		 0.36590907 0.10275318 0.3659091 0.10275315 0.37235248 0.087197527 0.37163776 0.088922925
		 0.37306792 0.085470282 0.37306982 0.085465744 0.37378806 0.08373189 0.37449807 0.082017615
		 0.37308168 0.085437089 0.37237534 0.087142341 0.37310356 0.085384332 0.37167341 0.088836968
		 0.38261896 0.062412102 0.38164881 0.064754233 0.38450909 0.057848923 0.38360584 0.060029566
		 0.38458559 0.057664204 0.38593924 0.054396257 0.38324201 0.060907871 0.38190466 0.064136483
		 0.38341764 0.060483828 0.38055736 0.067389138 0.38665432 0.05266989 0.38665429 0.05266989
		 0.38951457 0.04576458 0.38665432 0.052669886 0.38665432 0.052669886 0.38379404 0.0595752
		 0.3809337 0.06648051 0.38093376 0.066480517 0.37807345 0.073385827 0.36377198 0.1079126
		 0.36663228 0.10100727 0.363772 0.10791263 0.36091173 0.11481793 0.35805142 0.12172327
		 0.35805142 0.1217233 0.35805142 0.12172323 0.35805142 0.1217232 0.35519114 0.12862855
		 0.35805142 0.12172319 0.3809348 0.24602199 0.38164878 0.24774574 0.38307893 0.25119841
		 0.38022116 0.24429913 0.37950823 0.24257796 0.38164878 0.24774575 0.37737274 0.23742242
		 0.37523726 0.23226687 0.37453365 0.23056829 0.3731035 0.22711562 0.37092295 0.22185135
		 0.37163776 0.22357705 0.37163776 0.22357705 0.37020749 0.22012407 0.36949226 0.21839729
		 0.37020761 0.22012439 0.36877465 0.21666485 0.36805704 0.21493241 0.36733922 0.21319944
		 0.36733922 0.21319944 0.36091131 0.19768098 0.3616268 0.19940838 0.36019665 0.19595569
		 0.36019409 0.19594947 0.35947666 0.19421746 0.3587665 0.19250304 0.36017704 0.19590831
		 0.36087742 0.19759916 0.36014551 0.19583219 0.36157563 0.19928485 0.35064355 0.17289251
		 0.35161579 0.17523968 0.34875548 0.16833434 0.34966773 0.17053673 0.34869421 0.16818637
		 0.34732533 0.16488168 0.35005137 0.17146294 0.35140511 0.17473118 0.34990641 0.17111295
		 0.35276669 0.1780183 0.34661028 0.16315532 0.34661028 0.16315535 0.34375 0.15625
		 0.34661028 0.16315533 0.34661028 0.16315535 0.34947056 0.17006068 0.35233086 0.17696604
		 0.35233086 0.17696604 0.35519114 0.18387139 0.36949256 0.21839805 0.36663228 0.21149272
		 0.36949256 0.21839806 0.37235284 0.22530338 0.37521312 0.23220871 0.37521315 0.23220873
		 0.37521315 0.23220871 0.37521312 0.23220871 0.37807342 0.23911405 0.37521315 0.23220871
		 0.47928014 0.30391759 0.48101035 0.30463424 0.48446298 0.30606437 0.47755075 0.30320123
		 0.47582135 0.3024849 0.48101032 0.30463421 0.47066474 0.30034894 0.4655081 0.29821298
		 0.46380496 0.29750749 0.46035233 0.29607737 0.45511159 0.2939066 0.45684168 0.29462323
		 0.45684165 0.2946232 0.45338151 0.29318997 0.45165136 0.29247332 0.45338899 0.29319304
		 0.44991308 0.29175332 0.44817472 0.2910333 0.44643635 0.29031324 0.44643641 0.29031324
		 0.43094301 0.28389561 0.43267298 0.28461224 0.42922026 0.28318208 0.42921227 0.28317875
		 0.42748159 0.28246188 0.4257676 0.28175193 0.42916274 0.28315824 0.43084398 0.28385463
		 0.42907166 0.28312051 0.43252438 0.28455067 0.40615416 0.27362773 0.40850431 0.27460122
		 0.40159893 0.27174091 0.40380824 0.27265602 0.40146098 0.27168375 0.39814627 0.27031076
		 0.4047671 0.27305323 0.40807158 0.27442199 0.40447551 0.27293244 0.41138089 0.27579275
		 0.39641997 0.26959574 0.39641994 0.26959574 0.38951457 0.26673543 0.39641994 0.26959574
		 0.39641994 0.26959574 0.40332532 0.27245605 0.4102307 0.27531636 0.4102307 0.27531636
		 0.41713607 0.27817667 0.45166272 0.29247802 0.44475734 0.28961772 0.45166263 0.292478
		 0.45856804 0.29533833 0.46547338 0.29819861 0.46547329 0.29819858 0.46547344 0.29819864
		 0.46547356 0.2981987 0.47237885 0.30105895 0.46547353 0.2981987 0.58976555 0.27531788
		 0.59149575 0.27460122 0.59494841 0.27317107 0.58803606 0.27603424 0.58630669 0.27675059
		 0.59149569 0.27460122 0.58115005 0.27888656;
	setAttr ".uvst[0].uvsp[2750:2999]" 0.57599348 0.28102255 0.57429028 0.28172803
		 0.57083762 0.28315812 0.565597 0.28532887 0.56732708 0.28461224 0.56732708 0.28461224
		 0.56386691 0.28604549 0.56213683 0.28676212 0.56387442 0.28604239 0.56039846 0.28748217
		 0.55866003 0.2882022 0.55692172 0.28892231 0.55692172 0.28892231 0.54142833 0.29533979
		 0.54315835 0.2946232 0.53970569 0.29605335 0.53969765 0.29605669 0.53796691 0.29677358
		 0.53625304 0.2974835 0.53964806 0.29607722 0.54132915 0.29538092 0.53955686 0.29611498
		 0.54300952 0.29468486 0.51663947 0.30560771 0.51898968 0.30463421 0.51208436 0.30749452
		 0.51429355 0.30657941 0.51194632 0.30755168 0.50863165 0.30892467 0.51525235 0.30618227
		 0.51855683 0.30481353 0.51496077 0.30630308 0.52186608 0.30344278 0.50690526 0.30963969
		 0.50690532 0.30963972 0.5 0.3125 0.50690532 0.30963969 0.50690532 0.30963969 0.51381063
		 0.30677938 0.52071607 0.30391911 0.52071607 0.30391914 0.52762139 0.30105883 0.56214803
		 0.28675744 0.55524272 0.28961772 0.56214809 0.28675744 0.56905341 0.28389716 0.57595867
		 0.28103688 0.57595873 0.28103685 0.57595873 0.28103685 0.57595867 0.28103685 0.58286405
		 0.27817658 0.57595873 0.28103685 0.64767027 0.17696351 0.64838421 0.17523971 0.64981437
		 0.17178702 0.64695662 0.17868637 0.64624363 0.18040748 0.64838427 0.17523968 0.64410818
		 0.18556312 0.64197272 0.1907187 0.64126909 0.19241726 0.63983893 0.19586995 0.63765842
		 0.20113412 0.6383732 0.19940841 0.6383732 0.19940838 0.63694292 0.20286141 0.63622767
		 0.2045882 0.63694304 0.20286104 0.63551009 0.20632064 0.63479245 0.20805311 0.63407457
		 0.20978609 0.63407457 0.20978612 0.62764668 0.2253045 0.62836218 0.22357708 0.62693208
		 0.22702974 0.62692952 0.227036 0.62621206 0.22876804 0.62550193 0.2304824 0.62691242
		 0.22707719 0.62761277 0.22538638 0.62688088 0.22715336 0.62831098 0.22370073 0.61737901
		 0.25009295 0.61835122 0.24774577 0.61549091 0.25465107 0.61640322 0.25244874 0.6154297
		 0.25479913 0.61406082 0.25810373 0.61678684 0.25152254 0.61814058 0.24825437 0.61664188
		 0.25187254 0.61950213 0.24496725 0.61334574 0.25983015 0.61334574 0.25983012 0.61048543
		 0.26673543 0.61334568 0.25983015 0.61334568 0.25983012 0.61620593 0.25292483 0.6190663
		 0.24601954 0.61906624 0.2460195 0.62192655 0.2391142 0.63622797 0.20458741 0.63336766
		 0.21149275 0.63622797 0.20458737 0.63908827 0.19768208 0.64194858 0.19077675 0.64194858
		 0.19077671 0.64194858 0.19077677 0.64194864 0.19077681 0.64480889 0.18387145 0.64194858
		 0.19077682 0.61905825 0.06646122 0.61835122 0.064754233 0.61692107 0.061301596 0.61977029
		 0.068180263 0.6204797 0.06989295 0.61835116 0.064754263 0.62261349 0.075044394 0.62474722
		 0.080195814 0.62545073 0.081893995 0.62688082 0.085346639 0.62907314 0.090639308
		 0.62836218 0.088922918 0.62836224 0.088922948 0.62978786 0.092364669 0.63050163 0.094087914
		 0.62979239 0.092375621 0.63121527 0.095810764 0.63192886 0.097533561 0.63264447 0.099261224
		 0.63264441 0.099261194 0.63908792 0.11481702 0.6383732 0.1130916 0.63980335 0.1165443
		 0.63980526 0.11654878 0.64052343 0.11828263 0.6412335 0.11999696 0.63981712 0.11657737
		 0.63911074 0.11487208 0.63983893 0.11663009 0.63840878 0.11317739 0.6493544 0.13960242
		 0.64838421 0.13726029 0.65124452 0.14416566 0.65034127 0.14198495 0.65132099 0.14435031
		 0.65267462 0.14761832 0.64997745 0.14110661 0.64864004 0.13787793 0.65015304 0.1415306
		 0.64729273 0.13462523 0.65338969 0.14934461 0.65338969 0.14934462 0.65625 0.15625
		 0.65338969 0.14934462 0.65338969 0.14934462 0.65052938 0.14243925 0.64766908 0.13553388
		 0.64766908 0.13553387 0.64480877 0.12862852 0.63050747 0.094101913 0.63336766 0.10100725
		 0.63050747 0.094101951 0.62764716 0.087196574 0.62478679 0.080291227 0.62478685 0.080291279
		 0.62478685 0.080291212 0.62478685 0.080291182 0.62192655 0.07338585 0.62478685 0.080291152
		 0.50247264 0.0010241979 0.5 1.4901161e-08 0.50172633 0.00071508624 0.50485718 0.0020119129
		 0.50722486 0.002992668 0.50517899 0.0021452289 0.50926858 0.0038391864 0.51133049
		 0.0046932492 0.51496065 0.0061969068 0.51663947 0.0068923053 0.51208436 0.0050055142
		 0.52119887 0.0087808762 0.52575988 0.01067014 0.52215761 0.009178007 0.51855665 0.0076864474
		 0.52186596 0.009057181 0.54403055 0.018238068 0.55006349 0.020737005 0.54833722 0.020021934
		 0.53772414 0.015625853 0.53143072 0.013019042 0.52832496 0.011732578 0.52520305 0.010439467
		 0.39198726 0.044740371 0.38951457 0.04576458 0.39124089 0.045049511 0.39437181 0.043752648
		 0.3967396 0.042771891 0.39469355 0.043619365 0.39878336 0.041925337 0.40084532 0.041071232
		 0.40447551 0.039567575 0.40615416 0.038872264 0.40159893 0.040759079 0.41071361 0.036983661
		 0.41527477 0.035094369 0.41167247 0.036586493 0.40807152 0.038078044 0.41138089 0.036707278
		 0.43354571 0.027526325 0.43957859 0.02502742 0.4378522 0.025742494 0.42723912 0.030138571
		 0.42094561 0.032745406 0.41783991 0.034031857 0.41471812 0.035324953 0.34476295 0.15380457
		 0.34375 0.15625 0.34446508 0.15452367 0.3457486 0.15142497 0.34673068 0.14905405
		 0.34589523 0.15107098 0.34756237 0.14704609 0.34840026 0.14502318 0.34990647 0.14138696
		 0.35064358 0.13960747 0.34875548 0.14416566 0.35252807 0.13505788 0.35441604 0.13049991
		 0.35291171 0.13413166 0.35140517 0.13776876 0.35276675 0.13448161 0.36195228 0.11230592
		 0.36448717 0.10618611 0.36377209 0.10791244 0.35935232 0.11858271 0.35675579 0.1248513
		 0.3554455 0.12801455 0.35413113 0.13118775 0.38851842 0.26433054 0.38951457 0.26673543
		 0.38879949 0.26500911 0.38753563 0.26195782 0.38655367 0.2595872 0.38736933 0.26155645
		 0.38574168 0.25762683 0.38492602 0.25565767 0.38341767 0.25201613 0.38261893 0.25008786
		 0.38450909 0.25465107;
	setAttr ".uvst[0].uvsp[3000:3249]" 0.38074553 0.24556506 0.3788659 0.24102718
		 0.3803817 0.24468674 0.38190466 0.24836345 0.38055736 0.24511078 0.37134781 0.222877
		 0.36877751 0.21667172 0.36949256 0.21839806 0.37394261 0.22914144 0.37653559 0.23540135
		 0.37786925 0.23862115 0.37920466 0.24184506 0.49762696 0.31151703 0.5 0.3125 0.49827367
		 0.31178492 0.49525887 0.31053615 0.49289083 0.30955529 0.49482101 0.31035477 0.4909631
		 0.3087568 0.48903349 0.3079575 0.48538637 0.30644685 0.48335168 0.30560407 0.48791564
		 0.30749452 0.47884935 0.30373913 0.47432774 0.3018662 0.47800624 0.30338991 0.48170501
		 0.30492201 0.47848105 0.30358657 0.45619294 0.29435456 0.4499366 0.29176307 0.45166293
		 0.29247814 0.462457 0.29694921 0.46872032 0.29954353 0.47197187 0.30089039 0.47522387
		 0.30223739 0.60811239 0.26771837 0.61048543 0.26673543 0.60875911 0.26745051 0.60574424
		 0.26869929 0.60337621 0.26968014 0.60530645 0.26888067 0.60144842 0.27047867 0.59951878
		 0.27127793 0.59587169 0.27278861 0.59383708 0.27363139 0.59840107 0.27174091 0.58933473
		 0.27549633 0.58481306 0.27736926 0.58849156 0.27584559 0.59219033 0.27431348 0.58896637
		 0.27564889 0.56667817 0.28488097 0.56042171 0.28747249 0.56214809 0.28675744 0.57294226
		 0.28228635 0.57920557 0.27969199 0.58245707 0.27834511 0.58570921 0.27699807 0.65525383
		 0.1586549 0.65625 0.15625 0.65553492 0.15797633 0.65427101 0.16102764 0.65328908
		 0.16339828 0.65410477 0.16142902 0.65247703 0.16535865 0.6516614 0.16732784 0.65015304
		 0.1709694 0.64935434 0.17289756 0.65124452 0.16833434 0.64748096 0.17742041 0.64560133
		 0.18195836 0.64711714 0.17829877 0.6486401 0.17462206 0.64729273 0.17787477 0.6380831
		 0.20010865 0.63551283 0.20631392 0.63622785 0.20458758 0.64067793 0.19384417 0.64327091
		 0.18758424 0.64460456 0.18436444 0.64594001 0.18114053 0.61149836 0.048209991 0.61048543
		 0.04576458 0.61120051 0.047490917 0.61248398 0.050589561 0.61346614 0.052960493 0.61263067
		 0.050943583 0.61429775 0.054968413 0.61513573 0.056991253 0.61664188 0.060627468
		 0.61737907 0.062407058 0.61549091 0.057848923 0.61926347 0.066956587 0.62115139 0.071514502
		 0.61964715 0.067882769 0.61814058 0.064245664 0.61950213 0.067532778 0.6286875 0.089708291
		 0.63122237 0.095828071 0.63050735 0.094101727 0.62608767 0.083431542 0.62349117 0.077163003
		 0.62218094 0.073999777 0.62086654 0.070826605 0.53092527 0.012809658 0.52589506 0.010726085
		 0.52934772 0.012156228 0.53270119 0.013545273 0.53446913 0.014277541 0.53280038 0.01358637
		 0.53610426 0.014954861 0.53778756 0.01565212 0.53955686 0.016384993 0.54142827 0.017160201
		 0.53970569 0.016446656 0.54315031 0.017873459 0.54487234 0.018586731 0.54310071 0.017852904
		 0.54132915 0.017119098 0.54300952 0.017815124 0.55174339 0.02143283 0.55351615 0.022167148
		 0.5465678 0.019289009 0.54138595 0.017142627 0.54306555 0.017838335 0.54474854 0.018535478
		 0.55519533 0.022862658 0.55351639 0.022167226 0.55696905 0.023597369 0.55693746 0.023584273
		 0.55867201 0.024302743 0.56042176 0.025027512 0.5569216 0.023577716 0.55519277 0.022861604
		 0.5569216 0.023577705 0.56559694 0.027171165 0.56732708 0.027887797 0.56386685 0.026454519
		 0.56213677 0.025737893 0.56039834 0.025017824 0.55865997 0.024297765 0.5569216 0.023577694
		 0.55524093 0.022881562 0.55696881 0.023597291 0.55351353 0.022166062 0.55177969 0.02144789
		 0.55349779 0.022159528 0.55522132 0.022873435 0.57946908 0.032917231 0.58113772 0.03360837
		 0.58459038 0.035038508 0.58117616 0.033624321 0.58287627 0.034328509 0.58804303 0.036468655
		 0.57774276 0.032202147 0.57260346 0.030073378 0.57429004 0.030771993 0.58976555 0.037182122
		 0.59494841 0.03932894 0.5845834 0.035035614 0.57940125 0.03288912 0.57769728 0.032183297
		 0.5759933 0.031477477 0.57083738 0.029341839 0.5587405 0.024331152 0.56042147 0.025027433
		 0.5604611 0.025043834 0.56217498 0.025753727 0.56393307 0.026481967 0.5656988 0.027213356
		 0.60811245 0.044781633 0.60875911 0.045049511 0.60747063 0.044515789 0.60683066 0.044250727
		 0.60317481 0.042736419 0.59951872 0.041222069 0.59759808 0.040426455 0.60434186 0.043219846
		 0.59804195 0.040610328 0.59175831 0.038007569 0.59374619 0.038830951 0.59571707 0.039647359
		 0.59069264 0.037566163 0.56667799 0.027618967 0.56214786 0.025742505 0.57121575 0.029498549
		 0.57575285 0.031377897 0.58073068 0.033439767 0.5857091 0.035501909 0.60358006 0.042904273
		 0.61048543 0.04576458 0.60358012 0.042904295 0.59667468 0.040043987 0.5897693 0.037183706
		 0.58976936 0.037183724 0.58976936 0.037183687 0.58976924 0.037183668 0.58286393 0.034323387
		 0.56214809 0.025742576 0.56214809 0.025742583 0.56214797 0.025742561 0.56214803 0.025742548
		 0.56905329 0.028602827 0.57595867 0.031463109 0.57595861 0.031463094 0.56214786 0.025742494
		 0.55524254 0.022882219 0.56905317 0.028602792 0.57595855 0.03146309 0.57595855 0.031463083
		 0.57595855 0.031463075 0.50690526 0.0028602988 0.50690532 0.0028603002 0.5 1.4901161e-08
		 0.50690532 0.0028602972 0.50690532 0.0028602947 0.51381063 0.0057205767 0.52071589
		 0.0085808579 0.52071595 0.0085808532 0.50690532 0.0028602916 0.51381063 0.005720566
		 0.52071595 0.0085808421 0.52071595 0.0085808458 0.52071595 0.0085808486 0.52762127
		 0.011441128 0.54833728 0.020021945 0.54833722 0.020021955 0.54833722 0.020021966
		 0.5414319 0.017161686 0.53452665 0.014301407 0.42043984 0.03295492 0.41540956 0.035038508
		 0.41886228 0.03360837 0.42221579 0.032219302 0.42398369 0.031487033 0.42231494 0.032178223
		 0.42561889 0.030809689 0.4273023 0.030112406 0.42907161 0.029379532 0.43094298 0.02860437
		 0.42922026 0.02931794 0.43266499 0.027891088 0.43438703 0.027177798 0.43261543 0.02791162
		 0.43084389 0.028645426 0.43252432 0.027949378 0.44125843 0.024331598 0.44303125 0.023597278
		 0.43608275 0.026475441 0.43090084 0.028621843;
	setAttr ".uvst[0].uvsp[3250:3499]" 0.43258044 0.027926141 0.43426347 0.027228998
		 0.44470993 0.022901915 0.44303095 0.023597369 0.44648361 0.022167226 0.44645208 0.0221803
		 0.44818658 0.02146183 0.44993627 0.020737084 0.44643629 0.022186831 0.44470745 0.022902921
		 0.44643632 0.022186819 0.45511162 0.01859341 0.45684165 0.017876798 0.45338154 0.019310027
		 0.45165151 0.020026632 0.44991308 0.0207467 0.44817469 0.02146676 0.44643635 0.022186805
		 0.444756 0.022882862 0.44648391 0.022167135 0.44302857 0.023598392 0.4412947 0.024316587
		 0.44301271 0.023604948 0.44473633 0.022891043 0.4689838 0.012847343 0.47065228 0.012156228
		 0.474105 0.010726085 0.47069091 0.012140253 0.47239101 0.011436067 0.47755766 0.0092959423
		 0.46725756 0.013562402 0.46211824 0.015691143 0.4638049 0.014992531 0.47928017 0.0085824486
		 0.48446298 0.0064356569 0.47409809 0.010728933 0.46891609 0.012875401 0.46721208
		 0.013581224 0.4655081 0.014287044 0.4603523 0.016422661 0.44825566 0.021433273 0.44993657
		 0.020736994 0.44997609 0.020720616 0.45168981 0.020010745 0.453448 0.019282505 0.45521373
		 0.018551117 0.49762699 0.00098296686 0.49827367 0.00071508624 0.49698517 0.0012488021
		 0.49634522 0.0015138664 0.4926894 0.0030281665 0.48903349 0.0045425054 0.4871127
		 0.0053381086 0.49385655 0.0025447267 0.48755664 0.0051542292 0.48127306 0.0077569662
		 0.48326087 0.0069335941 0.48523185 0.0061171963 0.48020738 0.0081983814 0.456193
		 0.018145485 0.4516629 0.020021923 0.46073067 0.016265921 0.46526775 0.014386594 0.47024548
		 0.012324741 0.47522387 0.010262619 0.49309471 0.0028602753 0.49309465 0.0028603002
		 0.48618937 0.0057205623 0.47928399 0.0085808495 0.47928399 0.0085808709 0.47928408
		 0.0085808281 0.47928414 0.0085808048 0.47237879 0.011441098 0.45166269 0.020021997
		 0.45166263 0.020022012 0.45166266 0.020021982 0.45166272 0.020021969 0.45856807 0.017161679
		 0.4654735 0.014301389 0.4654735 0.01430137 0.45166296 0.020021912 0.44475758 0.022882208
		 0.45856822 0.017161638 0.46547347 0.014301366 0.4654735 0.014301359 0.46547347 0.014301351
		 0.39641991 0.042904295 0.39641991 0.042904295 0.38951457 0.04576458 0.39641991 0.042904291
		 0.39641991 0.042904288 0.40332526 0.040043999 0.41023061 0.037183706 0.41023064 0.037183702
		 0.39641994 0.042904284 0.40332532 0.040043987 0.41023073 0.037183695 0.4102307 0.037183695
		 0.4102307 0.037183702 0.41713601 0.034323409 0.43785217 0.025742505 0.43785214 0.025742516
		 0.43785214 0.025742527 0.43094677 0.02860282 0.42404142 0.031463116 0.35657015 0.12529944
		 0.35447609 0.13035497 0.35590619 0.12690231 0.35730225 0.12353195 0.35803181 0.12177067
		 0.35733634 0.12344964 0.35871536 0.12012043 0.3594099 0.11844364 0.36014551 0.11666774
		 0.36091128 0.11481899 0.36019665 0.1165443 0.36162421 0.1130978 0.36233714 0.11137661
		 0.36160716 0.11313895 0.36087739 0.11490077 0.36157566 0.11321504 0.36517781 0.10451874
		 0.36591727 0.10273343 0.36303642 0.10968846 0.36089426 0.11486003 0.36158922 0.11318232
		 0.36228365 0.11150572 0.36662415 0.10102688 0.36591721 0.10273363 0.36734736 0.099280953
		 0.36734194 0.099294014 0.36805958 0.097561464 0.36877751 0.095828287 0.36733922 0.099300526
		 0.36662337 0.10102866 0.36733922 0.099300504 0.37092301 0.090648621 0.37163776 0.088922948
		 0.37020749 0.092375904 0.36949202 0.094103187 0.36877465 0.09583509 0.36805704 0.097567521
		 0.36733922 0.099300474 0.36663184 0.10100827 0.36734742 0.099280767 0.36591649 0.10273536
		 0.36519998 0.10446516 0.36591375 0.10274193 0.36662802 0.10101755 0.37667888 0.07675273
		 0.37735838 0.075112276 0.37878847 0.071659602 0.3773821 0.075055003 0.37808782 0.073351249
		 0.38021863 0.068206936 0.37596384 0.078479022 0.37383804 0.083611146 0.37453371 0.081931658
		 0.38093475 0.066477999 0.38307893 0.061301596 0.378791 0.071653515 0.37664723 0.076829031
		 0.37594259 0.0785302 0.37523726 0.080233082 0.37310356 0.085384302 0.36808643 0.097496599
		 0.36877757 0.095828101 0.36879694 0.095781341 0.36950591 0.094069779 0.37023896 0.09230008
		 0.37097338 0.090527028 0.38851842 0.048169453 0.38879949 0.047490917 0.38825068 0.048815835
		 0.3879866 0.049453385 0.38645673 0.053146806 0.38492599 0.05684229 0.38413271 0.058757491
		 0.38695204 0.051950984 0.38434625 0.058241956 0.38174304 0.064526632 0.38255233 0.062572926
		 0.38335747 0.060629144 0.38127244 0.065662801 0.37134787 0.089622825 0.36949265 0.094101764
		 0.37322763 0.085084744 0.37510568 0.080550753 0.37715423 0.075605094 0.37920469 0.070654862
		 0.38665432 0.052669913 0.38665429 0.05266992 0.38379401 0.059575237 0.38093373 0.066480577
		 0.3809337 0.066480599 0.38093376 0.066480547 0.3809337 0.066480517 0.37807345 0.07338585
		 0.36949259 0.094101913 0.36949256 0.094101951 0.36949259 0.094101891 0.36949262 0.094101861
		 0.3723529 0.087196521 0.37521318 0.080291182 0.37521321 0.080291167 0.36949262 0.094101787
		 0.36663234 0.1010071 0.3723529 0.087196469 0.37521321 0.080291152 0.37521321 0.080291152
		 0.37521318 0.080291145 0.34661031 0.14934465 0.34661028 0.14934465 0.34375 0.15625
		 0.34661031 0.14934464 0.34661031 0.14934462 0.34947059 0.14243928 0.35233086 0.1355339
		 0.35233086 0.1355339 0.34661031 0.14934462 0.34947059 0.14243926 0.35233086 0.1355339
		 0.35233086 0.13553388 0.35233086 0.13553388 0.35519117 0.12862852 0.36377206 0.10791244
		 0.36377203 0.10791247 0.36377203 0.10791248 0.36091173 0.11481783 0.35805142 0.12172318
		 0.37667888 0.23574731 0.37878847 0.24084041 0.37735838 0.23738775 0.37595195 0.23399232
		 0.37522686 0.23224179 0.37592822 0.23393506 0.37453365 0.23056829 0.37383795 0.22888879
		 0.3731035 0.22711562 0.37235251 0.2253025 0.37306792 0.22702974 0.37163967 0.22358158
		 0.37092689 0.22186074 0.37165153 0.22361021 0.37237531 0.22535759 0.37167338 0.22366294
		 0.36808637 0.21500319 0.36734736 0.21321905 0.37022704 0.22017118 0.37236625 0.22533581;
	setAttr ".uvst[0].uvsp[3500:3749]" 0.37166902 0.22365248 0.37097332 0.22197285
		 0.36662418 0.21147311 0.36734736 0.21321905 0.36591721 0.20976639 0.36591178 0.2097533
		 0.36519486 0.20802251 0.36448705 0.2063137 0.36590907 0.20974676 0.3666234 0.21147127
		 0.36590907 0.20974675 0.36233777 0.20112479 0.3616268 0.19940838 0.36305243 0.20285013
		 0.36376709 0.20457552 0.36447984 0.20629625 0.36519346 0.20801909 0.36590907 0.20974675
		 0.36663181 0.21149154 0.36591721 0.20976639 0.36734658 0.21321717 0.36806092 0.21494167
		 0.36734384 0.21321061 0.36662793 0.21148233 0.35657012 0.18720053 0.35590619 0.18559769
		 0.35447609 0.18214503 0.35587215 0.18551536 0.35516307 0.18380348 0.35304594 0.17869234
		 0.3572852 0.18892685 0.3594099 0.19405629 0.35871536 0.1923795 0.35232282 0.17694667
		 0.35018563 0.17178702 0.35446507 0.1821184 0.35660726 0.18729018 0.3573083 0.18898256
		 0.35801193 0.19068132 0.36014551 0.19583219 0.36517772 0.20798109 0.36448705 0.20631371
		 0.36446649 0.20626405 0.36375573 0.20454812 0.36301929 0.20277022 0.36228362 0.20099415
		 0.34476298 0.15869544 0.34446508 0.15797633 0.34503353 0.15934867 0.3452979 0.15998696
		 0.3468473 0.16372754 0.34840026 0.16747673 0.34919137 0.16938663 0.34633517 0.16249122
		 0.3489387 0.16877666 0.35154185 0.17506124 0.35075247 0.17315553 0.34996474 0.17125368
		 0.35205165 0.17629197 0.36195216 0.2001939 0.363772 0.20458737 0.36006731 0.19564347
		 0.35818657 0.19110289 0.35616052 0.18621166 0.35413107 0.18131214 0.34661028 0.16315533
		 0.34661028 0.16315535 0.34947056 0.17006069 0.35233083 0.17696604 0.35233086 0.17696601
		 0.35233086 0.17696604 0.35233083 0.17696604 0.35519114 0.18387139 0.36377198 0.20458739
		 0.363772 0.20458737 0.36377198 0.20458737 0.363772 0.20458741 0.3609117 0.19768205
		 0.35805139 0.19077671 0.35805142 0.19077672 0.363772 0.20458739 0.36663228 0.21149272
		 0.36091173 0.19768205 0.35805148 0.19077672 0.35805145 0.19077672 0.35805148 0.19077672
		 0.38665429 0.25983006 0.38665429 0.25983009 0.38951457 0.26673543 0.38665429 0.25983012
		 0.38665429 0.25983006 0.38379401 0.25292474 0.38093373 0.24601939 0.3809337 0.24601939
		 0.38665432 0.25983012 0.38379401 0.25292474 0.38093367 0.24601939 0.3809337 0.24601942
		 0.38093373 0.24601941 0.37807342 0.23911405 0.36949253 0.21839802 0.36949259 0.21839805
		 0.36949256 0.21839806 0.37235284 0.22530338 0.37521309 0.23220868 0.46898383 0.29965267
		 0.474105 0.30177391 0.47065228 0.30034381 0.46723825 0.29892963 0.46549842 0.29820898
		 0.46719962 0.29891366 0.4638049 0.29750746 0.46211836 0.29680887 0.4603523 0.29607737
		 0.45856902 0.29533872 0.46029431 0.29605335 0.45684755 0.29462567 0.45512617 0.29391265
		 0.45686692 0.29463369 0.45860595 0.29535401 0.45689967 0.29464722 0.44825563 0.29106677
		 0.44648394 0.29033291 0.4534288 0.29320955 0.45859435 0.29534921 0.45690072 0.29464766
		 0.4552137 0.29394889 0.44471002 0.28959811 0.44648361 0.29033279 0.44303095 0.28890264
		 0.44299942 0.28888959 0.44126728 0.28817213 0.43957829 0.28747249 0.44298366 0.28888309
		 0.44470754 0.28959712 0.44298369 0.28888309 0.43437681 0.28531799 0.43267292 0.28461224
		 0.43610415 0.28603348 0.43783155 0.28674901 0.43954104 0.28745711 0.44125208 0.28816584
		 0.44298372 0.28888309 0.44475597 0.28961718 0.44303125 0.28890276 0.44648123 0.29033181
		 0.44820103 0.29104418 0.44646537 0.29032525 0.44473624 0.28960901 0.42043984 0.2795451
		 0.41886228 0.27889162 0.41540956 0.27746153 0.4187631 0.27885056 0.41703814 0.27813607
		 0.41195691 0.27603137 0.42216623 0.28026021 0.42730233 0.28238764 0.42561901 0.28169036
		 0.4101851 0.27529743 0.40505159 0.27317107 0.41536152 0.27744156 0.42053795 0.27958575
		 0.42221731 0.28028136 0.42390105 0.28097874 0.42907172 0.28312051 0.44125843 0.28816843
		 0.43957859 0.28747261 0.43953544 0.28745472 0.4378089 0.28673962 0.43603313 0.28600407
		 0.43426353 0.28527102 0.39198726 0.26775965 0.39124089 0.26745051 0.39264548 0.26803231
		 0.39328551 0.26829737 0.39705703 0.26985961 0.40084532 0.27142876 0.40274918 0.27221739
		 0.39579201 0.26933566 0.40207547 0.27193835 0.40835899 0.27454105 0.40648705 0.27376568
		 0.40461561 0.27299049 0.40965456 0.2750777 0.43354565 0.28497371 0.43785226 0.28675759
		 0.42896551 0.28307652 0.42440134 0.28118598 0.41956627 0.27918324 0.41471815 0.27717507
		 0.39641988 0.26959571 0.39641991 0.26959571 0.40332526 0.27245602 0.41023064 0.27531633
		 0.41023058 0.2753163 0.41023064 0.27531633 0.4102307 0.27531633 0.41713601 0.27817661
		 0.437852 0.28675747 0.43785194 0.28675744 0.43785203 0.28675747 0.43785208 0.2867575
		 0.43094674 0.28389722 0.42404139 0.28103691 0.42404139 0.28103691 0.43785223 0.28675756
		 0.44475758 0.28961784 0.43094689 0.28389728 0.42404151 0.28103697 0.42404145 0.28103697
		 0.42404148 0.28103691 0.49309471 0.30963975 0.49309465 0.30963972 0.5 0.3125 0.49309474
		 0.30963975 0.49309471 0.30963975 0.48618945 0.3067795 0.47928423 0.30391923 0.47928417
		 0.30391926 0.49309471 0.30963975 0.48618942 0.30677947 0.47928411 0.3039192 0.47928411
		 0.3039192 0.47928414 0.30391923 0.47237888 0.30105895 0.45166296 0.29247811 0.4516629
		 0.29247814 0.45166284 0.29247808 0.45856822 0.29533839 0.46547362 0.2981987 0.57946926
		 0.2795828 0.58459038 0.27746153 0.58113772 0.27889162 0.57772362 0.2803058 0.57598382
		 0.28102651 0.57768506 0.28032178 0.57429028 0.28172797 0.57260376 0.28242663 0.57083762
		 0.28315812 0.56905448 0.28389674 0.57077974 0.28318208 0.56733298 0.28460982 0.56561154
		 0.28532287 0.56735229 0.28460184 0.56909138 0.28388149 0.56738496 0.28458828 0.55874074
		 0.28816876 0.55696905 0.28890264 0.56391406 0.286026 0.5690797 0.28388631;
	setAttr ".uvst[0].uvsp[3750:3999]" 0.56738603 0.28458786 0.5656991 0.28528658
		 0.55519539 0.28963739 0.55696905 0.28890264 0.55351639 0.29033279 0.5534848 0.29034591
		 0.55175257 0.29106337 0.55006367 0.29176295 0.55346906 0.29035246 0.55519295 0.28963843
		 0.55346906 0.29035246 0.54486215 0.29391745 0.54315835 0.2946232 0.54658949 0.29320198
		 0.54831684 0.29248646 0.55002642 0.29177842 0.55173743 0.29106972 0.55346906 0.29035246
		 0.55524105 0.28961834 0.55351639 0.29033279 0.55696642 0.28890371 0.5586862 0.28819135
		 0.55695063 0.28891027 0.55522144 0.28962651 0.53092527 0.29969037 0.52934772 0.30034381
		 0.52589506 0.30177391 0.52924854 0.30038488 0.52752358 0.30109939 0.5224424 0.30320406
		 0.5326516 0.29897529 0.5377875 0.29684785 0.5361042 0.29754514 0.52067053 0.303938
		 0.51553702 0.30606437 0.5258469 0.3017939 0.53102326 0.29964978 0.53270262 0.29895416
		 0.53438628 0.2982567 0.53955686 0.29611498 0.55174363 0.29106712 0.55006373 0.29176295
		 0.55002058 0.2917808 0.54829413 0.29249594 0.54651833 0.29323149 0.54474866 0.29396445
		 0.50247264 0.31147581 0.50172633 0.31178492 0.50313091 0.31120318 0.50377083 0.31093806
		 0.50754237 0.30937588 0.5113306 0.30780673 0.51323438 0.30701816 0.50627744 0.30989987
		 0.51256084 0.30729717 0.51884431 0.30469447 0.5169723 0.30546987 0.5151009 0.30624509
		 0.52013981 0.30415785 0.54403079 0.29426184 0.54833734 0.292478 0.53945065 0.296159
		 0.53488654 0.29804954 0.53005147 0.30005229 0.52520329 0.30206043 0.50690538 0.30963972
		 0.50690532 0.30963972 0.51381069 0.30677944 0.52071607 0.30391917 0.52071607 0.30391914
		 0.52071607 0.30391914 0.52071607 0.30391914 0.52762139 0.30105883 0.5483374 0.29247802
		 0.54833734 0.292478 0.54833734 0.29247797 0.54833734 0.292478 0.54143202 0.29533827
		 0.53452677 0.29819858 0.53452671 0.29819855 0.54833734 0.292478 0.55524272 0.28961772
		 0.54143202 0.29533827 0.53452677 0.29819855 0.53452671 0.29819852 0.53452671 0.29819855
		 0.60358012 0.26959571 0.60358012 0.26959571 0.61048543 0.26673543 0.60358012 0.26959574
		 0.60358012 0.26959571 0.59667474 0.27245599 0.58976936 0.27531627 0.58976936 0.2753163
		 0.60358006 0.26959571 0.59667474 0.27245599 0.5897693 0.2753163 0.58976936 0.2753163
		 0.58976936 0.2753163 0.58286405 0.27817658 0.56214803 0.28675744 0.56214809 0.28675744
		 0.56214803 0.28675744 0.56905341 0.28389716 0.57595873 0.28103688 0.64341432 0.18723819
		 0.64552397 0.18214503 0.64409381 0.18559769 0.64268738 0.18899316 0.64196229 0.19074368
		 0.64266366 0.18905038 0.64126909 0.19241723 0.64057338 0.19409673 0.63983893 0.19586992
		 0.63908792 0.19768301 0.63980335 0.19595569 0.63837504 0.19940391 0.63766235 0.20112482
		 0.63838696 0.19937533 0.63911074 0.19762793 0.63840878 0.19932263 0.63482171 0.20798244
		 0.63408267 0.2097666 0.63696235 0.20281443 0.63910162 0.19764979 0.63840437 0.1993331
		 0.63770872 0.20101275 0.63335955 0.21151237 0.63408279 0.20976639 0.63265264 0.21321905
		 0.63264722 0.21323216 0.63193035 0.21496297 0.63122255 0.21667174 0.63264447 0.21323872
		 0.63335878 0.21151425 0.63264447 0.21323878 0.62907314 0.22186066 0.62836224 0.22357705
		 0.6297878 0.22013536 0.63050252 0.21841002 0.63121521 0.21668926 0.6319288 0.21496642
		 0.63264441 0.21323881 0.63336712 0.2114941 0.63265252 0.21321926 0.63408196 0.20976844
		 0.63479626 0.20804389 0.63407922 0.20977496 0.63336331 0.21150324 0.62330556 0.23578493
		 0.62264162 0.23738775 0.62121147 0.24084041 0.62260747 0.23747012 0.62189841 0.239182
		 0.61978132 0.24429306 0.62402058 0.23405865 0.62614524 0.22892925 0.62545073 0.23060602
		 0.61905825 0.24603878 0.61692107 0.25119841 0.62120044 0.24086711 0.62334263 0.23569539
		 0.62404364 0.23400298 0.62474722 0.23230419 0.62688082 0.22715339 0.63191301 0.21500455
		 0.63122237 0.21667193 0.63120186 0.21672155 0.63049108 0.21843745 0.62975466 0.22021538
		 0.62901896 0.22199146 0.61149842 0.26429006 0.61120051 0.26500911 0.61176896 0.2636368
		 0.61203331 0.26299855 0.61358273 0.25925794 0.61513567 0.25550875 0.61592674 0.25359887
		 0.61307061 0.26049423 0.61567414 0.25420883 0.61827731 0.24792427 0.61748791 0.24982999
		 0.61670011 0.25173184 0.61878705 0.24669358 0.6286875 0.22279172 0.63050735 0.21839827
		 0.62680268 0.22734213 0.62492198 0.23188271 0.62289596 0.23677391 0.62086648 0.2416734
		 0.61334568 0.25983009 0.61334574 0.25983009 0.61620599 0.25292477 0.6190663 0.24601944
		 0.61906624 0.24601941 0.61906624 0.24601947 0.61906624 0.2460195 0.62192655 0.23911417
		 0.63050735 0.21839808 0.63050747 0.21839806 0.63050735 0.21839812 0.63050735 0.21839814
		 0.6276471 0.22530349 0.62478685 0.23220883 0.62478685 0.23220885 0.63050735 0.21839821
		 0.63336766 0.21149293 0.62764704 0.22530356 0.62478685 0.23220888 0.62478679 0.23220888
		 0.62478673 0.23220886 0.65338969 0.16315535 0.65338969 0.16315535 0.65625 0.15625
		 0.65338969 0.16315536 0.65338963 0.16315538 0.65052938 0.17006074 0.64766914 0.17696612
		 0.64766908 0.17696613 0.65338975 0.16315538 0.65052938 0.17006074 0.64766902 0.17696609
		 0.64766908 0.1769661 0.64766914 0.17696612 0.64480877 0.18387148 0.63622791 0.20458758
		 0.63622797 0.20458755 0.63622797 0.20458755 0.63908827 0.19768219 0.64194852 0.19077685
		 0.6233055 0.076715082 0.62121147 0.071659602 0.62264162 0.075112276 0.62403762 0.078482576
		 0.62476712 0.080243848 0.62407178 0.078564942 0.62545073 0.08189404 0.6261453 0.083570763
		 0.62688088 0.085346669 0.62764663 0.087195516 0.62693208 0.085470282 0.62835956 0.088916659
		 0.62907249 0.090637788 0.62834251 0.088875458 0.62761283 0.087113641 0.62831098 0.088799305
		 0.63191301 0.097495429 0.63265252 0.09928073 0.62977171 0.092325777 0.62762958 0.087154254;
	setAttr ".uvst[0].uvsp[4000:4127]" 0.62832451 0.088831961 0.62901902 0.09050858
		 0.63335955 0.10098763 0.63265264 0.099280953 0.63408279 0.10273363 0.63407731 0.1027205
		 0.63479501 0.10445305 0.63551289 0.10618629 0.63407457 0.10271393 0.63335884 0.10098574
		 0.63407457 0.1027139 0.63765848 0.11136591 0.6383732 0.11309163 0.63694292 0.10963856
		 0.63622749 0.10791121 0.63551009 0.10617931 0.63479245 0.10444688 0.63407457 0.10271386
		 0.63336712 0.1010059 0.63408267 0.1027334 0.63265181 0.099278875 0.63193524 0.097549118
		 0.63264906 0.099272363 0.63336331 0.10099673 0.64341432 0.12526181 0.64409381 0.12690231
		 0.64552397 0.13035497 0.64411753 0.12695953 0.64482325 0.12866327 0.64695412 0.13380766
		 0.64269924 0.12353546 0.64057338 0.11840327 0.64126909 0.12008277 0.64767021 0.13553651
		 0.64981437 0.14071298 0.64552641 0.13036093 0.64338267 0.12518537 0.64267802 0.12348419
		 0.64197266 0.12178133 0.63983893 0.11663006 0.63482165 0.10451758 0.63551283 0.10618608
		 0.63553226 0.10623288 0.63624126 0.10794451 0.63697422 0.10971421 0.63770866 0.11148725
		 0.65525389 0.15384512 0.65553492 0.15452367 0.65498614 0.15319872 0.65472209 0.15256116
		 0.65319216 0.1488677 0.6516614 0.14517216 0.65086806 0.14325693 0.65368748 0.15006353
		 0.65108162 0.1437725 0.64847839 0.13748778 0.64928764 0.13944148 0.65009284 0.14138526
		 0.64800775 0.13635156 0.6380831 0.11239138 0.63622785 0.10791241 0.63996291 0.11692949
		 0.64184099 0.12146352 0.64388955 0.1264092 0.64594007 0.13135947 0.65338969 0.14934465
		 0.65338969 0.14934465 0.65052944 0.14243931 0.6476692 0.13553396 0.6476692 0.13553399
		 0.6476692 0.13553393 0.64766914 0.1355339 0.64480889 0.12862855 0.63622802 0.10791259
		 0.63622797 0.10791263 0.63622797 0.10791256 0.63622797 0.10791252 0.63908827 0.11481786
		 0.64194846 0.12172319 0.64194858 0.12172318 0.63622791 0.10791243 0.63336766 0.10100707
		 0.63908815 0.11481779 0.64194852 0.12172315 0.64194846 0.12172316 0.64194852 0.12172317
		 0.61334574 0.052669901 0.61334574 0.05266992 0.61334574 0.052669901 0.61334574 0.052669898
		 0.61620605 0.059575211 0.61906624 0.066480517 0.61906624 0.06648051 0.61334574 0.052669879
		 0.61620599 0.059575185 0.61906624 0.066480488 0.61906624 0.066480495 0.6190663 0.066480502
		 0.62192655 0.07338582 0.63050735 0.094101757 0.63050735 0.094101772 0.63050735 0.094101794
		 0.6276471 0.087196469 0.62478679 0.080291145 0.65338969 0.14934462 0.65625 0.15625
		 0.65625 0.15625 0.65338969 0.14934462 0.64766908 0.1769661 0.64480877 0.18387148
		 0.64480877 0.18387148 0.64766914 0.17696612 0.65338969 0.16315538 0.65338981 0.16315539
		 0.65052933 0.17006074 0.65052938 0.17006075 0.65052938 0.14243923 0.65052938 0.14243925
		 0.64480877 0.12862852 0.64480877 0.12862852 0.64766908 0.13553387 0.64766908 0.13553388
		 0.63908821 0.1976822 0.63622797 0.20458755 0.63622797 0.20458755 0.63908827 0.19768219
		 0.64194846 0.19077684 0.64194852 0.19077685;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".pt[4062:4085]" -type "float3"  4.1242404 -0.074261501 0.051871866 
		4.1385045 -0.074262924 0.86977482 3.216589 0.07428088 0.051871866 3.230855 0.07427913 
		0.86977488 4.181284 -0.07428088 -0.86904645 4.1670184 -0.07428088 -0.83458573 3.2736323 
		0.074260153 -0.86904669 3.259361 0.074260153 -0.83458579 4.1385045 -0.074261501 -0.76603019 
		3.230855 0.07428088 -0.76603019 4.1527886 -0.074262924 -0.80048966 3.2451332 0.07427913 
		-0.80048978 3.245126 0.07427913 0.90423435 4.1527824 -0.074262924 0.90423411 4.181273 
		-0.07428088 0.97279209 3.2736213 0.074260153 0.97279263 4.1670299 -0.074270301 0.9385125 
		3.2593739 0.074270301 0.93851274 3.3021991 0.07427913 -0.93833166 4.209856 -0.074262924 
		-0.9383319 4.224122 -0.074262924 -0.97279251 3.3164656 0.07427913 -0.97279263 4.1955919 
		-0.074262924 -0.90387118 3.2879338 0.07427913 -0.90387142;
	setAttr -s 4086 ".vt";
	setAttr ".vt[0:165]"  0.70710754 -0.99999619 -0.70710671 0 -0.99999619 -0.99999988
		 -0.70710754 -0.99999619 -0.70710671 -1 -0.99999619 0 -0.70710754 -0.99999619 0.70710671
		 0 -0.99999619 0.99999988 0.70710754 -0.99999619 0.70710677 1 -0.99999619 0 0.70710754 1 -0.70710671
		 0 1 -0.99999988 -0.70710754 1 -0.70710671 -1 1 0 -0.70710754 1 0.70710671 0 1 0.99999988
		 0.70710754 1 0.70710677 1 1 0 0 1 0 11.48075867 -3.40997791 -0.70710671 10.77365112 -3.40997791 -0.99999988
		 10.066543579 -3.40997982 -0.70710671 9.77365112 -3.40997791 -1.9520861e-15 10.066543579 -3.40997982 0.70710671
		 10.77365112 -3.40997982 0.99999988 11.48075867 -3.40997791 0.70710677 11.77365112 -3.40997791 -1.9275209e-15
		 30.10585022 -6.45802402 -0.70715356 29.39890289 -6.45802402 -0.99999988 29.39868164 -6.45802402 -2.8250335e-05
		 28.69161987 -6.45802402 -0.70705992 28.3985672 -6.45802402 -4.6806483e-05 28.69145966 -6.45802402 0.70697999
		 29.39873505 -6.45802402 0.99993366 30.10585022 -6.45802021 0.70712048 30.3985672 -6.45802402 -6.560611e-12
		 5.74037933 -2.20498848 -0.8535533 0.35355377 -0.99999619 -0.8535533 6.093933105 -2.20498848 -0.70710671
		 11.1272049 -3.40997791 -0.8535533 5.38682556 -2.20498848 -0.99999988 5.03327179 -2.20498848 -0.8535533
		 -0.35355377 -0.99999619 -0.8535533 10.42009735 -3.40997791 -0.8535533 4.67971802 -2.20498848 -0.70710671
		 4.53327179 -2.20498848 -0.35355335 -0.85355377 -0.99999619 -0.35355335 9.92009735 -3.40997791 -0.35355335
		 4.38682556 -2.20498848 -9.7604305e-16 4.53327179 -2.20498848 0.3535533 -0.85355377 -0.99999619 0.35355335
		 9.92009735 -3.40997982 0.35355335 4.67971802 -2.20498848 0.70710671 5.03327179 -2.20498848 0.8535533
		 -0.35355377 -0.99999619 0.8535533 10.42009735 -3.40997982 0.8535533 5.38682556 -2.20498848 0.99999988
		 5.74037933 -2.20498848 0.8535533 0.35355377 -0.99999619 0.8535533 11.1272049 -3.40997791 0.8535533
		 6.093933105 -2.20498848 0.70710677 6.24037933 -2.20498848 0.35355341 0.85355377 -0.99999619 0.35355338
		 11.6272049 -3.40997791 0.35355338 6.38682556 -2.20498848 -9.6376044e-16 6.24037933 -2.20498848 -0.35355335
		 0.85355377 -0.99999619 -0.35355335 11.6272049 -3.40997791 -0.35355335 2.87018585 -1.60249138 -0.92677653
		 2.69341278 -1.60249138 -0.99999988 0.1767807 -0.99999619 -0.92677659 3.046966553 -1.60249138 -0.8535533
		 5.56360626 -2.20498848 -0.92677659 2.16307831 -1.60249138 -0.78033 1.98630524 -1.60249138 -0.70710671
		 -0.53033447 -0.99999619 -0.78033 2.33985901 -1.60249138 -0.8535533 4.85649872 -2.20498848 -0.78033
		 1.76663971 -1.60249138 -0.17677665 1.69341278 -1.60249138 -4.8802153e-16 -0.9267807 -0.99999619 -0.17677668
		 1.83985901 -1.60249138 -0.35355335 4.46005249 -2.20498848 -0.17677668 1.91308594 -1.60249138 0.53033
		 1.98630524 -1.60249138 0.70710671 -0.78033447 -0.99999619 0.53033006 1.83985901 -1.60249138 0.35355335
		 4.60649872 -2.20498848 0.53033 2.51663208 -1.60249138 0.92677659 2.69341278 -1.60249138 0.99999988
		 -0.1767807 -0.99999619 0.92677659 2.33985901 -1.60249138 0.8535533 5.21005249 -2.20498848 0.92677659
		 3.22373962 -1.60249138 0.78033 3.40052032 -1.60249043 0.70710677 0.53033447 -0.99999619 0.78033006
		 3.046966553 -1.60249138 0.8535533 5.9171524 -2.20498848 0.78033006 3.62018585 -1.60249138 0.17677669
		 3.69341278 -1.60249138 -4.8188022e-16 0.9267807 -0.99999619 0.17677669 3.54696655 -1.60249138 0.35355341
		 6.31359863 -2.20498848 0.17677671 3.47373962 -1.60249043 -0.53033006 3.40052032 -1.60249043 -0.70710671
		 0.78033447 -0.99999619 -0.53033006 3.54696655 -1.60249138 -0.35355335 6.1671524 -2.20498848 -0.53033006
		 3.22373962 -1.60249138 -0.78032994 0.53033447 -0.99999619 -0.78033 5.9171524 -2.20498848 -0.78033
		 8.61057281 -2.80748272 -0.78033 8.78734589 -2.80748272 -0.70710671 11.30397797 -3.40997791 -0.78033
		 8.43379211 -2.80748367 -0.8535533 8.25701904 -2.80748367 -0.92677653 10.95042419 -3.40997791 -0.92677659
		 8.080238342 -2.80748367 -0.99999988 2.51663208 -1.60249138 -0.92677653 -0.1767807 -0.99999619 -0.92677659
		 5.21005249 -2.20498848 -0.92677659 7.90345764 -2.80748367 -0.92677659 10.59687042 -3.40997791 -0.92677659
		 7.72668457 -2.80748367 -0.8535533 7.54990387 -2.80748367 -0.78033 10.24331665 -3.40997791 -0.78033
		 7.3731308 -2.80748463 -0.70710671 1.91308594 -1.60249138 -0.53033012 -0.78033447 -0.99999619 -0.53033006
		 4.60649872 -2.20498848 -0.53033006 7.29990387 -2.80748367 -0.53033006 9.99332428 -3.40997791 -0.53033006
		 7.22668457 -2.80748367 -0.35355335 7.15345764 -2.80748367 -0.17677668 9.84687805 -3.40997791 -0.17677668
		 7.080238342 -2.80748367 -1.4640646e-15 1.76663971 -1.60249138 0.17677668 -0.9267807 -0.99999619 0.17677668
		 4.46005249 -2.20498848 0.17677665 7.15345764 -2.80748367 0.17677665 9.84687805 -3.40997791 0.17677668
		 7.22668457 -2.80748463 0.35355335 7.29990387 -2.80748463 0.53033 9.99332428 -3.40997982 0.53033006
		 7.3731308 -2.80748558 0.70710671 2.16307831 -1.60249138 0.78032994 -0.53033447 -0.99999619 0.78033
		 4.85649872 -2.20498848 0.78033 7.54990387 -2.80748463 0.78033 10.24331665 -3.40997982 0.78033
		 7.72668457 -2.80748463 0.8535533 7.90345764 -2.80748463 0.92677653 10.59687042 -3.40997982 0.92677659
		 8.080238342 -2.80748463 0.99999988 2.87018585 -1.60249138 0.92677653 0.1767807 -0.99999619 0.92677659
		 5.56360626 -2.20498848 0.92677659 8.25701904 -2.80748367 0.92677659 10.95042419 -3.40997791 0.92677659
		 8.43379211 -2.80748367 0.8535533 8.61057281 -2.80748272 0.78032994 11.30397797 -3.40997791 0.78033006
		 8.78734589 -2.80748272 0.70710677 3.47373962 -1.60249138 0.53033012 0.78033447 -0.99999619 0.53033006
		 6.1671524 -2.20498848 0.53033006 8.86056519 -2.80748367 0.53033006 11.5539856 -3.40997791 0.53033006;
	setAttr ".vt[166:331]" 8.93378448 -2.80748367 0.35355341 9.0070114136 -2.80748367 0.17677671
		 11.70043182 -3.40997791 0.17677669 9.080230713 -2.80748367 -1.4456404e-15 3.62018585 -1.60249138 -0.17677668
		 0.9267807 -0.99999619 -0.17677668 6.31359863 -2.20498848 -0.17677668 9.0070114136 -2.80748367 -0.17677665
		 11.70043182 -3.40997791 -0.17677668 8.93378448 -2.80748367 -0.35355335 8.86057281 -2.80748367 -0.53033012
		 11.5539856 -3.40997791 -0.53033006 8.43379211 -2.80748367 0.8535533 5.74037933 -2.20498848 0.8535533
		 5.56360626 -2.20498848 0.92677659 8.25701904 -2.80748367 0.92677659 18.13092804 -1.30970955 0.56501865
		 18.14283752 -1.30971146 0.53629488 18.56897736 -1.40769577 0.56501865 18.58087921 -1.40769672 0.53629488
		 18.17855835 -1.30971146 0.073873445 18.16666412 -1.30971146 -0.33104968 18.61660767 -1.40769672 0.073873445
		 18.60468292 -1.40769672 -0.33104968 18.59278107 -1.40769958 0.50756121 18.15473938 -1.30971336 0.50756121
		 18.16666412 -1.30971146 0.47879529 18.60468292 -1.40769672 0.47879529 18.59278107 -1.40769768 -0.35981345
		 18.15475464 -1.30971336 -0.35981345 18.14282227 -1.30971336 -0.38857603 18.58087921 -1.40769863 -0.38857603
		 20.43974304 -4.93399334 -0.85356563 10.86203766 -3.40997791 -0.96338814 11.03881073 -3.40997791 -0.89016491
		 11.21559143 -3.40997791 -0.81694162 11.3923645 -3.40997791 -0.74371833 20.79330444 -4.93400097 -0.70713007
		 29.75238037 -6.45802402 -0.85357672 20.086273193 -4.93400097 -0.99999988 19.73272705 -4.9340086 -0.85354239
		 10.15493011 -3.40997791 -0.74371833 10.33170319 -3.40997791 -0.81694162 10.50848389 -3.40997791 -0.89016491
		 10.68525696 -3.40997791 -0.96338814 29.045257568 -6.45802402 -0.85352993 19.37908173 -4.93400097 -0.70708334
		 19.23259735 -4.93400097 -0.35355476 9.81026459 -3.40997791 -0.088388339 9.88349152 -3.40997791 -0.26516503
		 9.95671082 -3.40997791 -0.44194168 10.029937744 -3.40997791 -0.61871839 28.54509735 -6.45802402 -0.35355335
		 19.086105347 -4.93400097 -2.3403241e-05 19.23253632 -4.93399906 0.35350993 10.029937744 -3.40997982 0.61871839
		 9.95671082 -3.40997982 0.44194168 9.88349152 -3.40997791 0.26516503 9.81026459 -3.40997791 0.088388339
		 28.54501343 -6.45802402 0.3534666 19.3789978 -4.93400097 0.70704335 19.73264313 -4.9340086 0.85350591
		 10.68525696 -3.40997982 0.96338814 10.50848389 -3.40997982 0.89016491 10.33170319 -3.40997982 0.81694162
		 10.15493011 -3.40997982 0.74371833 29.045097351 -6.45802402 0.85345685 20.08618927 -4.93400097 0.99996674
		 20.43974304 -4.93399906 0.85354012 11.3923645 -3.40997791 0.74371833 11.21559143 -3.40997791 0.81694168
		 11.03881073 -3.40997791 0.89016491 10.86203766 -3.40997791 0.96338814 29.75229645 -6.45802212 0.85352707
		 20.79330444 -4.93399906 0.70711362 20.93968964 -4.93399715 0.35355836 11.73703766 -3.40997791 0.088388346
		 11.66381836 -3.40997791 0.26516503 11.59059143 -3.40997791 0.44194174 11.51737213 -3.40997791 0.61871839
		 30.2522049 -6.45802212 0.35356024 21.086112976 -4.93400097 -3.2812694e-12 20.9397049 -4.93400097 -0.35356662
		 11.51737213 -3.40997791 -0.61871839 11.59059143 -3.40997791 -0.44194168 11.66381836 -3.40997791 -0.26516503
		 11.73703766 -3.40997791 -0.088388339 30.2522049 -6.45802402 -0.35357678 15.61099243 -4.18313694 -0.9532246
		 15.42996216 -4.17198849 -0.99999988 10.81784058 -3.40997791 -0.98169404 15.65089417 -4.17198467 -0.90847695
		 20.26301575 -4.93399715 -0.92678273 14.90392303 -4.18314457 -0.75387704 14.72281647 -4.17199039 -0.70709503
		 10.11074066 -3.40997887 -0.72541255 14.94382477 -4.1719923 -0.7986303 19.5559082 -4.93400478 -0.7803129
		 14.54498291 -4.18322659 -0.11210573 14.42987823 -4.17198849 -1.1701621e-05 9.79196167 -3.40997791 -0.044194169
		 14.52143097 -4.17198849 -0.22097155 19.15934753 -4.93400097 -0.17678908 14.74627686 -4.18335438 0.59620625
		 14.72277069 -4.17199039 0.707075 10.048240662 -3.40997982 0.66291255 14.63124084 -4.17198849 0.48611411
		 19.30577087 -4.93400097 0.53027666 15.38995361 -4.18344784 0.95442754 15.42992401 -4.17199039 0.99998331
		 10.72945404 -3.40997982 0.98169404 15.20895386 -4.17199421 0.90844703 19.90942383 -4.93400478 0.9267363
		 16.097061157 -4.18344402 0.752657 16.13703156 -4.17198658 0.70711017 11.43656158 -3.40997791 0.72541261
		 15.91605377 -4.17198658 0.79862928 20.61652374 -4.93399906 0.78032678 16.45339966 -4.18335056 0.1108749
		 16.42987823 -4.17198849 -1.6415984e-12 11.75534821 -3.40997791 0.044194173 16.33836365 -4.17198658 0.22097336
		 21.012901306 -4.93399906 0.17677918 16.25211334 -4.18322659 -0.59501708 16.13703156 -4.17198849 -0.70711839
		 11.49906158 -3.40997791 -0.66291255 16.22853851 -4.17198849 -0.48614252 20.86650085 -4.93400097 -0.53034836
		 15.64792633 -4.16447544 -0.89069712 10.90622711 -3.40997791 -0.94508243 10.99462128 -3.40997791 -0.90847069
		 15.73928833 -4.17198467 -0.87186521 15.73633575 -4.16433811 -0.85372686 11.083007813 -3.40997791 -0.87185913
		 11.17139435 -3.40997791 -0.83524746 15.82767487 -4.17198467 -0.83525354 15.82475281 -4.16419697 -0.81674337
		 11.25978088 -3.40997791 -0.79863584 11.34817505 -3.40997791 -0.76202416 15.91605377 -4.17198467 -0.79864204
		 16.097061157 -4.18344402 -0.75266767 11.43656158 -3.40997791 -0.72541255 20.61653137 -4.93399715 -0.78034788
		 25.27279663 -5.69600582 -0.78035682 25.44957733 -5.69601154 -0.70714188 29.9291153 -6.45802402 -0.78036505
		 25.096061707 -5.69600773 -0.85357112 24.91932678 -5.69600964 -0.92678541 29.57563782 -6.45802402 -0.92678833
		 24.74259186 -5.69601154 -0.99999988 14.94085693 -4.16448116 -0.8164103 10.1991272 -3.40997791 -0.76202416
		 10.28751373 -3.40997791 -0.79863584 15.032218933 -4.1719923 -0.83524197 15.029273987 -4.16434574 -0.85338032
		 10.37590027 -3.40997791 -0.83524746 10.4642868 -3.40997791 -0.87185913 15.12060547 -4.1719923 -0.87185371
		 15.11768341 -4.16420269 -0.89036381 10.55268097 -3.40997791 -0.90847069 10.6410675 -3.40997791 -0.94508243
		 15.208992 -4.1719923 -0.90846521 15.38999939 -4.18344784 -0.95444524 10.72945404 -3.40997791 -0.98169404
		 19.90950012 -4.93400478 -0.9267711 24.56581116 -5.69601727 -0.92676836;
	setAttr ".vt[332:497]" 29.22207642 -6.45802402 -0.92676491 24.38899231 -5.69601536 -0.85353613
		 24.21217346 -5.69601345 -0.78030384 28.86843872 -6.45802402 -0.7802949 24.035346985 -5.69601154 -0.7070716
		 14.49291992 -4.16440105 -0.26411167 9.82857513 -3.40997791 -0.13258252 9.86518097 -3.40997791 -0.22097084
		 14.55804443 -4.17198849 -0.30935991 14.52954865 -4.16434193 -0.35338655 9.90179443 -3.40997791 -0.30935919
		 9.9384079 -3.40997791 -0.39774752 14.59465027 -4.17198849 -0.39774823 14.56617737 -4.16428471 -0.44267523
		 9.97502136 -3.40997791 -0.4861359 10.011627197 -3.40997791 -0.57452416 14.63126373 -4.17198849 -0.48613653
		 14.74630737 -4.18335438 -0.59622788 10.048240662 -3.40997887 -0.66291255 19.30583191 -4.93400097 -0.53031904
		 23.96209717 -5.69601154 -0.53031319 28.6183548 -6.45802402 -0.53030664 23.88883972 -5.69601154 -0.35355407
		 23.81558228 -5.69600964 -0.17679496 28.47183228 -6.45802402 -0.17680009 23.74234009 -5.69601154 -3.5104862e-05
		 14.56615448 -4.16428471 0.44265303 10.011627197 -3.40997982 0.57452416 9.97502136 -3.40997982 0.4861359
		 14.59462738 -4.17198849 0.39772582 14.52951813 -4.16434193 0.35336438 9.9384079 -3.40997982 0.39774752
		 9.90179443 -3.40997887 0.30935919 14.55801392 -4.17198658 0.3093375 14.49289703 -4.16439915 0.26408947
		 9.86518097 -3.40997791 0.22097084 9.82857513 -3.40997791 0.13258252 14.52140045 -4.17198658 0.22094914
		 14.54496765 -4.18322468 0.11208212 9.79196167 -3.40997791 0.044194169 19.15932465 -4.93399906 0.17674327
		 23.81555176 -5.69600964 0.17672673 28.4717865 -6.45802402 0.17670989 23.88877106 -5.69600964 0.35348824
		 23.96199799 -5.69600964 0.53024983 28.61823273 -6.45802402 0.53022325 24.035232544 -5.69601154 0.7070117
		 15.11764526 -4.1642046 0.89034575 10.6410675 -3.40997982 0.94508243 10.55268097 -3.40997982 0.90847069
		 15.12055969 -4.17199421 0.87183547 15.02923584 -4.16434574 0.85336232 10.4642868 -3.40997982 0.87185913
		 10.37590027 -3.40997982 0.83524746 15.032173157 -4.17199421 0.83522379 14.94081879 -4.16448307 0.8163923
		 10.28751373 -3.40997982 0.79863584 10.1991272 -3.40997982 0.76202416 14.94378662 -4.17199421 0.79861212
		 14.90389252 -4.18314648 0.75385761 10.11074066 -3.40997982 0.72541255 19.55582428 -4.93400478 0.78027463
		 24.21205139 -5.69601345 0.78024662 28.8682785 -6.45802402 0.78021842 24.38887024 -5.69601536 0.85348141
		 24.56568909 -5.69601727 0.92671609 29.2219162 -6.45802402 0.92669523 24.74246216 -5.69601154 0.99995011
		 15.82475281 -4.16419888 0.81673086 11.34817505 -3.40997791 0.76202422 11.25978088 -3.40997791 0.79863584
		 15.82767487 -4.17198658 0.8352409 15.73633575 -4.16434193 0.85371435 11.17139435 -3.40997791 0.83524746
		 11.083007813 -3.40997791 0.87185913 15.7392807 -4.17198658 0.87185252 15.6479187 -4.16447735 0.89068431
		 10.99462128 -3.40997791 0.90847069 10.90622711 -3.40997791 0.94508243 15.65088654 -4.17198849 0.90846413
		 15.6109848 -4.18313885 0.95320958 10.81784058 -3.40997887 0.98169404 20.26296997 -4.93400097 0.9267534
		 24.91924286 -5.69600964 0.9267419 29.57550812 -6.45802212 0.92673039 25.09602356 -5.69600964 0.85353363
		 25.27279663 -5.69600773 0.78032535 29.92906952 -6.45802021 0.78032374 25.44957733 -5.69600773 0.70711702
		 16.27328491 -4.1642828 0.26443473 11.71873474 -3.40997791 0.13258252 11.68212128 -3.40997791 0.22097087
		 16.30175781 -4.17198658 0.3093617 16.23664856 -4.16434002 0.35372329 11.64550781 -3.40997791 0.30935919
		 11.60890198 -3.40997791 0.39774758 16.26513672 -4.17198658 0.39775005 16.2000351 -4.16439915 0.44299832
		 11.57228851 -3.40997791 0.4861359 11.53567505 -3.40997791 0.57452416 16.22853851 -4.17198658 0.48613837
		 16.25211334 -4.18322468 0.59501088 11.49906158 -3.40997791 0.66291261 20.86649323 -4.93399715 0.53033602
		 25.52276611 -5.69600773 0.53033847 30.17903137 -6.45802021 0.53034037 25.5959549 -5.69600964 0.35355929
		 25.66914368 -5.69600964 0.17678016 30.32538605 -6.45802212 0.17678012 25.74234009 -5.69601154 -4.9209404e-12
		 16.2000351 -4.16440105 -0.44300237 11.53567505 -3.40997791 -0.57452416 11.57228851 -3.40997791 -0.4861359
		 16.26514435 -4.17198849 -0.39775416 16.23665619 -4.16434193 -0.3537274 11.60890198 -3.40997791 -0.39774752
		 11.64550781 -3.40997791 -0.30935919 16.30176544 -4.17198849 -0.30936581 16.27328491 -4.16428471 -0.26443878
		 11.68212128 -3.40997791 -0.22097084 11.71873474 -3.40997791 -0.13258252 16.33837128 -4.17198849 -0.22097749
		 16.45339966 -4.18335247 -0.11087706 11.75534821 -3.40997791 -0.044194169 21.012901306 -4.93400097 -0.17678331
		 25.66914368 -5.69600964 -0.17678627 30.32538605 -6.45802402 -0.17678839 25.5959549 -5.69601154 -0.35357171
		 25.52276611 -5.69601154 -0.53035712 30.17903137 -6.45802402 -0.53036517 17.84845734 -4.55593395 -0.97001755
		 20.17464447 -4.93399906 -0.96339124 17.75811768 -4.55299473 -0.99999988 15.52047729 -4.17756176 -0.97661227
		 17.93700409 -4.55856609 -0.94000369 17.1413269 -4.55593967 -0.73707658 19.46749115 -4.93400288 -0.74369812
		 17.050949097 -4.55299664 -0.70708913 14.81336975 -4.17756748 -0.73048604 17.2299118 -4.55857372 -0.76709497
		 16.81207275 -4.55696583 -0.071966514 19.12273407 -4.93400097 -0.088406242 16.75799561 -4.55299473 -1.7552431e-05
		 14.48743439 -4.17760754 -0.056058712 16.85216522 -4.55861378 -0.1444474 17.0570755 -4.55884457 0.6357398
		 19.34238434 -4.93400097 0.61865997 17.050888062 -4.55299664 0.70705914 14.73451996 -4.17767239 0.65164065
		 17.026023865 -4.55867481 0.56324148 17.73577881 -4.56095219 0.97060955 19.99780273 -4.93400288 0.96335149
		 17.75805664 -4.55299664 0.99997503 15.40994263 -4.17772007 0.9772054 17.64968872 -4.55872631 0.94058186
		 18.44289398 -4.56094837 0.73647064 20.70491791 -4.93399906 0.74372023 18.46516418 -4.55299282 0.7071119
		 16.11704254 -4.17771626 0.72988355 18.35678864 -4.55872059 0.76649189 18.76419067 -4.55884266 0.071323894
		 21.049507141 -4.93399906 0.08838959 18.75798798 -4.55299473 -2.4614338e-12 16.44163513 -4.17767048 0.055437449
		 18.73314667 -4.55867481 0.14382702 18.5192337 -4.55696774 -0.63516963;
	setAttr ".vt[498:663]" 20.82990265 -4.93400097 -0.61873925 18.46516418 -4.55299473 -0.70712423
		 16.19457245 -4.17760754 -0.65106773 18.55931091 -4.55861378 -0.56268269 13.25978088 -3.78800964 -0.92703515
		 13.2564621 -3.79098225 -0.93593252 10.88413239 -3.40997791 -0.95423532 13.27708435 -3.78722668 -0.91788971
		 15.64941406 -4.16823101 -0.89958704 13.39247131 -3.78797054 -0.87197638 13.38904572 -3.79098225 -0.88101506
		 11.060905457 -3.40997791 -0.88101208 13.4096756 -3.78715706 -0.86279303 15.73781586 -4.16816235 -0.86279607
		 13.52515411 -3.78792858 -0.81691372 13.52163696 -3.79098225 -0.82609761 11.23768616 -3.40997791 -0.80778873
		 13.54227448 -3.78708649 -0.80768961 15.82621002 -4.16809177 -0.82599849 13.74481201 -3.79948711 -0.75517988
		 13.65421295 -3.79098225 -0.77118021 11.41446686 -3.40997791 -0.7345655 13.76681519 -3.79671097 -0.73904014
		 16.0065612793 -4.17771626 -0.77565485 23.033058167 -5.3150053 -0.74374425 20.70491791 -4.93399906 -0.74373901
		 23.1214447 -5.3150053 -0.70713603 25.3611908 -5.69600964 -0.74374938 22.944664 -5.31500149 -0.78035235
		 27.33585358 -6.077015877 -0.89018035 27.42421722 -6.077015877 -0.85357386 29.66400909 -6.45802402 -0.8901825
		 27.2474823 -6.077017784 -0.92678696 25.0076980591 -5.69600964 -0.89017832 12.55269623 -3.78801346 -0.78007197
		 12.54938507 -3.79098606 -0.77117431 10.17702484 -3.40997791 -0.75287127 12.56999207 -3.78723049 -0.78921723
		 14.94234467 -4.16823673 -0.80752033 12.68537903 -3.78797245 -0.83513063 12.68196106 -3.79098415 -0.82609183
		 10.35380554 -3.40997791 -0.82609445 12.70258331 -3.78716087 -0.84431392 15.03074646 -4.16816807 -0.84431124
		 12.81806946 -3.7879324 -0.89019316 12.81454468 -3.79098415 -0.88100934 10.53058624 -3.40997791 -0.89931786
		 12.83518219 -3.7870903 -0.89941728 15.11914825 -4.1680994 -0.88110876 13.037719727 -3.79948902 -0.95192838
		 12.94712067 -3.79098415 -0.93592674 10.70735931 -3.40997791 -0.97254103 13.0597229 -3.79671288 -0.96806961
		 15.29949188 -4.17772007 -0.93145531 22.32604218 -5.31500721 -0.96338469 19.99788666 -4.93400288 -0.96338558
		 22.41443634 -5.3150053 -0.99999988 24.65419769 -5.69601345 -0.96338415 22.23765564 -5.31501102 -0.92676973
		 26.62871552 -6.077019691 -0.81691617 26.71712494 -6.077019691 -0.85353303 28.95685577 -6.45802402 -0.81691235
		 26.54030609 -6.077019691 -0.78029937 24.30058289 -5.69601345 -0.81691998 12.16439056 -3.78926945 -0.17643607
		 12.16584778 -3.79098225 -0.15467995 9.81941223 -3.40997791 -0.11048542 12.16075134 -3.78718948 -0.19834709
		 14.50717926 -4.16819477 -0.2425416 12.21949005 -3.78928566 -0.30921984 12.22076416 -3.79098225 -0.28726247
		 9.89263916 -3.40997791 -0.28726208 12.21566772 -3.78715897 -0.33137286 14.54379272 -4.16816425 -0.33137321
		 12.27459717 -3.78930283 -0.44199979 12.27568054 -3.79098225 -0.41984499 9.96585846 -3.40997791 -0.46403879
		 12.27059937 -3.78713131 -0.4644056 14.58041382 -4.16813564 -0.42021173 12.39125061 -3.79830837 -0.59080464
		 12.33059692 -3.79098415 -0.55242747 10.039085388 -3.40997887 -0.6408155 12.39727783 -3.79666615 -0.62957025
		 14.68878937 -4.17767048 -0.54118222 21.67059326 -5.3150053 -0.61869687 19.342453 -4.93400097 -0.61870122
		 21.70721436 -5.3150053 -0.7070775 23.99872589 -5.69601154 -0.6186924 21.63396454 -5.3150053 -0.53031611
		 26.180336 -6.077017784 -0.2651757 26.21696472 -6.077017784 -0.35355371 28.508461 -6.45802402 -0.26517668
		 26.14370728 -6.077017784 -0.17679754 23.85221863 -5.69601154 -0.26517451 12.31733704 -3.79030895 0.5303964
		 12.33058929 -3.79098415 0.55241627 10.020782471 -3.40997982 0.59662127 12.28889465 -3.78713131 0.50858855
		 14.59869385 -4.16813564 0.4643836 12.26254272 -3.79033279 0.39773241 12.27566528 -3.79098415 0.41983378
		 9.94755554 -3.40997982 0.41984463 12.23396301 -3.78715897 0.3755559 14.56207275 -4.16816425 0.37554508
		 12.20774078 -3.7903595 0.2650744 12.2207489 -3.79098225 0.28725126 9.87433624 -3.40997791 0.24306795
		 12.179039 -3.78718758 0.24253015 14.52545166 -4.16819477 0.28671348 12.18270874 -3.79632759 0.11656906
		 12.16583252 -3.79098225 0.15466875 9.80110931 -3.40997791 0.066291258 12.16846466 -3.79660225 0.078138143
		 14.53318024 -4.17760754 0.16651563 21.45082092 -5.3150053 0.088352844 19.12271881 -4.93400097 0.08835993
		 21.41422272 -5.3150053 -2.925405e-05 23.77894592 -5.69600964 0.088345811 21.48743439 -5.3150053 0.176735
		 26.25350189 -6.077017784 0.44185701 26.21688843 -6.077017784 0.35347742 28.58162689 -6.45802402 0.44184491
		 26.29011536 -6.077017784 0.5302366 23.92538452 -5.69600964 0.44186905 12.92446899 -3.79091358 0.92672789
		 12.94710541 -3.79098606 0.93591762 10.66316223 -3.40997982 0.95423532 12.87935638 -3.78709221 0.91771412
		 15.16329956 -4.1680994 0.89939642 12.79190826 -3.79091644 0.87180746 12.81452179 -3.79098606 0.88100022
		 10.48638153 -3.40997982 0.88101208 12.74676514 -3.78716278 0.86261076 15.074897766 -4.16816998 0.8625989
		 12.6593399 -3.79092216 0.81688738 12.68193817 -3.79098606 0.82608271 10.30960846 -3.40997982 0.80778873
		 12.61416626 -3.7872324 0.80751407 14.98649597 -4.16823864 0.82580799 12.53153992 -3.79428959 0.75541878
		 12.54935455 -3.79098606 0.77116525 10.13283539 -3.40997982 0.7345655 12.50730896 -3.79656219 0.73963511
		 14.92383575 -4.1775713 0.77623487 21.7955246 -5.31500721 0.743644 19.46741486 -4.93400288 0.74365902
		 21.70711517 -5.3150053 0.70702755 24.12364197 -5.69601345 0.74362916 21.88394165 -5.31500912 0.78026062
		 26.80539703 -6.077021599 0.89008749 26.71697998 -6.077019691 0.85346913 29.13349915 -6.45802402 0.89007604
		 26.89379883 -6.077021599 0.92670566 24.47727203 -5.69601536 0.89009875 13.63157654 -3.79090977 0.7803635
		 13.65421295 -3.79098225 0.77117378 11.37026978 -3.40997791 0.75287127 13.58646393 -3.78708839 0.78937757
		 15.8704071 -4.16809368 0.80768001 13.49901581 -3.79091358 0.83528405 13.52162933 -3.79098225 0.82609129
		 11.1934967 -3.40997791 0.82609463 13.45386505 -3.78715897 0.84448093 15.78200531 -4.16816425 0.84447759
		 13.36644745 -3.79091644 0.89020419 13.38904572 -3.79098225 0.88100874;
	setAttr ".vt[664:829]" 11.016716003 -3.40997791 0.89931786 13.3212738 -3.78722668 0.8995775
		 15.69360352 -4.16823292 0.88126838 13.23863983 -3.79428768 0.95167035 13.2564621 -3.79098225 0.9359262
		 10.8399353 -3.40997887 0.97254103 13.2144165 -3.79655838 0.96745175 15.63093567 -4.17756557 0.93083692
		 22.50271606 -5.3150053 0.96335298 20.17457581 -4.93400097 0.96336007 22.4143219 -5.3150053 0.99995852
		 24.83084869 -5.69601154 0.963346 22.59111023 -5.3150053 0.92674768 27.51254272 -6.077015877 0.81692743
		 27.42415619 -6.077015877 0.85353035 29.84068298 -6.45802021 0.81692541 27.60092926 -6.077015877 0.78032458
		 25.18440247 -5.69600964 0.81692946 14.024452209 -3.79030704 0.17670073 14.037704468 -3.79098225 0.15468085
		 11.72789001 -3.40997791 0.11048543 13.99600983 -3.78713131 0.19850862 16.30582428 -4.16813564 0.24270405
		 13.96966553 -3.79033279 0.30936471 13.98278809 -3.79098225 0.28726336 11.65466309 -3.40997791 0.28726208
		 13.94107819 -3.78715706 0.33154124 16.26919556 -4.16816235 0.33154249 13.91487122 -3.79035759 0.44202277
		 13.9278717 -3.79098225 0.41984585 11.58144379 -3.40997791 0.46403882 13.8861618 -3.78718758 0.46456712
		 16.23258972 -4.16819286 0.42037418 13.88983917 -3.79632759 0.59052938 13.87295532 -3.79098225 0.55242836
		 11.50822449 -3.40997791 0.6408155 13.87558746 -3.79660034 0.62896174 16.2403183 -4.17760563 0.54057461
		 23.15803528 -5.3150034 0.61872643 20.82989502 -4.93399906 0.61872482 23.1214447 -5.3150034 0.70711529
		 25.48616791 -5.69600773 0.6187278 23.19463348 -5.3150034 0.53033721 27.96067047 -6.077015877 0.26517004
		 27.9240799 -6.077015877 0.35355979 30.28879547 -6.45802212 0.26517019 27.99726105 -6.077015877 0.17678013
		 25.63254547 -5.69600964 0.26516974 13.87149811 -3.78926945 -0.53067422 13.87295532 -3.79098225 -0.55243045
		 11.5265274 -3.40997791 -0.59662127 13.86785889 -3.78718948 -0.50876331 16.2142868 -4.16819477 -0.4645724
		 13.9265976 -3.78928566 -0.39789054 13.9278717 -3.79098225 -0.41984794 11.5997467 -3.40997791 -0.41984463
		 13.9227829 -3.78715897 -0.37573746 16.25090027 -4.16816425 -0.37574077 13.98170471 -3.78930283 -0.26511055
		 13.98278809 -3.79098225 -0.28726542 11.67297363 -3.40997791 -0.24306795 13.97770691 -3.78713131 -0.24270479
		 16.28752136 -4.16813564 -0.28690231 14.098342896 -3.79830647 -0.11630352 14.037704468 -3.79098225 -0.1546829
		 11.74619293 -3.40997791 -0.066291258 14.10437012 -3.79666615 -0.077535614 16.39588928 -4.17767048 -0.16592728
		 23.37761688 -5.3150053 -0.088392481 21.049507141 -4.93400097 -0.088391654 23.41422272 -5.3150053 -4.1011049e-12
		 25.70574188 -5.69601154 -0.088393137 23.34101868 -5.3150053 -0.17678478 27.88748932 -6.077017784 -0.44196779
		 27.9240799 -6.077017784 -0.35357425 30.21561432 -6.45802402 -0.44197097 27.85089874 -6.077017784 -0.53036118
		 25.55936432 -5.69601154 -0.44196442 13.19249725 -3.79926491 -0.9834013 13.10180664 -3.79098225 -0.99999988
		 10.79574585 -3.40997791 -0.99084699 13.2144165 -3.79655647 -0.96745926 13.23863983 -3.79428577 -0.9516772
		 10.8399353 -3.40997791 -0.97254103 15.6309433 -4.17755985 -0.93085074 18.023262024 -4.56076336 -0.90983045
		 18.045326233 -4.55299091 -0.88102126 20.35137939 -4.93399525 -0.89017409 12.48540497 -3.79927254 -0.72370142
		 12.39468384 -3.79098415 -0.70710087 10.088645935 -3.40997887 -0.7162596 12.50733185 -3.79656219 -0.73964471
		 12.53154755 -3.79428959 -0.75542831 10.13283539 -3.40997887 -0.7345655 14.9238739 -4.17756939 -0.7762537
		 17.31621552 -4.5607729 -0.79727334 17.33827209 -4.55300045 -0.8260864 19.64431 -4.93400669 -0.81692761
		 12.16278839 -3.79827976 -0.039667517 12.10176849 -3.79098225 -5.8508103e-06 9.7828064 -3.40997791 -0.022097085
		 12.16847229 -3.79660225 -0.078149945 12.182724 -3.79632759 -0.11658067 9.80110931 -3.40997791 -0.066291258
		 14.53321075 -4.17760754 -0.16653864 16.88368988 -4.5588541 -0.21697967 16.87701416 -4.55299473 -0.28726315
		 19.19596863 -4.93400097 -0.26517192 12.4119873 -3.79646873 0.66803569 12.39466095 -3.79098415 0.70709085
		 10.057388306 -3.40997982 0.6850096 12.39726257 -3.79666615 0.6295594 12.39122772 -3.79830837 0.59079343
		 10.039085388 -3.40997982 0.6408155 14.68875122 -4.17767048 0.54116023 16.98626709 -4.55706501 0.49103227
		 16.93188477 -4.55299473 0.41981199 19.26914978 -4.93399906 0.44189325 13.084114075 -3.79440784 0.98400617
		 13.10178375 -3.79098415 0.9999916 10.7515564 -3.40997982 0.99084699 13.059700012 -3.79671478 0.96806079
		 13.037704468 -3.79948997 0.95191926 10.70735931 -3.40997982 0.97254103 15.29945374 -4.17772198 0.93143725
		 17.56118774 -4.55603123 0.91075045 17.47080231 -4.55300236 0.88097644 19.82103729 -4.93400669 0.8901211
		 13.79121399 -3.79440403 0.72309166 13.8088913 -3.79098225 0.7071085 11.45866394 -3.40997791 0.71625966
		 13.76681519 -3.79671097 0.73903471 13.74481201 -3.79948711 0.75517392 11.41446686 -3.40997791 0.7345655
		 16.0065612793 -4.17771626 0.77564311 18.26828766 -4.55602169 0.7963171 18.17790222 -4.55299282 0.82608473
		 20.52812958 -4.93399906 0.81693351 14.11910248 -3.79646683 0.039056718 14.10176849 -3.79098225 -8.2176296e-13
		 11.76449585 -3.40997791 0.022097087 14.10437012 -3.79666424 0.077534534 14.098342896 -3.79830647 0.11630192
		 11.74619293 -3.40997791 0.066291258 16.39588165 -4.17766857 0.16592413 18.69340515 -4.55706501 0.21604101
		 18.63903046 -4.55299282 0.28726587 20.97629547 -4.93399906 0.26516876 13.86991119 -3.79827976 -0.66744918
		 13.8088913 -3.79098225 -0.70711255 11.48991394 -3.40997791 -0.6850096 13.87558746 -3.79660225 -0.62896478
		 13.88983917 -3.79632759 -0.59053195 11.50822449 -3.40997791 -0.6408155 16.24032593 -4.17760754 -0.5405798
		 18.59082031 -4.5588541 -0.49014452 18.5841217 -4.55299473 -0.41985458 20.90309906 -4.93400097 -0.44195747
		 13.27559662 -3.78347111 -0.90899986 10.92832947 -3.40997791 -0.93592954 10.97252655 -3.40997791 -0.91762358
		 13.3212738 -3.78722668 -0.89958394 13.36644745 -3.79091644 -0.89021057 11.016716003 -3.40997791 -0.89931786
		 15.69361115 -4.16823101 -0.8812812 18.039520264 -4.54851437 -0.87209702;
	setAttr ".vt[830:995]" 18.089515686 -4.55299091 -0.86271542 13.40820313 -3.78333473 -0.85372382
		 11.10511017 -3.40997791 -0.86270612 11.14929962 -3.40997791 -0.84440041 13.45386505 -3.78715706 -0.84448719
		 13.49901581 -3.79091358 -0.83529049 11.1934967 -3.40997791 -0.82609445 15.78200531 -4.16816235 -0.84449023
		 18.083656311 -4.54842091 -0.85357654 18.13370514 -4.55299091 -0.84440958 13.54080963 -3.78319168 -0.7984345
		 11.28187561 -3.40997791 -0.78948295 11.32608032 -3.40997791 -0.77117699 13.58646393 -3.78708649 -0.78938371
		 13.63157654 -3.79090786 -0.78036988 11.37026978 -3.40997791 -0.75287127 15.8704071 -4.16809177 -0.80769265
		 18.12779236 -4.54832172 -0.83504683 18.17790222 -4.55299091 -0.82610375 13.79121399 -3.79440403 -0.72309643
		 11.45866394 -3.40997791 -0.7162596 16.11704254 -4.17771626 -0.72989303 18.44289398 -4.56095028 -0.73648483
		 18.35678864 -4.55872059 -0.76650774 18.26829529 -4.55602169 -0.79633456 20.52813721 -4.93399525 -0.81695676
		 27.68932343 -6.077015877 -0.74375439 27.77771759 -6.077017784 -0.70714772 30.017478943 -6.45802402 -0.74375933
		 27.60095978 -6.077015877 -0.78036094 27.5125885 -6.077015877 -0.81696743 29.84074402 -6.45802402 -0.81697088
		 25.18443298 -5.69600773 -0.81696403 22.85626984 -5.31499958 -0.81696045 22.76790619 -5.31500149 -0.85356838
		 27.15911865 -6.077017784 -0.96339339 29.48726654 -6.45802402 -0.96339405 27.070747375 -6.077017784 -0.99999988
		 24.83096313 -5.69600964 -0.96339267 22.50279999 -5.3150053 -0.96339202 22.59117126 -5.3150034 -0.92678404
		 22.67953491 -5.3150034 -0.8901763 12.56850433 -3.78347492 -0.79810715 10.22122192 -3.40997791 -0.77117699
		 10.26541138 -3.40997791 -0.78948295 12.61418915 -3.78722858 -0.80752307 12.65936279 -3.79091835 -0.81689632
		 10.30960846 -3.40997791 -0.80778873 14.98654175 -4.16823673 -0.82582617 17.33247375 -4.54852581 -0.83501071
		 17.38246918 -4.55300045 -0.84439218 12.70111084 -3.78333664 -0.85338306 10.397995 -3.40997791 -0.84440041
		 10.44219208 -3.40997791 -0.86270612 12.7467804 -3.78716087 -0.86261976 12.79192352 -3.79091549 -0.87181658
		 10.48638153 -3.40997791 -0.88101208 15.074935913 -4.16816807 -0.86261702 17.37661743 -4.54843235 -0.85353112
		 17.42665863 -4.55300045 -0.86269802 12.83372498 -3.7831955 -0.90867233 10.5747757 -3.40997791 -0.91762358
		 10.61896515 -3.40997791 -0.93592954 12.87937927 -3.7870903 -0.91772312 12.92449188 -3.79091167 -0.92673707
		 10.66316223 -3.40997791 -0.95423532 15.16333771 -4.1680994 -0.89941454 17.42074585 -4.54833126 -0.87206072
		 17.47086334 -4.55300045 -0.88100386 13.084129333 -3.79440594 -0.98401463 10.7515564 -3.40997791 -0.99084699
		 15.40998077 -4.17771816 -0.9772225 17.73584747 -4.56095219 -0.97063524 17.64974976 -4.55872631 -0.9406082
		 17.56124878 -4.55602932 -0.91077709 19.82111359 -4.93400669 -0.89015669 26.98236084 -6.077021599 -0.9633835
		 29.31049347 -6.45802402 -0.9633823 26.89394379 -6.077021599 -0.92676663 26.80553436 -6.077019691 -0.89014977
		 29.13366699 -6.45802402 -0.89014739 24.47740173 -5.69601536 -0.89015222 22.1492691 -5.31501293 -0.8901546
		 22.06085968 -5.31501102 -0.85353923 26.45189667 -6.077019691 -0.74368256 28.7800293 -6.45802402 -0.74367732
		 26.36347961 -6.077019691 -0.70706582 24.12376404 -5.69601154 -0.74368769 21.79563141 -5.31500721 -0.74369293
		 21.8840332 -5.31500912 -0.78030837 21.97245026 -5.31501102 -0.8169238 12.14649963 -3.78339481 -0.21991722
		 9.83773041 -3.40997791 -0.1546796 9.8560257 -3.40997791 -0.19887376 12.17905426 -3.78718948 -0.24254125
		 12.20775604 -3.79035759 -0.26508558 9.87433624 -3.40997791 -0.24306795 14.52548218 -4.16819477 -0.28673577
		 16.85372925 -4.54773045 -0.30872461 16.89530945 -4.55299473 -0.33145732 12.20142365 -3.78333664 -0.35338622
		 9.91094208 -3.40997791 -0.33145627 9.92925262 -3.40997791 -0.37565044 12.23397064 -3.78715897 -0.37556702
		 12.26255798 -3.79033279 -0.39774361 9.94755554 -3.40997791 -0.41984463 14.56210327 -4.16816425 -0.37556735
		 16.87198639 -4.54768085 -0.3535302 16.91362 -4.55299473 -0.37565148 12.25636292 -3.78328037 -0.48686901
		 9.98416901 -3.40997791 -0.50823295 10.0024719238 -3.40997791 -0.55242717 12.28890228 -3.78713131 -0.50859976
		 12.31734467 -3.79030895 -0.53040749 10.020782471 -3.40997791 -0.59662127 14.59871674 -4.16813564 -0.46440589
		 16.89025116 -4.54763126 -0.39834738 16.93192291 -4.55299664 -0.41984561 12.41200256 -3.79646683 -0.66804612
		 10.057388306 -3.40997887 -0.6850096 14.73456573 -4.17767239 -0.65166146 17.057128906 -4.55884457 -0.63577098
		 17.026069641 -4.55867672 -0.56327343 16.98631287 -4.55706501 -0.49106511 19.26921082 -4.93400097 -0.44193691
		 26.32685852 -6.077019691 -0.61868793 28.65498352 -6.45802402 -0.61868328 26.29022217 -6.077019691 -0.53030992
		 26.25359344 -6.077017784 -0.4419319 28.58172607 -6.45802402 -0.44193 23.92546844 -5.69601154 -0.44193363
		 21.59733582 -5.31500721 -0.44193536 21.56071472 -5.3150053 -0.3535544 26.10707855 -6.077017784 -0.08841937
		 28.43519592 -6.45802402 -0.088423446 26.070449829 -6.077017784 -4.0955674e-05 23.77896118 -5.69601154 -0.088415042
		 21.45084381 -5.3150053 -0.088410705 21.48747253 -5.3150053 -0.17679203 21.524086 -5.3150053 -0.26517335
		 12.25635529 -3.78328037 0.48685795 10.0024719238 -3.40997982 0.55242717 9.98416901 -3.40997982 0.50823295
		 12.27058411 -3.78713131 0.46439445 12.27458191 -3.78930473 0.44198868 9.96585846 -3.40997982 0.46403879
		 14.5803833 -4.16813564 0.42018941 16.89020538 -4.54762936 0.39831394 16.91358185 -4.55299473 0.37561786
		 12.20141602 -3.78333664 0.35337529 9.92925262 -3.40997982 0.37565044 9.91094208 -3.40997887 0.33145627
		 12.21565247 -3.78715897 0.33136177 12.21948242 -3.78928566 0.30920863 9.89263916 -3.40997887 0.28726208
		 14.54376984 -4.16816425 0.33135089 16.87194061 -4.54767895 0.35349673 16.89527893 -4.55299282 0.3314237
		 12.146492 -3.78339481 0.21990623 9.8560257 -3.40997791 0.19887376 9.83773041 -3.40997791 0.1546796
		 12.16073608 -3.78718758 0.19833599 12.16437531 -3.7892704 0.17642495 9.81941223 -3.40997791 0.11048542
		 14.50714874 -4.16819286 0.24251929 16.8536911 -4.54773045 0.30869117;
	setAttr ".vt[996:1161]" 16.87696838 -4.55299282 0.28722954 12.16278076 -3.79827976 0.03965554
		 9.7828064 -3.40997791 0.022097085 14.48741913 -4.17760754 0.056035209 16.81206512 -4.55696583 0.071931496
		 16.85214233 -4.55861187 0.1444127 16.88365936 -4.5588522 0.21694522 19.19593048 -4.93399906 0.26512659
		 26.10706329 -6.077017784 0.088338763 28.43518066 -6.45802402 0.088331535 26.14366913 -6.077017784 0.17671829
		 26.18028259 -6.077017784 0.26509786 28.50839996 -6.45802402 0.26508826 23.85215759 -5.69600964 0.26510751
		 21.52404785 -5.3150053 0.26511711 21.56066132 -5.3150053 0.35349911 26.32673645 -6.077017784 0.61861616
		 28.65484619 -6.45802402 0.61860168 26.36334229 -6.077019691 0.70699584 23.99861145 -5.69600964 0.61863077
		 21.67049408 -5.3150053 0.61864543 21.63388062 -5.3150053 0.53026319 21.59726715 -5.3150053 0.44188106
		 12.83370209 -3.7831974 0.90866333 10.61896515 -3.40997982 0.93592954 10.5747757 -3.40997982 0.91762358
		 12.8351593 -3.78709221 0.89940816 12.81804657 -3.7879343 0.89018404 10.53058624 -3.40997982 0.89931786
		 15.11910248 -4.1680994 0.88109064 17.42068481 -4.54833317 0.87203348 17.42660522 -4.55300236 0.8626706
		 12.70109558 -3.7833395 0.8533743 10.44219208 -3.40997982 0.86270612 10.397995 -3.40997982 0.84440041
		 12.70256042 -3.78716278 0.84430486 12.68535614 -3.78797436 0.83512157 10.35380554 -3.40997982 0.82609445
		 15.030700684 -4.16816998 0.84429312 17.37654877 -4.54843235 0.85350382 17.38240814 -4.55300236 0.84436488
		 12.56848907 -3.78347683 0.79809833 10.26541138 -3.40997982 0.78948295 10.22122192 -3.40997982 0.77117699
		 12.56996918 -3.7872324 0.78920817 12.55268097 -3.78801346 0.78006279 10.17702484 -3.40997982 0.75287127
		 14.94230652 -4.16823864 0.80750221 17.33241272 -4.54852772 0.83498341 17.33821106 -4.55300236 0.82605892
		 12.48538208 -3.79927444 0.72369134 10.088645935 -3.40997982 0.7162596 14.8133316 -4.17756939 0.73046637
		 17.14125824 -4.55593967 0.73704702 17.2298584 -4.55857563 0.76706612 17.31615448 -4.5607748 0.79724503
		 19.6442337 -4.93400669 0.81689024 26.45175934 -6.077019691 0.74361426 28.77986145 -6.45802402 0.74359918
		 26.54016113 -6.077019691 0.78023255 26.62857819 -6.077019691 0.81685084 28.95668793 -6.45802402 0.81683767
		 24.30046082 -5.69601345 0.81686401 21.97234344 -5.31501102 0.81687713 22.060752869 -5.31501293 0.85349369
		 26.98220825 -6.077021599 0.96332395 29.31032562 -6.45802402 0.96331441 27.070602417 -6.077019691 0.99994195
		 24.65407562 -5.69601345 0.96333313 22.32594299 -5.31500912 0.96334237 22.23755646 -5.31501102 0.92672622
		 22.14916992 -5.31501293 0.89011014 13.54080963 -3.78319168 0.79842836 11.32608032 -3.40997791 0.77117717
		 11.28187561 -3.40997791 0.78948295 13.54227448 -3.78708839 0.80768329 13.52515411 -3.78793049 0.81690741
		 11.23768616 -3.40997791 0.80778873 15.82621002 -4.16809368 0.82598591 18.12779236 -4.54832554 0.83502787
		 18.13370514 -4.55299282 0.84439051 13.40820313 -3.78333473 0.85371763 11.14929962 -3.40997791 0.84440041
		 11.10511017 -3.40997791 0.86270612 13.4096756 -3.78715897 0.86278677 13.39246368 -3.78797054 0.87197006
		 11.060905457 -3.40997791 0.88101208 15.73780823 -4.16816425 0.86278343 18.083656311 -4.54842472 0.85355741
		 18.089508057 -4.55299282 0.86269629 13.27559662 -3.78347301 0.90899354 10.97252655 -3.40997791 0.91762358
		 10.92832947 -3.40997791 0.93592954 13.27707672 -3.78722668 0.9178834 13.25978088 -3.78801155 0.92702878
		 10.88413239 -3.40997791 0.95423532 15.64940643 -4.16823292 0.89957428 18.039520264 -4.54851818 0.87207788
		 18.045318604 -4.55299473 0.88100219 13.19248962 -3.79927063 0.98339325 10.79574585 -3.40997887 0.99084699
		 15.52045441 -4.17756557 0.97659647 17.84841156 -4.55593586 0.96999377 17.9369812 -4.55856991 0.9399814
		 18.023254395 -4.56076717 0.90980935 20.35135651 -4.93399906 0.89014673 27.15898895 -6.077017784 0.96333891
		 29.48712158 -6.45802402 0.963332 27.24737549 -6.077017784 0.92673612 27.33576965 -6.077015877 0.89013314
		 29.66390228 -6.45802212 0.89012873 25.0076293945 -5.69600964 0.89013779 22.67949677 -5.3150053 0.8901422
		 22.7678833 -5.3150053 0.85353684 27.68932343 -6.077015877 0.74372166 30.017456055 -6.45802021 0.74372208
		 27.77771759 -6.077015877 0.70711875 25.36118317 -5.69600773 0.74372125 23.033050537 -5.3150034 0.74372083
		 22.94465637 -5.3150034 0.78032613 22.85626984 -5.3150034 0.81693149 13.96347809 -3.78327847 0.22023934
		 11.7095871 -3.40997791 0.1546796 11.69127655 -3.40997791 0.19887379 13.97770691 -3.78713131 0.24270281
		 13.98169708 -3.78930283 0.26510853 11.67297363 -3.40997791 0.24306795 16.28752136 -4.16813564 0.2868982
		 18.59735107 -4.54762745 0.30876407 18.62072754 -4.55299282 0.33146003 13.90853119 -3.78333473 0.3537221
		 11.63635254 -3.40997791 0.3314563 11.61804962 -3.40997791 0.37565044 13.92277527 -3.78715706 0.37573543
		 13.92658997 -3.78928566 0.39788848 11.5997467 -3.40997791 0.41984466 16.25090027 -4.16816235 0.37573668
		 18.5790863 -4.54767704 0.35358128 18.60242462 -4.55299282 0.37565422 13.85359955 -3.78339481 0.48719117
		 11.56314087 -3.40997791 0.50823295 11.54483032 -3.40997791 0.55242717 13.86785889 -3.78718758 0.50876129
		 13.87149048 -3.78926945 0.53067231 11.5265274 -3.40997791 0.59662127 16.21427917 -4.16819286 0.46456832
		 18.56083679 -4.54772854 0.39838681 18.58410645 -4.55299282 0.41984835 13.86990356 -3.79827976 0.6674456
		 11.48991394 -3.40997791 0.68500972 16.19457245 -4.17760754 0.65106052 18.5192337 -4.55696392 0.63515878
		 18.55930328 -4.55861187 0.56267339 18.59081268 -4.55885029 0.4901368 20.90309143 -4.93399715 0.44194719
		 27.81430054 -6.077015877 0.61872911 30.1424408 -6.45802021 0.61873043 27.85089874 -6.077015877 0.53033942
		 27.88748169 -6.077015877 0.4419497 30.21561432 -6.45802021 0.44195032 25.55935669 -5.69600773 0.44194889
		 23.23123169 -5.3150034 0.44194809 23.26782227 -5.3150034 0.35355884 28.033851624 -6.077015877 0.088390231
		 30.36197662 -6.45802402 0.08839006 28.070449829 -6.077017784 -5.7407759e-12;
	setAttr ".vt[1162:1327]" 25.70574188 -5.69600964 0.088390082 23.37761688 -5.3150053 0.088389918
		 23.34101868 -5.3150034 0.17677966 23.30442047 -5.3150034 0.26516938 13.85360718 -3.78339481 -0.4871932
		 11.54483032 -3.40997791 -0.55242717 11.56314087 -3.40997791 -0.50823295 13.8861618 -3.78718948 -0.46456912
		 13.91487122 -3.79035759 -0.44202474 11.58144379 -3.40997791 -0.46403879 16.23258972 -4.16819477 -0.42037827
		 18.56084442 -4.54773045 -0.39839295 18.60242462 -4.55299473 -0.37566039 13.90853119 -3.78333664 -0.35372409
		 11.61804962 -3.40997791 -0.37565044 11.63635254 -3.40997791 -0.33145627 13.94107819 -3.78715897 -0.3315433
		 13.96966553 -3.79033279 -0.30936679 11.65466309 -3.40997791 -0.28726208 16.26920319 -4.16816425 -0.3315466
		 18.5790863 -4.54767895 -0.35358745 18.62072754 -4.55299473 -0.3314662 13.96347809 -3.78327847 -0.22024132
		 11.69127655 -3.40997791 -0.19887376 11.7095871 -3.40997791 -0.1546796 13.99600983 -3.78713131 -0.19851065
		 14.024452209 -3.79030704 -0.17670277 11.72789001 -3.40997791 -0.11048542 16.30583191 -4.16813564 -0.24270813
		 18.5973587 -4.54762936 -0.30877024 18.63903809 -4.55299473 -0.28727204 14.11910248 -3.79646683 -0.039057292
		 11.76449585 -3.40997791 -0.022097085 16.44164276 -4.17767048 -0.05543853 18.7641983 -4.55884266 -0.071325444
		 18.7331543 -4.55867481 -0.14383017 18.69340515 -4.55706501 -0.21604574 20.9763031 -4.93400097 -0.26517496
		 28.033859253 -6.077017784 -0.088393793 30.36197662 -6.45802402 -0.088394195 27.99726105 -6.077017784 -0.17678732
		 27.9606781 -6.077017784 -0.26518086 30.28879547 -6.45802402 -0.26518258 25.6325531 -5.69601154 -0.26517898
		 23.30442047 -5.3150053 -0.2651771 23.26782227 -5.3150053 -0.35356915 27.81430817 -6.077019691 -0.61875451
		 30.1424408 -6.45802402 -0.61875939 25.48617554 -5.69601154 -0.6187495 23.15803528 -5.3150053 -0.61874449
		 23.19463348 -5.3150053 -0.53035271 23.23123169 -5.3150053 -0.44196102 19.056274414 -4.7457056 -0.95004851
		 19.1000061 -4.74628162 -0.93339318 20.21883392 -4.93399906 -0.94508708 19.011550903 -4.74496555 -0.96670437
		 17.89272308 -4.55724812 -0.95501053 18.34915161 -4.74571133 -0.75704581 18.39290619 -4.74628925 -0.77370393
		 19.51170349 -4.93400478 -0.76200551 18.30441284 -4.74496937 -0.74038738 17.18561554 -4.55725574 -0.7520858
		 17.98961639 -4.74639034 -0.12040532 18.0057601929 -4.74630642 -0.16061825 19.14104462 -4.93400097 -0.13259764
		 17.9673996 -4.74548435 -0.080186382 16.83211517 -4.5577898 -0.10820696 18.18682098 -4.74703693 0.58697999
		 18.16589355 -4.74633884 0.54675907 19.32407379 -4.93400097 0.57446831 18.19972992 -4.74642277 0.62719989
		 17.041549683 -4.55876064 0.59949064 18.82424927 -4.74709797 0.95032144 18.77954865 -4.74636555 0.93365914
		 19.95361328 -4.93400478 0.94504392 18.86679077 -4.74747753 0.96698052 17.69273376 -4.55983829 0.95559573
		 19.53135681 -4.74709225 0.75675088 19.48666382 -4.74635792 0.77340937 20.6607132 -4.93399906 0.76202357
		 19.57390594 -4.74747372 0.74009544 18.39984131 -4.55983448 0.75148129 19.89395142 -4.74703503 0.12007939
		 19.87302399 -4.74633694 0.16030312 21.031204224 -4.93399906 0.13258438 19.90684509 -4.74642086 0.079856738
		 18.74867249 -4.55875874 0.10757546 19.69677734 -4.74639225 -0.58673215 19.71290588 -4.74630642 -0.54651558
		 20.84820557 -4.93400097 -0.57454383 19.67456818 -4.74548435 -0.62695444 18.53926849 -4.5577898 -0.59892619
		 14.45409393 -3.97979641 -0.91776288 15.65015411 -4.17010784 -0.90403193 14.45368195 -3.98148251 -0.92220467
		 13.25812531 -3.78949642 -0.93148386 14.45459747 -3.97811985 -0.913311 14.56461334 -3.97976971 -0.87191808
		 15.73854065 -4.17007351 -0.86733067 14.56416321 -3.98148251 -0.87644011 13.3907547 -3.78947544 -0.87649572
		 14.56513977 -3.97806454 -0.86738622 14.67512512 -3.979743 -0.82607061 15.82694244 -4.17003918 -0.83062601
		 14.6746521 -3.98148251 -0.8306756 13.52339172 -3.78945446 -0.82150567 14.67568207 -3.97801113 -0.82145607
		 14.83068085 -3.9850893 -0.77516842 15.96131134 -4.17484951 -0.78714842 14.78513336 -3.98148251 -0.78491116
		 13.69950867 -3.79523373 -0.76318008 14.87568665 -3.98860073 -0.7654174 21.82479095 -5.12450123 -0.7620458
		 21.78059387 -5.12449932 -0.78035009 20.66072083 -4.93399906 -0.76204348 21.86898041 -5.12450123 -0.74374163
		 22.98885345 -5.3150034 -0.7620483 26.21595764 -5.88651371 -0.87187594 25.051872253 -5.69600773 -0.87187475
		 26.26013947 -5.88651371 -0.85357255 27.3800354 -6.077015877 -0.87187713 26.17176819 -5.88651371 -0.89017934
		 13.74701691 -3.97980213 -0.78934431 14.94308472 -4.17011547 -0.80307531 13.74659729 -3.98148823 -0.78490227
		 12.55104065 -3.78950024 -0.7756232 13.74752045 -3.97812557 -0.79379618 13.85752869 -3.97977543 -0.83518898
		 15.031478882 -4.17008114 -0.83977664 13.85708618 -3.98148823 -0.8306669 12.68367004 -3.78947926 -0.83061123
		 13.85806274 -3.97807026 -0.83972096 13.96805573 -3.97974682 -0.88103634 15.11987305 -4.1700449 -0.87648118
		 13.96757507 -3.98148823 -0.87643147 12.81630707 -3.78945827 -0.88560128 13.96861267 -3.97801495 -0.88565099
		 14.12359619 -3.98509312 -0.93193972 15.25424194 -4.17485714 -0.9199602 14.078056335 -3.98148823 -0.92219597
		 12.99242401 -3.79523754 -0.94392747 14.16860962 -3.98860455 -0.94169182 21.11776733 -5.12450695 -0.94507778
		 21.073577881 -5.12450695 -0.92677039 19.9536972 -4.93400288 -0.94507837 21.16197205 -5.12450504 -0.96338511
		 22.28185272 -5.31500912 -0.94507724 25.50885773 -5.88651752 -0.83522618 24.3447876 -5.69601345 -0.83522809
		 25.55306244 -5.88651752 -0.85353452 26.67292023 -6.077019691 -0.83522457 25.46464539 -5.88651752 -0.81691808
		 13.34076691 -3.98028278 -0.19865741 14.51430511 -4.17009258 -0.23175658 13.34363556 -3.98148441 -0.18782575
		 12.16511536 -3.7901268 -0.16555801 13.33578491 -3.97873211 -0.20948884 13.38661194 -3.9802866 -0.30928382
		 14.55091095 -4.17007732 -0.32036656 13.38939667 -3.98148441 -0.29831117 12.22012329 -3.79013443 -0.29824114
		 13.38163757 -3.97872639 -0.32029653 13.43247986 -3.98029041 -0.41990903 14.58753204 -4.17006207 -0.40897998
		 13.43515778 -3.98148441 -0.40879661 12.27513885 -3.79014397 -0.43092239;
	setAttr ".vt[1328:1493]" 13.42750549 -3.97872066 -0.43110573 13.51228333 -3.98503971 -0.54257315
		 14.66001892 -4.17482853 -0.5136593 13.48092651 -3.98148632 -0.51928198 12.36092377 -3.79464626 -0.57161599
		 13.54001617 -3.98798847 -0.56599343 20.48821259 -5.12450314 -0.57450831 20.46989441 -5.12450314 -0.53031754
		 19.32414246 -4.93400097 -0.5745101 20.50652313 -5.12450314 -0.61869907 21.65227509 -5.3150053 -0.57450652
		 25.034591675 -5.88651562 -0.30936453 23.87052917 -5.69601154 -0.30936429 25.052909851 -5.88651562 -0.35355386
		 26.19865417 -6.077017784 -0.30936471 25.016273499 -5.88651562 -0.2651751 13.47281647 -3.98090649 0.50827736
		 14.61495972 -4.17006207 0.47524887 13.48091125 -3.98148632 0.51926517 12.32395935 -3.79064655 0.54140633
		 13.45801544 -3.9792223 0.49739 13.42714691 -3.98092556 0.39772275 14.57834625 -4.17007732 0.38663545
		 13.43515015 -3.98148632 0.40877977 12.269104 -3.7906599 0.40878305 13.41230774 -3.979249 0.38663876
		 13.38146973 -3.98094463 0.28717282 14.54174042 -4.17009258 0.29802549 13.38938141 -3.98148441 0.29829437
		 12.21424866 -3.79067135 0.27616283 13.36660004 -3.9792757 0.27589393 13.35414886 -3.98476887 0.1647846
		 14.52729034 -4.17479801 0.19373238 13.3436203 -3.98148441 0.18780895 12.17427826 -3.7936554 0.13561891
		 13.3579483 -3.98696804 0.14154235 20.3050766 -5.12450314 0.13254778 20.32337952 -5.12450123 0.17673913
		 19.1410141 -4.93399906 0.13255158 20.28677368 -5.12450314 0.088356391 21.4691391 -5.3150053 0.13254392
		 25.071136475 -5.88651371 0.39767292 23.9070816 -5.69600964 0.39767867 25.052833557 -5.88651371 0.35348284
		 26.23519897 -6.077017784 0.39766723 25.089447021 -5.88651371 0.44186303 14.066444397 -3.98141384 0.91757923
		 15.18612671 -4.17004681 0.90392178 14.078033447 -3.98149014 0.92218232 12.93578339 -3.79094887 0.93132275
		 14.043884277 -3.97950649 0.9130621 13.95598602 -3.98141766 0.87181163 15.097732544 -4.17008305 0.86721712
		 13.96754456 -3.98149014 0.87641788 12.80321503 -3.79095268 0.87640381 13.93340302 -3.97954464 0.86720318
		 13.84552002 -3.98142147 0.82604402 15.0093383789 -4.17011547 0.83051592 13.85706329 -3.98149014 0.83065325
		 12.67064667 -3.79095459 0.82148504 13.82292175 -3.97958088 0.82134771 13.73871613 -3.98396206 0.77537727
		 14.933815 -4.17478275 0.78742343 13.7465744 -3.98149014 0.78488863 12.54044342 -3.79263783 0.76329207
		 13.72768402 -3.98593044 0.76582682 20.67568207 -5.12450695 0.76195943 20.71987915 -5.12450695 0.7802676
		 19.51161957 -4.93400478 0.76196682 20.63146973 -5.12450504 0.74365151 21.83972931 -5.31500912 0.76195234
		 25.59713745 -5.88651943 0.87178421 24.43307495 -5.69601536 0.87179011 25.55292511 -5.88651752 0.85347527
		 26.76119232 -6.077021599 0.87177831 25.64133453 -5.88651943 0.89009315 14.77355957 -3.98140812 0.78950441
		 15.89323425 -4.17004108 0.80315471 14.78513336 -3.98148441 0.78490156 13.64289856 -3.79094505 0.7757687
		 14.75099182 -3.97950077 0.79402184 14.66308594 -3.98141384 0.83527237 15.80483246 -4.17007732 0.83985925
		 14.67464447 -3.98148441 0.83066601 13.51032257 -3.79094887 0.83068764 14.64051056 -3.97953892 0.83988082
		 14.55261993 -3.98141766 0.88103962 15.71643829 -4.17010975 0.87656039 14.56416321 -3.98148441 0.87643063
		 13.37774658 -3.79095078 0.88560641 14.5300293 -3.97957516 0.88573629 14.44581604 -3.98395824 0.93170488
		 15.64091492 -4.17477703 0.9196505 14.45367432 -3.98148632 0.9221952 13.24755096 -3.79263592 0.94379824
		 14.43479156 -3.98592663 0.94125354 21.38284302 -5.12450314 0.94505352 21.42703247 -5.12450314 0.92675048
		 20.21877289 -4.93400097 0.94505674 21.33865356 -5.12450314 0.96335644 22.54691315 -5.3150053 0.9450503
		 26.30428314 -5.88651371 0.83523023 25.14021301 -5.69600964 0.83523154 26.26008606 -5.88651371 0.85353196
		 27.46834564 -6.077015877 0.83522886 26.3484726 -5.88651371 0.81692845 15.1799469 -3.98090458 0.1988149
		 16.32209778 -4.17006207 0.2318387 15.18803406 -3.98148441 0.18782711 14.031082153 -3.79064465 0.16569076
		 15.16513824 -3.9792223 0.20970236 15.13426971 -3.98092556 0.30936953 16.28548431 -4.17007542 0.32045209
		 15.14227295 -3.98148441 0.29831254 13.97622681 -3.790658 0.29831403 15.11943054 -3.97924709 0.32045361
		 15.088600159 -3.98094463 0.41991955 16.24886322 -4.17009068 0.40906212 15.096504211 -3.98148441 0.40879798
		 13.92137146 -3.79067135 0.43093431 15.073730469 -3.9792757 0.43119848 15.061279297 -3.98476887 0.54230875
		 16.23442841 -4.17479801 0.51335645 15.050743103 -3.98148441 0.51928335 13.88139343 -3.7936554 0.57147878
		 15.065078735 -3.98696613 0.565552 22.012268066 -5.12450123 0.57453114 22.030570984 -5.12449932 0.53033662
		 20.84819794 -4.93399906 0.57453042 21.99397278 -5.12450123 0.6187256 23.17633057 -5.3150034 0.57453179
		 26.77830505 -5.88651371 0.30936471 25.61424255 -5.69600964 0.30936453 26.76000977 -5.88651371 0.35355955
		 27.94237518 -6.077015877 0.30936491 26.79660034 -5.88651371 0.26516986 15.047866821 -3.98028088 -0.50845486
		 16.22141266 -4.17009258 -0.47535747 15.050743103 -3.98148441 -0.51928645 13.8722229 -3.7901268 -0.54155236
		 15.042892456 -3.97873211 -0.49762335 15.093727112 -3.9802866 -0.39782834 16.25802612 -4.17007732 -0.38674748
		 15.096511841 -3.98148441 -0.40880105 13.92723846 -3.79013443 -0.40886924 15.088752747 -3.97872639 -0.38681567
		 15.13957977 -3.98029041 -0.28720313 16.29464722 -4.17006207 -0.29813406 15.14227295 -3.98148441 -0.29831558
		 13.9822464 -3.79014397 -0.27618799 15.13461304 -3.97872066 -0.27600643 15.21938324 -3.9850359 -0.16453727
		 16.36712646 -4.17482853 -0.19345239 15.18803406 -3.98148441 -0.18783019 14.068031311 -3.79464436 -0.13549322
		 15.24711609 -3.98798847 -0.1411154 22.19525909 -5.12450314 -0.13258804 22.17696381 -5.12450314 -0.17678402
		 21.031204224 -4.93400097 -0.13258748 22.21356964 -5.12450314 -0.088392064 23.35932159 -5.3150053 -0.13258861
		 26.74172211 -5.88651562 -0.39776954 25.57765961 -5.69601154 -0.39776808 26.7600174 -5.88651562 -0.35357296
		 27.90577698 -6.077017784 -0.397771 26.72341919 -5.88651562 -0.44196612;
	setAttr ".vt[1494:1659]" 14.31143951 -3.98499012 -0.99000406 15.47521973 -4.17477703 -0.98830605
		 14.2658844 -3.98148441 -0.99999988 13.14715576 -3.79512501 -0.99170065 14.35648346 -3.98841381 -0.98000681
		 12.028953552 -3.6028986 -0.96833158 12.016136169 -3.60326672 -0.97457665 10.82888794 -3.40997791 -0.97711748
		 12.039291382 -3.60213184 -0.96210921 13.22652435 -3.79541969 -0.95956832 16.83921814 -4.36608028 -0.90752381
		 15.64092255 -4.17477322 -0.91966385 16.84810638 -4.36248875 -0.89474905 18.034286499 -4.55687809 -0.8954258
		 16.82710266 -4.36916065 -0.9203406 13.60431671 -3.98499393 -0.7170949 14.76809692 -4.17478085 -0.71879053
		 13.55874634 -3.98148823 -0.70709789 12.44004059 -3.79512882 -0.71540117 13.64939117 -3.98841953 -0.72709364
		 11.32186127 -3.60290241 -0.73877418 11.30902863 -3.60327053 -0.73252869 10.12178802 -3.40997887 -0.72998905
		 11.33219147 -3.60213375 -0.74499691 12.51944733 -3.79542732 -0.74753654 16.132164 -4.36609173 -0.799582
		 14.93385315 -4.17478085 -0.78744197 16.14105225 -4.36249828 -0.81235838 17.32723999 -4.55688953 -0.81167978
		 16.12004089 -4.3691721 -0.78676355 13.29721069 -3.98500156 -0.023924278 14.45865631 -4.17479992 -0.028035209
		 13.26582336 -3.98148441 -8.7762155e-06 12.13227844 -3.794631 -0.019836685 13.32510376 -3.9879427 -0.047863118
		 10.99022675 -3.60389614 -0.076393925 10.98021698 -3.6032896 -0.061172061 9.79653168 -3.40997791 -0.05524271
		 10.99191284 -3.60315323 -0.091435954 12.17559814 -3.79646492 -0.097365305 15.70739746 -4.36593723 -0.22306114
		 14.52732086 -4.17479992 -0.19375509 15.69921875 -4.36249256 -0.25411737 16.88035583 -4.55592442 -0.25212142
		 15.70844269 -4.36822987 -0.19175915 13.56932831 -3.98482609 0.68347532 14.72864532 -4.17483044 0.67935783
		 13.55871582 -3.98148823 0.70708293 12.40332031 -3.79372692 0.6875633 13.57325745 -3.98707104 0.6598382
		 11.22268677 -3.60434914 0.63094252 11.2227478 -3.60332298 0.64623594 10.043663025 -3.40997982 0.65186399
		 11.21515656 -3.6041441 0.61580443 12.39424133 -3.79748631 0.61017644 15.81185913 -4.36531734 0.48444551
		 14.65999603 -4.17482853 0.51363719 15.78156281 -4.36249065 0.45296305 16.95907593 -4.55502796 0.45542213
		 15.83750916 -4.36736774 0.51609623 14.25808716 -3.98404598 0.99029815 15.4199295 -4.17485523 0.98859429
		 14.26585388 -3.98148823 0.99998754 13.092948914 -3.792696 0.99199891 14.24702454 -3.98606205 0.98060572
		 11.88440704 -3.6041832 0.96854675 11.89458466 -3.60334587 0.97487742 10.71840668 -3.40997982 0.97711748
		 11.87252808 -3.60473537 0.96223015 13.048706055 -3.79810143 0.95998996 16.38548279 -4.36475277 0.90789682
		 15.25419617 -4.17485714 0.91994214 16.33987427 -4.36249828 0.89471173 17.51599121 -4.55451679 0.89586341
		 16.43032074 -4.36687565 0.92109376 14.96520233 -3.98404217 0.71679699 16.12703705 -4.17485142 0.71849686
		 14.97296143 -3.98148441 0.70710933 13.80005646 -3.79269409 0.71510011 14.95413208 -3.98606014 0.72648764
		 12.59151459 -3.60418034 0.73855376 12.60168457 -3.60334396 0.73222369 11.42551422 -3.40997791 0.72998905
		 12.57963562 -3.60473251 0.74486971 13.7558136 -3.79809761 0.74710441 17.092590332 -4.36474514 0.79917443
		 15.96131134 -4.17485142 0.7871362 17.046981812 -4.36249065 0.81235701 18.22309875 -4.55450726 0.81120092
		 17.13742065 -4.36686993 0.78598011 15.27644348 -3.98482418 0.023608774 16.4357605 -4.17482853 0.027718725
		 15.26582336 -3.98148441 -1.2316807e-12 14.11043549 -3.79372501 0.019528359 15.28036499 -3.98706913 0.047247082
		 12.92979431 -3.60434723 0.076158188 12.92985535 -3.60332108 0.060864352 11.75077057 -3.40997791 0.055242717
		 12.92227173 -3.60414219 0.091296591 14.10135651 -3.7974844 0.096918218 17.51900482 -4.36531734 0.22263522
		 16.36712646 -4.17482853 0.19344874 17.48870087 -4.36249065 0.2541196 18.66622162 -4.55502796 0.25165343
		 17.54463959 -4.36736584 0.19098258 15.0043411255 -3.98499966 -0.68319869 16.165802 -4.17479992 -0.67909306
		 14.97296143 -3.98148441 -0.70711547 13.83940125 -3.794631 -0.68728089 15.032241821 -3.9879427 -0.65925848
		 12.69734192 -3.60389614 -0.6307162 12.68732452 -3.6032896 -0.64593863 11.50363922 -3.40997791 -0.65186399
		 12.69902802 -3.60315323 -0.61567366 13.88271332 -3.79646492 -0.60974836 17.41452026 -4.36593533 -0.48405749
		 16.23442841 -4.17479992 -0.51336116 17.40633392 -4.36249256 -0.45299852 18.58746338 -4.55592442 -0.45499954
		 17.41557312 -4.36822987 -0.51536208 12.093315125 -3.59711552 -0.92703772 12.091651917 -3.59860229 -0.93148613
		 10.9172821 -3.40997791 -0.94050598 12.10195923 -3.59672451 -0.92246467 13.27633667 -3.78534794 -0.91344488
		 12.18029022 -3.60041046 -0.89935231 12.15794373 -3.59860229 -0.90402728 11.0056686401 -3.40997791 -0.90389425
		 12.19158173 -3.60044765 -0.89476418 13.34385681 -3.78907204 -0.89489716 16.8915329 -4.36060047 -0.87198144
		 15.71644592 -4.17010784 -0.8765732 16.91439056 -4.36248875 -0.86729032 18.06451416 -4.55075359 -0.86740619
		 16.86656189 -4.35837269 -0.87668908 12.24804688 -3.59706116 -0.86280698 12.24634552 -3.59856796 -0.86732608
		 11.094062805 -3.40997791 -0.86728263 12.25665283 -3.59665489 -0.85821491 13.40893555 -3.78524494 -0.85825843
		 12.33495331 -3.60040665 -0.83527911 12.3126297 -3.59856796 -0.83986735 11.18244171 -3.40997791 -0.83067089
		 12.34626007 -3.60044575 -0.83069253 13.47644043 -3.78903484 -0.83988881 16.95783234 -4.36056614 -0.84443241
		 15.80484009 -4.17007351 -0.83987194 16.98069 -4.36248875 -0.83983159 18.10868073 -4.55070591 -0.848993
		 16.93283081 -4.35829067 -0.84903336 12.40279388 -3.59700584 -0.79857105 12.40102386 -3.59853172 -0.80316269
		 11.27082825 -3.40997791 -0.7940594 12.41134644 -3.59658432 -0.79395878 13.54154205 -3.78514004 -0.80306208
		 12.48960876 -3.60040092 -0.77120513 12.46731567 -3.59853172 -0.77570397 11.35922241 -3.40997791 -0.75744772
		 12.50092316 -3.60044289 -0.76662058 13.60902405 -3.7889967 -0.78487682 17.024131775 -4.36052799 -0.81687993
		 15.89323425 -4.17003918 -0.80316734 17.046981812 -4.36248875 -0.81237292 18.15284729 -4.55065823 -0.83057529
		 16.99909973 -4.35820675 -0.82136971 12.61460876 -3.60298157 -0.72595549;
	setAttr ".vt[1660:1825]" 12.60168457 -3.60334396 -0.73222637 11.44761658 -3.40997791 -0.72083604
		 12.62493896 -3.60219097 -0.71967804 13.77901459 -3.79555798 -0.73106825 17.29220581 -4.36618519 -0.72015232
		 16.12703705 -4.17485332 -0.71850574 17.30110168 -4.36249256 -0.70712131 18.4540329 -4.55697155 -0.7218045
		 17.27996826 -4.36933231 -0.73318893 19.44296265 -4.74577236 -0.79004294 19.48666382 -4.74635792 -0.77342784
		 20.57233429 -4.93399715 -0.79865229 19.39821625 -4.74500751 -0.80664563 18.31254578 -4.55737019 -0.78142118
		 26.56945038 -5.88651371 -0.72544831 25.40538025 -5.69600964 -0.72544557 26.61363983 -5.88651562 -0.7071448
		 27.73352051 -6.077015877 -0.72545105 26.5252533 -5.88651371 -0.74375188 28.72085571 -6.26751995 -0.79866606
		 28.76502991 -6.26751995 -0.78036296 29.88492584 -6.45802402 -0.79866797 28.67665863 -6.26751995 -0.81696916
		 27.55677032 -6.077015877 -0.79866415 23.97616577 -5.50550365 -0.83526593 25.14024353 -5.69600773 -0.8352676
		 23.93198395 -5.50550365 -0.85356981 22.81208801 -5.31500149 -0.83526438 24.020355225 -5.50550365 -0.81696224
		 28.36737823 -6.26752186 -0.94509071 28.41156006 -6.26751995 -0.92678756 29.53145599 -6.45802402 -0.94509125
		 28.32318878 -6.26751995 -0.96339375 27.20329285 -6.077017784 -0.94509017 23.62269592 -5.50550747 -0.98169619
		 24.78677368 -5.69600964 -0.98169619 23.57850647 -5.50550747 -0.99999988 22.45861053 -5.3150053 -0.98169589
		 23.66687775 -5.50550747 -0.96339232 21.47127533 -5.12449932 -0.90847927 21.42709351 -5.12449932 -0.92678338
		 20.30719757 -4.93399715 -0.90847844 21.51546478 -5.12449932 -0.89017522 22.63535309 -5.3150034 -0.90848017
		 11.38621521 -3.59711742 -0.78006917 11.38455963 -3.5986042 -0.7756207 10.21017456 -3.40997791 -0.76660055
		 11.39486694 -3.59672642 -0.78464204 12.56925201 -3.78535175 -0.79366219 11.47319031 -3.60041237 -0.80775452
		 11.45085144 -3.59860325 -0.80307943 10.2985611 -3.40997791 -0.80321223 11.48448944 -3.60044861 -0.81234252
		 12.63677979 -3.78907394 -0.81220973 16.18447876 -4.36061001 -0.8351261 15.0093765259 -4.17011356 -0.8305341
		 16.20733643 -4.36249828 -0.83981711 17.35747528 -4.55076313 -0.83970141 16.15950775 -4.35838223 -0.83041841
		 11.54095459 -3.59706306 -0.84429985 11.53924561 -3.59856987 -0.83978069 10.38694763 -3.40997791 -0.83982396
		 11.54956055 -3.5966568 -0.84889174 12.70185089 -3.78524876 -0.84884846 11.62785339 -3.60040855 -0.87182784
		 11.60553741 -3.59856987 -0.86723948 10.47533417 -3.40997791 -0.87643564 11.63915253 -3.6004467 -0.8764143
		 12.76935577 -3.7890377 -0.86721814 16.2507782 -4.36057377 -0.86267519 15.097770691 -4.17008114 -0.86723536
		 16.27363586 -4.36249828 -0.86727589 17.40164185 -4.55071545 -0.8581146 16.22577667 -4.35830021 -0.85807407
		 11.69568634 -3.59700775 -0.90853584 11.69393158 -3.59853363 -0.90394402 10.56372833 -3.40997791 -0.91304713
		 11.70424652 -3.59658527 -0.91314811 12.8344574 -3.78514194 -0.90404481 11.78250885 -3.60040283 -0.93590158
		 11.76022339 -3.59853363 -0.93140286 10.65211487 -3.40997791 -0.94965887 11.79383087 -3.60044479 -0.94048619
		 12.90193176 -3.78900051 -0.92223012 16.31707001 -4.36053753 -0.8902275 15.18616486 -4.1700449 -0.9039399
		 16.33992767 -4.36249828 -0.89473456 17.44580841 -4.55066776 -0.87653232 16.29204559 -4.35821438 -0.8857376
		 11.90751648 -3.60298157 -0.98115307 11.89459229 -3.60334396 -0.97488183 10.7405014 -3.40997791 -0.98627049
		 11.91783905 -3.60219193 -0.98743075 13.071929932 -3.79555798 -0.97604215 16.58515167 -4.3661871 -0.98696727
		 15.41996765 -4.17485332 -0.98861122 16.59403992 -4.36249256 -0.99999988 17.74697876 -4.55697346 -0.98531765
		 16.57291412 -4.36933613 -0.97392893 18.73592377 -4.74577808 -0.91707206 18.77962494 -4.74636364 -0.93368965
		 19.86530304 -4.93400478 -0.9084639 18.69117737 -4.74501705 -0.90046692 17.60549927 -4.55737591 -0.92569268
		 25.86248016 -5.88651752 -0.98169178 24.69838715 -5.69601345 -0.98169202 25.90666962 -5.88651562 -0.99999988
		 27.026550293 -6.077019691 -0.98169172 25.81827545 -5.88651752 -0.96338373 28.013816833 -6.26752186 -0.90845728
		 28.058013916 -6.26752186 -0.92676574 29.1778717 -6.45802402 -0.90845621 27.96960449 -6.26752186 -0.89014864
		 26.84973907 -6.077019691 -0.90845823 23.26913452 -5.50551319 -0.8718456 24.43318939 -5.69601536 -0.87184417
		 23.22492981 -5.50551319 -0.85353768 22.10506439 -5.31501293 -0.87184691 23.3133316 -5.5055151 -0.89015335
		 27.66017151 -6.26752186 -0.76198864 27.70436859 -6.26752186 -0.78029716 28.82423401 -6.45802402 -0.76198614
		 27.61595917 -6.26752186 -0.74368 26.49610138 -6.077019691 -0.76199096 22.9154892 -5.50550938 -0.72538239
		 24.079559326 -5.69601154 -0.72537971 22.87128448 -5.50550938 -0.70707458 21.75141907 -5.31500721 -0.72538519
		 22.95969391 -5.50550938 -0.74369037 20.76417542 -5.12450695 -0.79861814 20.7199707 -5.12450695 -0.78031063
		 19.60010529 -4.93400478 -0.79862028 20.80838013 -5.12450886 -0.81692576 21.92823792 -5.31500912 -0.79861599
		 10.99393463 -3.59772682 -0.17634237 10.99465942 -3.59858227 -0.1654648 9.83314514 -3.40997791 -0.14363104
		 10.99211884 -3.59668636 -0.1872984 12.15362549 -3.78529167 -0.20913216 11.034721375 -3.59988308 -0.24299185
		 11.022117615 -3.59858227 -0.23175606 9.86975861 -3.40997791 -0.23201939 11.041046143 -3.60016727 -0.25407678
		 12.19340515 -3.78877354 -0.25381342 15.71007538 -4.36054134 -0.30910888 14.54176331 -4.17009258 -0.29804784
		 15.72667694 -4.36249256 -0.32040861 16.87451935 -4.55036259 -0.32009098 15.68960571 -4.35796261 -0.29773021
		 11.058097839 -3.59772015 -0.33134422 11.058731079 -3.59856892 -0.320366 9.90637207 -3.40997791 -0.32040775
		 11.056182861 -3.5966568 -0.34242123 12.2085495 -3.78524685 -0.34237951 11.098686218 -3.59986115 -0.39774272
		 11.08618927 -3.59856892 -0.38665724 9.94297791 -3.40997791 -0.40879607 11.10504913 -3.60015583 -0.40879411
		 12.2482605 -3.78874493 -0.3866553 15.73753357 -4.36052608 -0.37562752 14.57837677 -4.17007732 -0.38665777
		 15.75413513 -4.36249256 -0.38669983 16.89279938 -4.55033588 -0.36459082 15.71704102 -4.35792065 -0.3645488
		 11.12226868 -3.59771442 -0.48634765 11.12281036 -3.59855461 -0.47527075;
	setAttr ".vt[1826:1991]" 9.97959137 -3.40997791 -0.49718443 11.12026978 -3.59662914 -0.49755096
		 12.26348114 -3.78520584 -0.47563732 11.1626358 -3.59983826 -0.552499 11.15026093 -3.59855461 -0.54156196
		 10.016204834 -3.40997791 -0.58557272 11.16906738 -3.60014343 -0.56351441 12.30312347 -3.78871918 -0.51950365
		 15.76498413 -4.36050701 -0.44215 14.61499023 -4.17006397 -0.47527122 15.78159332 -4.36249256 -0.4529911
		 16.91108704 -4.5503149 -0.40909651 15.74448395 -4.35788441 -0.43137661 11.23294067 -3.60396481 -0.66135627
		 11.22275543 -3.60332298 -0.64624143 10.052818298 -3.40997887 -0.67396104 11.23469543 -3.60322285 -0.67652786
		 12.4046402 -3.79656601 -0.64880818 15.89492798 -4.36595821 -0.67542726 14.7286911 -4.17483044 -0.67937827
		 15.88687897 -4.36249447 -0.70709211 17.054046631 -4.5559206 -0.67143011 15.89584351 -4.36825657 -0.64371616
		 18.14998627 -4.74645329 -0.50656158 18.16595459 -4.74634075 -0.5467962 19.28752899 -4.93400097 -0.48612794
		 18.12776184 -4.74553204 -0.46650103 17.0061950684 -4.55787182 -0.52716929 25.18110657 -5.88651562 -0.66287947
		 24.017036438 -5.69601154 -0.66288203 25.19941711 -5.88651562 -0.70706874 26.34516907 -6.077019691 -0.6628769
		 25.16278076 -5.88651562 -0.61869019 27.43598175 -6.26751995 -0.4861196 27.4542923 -6.26752186 -0.53030825
		 28.60003662 -6.45802402 -0.48611832 27.41766357 -6.26751995 -0.44193095 26.27191162 -6.077019691 -0.48612091
		 22.7430954 -5.50550747 -0.39774433 23.90715027 -5.69601154 -0.39774385 22.72477722 -5.50550747 -0.35355425
		 21.57902527 -5.3150053 -0.39774486 22.76140594 -5.50550938 -0.44193453 27.2894516 -6.26751995 -0.13261014
		 27.30776978 -6.26751995 -0.17679882 28.4535141 -6.45802402 -0.13261177 27.27114105 -6.26751995 -0.088421397
		 26.12539673 -6.077017784 -0.13260846 22.59659576 -5.50550747 -0.044222541 23.76065063 -5.69601154 -0.044225074
		 22.57827759 -5.50550747 -3.2179454e-05 21.43253326 -5.3150053 -0.044219978 22.61490631 -5.50550747 -0.088412873
		 20.34172058 -5.12450314 -0.22098161 20.32341003 -5.12450314 -0.17679055 19.17766571 -4.93400097 -0.2209805
		 20.36003113 -5.12450314 -0.2651726 21.50578308 -5.3150053 -0.22098269 11.14363098 -3.59821796 0.53054655
		 11.1502533 -3.59855556 0.54155636 10.0070571899 -3.40997982 0.56347573 11.12940979 -3.59662914 0.51964259
		 12.27262115 -3.78520584 0.49772325 11.12124634 -3.5990572 0.4641104 11.12280273 -3.59855556 0.47526515
		 9.97044373 -3.40997982 0.47508737 11.120224 -3.5996418 0.45301372 12.27258301 -3.78821754 0.45319158
		 15.74622345 -4.36043644 0.3979255 14.58750916 -4.17006207 0.40895763 15.75410461 -4.36249065 0.38667184
		 16.90189362 -4.550313 0.3869659 15.73529816 -4.35788441 0.40925169 11.079620361 -3.59824467 0.37560111
		 11.086181641 -3.59856987 0.38665172 9.93382263 -3.40997982 0.38669896 11.065330505 -3.59665775 0.36451286
		 12.21768188 -3.78524876 0.36446559 11.057060242 -3.59904861 0.30926424 11.05872345 -3.59856987 0.32036048
		 9.8972168 -3.40997887 0.29831064 11.056060791 -3.59963226 0.29823539 12.21756744 -3.78822327 0.3202852
		 15.71874237 -4.3604517 0.33139503 14.55088806 -4.17007732 0.32034421 15.72663879 -4.36249065 0.3203806
		 16.88360596 -4.55033588 0.34246019 15.70785522 -4.35792065 0.34242383 11.015609741 -3.59827137 0.22066212
		 11.022109985 -3.59858227 0.2317505 9.86061096 -3.40997791 0.20992231 11.0012588501 -3.59668636 0.20938998
		 12.1627655 -3.78529167 0.23121819 10.99288177 -3.59904099 0.15441635 10.99465179 -3.59858227 0.16545925
		 9.8239975 -3.40997791 0.12153398 10.99189758 -3.59962463 0.14345519 12.16255188 -3.78822899 0.18738046
		 15.69127655 -4.36046505 0.26486781 14.5142746 -4.17009068 0.23173422 15.69918823 -4.36249065 0.25408936
		 16.86533356 -4.55036068 0.29796034 15.68041992 -4.3579607 0.27560523 10.98015594 -3.60430527 0.046049908
		 10.98020935 -3.60328865 0.061166152 9.7873764 -3.40997791 0.033145629 10.97279358 -3.60412884 0.030876311
		 12.1656189 -3.79744053 0.05889684 15.62409973 -4.36526012 0.031968303 14.45864868 -4.17479992 0.028011754
		 15.59394073 -4.36249256 -1.4627025e-05 16.78502655 -4.55498219 0.035956971 15.64974213 -4.36728764 0.063983351
		 18.026725769 -4.74700642 0.20091929 18.0057296753 -4.74630642 0.16057797 19.17762756 -4.93399906 0.22093493
		 18.039794922 -4.74642658 0.24103589 16.86790466 -4.55873203 0.18067896 24.92469788 -5.88651562 0.044152148
		 23.76064301 -5.69600964 0.044155348 24.90639496 -5.88651562 -3.8030263e-05 26.088760376 -6.077017784 0.044148903
		 24.94300079 -5.88651371 0.088342287 27.32603455 -6.26751995 0.22090358 27.30773163 -6.26751995 0.17671409
		 28.49009705 -6.45802402 0.22089908 27.34433746 -6.26751995 0.26509306 26.16197205 -6.077017784 0.22090808
		 22.70640564 -5.50550747 0.30930302 23.87046814 -5.69600964 0.30929789 22.72471619 -5.50550747 0.35349369
		 21.54235077 -5.3150053 0.30930811 22.68811035 -5.50550747 0.26511234 27.47248077 -6.26751995 0.57441926
		 27.45417786 -6.26751995 0.53022993 28.63654327 -6.45802402 0.57441247 27.49078369 -6.26751995 0.61860895
		 26.30841827 -6.077017784 0.57442635 22.85286713 -5.50550747 0.66282886 24.016921997 -5.69601154 0.66282123
		 22.87117004 -5.50550938 0.70701963 21.68880463 -5.3150053 0.66283649 22.83455658 -5.50550747 0.6186381
		 20.45152283 -5.12450314 0.48607859 20.46982574 -5.12450314 0.53026998 19.28746033 -4.93399906 0.48608494
		 20.43321228 -5.12450123 0.44188717 21.6155777 -5.3150053 0.48607215 11.74889374 -3.59849834 0.92680329
		 11.76020813 -3.59853554 0.93139821 10.63002014 -3.40997982 0.94050598 11.72633362 -3.59658718 0.92229635
		 12.85652924 -3.78514385 0.9131887 11.68301392 -3.59856796 0.89934462 11.69392395 -3.59853554 0.90393949
		 10.54163361 -3.40997982 0.90389425 11.67431641 -3.59895706 0.89475095 12.82660675 -3.78751373 0.89479607
		 16.27220154 -4.36043453 0.87190127 15.1198349 -4.17004681 0.87646306 16.27359009 -4.36249828 0.86725307
		 17.42363739 -4.55066776 0.86735207 16.26989746 -4.35821629 0.876562 11.59421539 -3.59853649 0.86263835
		 11.60552979 -3.59857178 0.86723495 10.45323944 -3.40997982 0.86728263;
	setAttr ".vt[1992:2157]" 11.57164001 -3.59665871 0.85804021 12.72393036 -3.78525066 0.85799247
		 11.52832031 -3.59859943 0.83518314 11.53923035 -3.59857178 0.83977616 10.36485291 -3.40997982 0.83067089
		 11.51957703 -3.59897709 0.83060801 12.6939621 -3.78756809 0.83971322 16.20590973 -4.36047459 0.84434396
		 15.031440735 -4.17008305 0.8397584 16.20729065 -4.36249828 0.83979434 17.37947845 -4.55071735 0.84893435
		 16.20362854 -4.35830021 0.84889841 11.43953705 -3.59857368 0.79847711 11.45084381 -3.59860516 0.80307496
		 10.27645874 -3.40997982 0.7940594 11.41695404 -3.59672832 0.79379064 12.59132385 -3.78535366 0.80280614
		 11.37362671 -3.59863281 0.77102488 11.38454437 -3.59860516 0.77561617 10.18807983 -3.40997982 0.75744772
		 11.36485291 -3.59899616 0.76646709 12.56132507 -3.78762341 0.78463548 16.13961792 -4.36051083 0.81679022
		 14.94304657 -4.17011547 0.80305719 16.14099884 -4.36249828 0.81233555 17.33531189 -4.55076504 0.83052117
		 16.13735962 -4.35838413 0.82124281 11.29882813 -3.6040802 0.72625262 11.309021 -3.60327148 0.7325238
		 10.099693298 -3.40997982 0.72083604 11.28701019 -3.6046257 0.71997547 12.49635315 -3.79791737 0.73166317
		 15.93242645 -4.3646841 0.72041076 14.76805115 -4.17478085 0.71877068 15.88683319 -4.36249447 0.70706707
		 17.096076965 -4.5544672 0.72205305 15.97729492 -4.36675358 0.73375672 18.43753815 -4.7470026 0.79038215
		 18.39283752 -4.74629116 0.77367032 19.60002899 -4.93400669 0.79858243 18.48019409 -4.7473917 0.80706763
		 17.27300262 -4.55967617 0.78215557 25.24349213 -5.88651562 0.72531271 24.079437256 -5.69601154 0.7253204
		 25.19928741 -5.88651562 0.70700377 26.40755463 -6.077019691 0.72530502 25.28769684 -5.88651752 0.74362165
		 27.74842072 -6.26752186 0.79853481 27.704216 -6.26752186 0.78022552 28.91248322 -6.45802402 0.79852802
		 27.79263306 -6.26752186 0.81684422 26.58437347 -6.077019691 0.79854167 23.18061066 -5.50551319 0.83517903
		 24.34466553 -5.69601536 0.83517265 23.22481537 -5.5055151 0.85348749 22.016548157 -5.31501102 0.83518541
		 23.13640594 -5.50551319 0.81687057 28.10205841 -6.26752186 0.94500977 28.057853699 -6.26752186 0.92670041
		 29.26612091 -6.45802402 0.94500482 28.14627075 -6.26752186 0.96331912 26.93801117 -6.077021599 0.94501483
		 23.53420258 -5.50550938 0.98164612 24.69826508 -5.69601345 0.98164159 23.57839966 -5.50550938 0.99995428
		 22.37013245 -5.31500721 0.98165041 23.49000549 -5.50551128 0.96333772 21.029296875 -5.12450886 0.90842348
		 21.073486328 -5.12450886 0.92673123 19.86522675 -4.93400669 0.90842867 20.98509979 -5.12451077 0.89011562
		 22.19335938 -5.31501293 0.90841812 12.45600128 -3.59849548 0.78029603 12.46731567 -3.59853172 0.77570093
		 11.33712769 -3.40997791 0.76660073 12.43344879 -3.59658432 0.78480273 13.56363678 -3.78514004 0.79390299
		 12.39012909 -3.59856415 0.80775452 12.40102386 -3.59853172 0.80315959 11.24873352 -3.40997791 0.80321223
		 12.38141632 -3.5989542 0.81234813 13.53371429 -3.78750992 0.81229538 16.97930908 -4.36042881 0.83516765
		 15.82694244 -4.17004108 0.83061337 16.98069 -4.36249065 0.83981574 18.13075256 -4.55066013 0.83970916
		 16.977005 -4.35821056 0.83050692 12.30133057 -3.59853363 0.8444609 12.3126297 -3.59856796 0.83986425
		 11.16034698 -3.40997791 0.83982396 12.27874756 -3.59665585 0.84905905 13.43103027 -3.78524685 0.84909934
		 12.23543549 -3.59859657 0.87191588 12.24634552 -3.59856796 0.86732292 11.07195282 -3.40997791 0.87643564
		 12.22668457 -3.59897423 0.87649101 13.40106964 -3.78756523 0.86737835 16.91300964 -4.36046696 0.86272496
		 15.73854065 -4.17007732 0.86731797 16.91439056 -4.36249065 0.8672744 18.086585999 -4.55070782 0.85812688
		 16.91072845 -4.35829258 0.85817045 12.14664459 -3.59856987 0.90862191 12.15794373 -3.59860229 0.90402406
		 10.98357391 -3.40997791 0.91304713 12.12405396 -3.59672451 0.91330856 13.2984314 -3.78534985 0.90428549
		 12.080734253 -3.59862995 0.9360742 12.091651917 -3.59860229 0.93148291 10.89517975 -3.40997791 0.94965887
		 12.071960449 -3.59899426 0.94063205 13.26842499 -3.78761959 0.92245609 16.84672546 -4.3605032 0.89027852
		 15.65015411 -4.17010975 0.90401924 16.84810638 -4.36249256 0.89473313 18.042419434 -4.55075741 0.87654006
		 16.84445953 -4.3583765 0.88582605 12.0059356689 -3.60407925 0.9808436 12.016136169 -3.60326958 0.9745729
		 10.80679321 -3.40997887 0.98627049 11.99411774 -3.60462475 0.98712009 13.20345306 -3.79791546 0.9754225
		 16.63957977 -4.3646822 0.98663813 15.47518921 -4.17477703 0.98828989 16.59399414 -4.36249447 0.99997914
		 17.80323029 -4.55446529 0.9849844 16.68443298 -4.36674976 0.97329521 19.14466095 -4.74699688 0.91665953
		 19.099975586 -4.74628544 0.93336743 20.30715942 -4.93400097 0.90845007 19.18730164 -4.74738407 0.89997804
		 17.9801178 -4.55966854 0.92489535 25.95072174 -5.88651562 0.98164427 24.78665924 -5.69601154 0.98164803
		 25.90653229 -5.88651562 0.999946 27.11479187 -6.077017784 0.9816404 25.99491882 -5.88651562 0.96334243
		 28.45563507 -6.26751995 0.90843213 28.41144562 -6.26751995 0.9267332 29.6197052 -6.45802212 0.9084295
		 28.49983215 -6.26751804 0.89013094 27.29157257 -6.077015877 0.90843457 23.88776398 -5.50550747 0.87183774
		 25.051826477 -5.69600964 0.87183571 23.93195343 -5.50550747 0.85353523 22.72368622 -5.3150053 0.87183952
		 23.84355927 -5.50550747 0.89014 28.80919647 -6.26751804 0.76202303 28.76500702 -6.26751804 0.7803241
		 29.9732666 -6.45802021 0.76202285 28.85339355 -6.26751804 0.7437219 27.64512634 -6.077015877 0.76202315
		 24.24131775 -5.50550747 0.72541869 25.40538025 -5.69600773 0.72541916 24.2855072 -5.50550556 0.70711625
		 23.07724762 -5.3150034 0.72541809 24.19712067 -5.50550556 0.74372101 21.73639679 -5.12450123 0.79862952
		 21.78059387 -5.12450123 0.78032649 20.57232666 -4.93399906 0.79863018 21.69220734 -5.12450123 0.81693256
		 22.90046692 -5.3150034 0.79862881 12.85075378 -3.59821701 0.17655544 12.8573761 -3.59855366 0.16554557
		 11.7141571 -3.40997791 0.14363104 12.83652496 -3.59662819 0.18745947;
	setAttr ".vt[2158:2323]" 13.97973633 -3.78520393 0.20937397 12.82836914 -3.59905529 0.24299155
		 12.82991028 -3.59855366 0.23183681 11.67755127 -3.40997791 0.23201941 12.82733154 -3.59963989 0.25408819
		 13.97970581 -3.78821564 0.25390562 17.45335388 -4.36043644 0.30915716 16.29463959 -4.17006207 0.29812995
		 17.46124268 -4.36249065 0.32041082 18.60903931 -4.55031109 0.32011205 17.44243622 -4.3578825 0.29783112
		 12.78673553 -3.59824276 0.33150089 12.79329681 -3.59856796 0.32045019 11.64093781 -3.40997791 0.32040775
		 12.77244568 -3.59665585 0.34258917 13.92480469 -3.78524685 0.34263167 12.76417542 -3.59904766 0.39783776
		 12.76583099 -3.59856796 0.38674152 11.60432434 -3.40997791 0.40879613 12.76316833 -3.59963131 0.40886658
		 13.92468262 -3.78822136 0.38681194 17.42588043 -4.36044979 0.37568778 16.25801849 -4.17007542 0.38674334
		 17.43378448 -4.36249065 0.38670212 18.59075165 -4.55033398 0.36461774 17.41499329 -4.35791874 0.36465898
		 12.72272491 -3.59827042 0.48643991 12.72921753 -3.59858227 0.47535151 11.56771088 -3.40997791 0.49718443
		 12.70836639 -3.59668636 0.49771208 13.86988831 -3.78529167 0.47587919 12.69998932 -3.59904099 0.55268562
		 12.70176697 -3.59858227 0.54164273 11.53110504 -3.40997791 0.58557272 12.69900513 -3.59962368 0.56364679
		 13.86967468 -3.78822899 0.5197168 17.39841461 -4.36046505 0.44221491 16.22140503 -4.17009068 0.47535333
		 17.40632629 -4.36249065 0.45299336 18.57247925 -4.55036068 0.40911758 17.38755798 -4.3579607 0.43147758
		 12.68727112 -3.60430527 0.66105366 12.68732452 -3.60328865 0.6459372 11.49449158 -3.40997791 0.6739611
		 12.67990875 -3.60412884 0.67622769 13.87274933 -3.79743862 0.64820373 17.33126068 -4.36525822 0.67512643
		 16.165802 -4.17479801 0.67908537 17.30110168 -4.36249065 0.70711094 18.49219513 -4.55497837 0.67113531
		 17.35689545 -4.36728573 0.64310968 19.73388672 -4.74700451 0.50616091 19.71289825 -4.74630451 0.54650474
		 20.88479614 -4.93399715 0.48614159 19.74695587 -4.74642467 0.46604198 18.57505798 -4.55873013 0.5264051
		 26.63194275 -5.8865118 0.66292316 25.46787262 -5.69600773 0.66292244 26.61363983 -5.8865118 0.70711792
		 27.79600525 -6.077015877 0.66292393 26.65023804 -5.8865118 0.61872846 29.033256531 -6.26751804 0.48614496
		 29.014961243 -6.26751804 0.5303399 30.19732666 -6.45802021 0.48614535 29.051551819 -6.26751804 0.44195002
		 27.8691864 -6.077015877 0.48614454 24.41358948 -5.50550556 0.39775375 25.57765198 -5.69600773 0.3977541
		 24.43188477 -5.50550556 0.35355905 23.24951935 -5.3150034 0.39775348 24.39528656 -5.50550556 0.44194847
		 29.17961884 -6.26751995 0.13258517 29.16132355 -6.26751995 0.17678013 30.34368134 -6.45802212 0.13258509
		 29.19792175 -6.26751995 0.088390149 28.015563965 -6.077015877 0.13258518 24.55997467 -5.50550747 0.044195008
		 25.72403717 -5.69600964 0.044195041 24.57827759 -5.50550747 -4.5110226e-12 23.39592743 -5.3150053 0.044194959
		 24.54167938 -5.50550747 0.08839 22.15865326 -5.12450123 0.22097428 22.17696381 -5.12450123 0.17677942
		 20.99460602 -4.93399906 0.22097397 22.14035797 -5.12450123 0.26516908 23.32272339 -5.3150034 0.22097452
		 12.70104218 -3.59772682 -0.53076619 12.70176697 -3.59858227 -0.54164374 11.54026031 -3.40997791 -0.56347573
		 12.69921875 -3.59668636 -0.51981014 13.86073303 -3.78529167 -0.49797827 12.74182129 -3.59988308 -0.46411669
		 12.72922516 -3.59858227 -0.47535253 11.57686615 -3.40997791 -0.47508737 12.74815369 -3.60016727 -0.45303178
		 13.9005127 -3.78877354 -0.45329696 17.41719055 -4.36054134 -0.39800695 16.24887085 -4.17009258 -0.4090662
		 17.43378448 -4.36249256 -0.38670725 18.58163452 -4.55036068 -0.38702667 17.39671326 -4.35796261 -0.40938562
		 12.76519775 -3.59772015 -0.37576431 12.76583862 -3.59856892 -0.38674247 11.61347198 -3.40997791 -0.38669896
		 12.76329041 -3.5966568 -0.36468726 13.91564941 -3.78524685 -0.36473078 12.80578613 -3.59986115 -0.30936581
		 12.79329681 -3.59856892 -0.32045126 11.65008545 -3.40997791 -0.29831064 12.81216431 -3.60015583 -0.29831445
		 13.95537567 -3.78874493 -0.32045504 17.44463348 -4.36052418 -0.33148837 16.28548431 -4.17007732 -0.32045621
		 17.46124268 -4.36249256 -0.320416 18.59991455 -4.55033588 -0.34252682 17.42414856 -4.35792065 -0.34256703
		 12.82936859 -3.59771442 -0.22076091 12.82991028 -3.59855461 -0.23183784 11.68670654 -3.40997791 -0.20992231
		 12.82737732 -3.59662819 -0.20955753 13.97058868 -3.78520584 -0.23147304 12.86975098 -3.5998373 -0.15460949
		 12.8573761 -3.59855461 -0.16554658 11.72331238 -3.40997791 -0.12153398 12.8761673 -3.60014248 -0.1435941
		 14.010238647 -3.78871918 -0.18760669 17.47209167 -4.36050701 -0.26496586 16.32209778 -4.17006207 -0.23184282
		 17.4887085 -4.36249256 -0.25412476 18.61819458 -4.550313 -0.29802114 17.45159149 -4.35788441 -0.27573919
		 12.94004059 -3.6039629 -0.045749422 12.92986298 -3.60332108 -0.060864888 11.75991821 -3.40997791 -0.033145629
		 12.94179535 -3.60322189 -0.030577188 14.11174011 -3.79656601 -0.058296453 17.60199738 -4.36595631 -0.031667814
		 16.4357605 -4.17482853 -0.027719265 17.59394073 -4.36249256 -2.0515161e-12 18.76109314 -4.5559206 -0.035662722
		 17.6029129 -4.36825657 -0.063381985 19.85707092 -4.74645329 -0.20054555 19.87303162 -4.74633884 -0.16030675
		 20.99460602 -4.93400097 -0.22097912 19.83485413 -4.74553204 -0.24061035 18.71327972 -4.55786991 -0.17993796
		 26.88809967 -5.88651562 -0.044196736 25.72403717 -5.69601154 -0.044196568 26.90639496 -5.88651562 -5.3308582e-12
		 28.052154541 -6.077017784 -0.044196896 26.86979675 -5.88651562 -0.088393465 29.14303589 -6.26751995 -0.2209848
		 29.16132355 -6.26751995 -0.17678785 30.30709839 -6.45802402 -0.22098549 29.1247406 -6.26751995 -0.26518172
		 27.97896576 -6.077017784 -0.22098409 24.45019531 -5.50550747 -0.30937427 25.61425018 -5.69601154 -0.30937535
		 24.4318924 -5.50550747 -0.35357043 23.28612518 -5.3150053 -0.30937314 24.46848297 -5.50550747 -0.26517802
		 28.99667358 -6.26752186 -0.57456005 29.014961243 -6.26751995 -0.5303632 30.16073608 -6.45802402 -0.57456231
		 28.97837067 -6.26752186 -0.61875695 27.83259583 -6.077019691 -0.57455784;
	setAttr ".vt[2324:2489]" 24.30381012 -5.50550747 -0.66294301 25.46787262 -5.69601154 -0.66294563
		 24.2855072 -5.50550747 -0.70713896 23.13973999 -5.3150053 -0.66294026 24.32210541 -5.50550747 -0.61874694
		 22.048866272 -5.12450314 -0.48615485 22.030570984 -5.12450314 -0.53035057 20.88480377 -4.93400097 -0.48615292
		 22.067169189 -5.12450314 -0.44195926 23.21292877 -5.3150053 -0.48615685 18.96695709 -4.74424648 -0.98336011
		 20.13046265 -4.93399906 -0.98169559 18.92220306 -4.74349689 -0.99999988 17.80329132 -4.55446339 -0.98500872
		 16.63961792 -4.36468029 -0.98665833 16.6844635 -4.36674595 -0.97331488 16.72917938 -4.36879444 -0.95997119
		 15.56573486 -4.1803484 -0.96491843 16.77400208 -4.37085056 -0.94661415 18.25979614 -4.7442503 -0.72372878
		 19.42328644 -4.93400288 -0.72539073 18.2150116 -4.7434988 -0.70708627 17.096138 -4.5544672 -0.72208291
		 15.93248749 -4.3646841 -0.72043556 15.97734833 -4.36675358 -0.73378134 16.022079468 -4.36880016 -0.747127
		 14.85864258 -4.18035603 -0.74218154 16.066917419 -4.3708601 -0.76048601 17.94597626 -4.74470425 -0.039962001
		 19.10441589 -4.93400097 -0.044214822 17.92205048 -4.74349689 -2.0477837e-05 16.78503418 -4.55498219 -0.035992034
		 15.62409973 -4.36526012 -0.031997576 15.64974976 -4.36728764 -0.064012617 15.67462921 -4.36918736 -0.09603323
		 14.51620483 -4.18041706 -0.084082223 15.69857788 -4.37092113 -0.12827656 18.21308136 -4.74587917 0.66746002
		 19.36069489 -4.93400097 0.66285163 18.21494293 -4.7434988 0.70705128 17.053985596 -4.5559206 0.67139947
		 15.8948822 -4.36595821 0.67540187 15.89580536 -4.36825657 0.64369023 15.89627075 -4.37048626 0.61193776
		 14.74039459 -4.18051243 0.62392342 15.88615417 -4.3710146 0.57972383 18.90874481 -4.74774456 0.98368305
		 20.041999817 -4.93400288 0.98165911 18.92212677 -4.7434988 0.99997085 17.74691772 -4.55697536 0.98529226
		 16.58509827 -4.3661871 0.98694617 16.57286072 -4.36933613 0.97390747 16.56122589 -4.3725996 0.96082538
		 15.39994049 -4.180583 0.96581656 16.51982117 -4.37108707 0.9475047 19.61585236 -4.74774075 0.72339684
		 20.74910736 -4.93399906 0.7254169 19.62923431 -4.74349499 0.70711279 18.4540329 -4.55697155 0.72179121
		 17.29220581 -4.36618328 0.72014129 17.27996826 -4.36933231 0.73317707 17.26832581 -4.37259579 0.74625635
		 16.10705566 -4.18057919 0.7412703 17.22692108 -4.37108135 0.75957441 19.92019653 -4.74587727 0.039593827
		 21.067802429 -4.93400097 0.044194795 19.92205048 -4.74349689 -2.8713514e-12 18.76109314 -4.55591869 0.035661947
		 17.60199738 -4.36595631 0.031667139 17.6029129 -4.36825466 0.063380674 17.60339355 -4.37048626 0.095135048
		 16.4475174 -4.18051052 0.083156176 17.59326935 -4.37101269 0.12735097 19.65313721 -4.74470425 -0.66718233
		 20.81159973 -4.93400097 -0.66293466 19.62923431 -4.74349689 -0.70712721 18.49220276 -4.55498219 -0.67114693
		 17.33126068 -4.36526012 -0.67513609 17.35689545 -4.36728764 -0.64311868 17.3817749 -4.36918736 -0.61109573
		 16.2233429 -4.18041706 -0.6230424 17.40571594 -4.37092113 -0.57884991 12.062156677 -3.59919453 -0.94520473
		 12.059249878 -3.60047913 -0.94966036 10.87308502 -3.40997791 -0.95881182 12.071960449 -3.5989933 -0.9406352
		 12.080734253 -3.59862995 -0.93607742 10.89517975 -3.40997791 -0.94965887 13.26842499 -3.78761768 -0.9224624
		 14.45612335 -3.97660732 -0.90884757 14.46250153 -3.97585011 -0.90429348 15.64867401 -4.16635227 -0.89514202
		 12.21690369 -3.59918213 -0.88107336 12.21393585 -3.60047913 -0.88559002 11.049858093 -3.40997791 -0.88558853
		 12.22668457 -3.59897423 -0.87649423 12.23543549 -3.59859657 -0.8719191 11.07195282 -3.40997791 -0.87643564
		 13.40106964 -3.78756332 -0.86738467 14.56671143 -3.97652912 -0.86285043 14.57300568 -3.97574711 -0.85825998
		 15.73706818 -4.16624928 -0.85826141 12.37165833 -3.59916782 -0.81694049 12.36860657 -3.60047913 -0.82151955
		 11.22663879 -3.40997791 -0.81236517 12.38141632 -3.59895325 -0.81235117 12.39012909 -3.59856415 -0.80775762
		 11.24873352 -3.40997791 -0.80321223 13.53371429 -3.78750896 -0.81230164 14.6772995 -3.97645283 -0.81684589
		 14.68350983 -3.9756403 -0.81221652 15.82548523 -4.16614246 -0.82137096 12.56834412 -3.60539913 -0.75124264
		 12.52329254 -3.60047913 -0.75744927 11.40341187 -3.40997791 -0.73914194 12.57963562 -3.60473251 -0.74487269
		 12.59151459 -3.60418034 -0.73855662 11.42551422 -3.40997791 -0.72998905 13.7558136 -3.79809761 -0.74711001
		 14.92010498 -3.99199772 -0.75561249 14.93193054 -3.99007702 -0.7458539 16.051811218 -4.18057919 -0.76416123
		 21.91317749 -5.12450314 -0.72543734 20.74910736 -4.93399906 -0.72543454 21.95737457 -5.12450314 -0.70713305
		 23.07724762 -5.3150053 -0.72544008 24.24131775 -5.50550747 -0.72544283 24.19712067 -5.50550747 -0.74374676
		 24.15292358 -5.50550556 -0.76205069 25.31699371 -5.69600773 -0.76205313 24.10873413 -5.50550365 -0.78035462
		 28.54411316 -6.26751995 -0.87187833 28.58829498 -6.26751995 -0.85357535 29.70819092 -6.45802402 -0.87187952
		 28.49993134 -6.26751995 -0.89018142 28.45574188 -6.26751995 -0.90848458 29.61982727 -6.45802402 -0.90848541
		 27.29166412 -6.077015877 -0.90848356 26.12758636 -5.88651371 -0.90848273 26.083404541 -5.88651371 -0.92678618
		 24.96350861 -5.69600964 -0.9084819 11.35505676 -3.59919643 -0.76190209 11.35214996 -3.60048199 -0.75744635
		 10.16597748 -3.40997791 -0.74829483 11.36486053 -3.59899521 -0.76647162 11.37364197 -3.59863186 -0.77102941
		 10.18807983 -3.40997791 -0.75744772 12.56134796 -3.7876215 -0.7846446 13.74904633 -3.97661304 -0.79825962
		 13.7554245 -3.97585583 -0.80281371 14.94160461 -4.1663599 -0.81196535 11.5098114 -3.59918308 -0.82603353
		 11.50682831 -3.60048103 -0.82151675 10.34275818 -3.40997791 -0.82151806 11.51959229 -3.59897518 -0.83061248
		 11.52833557 -3.59859848 -0.83518773 10.36485291 -3.40997791 -0.83067089 12.69398499 -3.78756714 -0.83972228
		 13.8596344 -3.97653484 -0.84425676 13.86592865 -3.97575283 -0.84884715 15.030006409 -4.166255 -0.84884584
		 11.66455078 -3.59916973 -0.89016616 11.66151428 -3.60048103 -0.88558716 10.51953888 -3.40997791 -0.89474136
		 11.67432404 -3.59895515 -0.89475548 11.68302917 -3.5985651 -0.89934915;
	setAttr ".vt[2490:2655]" 10.54163361 -3.40997791 -0.90389425 12.82662201 -3.78751183 -0.89480519
		 13.97022247 -3.97645855 -0.89026135 13.97644043 -3.97564602 -0.89489055 15.11841583 -4.16615009 -0.88573629
		 11.8612442 -3.60540104 -0.95586431 11.81619263 -3.60048103 -0.94965738 10.69631195 -3.40997791 -0.96796459
		 11.87254333 -3.60473347 -0.96223468 11.8844223 -3.60418129 -0.96855128 10.71840668 -3.40997791 -0.97711748
		 13.048721313 -3.79810143 -0.95999902 14.21303558 -3.99200153 -0.95149791 14.22486115 -3.99007893 -0.96125752
		 15.34474182 -4.180583 -0.94295025 21.20615387 -5.12450314 -0.98169243 20.04208374 -4.93400097 -0.98169267
		 21.25035095 -5.12450314 -0.99999988 22.37023926 -5.31500721 -0.98169231 23.53431702 -5.50550938 -0.98169214
		 23.49011993 -5.50551128 -0.96338451 23.44593048 -5.50551319 -0.9450767 24.61000824 -5.69601536 -0.94507617
		 23.40174103 -5.5055151 -0.92676896 27.83699036 -6.26752186 -0.8352229 27.88119507 -6.26752186 -0.85353148
		 29.0010528564 -6.45802402 -0.83522117 27.79277802 -6.26752186 -0.81691432 27.7485733 -6.26752186 -0.79860568
		 28.91265106 -6.45802402 -0.79860365 26.5845108 -6.077019691 -0.79860777 25.42044067 -5.88651752 -0.79860979
		 25.37623596 -5.88651752 -0.78030157 24.25637817 -5.69601345 -0.79861188 10.98947144 -3.59997177 -0.13245888
		 10.98806 -3.60048008 -0.12153415 9.81484222 -3.40997791 -0.099436879 10.99190521 -3.59962368 -0.14346075
		 10.9928894 -3.59904099 -0.15442191 9.8239975 -3.40997791 -0.12153398 12.16256714 -3.78822899 -0.18739158
		 13.3322525 -3.97741604 -0.22036098 13.32684326 -3.97579479 -0.23122936 14.50004578 -4.16629696 -0.25332665
		 11.053634644 -3.59998322 -0.28719866 11.052124023 -3.60048008 -0.27621377 9.88806152 -3.40997791 -0.27621359
		 11.05606842 -3.59963226 -0.29824096 11.057067871 -3.59904766 -0.30926973 9.8972168 -3.40997791 -0.29831064
		 12.2175827 -3.78822327 -0.32029632 13.3780899 -3.97739697 -0.33132264 13.37260437 -3.97574902 -0.34237969
		 14.53666687 -4.16625118 -0.34237987 11.11779785 -3.59999371 -0.44193599 11.11619568 -3.60048008 -0.43089333
		 9.96128845 -3.40997791 -0.45299023 11.12023163 -3.59963989 -0.45301926 11.12125397 -3.59905624 -0.46411601
		 9.97044373 -3.40997791 -0.47508737 12.27259064 -3.78821564 -0.45320269 13.42393494 -3.97737789 -0.44228914
		 13.41838837 -3.97570705 -0.45354041 14.57330322 -4.16621113 -0.43144351 11.20955658 -3.60425854 -0.60056216
		 11.18026733 -3.60048103 -0.58557296 10.034507751 -3.40997791 -0.62976694 11.21516418 -3.60414314 -0.61581004
		 11.2226944 -3.60434818 -0.63094801 10.043663025 -3.40997887 -0.65186399 12.39426422 -3.79748631 -0.61018741
		 13.56582642 -3.99061871 -0.58952314 13.5717926 -3.99001026 -0.61289907 14.71754456 -4.18051243 -0.56870508
		 20.52483368 -5.12450314 -0.66288978 19.36077118 -4.93400097 -0.66289228 20.54315186 -5.12450314 -0.70708036
		 21.68890381 -5.3150053 -0.66288716 22.85296631 -5.50550938 -0.66288465 22.83465576 -5.50550747 -0.61869466
		 22.81633759 -5.50550747 -0.57450461 23.98040771 -5.69601154 -0.57450283 22.79803467 -5.50550938 -0.53031468
		 27.36271667 -6.26751995 -0.30936483 27.38102722 -6.26751995 -0.35355353 28.52677155 -6.45802402 -0.309365
		 27.3443985 -6.26751995 -0.26517621 27.32608032 -6.26751995 -0.22098754 28.49015045 -6.45802402 -0.22098839
		 26.16202545 -6.077017784 -0.22098662 24.99796295 -5.88651562 -0.22098568 24.9796524 -5.88651371 -0.17679626
		 23.83389282 -5.69601154 -0.22098474 11.17510223 -3.60038567 0.57453525 11.1802597 -3.60048199 0.58556736
		 10.025360107 -3.40997982 0.60766983 11.16905975 -3.60014439 0.56350887 11.1626358 -3.59983921 0.55249345
		 10.016204834 -3.40997982 0.58557272 12.30310822 -3.78871918 0.51949251 13.44359589 -3.97760105 0.48649162
		 13.42752838 -3.97570705 0.47562078 14.58242035 -4.16621113 0.45351833 11.1110611 -3.60039139 0.41984329
		 11.11618805 -3.60048199 0.43088776 9.95213318 -3.40997982 0.43089318 11.10504913 -3.60015678 0.4087885
		 11.098678589 -3.59986305 0.39773718 9.94297791 -3.40997982 0.40879607 12.24824524 -3.78874683 0.38664418
		 13.39782715 -3.97763157 0.37555122 13.38173676 -3.97574902 0.36446017 14.54579926 -4.16625118 0.36445475
		 11.047027588 -3.60039711 0.26515284 11.052116394 -3.60048103 0.27620816 9.87891388 -3.40997791 0.25411648
		 11.041038513 -3.60016823 0.25407118 11.034713745 -3.59988403 0.24298628 9.86975861 -3.40997791 0.23201939
		 12.19338989 -3.78877354 0.2538023 13.35207367 -3.97766399 0.26461834 13.33596802 -3.97579479 0.25330979
		 14.50917816 -4.16629696 0.27540147 10.9943924 -3.6025362 0.10650681 10.98805237 -3.60048008 0.12152854
		 9.80568695 -3.40997791 0.077339798 10.99191284 -3.60315323 0.09143015 10.99022675 -3.60389614 0.076388046
		 9.79653168 -3.40997791 0.05524271 12.17559052 -3.79646492 0.097353607 13.36094666 -3.98903751 0.11826577
		 13.35671997 -3.98991299 0.095110133 14.53907776 -4.18041515 0.13929889 20.26847076 -5.12450314 0.044165015
		 19.10441589 -4.93400097 0.044168264 20.25016785 -5.12450314 -2.6328646e-05 21.43252563 -5.3150053 0.044161797
		 22.59658813 -5.50550747 0.044158593 22.61488342 -5.50550747 0.088349327 22.63318634 -5.50550747 0.13254006
		 23.79724884 -5.69600964 0.13253626 22.65149689 -5.50550747 0.17673086 27.39926147 -6.26751995 0.39766148
		 27.38095093 -6.26751995 0.35347199 28.56331635 -6.45802402 0.39765576 27.41756439 -6.26751995 0.44185093
		 27.43585968 -6.26751995 0.48604038 28.59992981 -6.45802402 0.4860341 26.27181244 -6.077017784 0.48604679
		 25.10774994 -5.88651371 0.48605314 25.12605286 -5.88651371 0.53024322 23.94368744 -5.69600964 0.48605943
		 11.80512238 -3.60048485 0.94506633 11.816185 -3.60048294 0.94965285 10.67420959 -3.40997982 0.95881182
		 11.79381561 -3.6004467 0.9404816 11.78250122 -3.60040569 0.93589705 10.65211487 -3.40997982 0.94965887
		 12.90190887 -3.78900242 0.922221 14.021324158 -3.97759914 0.90854496 13.99849701 -3.97564793 0.90403003
		 15.14047241 -4.16615009 0.89487112 11.65045166 -3.60048485 0.88099611 11.66150665 -3.60048294 0.88558257
		 10.49743652 -3.40997982 0.88558853 11.6391449 -3.60044861 0.87640977;
	setAttr ".vt[2656:2821]" 11.62784576 -3.60040951 0.87182313 10.47533417 -3.40997982 0.87643564
		 12.76933289 -3.78903961 0.86720908 13.91082001 -3.97767162 0.86259484 13.88800049 -3.97575474 0.85798657
		 15.052062988 -4.16625881 0.85798061 11.49577332 -3.6004858 0.816926 11.50682831 -3.60048294 0.82151216
		 10.32065582 -3.40997982 0.81236517 11.48447418 -3.60045052 0.81233799 11.47317505 -3.60041428 0.80774993
		 10.2985611 -3.40997982 0.80321223 12.6367569 -3.78907585 0.81220073 13.80033112 -3.97773838 0.81665146
		 13.77749634 -3.97585773 0.81195313 14.96366119 -4.1663599 0.82110018 11.34217834 -3.60131359 0.75121123
		 11.35214233 -3.60048294 0.75744182 10.14388275 -3.40997982 0.73914194 11.33218384 -3.60213566 0.74499208
		 11.32184601 -3.60290241 0.73876929 10.12178802 -3.40997982 0.72998905 12.51942444 -3.79542732 0.747527
		 13.71699524 -3.98795223 0.75628024 13.70559692 -3.98985577 0.74674642 14.91385651 -4.18035793 0.76504624
		 20.58726501 -5.12450504 0.72534353 19.42320251 -4.93400288 0.72535121 20.5430603 -5.12450314 0.70703542
		 21.75131989 -5.31500721 0.72533584 22.91538239 -5.50550938 0.72532803 22.95957947 -5.50550938 0.74363661
		 23.0037918091 -5.50551128 0.76194513 24.16784668 -5.69601345 0.76193786 23.047988892 -5.50551128 0.78025359
		 27.92523956 -6.26752186 0.87177235 27.88104248 -6.26752186 0.85346293 29.089302063 -6.45802402 0.87176645
		 27.9694519 -6.26752186 0.8900817 28.013656616 -6.26752186 0.90839118 29.17771149 -6.45802402 0.90838569
		 26.84959412 -6.077021599 0.9083966 25.68553925 -5.88651943 0.90840209 25.72975159 -5.88651943 0.92671096
		 24.52148438 -5.69601727 0.90840751 12.51223755 -3.60048199 0.76203269 12.52329254 -3.60048008 0.75744611
		 11.38131714 -3.40997791 0.74829483 12.50092316 -3.60044384 0.76661742 12.48960114 -3.60040188 0.77120209
		 11.35922241 -3.40997791 0.75744772 13.60902405 -3.7889986 0.78487056 14.7284317 -3.97759342 0.79853898
		 14.70561218 -3.9756422 0.80305421 15.84757996 -4.16614628 0.81220543 12.35755157 -3.60048199 0.82610291
		 12.36860657 -3.60048008 0.82151651 11.20454407 -3.40997791 0.82151818 12.34625244 -3.60044575 0.83068931
		 12.33494568 -3.60040665 0.83527589 11.18244171 -3.40997791 0.83067107 13.47644043 -3.7890358 0.83988249
		 14.61793518 -3.97766399 0.84448928 14.5951004 -3.97574902 0.84909773 15.75917053 -4.16625118 0.84909606
		 12.20287323 -3.60048294 0.89017302 12.21393585 -3.60048008 0.88558686 11.027763367 -3.40997791 0.89474136
		 12.19158173 -3.60044765 0.89476103 12.18029022 -3.60041142 0.89934903 11.0056686401 -3.40997791 0.90389425
		 13.34385681 -3.78907204 0.89489084 14.50743866 -3.97773266 0.8904326 14.48459625 -3.97585201 0.89513087
		 15.67076111 -4.16635418 0.88597631 12.049285889 -3.60131168 0.95588714 12.059249878 -3.60048103 0.94965714
		 10.8509903 -3.40997791 0.96796459 12.039291382 -3.6021328 0.96210569 12.028953552 -3.60290051 0.96832788
		 10.82888794 -3.40997887 0.97711748 13.22652435 -3.79542351 0.95956099 14.42410278 -3.98794842 0.95079857
		 14.41270447 -3.98985004 0.96033072 15.62096405 -4.18035221 0.94202328 21.29445648 -5.12450314 0.98165953
		 20.13038635 -4.93400097 0.98166341 21.2502594 -5.12450314 0.99996257 22.45852661 -5.3150053 0.98165566
		 23.62258911 -5.50550747 0.9816519 23.66678619 -5.50550747 0.96334946 23.71098328 -5.50550938 0.9450472
		 24.87505341 -5.69600964 0.94504392 23.75517273 -5.50550747 0.92674482 28.63241577 -6.26751804 0.83522761
		 28.58821869 -6.26751804 0.85352874 29.7964859 -6.45802212 0.83522618 28.67661285 -6.26751804 0.81692642
		 28.72080231 -6.26751804 0.79862529 29.88487244 -6.45802021 0.79862458 27.55673981 -6.077015877 0.79862601
		 26.39266205 -5.88651371 0.79862666 26.43686676 -5.88651371 0.78032494 25.22859955 -5.69600773 0.79862738
		 12.88221741 -3.60038376 0.13256669 12.88737488 -3.60048008 0.1215346 11.73246765 -3.40997791 0.099436894
		 12.8761673 -3.60014248 0.14359306 12.86975098 -3.5998373 0.15460846 11.72331238 -3.40997791 0.12153398
		 14.010238647 -3.78871918 0.18760465 15.15071869 -3.97759914 0.22060083 15.13465118 -3.97570705 0.23147167
		 16.28955841 -4.16620922 0.25356936 12.81817627 -3.60039043 0.28725863 12.82330322 -3.60048008 0.27621418
		 11.65924072 -3.40997791 0.27621359 12.81216431 -3.60015488 0.29831341 12.80578613 -3.59986115 0.3093648
		 11.65008545 -3.40997791 0.29831064 13.95536804 -3.78874493 0.32045299 15.10494995 -3.97762966 0.33154115
		 15.088859558 -3.97574902 0.34263229 16.25292206 -4.16625118 0.34263289 12.75413513 -3.60039616 0.44194913
		 12.75923157 -3.60048008 0.43089381 11.58602142 -3.40997791 0.45299029 12.74815369 -3.60016727 0.45303077
		 12.74182129 -3.59988308 0.46411568 11.57686615 -3.40997791 0.47508737 13.9005127 -3.78877354 0.45329493
		 15.059196472 -3.97766209 0.44247413 15.04309082 -3.97579479 0.45378271 16.21630859 -4.16629696 0.43168625
		 12.70150757 -3.60253716 0.60059547 12.69515991 -3.60048008 0.58557338 11.51279449 -3.40997791 0.62976694
		 12.69902802 -3.60315323 0.61567247 12.69734192 -3.60389614 0.63071489 11.50363922 -3.40997791 0.65186399
		 13.88271332 -3.79646301 0.60974556 15.068084717 -3.98903561 0.58882964 15.063850403 -3.98991299 0.61198628
		 16.24621582 -4.18041515 0.56779277 21.97566986 -5.12450123 0.66292012 20.81159973 -4.93399906 0.66291922
		 21.95737457 -5.12450123 0.70711446 23.13973236 -5.3150034 0.66292089 24.30381012 -5.50550556 0.66292161
		 24.32209778 -5.50550556 0.61872709 24.3404007 -5.50550556 0.57453251 25.5044632 -5.69600773 0.5745331
		 24.35869598 -5.50550556 0.53033781 29.10643768 -6.26751804 0.30936509 29.088142395 -6.26751804 0.35356003
		 30.27050018 -6.45802212 0.30936521 29.1247406 -6.26751995 0.2651701 29.14303589 -6.26751995 0.22097516
		 30.30709839 -6.45802212 0.22097516 27.97896576 -6.077015877 0.22097507 26.81490326 -5.88651371 0.22097504
		 26.83319855 -5.88651371 0.17678015 25.65084076 -5.69600964 0.22097494 12.69658661 -3.59997177 -0.57464963
		 12.69515991 -3.60048008 -0.58557439 11.52194214 -3.40997791 -0.60766983;
	setAttr ".vt[2822:2987]" 12.69901276 -3.59962368 -0.56364775 12.69998932 -3.59904194 -0.55268657
		 11.53110504 -3.40997791 -0.58557272 13.86967468 -3.78822899 -0.51971877 15.039360046 -3.97741795 -0.48675117
		 15.033943176 -3.97579479 -0.47588283 16.20716095 -4.16629696 -0.45378739 12.76074219 -3.59998322 -0.41990981
		 12.7592392 -3.60048008 -0.43089482 11.59516907 -3.40997791 -0.43089318 12.76317596 -3.59963226 -0.4088676
		 12.76417542 -3.59904766 -0.39783874 11.60432434 -3.40997791 -0.40879607 13.92468262 -3.78822327 -0.386814
		 15.085197449 -3.97739697 -0.37578946 15.079711914 -3.97574902 -0.36473244 16.24377441 -4.16625118 -0.36473408
		 12.8249054 -3.59999371 -0.26517257 12.82330322 -3.60048008 -0.27621523 11.66838837 -3.40997791 -0.25411648
		 12.82733917 -3.59963989 -0.25408924 12.82836914 -3.59905624 -0.24299257 11.67755127 -3.40997791 -0.23201939
		 13.97970581 -3.78821564 -0.25390768 15.13104248 -3.97737789 -0.26482308 15.12550354 -3.97570705 -0.25357181
		 16.28040314 -4.16621113 -0.27567053 12.91666412 -3.60425758 -0.10654578 12.88737488 -3.60048008 -0.12153563
		 11.7416153 -3.40997791 -0.077339798 12.92227173 -3.60414219 -0.091297381 12.92979431 -3.60434723 -0.076158851
		 11.75077057 -3.40997791 -0.05524271 14.10135651 -3.79748631 -0.096919566 15.27292633 -3.99061871 -0.11758391
		 15.27889252 -3.99001026 -0.094206333 16.42464447 -4.18051243 -0.13840216 22.23186493 -5.12450314 -0.044196066
		 21.067802429 -4.93400097 -0.044195827 22.25016785 -5.12450314 -3.6911871e-12 23.39592743 -5.3150053 -0.044196241
		 24.55997467 -5.50550747 -0.044196416 24.54167938 -5.50550747 -0.088392809 24.52338409 -5.50550747 -0.13258916
		 25.68743896 -5.69601154 -0.1325897 24.50508118 -5.50550747 -0.17678553 29.069854736 -6.26752186 -0.39777249
		 29.088150024 -6.26751995 -0.35357553 30.23391724 -6.45802402 -0.39777383 29.051551819 -6.26751995 -0.44196939
		 29.033256531 -6.26752186 -0.48616636 30.19732666 -6.45802402 -0.48616809 27.86919403 -6.077017784 -0.48616448
		 26.70513153 -5.88651562 -0.48616266 26.68682861 -5.88651562 -0.53035915 25.5410614 -5.69601154 -0.48616073
		 11.98285675 -3.60527515 -0.99344528 11.93772888 -3.60048008 -0.99999988 10.78469849 -3.40997791 -0.99542344
		 11.99411774 -3.60462189 -0.9871242 12.0059356689 -3.60407639 -0.98084748 10.80679321 -3.40997791 -0.98627049
		 13.20345306 -3.79791164 -0.97543037 14.40097046 -3.99172878 -0.96996474 14.4127121 -3.98984432 -0.96034199
		 12.049285889 -3.60130978 -0.95589048 10.8509903 -3.40997791 -0.96796459 13.24755096 -3.79263401 -0.94380492
		 14.44581604 -3.98395443 -0.93171477 14.43479156 -3.98592281 -0.94126397 14.42410278 -3.9879446 -0.95080942
		 15.62097168 -4.18034649 -0.9420377 19.22937012 -4.74765491 -0.88328552 19.24253845 -4.74349308 -0.86729348
		 20.39556885 -4.93399525 -0.87186992 19.18732452 -4.74738026 -0.9000023 19.14469147 -4.74699306 -0.91668469
		 17.98013306 -4.55966473 -0.92491704 16.81558228 -4.37235165 -0.93319207 11.27576447 -3.60527992 -0.71365893
		 11.23060608 -3.60048199 -0.70710379 10.077598572 -3.40997982 -0.71168309 11.28701782 -3.6046257 -0.71998048
		 11.29884338 -3.6040802 -0.72625756 10.099693298 -3.40997887 -0.72083604 12.49636841 -3.79791737 -0.73167312
		 13.69390106 -3.99173641 -0.7371369 13.70562744 -3.98985386 -0.7467609 11.34218597 -3.60131264 -0.75121593
		 10.14388275 -3.40997791 -0.73914194 12.54046631 -3.79263783 -0.76330137 13.73874664 -3.98396015 -0.7753911
		 13.72771454 -3.98592854 -0.76584101 13.71702576 -3.98795223 -0.75629449 14.91390228 -4.18035793 -0.76506537
		 18.52232361 -4.74766827 -0.82382023 18.53549957 -4.74350452 -0.83981442 19.68852234 -4.93400669 -0.83523494
		 18.48026276 -4.7473917 -0.80710053 18.43760681 -4.74700069 -0.79041535 17.27306366 -4.55967426 -0.78218412
		 16.10852051 -4.372365 -0.77391022 10.96734619 -3.60426998 -0.015653357 10.93770599 -3.60048008 -2.9254052e-06
		 9.77822876 -3.40997791 -0.011048542 10.97280121 -3.60412884 -0.030882299 10.98015594 -3.60430622 -0.046055876
		 9.7873764 -3.40997791 -0.033145629 12.16562653 -3.79744053 -0.058908731 13.35109711 -3.99056911 -0.071857028
		 13.35671997 -3.98991489 -0.095127836 10.99440002 -3.60253716 -0.10651255 9.80568695 -3.40997791 -0.077339798
		 12.17427826 -3.7936554 -0.13563029 13.35417175 -3.98476887 -0.16480169 13.35796356 -3.98696804 -0.14155966
		 13.36096954 -3.98903751 -0.1182833 14.53909302 -4.18041706 -0.13932219 18.05330658 -4.74590778 -0.28124475
		 18.054801941 -4.74349689 -0.32040891 19.21427917 -4.93400097 -0.30936334 18.039833069 -4.74642658 -0.24107577
		 18.026763916 -4.74700832 -0.20095938 16.86792755 -4.55873394 -0.18071353 15.70909882 -4.37046337 -0.16040404
		 11.23731995 -3.60262489 0.69170779 11.23059845 -3.60048199 0.70709878 10.061965942 -3.40997982 0.69605815
		 11.23468781 -3.6032238 0.67652261 11.23293304 -3.60396385 0.66135085 10.052818298 -3.40997982 0.67396104
		 12.40462494 -3.79656792 0.64879751 13.57631683 -3.98917294 0.63618785 13.57176971 -3.99001026 0.61288285
		 11.20954895 -3.60425854 0.60055655 10.034507751 -3.40997982 0.62976694 12.36090088 -3.79464626 0.57160485
		 13.51226044 -3.98503971 0.54255652 13.53999329 -3.98798847 0.5659768 13.56580353 -3.99061871 0.58950669
		 14.71751404 -4.18051243 0.56868327 18.1062851 -4.7447443 0.42637253 18.082206726 -4.74349689 0.38666099
		 19.25084686 -4.93399906 0.39770162 18.12770844 -4.74553204 0.46646276 18.14993286 -4.74645329 0.50652391
		 17.0061416626 -4.55786991 0.52713686 15.86236572 -4.36928844 0.5477761 11.92780304 -3.60134506 0.99370468
		 11.93771362 -3.60048199 0.99999571 10.76260376 -3.40997982 0.99542344 11.91783142 -3.60219383 0.98742658
		 11.90750885 -3.60298347 0.98114866 10.7405014 -3.40997982 0.98627049 13.071907043 -3.79555988 0.97603345
		 14.23630524 -3.98813915 0.97091323 14.22483063 -3.99008083 0.96124417 11.86122894 -3.60540295 0.95585972
		 10.69631195 -3.40997982 0.96796459 12.99240112 -3.79523945 0.94391841 14.1235733 -3.98509502 0.93192607
		 14.1685791 -3.98860645 0.94167823 14.21300507 -3.99200344 0.95148426 15.34470367 -4.18058491 0.94293231
		 18.64649963 -4.74427891 0.88382834 18.60172272 -4.74350643 0.86724114;
	setAttr ".vt[2988:3153]" 19.77684021 -4.9340086 0.87181354 18.6911087 -4.74501896 0.90043581
		 18.7358551 -4.74577999 0.9170413 17.6054306 -4.55737782 0.92566615 16.47501373 -4.36897564 0.93429244
		 12.63491058 -3.6013422 0.71339822 12.6448288 -3.60048008 0.70710766 11.4697113 -3.40997791 0.71168327
		 12.62493896 -3.60219097 0.71967566 12.61460876 -3.60298157 0.7259528 11.44761658 -3.40997791 0.72083616
		 13.77901459 -3.79555798 0.73106325 14.94340515 -3.98813534 0.73617858 14.93193054 -3.99007702 0.74584585
		 12.56834412 -3.60539913 0.75123954 11.40341187 -3.40997791 0.73914194 13.69950867 -3.79523563 0.76317394
		 14.83067322 -3.9850893 0.77515918 14.87568665 -3.98860264 0.76540852 14.92010498 -3.99199772 0.75560403
		 16.051803589 -4.18057919 0.76415002 19.35359955 -4.74427128 0.82322878 19.30882263 -4.74349689 0.8398124
		 20.48394012 -4.93399906 0.83523679 19.39821625 -4.74501133 0.80662531 19.44296265 -4.74577236 0.79002345
		 18.31254578 -4.55737019 0.7814045 17.18212128 -4.36896801 0.77278399 12.94442749 -3.60262299 0.015391387
		 12.93770599 -3.60048008 -4.118452e-13 11.76907349 -3.40997791 0.011048543 12.94179535 -3.60322189 0.030576903
		 12.94004059 -3.6039629 0.045748997 11.75991821 -3.40997791 0.033145629 14.11173248 -3.7965641 0.058295626
		 15.28343201 -3.98917294 0.070898578 15.27888489 -3.99000835 0.094204716 12.91666412 -3.60425663 0.10654487
		 11.7416153 -3.40997791 0.077339798 14.068023682 -3.79464436 0.13549139 15.21938324 -3.9850359 0.16453457
		 15.24711609 -3.98798656 0.14111303 15.27292633 -3.9906168 0.11758193 16.42463684 -4.18051052 0.13839951
		 19.81343079 -4.74474239 0.28069788 19.78936005 -4.74349499 0.3204121 20.95800018 -4.93399715 0.30936357
		 19.8348465 -4.74553204 0.24060492 19.85707092 -4.74645329 0.200541 18.71327209 -4.55786991 0.17993402
		 17.56948853 -4.36928654 0.15930071 12.67446136 -3.60426903 -0.69145876 12.6448288 -3.60048008 -0.70710963
		 11.4853363 -3.40997791 -0.69605815 12.67990875 -3.60412884 -0.67622936 12.68727875 -3.60430622 -0.66105527
		 11.49449158 -3.40997791 -0.67396104 13.87275696 -3.79744053 -0.64820689 15.058227539 -3.99056911 -0.6352632
		 15.063850403 -3.98991489 -0.61199087 12.70150757 -3.60253811 -0.60059661 11.51279449 -3.40997791 -0.62976694
		 13.88139343 -3.7936554 -0.57148123 15.061279297 -3.98476887 -0.5423122 15.065078735 -3.98696804 -0.56555587
		 15.068084717 -3.98903751 -0.58883381 16.24622345 -4.18041706 -0.56779844 19.76041412 -4.74590588 -0.42587838
		 19.76191711 -4.74349689 -0.38671058 20.92140198 -4.93400097 -0.39776206 19.74695587 -4.74642658 -0.46605101
		 19.73389435 -4.74700832 -0.50617099 18.57506561 -4.55873394 -0.52641368 17.41622162 -4.37046337 -0.54671991
		 12.10122681 -3.59484673 -0.91801971 10.93937683 -3.40997791 -0.93135309 10.96147156 -3.40997791 -0.92220002
		 12.12406158 -3.59672451 -0.91331178 12.14664459 -3.59856987 -0.90862513 10.98357391 -3.40997791 -0.91304713
		 13.2984314 -3.78534794 -0.90429193 14.45960236 -3.97361279 -0.89983088 14.48459625 -3.97585011 -0.89514047
		 12.20287323 -3.60048199 -0.89017612 11.027763367 -3.40997791 -0.89474136 13.37774658 -3.79094887 -0.88561285
		 14.55261993 -3.98141766 -0.88104945 14.5300293 -3.97957516 -0.88574588 14.50743866 -3.97773266 -0.89044237
		 15.67076874 -4.16635227 -0.88598919 19.23813629 -4.74101353 -0.86284012 19.26463318 -4.74349308 -0.85814053
		 18.042419434 -4.55075359 -0.87655914 16.84672546 -4.36050129 -0.89029443 16.84446716 -4.35837269 -0.88584197
		 16.84156799 -4.35613537 -0.88138026 12.25592041 -3.59474373 -0.85368043 11.11615753 -3.40997791 -0.85812968
		 11.13825226 -3.40997791 -0.84897685 12.27874756 -3.59665489 -0.84906209 12.30133057 -3.59853268 -0.84446388
		 11.16034698 -3.40997791 -0.83982396 13.43103027 -3.78524494 -0.84910548 14.57007599 -3.97346401 -0.85369003
		 14.5951004 -3.97574711 -0.84910703 12.35755157 -3.60048199 -0.82610601 11.20454407 -3.40997791 -0.82151806
		 13.51032257 -3.79094696 -0.83069408 14.66308594 -3.98141193 -0.83528191 14.64051056 -3.97953701 -0.83989036
		 14.61793518 -3.97766399 -0.84449875 15.75917053 -4.16624928 -0.84910858 19.26017761 -4.74095631 -0.85356832
		 19.28673553 -4.74349308 -0.84898752 18.086585999 -4.55070591 -0.85814601 16.91301727 -4.36046505 -0.86274081
		 16.91073608 -4.35829067 -0.8581863 16.9078064 -4.35600758 -0.85361731 12.41062164 -3.59463787 -0.7893312
		 11.29292297 -3.40997791 -0.78490651 11.31503296 -3.40997791 -0.7757535 12.43344879 -3.59658432 -0.78480577
		 12.45600128 -3.59849548 -0.78029895 11.33712769 -3.40997791 -0.76660055 13.56363678 -3.78514004 -0.79390919
		 14.68055725 -3.97330761 -0.80753458 14.70561218 -3.9756403 -0.80306351 12.51223755 -3.60048199 -0.76203597
		 11.38131714 -3.40997791 -0.74829483 13.64289856 -3.79094505 -0.77577507 14.77355957 -3.98140621 -0.78951401
		 14.75099182 -3.97949886 -0.79403126 14.7284317 -3.97759151 -0.79854846 15.84757996 -4.16614246 -0.81221807
		 19.28221893 -4.74089909 -0.84429067 19.30883026 -4.74349308 -0.83983469 18.13075256 -4.55065823 -0.83972824
		 16.97930908 -4.360425 -0.83518344 16.977005 -4.35820675 -0.83052266 16.9740448 -4.35587406 -0.82584178
		 12.63491058 -3.60134315 -0.71340042 11.4697113 -3.40997791 -0.71168309 13.80005646 -3.79269409 -0.71510446
		 14.96520233 -3.98404408 -0.71680373 14.95413208 -3.98606014 -0.72649473 14.94340515 -3.98813534 -0.73618603
		 16.10705566 -4.18057919 -0.74128026 19.61585236 -4.74774075 -0.72341228 19.57390594 -4.74747372 -0.74011195
		 19.53135681 -4.74709034 -0.75676835 18.39984131 -4.55983448 -0.75149632 17.26832581 -4.37259579 -0.74626893
		 17.22692871 -4.37108135 -0.75958771 19.35359955 -4.74426746 -0.82325017 20.48394775 -4.93399525 -0.83526117
		 18.22309875 -4.55450535 -0.81121922 17.092597961 -4.36474323 -0.79918975 17.13742828 -4.36686802 -0.78599471
		 17.18212128 -4.36896801 -0.772798 28.89758301 -6.26751995 -0.72545373 28.94178009 -6.26751995 -0.70715064
		 30.061668396 -6.45802402 -0.72545648 28.85340118 -6.26751995 -0.74375689 28.80921936 -6.26751804 -0.76205987
		 29.97329712 -6.45802402 -0.76206219 27.64513397 -6.077015877 -0.76205766;
	setAttr ".vt[3154:3319]" 26.48106384 -5.8865118 -0.7620554 26.43688202 -5.8865118 -0.78035891
		 28.63248444 -6.26751995 -0.83527231 29.79655457 -6.45802402 -0.83527386 27.46839905 -6.077015877 -0.83527064
		 26.30432892 -5.88651371 -0.83526915 26.34851074 -5.88651371 -0.8169657 26.3927002 -5.88651371 -0.79866236
		 25.22862244 -5.69600773 -0.79866034 21.64801025 -5.12449551 -0.83526278 21.60382843 -5.12449741 -0.853567
		 21.69220734 -5.12449741 -0.81695861 21.73639679 -5.12449932 -0.79865426 22.90047455 -5.31500149 -0.7986564
		 24.064537048 -5.50550365 -0.79865843 28.27900696 -6.26751995 -0.98169672 29.44308472 -6.45802402 -0.98169696
		 28.23482513 -6.26751995 -0.99999988 27.1149292 -6.077017784 -0.98169667 25.95085144 -5.88651371 -0.98169643
		 25.99503326 -5.88651371 -0.96339297 26.039222717 -5.88651371 -0.94508964 24.87513733 -5.69600964 -0.9450891
		 21.29454041 -5.12450314 -0.98169577 21.33872223 -5.12450123 -0.96339166 21.38291168 -5.12450123 -0.94508761
		 22.54698181 -5.3150034 -0.94508803 23.7110672 -5.50550747 -0.94508845 23.75524902 -5.50550556 -0.92678475
		 21.55963898 -5.12449932 -0.87187111 22.72372437 -5.31500149 -0.87187231 23.88780212 -5.50550365 -0.87187344
		 23.84361267 -5.50550556 -0.89017731 23.79943085 -5.50550747 -0.908481 11.39411926 -3.59484863 -0.78908718
		 10.23226929 -3.40997791 -0.7757535 10.25436401 -3.40997791 -0.78490651 11.41696167 -3.59672642 -0.79379511
		 11.43954468 -3.59857178 -0.79848152 10.27645874 -3.40997791 -0.7940594 12.59134674 -3.78535175 -0.80281514
		 13.75252533 -3.97361851 -0.80727631 13.77751923 -3.97585583 -0.81196666 11.49577332 -3.60048294 -0.81693065
		 10.32065582 -3.40997791 -0.81236517 12.67066193 -3.79095268 -0.8214941 13.84554291 -3.98142147 -0.82605767
		 13.82294464 -3.97957897 -0.82136124 13.800354 -3.97773647 -0.81666487 14.96369934 -4.1663599 -0.82111824
		 18.53110504 -4.74102497 -0.84426767 18.5575943 -4.74350452 -0.84896725 17.33538055 -4.55076313 -0.83054847
		 16.13967133 -4.36050892 -0.81681281 16.1374054 -4.35838223 -0.82126552 16.1344986 -4.35614491 -0.82572711
		 11.5488205 -3.59474564 -0.85342622 10.40904999 -3.40997791 -0.84897685 10.43114471 -3.40997791 -0.85812968
		 11.57165527 -3.5966568 -0.85804462 11.59422302 -3.59853458 -0.86264271 10.45323944 -3.40997791 -0.86728263
		 12.72395325 -3.78524876 -0.85800141 13.86299896 -3.97346783 -0.85341692 13.88802338 -3.97575283 -0.85800004
		 11.65045929 -3.60048389 -0.88100076 10.49743652 -3.40997791 -0.88558853 12.80323792 -3.79095078 -0.87641299
		 13.95601654 -3.98141766 -0.87182534 13.9334259 -3.97954273 -0.86721683 13.91085815 -3.97766972 -0.86260837
		 15.052108765 -4.166255 -0.85799873 18.55313873 -4.74096775 -0.85353941 18.57969666 -4.74350452 -0.8581202
		 17.37954712 -4.55071545 -0.84896165 16.20596313 -4.36047459 -0.84436661 16.20368195 -4.35830021 -0.84892118
		 16.20075226 -4.35601521 -0.85349023 11.70352173 -3.59463882 -0.91777557 10.58582306 -3.40997791 -0.92220002
		 10.60791779 -3.40997791 -0.93135309 11.72634125 -3.59658527 -0.92230088 11.74890137 -3.59849644 -0.92680776
		 10.63002014 -3.40997791 -0.94050598 12.85655212 -3.78514194 -0.91319776 13.97348022 -3.97331333 -0.89957225
		 13.99853516 -3.97564602 -0.90404344 11.80513763 -3.60048294 -0.9450708 10.67420959 -3.40997791 -0.95881182
		 12.93580627 -3.79094887 -0.93133187 14.066482544 -3.98141193 -0.91759306 14.043914795 -3.97950459 -0.91307575
		 14.021362305 -3.97759724 -0.90855861 15.14051056 -4.16615009 -0.89488918 18.57518768 -4.74091053 -0.86281693
		 18.60179138 -4.74350452 -0.86727309 17.42370605 -4.55066776 -0.86737943 16.27225494 -4.36043262 -0.87192398
		 16.26995087 -4.35821438 -0.87658477 16.26699066 -4.35588169 -0.88126558 11.9278183 -3.60134315 -0.99370873
		 10.76260376 -3.40997791 -0.99542344 13.092971802 -3.79269409 -0.9920072 14.25812531 -3.98404408 -0.99031073
		 14.24705505 -3.98606014 -0.98061866 14.23633575 -3.98813725 -0.97092634 15.39998627 -4.180583 -0.96583384
		 18.90882111 -4.74774456 -0.98371261 18.86686707 -4.74747753 -0.96701044 18.82431793 -4.74709606 -0.95035154
		 17.69280243 -4.55983829 -0.95562172 16.56127167 -4.3725996 -0.9608472 16.51987457 -4.37108517 -0.94752681
		 18.6465683 -4.744277 -0.88385999 19.7769165 -4.93400669 -0.87184954 17.51605988 -4.55451488 -0.89589041
		 16.38553619 -4.36475086 -0.90791947 16.43037415 -4.36687374 -0.92111623 16.47506714 -4.36897373 -0.93431485
		 28.19062805 -6.26752186 -0.98169148 29.35469818 -6.45802402 -0.98169118 28.14642334 -6.26752186 -0.96338296
		 28.10221863 -6.26752186 -0.94507438 29.26628876 -6.45802402 -0.9450736 26.9381485 -6.077021599 -0.94507504
		 25.774086 -5.88651943 -0.94507563 25.72988129 -5.88651943 -0.92676747 27.92539978 -6.26752186 -0.87183994
		 29.08946228 -6.45802402 -0.87183869 26.76132965 -6.077019691 -0.87184143 25.59725952 -5.88651752 -0.87184274
		 25.64147186 -5.88651943 -0.89015102 25.68567657 -5.88651943 -0.90845931 24.52160645 -5.69601727 -0.90846026
		 20.94100189 -5.12451077 -0.87184829 20.89678955 -5.12451077 -0.85354078 20.98519135 -5.12450886 -0.89015561
		 21.029380798 -5.12450886 -0.90846294 22.19345856 -5.31501102 -0.90846223 23.35753632 -5.5055151 -0.90846133
		 27.57175446 -6.26752186 -0.72537142 28.73582458 -6.45802402 -0.72536862 27.52755737 -6.26752186 -0.70706284
		 26.40769196 -6.077019691 -0.72537422 25.24362183 -5.88651562 -0.72537696 25.28781891 -5.88651562 -0.74368513
		 25.33203125 -5.88651562 -0.76199335 24.16796112 -5.69601345 -0.76199579 20.58735657 -5.12450504 -0.72538793
		 20.63156128 -5.12450504 -0.74369544 20.67576599 -5.12450695 -0.76200312 21.83983612 -5.31500912 -0.76200068
		 23.0038986206 -5.50551128 -0.76199824 23.048103333 -5.50551128 -0.78030604 20.85258484 -5.12450886 -0.83523333
		 22.016654968 -5.31501102 -0.83523154 23.18071747 -5.50551319 -0.83522981 23.13651276 -5.50551128 -0.81692189
		 23.092308044 -5.50551128 -0.79861408 10.98498535 -3.59478951 -0.19808349 9.84230042 -3.40997791 -0.16572814
		 9.85145569 -3.40997791 -0.18782522 11.0012664795 -3.59668636 -0.2093955 11.015617371 -3.59827137 -0.22066768
		 9.86061096 -3.40997791 -0.20992231 12.16278076 -3.78529167 -0.23122923;
	setAttr ".vt[3320:3485]" 13.31520081 -3.97316265 -0.24196061 13.33599091 -3.97579479 -0.25332645
		 11.047035217 -3.60039616 -0.26515847 9.87891388 -3.40997791 -0.25411648 12.21426392 -3.79067135 -0.27617401
		 13.38148499 -3.98094463 -0.2871896 13.36662292 -3.9792757 -0.27591068 13.35209656 -3.97766399 -0.26463509
		 14.50920868 -4.16629696 -0.27542371 18.039749146 -4.74031162 -0.33108011 18.063949585 -4.74349689 -0.34250605
		 16.8653717 -4.55036259 -0.29799387 15.69130707 -4.36046696 -0.2648958 15.68045044 -4.35796261 -0.27563313
		 15.66881561 -4.35533237 -0.28636339 11.049064636 -3.59474564 -0.35342786 9.91551971 -3.40997791 -0.3425048
		 9.92467499 -3.40997791 -0.36460188 11.065338135 -3.5966568 -0.36451834 11.079627991 -3.59824276 -0.37560666
		 9.93382263 -3.40997791 -0.38669896 12.21769714 -3.78524685 -0.36447662 13.36093903 -3.97309208 -0.35341662
		 13.38175964 -3.97574902 -0.3644768 11.11106873 -3.60039043 -0.41984895 9.95213318 -3.40997791 -0.43089318
		 12.26911926 -3.790658 -0.40879425 13.4271698 -3.98092556 -0.39773962 13.412323 -3.97924709 -0.38665551
		 13.39785004 -3.97762966 -0.37556797 14.54582214 -4.16625118 -0.36447695 18.048843384 -4.74027729 -0.35354701
		 18.073104858 -4.74349689 -0.3646031 16.88365173 -4.55033588 -0.34249377 15.71877289 -4.3604517 -0.33142298
		 15.70788574 -4.35792065 -0.34245169 15.6962204 -4.35526371 -0.3534877 11.11315155 -3.59470272 -0.50878274
		 9.98874664 -3.40997791 -0.51928151 9.99790192 -3.40997791 -0.54137862 11.12941742 -3.59662914 -0.51964808
		 11.14363861 -3.59821701 -0.53055209 10.0070571899 -3.40997791 -0.56347573 12.27263641 -3.78520584 -0.4977344
		 13.40670013 -3.97302532 -0.46488881 13.42753601 -3.97570705 -0.4756375 11.17510223 -3.60038471 -0.57454079
		 10.025360107 -3.40997791 -0.60766983 12.32397461 -3.79064655 -0.54141748 13.47283936 -3.98090649 -0.50829417
		 13.45803833 -3.9792223 -0.49740672 13.44361115 -3.97759914 -0.48650828 14.58245087 -4.16621113 -0.45354053
		 18.057952881 -4.74024487 -0.37602177 18.082260132 -4.7434988 -0.38670021 16.90193939 -4.550313 -0.3869994
		 15.74624634 -4.36043835 -0.39795345 15.73532867 -4.35788441 -0.40927956 15.72364044 -4.35520267 -0.42062703
		 11.23732758 -3.60262489 -0.69171292 10.061965942 -3.40997982 -0.69605815 12.4033432 -3.79372692 -0.68757349
		 13.56935883 -3.98482609 -0.68349063 13.57328033 -3.98707104 -0.65985376 13.57633972 -3.98917294 -0.63620383
		 14.74044037 -4.18051243 -0.62394464 18.21315002 -4.74587917 -0.66749573 18.19979858 -4.74642277 -0.62723613
		 18.18688202 -4.74703884 -0.5870167 17.041603088 -4.55876255 -0.59952223 15.89631653 -4.37048626 -0.61196423
		 15.88619232 -4.3710146 -0.57975066 18.1063385 -4.7447443 -0.42641133 19.2509079 -4.93400097 -0.39774585
		 16.9591217 -4.55502987 -0.45545539 15.81189728 -4.36531734 -0.4844732 15.8375473 -4.36736774 -0.51612359
		 15.86240387 -4.36928844 -0.54780328 27.5092392 -6.26752186 -0.66287434 28.6733017 -6.45802402 -0.6628716
		 27.49092102 -6.26752186 -0.6186856 27.47260284 -6.26752186 -0.57449687 28.63667297 -6.45802402 -0.57449496
		 26.30854034 -6.077019691 -0.57449895 25.14447021 -5.88651562 -0.57450086 25.12615967 -5.88651562 -0.53031158
		 27.3993454 -6.26751995 -0.39774227 28.5634079 -6.45802402 -0.39774168 26.2352829 -6.077017784 -0.39774281
		 25.071220398 -5.88651562 -0.39774334 25.089530945 -5.88651562 -0.44193277 25.10784912 -5.88651562 -0.48612219
		 23.94378662 -5.69601154 -0.48612341 20.4149704 -5.12450314 -0.39774534 20.39665222 -5.12450314 -0.35355461
		 20.43328094 -5.12450314 -0.44193614 20.45159149 -5.12450314 -0.48612693 21.61565399 -5.3150053 -0.48612574
		 22.77971649 -5.50550938 -0.4861246 27.25282288 -6.26751995 -0.044232678 28.41688538 -6.45802402 -0.044235125
		 27.23451233 -6.26751995 -4.3881075e-05 26.088768005 -6.077017784 -0.044230163 24.92470551 -5.88651562 -0.044227645
		 24.94302368 -5.88651562 -0.088417202 24.96133423 -5.88651371 -0.13260674 23.79727173 -5.69601154 -0.13260502
		 20.26847839 -5.12450314 -0.044217419 20.28678894 -5.12450314 -0.08840847 20.30510712 -5.12450314 -0.13259953
		 21.46915436 -5.3150053 -0.13260137 22.63321686 -5.50550747 -0.13260321 22.65151978 -5.50550747 -0.1767935
		 20.37834167 -5.12450314 -0.30936363 21.54240417 -5.3150053 -0.3093639 22.70646667 -5.50550747 -0.30936411
		 22.68815613 -5.50550747 -0.26517391 22.66983795 -5.50550747 -0.22098373 11.11314392 -3.59470367 0.50877726
		 9.99790192 -3.40997982 0.54137862 9.98874664 -3.40997982 0.51928151 11.12026215 -3.59662914 0.49754542
		 11.12226105 -3.59771538 0.4863421 9.97959137 -3.40997982 0.49718443 12.26346588 -3.78520584 0.4756262
		 13.40667725 -3.97302532 0.46487218 13.41836548 -3.97570705 0.45352376 11.11779022 -3.59999561 0.44193041
		 9.96128845 -3.40997982 0.45299023 12.2751236 -3.79014397 0.43091124 13.43245697 -3.98029232 0.41989234
		 13.4274826 -3.97872066 0.43108904 13.42391205 -3.97737789 0.44227239 14.57327271 -4.16621113 0.43142122
		 18.057907104 -4.74024296 0.37598267 18.073059082 -4.74349689 0.36456388 16.91104126 -4.550313 0.40906295
		 15.76494598 -4.36050701 0.44212204 15.74445343 -4.35788441 0.43134874 15.72360992 -4.35520077 0.42059922
		 11.049057007 -3.59474659 0.35342249 9.92467499 -3.40997982 0.36460188 9.91551971 -3.40997982 0.3425048
		 11.056175232 -3.59665775 0.34241578 11.05809021 -3.5977211 0.33133867 9.90637207 -3.40997887 0.32040775
		 12.20853424 -3.78524876 0.34236854 13.36091614 -3.97309399 0.35340005 13.37258911 -3.97574902 0.34236306
		 11.053627014 -3.59998417 0.28719312 9.88806152 -3.40997791 0.27621359 12.22010803 -3.79013443 0.29822996
		 13.38659668 -3.9802866 0.30926704 13.38162231 -3.97872639 0.32027978 13.37806702 -3.97739697 0.33130595
		 14.53663635 -4.16625118 0.34235764 18.048805237 -4.74027538 0.35350791 18.063911438 -4.74349499 0.34246683
		 16.89276886 -4.55033588 0.36455733 15.73749542 -4.36052227 0.37559953 15.7170105 -4.35792065 0.36452091
		 15.69618988 -4.35526371 0.35345989 10.98498535 -3.59478951 0.19807807 9.85145569 -3.40997791 0.18782522
		 9.84230042 -3.40997791 0.16572814 10.99210358 -3.59668636 0.1872929;
	setAttr ".vt[3486:3651]" 10.993927 -3.59772587 0.17633684 9.83314514 -3.40997791 0.14363104
		 12.15361023 -3.78529167 0.20912111 13.31517792 -3.97316265 0.24194407 13.32682037 -3.97579479 0.23121274
		 10.98946381 -3.59997177 0.13245331 9.81484222 -3.40997791 0.099436879 12.16510773 -3.7901268 0.16554685
		 13.34074402 -3.98028088 0.19864064 13.33576202 -3.97873211 0.20947212 13.33222961 -3.97741604 0.22034431
		 14.50002289 -4.16629696 0.25330439 18.039703369 -4.74030972 0.33104095 18.054756165 -4.74349499 0.32036972
		 16.8744812 -4.55036068 0.32005742 15.71005249 -4.36054134 0.30908096 15.6895752 -4.3579607 0.29770231
		 15.66877747 -4.35533047 0.28633553 10.96734619 -3.60426998 0.015647355 9.77822876 -3.40997791 0.011048542
		 12.13227844 -3.794631 0.019824844 13.29720306 -3.98499966 0.023906605 13.32510376 -3.9879427 0.047845375
		 13.35108948 -3.99056911 0.071839213 14.5161972 -4.18041515 0.084058665 17.945961 -4.74470425 0.03992115
		 17.96739197 -4.74548244 0.080145717 17.98959351 -4.74639034 0.12036481 16.83210754 -4.5577898 0.1081721
		 15.67461395 -4.36918736 0.096004002 15.69855499 -4.37091923 0.12824741 18.053253174 -4.74590397 0.28120509
		 19.2142334 -4.93399906 0.30931824 16.88031769 -4.55592442 0.25208738 15.70737457 -4.36593533 0.22303276
		 15.7084198 -4.36822987 0.19173042 15.7090683 -4.37046337 0.16037495 27.25281525 -6.26751995 0.044145655
		 28.41687012 -6.45802402 0.044142365 27.27112579 -6.26751995 0.088335156 27.28942108 -6.26751995 0.13252465
		 28.45348358 -6.45802402 0.13252072 26.12536621 -6.077017784 0.13252854 24.96130371 -5.88651371 0.13253242
		 24.97960663 -5.88651371 0.17672253 27.36264801 -6.26751995 0.30928245 28.52670288 -6.45802402 0.30927742
		 26.19858551 -6.077017784 0.30928764 25.03452301 -5.88651371 0.30929279 25.016220093 -5.88651371 0.26510268
		 24.99791718 -5.88651371 0.22091256 23.83385468 -5.69600964 0.22091711 20.3782959 -5.12450123 0.30931324
		 20.39659882 -5.12450123 0.35350454 20.35998535 -5.12450123 0.26512188 20.34169006 -5.12450123 0.22093056
		 21.50574493 -5.3150053 0.22092608 22.6697998 -5.50550747 0.22092162 27.50909424 -6.26751995 0.6627984
		 28.67314911 -6.45802402 0.66279083 27.52740479 -6.26752186 0.70698792 26.34503937 -6.077019691 0.66280603
		 25.18096924 -5.88651371 0.66281354 25.16267395 -5.88651562 0.6186235 25.1443634 -5.88651562 0.57443339
		 23.9803009 -5.69600964 0.5744403 20.52474976 -5.12450314 0.66284412 20.50643921 -5.12450314 0.6186527
		 20.48813629 -5.12450314 0.57446128 21.65219116 -5.3150053 0.57445425 22.81624603 -5.50550747 0.57444739
		 22.79794312 -5.50550747 0.53025651 20.41490173 -5.12450123 0.39769578 21.57896423 -5.3150053 0.39769009
		 22.7430191 -5.50550747 0.39768431 22.76132202 -5.50550747 0.44187504 22.77963257 -5.50550747 0.48606572
		 11.7035141 -3.59464073 0.91777116 10.60791779 -3.40997982 0.93135309 10.58582306 -3.40997982 0.92220002
		 11.70423889 -3.59658718 0.91314346 11.69567871 -3.59700966 0.90853119 10.56372833 -3.40997982 0.91304713
		 12.83443451 -3.78514385 0.90403581 13.97344208 -3.97331333 0.89955872 13.97640228 -3.97564793 0.8948769
		 11.66454315 -3.59917259 0.89016163 10.51953888 -3.40997982 0.89474136 12.81628418 -3.78946018 0.8855921
		 13.96802521 -3.97974873 0.88102269 13.96857452 -3.97801685 0.8856374 13.97019196 -3.97645855 0.89024758
		 15.11837769 -4.16615009 0.88571823 18.57511139 -4.74091244 0.86278504 18.57962799 -4.74350643 0.85808825
		 17.44574738 -4.55066776 0.87650496 16.31702423 -4.36053753 0.89020467 16.29199982 -4.35821629 0.88571495
		 16.26693726 -4.3558836 0.88124293 11.54881287 -3.59474754 0.85342187 10.43114471 -3.40997982 0.85812968
		 10.40904999 -3.40997982 0.84897685 11.54954529 -3.59665871 0.84888732 11.54094696 -3.59706497 0.84429544
		 10.38694763 -3.40997982 0.83982396 12.701828 -3.78525066 0.84883964 13.86296844 -3.97347164 0.85340357
		 13.86589813 -3.97575474 0.84883368 11.50979614 -3.59918499 0.826029 10.34275818 -3.40997982 0.82151806
		 12.68365479 -3.78948116 0.83060211 13.85749817 -3.97977543 0.83517528 13.85803223 -3.97807217 0.83970737
		 13.85959625 -3.97653675 0.84424317 15.029968262 -4.16625881 0.84882772 18.55307007 -4.74096966 0.85350764
		 18.55752563 -4.74350643 0.84893537 17.40157318 -4.55071735 0.85808724 16.25071716 -4.36057568 0.86265224
		 16.22572327 -4.35830021 0.85805136 16.20069122 -4.35601902 0.85346764 11.39411163 -3.59485054 0.78908277
		 10.25436401 -3.40997982 0.78490651 10.23226929 -3.40997982 0.7757535 11.39485931 -3.59672832 0.78463769
		 11.38619995 -3.59711933 0.78006482 10.21017456 -3.40997982 0.76660055 12.56922913 -3.78535366 0.79365325
		 13.75249481 -3.97362041 0.8072629 13.75539398 -3.97585773 0.8028003 11.35504913 -3.59919834 0.76189756
		 10.16597748 -3.40997982 0.74829483 12.55101776 -3.78950024 0.77561402 13.74698639 -3.97980404 0.78933066
		 13.74748993 -3.97812748 0.79378253 13.74901581 -3.97661304 0.79824615 14.94155884 -4.1663599 0.81194717
		 18.53103638 -4.74102688 0.84423578 18.53543091 -4.74350643 0.83978242 17.35741425 -4.55076504 0.83967417
		 16.18443298 -4.36061001 0.83510333 16.15946198 -4.35838413 0.83039576 16.13446045 -4.35614681 0.82570457
		 11.27574921 -3.60528088 0.7136538 10.077598572 -3.40997982 0.71168309 12.44002533 -3.79512882 0.71539104
		 13.60428619 -3.98499584 0.71708006 13.64935303 -3.98842144 0.72707886 13.69386292 -3.99173641 0.73712212
		 14.85861206 -4.18035793 0.74216199 18.25971985 -4.7442503 0.72369421 18.30433655 -4.74496937 0.74035305
		 18.34908295 -4.74571323 0.75701195 17.18556213 -4.55725574 0.7520566 16.022033691 -4.36880207 0.74710262
		 16.066871643 -4.3708601 0.76046193 18.52225494 -4.74766827 0.82378775 19.68843842 -4.9340086 0.83519804
		 17.32718658 -4.55688953 0.81165195 16.13211823 -4.36609173 0.79955882 16.11999512 -4.369174 0.78673995
		 16.1084671 -4.372365 0.77388638 27.5716095 -6.26751995 0.72529721 28.73566437 -6.45802402 0.72528958
		 27.61581421 -6.26752186 0.74360669 27.66001892 -6.26752186 0.76191616;
	setAttr ".vt[3652:3817]" 28.82407379 -6.45802402 0.76190877 26.49595642 -6.077019691 0.76192337
		 25.33190155 -5.88651752 0.76193064 25.37611389 -5.88651752 0.78023958 27.83683777 -6.26752186 0.83515364
		 29.00089263916 -6.45802402 0.83514726 26.67277527 -6.077019691 0.83515996 25.5087204 -5.88651752 0.83516639
		 25.46451569 -5.88651752 0.81685746 25.42031097 -5.88651752 0.79854852 24.2562561 -5.69601345 0.79855531
		 20.85249329 -5.12451077 0.83519167 20.89670563 -5.12451077 0.85349977 20.80828857 -5.12450886 0.81688368
		 20.76408386 -5.12450886 0.79857564 21.92814636 -5.31500912 0.79856884 23.092193604 -5.50551128 0.79856217
		 28.19047546 -6.26752186 0.9816286 29.35453033 -6.45802402 0.98162395 28.23466492 -6.26752186 0.99993777
		 27.026405334 -6.077019691 0.98163289 25.86234283 -5.88651752 0.9816373 25.81814575 -5.88651752 0.96332848
		 25.7739563 -5.88652134 0.94501984 24.60987854 -5.69601536 0.94502461 21.20606232 -5.12450504 0.98165482
		 21.16187286 -5.12450695 0.96334696 21.11768341 -5.12450695 0.94503909 22.28174591 -5.31500912 0.94503427
		 23.44581604 -5.50551319 0.94502944 23.40161896 -5.5055151 0.92672116 20.94090271 -5.12451077 0.87180775
		 22.10496521 -5.31501293 0.87180185 23.26901245 -5.5055151 0.87179589 23.31322479 -5.5055151 0.89010447
		 23.3574295 -5.5055151 0.90841299 12.41062164 -3.59463787 0.78932816 11.31503296 -3.40997791 0.77575356
		 11.29292297 -3.40997791 0.78490651 12.41133881 -3.59658432 0.79395568 12.40279388 -3.5970068 0.79856795
		 11.27082825 -3.40997791 0.7940594 13.54154205 -3.78514004 0.80305588 14.68054962 -3.97330952 0.80752528
		 14.68350983 -3.9756422 0.8122071 12.37165833 -3.59916878 0.81693745 11.22663879 -3.40997791 0.81236517
		 13.52339172 -3.78945637 0.82149929 14.67512512 -3.979743 0.82606119 14.67568207 -3.97801304 0.82144666
		 14.6772995 -3.97645473 0.81683642 15.82548523 -4.16614628 0.82135838 19.28221893 -4.7409029 0.84426856
		 19.28672791 -4.74349689 0.84896529 18.15284729 -4.55066013 0.83055627 17.024124146 -4.36053181 0.81686407
		 16.99909973 -4.35821056 0.82135391 16.9740448 -4.35587597 0.82582593 12.25592041 -3.59474468 0.85367745
		 11.13825226 -3.40997791 0.84897685 11.11615753 -3.40997791 0.85812968 12.25665283 -3.59665585 0.85821193
		 12.24804688 -3.59706211 0.86280376 11.094062805 -3.40997791 0.86728263 13.40893555 -3.78524685 0.85825223
		 14.57008362 -3.97346401 0.85368085 14.57300568 -3.97574902 0.85825062 12.21690369 -3.59918213 0.88107008
		 11.049858093 -3.40997791 0.88558853 13.3907547 -3.78947735 0.87648934 14.56460571 -3.97977161 0.87190861
		 14.56513214 -3.97806644 0.86737669 14.56671143 -3.97653103 0.86284095 15.73706818 -4.16625118 0.85824895
		 19.26016998 -4.74096012 0.85354584 19.26463318 -4.74349689 0.85811812 18.10868073 -4.55070782 0.84897399
		 16.95783234 -4.36056805 0.84441644 16.93283081 -4.35829258 0.8490175 16.90779877 -4.35600948 0.85360146
		 12.10121918 -3.59484768 0.91801655 10.96147156 -3.40997791 0.92220002 10.93937683 -3.40997791 0.93135309
		 12.10195923 -3.59672546 0.92246145 12.093315125 -3.59711647 0.92703438 10.9172821 -3.40997791 0.94050598
		 13.27632904 -3.78534985 0.91343838 14.45960236 -3.97361469 0.8998214 14.46250153 -3.97585201 0.90428388
		 12.062156677 -3.59919643 0.9452014 10.87308502 -3.40997791 0.95881182 13.25812531 -3.78949833 0.93147743
		 14.4540863 -3.97979832 0.91775334 14.45458984 -3.97812176 0.91330147 14.45611572 -3.97660923 0.90883785
		 15.64866638 -4.16635418 0.89512932 19.23813629 -4.74101734 0.86281794 19.24253082 -4.74349689 0.86727118
		 18.06451416 -4.5507555 0.86738706 16.8915329 -4.36060238 0.87196553 16.86655426 -4.3583765 0.87667316
		 16.84155273 -4.35613918 0.88136429 11.98285675 -3.60527897 0.99344122 10.78469849 -3.40997982 0.99542344
		 13.1471405 -3.79512691 0.99169236 14.31141663 -3.98499393 0.98999202 14.3564682 -3.98841763 0.97999489
		 14.40097046 -3.9917326 0.96995312 15.5657196 -4.18035221 0.964903 18.96688843 -4.74424839 0.98333198
		 19.011489868 -4.74496746 0.96667695 19.056236267 -4.74570942 0.95002186 17.8927002 -4.55725193 0.95498765
		 16.72916412 -4.36879635 0.95995194 16.77398682 -4.37085438 0.94659555 19.22936249 -4.74765873 0.8832621
		 20.39554596 -4.93399906 0.8718434 18.034286499 -4.5568819 0.89540577 16.83921814 -4.36608601 0.90750718
		 16.82709503 -4.36916637 0.92032307 16.81556702 -4.37235737 0.93317372 28.278862 -6.26752186 0.98163664
		 29.44293213 -6.45802402 0.98163277 28.32305908 -6.26751995 0.96333545 28.36724854 -6.26752186 0.94503438
		 29.53131866 -6.45802212 0.94503117 27.20318604 -6.077017784 0.94503748 26.039115906 -5.88651562 0.94504082
		 26.083312988 -5.88651371 0.92673898 28.54402924 -6.26751804 0.87182999 29.70809174 -6.45802212 0.8718279
		 27.37995911 -6.077015877 0.87183177 26.21589661 -5.88651371 0.87183374 26.17169952 -5.88651371 0.89013553
		 26.12751007 -5.88651371 0.90843737 24.96343231 -5.69600964 0.90843982 21.55962372 -5.12450123 0.87184143
		 21.60381317 -5.12450123 0.85353851 21.51542664 -5.12450314 0.89014447 21.47123718 -5.12450314 0.90844744
		 22.63529968 -5.3150053 0.90844488 23.79936981 -5.50550747 0.90844232 28.89758301 -6.26751804 0.72542071
		 30.061653137 -6.45802021 0.72542131 28.94178009 -6.26751804 0.70711958 27.73351288 -6.077015877 0.72542024
		 26.56945038 -5.88651371 0.72541976 26.5252533 -5.88651371 0.74372149 26.48105621 -5.88651371 0.76202321
		 25.31699371 -5.69600773 0.76202333 21.91317749 -5.12449932 0.72541749 21.86898041 -5.12450123 0.74372053
		 21.82478333 -5.12450123 0.76202345 22.98885345 -5.3150034 0.76202339 24.15292358 -5.50550556 0.76202339
		 24.1087265 -5.50550556 0.78032571 21.64801025 -5.12450123 0.8352356 22.81208038 -5.3150053 0.83523417
		 23.97614288 -5.50550556 0.83523285 24.020339966 -5.50550556 0.81693053 24.064529419 -5.50550556 0.79862809
		 12.82025909 -3.59470177 0.19832483 11.70500183 -3.40997791 0.16572815 11.69585419 -3.40997791 0.18782522
		 12.82737732 -3.59662819 0.20955655 12.82936859 -3.59771347 0.2207599;
	setAttr ".vt[3818:3983]" 11.68670654 -3.40997791 0.20992233 13.97058868 -3.78520393 0.23147106
		 15.11380768 -3.97302341 0.24222037 15.12549591 -3.97570705 0.25356877 12.8249054 -3.59999371 0.26517156
		 11.66838837 -3.40997791 0.25411648 13.9822464 -3.79014206 0.2761859 15.13957977 -3.98029041 0.28720003
		 15.13461304 -3.97871876 0.27600333 15.13104248 -3.97737598 0.26482001 16.28040314 -4.16620922 0.27566648
		 19.76506042 -4.74024296 0.33109054 19.7802124 -4.74349499 0.34250921 18.61818695 -4.55031109 0.29801494
		 17.47209167 -4.36050701 0.26496074 17.45158386 -4.3578825 0.27573407 17.43074799 -4.35520077 0.28648365
		 12.75616455 -3.59474468 0.35367957 11.63178253 -3.40997791 0.34250486 11.62262726 -3.40997791 0.36460188
		 12.76329041 -3.59665585 0.36468628 12.76519775 -3.59772015 0.37576333 11.61347198 -3.40997791 0.38669902
		 13.91564941 -3.78524685 0.36472878 15.06804657 -3.97309208 0.35369241 15.079711914 -3.97574902 0.3647294
		 12.76074219 -3.59998322 0.41990888 11.59516907 -3.40997791 0.43089318 13.92723083 -3.79013443 0.40886718
		 15.093719482 -3.9802866 0.3978253 15.088745117 -3.97872448 0.38681257 15.085197449 -3.97739506 0.37578648
		 16.24377441 -4.16625118 0.36473 19.75595093 -4.74027348 0.35356525 19.77105713 -4.74349499 0.36460626
		 18.59990692 -4.55033398 0.34252065 17.44463348 -4.36052227 0.33148319 17.42414093 -4.35791874 0.3425619
		 17.40332031 -4.3552618 0.35362297 12.6920929 -3.59478951 0.50902414 11.5585556 -3.40997791 0.51928151
		 11.54940796 -3.40997791 0.54137862 12.69921875 -3.59668636 0.51980919 12.70104218 -3.59772682 0.5307653
		 11.54026031 -3.40997791 0.56347573 13.8607254 -3.78529167 0.49797624 15.02230835 -3.97316074 0.46514848
		 15.033943176 -3.97579479 0.47587976 12.69657898 -3.59997177 0.57464868 11.52194214 -3.40997791 0.60766983
		 13.8722229 -3.7901268 0.54155034 15.047866821 -3.98028088 0.5084517 15.042884827 -3.97873211 0.49762034
		 15.039360046 -3.97741604 0.4867481 16.20715332 -4.16629696 0.4537833 19.74684906 -4.74030781 0.37603217
		 19.76190186 -4.74349499 0.38670337 18.58162689 -4.55036068 0.38702053 17.41719055 -4.36053944 0.39800179
		 17.39671326 -4.3579607 0.4093805 17.37592316 -4.35533047 0.42074728 12.67446136 -3.60426903 0.69145697
		 11.4853363 -3.40997791 0.69605827 13.83940125 -3.794631 0.68727708 15.0043411255 -3.98499966 0.68319291
		 15.032241821 -3.9879427 0.65925312 15.05821991 -3.99056911 0.6352582 16.2233429 -4.18041515 0.62303567
		 19.65313721 -4.74470234 0.66716874 19.67456818 -4.74548244 0.62694174 19.69676971 -4.74638844 0.58672035
		 18.53926849 -4.5577879 0.59891611 17.38176727 -4.36918545 0.61108726 17.40570831 -4.37091732 0.57884216
		 19.76040649 -4.74590397 0.42587039 20.92139435 -4.93399715 0.39775276 18.58746338 -4.55592251 0.45499259
		 17.41451263 -4.36593533 0.48405167 17.41556549 -4.36822987 0.51535571 17.41622162 -4.37046146 0.54671288
		 28.96007538 -6.26751804 0.66292465 30.12414551 -6.45802021 0.66292542 28.97837067 -6.26751804 0.61872977
		 28.99666595 -6.26751804 0.57453489 30.16073608 -6.45802021 0.57453537 27.83259583 -6.077015877 0.5745343
		 26.66853333 -5.88651371 0.5745337 26.68682098 -5.88651371 0.53033894 29.069839478 -6.26751804 0.39775506
		 30.23391724 -6.45802212 0.39775527 27.90577698 -6.077015877 0.3977547 26.74172211 -5.8865118 0.39775437
		 26.72341919 -5.8865118 0.44194928 26.7051239 -5.8865118 0.48614413 25.5410614 -5.69600773 0.48614368
		 22.085456848 -5.12450123 0.39775306 22.10375977 -5.12449932 0.3535586 22.06716156 -5.12449932 0.44194764
		 22.048858643 -5.12449932 0.48614213 23.21292877 -5.3150034 0.48614261 24.37699127 -5.50550556 0.48614308
		 29.21620941 -6.26751995 0.044195142 30.38027191 -6.45802402 0.04419503 29.23451233 -6.26751995 -6.1506937e-12
		 28.052154541 -6.077017784 0.044195116 26.88809204 -5.88651562 0.044195097 26.86979675 -5.88651371 0.088390157
		 26.85150146 -5.88651371 0.13258523 25.68743896 -5.69600964 0.13258512 22.23186493 -5.12450314 0.044194911
		 22.21356201 -5.12450123 0.088389754 22.19525909 -5.12450123 0.13258459 23.35932159 -5.3150053 0.13258478
		 24.52338409 -5.50550747 0.13258499 24.50508118 -5.50550556 0.17677993 22.12205505 -5.12449932 0.3093639
		 23.28612518 -5.3150034 0.30936411 24.45018768 -5.50550556 0.30936438 24.46848297 -5.50550556 0.26516956
		 24.48678589 -5.50550556 0.22097479 12.6920929 -3.59478951 -0.50902516 11.54940796 -3.40997791 -0.54137862
		 11.5585556 -3.40997791 -0.51928151 12.70837402 -3.59668636 -0.49771306 12.72272491 -3.59827042 -0.4864409
		 11.56771088 -3.40997791 -0.49718443 13.86988831 -3.78529167 -0.47588116 15.02230835 -3.97316265 -0.46515161
		 15.04309845 -3.97579479 -0.45378578 12.75413513 -3.60039616 -0.44195011 11.58602142 -3.40997791 -0.45299023
		 13.92137146 -3.79067135 -0.43093634 15.088600159 -3.98094463 -0.41992259 15.073730469 -3.9792757 -0.43120152
		 15.059196472 -3.97766399 -0.44247717 16.21630859 -4.16629696 -0.43169034 19.74686432 -4.74031162 -0.37603939
		 19.77106476 -4.74349689 -0.3646135 18.57248688 -4.55036068 -0.40912378 17.39842224 -4.36046696 -0.44222006
		 17.38756561 -4.35796261 -0.4314827 17.37592316 -4.35533237 -0.42075247 12.75616455 -3.59474564 -0.35368058
		 11.62262726 -3.40997791 -0.36460188 11.63178253 -3.40997791 -0.3425048 12.77244568 -3.5966568 -0.34259018
		 12.78673553 -3.59824276 -0.33150187 11.64093781 -3.40997791 -0.32040775 13.92481232 -3.78524685 -0.34263369
		 15.06804657 -3.97309208 -0.35369551 15.088867188 -3.97574902 -0.34263533 12.81817627 -3.60039139 -0.2872597
		 11.65924072 -3.40997791 -0.27621359 13.97622681 -3.790658 -0.29831612 15.13427734 -3.98092556 -0.30937257
		 15.11943054 -3.979249 -0.32045668 15.10494995 -3.97763157 -0.33154422 16.25292969 -4.16625118 -0.342637
		 19.75595856 -4.74027729 -0.35357243 19.7802124 -4.74349689 -0.34251642 18.59075165 -4.55033588 -0.3646239
		 17.42588043 -4.3604517 -0.3756929 17.41500092 -4.35792065 -0.36466411 17.40333557 -4.35526371 -0.35362807
		 12.82025909 -3.59470272 -0.19832583 11.69585419 -3.40997791 -0.18782522;
	setAttr ".vt[3984:4085]" 11.70500183 -3.40997791 -0.16572814 12.83652496 -3.59662819 -0.18746045
		 12.85075378 -3.59821701 -0.17655644 11.7141571 -3.40997791 -0.14363104 13.97973633 -3.78520584 -0.20937598
		 15.11381531 -3.97302532 -0.24222341 15.13465118 -3.97570705 -0.23147471 12.88221741 -3.60038376 -0.13256773
		 11.73246765 -3.40997791 -0.099436879 14.031082153 -3.79064465 -0.16569284 15.1799469 -3.98090649 -0.198818
		 15.16513824 -3.9792223 -0.20970544 15.15071869 -3.97759914 -0.22060388 16.28955841 -4.16621113 -0.25357345
		 19.76506805 -4.74024487 -0.33109769 19.78936768 -4.74349689 -0.32041931 18.60903931 -4.550313 -0.32011822
		 17.45336151 -4.36043835 -0.30916238 17.44243622 -4.35788441 -0.29783627 17.43075562 -4.35520077 -0.28648874
		 12.94442749 -3.60262394 -0.015391544 11.76907349 -3.40997791 -0.011048542 14.11043549 -3.79372501 -0.019528646
		 15.27644348 -3.98482418 -0.023609186 15.28036499 -3.98706913 -0.047247909 15.28343201 -3.98917294 -0.070899814
		 16.44752502 -4.18051243 -0.083157785 19.92019653 -4.74587727 -0.039594714 19.90684509 -4.74642277 -0.079858549
		 19.89395905 -4.74703693 -0.12008212 18.74868011 -4.55876064 -0.10757782 17.60339355 -4.37048626 -0.095137
		 17.59327698 -4.3710146 -0.12735362 19.81343842 -4.7447443 -0.28070423 20.95800781 -4.93400097 -0.30937079
		 18.66622162 -4.55502987 -0.25165889 17.51900482 -4.36531734 -0.22263974 17.54464722 -4.36736774 -0.1909865
		 17.56949615 -4.36928844 -0.15930405 29.21620941 -6.26752186 -0.044197056 30.38027191 -6.45802402 -0.044197097
		 29.19792175 -6.26751995 -0.088393994 29.17961884 -6.26751995 -0.13259098 30.34368134 -6.45802402 -0.13259129
		 28.015563965 -6.077017784 -0.13259056 26.85150146 -5.88651562 -0.13259019 26.83319855 -5.88651371 -0.17678681
		 29.10643768 -6.26752186 -0.30937865 30.27050018 -6.45802402 -0.3093797 27.94237518 -6.077017784 -0.30937755
		 26.77831268 -5.88651562 -0.30937642 26.79660797 -5.88651562 -0.26517993 26.81491089 -5.88651562 -0.22098339
		 25.65084076 -5.69601154 -0.22098263 22.12206268 -5.12450314 -0.30937198 22.1037674 -5.12450314 -0.3535679
		 22.1403656 -5.12450314 -0.26517603 22.15866089 -5.12450314 -0.22098011 23.32272339 -5.3150053 -0.22098093
		 24.48678589 -5.50550747 -0.22098181 28.96008301 -6.26752186 -0.66295379 30.12414551 -6.45802402 -0.66295648
		 27.79601288 -6.077019691 -0.66295105 26.63194275 -5.88651562 -0.66294831 26.65023804 -5.88651562 -0.618752
		 26.66853333 -5.88651562 -0.57455558 25.5044632 -5.69601154 -0.57455331 21.97566986 -5.12450314 -0.66293746
		 21.99397278 -5.12450314 -0.61874187 22.012268066 -5.12450314 -0.57454628 23.17633057 -5.3150053 -0.5745486
		 24.34040833 -5.50550747 -0.57455099 24.35869598 -5.50550747 -0.53035492 22.085464478 -5.12450314 -0.39776358
		 23.24952698 -5.3150053 -0.3977651 24.41359711 -5.50550747 -0.39776659 24.39529419 -5.50550747 -0.44196269
		 24.37700653 -5.50550747 -0.48615891 25.84074402 -4.21133709 1.413481e-05 25.82244873 -4.21133518 -1.048939109
		 27.0047988892 -4.40184116 1.413481e-05 26.9865036 -4.40183926 -1.048939347 25.76758575 -4.21131229 1.18108368
		 25.78588104 -4.21131229 1.13688803 26.93164063 -4.40181446 1.18108404 26.94994354 -4.40181446 1.13688827
		 25.82244873 -4.21133709 1.048966289 26.9865036 -4.40184116 1.048966289 25.80413055 -4.21133518 1.093160272
		 26.96819305 -4.40183926 1.093160391 26.96820068 -4.40183926 -1.09313333 25.80413818 -4.21133518 -1.093133092
		 25.76760101 -4.21131229 -1.18105781 26.93165588 -4.40181446 -1.18105853 25.78586578 -4.21132565 -1.13709462
		 26.94992828 -4.40182781 -1.13709521 26.89500427 -4.40183926 1.26994169 25.73094177 -4.21133518 1.26994157
		 25.71264648 -4.21133518 1.3141371 26.87670898 -4.40183926 1.31413722 25.74923706 -4.21133518 1.22574604
		 26.91329956 -4.40183926 1.22574627;
	setAttr -s 7864 ".ed";
	setAttr ".ed[0:165]"  0 107 1 1 117 1 2 126 0 3 135 0 4 144 1 5 153 1 6 162 0
		 7 171 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 0 8 0 1 9 0 2 10 0
		 3 11 0 4 12 0 5 13 0 6 14 0 7 15 0 8 16 1 9 16 1 10 16 1 11 16 1 12 16 1 13 16 1
		 14 16 1 15 16 1 0 102 0 1 67 0 17 3129 1 2 72 0 18 3255 1 3 77 1 19 3380 0 4 82 0
		 20 3505 0 5 87 0 21 3630 1 6 92 0 22 3755 1 7 97 1 23 3880 0 24 4005 0 17 3040 0
		 18 2879 0 25 3149 0 27 25 1 27 26 1 19 2902 0 26 3274 0 27 28 1 20 2925 0 28 3399 0
		 27 29 1 21 2948 0 29 3524 0 27 30 1 22 2971 0 30 3649 0 27 31 1 23 2994 0 31 3774 0
		 27 32 1 24 3017 0 32 3899 0 27 33 1 33 4024 0 35 68 1 36 110 0 37 3085 1 38 115 0
		 35 69 0 36 108 0 37 112 0 38 70 0 40 73 1 41 3211 1 42 124 0 40 74 0 38 118 0 41 121 0
		 42 75 0 44 78 0 45 3336 0 46 133 1 44 79 0 42 127 0 45 130 0 46 80 0 48 83 0 49 3461 0
		 50 142 0 48 84 0 46 136 0 49 139 0 50 85 0 52 88 1 53 3586 1 54 151 0 52 89 0 50 145 0
		 53 148 0 54 90 0 56 93 1 57 3711 1 58 160 0 56 94 0 54 154 0 57 157 0 58 95 0 60 98 0
		 61 3836 0 60 99 0 58 163 0 61 166 0 62 100 0 64 103 0 65 3961 0 64 104 0 62 172 0
		 65 175 0 36 105 0 67 38 0 68 1 1 69 34 0 70 34 0 67 66 0 68 66 0 69 66 0 70 66 0
		 72 42 0 73 2 1 74 39 0 75 39 0 72 71 0 73 71 0 74 71 0 75 71 0 77 46 1 78 3 0 79 43 0
		 80 43 0 77 76 0 78 76 0 79 76 0 80 76 0 82 50 0 83 4 0 84 47 0 85 47 0 82 81 0 83 81 0
		 84 81 0 85 81 0 87 54 0 88 5 1 89 51 0 90 51 0 87 86 0 88 86 0 89 86 0;
	setAttr ".ed[166:331]" 90 86 0 92 58 0 93 6 1 94 55 0 95 55 0 92 91 0 93 91 0
		 94 91 0 95 91 0 97 62 1 98 7 0 99 59 0 100 59 0 97 96 0 98 96 0 99 96 0 100 96 0
		 102 36 0 103 0 0 104 63 0 105 63 0 102 101 0 103 101 0 104 101 0 105 101 0 107 35 1
		 108 34 0 107 106 0 102 106 0 108 106 0 69 106 0 110 17 0 111 3107 1 112 34 0 110 109 0
		 111 109 0 112 109 0 108 109 0 114 3063 1 115 18 0 114 113 0 115 113 0 70 113 0 112 113 0
		 117 40 1 118 39 0 117 116 0 67 116 0 118 116 0 74 116 0 120 3233 1 121 39 0 115 119 0
		 120 119 0 121 119 0 118 119 0 123 3189 1 124 19 0 123 122 0 124 122 0 75 122 0 121 122 0
		 126 44 0 127 43 0 126 125 0 72 125 0 127 125 0 79 125 0 129 3358 0 130 43 0 124 128 0
		 129 128 0 130 128 0 127 128 0 132 3314 0 133 20 1 132 131 0 133 131 0 80 131 0 130 131 0
		 135 48 0 136 47 0 135 134 0 77 134 0 136 134 0 84 134 0 138 3483 0 139 47 0 133 137 0
		 138 137 0 139 137 0 136 137 0 141 3439 0 142 21 0 141 140 0 142 140 0 85 140 0 139 140 0
		 144 52 1 145 51 0 144 143 0 82 143 0 145 143 0 89 143 0 147 3608 1 148 51 0 142 146 0
		 147 146 0 148 146 0 145 146 0 150 3564 1 151 22 0 150 149 0 151 149 0 90 149 0 148 149 0
		 153 56 1 154 55 0 153 152 0 87 152 0 154 152 0 94 152 0 156 3733 1 157 55 0 151 155 0
		 156 155 0 157 155 0 154 155 0 159 3689 1 160 23 0 159 158 0 160 158 0 95 158 0 157 158 0
		 162 60 0 163 59 0 162 161 0 92 161 0 163 161 0 99 161 0 165 3858 0 160 164 0 165 164 0
		 166 164 0 168 3814 0 169 24 1 168 167 0 169 167 0 166 167 0 171 64 0 172 63 0 171 170 0
		 97 170 0 172 170 0 104 170 0 174 3983 0 169 173 0 174 173 0 175 173 0 177 3939 0
		 177 176 0 110 176 0 105 176 0 175 176 0 157 178 0 55 179 0;
	setAttr ".ed[332:497]" 178 179 0 154 180 0 180 179 0 155 181 0 180 181 0 178 181 0
		 58 182 0 163 183 1 182 183 0 160 184 0 182 184 0 164 185 1 184 185 0 183 185 0 62 186 1
		 172 187 1 186 187 0 169 188 1 186 188 1 173 189 1 188 189 0 187 189 0 166 190 1 59 191 1
		 190 191 0 183 191 0 190 185 0 100 192 1 186 192 0 167 193 1 192 193 0 188 193 0 192 191 0
		 190 193 0 175 194 1 63 195 1 194 195 0 187 195 0 194 189 0 105 196 0 196 195 0 176 197 0
		 194 197 0 196 197 0 199 2888 1 200 3072 1 201 3094 1 202 3116 1 203 2448 0 204 2457 0
		 205 2507 0 199 2407 0 200 2417 0 201 2427 0 202 2437 0 203 2447 0 204 2456 0 205 2335 0
		 207 2911 1 208 3198 1 209 3220 1 210 3242 1 211 2516 0 212 2566 0 207 2466 0 208 2476 0
		 209 2486 0 210 2496 0 205 2506 0 211 2515 0 212 2344 0 214 2934 0 215 3323 0 216 3345 0
		 217 3367 0 218 2575 0 219 2625 0 214 2525 0 215 2535 0 216 2545 0 217 2555 0 212 2565 0
		 218 2574 0 219 2353 0 221 2957 0 222 3448 0 223 3470 0 224 3492 0 225 2634 0 226 2684 0
		 221 2584 0 222 2594 0 223 2604 0 224 2614 0 219 2624 0 225 2633 0 226 2362 0 228 2980 1
		 229 3573 1 230 3595 1 231 3617 1 232 2693 0 233 2743 0 228 2643 0 229 2653 0 230 2663 0
		 231 2673 0 226 2683 0 232 2692 0 233 2371 0 235 3003 1 236 3698 1 237 3720 1 238 3742 1
		 239 2752 0 240 2802 0 235 2702 0 236 2712 0 237 2722 0 238 2732 0 233 2742 0 239 2751 0
		 240 2380 0 242 3026 0 243 3823 0 244 3845 0 245 3867 0 246 2811 0 247 2861 0 242 2761 0
		 243 2771 0 244 2781 0 245 2791 0 240 2801 0 246 2810 0 247 2389 0 249 3049 0 250 3948 0
		 251 3970 0 252 3992 0 253 2870 0 249 2820 0 250 2830 0 251 2840 0 252 2850 0 247 2860 0
		 253 2869 0 203 2398 0 255 1756 0 256 2883 1 257 1506 0 258 1701 0 255 1495 0 256 1500 0
		 257 1505 0 258 1215 0 260 1846 0 261 2906 1 262 1521 0 263 1791 0 260 1510 0 261 1515 0
		 262 1520 0 263 1220 0 265 1936 0 266 2929 0;
	setAttr ".ed[498:663]" 267 1536 0 268 1881 0 265 1525 0 266 1530 0 267 1535 0
		 268 1225 0 270 2026 0 271 2952 0 272 1551 0 273 1971 0 270 1540 0 271 1545 0 272 1550 0
		 273 1230 0 275 2116 0 276 2975 1 277 1566 0 278 2061 0 275 1555 0 276 1560 0 277 1565 0
		 278 1235 0 280 2206 0 281 2998 1 282 1581 0 283 2151 0 280 1570 0 281 1575 0 282 1580 0
		 283 1240 0 285 2296 0 286 3021 0 287 1596 0 288 2241 0 285 1585 0 286 1590 0 287 1595 0
		 288 1245 0 290 1666 0 291 3044 0 292 1611 0 293 2331 0 290 1600 0 291 1605 0 292 1610 0
		 293 1250 0 295 2411 1 296 3067 1 297 1626 0 295 1615 0 296 1620 0 297 1625 0 257 1255 0
		 299 2421 1 300 3089 1 301 1641 0 299 1630 0 300 1635 0 301 1640 0 297 1260 0 303 2431 1
		 304 3111 1 305 1656 0 303 1645 0 304 1650 0 305 1655 0 301 1265 0 307 2441 1 308 1671 0
		 307 1660 0 290 1665 0 308 1670 0 305 1270 0 310 1676 0 311 1681 0 312 1686 0 310 1675 0
		 311 1680 0 312 1685 0 308 1275 0 314 1691 0 315 1766 0 314 1690 0 315 1695 0 258 1700 0
		 312 1280 0 317 2470 1 318 3193 1 319 1716 0 317 1705 0 318 1710 0 319 1715 0 262 1285 0
		 321 2480 1 322 3215 1 323 1731 0 321 1720 0 322 1725 0 323 1730 0 319 1290 0 325 2490 1
		 326 3237 1 327 1746 0 325 1735 0 326 1740 0 327 1745 0 323 1295 0 329 2500 1 330 1761 0
		 329 1750 0 255 1755 0 330 1760 0 327 1300 0 332 1771 0 333 1776 0 315 1765 0 332 1770 0
		 333 1775 0 330 1305 0 335 1781 0 336 1856 0 335 1780 0 336 1785 0 263 1790 0 333 1310 0
		 338 2529 0 339 3318 0 340 1806 0 338 1795 0 339 1800 0 340 1805 0 267 1315 0 342 2539 0
		 343 3340 0 344 1821 0 342 1810 0 343 1815 0 344 1820 0 340 1320 0 346 2549 0 347 3362 0
		 348 1836 0 346 1825 0 347 1830 0 348 1835 0 344 1325 0 350 2559 0 351 1851 0 350 1840 0
		 260 1845 0 351 1850 0 348 1330 0 353 1861 0 354 1866 0 336 1855 0 353 1860 0 354 1865 0
		 351 1335 0 356 1871 0 357 1946 0 356 1870 0 357 1875 0 268 1880 0 354 1340 0 359 2588 0
		 360 3443 0;
	setAttr ".ed[664:829]" 361 1896 0 359 1885 0 360 1890 0 361 1895 0 272 1345 0
		 363 2598 0 364 3465 0 365 1911 0 363 1900 0 364 1905 0 365 1910 0 361 1350 0 367 2608 0
		 368 3487 0 369 1926 0 367 1915 0 368 1920 0 369 1925 0 365 1355 0 371 2618 0 372 1941 0
		 371 1930 0 265 1935 0 372 1940 0 369 1360 0 374 1951 0 375 1956 0 357 1945 0 374 1950 0
		 375 1955 0 372 1365 0 377 1961 0 378 2036 0 377 1960 0 378 1965 0 273 1970 0 375 1370 0
		 380 2647 1 381 3568 1 382 1986 0 380 1975 0 381 1980 0 382 1985 0 277 1375 0 384 2657 1
		 385 3590 1 386 2001 0 384 1990 0 385 1995 0 386 2000 0 382 1380 0 388 2667 1 389 3612 1
		 390 2016 0 388 2005 0 389 2010 0 390 2015 0 386 1385 0 392 2677 1 393 2031 0 392 2020 0
		 270 2025 0 393 2030 0 390 1390 0 395 2041 0 396 2046 0 378 2035 0 395 2040 0 396 2045 0
		 393 1395 0 398 2051 0 399 2126 0 398 2050 0 399 2055 0 278 2060 0 396 1400 0 401 2706 1
		 402 3693 1 403 2076 0 401 2065 0 402 2070 0 403 2075 0 282 1405 0 405 2716 1 406 3715 1
		 407 2091 0 405 2080 0 406 2085 0 407 2090 0 403 1410 0 409 2726 1 410 3737 1 411 2106 0
		 409 2095 0 410 2100 0 411 2105 0 407 1415 0 413 2736 1 414 2121 0 413 2110 0 275 2115 0
		 414 2120 0 411 1420 0 416 2131 0 417 2136 0 399 2125 0 416 2130 0 417 2135 0 414 1425 0
		 419 2141 0 420 2216 0 419 2140 0 420 2145 0 283 2150 0 417 1430 0 422 2765 0 423 3818 0
		 424 2166 0 422 2155 0 423 2160 0 424 2165 0 287 1435 0 426 2775 0 427 3840 0 428 2181 0
		 426 2170 0 427 2175 0 428 2180 0 424 1440 0 430 2785 0 431 3862 0 432 2196 0 430 2185 0
		 431 2190 0 432 2195 0 428 1445 0 434 2795 0 435 2211 0 434 2200 0 280 2205 0 435 2210 0
		 432 1450 0 437 2221 0 438 2226 0 420 2215 0 437 2220 0 438 2225 0 435 1455 0 440 2231 0
		 441 2306 0 440 2230 0 441 2235 0 288 2240 0 438 1460 0 443 2824 0 444 3943 0 445 2256 0
		 443 2245 0 444 2250 0 445 2255 0 292 1465 0 447 2834 0 448 3965 0 449 2271 0 447 2260 0
		 448 2265 0;
	setAttr ".ed[830:995]" 449 2270 0 445 1470 0 451 2844 0 452 3987 0 453 2286 0
		 451 2275 0 452 2280 0 453 2285 0 449 1475 0 455 2854 0 456 2301 0 455 2290 0 285 2295 0
		 456 2300 0 453 1480 0 458 2311 0 459 2316 0 441 2305 0 458 2310 0 459 2315 0 456 1485 0
		 461 2321 0 461 2320 0 310 2325 0 293 2330 0 459 1490 0 463 1216 0 464 2336 0 465 2341 0
		 466 2342 0 463 1217 0 464 2337 0 465 2339 0 466 1218 0 468 1221 0 469 2345 0 470 2350 0
		 471 2351 0 468 1222 0 469 2346 0 470 2348 0 471 1223 0 473 1226 0 474 2354 0 475 2359 0
		 476 2360 0 473 1227 0 474 2355 0 475 2357 0 476 1228 0 478 1231 0 479 2363 0 480 2368 0
		 481 2369 0 478 1232 0 479 2364 0 480 2366 0 481 1233 0 483 1236 0 484 2372 0 485 2377 0
		 486 2378 0 483 1237 0 484 2373 0 485 2375 0 486 1238 0 488 1241 0 489 2381 0 490 2386 0
		 491 2387 0 488 1242 0 489 2382 0 490 2384 0 491 1243 0 493 1246 0 494 2390 0 495 2395 0
		 496 2396 0 493 1247 0 494 2391 0 495 2393 0 496 1248 0 498 1251 0 499 2399 0 500 2404 0
		 501 2405 0 498 1252 0 499 2400 0 500 2402 0 501 1253 0 503 1256 0 504 2408 1 505 2414 0
		 506 2415 0 503 1257 0 504 2409 0 505 2412 0 506 1258 0 508 1261 0 509 2418 1 510 2424 0
		 511 2425 0 508 1262 0 509 2419 0 510 2422 0 511 1263 0 513 1266 0 514 2428 1 515 2434 0
		 516 2435 0 513 1267 0 514 2429 0 515 2432 0 516 1268 0 518 1271 0 519 2438 1 520 2444 0
		 521 2445 0 518 1272 0 519 2439 0 520 2442 0 521 1273 0 523 1276 0 524 2326 0 525 2453 0
		 526 2454 0 523 1277 0 524 2449 0 525 2451 0 526 1278 0 528 1281 0 529 2460 0 530 2463 0
		 531 2464 0 528 1282 0 529 2458 0 530 2461 0 531 1283 0 533 1286 0 534 2467 1 535 2473 0
		 536 2474 0 533 1287 0 534 2468 0 535 2471 0 536 1288 0 538 1291 0 539 2477 1 540 2483 0
		 541 2484 0 538 1292 0 539 2478 0 540 2481 0 541 1293 0 543 1296 0 544 2487 1 545 2493 0
		 546 2494 0 543 1297 0 544 2488 0 545 2491 0 546 1298 0 548 1301 0 549 2497 1 550 2503 0
		 551 2504 0;
	setAttr ".ed[996:1161]" 548 1302 0 549 2498 0 550 2501 0 551 1303 0 553 1306 0
		 554 1696 0 555 2512 0 556 2513 0 553 1307 0 554 2508 0 555 2510 0 556 1308 0 558 1311 0
		 559 2519 0 560 2522 0 561 2523 0 558 1312 0 559 2517 0 560 2520 0 561 1313 0 563 1316 0
		 564 2526 0 565 2532 0 566 2533 0 563 1317 0 564 2527 0 565 2530 0 566 1318 0 568 1321 0
		 569 2536 0 570 2542 0 571 2543 0 568 1322 0 569 2537 0 570 2540 0 571 1323 0 573 1326 0
		 574 2546 0 575 2552 0 576 2553 0 573 1327 0 574 2547 0 575 2550 0 576 1328 0 578 1331 0
		 579 2556 0 580 2562 0 581 2563 0 578 1332 0 579 2557 0 580 2560 0 581 1333 0 583 1336 0
		 584 1786 0 585 2571 0 586 2572 0 583 1337 0 584 2567 0 585 2569 0 586 1338 0 588 1341 0
		 589 2578 0 590 2581 0 591 2582 0 588 1342 0 589 2576 0 590 2579 0 591 1343 0 593 1346 0
		 594 2585 0 595 2591 0 596 2592 0 593 1347 0 594 2586 0 595 2589 0 596 1348 0 598 1351 0
		 599 2595 0 600 2601 0 601 2602 0 598 1352 0 599 2596 0 600 2599 0 601 1353 0 603 1356 0
		 604 2605 0 605 2611 0 606 2612 0 603 1357 0 604 2606 0 605 2609 0 606 1358 0 608 1361 0
		 609 2615 0 610 2621 0 611 2622 0 608 1362 0 609 2616 0 610 2619 0 611 1363 0 613 1366 0
		 614 1876 0 615 2630 0 616 2631 0 613 1367 0 614 2626 0 615 2628 0 616 1368 0 618 1371 0
		 619 2637 0 620 2640 0 621 2641 0 618 1372 0 619 2635 0 620 2638 0 621 1373 0 623 1376 0
		 624 2644 1 625 2650 0 626 2651 0 623 1377 0 624 2645 0 625 2648 0 626 1378 0 628 1381 0
		 629 2654 1 630 2660 0 631 2661 0 628 1382 0 629 2655 0 630 2658 0 631 1383 0 633 1386 0
		 634 2664 1 635 2670 0 636 2671 0 633 1387 0 634 2665 0 635 2668 0 636 1388 0 638 1391 0
		 639 2674 1 640 2680 0 641 2681 0 638 1392 0 639 2675 0 640 2678 0 641 1393 0 643 1396 0
		 644 1966 0 645 2689 0 646 2690 0 643 1397 0 644 2685 0 645 2687 0 646 1398 0 648 1401 0
		 649 2696 0 650 2699 0 651 2700 0 648 1402 0 649 2694 0 650 2697 0 651 1403 0 653 1406 0
		 654 2703 1;
	setAttr ".ed[1162:1327]" 655 2709 0 656 2710 0 653 1407 0 654 2704 0 655 2707 0
		 656 1408 0 658 1411 0 659 2713 1 660 2719 0 661 2720 0 658 1412 0 659 2714 0 660 2717 0
		 661 1413 0 663 1416 0 664 2723 1 665 2729 0 666 2730 0 663 1417 0 664 2724 0 665 2727 0
		 666 1418 0 668 1421 0 669 2733 1 670 2739 0 671 2740 0 668 1422 0 669 2734 0 670 2737 0
		 671 1423 0 673 1426 0 674 2056 0 675 2748 0 676 2749 0 673 1427 0 674 2744 0 675 2746 0
		 676 1428 0 678 1431 0 679 2755 0 680 2758 0 681 2759 0 678 1432 0 679 2753 0 680 2756 0
		 681 1433 0 683 1436 0 684 2762 0 685 2768 0 686 2769 0 683 1437 0 684 2763 0 685 2766 0
		 686 1438 0 688 1441 0 689 2772 0 690 2778 0 691 2779 0 688 1442 0 689 2773 0 690 2776 0
		 691 1443 0 693 1446 0 694 2782 0 695 2788 0 696 2789 0 693 1447 0 694 2783 0 695 2786 0
		 696 1448 0 698 1451 0 699 2792 0 700 2798 0 701 2799 0 698 1452 0 699 2793 0 700 2796 0
		 701 1453 0 703 1456 0 704 2146 0 705 2807 0 706 2808 0 703 1457 0 704 2803 0 705 2805 0
		 706 1458 0 708 1461 0 709 2814 0 710 2817 0 711 2818 0 708 1462 0 709 2812 0 710 2815 0
		 711 1463 0 713 1466 0 714 2821 0 715 2827 0 716 2828 0 713 1467 0 714 2822 0 715 2825 0
		 716 1468 0 718 1471 0 719 2831 0 720 2837 0 721 2838 0 718 1472 0 719 2832 0 720 2835 0
		 721 1473 0 723 1476 0 724 2841 0 725 2847 0 726 2848 0 723 1477 0 724 2842 0 725 2845 0
		 726 1478 0 728 1481 0 729 2851 0 730 2857 0 731 2858 0 728 1482 0 729 2852 0 730 2855 0
		 731 1483 0 733 1486 0 735 2866 0 736 2867 0 733 1487 0 734 2862 0 735 2864 0 736 1488 0
		 738 1491 0 739 2873 0 740 2876 0 741 2877 0 738 1492 0 739 2871 0 740 2874 0 741 1493 0
		 743 1496 0 744 2880 1 745 2886 0 743 1497 0 744 2881 0 745 2884 0 465 1498 0 747 1501 1
		 748 2893 0 747 1502 0 503 2889 0 748 2891 0 745 1503 0 750 2895 0 751 2896 0 750 1507 0
		 751 2897 0 466 2899 0 748 1508 0 753 1511 0 754 2903 1 755 2909 0 753 1512 0 754 2904 0
		 755 2907 0;
	setAttr ".ed[1328:1493]" 470 1513 0 757 1516 1 758 2916 0 757 1517 0 533 2912 0
		 758 2914 0 755 1518 0 760 2918 0 761 2919 0 760 1522 0 761 2920 0 471 2922 0 758 1523 0
		 763 1526 0 764 2926 0 765 2932 0 763 1527 0 764 2927 0 765 2930 0 475 1528 0 767 1531 0
		 768 2939 0 767 1532 0 563 2935 0 768 2937 0 765 1533 0 770 2941 0 771 2942 0 770 1537 0
		 771 2943 0 476 2945 0 768 1538 0 773 1541 0 774 2949 0 775 2955 0 773 1542 0 774 2950 0
		 775 2953 0 480 1543 0 777 1546 0 778 2962 0 777 1547 0 593 2958 0 778 2960 0 775 1548 0
		 780 2964 0 781 2965 0 780 1552 0 781 2966 0 481 2968 0 778 1553 0 783 1556 0 784 2972 1
		 785 2978 0 783 1557 0 784 2973 0 785 2976 0 485 1558 0 787 1561 1 788 2985 0 787 1562 0
		 623 2981 0 788 2983 0 785 1563 0 790 2987 0 791 2988 0 790 1567 0 791 2989 0 486 2991 0
		 788 1568 0 793 1571 0 794 2995 1 795 3001 0 793 1572 0 794 2996 0 795 2999 0 490 1573 0
		 797 1576 1 798 3008 0 797 1577 0 653 3004 0 798 3006 0 795 1578 0 800 3010 0 801 3011 0
		 800 1582 0 801 3012 0 491 3014 0 798 1583 0 803 1586 0 804 3018 0 805 3024 0 803 1587 0
		 804 3019 0 805 3022 0 495 1588 0 807 1591 0 808 3031 0 807 1592 0 683 3027 0 808 3029 0
		 805 1593 0 810 3033 0 811 3034 0 810 1597 0 811 3035 0 496 3037 0 808 1598 0 813 1601 0
		 814 3041 0 815 3047 0 813 1602 0 814 3042 0 815 3045 0 500 1603 0 817 1606 0 818 3054 0
		 817 1607 0 713 3050 0 818 3052 0 815 1608 0 820 3056 0 821 3057 0 820 1612 0 821 3058 0
		 501 3060 0 818 1613 0 823 1616 1 824 3064 1 825 3070 0 823 1617 0 824 3065 0 825 3068 0
		 505 1618 0 827 1621 1 828 3077 0 827 1622 0 508 3073 0 828 3075 0 825 1623 0 830 3079 0
		 830 1627 0 750 3080 0 506 3082 0 828 1628 0 832 1631 1 833 3086 1 834 3092 0 832 1632 0
		 833 3087 0 834 3090 0 510 1633 0 836 1636 1 837 3099 0 836 1637 0 513 3095 0 837 3097 0
		 834 1638 0 839 3101 0 839 1642 0 830 3102 0 511 3104 0 837 1643 0 841 1646 1 842 3108 1
		 843 3114 0;
	setAttr ".ed[1494:1659]" 841 1647 0 842 3109 0 843 3112 0 515 1648 0 845 1651 1
		 846 3121 0 845 1652 0 518 3117 0 846 3119 0 843 1653 0 848 3123 0 848 1657 0 839 3124 0
		 516 3126 0 846 1658 0 850 1661 1 851 3134 0 850 1662 0 813 3130 0 851 3132 0 520 1663 0
		 853 3140 0 499 1667 0 523 3136 0 853 3138 0 851 1668 0 855 3142 0 855 1672 0 848 3143 0
		 521 3145 0 853 1673 0 857 3148 0 858 3152 0 859 3155 0 857 1677 0 858 3150 0 859 3153 0
		 525 1678 0 861 3157 0 862 3162 0 861 1682 0 528 3158 0 862 3160 0 859 1683 0 864 3164 0
		 864 1687 0 855 3165 0 526 3167 0 862 1688 0 866 3170 0 867 3171 0 868 3176 0 866 1692 0
		 867 3172 0 868 3174 0 530 1693 0 870 3182 0 554 1697 0 463 3178 0 870 3180 0 868 1698 0
		 751 1702 0 864 3184 0 531 3186 0 870 1703 0 873 1706 1 874 3190 1 875 3196 0 873 1707 0
		 874 3191 0 875 3194 0 535 1708 0 877 1711 1 878 3203 0 877 1712 0 538 3199 0 878 3201 0
		 875 1713 0 880 3205 0 880 1717 0 760 3206 0 536 3208 0 878 1718 0 882 1721 1 883 3212 1
		 884 3218 0 882 1722 0 883 3213 0 884 3216 0 540 1723 0 886 1726 1 887 3225 0 886 1727 0
		 543 3221 0 887 3223 0 884 1728 0 889 3227 0 889 1732 0 880 3228 0 541 3230 0 887 1733 0
		 891 1736 1 892 3234 1 893 3240 0 891 1737 0 892 3235 0 893 3238 0 545 1738 0 895 1741 1
		 896 3247 0 895 1742 0 548 3243 0 896 3245 0 893 1743 0 898 3249 0 898 1747 0 889 3250 0
		 546 3252 0 896 1748 0 900 1751 1 901 3260 0 900 1752 0 743 3256 0 901 3258 0 550 1753 0
		 903 3266 0 464 1757 0 553 3262 0 903 3264 0 901 1758 0 905 3268 0 905 1762 0 898 3269 0
		 551 3271 0 903 1763 0 907 3277 0 908 3280 0 867 1767 0 907 3275 0 908 3278 0 555 1768 0
		 910 3282 0 911 3287 0 910 1772 0 558 3283 0 911 3285 0 908 1773 0 913 3289 0 913 1777 0
		 905 3290 0 556 3292 0 911 1778 0 915 3295 0 916 3296 0 917 3301 0 915 1782 0 916 3297 0
		 917 3299 0 560 1783 0 919 3307 0 584 1787 0 468 3303 0 919 3305 0 917 1788 0 761 1792 0
		 913 3309 0;
	setAttr ".ed[1660:1825]" 561 3311 0 919 1793 0 922 1796 0 923 3315 0 924 3321 0
		 922 1797 0 923 3316 0 924 3319 0 565 1798 0 926 1801 0 927 3328 0 926 1802 0 568 3324 0
		 927 3326 0 924 1803 0 929 3330 0 929 1807 0 770 3331 0 566 3333 0 927 1808 0 931 1811 0
		 932 3337 0 933 3343 0 931 1812 0 932 3338 0 933 3341 0 570 1813 0 935 1816 0 936 3350 0
		 935 1817 0 573 3346 0 936 3348 0 933 1818 0 938 3352 0 938 1822 0 929 3353 0 571 3355 0
		 936 1823 0 940 1826 0 941 3359 0 942 3365 0 940 1827 0 941 3360 0 942 3363 0 575 1828 0
		 944 1831 0 945 3372 0 944 1832 0 578 3368 0 945 3370 0 942 1833 0 947 3374 0 947 1837 0
		 938 3375 0 576 3377 0 945 1838 0 949 1841 0 950 3385 0 949 1842 0 753 3381 0 950 3383 0
		 580 1843 0 952 3391 0 469 1847 0 583 3387 0 952 3389 0 950 1848 0 954 3393 0 954 1852 0
		 947 3394 0 581 3396 0 952 1853 0 956 3402 0 957 3405 0 916 1857 0 956 3400 0 957 3403 0
		 585 1858 0 959 3407 0 960 3412 0 959 1862 0 588 3408 0 960 3410 0 957 1863 0 962 3414 0
		 962 1867 0 954 3415 0 586 3417 0 960 1868 0 964 3420 0 965 3421 0 966 3426 0 964 1872 0
		 965 3422 0 966 3424 0 590 1873 0 968 3432 0 614 1877 0 473 3428 0 968 3430 0 966 1878 0
		 771 1882 0 962 3434 0 591 3436 0 968 1883 0 971 1886 0 972 3440 0 973 3446 0 971 1887 0
		 972 3441 0 973 3444 0 595 1888 0 975 1891 0 976 3453 0 975 1892 0 598 3449 0 976 3451 0
		 973 1893 0 978 3455 0 978 1897 0 780 3456 0 596 3458 0 976 1898 0 980 1901 0 981 3462 0
		 982 3468 0 980 1902 0 981 3463 0 982 3466 0 600 1903 0 984 1906 0 985 3475 0 984 1907 0
		 603 3471 0 985 3473 0 982 1908 0 987 3477 0 987 1912 0 978 3478 0 601 3480 0 985 1913 0
		 989 1916 0 990 3484 0 991 3490 0 989 1917 0 990 3485 0 991 3488 0 605 1918 0 993 1921 0
		 994 3497 0 993 1922 0 608 3493 0 994 3495 0 991 1923 0 996 3499 0 996 1927 0 987 3500 0
		 606 3502 0 994 1928 0 998 1931 0 999 3510 0 998 1932 0 763 3506 0 999 3508 0 610 1933 0
		 1001 3516 0;
	setAttr ".ed[1826:1991]" 474 1937 0 613 3512 0 1001 3514 0 999 1938 0 1003 3518 0
		 1003 1942 0 996 3519 0 611 3521 0 1001 1943 0 1005 3527 0 1006 3530 0 965 1947 0
		 1005 3525 0 1006 3528 0 615 1948 0 1008 3532 0 1009 3537 0 1008 1952 0 618 3533 0
		 1009 3535 0 1006 1953 0 1011 3539 0 1011 1957 0 1003 3540 0 616 3542 0 1009 1958 0
		 1013 3545 0 1014 3546 0 1015 3551 0 1013 1962 0 1014 3547 0 1015 3549 0 620 1963 0
		 1017 3557 0 644 1967 0 478 3553 0 1017 3555 0 1015 1968 0 781 1972 0 1011 3559 0
		 621 3561 0 1017 1973 0 1020 1976 1 1021 3565 1 1022 3571 0 1020 1977 0 1021 3566 0
		 1022 3569 0 625 1978 0 1024 1981 1 1025 3578 0 1024 1982 0 628 3574 0 1025 3576 0
		 1022 1983 0 1027 3580 0 1027 1987 0 790 3581 0 626 3583 0 1025 1988 0 1029 1991 1
		 1030 3587 1 1031 3593 0 1029 1992 0 1030 3588 0 1031 3591 0 630 1993 0 1033 1996 1
		 1034 3600 0 1033 1997 0 633 3596 0 1034 3598 0 1031 1998 0 1036 3602 0 1036 2002 0
		 1027 3603 0 631 3605 0 1034 2003 0 1038 2006 1 1039 3609 1 1040 3615 0 1038 2007 0
		 1039 3610 0 1040 3613 0 635 2008 0 1042 2011 1 1043 3622 0 1042 2012 0 638 3618 0
		 1043 3620 0 1040 2013 0 1045 3624 0 1045 2017 0 1036 3625 0 636 3627 0 1043 2018 0
		 1047 2021 1 1048 3635 0 1047 2022 0 773 3631 0 1048 3633 0 640 2023 0 1050 3641 0
		 479 2027 0 643 3637 0 1050 3639 0 1048 2028 0 1052 3643 0 1052 2032 0 1045 3644 0
		 641 3646 0 1050 2033 0 1054 3652 0 1055 3655 0 1014 2037 0 1054 3650 0 1055 3653 0
		 645 2038 0 1057 3657 0 1058 3662 0 1057 2042 0 648 3658 0 1058 3660 0 1055 2043 0
		 1060 3664 0 1060 2047 0 1052 3665 0 646 3667 0 1058 2048 0 1062 3670 0 1063 3671 0
		 1064 3676 0 1062 2052 0 1063 3672 0 1064 3674 0 650 2053 0 1066 3682 0 674 2057 0
		 483 3678 0 1066 3680 0 1064 2058 0 791 2062 0 1060 3684 0 651 3686 0 1066 2063 0
		 1069 2066 1 1070 3690 1 1071 3696 0 1069 2067 0 1070 3691 0 1071 3694 0 655 2068 0
		 1073 2071 1 1074 3703 0 1073 2072 0 658 3699 0 1074 3701 0 1071 2073 0 1076 3705 0
		 1076 2077 0 800 3706 0 656 3708 0 1074 2078 0 1078 2081 1 1079 3712 1 1080 3718 0;
	setAttr ".ed[1992:2157]" 1078 2082 0 1079 3713 0 1080 3716 0 660 2083 0 1082 2086 1
		 1083 3725 0 1082 2087 0 663 3721 0 1083 3723 0 1080 2088 0 1085 3727 0 1085 2092 0
		 1076 3728 0 661 3730 0 1083 2093 0 1087 2096 1 1088 3734 1 1089 3740 0 1087 2097 0
		 1088 3735 0 1089 3738 0 665 2098 0 1091 2101 1 1092 3747 0 1091 2102 0 668 3743 0
		 1092 3745 0 1089 2103 0 1094 3749 0 1094 2107 0 1085 3750 0 666 3752 0 1092 2108 0
		 1096 2111 1 1097 3760 0 1096 2112 0 783 3756 0 1097 3758 0 670 2113 0 1099 3766 0
		 484 2117 0 673 3762 0 1099 3764 0 1097 2118 0 1101 3768 0 1101 2122 0 1094 3769 0
		 671 3771 0 1099 2123 0 1103 3777 0 1104 3780 0 1063 2127 0 1103 3775 0 1104 3778 0
		 675 2128 0 1106 3782 0 1107 3787 0 1106 2132 0 678 3783 0 1107 3785 0 1104 2133 0
		 1109 3789 0 1109 2137 0 1101 3790 0 676 3792 0 1107 2138 0 1111 3795 0 1112 3796 0
		 1113 3801 0 1111 2142 0 1112 3797 0 1113 3799 0 680 2143 0 1115 3807 0 704 2147 0
		 488 3803 0 1115 3805 0 1113 2148 0 801 2152 0 1109 3809 0 681 3811 0 1115 2153 0
		 1118 2156 0 1119 3815 0 1120 3821 0 1118 2157 0 1119 3816 0 1120 3819 0 685 2158 0
		 1122 2161 0 1123 3828 0 1122 2162 0 688 3824 0 1123 3826 0 1120 2163 0 1125 3830 0
		 1125 2167 0 810 3831 0 686 3833 0 1123 2168 0 1127 2171 0 1128 3837 0 1129 3843 0
		 1127 2172 0 1128 3838 0 1129 3841 0 690 2173 0 1131 2176 0 1132 3850 0 1131 2177 0
		 693 3846 0 1132 3848 0 1129 2178 0 1134 3852 0 1134 2182 0 1125 3853 0 691 3855 0
		 1132 2183 0 1136 2186 0 1137 3859 0 1138 3865 0 1136 2187 0 1137 3860 0 1138 3863 0
		 695 2188 0 1140 2191 0 1141 3872 0 1140 2192 0 698 3868 0 1141 3870 0 1138 2193 0
		 1143 3874 0 1143 2197 0 1134 3875 0 696 3877 0 1141 2198 0 1145 2201 0 1146 3885 0
		 1145 2202 0 793 3881 0 1146 3883 0 700 2203 0 1148 3891 0 489 2207 0 703 3887 0 1148 3889 0
		 1146 2208 0 1150 3893 0 1150 2212 0 1143 3894 0 701 3896 0 1148 2213 0 1152 3902 0
		 1153 3905 0 1112 2217 0 1152 3900 0 1153 3903 0 705 2218 0 1155 3907 0 1156 3912 0
		 1155 2222 0 708 3908 0 1156 3910 0 1153 2223 0 1158 3914 0 1158 2227 0;
	setAttr ".ed[2158:2323]" 1150 3915 0 706 3917 0 1156 2228 0 1160 3920 0 1161 3921 0
		 1162 3926 0 1160 2232 0 1161 3922 0 1162 3924 0 710 2233 0 734 2237 0 493 3928 0
		 1164 3930 0 1162 2238 0 811 2242 0 1158 3934 0 711 3936 0 1164 2243 0 1167 2246 0
		 1168 3940 0 1169 3946 0 1167 2247 0 1168 3941 0 1169 3944 0 715 2248 0 1171 2251 0
		 1172 3953 0 1171 2252 0 718 3949 0 1172 3951 0 1169 2253 0 1174 3955 0 1174 2257 0
		 820 3956 0 716 3958 0 1172 2258 0 1176 2261 0 1177 3962 0 1178 3968 0 1176 2262 0
		 1177 3963 0 1178 3966 0 720 2263 0 1180 2266 0 1181 3975 0 1180 2267 0 723 3971 0
		 1181 3973 0 1178 2268 0 1183 3977 0 1183 2272 0 1174 3978 0 721 3980 0 1181 2273 0
		 1185 2276 0 1186 3984 0 1187 3990 0 1185 2277 0 1186 3985 0 1187 3988 0 725 2278 0
		 1189 2281 0 1190 3997 0 1189 2282 0 728 3993 0 1190 3995 0 1187 2283 0 1192 3999 0
		 1192 2287 0 1183 4000 0 726 4002 0 1190 2288 0 1194 2291 0 1195 4010 0 1194 2292 0
		 803 4006 0 1195 4008 0 730 2293 0 1197 4016 0 494 2297 0 733 4012 0 1197 4014 0 1195 2298 0
		 1199 4018 0 1199 2302 0 1192 4019 0 731 4021 0 1197 2303 0 1201 4027 0 1202 4030 0
		 1161 2307 0 1201 4025 0 1202 4028 0 735 2308 0 1204 4032 0 1205 4037 0 1204 2312 0
		 738 4033 0 1205 4035 0 1202 2313 0 1207 4039 0 1207 2317 0 1199 4040 0 736 4042 0
		 1205 2318 0 1209 4045 0 1210 4050 0 1209 2322 0 857 4046 0 1210 4048 0 740 2323 0
		 1212 4056 0 524 2327 0 498 4052 0 1212 4054 0 1210 2328 0 821 2332 0 1207 4058 0
		 741 4060 0 1212 2333 0 1215 466 0 1216 258 0 1217 462 0 1218 462 0 1215 1214 0 1216 1214 0
		 1217 1214 0 1218 1214 0 1220 471 0 1221 263 0 1222 467 0 1223 467 0 1220 1219 0 1221 1219 0
		 1222 1219 0 1223 1219 0 1225 476 0 1226 268 0 1227 472 0 1228 472 0 1225 1224 0 1226 1224 0
		 1227 1224 0 1228 1224 0 1230 481 0 1231 273 0 1232 477 0 1233 477 0 1230 1229 0 1231 1229 0
		 1232 1229 0 1233 1229 0 1235 486 0 1236 278 0 1237 482 0 1238 482 0 1235 1234 0 1236 1234 0
		 1237 1234 0 1238 1234 0 1240 491 0 1241 283 0 1242 487 0 1243 487 0 1240 1239 0 1241 1239 0;
	setAttr ".ed[2324:2489]" 1242 1239 0 1243 1239 0 1245 496 0 1246 288 0 1247 492 0
		 1248 492 0 1245 1244 0 1246 1244 0 1247 1244 0 1248 1244 0 1250 501 0 1251 293 0
		 1252 497 0 1253 497 0 1250 1249 0 1251 1249 0 1252 1249 0 1253 1249 0 1255 506 0
		 1256 257 0 1257 502 0 1258 502 0 1255 1254 0 1256 1254 0 1257 1254 0 1258 1254 0
		 1260 511 0 1261 297 0 1262 507 0 1263 507 0 1260 1259 0 1261 1259 0 1262 1259 0 1263 1259 0
		 1265 516 0 1266 301 0 1267 512 0 1268 512 0 1265 1264 0 1266 1264 0 1267 1264 0 1268 1264 0
		 1270 521 0 1271 305 0 1272 517 0 1273 517 0 1270 1269 0 1271 1269 0 1272 1269 0 1273 1269 0
		 1275 526 0 1276 308 0 1277 522 0 1278 522 0 1275 1274 0 1276 1274 0 1277 1274 0 1278 1274 0
		 1280 531 0 1281 312 0 1282 527 0 1283 527 0 1280 1279 0 1281 1279 0 1282 1279 0 1283 1279 0
		 1285 536 0 1286 262 0 1287 532 0 1288 532 0 1285 1284 0 1286 1284 0 1287 1284 0 1288 1284 0
		 1290 541 0 1291 319 0 1292 537 0 1293 537 0 1290 1289 0 1291 1289 0 1292 1289 0 1293 1289 0
		 1295 546 0 1296 323 0 1297 542 0 1298 542 0 1295 1294 0 1296 1294 0 1297 1294 0 1298 1294 0
		 1300 551 0 1301 327 0 1302 547 0 1303 547 0 1300 1299 0 1301 1299 0 1302 1299 0 1303 1299 0
		 1305 556 0 1306 330 0 1307 552 0 1308 552 0 1305 1304 0 1306 1304 0 1307 1304 0 1308 1304 0
		 1310 561 0 1311 333 0 1312 557 0 1313 557 0 1310 1309 0 1311 1309 0 1312 1309 0 1313 1309 0
		 1315 566 0 1316 267 0 1317 562 0 1318 562 0 1315 1314 0 1316 1314 0 1317 1314 0 1318 1314 0
		 1320 571 0 1321 340 0 1322 567 0 1323 567 0 1320 1319 0 1321 1319 0 1322 1319 0 1323 1319 0
		 1325 576 0 1326 344 0 1327 572 0 1328 572 0 1325 1324 0 1326 1324 0 1327 1324 0 1328 1324 0
		 1330 581 0 1331 348 0 1332 577 0 1333 577 0 1330 1329 0 1331 1329 0 1332 1329 0 1333 1329 0
		 1335 586 0 1336 351 0 1337 582 0 1338 582 0 1335 1334 0 1336 1334 0 1337 1334 0 1338 1334 0
		 1340 591 0 1341 354 0 1342 587 0 1343 587 0 1340 1339 0 1341 1339 0 1342 1339 0 1343 1339 0
		 1345 596 0 1346 272 0 1347 592 0 1348 592 0;
	setAttr ".ed[2490:2655]" 1345 1344 0 1346 1344 0 1347 1344 0 1348 1344 0 1350 601 0
		 1351 361 0 1352 597 0 1353 597 0 1350 1349 0 1351 1349 0 1352 1349 0 1353 1349 0
		 1355 606 0 1356 365 0 1357 602 0 1358 602 0 1355 1354 0 1356 1354 0 1357 1354 0 1358 1354 0
		 1360 611 0 1361 369 0 1362 607 0 1363 607 0 1360 1359 0 1361 1359 0 1362 1359 0 1363 1359 0
		 1365 616 0 1366 372 0 1367 612 0 1368 612 0 1365 1364 0 1366 1364 0 1367 1364 0 1368 1364 0
		 1370 621 0 1371 375 0 1372 617 0 1373 617 0 1370 1369 0 1371 1369 0 1372 1369 0 1373 1369 0
		 1375 626 0 1376 277 0 1377 622 0 1378 622 0 1375 1374 0 1376 1374 0 1377 1374 0 1378 1374 0
		 1380 631 0 1381 382 0 1382 627 0 1383 627 0 1380 1379 0 1381 1379 0 1382 1379 0 1383 1379 0
		 1385 636 0 1386 386 0 1387 632 0 1388 632 0 1385 1384 0 1386 1384 0 1387 1384 0 1388 1384 0
		 1390 641 0 1391 390 0 1392 637 0 1393 637 0 1390 1389 0 1391 1389 0 1392 1389 0 1393 1389 0
		 1395 646 0 1396 393 0 1397 642 0 1398 642 0 1395 1394 0 1396 1394 0 1397 1394 0 1398 1394 0
		 1400 651 0 1401 396 0 1402 647 0 1403 647 0 1400 1399 0 1401 1399 0 1402 1399 0 1403 1399 0
		 1405 656 0 1406 282 0 1407 652 0 1408 652 0 1405 1404 0 1406 1404 0 1407 1404 0 1408 1404 0
		 1410 661 0 1411 403 0 1412 657 0 1413 657 0 1410 1409 0 1411 1409 0 1412 1409 0 1413 1409 0
		 1415 666 0 1416 407 0 1417 662 0 1418 662 0 1415 1414 0 1416 1414 0 1417 1414 0 1418 1414 0
		 1420 671 0 1421 411 0 1422 667 0 1423 667 0 1420 1419 0 1421 1419 0 1422 1419 0 1423 1419 0
		 1425 676 0 1426 414 0 1427 672 0 1428 672 0 1425 1424 0 1426 1424 0 1427 1424 0 1428 1424 0
		 1430 681 0 1431 417 0 1432 677 0 1433 677 0 1430 1429 0 1431 1429 0 1432 1429 0 1433 1429 0
		 1435 686 0 1436 287 0 1437 682 0 1438 682 0 1435 1434 0 1436 1434 0 1437 1434 0 1438 1434 0
		 1440 691 0 1441 424 0 1442 687 0 1443 687 0 1440 1439 0 1441 1439 0 1442 1439 0 1443 1439 0
		 1445 696 0 1446 428 0 1447 692 0 1448 692 0 1445 1444 0 1446 1444 0 1447 1444 0 1448 1444 0
		 1450 701 0 1451 432 0;
	setAttr ".ed[2656:2821]" 1452 697 0 1453 697 0 1450 1449 0 1451 1449 0 1452 1449 0
		 1453 1449 0 1455 706 0 1456 435 0 1457 702 0 1458 702 0 1455 1454 0 1456 1454 0 1457 1454 0
		 1458 1454 0 1460 711 0 1461 438 0 1462 707 0 1463 707 0 1460 1459 0 1461 1459 0 1462 1459 0
		 1463 1459 0 1465 716 0 1466 292 0 1467 712 0 1468 712 0 1465 1464 0 1466 1464 0 1467 1464 0
		 1468 1464 0 1470 721 0 1471 445 0 1472 717 0 1473 717 0 1470 1469 0 1471 1469 0 1472 1469 0
		 1473 1469 0 1475 726 0 1476 449 0 1477 722 0 1478 722 0 1475 1474 0 1476 1474 0 1477 1474 0
		 1478 1474 0 1480 731 0 1481 453 0 1482 727 0 1483 727 0 1480 1479 0 1481 1479 0 1482 1479 0
		 1483 1479 0 1485 736 0 1486 456 0 1487 732 0 1488 732 0 1485 1484 0 1486 1484 0 1487 1484 0
		 1488 1484 0 1490 741 0 1491 459 0 1492 737 0 1493 737 0 1490 1489 0 1491 1489 0 1492 1489 0
		 1493 1489 0 1495 465 0 1496 255 0 1497 742 0 1498 742 0 1495 1494 0 1496 1494 0 1497 1494 0
		 1498 1494 0 1500 745 0 1501 256 1 1502 746 0 1503 746 0 1500 1499 0 1501 1499 0 1502 1499 0
		 1503 1499 0 1505 748 0 1506 750 0 1507 749 0 1508 749 0 1505 1504 0 1506 1504 0 1507 1504 0
		 1508 1504 0 1510 470 0 1511 260 0 1512 752 0 1513 752 0 1510 1509 0 1511 1509 0 1512 1509 0
		 1513 1509 0 1515 755 0 1516 261 1 1517 756 0 1518 756 0 1515 1514 0 1516 1514 0 1517 1514 0
		 1518 1514 0 1520 758 0 1521 760 0 1522 759 0 1523 759 0 1520 1519 0 1521 1519 0 1522 1519 0
		 1523 1519 0 1525 475 0 1526 265 0 1527 762 0 1528 762 0 1525 1524 0 1526 1524 0 1527 1524 0
		 1528 1524 0 1530 765 0 1531 266 0 1532 766 0 1533 766 0 1530 1529 0 1531 1529 0 1532 1529 0
		 1533 1529 0 1535 768 0 1536 770 0 1537 769 0 1538 769 0 1535 1534 0 1536 1534 0 1537 1534 0
		 1538 1534 0 1540 480 0 1541 270 0 1542 772 0 1543 772 0 1540 1539 0 1541 1539 0 1542 1539 0
		 1543 1539 0 1545 775 0 1546 271 0 1547 776 0 1548 776 0 1545 1544 0 1546 1544 0 1547 1544 0
		 1548 1544 0 1550 778 0 1551 780 0 1552 779 0 1553 779 0 1550 1549 0 1551 1549 0 1552 1549 0
		 1553 1549 0;
	setAttr ".ed[2822:2987]" 1555 485 0 1556 275 0 1557 782 0 1558 782 0 1555 1554 0
		 1556 1554 0 1557 1554 0 1558 1554 0 1560 785 0 1561 276 1 1562 786 0 1563 786 0 1560 1559 0
		 1561 1559 0 1562 1559 0 1563 1559 0 1565 788 0 1566 790 0 1567 789 0 1568 789 0 1565 1564 0
		 1566 1564 0 1567 1564 0 1568 1564 0 1570 490 0 1571 280 0 1572 792 0 1573 792 0 1570 1569 0
		 1571 1569 0 1572 1569 0 1573 1569 0 1575 795 0 1576 281 1 1577 796 0 1578 796 0 1575 1574 0
		 1576 1574 0 1577 1574 0 1578 1574 0 1580 798 0 1581 800 0 1582 799 0 1583 799 0 1580 1579 0
		 1581 1579 0 1582 1579 0 1583 1579 0 1585 495 0 1586 285 0 1587 802 0 1588 802 0 1585 1584 0
		 1586 1584 0 1587 1584 0 1588 1584 0 1590 805 0 1591 286 0 1592 806 0 1593 806 0 1590 1589 0
		 1591 1589 0 1592 1589 0 1593 1589 0 1595 808 0 1596 810 0 1597 809 0 1598 809 0 1595 1594 0
		 1596 1594 0 1597 1594 0 1598 1594 0 1600 500 0 1601 290 0 1602 812 0 1603 812 0 1600 1599 0
		 1601 1599 0 1602 1599 0 1603 1599 0 1605 815 0 1606 291 0 1607 816 0 1608 816 0 1605 1604 0
		 1606 1604 0 1607 1604 0 1608 1604 0 1610 818 0 1611 820 0 1612 819 0 1613 819 0 1610 1609 0
		 1611 1609 0 1612 1609 0 1613 1609 0 1615 505 0 1616 295 1 1617 822 0 1618 822 0 1615 1614 0
		 1616 1614 0 1617 1614 0 1618 1614 0 1620 825 0 1621 296 1 1622 826 0 1623 826 0 1620 1619 0
		 1621 1619 0 1622 1619 0 1623 1619 0 1625 828 0 1626 830 0 1627 829 0 1628 829 0 1625 1624 0
		 1626 1624 0 1627 1624 0 1628 1624 0 1630 510 0 1631 299 1 1632 831 0 1633 831 0 1630 1629 0
		 1631 1629 0 1632 1629 0 1633 1629 0 1635 834 0 1636 300 1 1637 835 0 1638 835 0 1635 1634 0
		 1636 1634 0 1637 1634 0 1638 1634 0 1640 837 0 1641 839 0 1642 838 0 1643 838 0 1640 1639 0
		 1641 1639 0 1642 1639 0 1643 1639 0 1645 515 0 1646 303 1 1647 840 0 1648 840 0 1645 1644 0
		 1646 1644 0 1647 1644 0 1648 1644 0 1650 843 0 1651 304 1 1652 844 0 1653 844 0 1650 1649 0
		 1651 1649 0 1652 1649 0 1653 1649 0 1655 846 0 1656 848 0 1657 847 0 1658 847 0 1655 1654 0
		 1656 1654 0;
	setAttr ".ed[2988:3153]" 1657 1654 0 1658 1654 0 1660 520 0 1661 307 1 1662 849 0
		 1663 849 0 1660 1659 0 1661 1659 0 1662 1659 0 1663 1659 0 1665 851 0 1666 499 0
		 1667 852 0 1668 852 0 1665 1664 0 1666 1664 0 1667 1664 0 1668 1664 0 1670 853 0
		 1671 855 0 1672 854 0 1673 854 0 1670 1669 0 1671 1669 0 1672 1669 0 1673 1669 0
		 1675 525 0 1676 857 0 1677 856 0 1678 856 0 1675 1674 0 1676 1674 0 1677 1674 0 1678 1674 0
		 1680 859 0 1681 861 0 1682 860 0 1683 860 0 1680 1679 0 1681 1679 0 1682 1679 0 1683 1679 0
		 1685 862 0 1686 864 0 1687 863 0 1688 863 0 1685 1684 0 1686 1684 0 1687 1684 0 1688 1684 0
		 1690 530 0 1691 866 0 1692 865 0 1693 865 0 1690 1689 0 1691 1689 0 1692 1689 0 1693 1689 0
		 1695 868 0 1696 315 0 1697 869 0 1698 869 0 1695 1694 0 1696 1694 0 1697 1694 0 1698 1694 0
		 1700 870 0 1701 751 0 1702 871 0 1703 871 0 1700 1699 0 1701 1699 0 1702 1699 0 1703 1699 0
		 1705 535 0 1706 317 1 1707 872 0 1708 872 0 1705 1704 0 1706 1704 0 1707 1704 0 1708 1704 0
		 1710 875 0 1711 318 1 1712 876 0 1713 876 0 1710 1709 0 1711 1709 0 1712 1709 0 1713 1709 0
		 1715 878 0 1716 880 0 1717 879 0 1718 879 0 1715 1714 0 1716 1714 0 1717 1714 0 1718 1714 0
		 1720 540 0 1721 321 1 1722 881 0 1723 881 0 1720 1719 0 1721 1719 0 1722 1719 0 1723 1719 0
		 1725 884 0 1726 322 1 1727 885 0 1728 885 0 1725 1724 0 1726 1724 0 1727 1724 0 1728 1724 0
		 1730 887 0 1731 889 0 1732 888 0 1733 888 0 1730 1729 0 1731 1729 0 1732 1729 0 1733 1729 0
		 1735 545 0 1736 325 1 1737 890 0 1738 890 0 1735 1734 0 1736 1734 0 1737 1734 0 1738 1734 0
		 1740 893 0 1741 326 1 1742 894 0 1743 894 0 1740 1739 0 1741 1739 0 1742 1739 0 1743 1739 0
		 1745 896 0 1746 898 0 1747 897 0 1748 897 0 1745 1744 0 1746 1744 0 1747 1744 0 1748 1744 0
		 1750 550 0 1751 329 1 1752 899 0 1753 899 0 1750 1749 0 1751 1749 0 1752 1749 0 1753 1749 0
		 1755 901 0 1756 464 0 1757 902 0 1758 902 0 1755 1754 0 1756 1754 0 1757 1754 0 1758 1754 0
		 1760 903 0 1761 905 0 1762 904 0 1763 904 0;
	setAttr ".ed[3154:3319]" 1760 1759 0 1761 1759 0 1762 1759 0 1763 1759 0 1765 555 0
		 1766 867 0 1767 906 0 1768 906 0 1765 1764 0 1766 1764 0 1767 1764 0 1768 1764 0
		 1770 908 0 1771 910 0 1772 909 0 1773 909 0 1770 1769 0 1771 1769 0 1772 1769 0 1773 1769 0
		 1775 911 0 1776 913 0 1777 912 0 1778 912 0 1775 1774 0 1776 1774 0 1777 1774 0 1778 1774 0
		 1780 560 0 1781 915 0 1782 914 0 1783 914 0 1780 1779 0 1781 1779 0 1782 1779 0 1783 1779 0
		 1785 917 0 1786 336 0 1787 918 0 1788 918 0 1785 1784 0 1786 1784 0 1787 1784 0 1788 1784 0
		 1790 919 0 1791 761 0 1792 920 0 1793 920 0 1790 1789 0 1791 1789 0 1792 1789 0 1793 1789 0
		 1795 565 0 1796 338 0 1797 921 0 1798 921 0 1795 1794 0 1796 1794 0 1797 1794 0 1798 1794 0
		 1800 924 0 1801 339 0 1802 925 0 1803 925 0 1800 1799 0 1801 1799 0 1802 1799 0 1803 1799 0
		 1805 927 0 1806 929 0 1807 928 0 1808 928 0 1805 1804 0 1806 1804 0 1807 1804 0 1808 1804 0
		 1810 570 0 1811 342 0 1812 930 0 1813 930 0 1810 1809 0 1811 1809 0 1812 1809 0 1813 1809 0
		 1815 933 0 1816 343 0 1817 934 0 1818 934 0 1815 1814 0 1816 1814 0 1817 1814 0 1818 1814 0
		 1820 936 0 1821 938 0 1822 937 0 1823 937 0 1820 1819 0 1821 1819 0 1822 1819 0 1823 1819 0
		 1825 575 0 1826 346 0 1827 939 0 1828 939 0 1825 1824 0 1826 1824 0 1827 1824 0 1828 1824 0
		 1830 942 0 1831 347 0 1832 943 0 1833 943 0 1830 1829 0 1831 1829 0 1832 1829 0 1833 1829 0
		 1835 945 0 1836 947 0 1837 946 0 1838 946 0 1835 1834 0 1836 1834 0 1837 1834 0 1838 1834 0
		 1840 580 0 1841 350 0 1842 948 0 1843 948 0 1840 1839 0 1841 1839 0 1842 1839 0 1843 1839 0
		 1845 950 0 1846 469 0 1847 951 0 1848 951 0 1845 1844 0 1846 1844 0 1847 1844 0 1848 1844 0
		 1850 952 0 1851 954 0 1852 953 0 1853 953 0 1850 1849 0 1851 1849 0 1852 1849 0 1853 1849 0
		 1855 585 0 1856 916 0 1857 955 0 1858 955 0 1855 1854 0 1856 1854 0 1857 1854 0 1858 1854 0
		 1860 957 0 1861 959 0 1862 958 0 1863 958 0 1860 1859 0 1861 1859 0 1862 1859 0 1863 1859 0
		 1865 960 0 1866 962 0;
	setAttr ".ed[3320:3485]" 1867 961 0 1868 961 0 1865 1864 0 1866 1864 0 1867 1864 0
		 1868 1864 0 1870 590 0 1871 964 0 1872 963 0 1873 963 0 1870 1869 0 1871 1869 0 1872 1869 0
		 1873 1869 0 1875 966 0 1876 357 0 1877 967 0 1878 967 0 1875 1874 0 1876 1874 0 1877 1874 0
		 1878 1874 0 1880 968 0 1881 771 0 1882 969 0 1883 969 0 1880 1879 0 1881 1879 0 1882 1879 0
		 1883 1879 0 1885 595 0 1886 359 0 1887 970 0 1888 970 0 1885 1884 0 1886 1884 0 1887 1884 0
		 1888 1884 0 1890 973 0 1891 360 0 1892 974 0 1893 974 0 1890 1889 0 1891 1889 0 1892 1889 0
		 1893 1889 0 1895 976 0 1896 978 0 1897 977 0 1898 977 0 1895 1894 0 1896 1894 0 1897 1894 0
		 1898 1894 0 1900 600 0 1901 363 0 1902 979 0 1903 979 0 1900 1899 0 1901 1899 0 1902 1899 0
		 1903 1899 0 1905 982 0 1906 364 0 1907 983 0 1908 983 0 1905 1904 0 1906 1904 0 1907 1904 0
		 1908 1904 0 1910 985 0 1911 987 0 1912 986 0 1913 986 0 1910 1909 0 1911 1909 0 1912 1909 0
		 1913 1909 0 1915 605 0 1916 367 0 1917 988 0 1918 988 0 1915 1914 0 1916 1914 0 1917 1914 0
		 1918 1914 0 1920 991 0 1921 368 0 1922 992 0 1923 992 0 1920 1919 0 1921 1919 0 1922 1919 0
		 1923 1919 0 1925 994 0 1926 996 0 1927 995 0 1928 995 0 1925 1924 0 1926 1924 0 1927 1924 0
		 1928 1924 0 1930 610 0 1931 371 0 1932 997 0 1933 997 0 1930 1929 0 1931 1929 0 1932 1929 0
		 1933 1929 0 1935 999 0 1936 474 0 1937 1000 0 1938 1000 0 1935 1934 0 1936 1934 0
		 1937 1934 0 1938 1934 0 1940 1001 0 1941 1003 0 1942 1002 0 1943 1002 0 1940 1939 0
		 1941 1939 0 1942 1939 0 1943 1939 0 1945 615 0 1946 965 0 1947 1004 0 1948 1004 0
		 1945 1944 0 1946 1944 0 1947 1944 0 1948 1944 0 1950 1006 0 1951 1008 0 1952 1007 0
		 1953 1007 0 1950 1949 0 1951 1949 0 1952 1949 0 1953 1949 0 1955 1009 0 1956 1011 0
		 1957 1010 0 1958 1010 0 1955 1954 0 1956 1954 0 1957 1954 0 1958 1954 0 1960 620 0
		 1961 1013 0 1962 1012 0 1963 1012 0 1960 1959 0 1961 1959 0 1962 1959 0 1963 1959 0
		 1965 1015 0 1966 378 0 1967 1016 0 1968 1016 0 1965 1964 0 1966 1964 0 1967 1964 0
		 1968 1964 0;
	setAttr ".ed[3486:3651]" 1970 1017 0 1971 781 0 1972 1018 0 1973 1018 0 1970 1969 0
		 1971 1969 0 1972 1969 0 1973 1969 0 1975 625 0 1976 380 1 1977 1019 0 1978 1019 0
		 1975 1974 0 1976 1974 0 1977 1974 0 1978 1974 0 1980 1022 0 1981 381 1 1982 1023 0
		 1983 1023 0 1980 1979 0 1981 1979 0 1982 1979 0 1983 1979 0 1985 1025 0 1986 1027 0
		 1987 1026 0 1988 1026 0 1985 1984 0 1986 1984 0 1987 1984 0 1988 1984 0 1990 630 0
		 1991 384 1 1992 1028 0 1993 1028 0 1990 1989 0 1991 1989 0 1992 1989 0 1993 1989 0
		 1995 1031 0 1996 385 1 1997 1032 0 1998 1032 0 1995 1994 0 1996 1994 0 1997 1994 0
		 1998 1994 0 2000 1034 0 2001 1036 0 2002 1035 0 2003 1035 0 2000 1999 0 2001 1999 0
		 2002 1999 0 2003 1999 0 2005 635 0 2006 388 1 2007 1037 0 2008 1037 0 2005 2004 0
		 2006 2004 0 2007 2004 0 2008 2004 0 2010 1040 0 2011 389 1 2012 1041 0 2013 1041 0
		 2010 2009 0 2011 2009 0 2012 2009 0 2013 2009 0 2015 1043 0 2016 1045 0 2017 1044 0
		 2018 1044 0 2015 2014 0 2016 2014 0 2017 2014 0 2018 2014 0 2020 640 0 2021 392 1
		 2022 1046 0 2023 1046 0 2020 2019 0 2021 2019 0 2022 2019 0 2023 2019 0 2025 1048 0
		 2026 479 0 2027 1049 0 2028 1049 0 2025 2024 0 2026 2024 0 2027 2024 0 2028 2024 0
		 2030 1050 0 2031 1052 0 2032 1051 0 2033 1051 0 2030 2029 0 2031 2029 0 2032 2029 0
		 2033 2029 0 2035 645 0 2036 1014 0 2037 1053 0 2038 1053 0 2035 2034 0 2036 2034 0
		 2037 2034 0 2038 2034 0 2040 1055 0 2041 1057 0 2042 1056 0 2043 1056 0 2040 2039 0
		 2041 2039 0 2042 2039 0 2043 2039 0 2045 1058 0 2046 1060 0 2047 1059 0 2048 1059 0
		 2045 2044 0 2046 2044 0 2047 2044 0 2048 2044 0 2050 650 0 2051 1062 0 2052 1061 0
		 2053 1061 0 2050 2049 0 2051 2049 0 2052 2049 0 2053 2049 0 2055 1064 0 2056 399 0
		 2057 1065 0 2058 1065 0 2055 2054 0 2056 2054 0 2057 2054 0 2058 2054 0 2060 1066 0
		 2061 791 0 2062 1067 0 2063 1067 0 2060 2059 0 2061 2059 0 2062 2059 0 2063 2059 0
		 2065 655 0 2066 401 1 2067 1068 0 2068 1068 0 2065 2064 0 2066 2064 0 2067 2064 0
		 2068 2064 0 2070 1071 0 2071 402 1 2072 1072 0 2073 1072 0 2070 2069 0 2071 2069 0;
	setAttr ".ed[3652:3817]" 2072 2069 0 2073 2069 0 2075 1074 0 2076 1076 0 2077 1075 0
		 2078 1075 0 2075 2074 0 2076 2074 0 2077 2074 0 2078 2074 0 2080 660 0 2081 405 1
		 2082 1077 0 2083 1077 0 2080 2079 0 2081 2079 0 2082 2079 0 2083 2079 0 2085 1080 0
		 2086 406 1 2087 1081 0 2088 1081 0 2085 2084 0 2086 2084 0 2087 2084 0 2088 2084 0
		 2090 1083 0 2091 1085 0 2092 1084 0 2093 1084 0 2090 2089 0 2091 2089 0 2092 2089 0
		 2093 2089 0 2095 665 0 2096 409 1 2097 1086 0 2098 1086 0 2095 2094 0 2096 2094 0
		 2097 2094 0 2098 2094 0 2100 1089 0 2101 410 1 2102 1090 0 2103 1090 0 2100 2099 0
		 2101 2099 0 2102 2099 0 2103 2099 0 2105 1092 0 2106 1094 0 2107 1093 0 2108 1093 0
		 2105 2104 0 2106 2104 0 2107 2104 0 2108 2104 0 2110 670 0 2111 413 1 2112 1095 0
		 2113 1095 0 2110 2109 0 2111 2109 0 2112 2109 0 2113 2109 0 2115 1097 0 2116 484 0
		 2117 1098 0 2118 1098 0 2115 2114 0 2116 2114 0 2117 2114 0 2118 2114 0 2120 1099 0
		 2121 1101 0 2122 1100 0 2123 1100 0 2120 2119 0 2121 2119 0 2122 2119 0 2123 2119 0
		 2125 675 0 2126 1063 0 2127 1102 0 2128 1102 0 2125 2124 0 2126 2124 0 2127 2124 0
		 2128 2124 0 2130 1104 0 2131 1106 0 2132 1105 0 2133 1105 0 2130 2129 0 2131 2129 0
		 2132 2129 0 2133 2129 0 2135 1107 0 2136 1109 0 2137 1108 0 2138 1108 0 2135 2134 0
		 2136 2134 0 2137 2134 0 2138 2134 0 2140 680 0 2141 1111 0 2142 1110 0 2143 1110 0
		 2140 2139 0 2141 2139 0 2142 2139 0 2143 2139 0 2145 1113 0 2146 420 0 2147 1114 0
		 2148 1114 0 2145 2144 0 2146 2144 0 2147 2144 0 2148 2144 0 2150 1115 0 2151 801 0
		 2152 1116 0 2153 1116 0 2150 2149 0 2151 2149 0 2152 2149 0 2153 2149 0 2155 685 0
		 2156 422 0 2157 1117 0 2158 1117 0 2155 2154 0 2156 2154 0 2157 2154 0 2158 2154 0
		 2160 1120 0 2161 423 0 2162 1121 0 2163 1121 0 2160 2159 0 2161 2159 0 2162 2159 0
		 2163 2159 0 2165 1123 0 2166 1125 0 2167 1124 0 2168 1124 0 2165 2164 0 2166 2164 0
		 2167 2164 0 2168 2164 0 2170 690 0 2171 426 0 2172 1126 0 2173 1126 0 2170 2169 0
		 2171 2169 0 2172 2169 0 2173 2169 0 2175 1129 0 2176 427 0 2177 1130 0 2178 1130 0;
	setAttr ".ed[3818:3983]" 2175 2174 0 2176 2174 0 2177 2174 0 2178 2174 0 2180 1132 0
		 2181 1134 0 2182 1133 0 2183 1133 0 2180 2179 0 2181 2179 0 2182 2179 0 2183 2179 0
		 2185 695 0 2186 430 0 2187 1135 0 2188 1135 0 2185 2184 0 2186 2184 0 2187 2184 0
		 2188 2184 0 2190 1138 0 2191 431 0 2192 1139 0 2193 1139 0 2190 2189 0 2191 2189 0
		 2192 2189 0 2193 2189 0 2195 1141 0 2196 1143 0 2197 1142 0 2198 1142 0 2195 2194 0
		 2196 2194 0 2197 2194 0 2198 2194 0 2200 700 0 2201 434 0 2202 1144 0 2203 1144 0
		 2200 2199 0 2201 2199 0 2202 2199 0 2203 2199 0 2205 1146 0 2206 489 0 2207 1147 0
		 2208 1147 0 2205 2204 0 2206 2204 0 2207 2204 0 2208 2204 0 2210 1148 0 2211 1150 0
		 2212 1149 0 2213 1149 0 2210 2209 0 2211 2209 0 2212 2209 0 2213 2209 0 2215 705 0
		 2216 1112 0 2217 1151 0 2218 1151 0 2215 2214 0 2216 2214 0 2217 2214 0 2218 2214 0
		 2220 1153 0 2221 1155 0 2222 1154 0 2223 1154 0 2220 2219 0 2221 2219 0 2222 2219 0
		 2223 2219 0 2225 1156 0 2226 1158 0 2227 1157 0 2228 1157 0 2225 2224 0 2226 2224 0
		 2227 2224 0 2228 2224 0 2230 710 0 2231 1160 0 2232 1159 0 2233 1159 0 2230 2229 0
		 2231 2229 0 2232 2229 0 2233 2229 0 2235 1162 0 2236 441 0 2237 1163 0 2235 2234 0
		 2236 2234 0 2238 2234 0 2240 1164 0 2241 811 0 2242 1165 0 2243 1165 0 2240 2239 0
		 2241 2239 0 2242 2239 0 2243 2239 0 2245 715 0 2246 443 0 2247 1166 0 2248 1166 0
		 2245 2244 0 2246 2244 0 2247 2244 0 2248 2244 0 2250 1169 0 2251 444 0 2252 1170 0
		 2253 1170 0 2250 2249 0 2251 2249 0 2252 2249 0 2253 2249 0 2255 1172 0 2256 1174 0
		 2257 1173 0 2258 1173 0 2255 2254 0 2256 2254 0 2257 2254 0 2258 2254 0 2260 720 0
		 2261 447 0 2262 1175 0 2263 1175 0 2260 2259 0 2261 2259 0 2262 2259 0 2263 2259 0
		 2265 1178 0 2266 448 0 2267 1179 0 2268 1179 0 2265 2264 0 2266 2264 0 2267 2264 0
		 2268 2264 0 2270 1181 0 2271 1183 0 2272 1182 0 2273 1182 0 2270 2269 0 2271 2269 0
		 2272 2269 0 2273 2269 0 2275 725 0 2276 451 0 2277 1184 0 2278 1184 0 2275 2274 0
		 2276 2274 0 2277 2274 0 2278 2274 0 2280 1187 0 2281 452 0 2282 1188 0 2283 1188 0;
	setAttr ".ed[3984:4149]" 2280 2279 0 2281 2279 0 2282 2279 0 2283 2279 0 2285 1190 0
		 2286 1192 0 2287 1191 0 2288 1191 0 2285 2284 0 2286 2284 0 2287 2284 0 2288 2284 0
		 2290 730 0 2291 455 0 2292 1193 0 2293 1193 0 2290 2289 0 2291 2289 0 2292 2289 0
		 2293 2289 0 2295 1195 0 2296 494 0 2297 1196 0 2298 1196 0 2295 2294 0 2296 2294 0
		 2297 2294 0 2298 2294 0 2300 1197 0 2301 1199 0 2302 1198 0 2303 1198 0 2300 2299 0
		 2301 2299 0 2302 2299 0 2303 2299 0 2305 735 0 2306 1161 0 2307 1200 0 2308 1200 0
		 2305 2304 0 2306 2304 0 2307 2304 0 2308 2304 0 2310 1202 0 2311 1204 0 2312 1203 0
		 2313 1203 0 2310 2309 0 2311 2309 0 2312 2309 0 2313 2309 0 2315 1205 0 2316 1207 0
		 2317 1206 0 2318 1206 0 2315 2314 0 2316 2314 0 2317 2314 0 2318 2314 0 2320 740 0
		 2321 1209 0 2322 1208 0 2323 1208 0 2320 2319 0 2321 2319 0 2322 2319 0 2323 2319 0
		 2325 1210 0 2326 310 0 2327 1211 0 2328 1211 0 2325 2324 0 2326 2324 0 2327 2324 0
		 2328 2324 0 2330 1212 0 2331 821 0 2332 1213 0 2333 1213 0 2330 2329 0 2331 2329 0
		 2332 2329 0 2333 2329 0 2335 463 0 2336 205 0 2337 462 0 2335 2334 0 2336 2334 0
		 2337 2334 0 1217 2334 0 2339 462 0 1756 2338 0 1495 2338 0 2339 2338 0 2337 2338 0
		 2341 254 0 2342 254 0 2341 2340 0 2342 2340 0 1218 2340 0 2339 2340 0 2344 468 0
		 2345 212 0 2346 467 0 2344 2343 0 2345 2343 0 2346 2343 0 1222 2343 0 2348 467 0
		 1846 2347 0 1510 2347 0 2348 2347 0 2346 2347 0 2350 259 0 2351 259 0 2350 2349 0
		 2351 2349 0 1223 2349 0 2348 2349 0 2353 473 0 2354 219 0 2355 472 0 2353 2352 0
		 2354 2352 0 2355 2352 0 1227 2352 0 2357 472 0 1936 2356 0 1525 2356 0 2357 2356 0
		 2355 2356 0 2359 264 0 2360 264 0 2359 2358 0 2360 2358 0 1228 2358 0 2357 2358 0
		 2362 478 0 2363 226 0 2364 477 0 2362 2361 0 2363 2361 0 2364 2361 0 1232 2361 0
		 2366 477 0 2026 2365 0 1540 2365 0 2366 2365 0 2364 2365 0 2368 269 0 2369 269 0
		 2368 2367 0 2369 2367 0 1233 2367 0 2366 2367 0 2371 483 0 2372 233 0 2373 482 0
		 2371 2370 0 2372 2370 0 2373 2370 0 1237 2370 0 2375 482 0 2116 2374 0 1555 2374 0;
	setAttr ".ed[4150:4315]" 2375 2374 0 2373 2374 0 2377 274 0 2378 274 0 2377 2376 0
		 2378 2376 0 1238 2376 0 2375 2376 0 2380 488 0 2381 240 0 2382 487 0 2380 2379 0
		 2381 2379 0 2382 2379 0 1242 2379 0 2384 487 0 2206 2383 0 1570 2383 0 2384 2383 0
		 2382 2383 0 2386 279 0 2387 279 0 2386 2385 0 2387 2385 0 1243 2385 0 2384 2385 0
		 2389 493 0 2390 247 0 2391 492 0 2389 2388 0 2390 2388 0 2391 2388 0 1247 2388 0
		 2393 492 0 2296 2392 0 1585 2392 0 2393 2392 0 2391 2392 0 2395 284 0 2396 284 0
		 2395 2394 0 2396 2394 0 1248 2394 0 2393 2394 0 2398 498 0 2399 203 0 2400 497 0
		 2398 2397 0 2399 2397 0 2400 2397 0 1252 2397 0 2402 497 0 1666 2401 0 1600 2401 0
		 2402 2401 0 2400 2401 0 2404 289 0 2405 289 0 2404 2403 0 2405 2403 0 1253 2403 0
		 2402 2403 0 2407 503 0 2408 199 1 2409 502 0 2407 2406 0 2408 2406 0 2409 2406 0
		 1257 2406 0 2411 504 1 2412 502 0 2411 2410 0 1615 2410 0 2412 2410 0 2409 2410 0
		 2414 294 0 2415 294 0 2414 2413 0 2415 2413 0 1258 2413 0 2412 2413 0 2417 508 0
		 2418 200 1 2419 507 0 2417 2416 0 2418 2416 0 2419 2416 0 1262 2416 0 2421 509 1
		 2422 507 0 2421 2420 0 1630 2420 0 2422 2420 0 2419 2420 0 2424 298 0 2425 298 0
		 2424 2423 0 2425 2423 0 1263 2423 0 2422 2423 0 2427 513 0 2428 201 1 2429 512 0
		 2427 2426 0 2428 2426 0 2429 2426 0 1267 2426 0 2431 514 1 2432 512 0 2431 2430 0
		 1645 2430 0 2432 2430 0 2429 2430 0 2434 302 0 2435 302 0 2434 2433 0 2435 2433 0
		 1268 2433 0 2432 2433 0 2437 518 0 2438 202 1 2439 517 0 2437 2436 0 2438 2436 0
		 2439 2436 0 1272 2436 0 2441 519 1 2442 517 0 2441 2440 0 1660 2440 0 2442 2440 0
		 2439 2440 0 2444 306 0 2445 306 0 2444 2443 0 2445 2443 0 1273 2443 0 2442 2443 0
		 2447 523 0 2448 524 0 2449 522 0 2447 2446 0 2448 2446 0 2449 2446 0 1277 2446 0
		 2451 522 0 2326 2450 0 1675 2450 0 2451 2450 0 2449 2450 0 2453 309 0 2454 309 0
		 2453 2452 0 2454 2452 0 1278 2452 0 2451 2452 0 2456 528 0 2457 529 0 2458 527 0
		 2456 2455 0 2457 2455 0 2458 2455 0 1282 2455 0 2460 314 0 2461 527 0 2460 2459 0;
	setAttr ".ed[4316:4481]" 1690 2459 0 2461 2459 0 2458 2459 0 2463 313 0 2464 313 0
		 2463 2462 0 2464 2462 0 1283 2462 0 2461 2462 0 2466 533 0 2467 207 1 2468 532 0
		 2466 2465 0 2467 2465 0 2468 2465 0 1287 2465 0 2470 534 1 2471 532 0 2470 2469 0
		 1705 2469 0 2471 2469 0 2468 2469 0 2473 316 0 2474 316 0 2473 2472 0 2474 2472 0
		 1288 2472 0 2471 2472 0 2476 538 0 2477 208 1 2478 537 0 2476 2475 0 2477 2475 0
		 2478 2475 0 1292 2475 0 2480 539 1 2481 537 0 2480 2479 0 1720 2479 0 2481 2479 0
		 2478 2479 0 2483 320 0 2484 320 0 2483 2482 0 2484 2482 0 1293 2482 0 2481 2482 0
		 2486 543 0 2487 209 1 2488 542 0 2486 2485 0 2487 2485 0 2488 2485 0 1297 2485 0
		 2490 544 1 2491 542 0 2490 2489 0 1735 2489 0 2491 2489 0 2488 2489 0 2493 324 0
		 2494 324 0 2493 2492 0 2494 2492 0 1298 2492 0 2491 2492 0 2496 548 0 2497 210 1
		 2498 547 0 2496 2495 0 2497 2495 0 2498 2495 0 1302 2495 0 2500 549 1 2501 547 0
		 2500 2499 0 1750 2499 0 2501 2499 0 2498 2499 0 2503 328 0 2504 328 0 2503 2502 0
		 2504 2502 0 1303 2502 0 2501 2502 0 2506 553 0 2507 554 0 2508 552 0 2506 2505 0
		 2507 2505 0 2508 2505 0 1307 2505 0 2510 552 0 1696 2509 0 1765 2509 0 2510 2509 0
		 2508 2509 0 2512 331 0 2513 331 0 2512 2511 0 2513 2511 0 1308 2511 0 2510 2511 0
		 2515 558 0 2516 559 0 2517 557 0 2515 2514 0 2516 2514 0 2517 2514 0 1312 2514 0
		 2519 335 0 2520 557 0 2519 2518 0 1780 2518 0 2520 2518 0 2517 2518 0 2522 334 0
		 2523 334 0 2522 2521 0 2523 2521 0 1313 2521 0 2520 2521 0 2525 563 0 2526 214 0
		 2527 562 0 2525 2524 0 2526 2524 0 2527 2524 0 1317 2524 0 2529 564 0 2530 562 0
		 2529 2528 0 1795 2528 0 2530 2528 0 2527 2528 0 2532 337 0 2533 337 0 2532 2531 0
		 2533 2531 0 1318 2531 0 2530 2531 0 2535 568 0 2536 215 0 2537 567 0 2535 2534 0
		 2536 2534 0 2537 2534 0 1322 2534 0 2539 569 0 2540 567 0 2539 2538 0 1810 2538 0
		 2540 2538 0 2537 2538 0 2542 341 0 2543 341 0 2542 2541 0 2543 2541 0 1323 2541 0
		 2540 2541 0 2545 573 0 2546 216 0 2547 572 0 2545 2544 0 2546 2544 0 2547 2544 0;
	setAttr ".ed[4482:4647]" 1327 2544 0 2549 574 0 2550 572 0 2549 2548 0 1825 2548 0
		 2550 2548 0 2547 2548 0 2552 345 0 2553 345 0 2552 2551 0 2553 2551 0 1328 2551 0
		 2550 2551 0 2555 578 0 2556 217 0 2557 577 0 2555 2554 0 2556 2554 0 2557 2554 0
		 1332 2554 0 2559 579 0 2560 577 0 2559 2558 0 1840 2558 0 2560 2558 0 2557 2558 0
		 2562 349 0 2563 349 0 2562 2561 0 2563 2561 0 1333 2561 0 2560 2561 0 2565 583 0
		 2566 584 0 2567 582 0 2565 2564 0 2566 2564 0 2567 2564 0 1337 2564 0 2569 582 0
		 1786 2568 0 1855 2568 0 2569 2568 0 2567 2568 0 2571 352 0 2572 352 0 2571 2570 0
		 2572 2570 0 1338 2570 0 2569 2570 0 2574 588 0 2575 589 0 2576 587 0 2574 2573 0
		 2575 2573 0 2576 2573 0 1342 2573 0 2578 356 0 2579 587 0 2578 2577 0 1870 2577 0
		 2579 2577 0 2576 2577 0 2581 355 0 2582 355 0 2581 2580 0 2582 2580 0 1343 2580 0
		 2579 2580 0 2584 593 0 2585 221 0 2586 592 0 2584 2583 0 2585 2583 0 2586 2583 0
		 1347 2583 0 2588 594 0 2589 592 0 2588 2587 0 1885 2587 0 2589 2587 0 2586 2587 0
		 2591 358 0 2592 358 0 2591 2590 0 2592 2590 0 1348 2590 0 2589 2590 0 2594 598 0
		 2595 222 0 2596 597 0 2594 2593 0 2595 2593 0 2596 2593 0 1352 2593 0 2598 599 0
		 2599 597 0 2598 2597 0 1900 2597 0 2599 2597 0 2596 2597 0 2601 362 0 2602 362 0
		 2601 2600 0 2602 2600 0 1353 2600 0 2599 2600 0 2604 603 0 2605 223 0 2606 602 0
		 2604 2603 0 2605 2603 0 2606 2603 0 1357 2603 0 2608 604 0 2609 602 0 2608 2607 0
		 1915 2607 0 2609 2607 0 2606 2607 0 2611 366 0 2612 366 0 2611 2610 0 2612 2610 0
		 1358 2610 0 2609 2610 0 2614 608 0 2615 224 0 2616 607 0 2614 2613 0 2615 2613 0
		 2616 2613 0 1362 2613 0 2618 609 0 2619 607 0 2618 2617 0 1930 2617 0 2619 2617 0
		 2616 2617 0 2621 370 0 2622 370 0 2621 2620 0 2622 2620 0 1363 2620 0 2619 2620 0
		 2624 613 0 2625 614 0 2626 612 0 2624 2623 0 2625 2623 0 2626 2623 0 1367 2623 0
		 2628 612 0 1876 2627 0 1945 2627 0 2628 2627 0 2626 2627 0 2630 373 0 2631 373 0
		 2630 2629 0 2631 2629 0 1368 2629 0 2628 2629 0 2633 618 0 2634 619 0 2635 617 0;
	setAttr ".ed[4648:4813]" 2633 2632 0 2634 2632 0 2635 2632 0 1372 2632 0 2637 377 0
		 2638 617 0 2637 2636 0 1960 2636 0 2638 2636 0 2635 2636 0 2640 376 0 2641 376 0
		 2640 2639 0 2641 2639 0 1373 2639 0 2638 2639 0 2643 623 0 2644 228 1 2645 622 0
		 2643 2642 0 2644 2642 0 2645 2642 0 1377 2642 0 2647 624 1 2648 622 0 2647 2646 0
		 1975 2646 0 2648 2646 0 2645 2646 0 2650 379 0 2651 379 0 2650 2649 0 2651 2649 0
		 1378 2649 0 2648 2649 0 2653 628 0 2654 229 1 2655 627 0 2653 2652 0 2654 2652 0
		 2655 2652 0 1382 2652 0 2657 629 1 2658 627 0 2657 2656 0 1990 2656 0 2658 2656 0
		 2655 2656 0 2660 383 0 2661 383 0 2660 2659 0 2661 2659 0 1383 2659 0 2658 2659 0
		 2663 633 0 2664 230 1 2665 632 0 2663 2662 0 2664 2662 0 2665 2662 0 1387 2662 0
		 2667 634 1 2668 632 0 2667 2666 0 2005 2666 0 2668 2666 0 2665 2666 0 2670 387 0
		 2671 387 0 2670 2669 0 2671 2669 0 1388 2669 0 2668 2669 0 2673 638 0 2674 231 1
		 2675 637 0 2673 2672 0 2674 2672 0 2675 2672 0 1392 2672 0 2677 639 1 2678 637 0
		 2677 2676 0 2020 2676 0 2678 2676 0 2675 2676 0 2680 391 0 2681 391 0 2680 2679 0
		 2681 2679 0 1393 2679 0 2678 2679 0 2683 643 0 2684 644 0 2685 642 0 2683 2682 0
		 2684 2682 0 2685 2682 0 1397 2682 0 2687 642 0 1966 2686 0 2035 2686 0 2687 2686 0
		 2685 2686 0 2689 394 0 2690 394 0 2689 2688 0 2690 2688 0 1398 2688 0 2687 2688 0
		 2692 648 0 2693 649 0 2694 647 0 2692 2691 0 2693 2691 0 2694 2691 0 1402 2691 0
		 2696 398 0 2697 647 0 2696 2695 0 2050 2695 0 2697 2695 0 2694 2695 0 2699 397 0
		 2700 397 0 2699 2698 0 2700 2698 0 1403 2698 0 2697 2698 0 2702 653 0 2703 235 1
		 2704 652 0 2702 2701 0 2703 2701 0 2704 2701 0 1407 2701 0 2706 654 1 2707 652 0
		 2706 2705 0 2065 2705 0 2707 2705 0 2704 2705 0 2709 400 0 2710 400 0 2709 2708 0
		 2710 2708 0 1408 2708 0 2707 2708 0 2712 658 0 2713 236 1 2714 657 0 2712 2711 0
		 2713 2711 0 2714 2711 0 1412 2711 0 2716 659 1 2717 657 0 2716 2715 0 2080 2715 0
		 2717 2715 0 2714 2715 0 2719 404 0 2720 404 0 2719 2718 0 2720 2718 0 1413 2718 0;
	setAttr ".ed[4814:4979]" 2717 2718 0 2722 663 0 2723 237 1 2724 662 0 2722 2721 0
		 2723 2721 0 2724 2721 0 1417 2721 0 2726 664 1 2727 662 0 2726 2725 0 2095 2725 0
		 2727 2725 0 2724 2725 0 2729 408 0 2730 408 0 2729 2728 0 2730 2728 0 1418 2728 0
		 2727 2728 0 2732 668 0 2733 238 1 2734 667 0 2732 2731 0 2733 2731 0 2734 2731 0
		 1422 2731 0 2736 669 1 2737 667 0 2736 2735 0 2110 2735 0 2737 2735 0 2734 2735 0
		 2739 412 0 2740 412 0 2739 2738 0 2740 2738 0 1423 2738 0 2737 2738 0 2742 673 0
		 2743 674 0 2744 672 0 2742 2741 0 2743 2741 0 2744 2741 0 1427 2741 0 2746 672 0
		 2056 2745 0 2125 2745 0 2746 2745 0 2744 2745 0 2748 415 0 2749 415 0 2748 2747 0
		 2749 2747 0 1428 2747 0 2746 2747 0 2751 678 0 2752 679 0 2753 677 0 2751 2750 0
		 2752 2750 0 2753 2750 0 1432 2750 0 2755 419 0 2756 677 0 2755 2754 0 2140 2754 0
		 2756 2754 0 2753 2754 0 2758 418 0 2759 418 0 2758 2757 0 2759 2757 0 1433 2757 0
		 2756 2757 0 2761 683 0 2762 242 0 2763 682 0 2761 2760 0 2762 2760 0 2763 2760 0
		 1437 2760 0 2765 684 0 2766 682 0 2765 2764 0 2155 2764 0 2766 2764 0 2763 2764 0
		 2768 421 0 2769 421 0 2768 2767 0 2769 2767 0 1438 2767 0 2766 2767 0 2771 688 0
		 2772 243 0 2773 687 0 2771 2770 0 2772 2770 0 2773 2770 0 1442 2770 0 2775 689 0
		 2776 687 0 2775 2774 0 2170 2774 0 2776 2774 0 2773 2774 0 2778 425 0 2779 425 0
		 2778 2777 0 2779 2777 0 1443 2777 0 2776 2777 0 2781 693 0 2782 244 0 2783 692 0
		 2781 2780 0 2782 2780 0 2783 2780 0 1447 2780 0 2785 694 0 2786 692 0 2785 2784 0
		 2185 2784 0 2786 2784 0 2783 2784 0 2788 429 0 2789 429 0 2788 2787 0 2789 2787 0
		 1448 2787 0 2786 2787 0 2791 698 0 2792 245 0 2793 697 0 2791 2790 0 2792 2790 0
		 2793 2790 0 1452 2790 0 2795 699 0 2796 697 0 2795 2794 0 2200 2794 0 2796 2794 0
		 2793 2794 0 2798 433 0 2799 433 0 2798 2797 0 2799 2797 0 1453 2797 0 2796 2797 0
		 2801 703 0 2802 704 0 2803 702 0 2801 2800 0 2802 2800 0 2803 2800 0 1457 2800 0
		 2805 702 0 2146 2804 0 2215 2804 0 2805 2804 0 2803 2804 0 2807 436 0 2808 436 0;
	setAttr ".ed[4980:5145]" 2807 2806 0 2808 2806 0 1458 2806 0 2805 2806 0 2810 708 0
		 2811 709 0 2812 707 0 2810 2809 0 2811 2809 0 2812 2809 0 1462 2809 0 2814 440 0
		 2815 707 0 2814 2813 0 2230 2813 0 2815 2813 0 2812 2813 0 2817 439 0 2818 439 0
		 2817 2816 0 2818 2816 0 1463 2816 0 2815 2816 0 2820 713 0 2821 249 0 2822 712 0
		 2820 2819 0 2821 2819 0 2822 2819 0 1467 2819 0 2824 714 0 2825 712 0 2824 2823 0
		 2245 2823 0 2825 2823 0 2822 2823 0 2827 442 0 2828 442 0 2827 2826 0 2828 2826 0
		 1468 2826 0 2825 2826 0 2830 718 0 2831 250 0 2832 717 0 2830 2829 0 2831 2829 0
		 2832 2829 0 1472 2829 0 2834 719 0 2835 717 0 2834 2833 0 2260 2833 0 2835 2833 0
		 2832 2833 0 2837 446 0 2838 446 0 2837 2836 0 2838 2836 0 1473 2836 0 2835 2836 0
		 2840 723 0 2841 251 0 2842 722 0 2840 2839 0 2841 2839 0 2842 2839 0 1477 2839 0
		 2844 724 0 2845 722 0 2844 2843 0 2275 2843 0 2845 2843 0 2842 2843 0 2847 450 0
		 2848 450 0 2847 2846 0 2848 2846 0 1478 2846 0 2845 2846 0 2850 728 0 2851 252 0
		 2852 727 0 2850 2849 0 2851 2849 0 2852 2849 0 1482 2849 0 2854 729 0 2855 727 0
		 2854 2853 0 2290 2853 0 2855 2853 0 2852 2853 0 2857 454 0 2858 454 0 2857 2856 0
		 2858 2856 0 1483 2856 0 2855 2856 0 2860 733 0 2861 734 0 2862 732 0 2860 2859 0
		 2861 2859 0 2862 2859 0 1487 2859 0 2236 2863 0 2305 2863 0 2864 2863 0 2866 457 0
		 2867 457 0 2866 2865 0 2867 2865 0 2864 2865 0 2869 738 0 2870 739 0 2871 737 0 2869 2868 0
		 2870 2868 0 2871 2868 0 1492 2868 0 2873 461 0 2874 737 0 2873 2872 0 2320 2872 0
		 2874 2872 0 2871 2872 0 2876 460 0 2877 460 0 2876 2875 0 2877 2875 0 1493 2875 0
		 2874 2875 0 2879 743 0 2880 18 1 2881 742 0 2879 2878 0 2880 2878 0 2881 2878 0 1497 2878 0
		 2883 744 1 2884 742 0 2883 2882 0 1500 2882 0 2884 2882 0 2881 2882 0 2886 254 0
		 2886 2885 0 2341 2885 0 1498 2885 0 2884 2885 0 2888 747 1 2889 746 0 2888 2887 0
		 2407 2887 0 2889 2887 0 1502 2887 0 2891 746 0 1256 2890 0 1505 2890 0 2891 2890 0
		 2889 2890 0 2893 254 0 2893 2892 0 2886 2892 0 1503 2892 0;
	setAttr ".ed[5146:5311]" 2891 2892 0 2895 198 0 2896 198 0 2897 749 0 2895 2894 0
		 2896 2894 0 2897 2894 0 1507 2894 0 2899 749 0 1701 2898 0 1215 2898 0 2899 2898 0
		 2897 2898 0 2342 2900 0 2893 2900 0 1508 2900 0 2899 2900 0 2902 753 0 2903 19 1
		 2904 752 0 2902 2901 0 2903 2901 0 2904 2901 0 1512 2901 0 2906 754 1 2907 752 0
		 2906 2905 0 1515 2905 0 2907 2905 0 2904 2905 0 2909 259 0 2909 2908 0 2350 2908 0
		 1513 2908 0 2907 2908 0 2911 757 1 2912 756 0 2911 2910 0 2466 2910 0 2912 2910 0
		 1517 2910 0 2914 756 0 1286 2913 0 1520 2913 0 2914 2913 0 2912 2913 0 2916 259 0
		 2916 2915 0 2909 2915 0 1518 2915 0 2914 2915 0 2918 206 0 2919 206 0 2920 759 0
		 2918 2917 0 2919 2917 0 2920 2917 0 1522 2917 0 2922 759 0 1791 2921 0 1220 2921 0
		 2922 2921 0 2920 2921 0 2351 2923 0 2916 2923 0 1523 2923 0 2922 2923 0 2925 763 0
		 2926 20 0 2927 762 0 2925 2924 0 2926 2924 0 2927 2924 0 1527 2924 0 2929 764 0 2930 762 0
		 2929 2928 0 1530 2928 0 2930 2928 0 2927 2928 0 2932 264 0 2932 2931 0 2359 2931 0
		 1528 2931 0 2930 2931 0 2934 767 0 2935 766 0 2934 2933 0 2525 2933 0 2935 2933 0
		 1532 2933 0 2937 766 0 1316 2936 0 1535 2936 0 2937 2936 0 2935 2936 0 2939 264 0
		 2939 2938 0 2932 2938 0 1533 2938 0 2937 2938 0 2941 213 0 2942 213 0 2943 769 0
		 2941 2940 0 2942 2940 0 2943 2940 0 1537 2940 0 2945 769 0 1881 2944 0 1225 2944 0
		 2945 2944 0 2943 2944 0 2360 2946 0 2939 2946 0 1538 2946 0 2945 2946 0 2948 773 0
		 2949 21 0 2950 772 0 2948 2947 0 2949 2947 0 2950 2947 0 1542 2947 0 2952 774 0 2953 772 0
		 2952 2951 0 1545 2951 0 2953 2951 0 2950 2951 0 2955 269 0 2955 2954 0 2368 2954 0
		 1543 2954 0 2953 2954 0 2957 777 0 2958 776 0 2957 2956 0 2584 2956 0 2958 2956 0
		 1547 2956 0 2960 776 0 1346 2959 0 1550 2959 0 2960 2959 0 2958 2959 0 2962 269 0
		 2962 2961 0 2955 2961 0 1548 2961 0 2960 2961 0 2964 220 0 2965 220 0 2966 779 0
		 2964 2963 0 2965 2963 0 2966 2963 0 1552 2963 0 2968 779 0 1971 2967 0 1230 2967 0
		 2968 2967 0 2966 2967 0 2369 2969 0 2962 2969 0 1553 2969 0;
	setAttr ".ed[5312:5477]" 2968 2969 0 2971 783 0 2972 22 1 2973 782 0 2971 2970 0
		 2972 2970 0 2973 2970 0 1557 2970 0 2975 784 1 2976 782 0 2975 2974 0 1560 2974 0
		 2976 2974 0 2973 2974 0 2978 274 0 2978 2977 0 2377 2977 0 1558 2977 0 2976 2977 0
		 2980 787 1 2981 786 0 2980 2979 0 2643 2979 0 2981 2979 0 1562 2979 0 2983 786 0
		 1376 2982 0 1565 2982 0 2983 2982 0 2981 2982 0 2985 274 0 2985 2984 0 2978 2984 0
		 1563 2984 0 2983 2984 0 2987 227 0 2988 227 0 2989 789 0 2987 2986 0 2988 2986 0
		 2989 2986 0 1567 2986 0 2991 789 0 2061 2990 0 1235 2990 0 2991 2990 0 2989 2990 0
		 2378 2992 0 2985 2992 0 1568 2992 0 2991 2992 0 2994 793 0 2995 23 1 2996 792 0 2994 2993 0
		 2995 2993 0 2996 2993 0 1572 2993 0 2998 794 1 2999 792 0 2998 2997 0 1575 2997 0
		 2999 2997 0 2996 2997 0 3001 279 0 3001 3000 0 2386 3000 0 1573 3000 0 2999 3000 0
		 3003 797 1 3004 796 0 3003 3002 0 2702 3002 0 3004 3002 0 1577 3002 0 3006 796 0
		 1406 3005 0 1580 3005 0 3006 3005 0 3004 3005 0 3008 279 0 3008 3007 0 3001 3007 0
		 1578 3007 0 3006 3007 0 3010 234 0 3011 234 0 3012 799 0 3010 3009 0 3011 3009 0
		 3012 3009 0 1582 3009 0 3014 799 0 2151 3013 0 1240 3013 0 3014 3013 0 3012 3013 0
		 2387 3015 0 3008 3015 0 1583 3015 0 3014 3015 0 3017 803 0 3018 24 0 3019 802 0 3017 3016 0
		 3018 3016 0 3019 3016 0 1587 3016 0 3021 804 0 3022 802 0 3021 3020 0 1590 3020 0
		 3022 3020 0 3019 3020 0 3024 284 0 3024 3023 0 2395 3023 0 1588 3023 0 3022 3023 0
		 3026 807 0 3027 806 0 3026 3025 0 2761 3025 0 3027 3025 0 1592 3025 0 3029 806 0
		 1436 3028 0 1595 3028 0 3029 3028 0 3027 3028 0 3031 284 0 3031 3030 0 3024 3030 0
		 1593 3030 0 3029 3030 0 3033 241 0 3034 241 0 3035 809 0 3033 3032 0 3034 3032 0
		 3035 3032 0 1597 3032 0 3037 809 0 2241 3036 0 1245 3036 0 3037 3036 0 3035 3036 0
		 2396 3038 0 3031 3038 0 1598 3038 0 3037 3038 0 3040 813 0 3041 17 0 3042 812 0 3040 3039 0
		 3041 3039 0 3042 3039 0 1602 3039 0 3044 814 0 3045 812 0 3044 3043 0 1605 3043 0
		 3045 3043 0 3042 3043 0 3047 289 0 3047 3046 0;
	setAttr ".ed[5478:5643]" 2404 3046 0 1603 3046 0 3045 3046 0 3049 817 0 3050 816 0
		 3049 3048 0 2820 3048 0 3050 3048 0 1607 3048 0 3052 816 0 1466 3051 0 1610 3051 0
		 3052 3051 0 3050 3051 0 3054 289 0 3054 3053 0 3047 3053 0 1608 3053 0 3052 3053 0
		 3056 248 0 3057 248 0 3058 819 0 3056 3055 0 3057 3055 0 3058 3055 0 1612 3055 0
		 3060 819 0 2331 3059 0 1250 3059 0 3060 3059 0 3058 3059 0 2405 3061 0 3054 3061 0
		 1613 3061 0 3060 3061 0 3063 823 1 3064 114 1 3065 822 0 3063 3062 0 3064 3062 0
		 3065 3062 0 1617 3062 0 3067 824 1 3068 822 0 3067 3066 0 1620 3066 0 3068 3066 0
		 3065 3066 0 3070 294 0 3070 3069 0 2414 3069 0 1618 3069 0 3068 3069 0 3072 827 1
		 3073 826 0 3072 3071 0 2417 3071 0 3073 3071 0 1622 3071 0 3075 826 0 1261 3074 0
		 1625 3074 0 3075 3074 0 3073 3074 0 3077 294 0 3077 3076 0 3070 3076 0 1623 3076 0
		 3075 3076 0 3079 198 0 3080 829 0 3079 3078 0 2895 3078 0 3080 3078 0 1627 3078 0
		 3082 829 0 1506 3081 0 1255 3081 0 3082 3081 0 3080 3081 0 2415 3083 0 3077 3083 0
		 1628 3083 0 3082 3083 0 3085 832 1 3086 37 1 3087 831 0 3085 3084 0 3086 3084 0 3087 3084 0
		 1632 3084 0 3089 833 1 3090 831 0 3089 3088 0 1635 3088 0 3090 3088 0 3087 3088 0
		 3092 298 0 3092 3091 0 2424 3091 0 1633 3091 0 3090 3091 0 3094 836 1 3095 835 0
		 3094 3093 0 2427 3093 0 3095 3093 0 1637 3093 0 3097 835 0 1266 3096 0 1640 3096 0
		 3097 3096 0 3095 3096 0 3099 298 0 3099 3098 0 3092 3098 0 1638 3098 0 3097 3098 0
		 3101 198 0 3102 838 0 3101 3100 0 3079 3100 0 3102 3100 0 1642 3100 0 3104 838 0
		 1626 3103 0 1260 3103 0 3104 3103 0 3102 3103 0 2425 3105 0 3099 3105 0 1643 3105 0
		 3104 3105 0 3107 841 1 3108 111 1 3109 840 0 3107 3106 0 3108 3106 0 3109 3106 0
		 1647 3106 0 3111 842 1 3112 840 0 3111 3110 0 1650 3110 0 3112 3110 0 3109 3110 0
		 3114 302 0 3114 3113 0 2434 3113 0 1648 3113 0 3112 3113 0 3116 845 1 3117 844 0
		 3116 3115 0 2437 3115 0 3117 3115 0 1652 3115 0 3119 844 0 1271 3118 0 1655 3118 0
		 3119 3118 0 3117 3118 0 3121 302 0 3121 3120 0 3114 3120 0 1653 3120 0;
	setAttr ".ed[5644:5809]" 3119 3120 0 3123 198 0 3124 847 0 3123 3122 0 3101 3122 0
		 3124 3122 0 1657 3122 0 3126 847 0 1641 3125 0 1265 3125 0 3126 3125 0 3124 3125 0
		 2435 3127 0 3121 3127 0 1658 3127 0 3126 3127 0 3129 850 1 3130 849 0 3129 3128 0
		 3040 3128 0 3130 3128 0 1662 3128 0 3132 849 0 1601 3131 0 1665 3131 0 3132 3131 0
		 3130 3131 0 3134 306 0 3134 3133 0 2444 3133 0 1663 3133 0 3132 3133 0 3136 852 0
		 2399 3135 0 2447 3135 0 3136 3135 0 1667 3135 0 3138 852 0 1276 3137 0 1670 3137 0
		 3138 3137 0 3136 3137 0 3140 306 0 3140 3139 0 3134 3139 0 1668 3139 0 3138 3139 0
		 3142 198 0 3143 854 0 3142 3141 0 3123 3141 0 3143 3141 0 1672 3141 0 3145 854 0
		 1656 3144 0 1270 3144 0 3145 3144 0 3143 3144 0 2445 3146 0 3140 3146 0 1673 3146 0
		 3145 3146 0 3148 25 0 3149 858 0 3150 856 0 3148 3147 0 3149 3147 0 3150 3147 0 1677 3147 0
		 3152 311 0 3153 856 0 3152 3151 0 1680 3151 0 3153 3151 0 3150 3151 0 3155 309 0
		 3155 3154 0 2453 3154 0 1678 3154 0 3153 3154 0 3157 204 0 3158 860 0 3157 3156 0
		 2456 3156 0 3158 3156 0 1682 3156 0 3160 860 0 1281 3159 0 1685 3159 0 3160 3159 0
		 3158 3159 0 3162 309 0 3162 3161 0 3155 3161 0 1683 3161 0 3160 3161 0 3164 198 0
		 3165 863 0 3164 3163 0 3142 3163 0 3165 3163 0 1687 3163 0 3167 863 0 1671 3166 0
		 1275 3166 0 3167 3166 0 3165 3166 0 2454 3168 0 3162 3168 0 1688 3168 0 3167 3168 0
		 3170 26 0 3171 26 0 3172 865 0 3170 3169 0 3171 3169 0 3172 3169 0 1692 3169 0 3174 865 0
		 1766 3173 0 1695 3173 0 3174 3173 0 3172 3173 0 3176 313 0 3176 3175 0 2463 3175 0
		 1693 3175 0 3174 3175 0 3178 869 0 2507 3177 0 2335 3177 0 3178 3177 0 1697 3177 0
		 3180 869 0 1216 3179 0 1700 3179 0 3180 3179 0 3178 3179 0 3182 313 0 3182 3181 0
		 3176 3181 0 1698 3181 0 3180 3181 0 3184 871 0 2896 3183 0 3164 3183 0 3184 3183 0
		 1702 3183 0 3186 871 0 1686 3185 0 1280 3185 0 3186 3185 0 3184 3185 0 2464 3187 0
		 3182 3187 0 1703 3187 0 3186 3187 0 3189 873 1 3190 123 1 3191 872 0 3189 3188 0
		 3190 3188 0 3191 3188 0 1707 3188 0 3193 874 1 3194 872 0;
	setAttr ".ed[5810:5975]" 3193 3192 0 1710 3192 0 3194 3192 0 3191 3192 0 3196 316 0
		 3196 3195 0 2473 3195 0 1708 3195 0 3194 3195 0 3198 877 1 3199 876 0 3198 3197 0
		 2476 3197 0 3199 3197 0 1712 3197 0 3201 876 0 1291 3200 0 1715 3200 0 3201 3200 0
		 3199 3200 0 3203 316 0 3203 3202 0 3196 3202 0 1713 3202 0 3201 3202 0 3205 206 0
		 3206 879 0 3205 3204 0 2918 3204 0 3206 3204 0 1717 3204 0 3208 879 0 1521 3207 0
		 1285 3207 0 3208 3207 0 3206 3207 0 2474 3209 0 3203 3209 0 1718 3209 0 3208 3209 0
		 3211 882 1 3212 41 1 3213 881 0 3211 3210 0 3212 3210 0 3213 3210 0 1722 3210 0 3215 883 1
		 3216 881 0 3215 3214 0 1725 3214 0 3216 3214 0 3213 3214 0 3218 320 0 3218 3217 0
		 2483 3217 0 1723 3217 0 3216 3217 0 3220 886 1 3221 885 0 3220 3219 0 2486 3219 0
		 3221 3219 0 1727 3219 0 3223 885 0 1296 3222 0 1730 3222 0 3223 3222 0 3221 3222 0
		 3225 320 0 3225 3224 0 3218 3224 0 1728 3224 0 3223 3224 0 3227 206 0 3228 888 0
		 3227 3226 0 3205 3226 0 3228 3226 0 1732 3226 0 3230 888 0 1716 3229 0 1290 3229 0
		 3230 3229 0 3228 3229 0 2484 3231 0 3225 3231 0 1733 3231 0 3230 3231 0 3233 891 1
		 3234 120 1 3235 890 0 3233 3232 0 3234 3232 0 3235 3232 0 1737 3232 0 3237 892 1
		 3238 890 0 3237 3236 0 1740 3236 0 3238 3236 0 3235 3236 0 3240 324 0 3240 3239 0
		 2493 3239 0 1738 3239 0 3238 3239 0 3242 895 1 3243 894 0 3242 3241 0 2496 3241 0
		 3243 3241 0 1742 3241 0 3245 894 0 1301 3244 0 1745 3244 0 3245 3244 0 3243 3244 0
		 3247 324 0 3247 3246 0 3240 3246 0 1743 3246 0 3245 3246 0 3249 206 0 3250 897 0
		 3249 3248 0 3227 3248 0 3250 3248 0 1747 3248 0 3252 897 0 1731 3251 0 1295 3251 0
		 3252 3251 0 3250 3251 0 2494 3253 0 3247 3253 0 1748 3253 0 3252 3253 0 3255 900 1
		 3256 899 0 3255 3254 0 2879 3254 0 3256 3254 0 1752 3254 0 3258 899 0 1496 3257 0
		 1755 3257 0 3258 3257 0 3256 3257 0 3260 328 0 3260 3259 0 2503 3259 0 1753 3259 0
		 3258 3259 0 3262 902 0 2336 3261 0 2506 3261 0 3262 3261 0 1757 3261 0 3264 902 0
		 1306 3263 0 1760 3263 0 3264 3263 0 3262 3263 0 3266 328 0 3266 3265 0;
	setAttr ".ed[5976:6141]" 3260 3265 0 1758 3265 0 3264 3265 0 3268 206 0 3269 904 0
		 3268 3267 0 3249 3267 0 3269 3267 0 1762 3267 0 3271 904 0 1746 3270 0 1300 3270 0
		 3271 3270 0 3269 3270 0 2504 3272 0 3266 3272 0 1763 3272 0 3271 3272 0 3274 907 0
		 3275 906 0 3171 3273 0 3274 3273 0 3275 3273 0 1767 3273 0 3277 332 0 3278 906 0
		 3277 3276 0 1770 3276 0 3278 3276 0 3275 3276 0 3280 331 0 3280 3279 0 2512 3279 0
		 1768 3279 0 3278 3279 0 3282 211 0 3283 909 0 3282 3281 0 2515 3281 0 3283 3281 0
		 1772 3281 0 3285 909 0 1311 3284 0 1775 3284 0 3285 3284 0 3283 3284 0 3287 331 0
		 3287 3286 0 3280 3286 0 1773 3286 0 3285 3286 0 3289 206 0 3290 912 0 3289 3288 0
		 3268 3288 0 3290 3288 0 1777 3288 0 3292 912 0 1761 3291 0 1305 3291 0 3292 3291 0
		 3290 3291 0 2513 3293 0 3287 3293 0 1778 3293 0 3292 3293 0 3295 28 0 3296 28 0 3297 914 0
		 3295 3294 0 3296 3294 0 3297 3294 0 1782 3294 0 3299 914 0 1856 3298 0 1785 3298 0
		 3299 3298 0 3297 3298 0 3301 334 0 3301 3300 0 2522 3300 0 1783 3300 0 3299 3300 0
		 3303 918 0 2566 3302 0 2344 3302 0 3303 3302 0 1787 3302 0 3305 918 0 1221 3304 0
		 1790 3304 0 3305 3304 0 3303 3304 0 3307 334 0 3307 3306 0 3301 3306 0 1788 3306 0
		 3305 3306 0 3309 920 0 2919 3308 0 3289 3308 0 3309 3308 0 1792 3308 0 3311 920 0
		 1776 3310 0 1310 3310 0 3311 3310 0 3309 3310 0 2523 3312 0 3307 3312 0 1793 3312 0
		 3311 3312 0 3314 922 0 3315 132 0 3316 921 0 3314 3313 0 3315 3313 0 3316 3313 0
		 1797 3313 0 3318 923 0 3319 921 0 3318 3317 0 1800 3317 0 3319 3317 0 3316 3317 0
		 3321 337 0 3321 3320 0 2532 3320 0 1798 3320 0 3319 3320 0 3323 926 0 3324 925 0
		 3323 3322 0 2535 3322 0 3324 3322 0 1802 3322 0 3326 925 0 1321 3325 0 1805 3325 0
		 3326 3325 0 3324 3325 0 3328 337 0 3328 3327 0 3321 3327 0 1803 3327 0 3326 3327 0
		 3330 213 0 3331 928 0 3330 3329 0 2941 3329 0 3331 3329 0 1807 3329 0 3333 928 0
		 1536 3332 0 1315 3332 0 3333 3332 0 3331 3332 0 2533 3334 0 3328 3334 0 1808 3334 0
		 3333 3334 0 3336 931 0 3337 45 0 3338 930 0 3336 3335 0 3337 3335 0;
	setAttr ".ed[6142:6307]" 3338 3335 0 1812 3335 0 3340 932 0 3341 930 0 3340 3339 0
		 1815 3339 0 3341 3339 0 3338 3339 0 3343 341 0 3343 3342 0 2542 3342 0 1813 3342 0
		 3341 3342 0 3345 935 0 3346 934 0 3345 3344 0 2545 3344 0 3346 3344 0 1817 3344 0
		 3348 934 0 1326 3347 0 1820 3347 0 3348 3347 0 3346 3347 0 3350 341 0 3350 3349 0
		 3343 3349 0 1818 3349 0 3348 3349 0 3352 213 0 3353 937 0 3352 3351 0 3330 3351 0
		 3353 3351 0 1822 3351 0 3355 937 0 1806 3354 0 1320 3354 0 3355 3354 0 3353 3354 0
		 2543 3356 0 3350 3356 0 1823 3356 0 3355 3356 0 3358 940 0 3359 129 0 3360 939 0
		 3358 3357 0 3359 3357 0 3360 3357 0 1827 3357 0 3362 941 0 3363 939 0 3362 3361 0
		 1830 3361 0 3363 3361 0 3360 3361 0 3365 345 0 3365 3364 0 2552 3364 0 1828 3364 0
		 3363 3364 0 3367 944 0 3368 943 0 3367 3366 0 2555 3366 0 3368 3366 0 1832 3366 0
		 3370 943 0 1331 3369 0 1835 3369 0 3370 3369 0 3368 3369 0 3372 345 0 3372 3371 0
		 3365 3371 0 1833 3371 0 3370 3371 0 3374 213 0 3375 946 0 3374 3373 0 3352 3373 0
		 3375 3373 0 1837 3373 0 3377 946 0 1821 3376 0 1325 3376 0 3377 3376 0 3375 3376 0
		 2553 3378 0 3372 3378 0 1838 3378 0 3377 3378 0 3380 949 0 3381 948 0 3380 3379 0
		 2902 3379 0 3381 3379 0 1842 3379 0 3383 948 0 1511 3382 0 1845 3382 0 3383 3382 0
		 3381 3382 0 3385 349 0 3385 3384 0 2562 3384 0 1843 3384 0 3383 3384 0 3387 951 0
		 2345 3386 0 2565 3386 0 3387 3386 0 1847 3386 0 3389 951 0 1336 3388 0 1850 3388 0
		 3389 3388 0 3387 3388 0 3391 349 0 3391 3390 0 3385 3390 0 1848 3390 0 3389 3390 0
		 3393 213 0 3394 953 0 3393 3392 0 3374 3392 0 3394 3392 0 1852 3392 0 3396 953 0
		 1836 3395 0 1330 3395 0 3396 3395 0 3394 3395 0 2563 3397 0 3391 3397 0 1853 3397 0
		 3396 3397 0 3399 956 0 3400 955 0 3296 3398 0 3399 3398 0 3400 3398 0 1857 3398 0
		 3402 353 0 3403 955 0 3402 3401 0 1860 3401 0 3403 3401 0 3400 3401 0 3405 352 0
		 3405 3404 0 2571 3404 0 1858 3404 0 3403 3404 0 3407 218 0 3408 958 0 3407 3406 0
		 2574 3406 0 3408 3406 0 1862 3406 0 3410 958 0 1341 3409 0 1865 3409 0 3410 3409 0;
	setAttr ".ed[6308:6473]" 3408 3409 0 3412 352 0 3412 3411 0 3405 3411 0 1863 3411 0
		 3410 3411 0 3414 213 0 3415 961 0 3414 3413 0 3393 3413 0 3415 3413 0 1867 3413 0
		 3417 961 0 1851 3416 0 1335 3416 0 3417 3416 0 3415 3416 0 2572 3418 0 3412 3418 0
		 1868 3418 0 3417 3418 0 3420 29 0 3421 29 0 3422 963 0 3420 3419 0 3421 3419 0 3422 3419 0
		 1872 3419 0 3424 963 0 1946 3423 0 1875 3423 0 3424 3423 0 3422 3423 0 3426 355 0
		 3426 3425 0 2581 3425 0 1873 3425 0 3424 3425 0 3428 967 0 2625 3427 0 2353 3427 0
		 3428 3427 0 1877 3427 0 3430 967 0 1226 3429 0 1880 3429 0 3430 3429 0 3428 3429 0
		 3432 355 0 3432 3431 0 3426 3431 0 1878 3431 0 3430 3431 0 3434 969 0 2942 3433 0
		 3414 3433 0 3434 3433 0 1882 3433 0 3436 969 0 1866 3435 0 1340 3435 0 3436 3435 0
		 3434 3435 0 2582 3437 0 3432 3437 0 1883 3437 0 3436 3437 0 3439 971 0 3440 141 0
		 3441 970 0 3439 3438 0 3440 3438 0 3441 3438 0 1887 3438 0 3443 972 0 3444 970 0
		 3443 3442 0 1890 3442 0 3444 3442 0 3441 3442 0 3446 358 0 3446 3445 0 2591 3445 0
		 1888 3445 0 3444 3445 0 3448 975 0 3449 974 0 3448 3447 0 2594 3447 0 3449 3447 0
		 1892 3447 0 3451 974 0 1351 3450 0 1895 3450 0 3451 3450 0 3449 3450 0 3453 358 0
		 3453 3452 0 3446 3452 0 1893 3452 0 3451 3452 0 3455 220 0 3456 977 0 3455 3454 0
		 2964 3454 0 3456 3454 0 1897 3454 0 3458 977 0 1551 3457 0 1345 3457 0 3458 3457 0
		 3456 3457 0 2592 3459 0 3453 3459 0 1898 3459 0 3458 3459 0 3461 980 0 3462 49 0
		 3463 979 0 3461 3460 0 3462 3460 0 3463 3460 0 1902 3460 0 3465 981 0 3466 979 0
		 3465 3464 0 1905 3464 0 3466 3464 0 3463 3464 0 3468 362 0 3468 3467 0 2601 3467 0
		 1903 3467 0 3466 3467 0 3470 984 0 3471 983 0 3470 3469 0 2604 3469 0 3471 3469 0
		 1907 3469 0 3473 983 0 1356 3472 0 1910 3472 0 3473 3472 0 3471 3472 0 3475 362 0
		 3475 3474 0 3468 3474 0 1908 3474 0 3473 3474 0 3477 220 0 3478 986 0 3477 3476 0
		 3455 3476 0 3478 3476 0 1912 3476 0 3480 986 0 1896 3479 0 1350 3479 0 3480 3479 0
		 3478 3479 0 2602 3481 0 3475 3481 0 1913 3481 0 3480 3481 0 3483 989 0;
	setAttr ".ed[6474:6639]" 3484 138 0 3485 988 0 3483 3482 0 3484 3482 0 3485 3482 0
		 1917 3482 0 3487 990 0 3488 988 0 3487 3486 0 1920 3486 0 3488 3486 0 3485 3486 0
		 3490 366 0 3490 3489 0 2611 3489 0 1918 3489 0 3488 3489 0 3492 993 0 3493 992 0
		 3492 3491 0 2614 3491 0 3493 3491 0 1922 3491 0 3495 992 0 1361 3494 0 1925 3494 0
		 3495 3494 0 3493 3494 0 3497 366 0 3497 3496 0 3490 3496 0 1923 3496 0 3495 3496 0
		 3499 220 0 3500 995 0 3499 3498 0 3477 3498 0 3500 3498 0 1927 3498 0 3502 995 0
		 1911 3501 0 1355 3501 0 3502 3501 0 3500 3501 0 2612 3503 0 3497 3503 0 1928 3503 0
		 3502 3503 0 3505 998 0 3506 997 0 3505 3504 0 2925 3504 0 3506 3504 0 1932 3504 0
		 3508 997 0 1526 3507 0 1935 3507 0 3508 3507 0 3506 3507 0 3510 370 0 3510 3509 0
		 2621 3509 0 1933 3509 0 3508 3509 0 3512 1000 0 2354 3511 0 2624 3511 0 3512 3511 0
		 1937 3511 0 3514 1000 0 1366 3513 0 1940 3513 0 3514 3513 0 3512 3513 0 3516 370 0
		 3516 3515 0 3510 3515 0 1938 3515 0 3514 3515 0 3518 220 0 3519 1002 0 3518 3517 0
		 3499 3517 0 3519 3517 0 1942 3517 0 3521 1002 0 1926 3520 0 1360 3520 0 3521 3520 0
		 3519 3520 0 2622 3522 0 3516 3522 0 1943 3522 0 3521 3522 0 3524 1005 0 3525 1004 0
		 3421 3523 0 3524 3523 0 3525 3523 0 1947 3523 0 3527 374 0 3528 1004 0 3527 3526 0
		 1950 3526 0 3528 3526 0 3525 3526 0 3530 373 0 3530 3529 0 2630 3529 0 1948 3529 0
		 3528 3529 0 3532 225 0 3533 1007 0 3532 3531 0 2633 3531 0 3533 3531 0 1952 3531 0
		 3535 1007 0 1371 3534 0 1955 3534 0 3535 3534 0 3533 3534 0 3537 373 0 3537 3536 0
		 3530 3536 0 1953 3536 0 3535 3536 0 3539 220 0 3540 1010 0 3539 3538 0 3518 3538 0
		 3540 3538 0 1957 3538 0 3542 1010 0 1941 3541 0 1365 3541 0 3542 3541 0 3540 3541 0
		 2631 3543 0 3537 3543 0 1958 3543 0 3542 3543 0 3545 30 0 3546 30 0 3547 1012 0 3545 3544 0
		 3546 3544 0 3547 3544 0 1962 3544 0 3549 1012 0 2036 3548 0 1965 3548 0 3549 3548 0
		 3547 3548 0 3551 376 0 3551 3550 0 2640 3550 0 1963 3550 0 3549 3550 0 3553 1016 0
		 2684 3552 0 2362 3552 0 3553 3552 0 1967 3552 0 3555 1016 0 1231 3554 0;
	setAttr ".ed[6640:6805]" 1970 3554 0 3555 3554 0 3553 3554 0 3557 376 0 3557 3556 0
		 3551 3556 0 1968 3556 0 3555 3556 0 3559 1018 0 2965 3558 0 3539 3558 0 3559 3558 0
		 1972 3558 0 3561 1018 0 1956 3560 0 1370 3560 0 3561 3560 0 3559 3560 0 2641 3562 0
		 3557 3562 0 1973 3562 0 3561 3562 0 3564 1020 1 3565 150 1 3566 1019 0 3564 3563 0
		 3565 3563 0 3566 3563 0 1977 3563 0 3568 1021 1 3569 1019 0 3568 3567 0 1980 3567 0
		 3569 3567 0 3566 3567 0 3571 379 0 3571 3570 0 2650 3570 0 1978 3570 0 3569 3570 0
		 3573 1024 1 3574 1023 0 3573 3572 0 2653 3572 0 3574 3572 0 1982 3572 0 3576 1023 0
		 1381 3575 0 1985 3575 0 3576 3575 0 3574 3575 0 3578 379 0 3578 3577 0 3571 3577 0
		 1983 3577 0 3576 3577 0 3580 227 0 3581 1026 0 3580 3579 0 2987 3579 0 3581 3579 0
		 1987 3579 0 3583 1026 0 1566 3582 0 1375 3582 0 3583 3582 0 3581 3582 0 2651 3584 0
		 3578 3584 0 1988 3584 0 3583 3584 0 3586 1029 1 3587 53 1 3588 1028 0 3586 3585 0
		 3587 3585 0 3588 3585 0 1992 3585 0 3590 1030 1 3591 1028 0 3590 3589 0 1995 3589 0
		 3591 3589 0 3588 3589 0 3593 383 0 3593 3592 0 2660 3592 0 1993 3592 0 3591 3592 0
		 3595 1033 1 3596 1032 0 3595 3594 0 2663 3594 0 3596 3594 0 1997 3594 0 3598 1032 0
		 1386 3597 0 2000 3597 0 3598 3597 0 3596 3597 0 3600 383 0 3600 3599 0 3593 3599 0
		 1998 3599 0 3598 3599 0 3602 227 0 3603 1035 0 3602 3601 0 3580 3601 0 3603 3601 0
		 2002 3601 0 3605 1035 0 1986 3604 0 1380 3604 0 3605 3604 0 3603 3604 0 2661 3606 0
		 3600 3606 0 2003 3606 0 3605 3606 0 3608 1038 1 3609 147 1 3610 1037 0 3608 3607 0
		 3609 3607 0 3610 3607 0 2007 3607 0 3612 1039 1 3613 1037 0 3612 3611 0 2010 3611 0
		 3613 3611 0 3610 3611 0 3615 387 0 3615 3614 0 2670 3614 0 2008 3614 0 3613 3614 0
		 3617 1042 1 3618 1041 0 3617 3616 0 2673 3616 0 3618 3616 0 2012 3616 0 3620 1041 0
		 1391 3619 0 2015 3619 0 3620 3619 0 3618 3619 0 3622 387 0 3622 3621 0 3615 3621 0
		 2013 3621 0 3620 3621 0 3624 227 0 3625 1044 0 3624 3623 0 3602 3623 0 3625 3623 0
		 2017 3623 0 3627 1044 0 2001 3626 0 1385 3626 0 3627 3626 0 3625 3626 0 2671 3628 0;
	setAttr ".ed[6806:6971]" 3622 3628 0 2018 3628 0 3627 3628 0 3630 1047 1 3631 1046 0
		 3630 3629 0 2948 3629 0 3631 3629 0 2022 3629 0 3633 1046 0 1541 3632 0 2025 3632 0
		 3633 3632 0 3631 3632 0 3635 391 0 3635 3634 0 2680 3634 0 2023 3634 0 3633 3634 0
		 3637 1049 0 2363 3636 0 2683 3636 0 3637 3636 0 2027 3636 0 3639 1049 0 1396 3638 0
		 2030 3638 0 3639 3638 0 3637 3638 0 3641 391 0 3641 3640 0 3635 3640 0 2028 3640 0
		 3639 3640 0 3643 227 0 3644 1051 0 3643 3642 0 3624 3642 0 3644 3642 0 2032 3642 0
		 3646 1051 0 2016 3645 0 1390 3645 0 3646 3645 0 3644 3645 0 2681 3647 0 3641 3647 0
		 2033 3647 0 3646 3647 0 3649 1054 0 3650 1053 0 3546 3648 0 3649 3648 0 3650 3648 0
		 2037 3648 0 3652 395 0 3653 1053 0 3652 3651 0 2040 3651 0 3653 3651 0 3650 3651 0
		 3655 394 0 3655 3654 0 2689 3654 0 2038 3654 0 3653 3654 0 3657 232 0 3658 1056 0
		 3657 3656 0 2692 3656 0 3658 3656 0 2042 3656 0 3660 1056 0 1401 3659 0 2045 3659 0
		 3660 3659 0 3658 3659 0 3662 394 0 3662 3661 0 3655 3661 0 2043 3661 0 3660 3661 0
		 3664 227 0 3665 1059 0 3664 3663 0 3643 3663 0 3665 3663 0 2047 3663 0 3667 1059 0
		 2031 3666 0 1395 3666 0 3667 3666 0 3665 3666 0 2690 3668 0 3662 3668 0 2048 3668 0
		 3667 3668 0 3670 31 0 3671 31 0 3672 1061 0 3670 3669 0 3671 3669 0 3672 3669 0 2052 3669 0
		 3674 1061 0 2126 3673 0 2055 3673 0 3674 3673 0 3672 3673 0 3676 397 0 3676 3675 0
		 2699 3675 0 2053 3675 0 3674 3675 0 3678 1065 0 2743 3677 0 2371 3677 0 3678 3677 0
		 2057 3677 0 3680 1065 0 1236 3679 0 2060 3679 0 3680 3679 0 3678 3679 0 3682 397 0
		 3682 3681 0 3676 3681 0 2058 3681 0 3680 3681 0 3684 1067 0 2988 3683 0 3664 3683 0
		 3684 3683 0 2062 3683 0 3686 1067 0 2046 3685 0 1400 3685 0 3686 3685 0 3684 3685 0
		 2700 3687 0 3682 3687 0 2063 3687 0 3686 3687 0 3689 1069 1 3690 159 1 3691 1068 0
		 3689 3688 0 3690 3688 0 3691 3688 0 2067 3688 0 3693 1070 1 3694 1068 0 3693 3692 0
		 2070 3692 0 3694 3692 0 3691 3692 0 3696 400 0 3696 3695 0 2709 3695 0 2068 3695 0
		 3694 3695 0 3698 1073 1 3699 1072 0 3698 3697 0 2712 3697 0 3699 3697 0;
	setAttr ".ed[6972:7137]" 2072 3697 0 3701 1072 0 1411 3700 0 2075 3700 0 3701 3700 0
		 3699 3700 0 3703 400 0 3703 3702 0 3696 3702 0 2073 3702 0 3701 3702 0 3705 234 0
		 3706 1075 0 3705 3704 0 3010 3704 0 3706 3704 0 2077 3704 0 3708 1075 0 1581 3707 0
		 1405 3707 0 3708 3707 0 3706 3707 0 2710 3709 0 3703 3709 0 2078 3709 0 3708 3709 0
		 3711 1078 1 3712 57 1 3713 1077 0 3711 3710 0 3712 3710 0 3713 3710 0 2082 3710 0
		 3715 1079 1 3716 1077 0 3715 3714 0 2085 3714 0 3716 3714 0 3713 3714 0 3718 404 0
		 3718 3717 0 2719 3717 0 2083 3717 0 3716 3717 0 3720 1082 1 3721 1081 0 3720 3719 0
		 2722 3719 0 3721 3719 0 2087 3719 0 3723 1081 0 1416 3722 0 2090 3722 0 3723 3722 0
		 3721 3722 0 3725 404 0 3725 3724 0 3718 3724 0 2088 3724 0 3723 3724 0 3727 234 0
		 3728 1084 0 3727 3726 0 3705 3726 0 3728 3726 0 2092 3726 0 3730 1084 0 2076 3729 0
		 1410 3729 0 3730 3729 0 3728 3729 0 2720 3731 0 3725 3731 0 2093 3731 0 3730 3731 0
		 3733 1087 1 3734 156 1 3735 1086 0 3733 3732 0 3734 3732 0 3735 3732 0 2097 3732 0
		 3737 1088 1 3738 1086 0 3737 3736 0 2100 3736 0 3738 3736 0 3735 3736 0 3740 408 0
		 3740 3739 0 2729 3739 0 2098 3739 0 3738 3739 0 3742 1091 1 3743 1090 0 3742 3741 0
		 2732 3741 0 3743 3741 0 2102 3741 0 3745 1090 0 1421 3744 0 2105 3744 0 3745 3744 0
		 3743 3744 0 3747 408 0 3747 3746 0 3740 3746 0 2103 3746 0 3745 3746 0 3749 234 0
		 3750 1093 0 3749 3748 0 3727 3748 0 3750 3748 0 2107 3748 0 3752 1093 0 2091 3751 0
		 1415 3751 0 3752 3751 0 3750 3751 0 2730 3753 0 3747 3753 0 2108 3753 0 3752 3753 0
		 3755 1096 1 3756 1095 0 3755 3754 0 2971 3754 0 3756 3754 0 2112 3754 0 3758 1095 0
		 1556 3757 0 2115 3757 0 3758 3757 0 3756 3757 0 3760 412 0 3760 3759 0 2739 3759 0
		 2113 3759 0 3758 3759 0 3762 1098 0 2372 3761 0 2742 3761 0 3762 3761 0 2117 3761 0
		 3764 1098 0 1426 3763 0 2120 3763 0 3764 3763 0 3762 3763 0 3766 412 0 3766 3765 0
		 3760 3765 0 2118 3765 0 3764 3765 0 3768 234 0 3769 1100 0 3768 3767 0 3749 3767 0
		 3769 3767 0 2122 3767 0 3771 1100 0 2106 3770 0 1420 3770 0 3771 3770 0 3769 3770 0;
	setAttr ".ed[7138:7303]" 2740 3772 0 3766 3772 0 2123 3772 0 3771 3772 0 3774 1103 0
		 3775 1102 0 3671 3773 0 3774 3773 0 3775 3773 0 2127 3773 0 3777 416 0 3778 1102 0
		 3777 3776 0 2130 3776 0 3778 3776 0 3775 3776 0 3780 415 0 3780 3779 0 2748 3779 0
		 2128 3779 0 3778 3779 0 3782 239 0 3783 1105 0 3782 3781 0 2751 3781 0 3783 3781 0
		 2132 3781 0 3785 1105 0 1431 3784 0 2135 3784 0 3785 3784 0 3783 3784 0 3787 415 0
		 3787 3786 0 3780 3786 0 2133 3786 0 3785 3786 0 3789 234 0 3790 1108 0 3789 3788 0
		 3768 3788 0 3790 3788 0 2137 3788 0 3792 1108 0 2121 3791 0 1425 3791 0 3792 3791 0
		 3790 3791 0 2749 3793 0 3787 3793 0 2138 3793 0 3792 3793 0 3795 32 0 3796 32 0 3797 1110 0
		 3795 3794 0 3796 3794 0 3797 3794 0 2142 3794 0 3799 1110 0 2216 3798 0 2145 3798 0
		 3799 3798 0 3797 3798 0 3801 418 0 3801 3800 0 2758 3800 0 2143 3800 0 3799 3800 0
		 3803 1114 0 2802 3802 0 2380 3802 0 3803 3802 0 2147 3802 0 3805 1114 0 1241 3804 0
		 2150 3804 0 3805 3804 0 3803 3804 0 3807 418 0 3807 3806 0 3801 3806 0 2148 3806 0
		 3805 3806 0 3809 1116 0 3011 3808 0 3789 3808 0 3809 3808 0 2152 3808 0 3811 1116 0
		 2136 3810 0 1430 3810 0 3811 3810 0 3809 3810 0 2759 3812 0 3807 3812 0 2153 3812 0
		 3811 3812 0 3814 1118 0 3815 168 0 3816 1117 0 3814 3813 0 3815 3813 0 3816 3813 0
		 2157 3813 0 3818 1119 0 3819 1117 0 3818 3817 0 2160 3817 0 3819 3817 0 3816 3817 0
		 3821 421 0 3821 3820 0 2768 3820 0 2158 3820 0 3819 3820 0 3823 1122 0 3824 1121 0
		 3823 3822 0 2771 3822 0 3824 3822 0 2162 3822 0 3826 1121 0 1441 3825 0 2165 3825 0
		 3826 3825 0 3824 3825 0 3828 421 0 3828 3827 0 3821 3827 0 2163 3827 0 3826 3827 0
		 3830 241 0 3831 1124 0 3830 3829 0 3033 3829 0 3831 3829 0 2167 3829 0 3833 1124 0
		 1596 3832 0 1435 3832 0 3833 3832 0 3831 3832 0 2769 3834 0 3828 3834 0 2168 3834 0
		 3833 3834 0 3836 1127 0 3837 61 0 3838 1126 0 3836 3835 0 3837 3835 0 3838 3835 0
		 2172 3835 0 3840 1128 0 3841 1126 0 3840 3839 0 2175 3839 0 3841 3839 0 3838 3839 0
		 3843 425 0 3843 3842 0 2778 3842 0 2173 3842 0 3841 3842 0 3845 1131 0;
	setAttr ".ed[7304:7469]" 3846 1130 0 3845 3844 0 2781 3844 0 3846 3844 0 2177 3844 0
		 3848 1130 0 1446 3847 0 2180 3847 0 3848 3847 0 3846 3847 0 3850 425 0 3850 3849 0
		 3843 3849 0 2178 3849 0 3848 3849 0 3852 241 0 3853 1133 0 3852 3851 0 3830 3851 0
		 3853 3851 0 2182 3851 0 3855 1133 0 2166 3854 0 1440 3854 0 3855 3854 0 3853 3854 0
		 2779 3856 0 3850 3856 0 2183 3856 0 3855 3856 0 3858 1136 0 3859 165 0 3860 1135 0
		 3858 3857 0 3859 3857 0 3860 3857 0 2187 3857 0 3862 1137 0 3863 1135 0 3862 3861 0
		 2190 3861 0 3863 3861 0 3860 3861 0 3865 429 0 3865 3864 0 2788 3864 0 2188 3864 0
		 3863 3864 0 3867 1140 0 3868 1139 0 3867 3866 0 2791 3866 0 3868 3866 0 2192 3866 0
		 3870 1139 0 1451 3869 0 2195 3869 0 3870 3869 0 3868 3869 0 3872 429 0 3872 3871 0
		 3865 3871 0 2193 3871 0 3870 3871 0 3874 241 0 3875 1142 0 3874 3873 0 3852 3873 0
		 3875 3873 0 2197 3873 0 3877 1142 0 2181 3876 0 1445 3876 0 3877 3876 0 3875 3876 0
		 2789 3878 0 3872 3878 0 2198 3878 0 3877 3878 0 3880 1145 0 3881 1144 0 3880 3879 0
		 2994 3879 0 3881 3879 0 2202 3879 0 3883 1144 0 1571 3882 0 2205 3882 0 3883 3882 0
		 3881 3882 0 3885 433 0 3885 3884 0 2798 3884 0 2203 3884 0 3883 3884 0 3887 1147 0
		 2381 3886 0 2801 3886 0 3887 3886 0 2207 3886 0 3889 1147 0 1456 3888 0 2210 3888 0
		 3889 3888 0 3887 3888 0 3891 433 0 3891 3890 0 3885 3890 0 2208 3890 0 3889 3890 0
		 3893 241 0 3894 1149 0 3893 3892 0 3874 3892 0 3894 3892 0 2212 3892 0 3896 1149 0
		 2196 3895 0 1450 3895 0 3896 3895 0 3894 3895 0 2799 3897 0 3891 3897 0 2213 3897 0
		 3896 3897 0 3899 1152 0 3900 1151 0 3796 3898 0 3899 3898 0 3900 3898 0 2217 3898 0
		 3902 437 0 3903 1151 0 3902 3901 0 2220 3901 0 3903 3901 0 3900 3901 0 3905 436 0
		 3905 3904 0 2807 3904 0 2218 3904 0 3903 3904 0 3907 246 0 3908 1154 0 3907 3906 0
		 2810 3906 0 3908 3906 0 2222 3906 0 3910 1154 0 1461 3909 0 2225 3909 0 3910 3909 0
		 3908 3909 0 3912 436 0 3912 3911 0 3905 3911 0 2223 3911 0 3910 3911 0 3914 241 0
		 3915 1157 0 3914 3913 0 3893 3913 0 3915 3913 0 2227 3913 0 3917 1157 0 2211 3916 0;
	setAttr ".ed[7470:7635]" 1455 3916 0 3917 3916 0 3915 3916 0 2808 3918 0 3912 3918 0
		 2228 3918 0 3917 3918 0 3920 33 0 3921 33 0 3922 1159 0 3920 3919 0 3921 3919 0 3922 3919 0
		 2232 3919 0 3924 1159 0 2306 3923 0 2235 3923 0 3924 3923 0 3922 3923 0 3926 439 0
		 3926 3925 0 2817 3925 0 2233 3925 0 3924 3925 0 3928 1163 0 2861 3927 0 2389 3927 0
		 3928 3927 0 2237 3927 0 3930 1163 0 1246 3929 0 2240 3929 0 3930 3929 0 3928 3929 0
		 3932 439 0 3932 3931 0 3926 3931 0 2238 3931 0 3934 1165 0 3034 3933 0 3914 3933 0
		 3934 3933 0 2242 3933 0 2226 3935 0 1460 3935 0 3936 3935 0 3934 3935 0 2818 3937 0
		 3932 3937 0 3936 3937 0 3939 1167 0 3940 177 0 3941 1166 0 3939 3938 0 3940 3938 0
		 3941 3938 0 2247 3938 0 3943 1168 0 3944 1166 0 3943 3942 0 2250 3942 0 3944 3942 0
		 3941 3942 0 3946 442 0 3946 3945 0 2827 3945 0 2248 3945 0 3944 3945 0 3948 1171 0
		 3949 1170 0 3948 3947 0 2830 3947 0 3949 3947 0 2252 3947 0 3951 1170 0 1471 3950 0
		 2255 3950 0 3951 3950 0 3949 3950 0 3953 442 0 3953 3952 0 3946 3952 0 2253 3952 0
		 3951 3952 0 3955 248 0 3956 1173 0 3955 3954 0 3056 3954 0 3956 3954 0 2257 3954 0
		 3958 1173 0 1611 3957 0 1465 3957 0 3958 3957 0 3956 3957 0 2828 3959 0 3953 3959 0
		 2258 3959 0 3958 3959 0 3961 1176 0 3962 65 0 3963 1175 0 3961 3960 0 3962 3960 0
		 3963 3960 0 2262 3960 0 3965 1177 0 3966 1175 0 3965 3964 0 2265 3964 0 3966 3964 0
		 3963 3964 0 3968 446 0 3968 3967 0 2837 3967 0 2263 3967 0 3966 3967 0 3970 1180 0
		 3971 1179 0 3970 3969 0 2840 3969 0 3971 3969 0 2267 3969 0 3973 1179 0 1476 3972 0
		 2270 3972 0 3973 3972 0 3971 3972 0 3975 446 0 3975 3974 0 3968 3974 0 2268 3974 0
		 3973 3974 0 3977 248 0 3978 1182 0 3977 3976 0 3955 3976 0 3978 3976 0 2272 3976 0
		 3980 1182 0 2256 3979 0 1470 3979 0 3980 3979 0 3978 3979 0 2838 3981 0 3975 3981 0
		 2273 3981 0 3980 3981 0 3983 1185 0 3984 174 0 3985 1184 0 3983 3982 0 3984 3982 0
		 3985 3982 0 2277 3982 0 3987 1186 0 3988 1184 0 3987 3986 0 2280 3986 0 3988 3986 0
		 3985 3986 0 3990 450 0 3990 3989 0 2847 3989 0 2278 3989 0 3988 3989 0;
	setAttr ".ed[7636:7801]" 3992 1189 0 3993 1188 0 3992 3991 0 2850 3991 0 3993 3991 0
		 2282 3991 0 3995 1188 0 1481 3994 0 2285 3994 0 3995 3994 0 3993 3994 0 3997 450 0
		 3997 3996 0 3990 3996 0 2283 3996 0 3995 3996 0 3999 248 0 4000 1191 0 3999 3998 0
		 3977 3998 0 4000 3998 0 2287 3998 0 4002 1191 0 2271 4001 0 1475 4001 0 4002 4001 0
		 4000 4001 0 2848 4003 0 3997 4003 0 2288 4003 0 4002 4003 0 4005 1194 0 4006 1193 0
		 4005 4004 0 3017 4004 0 4006 4004 0 2292 4004 0 4008 1193 0 1586 4007 0 2295 4007 0
		 4008 4007 0 4006 4007 0 4010 454 0 4010 4009 0 2857 4009 0 2293 4009 0 4008 4009 0
		 4012 1196 0 2390 4011 0 2860 4011 0 4012 4011 0 2297 4011 0 4014 1196 0 1486 4013 0
		 2300 4013 0 4014 4013 0 4012 4013 0 4016 454 0 4016 4015 0 4010 4015 0 2298 4015 0
		 4014 4015 0 4018 248 0 4019 1198 0 4018 4017 0 3999 4017 0 4019 4017 0 2302 4017 0
		 4021 1198 0 2286 4020 0 1480 4020 0 4021 4020 0 4019 4020 0 2858 4022 0 4016 4022 0
		 2303 4022 0 4021 4022 0 4024 1201 0 4025 1200 0 3921 4023 0 4024 4023 0 4025 4023 0
		 2307 4023 0 4027 458 0 4028 1200 0 4027 4026 0 2310 4026 0 4028 4026 0 4025 4026 0
		 4030 457 0 4030 4029 0 2866 4029 0 2308 4029 0 4028 4029 0 4032 253 0 4033 1203 0
		 4032 4031 0 2869 4031 0 4033 4031 0 2312 4031 0 4035 1203 0 1491 4034 0 2315 4034 0
		 4035 4034 0 4033 4034 0 4037 457 0 4037 4036 0 4030 4036 0 2313 4036 0 4035 4036 0
		 4039 248 0 4040 1206 0 4039 4038 0 4018 4038 0 4040 4038 0 2317 4038 0 4042 1206 0
		 2301 4041 0 1485 4041 0 4042 4041 0 4040 4041 0 2867 4043 0 4037 4043 0 2318 4043 0
		 4042 4043 0 4045 25 0 4046 1208 0 4045 4044 0 3148 4044 0 4046 4044 0 2322 4044 0
		 4048 1208 0 1676 4047 0 2325 4047 0 4048 4047 0 4046 4047 0 4050 460 0 4050 4049 0
		 2876 4049 0 2323 4049 0 4048 4049 0 4052 1211 0 2448 4051 0 2398 4051 0 4052 4051 0
		 2327 4051 0 4054 1211 0 1251 4053 0 2330 4053 0 4054 4053 0 4052 4053 0 4056 460 0
		 4056 4055 0 4050 4055 0 2328 4055 0 4054 4055 0 4058 1213 0 3057 4057 0 4039 4057 0
		 4058 4057 0 2332 4057 0 4060 1213 0 2316 4059 0 1490 4059 0 4060 4059 0 4058 4059 0;
	setAttr ".ed[7802:7863]" 2877 4061 0 4056 4061 0 2333 4061 0 4060 4061 0 734 4062 1
		 2862 4063 1 4062 4063 0 2236 4064 1 4062 4064 0 2863 4065 1 4064 4065 0 4063 4065 0
		 1164 4066 1 3930 4067 1 4066 4067 0 3932 4068 1 4066 4068 0 3931 4069 1 4068 4069 0
		 4067 4069 0 2237 4070 1 4062 4070 0 2234 4071 1 4070 4071 0 4064 4071 0 1163 4072 1
		 4070 4072 0 2238 4073 1 4073 4072 0 4073 4071 0 2864 4074 1 732 4075 1 4074 4075 0
		 4063 4075 0 4074 4065 0 736 4076 0 2867 4077 0 4076 4077 0 1488 4078 1 4076 4078 0
		 2865 4079 1 4078 4079 0 4077 4079 0 4078 4075 0 4074 4079 0 4067 4072 0 4073 4069 0
		 3936 4080 1 1165 4081 1 4080 4081 0 3934 4082 0 4082 4081 0 3935 4083 0 4082 4083 0
		 4080 4083 0 2243 4084 1 4066 4084 0 3937 4085 1 4084 4085 0 4068 4085 0 4084 4081 0
		 4080 4085 0;
	setAttr -s 3780 -ch 15728 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 7 0 191 72 128 17 -9 -17
		mu 0 7 8 141 53 94 9 18 17
		f 7 1 210 80 136 18 -10 -18
		mu 0 7 9 152 59 100 10 19 18
		f 7 2 228 87 144 19 -11 -19
		mu 0 7 10 162 64 106 11 20 19
		f 7 3 246 94 152 20 -12 -20
		mu 0 7 11 172 69 112 12 21 20
		f 7 4 264 101 160 21 -13 -21
		mu 0 7 12 182 74 118 13 22 21
		f 7 5 282 108 168 22 -14 -22
		mu 0 7 13 192 79 124 14 23 22
		f 7 6 300 115 176 23 -15 -23
		mu 0 7 14 202 84 130 15 24 23
		f 7 7 315 121 184 16 -16 -24
		mu 0 7 15 212 89 136 16 25 24
		f 18 -5756 -1544 -3040 -579 -4314 -962 -4308 -382 -5725 -1533 -3024 -573 -5714 -1527
		 -5708 -51 -52 52
		mu 0 18 44 3212 908 1733 356 2502 571 2499 246 3199 903 1723 353 3194 900 3191 45 34
		f 18 -6043 -1647 -3184 -618 -4427 -1010 -4421 -395 -6012 -1636 -3168 -612 -6001 -1630
		 -5995 -55 -53 55
		mu 0 18 46 3337 957 1823 377 2561 601 2558 253 3324 952 1813 374 3319 949 3316 44 34
		f 18 -6330 -1750 -3328 -657 -4540 -1058 -4534 -408 -6299 -1739 -3312 -651 -6288 -1733
		 -6282 -58 -56 58
		mu 0 18 47 3462 1006 1913 398 2620 631 2617 260 3449 1001 1903 395 3444 998 3441 46 34
		f 18 -6617 -1853 -3472 -696 -4653 -1106 -4647 -421 -6586 -1842 -3456 -690 -6575 -1836
		 -6569 -61 -59 61
		mu 0 18 48 3587 1055 2003 419 2679 661 2676 267 3574 1050 1993 416 3569 1047 3566 47 34
		f 18 -6904 -1956 -3616 -735 -4766 -1154 -4760 -434 -6873 -1945 -3600 -729 -6862 -1939
		 -6856 -64 -62 64
		mu 0 18 49 3712 1104 2093 440 2738 691 2735 274 3699 1099 2083 437 3694 1096 3691 48 34
		f 18 -7191 -2059 -3760 -774 -4879 -1202 -4873 -447 -7160 -2048 -3744 -768 -7149 -2042
		 -7143 -67 -65 67
		mu 0 18 50 3837 1153 2183 461 2797 721 2794 281 3824 1148 2173 458 3819 1145 3816 49 34
		f 18 -7478 -2162 -3904 -813 -4992 -1250 -4986 -460 -7447 -2151 -3888 -807 -7436 -2145
		 -7430 -70 -68 70
		mu 0 18 51 3962 1202 2273 482 2856 751 2853 288 3949 1197 2263 479 3944 1194 3941 50 34
		f 18 -7762 -2264 -4046 -852 -5102 -1297 -5096 -473 -7731 -2253 -4030 -846 -7720 -2247
		 -7714 -72 -71 51
		mu 0 18 45 4087 1251 2363 503 2915 781 2912 295 4074 1246 2353 500 4069 1243 4066 51 34
		f 3 8 25 -25
		mu 0 3 32 31 35
		f 3 9 26 -26
		mu 0 3 31 30 35
		f 3 10 27 -27
		mu 0 3 30 29 35
		f 3 11 28 -28
		mu 0 3 29 28 35
		f 3 12 29 -29
		mu 0 3 28 27 35
		f 3 13 30 -30
		mu 0 3 27 26 35
		f 3 14 31 -31
		mu 0 3 26 33 35
		f 3 15 24 -32
		mu 0 3 33 32 35
		f 4 -80 -128 131 -135
		mu 0 4 97 57 93 92
		f 4 -87 -136 139 -143
		mu 0 4 103 62 99 98
		f 4 -94 -144 147 -151
		mu 0 4 109 67 105 104
		f 4 -101 -152 155 -159
		mu 0 4 115 72 111 110
		f 4 -108 -160 163 -167
		mu 0 4 121 77 117 116
		f 4 -115 -168 171 -175
		mu 0 4 127 82 123 122
		f 4 -121 -176 179 -183
		mu 0 4 133 87 129 128
		f 4 -127 -184 187 -191
		mu 0 4 139 55 135 134
		f 4 -864 -2279 2282 -2286
		mu 0 4 1260 508 1257 1256
		f 4 -872 -2287 2290 -2294
		mu 0 4 1265 513 1262 1261
		f 4 -880 -2295 2298 -2302
		mu 0 4 1270 518 1267 1266
		f 4 -888 -2303 2306 -2310
		mu 0 4 1275 523 1272 1271
		f 4 -896 -2311 2314 -2318
		mu 0 4 1280 528 1277 1276
		f 4 -904 -2319 2322 -2326
		mu 0 4 1285 533 1282 1281
		f 4 -912 -2327 2330 -2334
		mu 0 4 1290 538 1287 1286
		f 4 -920 -2335 2338 -2342
		mu 0 4 1295 543 1292 1291
		f 4 -77 -192 193 -197
		mu 0 4 96 54 142 140
		f 4 -78 73 200 -204
		mu 0 4 143 55 145 144
		f 19 -79 74 5562 1473 2943 551 4238 929 4232 377 5531 1462 2927 545 5520 1456 5514 206
		 -210
		mu 0 19 147 56 3127 874 1673 341 2463 551 2460 242 3114 869 1663 338 3109 866 3106 149
		 148
		f 4 -84 -211 212 -216
		mu 0 4 102 60 153 151
		f 4 -85 75 218 -222
		mu 0 4 154 57 150 155
		f 19 -86 81 5850 1577 3087 591 4351 977 4345 391 5819 1566 3071 585 5808 1560 5802 224
		 -228
		mu 0 19 157 61 3253 924 1763 363 2522 581 2519 250 3240 919 1753 360 3235 916 3232 159
		 158
		f 4 -91 -229 230 -234
		mu 0 4 108 65 163 161
		f 4 -92 82 236 -240
		mu 0 4 164 62 160 165
		f 19 -93 88 6137 1680 3231 630 4464 1025 4458 404 6106 1669 3215 624 6095 1663 6089
		 242 -246
		mu 0 19 167 66 3378 973 1853 384 2581 611 2578 257 3365 968 1843 381 3360 965 3357 169
		 168
		f 4 -98 -247 248 -252
		mu 0 4 114 70 173 171
		f 4 -99 89 254 -258
		mu 0 4 174 67 170 175
		f 19 -100 95 6424 1783 3375 669 4577 1073 4571 417 6393 1772 3359 663 6382 1766 6376
		 260 -264
		mu 0 19 177 71 3503 1022 1943 405 2640 641 2637 264 3490 1017 1933 402 3485 1014 3482 179
		 178
		f 4 -105 -265 266 -270
		mu 0 4 120 75 183 181
		f 4 -106 96 272 -276
		mu 0 4 184 72 180 185
		f 19 -107 102 6711 1886 3519 708 4690 1121 4684 430 6680 1875 3503 702 6669 1869 6663
		 278 -282
		mu 0 19 187 76 3628 1071 2033 426 2699 671 2696 271 3615 1066 2023 423 3610 1063 3607 189
		 188
		f 4 -112 -283 284 -288
		mu 0 4 126 80 193 191
		f 4 -113 103 290 -294
		mu 0 4 194 77 190 195
		f 19 -114 109 6998 1989 3663 747 4803 1169 4797 443 6967 1978 3647 741 6956 1972 6950
		 296 -300
		mu 0 19 197 81 3753 1120 2123 447 2758 701 2755 278 3740 1115 2113 444 3735 1112 3732 199
		 198
		f 4 -118 -301 302 -306
		mu 0 4 132 85 203 201
		f 4 -341 342 344 -346
		mu 0 4 224 225 226 227
		f 19 -120 116 7285 2092 3807 786 4916 1217 4910 456 7254 2081 3791 780 7243 2075 7237
		 312 -315
		mu 0 19 207 86 3878 1169 2213 468 2817 731 2814 285 3865 1164 2203 465 3860 1161 3857 209
		 208
		f 4 -124 -316 317 -321
		mu 0 4 138 90 213 211
		f 4 -349 350 352 -354
		mu 0 4 228 229 230 231
		f 19 -126 122 7569 2194 3949 825 5029 1265 5023 469 7538 2183 3933 819 7527 2177 7521
		 326 -330
		mu 0 19 217 91 4003 1218 2303 489 2876 761 2873 292 3990 1213 2293 486 3985 1210 3982 219
		 218
		f 4 -34 -129 132 -132
		mu 0 4 93 1 95 92
		f 4 -73 76 133 -133
		mu 0 4 95 54 96 92
		f 4 129 -131 134 -134
		mu 0 4 96 52 97 92
		f 4 -36 -137 140 -140
		mu 0 4 99 2 101 98
		f 4 -81 83 141 -141
		mu 0 4 101 60 102 98
		f 4 137 -139 142 -142
		mu 0 4 102 58 103 98
		f 4 -38 -145 148 -148
		mu 0 4 105 3 107 104
		f 4 -88 90 149 -149
		mu 0 4 107 65 108 104
		f 4 145 -147 150 -150
		mu 0 4 108 63 109 104
		f 4 -40 -153 156 -156
		mu 0 4 111 4 113 110
		f 4 -95 97 157 -157
		mu 0 4 113 70 114 110
		f 4 153 -155 158 -158
		mu 0 4 114 68 115 110
		f 4 -42 -161 164 -164
		mu 0 4 117 5 119 116
		f 4 -102 104 165 -165
		mu 0 4 119 75 120 116
		f 4 161 -163 166 -166
		mu 0 4 120 73 121 116
		f 4 -44 -169 172 -172
		mu 0 4 123 6 125 122
		f 4 -109 111 173 -173
		mu 0 4 125 80 126 122
		f 4 169 -171 174 -174
		mu 0 4 126 78 127 122
		f 4 -46 -177 180 -180
		mu 0 4 129 7 131 128
		f 4 -116 117 181 -181
		mu 0 4 131 85 132 128
		f 4 177 -179 182 -182
		mu 0 4 132 83 133 128
		f 4 -33 -185 188 -188
		mu 0 4 135 0 137 134
		f 4 -122 123 189 -189
		mu 0 4 137 90 138 134
		f 4 185 -187 190 -190
		mu 0 4 138 88 139 134
		f 4 -1 32 194 -194
		mu 0 4 142 0 135 140
		f 4 183 77 195 -195
		mu 0 4 135 55 143 140
		f 4 192 -130 196 -196
		mu 0 4 143 52 96 140
		f 19 197 34 5660 1509 2991 565 4276 945 4270 379 5629 1498 2975 559 5618 1492 5612 201
		 -201
		mu 0 19 145 37 3171 892 1703 349 2483 561 2480 244 3158 887 1693 346 3153 884 3150 146
		 144
		f 19 198 5611 1491 2967 558 4257 937 4251 378 5580 1480 2951 552 5569 1474 5563 78 202
		 -202
		mu 0 19 146 3149 883 1688 345 2473 556 2470 243 3136 878 1678 342 3131 875 3128 56 147
		 144
		f 4 199 -193 203 -203
		mu 0 4 147 52 143 144
		f 19 204 5513 1455 2919 544 4219 921 4213 376 5131 1310 2735 481 5120 1304 5114 -206
		 207 -207
		mu 0 19 149 3105 865 1658 337 2453 546 2450 241 2930 789 1543 298 2925 786 2922 36 150
		 148
		f 4 -76 79 208 -208
		mu 0 4 150 57 97 148
		f 4 130 -200 209 -209
		mu 0 4 97 52 147 148
		f 4 -2 33 213 -213
		mu 0 4 153 1 93 151
		f 4 127 84 214 -214
		mu 0 4 93 57 154 151
		f 4 211 -138 215 -215
		mu 0 4 154 58 102 151
		f 19 205 36 5948 1613 3135 605 4389 993 4383 393 5917 1602 3119 599 5906 1596 5900 219
		 -219
		mu 0 19 150 36 3297 942 1793 371 2542 591 2539 252 3284 937 1783 368 3279 934 3276 156
		 155
		f 19 216 5899 1595 3111 598 4370 985 4364 392 5868 1584 3095 592 5857 1578 5851 85 220
		 -220
		mu 0 19 156 3275 933 1778 367 2532 586 2529 251 3262 928 1768 364 3257 925 3254 61 157
		 155
		f 4 217 -212 221 -221
		mu 0 4 157 58 154 155
		f 19 222 5801 1559 3063 584 4332 969 4326 390 5181 1329 2759 489 5170 1323 5164 -224
		 225 -225
		mu 0 19 159 3231 915 1748 359 2512 576 2509 249 2953 799 1558 303 2948 796 2945 38 160
		 158
		f 4 -83 86 226 -226
		mu 0 4 160 62 103 158
		f 4 138 -218 227 -227
		mu 0 4 103 58 157 158
		f 4 -3 35 231 -231
		mu 0 4 163 2 99 161
		f 4 135 91 232 -232
		mu 0 4 99 62 164 161
		f 4 229 -146 233 -233
		mu 0 4 164 63 108 161
		f 19 223 38 6235 1716 3279 644 4502 1041 4496 406 6204 1705 3263 638 6193 1699 6187
		 237 -237
		mu 0 19 160 38 3422 991 1883 392 2601 621 2598 259 3409 986 1873 389 3404 983 3401 166
		 165
		f 19 234 6186 1698 3255 637 4483 1033 4477 405 6155 1687 3239 631 6144 1681 6138 92
		 238 -238
		mu 0 19 166 3400 982 1868 388 2591 616 2588 258 3387 977 1858 385 3382 974 3379 66 167
		 165
		f 4 235 -230 239 -239
		mu 0 4 167 63 164 165
		f 19 240 6088 1662 3207 623 4445 1017 4439 403 5231 1348 2783 497 5220 1342 5214 -242
		 243 -243
		mu 0 19 169 3356 964 1838 380 2571 606 2568 256 2976 809 1573 308 2971 806 2968 39 170
		 168
		f 4 -90 93 244 -244
		mu 0 4 170 67 109 168
		f 4 146 -236 245 -245
		mu 0 4 109 63 167 168
		f 4 -4 37 249 -249
		mu 0 4 173 3 105 171
		f 4 143 98 250 -250
		mu 0 4 105 67 174 171
		f 4 247 -154 251 -251
		mu 0 4 174 68 114 171
		f 19 241 40 6522 1819 3423 683 4615 1089 4609 419 6491 1808 3407 677 6480 1802 6474
		 255 -255
		mu 0 19 170 39 3547 1040 1973 413 2660 651 2657 266 3534 1035 1963 410 3529 1032 3526 176
		 175
		f 19 252 6473 1801 3399 676 4596 1081 4590 418 6442 1790 3383 670 6431 1784 6425 99
		 256 -256
		mu 0 19 176 3525 1031 1958 409 2650 646 2647 265 3512 1026 1948 406 3507 1023 3504 71 177
		 175
		f 4 253 -248 257 -257
		mu 0 4 177 68 174 175
		f 19 258 6375 1765 3351 662 4558 1065 4552 416 5281 1367 2807 505 5270 1361 5264 -260
		 261 -261
		mu 0 19 179 3481 1013 1928 401 2630 636 2627 263 2999 819 1588 313 2994 816 2991 40 180
		 178
		f 4 -97 100 262 -262
		mu 0 4 180 72 115 178
		f 4 154 -254 263 -263
		mu 0 4 115 68 177 178
		f 4 -5 39 267 -267
		mu 0 4 183 4 111 181
		f 4 151 105 268 -268
		mu 0 4 111 72 184 181
		f 4 265 -162 269 -269
		mu 0 4 184 73 120 181
		f 19 259 42 6809 1922 3567 722 4728 1137 4722 432 6778 1911 3551 716 6767 1905 6761
		 273 -273
		mu 0 19 180 40 3672 1089 2063 434 2719 681 2716 273 3659 1084 2053 431 3654 1081 3651 186
		 185
		f 19 270 6760 1904 3543 715 4709 1129 4703 431 6729 1893 3527 709 6718 1887 6712 106
		 274 -274
		mu 0 19 186 3650 1080 2048 430 2709 676 2706 272 3637 1075 2038 427 3632 1072 3629 76 187
		 185
		f 4 271 -266 275 -275
		mu 0 4 187 73 184 185
		f 19 276 6662 1868 3495 701 4671 1113 4665 429 5331 1386 2831 513 5320 1380 5314 -278
		 279 -279
		mu 0 19 189 3606 1062 2018 422 2689 666 2686 270 3022 829 1603 318 3017 826 3014 41 190
		 188
		f 4 -104 107 280 -280
		mu 0 4 190 77 121 188
		f 4 162 -272 281 -281
		mu 0 4 121 73 187 188
		f 4 -6 41 285 -285
		mu 0 4 193 5 117 191
		f 4 159 112 286 -286
		mu 0 4 117 77 194 191
		f 4 283 -170 287 -287
		mu 0 4 194 78 126 191
		f 19 277 44 7096 2025 3711 761 4841 1185 4835 445 7065 2014 3695 755 7054 2008 7048
		 291 -291
		mu 0 19 190 41 3797 1138 2153 455 2778 711 2775 280 3784 1133 2143 452 3779 1130 3776 196
		 195
		f 19 288 7047 2007 3687 754 4822 1177 4816 444 7016 1996 3671 748 7005 1990 6999 113
		 292 -292
		mu 0 19 196 3775 1129 2138 451 2768 706 2765 279 3762 1124 2128 448 3757 1121 3754 81 197
		 195
		f 4 332 -335 336 -338
		mu 0 4 220 221 222 223
		f 19 294 6949 1971 3639 740 4784 1161 4778 442 5381 1405 2855 521 5370 1399 5364 -296
		 297 -297
		mu 0 19 199 3731 1111 2108 443 2748 696 2745 277 3045 839 1618 323 3040 836 3037 42 200
		 198
		f 4 -111 114 298 -298
		mu 0 4 200 82 127 198
		f 4 170 -290 299 -299
		mu 0 4 127 78 197 198
		f 4 -7 43 303 -303
		mu 0 4 203 6 123 201
		f 4 167 118 304 -304
		mu 0 4 123 82 204 201
		f 4 301 -178 305 -305
		mu 0 4 204 83 132 201
		f 19 295 46 7383 2128 3855 800 4954 1233 4948 458 7352 2117 3839 794 7341 2111 7335
		 308 -308
		mu 0 19 200 42 3922 1187 2243 476 2837 741 2834 287 3909 1182 2233 473 3904 1179 3901 206
		 205
		f 19 306 7334 2110 3831 793 4935 1225 4929 457 7303 2099 3815 787 7292 2093 7286 119
		 309 -309
		mu 0 19 206 3900 1178 2228 472 2827 736 2824 286 3887 1173 2218 469 3882 1170 3879 86 207
		 205
		f 4 356 -358 345 -359
		mu 0 4 233 232 224 227
		f 19 310 7236 2074 3783 779 4897 1209 4891 455 5431 1424 2879 529 5420 1418 5414 -312
		 313 -313
		mu 0 19 209 3856 1160 2198 464 2807 726 2804 284 3068 849 1633 328 3063 846 3060 43 210
		 208
		f 4 -351 360 362 -364
		mu 0 4 230 229 234 235
		f 4 364 -357 365 -363
		mu 0 4 234 232 233 235
		f 4 -8 45 318 -318
		mu 0 4 213 7 129 211
		f 4 175 124 319 -319
		mu 0 4 129 87 214 211
		f 4 316 -186 320 -320
		mu 0 4 214 88 138 211
		f 19 311 47 7667 2230 3997 839 5067 1281 5061 471 7636 2219 3981 833 7625 2213 7619
		 323 -323
		mu 0 19 210 43 4047 1236 2333 497 2896 771 2893 294 4034 1231 2323 494 4029 1228 4026 216
		 215
		f 19 321 7618 2212 3973 832 5048 1273 5042 470 7587 2201 3957 826 7576 2195 7570 125
		 324 -324
		mu 0 19 216 4025 1227 2318 493 2886 766 2883 293 4012 1222 2308 490 4007 1219 4004 91 217
		 215
		f 4 368 -370 353 -371
		mu 0 4 237 236 228 231
		f 19 325 7520 2176 3925 818 5010 1257 5004 468 5481 1443 2903 537 5470 1437 5464 -198
		 327 -327
		mu 0 19 219 3981 1209 2288 485 2866 756 2863 291 3091 859 1648 333 3086 856 3083 37 145
		 218
		f 4 -74 126 328 -328
		mu 0 4 145 55 139 218
		f 4 372 -369 374 -376
		mu 0 4 238 236 237 239
		f 4 289 331 -333 -331
		mu 0 4 197 78 221 220
		f 4 -284 333 334 -332
		mu 0 4 78 194 222 221
		f 4 293 335 -337 -334
		mu 0 4 194 195 223 222
		f 4 -293 330 337 -336
		mu 0 4 195 197 220 223
		f 4 -119 338 340 -340
		mu 0 4 204 82 225 224
		f 4 110 341 -343 -339
		mu 0 4 82 200 226 225
		f 4 307 343 -345 -342
		mu 0 4 200 205 227 226
		f 4 -125 346 348 -348
		mu 0 4 214 87 229 228
		f 4 322 351 -353 -350
		mu 0 4 210 215 231 230
		f 4 -302 339 357 -356
		mu 0 4 83 204 224 232
		f 4 -310 354 358 -344
		mu 0 4 205 207 233 227
		f 4 120 359 -361 -347
		mu 0 4 87 133 234 229
		f 4 -314 349 363 -362
		mu 0 4 208 210 230 235
		f 4 178 355 -365 -360
		mu 0 4 133 83 232 234
		f 4 314 361 -366 -355
		mu 0 4 207 208 235 233
		f 4 -317 347 369 -368
		mu 0 4 88 214 228 236
		f 4 -325 366 370 -352
		mu 0 4 215 217 237 231
		f 4 186 367 -373 -372
		mu 0 4 139 88 236 238
		f 4 329 373 -375 -367
		mu 0 4 217 218 239 237
		f 4 -329 371 375 -374
		mu 0 4 218 139 238 239
		f 4 -928 -2343 2346 -2350
		mu 0 4 1300 548 1297 1296
		f 4 -936 -2351 2354 -2358
		mu 0 4 1305 553 1302 1301
		f 4 -944 -2359 2362 -2366
		mu 0 4 1310 558 1307 1306
		f 4 -952 -2367 2370 -2374
		mu 0 4 1315 563 1312 1311
		f 4 -960 -2375 2378 -2382
		mu 0 4 1320 568 1317 1316
		f 4 -968 -2383 2386 -2390
		mu 0 4 1325 573 1322 1321
		f 4 -976 -2391 2394 -2398
		mu 0 4 1330 578 1327 1326
		f 4 -984 -2399 2402 -2406
		mu 0 4 1335 583 1332 1331
		f 4 -992 -2407 2410 -2414
		mu 0 4 1340 588 1337 1336
		f 4 -1000 -2415 2418 -2422
		mu 0 4 1345 593 1342 1341
		f 4 -1008 -2423 2426 -2430
		mu 0 4 1350 598 1347 1346
		f 4 -1016 -2431 2434 -2438
		mu 0 4 1355 603 1352 1351
		f 4 -1024 -2439 2442 -2446
		mu 0 4 1360 608 1357 1356
		f 4 -1032 -2447 2450 -2454
		mu 0 4 1365 613 1362 1361
		f 4 -1040 -2455 2458 -2462
		mu 0 4 1370 618 1367 1366
		f 4 -1048 -2463 2466 -2470
		mu 0 4 1375 623 1372 1371
		f 4 -1056 -2471 2474 -2478
		mu 0 4 1380 628 1377 1376
		f 4 -1064 -2479 2482 -2486
		mu 0 4 1385 633 1382 1381
		f 4 -1072 -2487 2490 -2494
		mu 0 4 1390 638 1387 1386
		f 4 -1080 -2495 2498 -2502
		mu 0 4 1395 643 1392 1391
		f 4 -1088 -2503 2506 -2510
		mu 0 4 1400 648 1397 1396
		f 4 -1096 -2511 2514 -2518
		mu 0 4 1405 653 1402 1401
		f 4 -1104 -2519 2522 -2526
		mu 0 4 1410 658 1407 1406
		f 4 -1112 -2527 2530 -2534
		mu 0 4 1415 663 1412 1411
		f 4 -1120 -2535 2538 -2542
		mu 0 4 1420 668 1417 1416
		f 4 -1128 -2543 2546 -2550
		mu 0 4 1425 673 1422 1421
		f 4 -1136 -2551 2554 -2558
		mu 0 4 1430 678 1427 1426
		f 4 -1144 -2559 2562 -2566
		mu 0 4 1435 683 1432 1431
		f 4 -1152 -2567 2570 -2574
		mu 0 4 1440 688 1437 1436
		f 4 -1160 -2575 2578 -2582
		mu 0 4 1445 693 1442 1441
		f 4 -1168 -2583 2586 -2590
		mu 0 4 1450 698 1447 1446
		f 4 -1176 -2591 2594 -2598
		mu 0 4 1455 703 1452 1451
		f 4 -1184 -2599 2602 -2606
		mu 0 4 1460 708 1457 1456
		f 4 -1192 -2607 2610 -2614
		mu 0 4 1465 713 1462 1461
		f 4 -1200 -2615 2618 -2622
		mu 0 4 1470 718 1467 1466
		f 4 -1208 -2623 2626 -2630
		mu 0 4 1475 723 1472 1471
		f 4 -1216 -2631 2634 -2638
		mu 0 4 1480 728 1477 1476
		f 4 -1224 -2639 2642 -2646
		mu 0 4 1485 733 1482 1481
		f 4 -1232 -2647 2650 -2654
		mu 0 4 1490 738 1487 1486
		f 4 -1240 -2655 2658 -2662
		mu 0 4 1495 743 1492 1491
		f 4 -1248 -2663 2666 -2670
		mu 0 4 1500 748 1497 1496
		f 4 -1256 -2671 2674 -2678
		mu 0 4 1505 753 1502 1501
		f 4 -1264 -2679 2682 -2686
		mu 0 4 1510 758 1507 1506
		f 4 -1272 -2687 2690 -2694
		mu 0 4 1515 763 1512 1511
		f 4 -1280 -2695 2698 -2702
		mu 0 4 1520 768 1517 1516
		f 4 -1288 -2703 2706 -2710
		mu 0 4 1525 773 1522 1521
		f 4 -1295 -2711 2714 -2718
		mu 0 4 1530 778 1527 1526
		f 4 -1303 -2719 2722 -2726
		mu 0 4 1535 783 1532 1531
		f 4 -1310 -2727 2730 -2734
		mu 0 4 1540 507 1537 1536
		f 4 -1316 -2735 2738 -2742
		mu 0 4 1545 787 1542 1541
		f 4 -1322 -2743 2746 -2750
		mu 0 4 1550 790 1547 1546
		f 4 -1329 -2751 2754 -2758
		mu 0 4 1555 512 1552 1551
		f 4 -1335 -2759 2762 -2766
		mu 0 4 1560 797 1557 1556
		f 4 -1341 -2767 2770 -2774
		mu 0 4 1565 800 1562 1561
		f 4 -1348 -2775 2778 -2782
		mu 0 4 1570 517 1567 1566
		f 4 -1354 -2783 2786 -2790
		mu 0 4 1575 807 1572 1571
		f 4 -1360 -2791 2794 -2798
		mu 0 4 1580 810 1577 1576
		f 4 -1367 -2799 2802 -2806
		mu 0 4 1585 522 1582 1581
		f 4 -1373 -2807 2810 -2814
		mu 0 4 1590 817 1587 1586
		f 4 -1379 -2815 2818 -2822
		mu 0 4 1595 820 1592 1591
		f 4 -1386 -2823 2826 -2830
		mu 0 4 1600 527 1597 1596
		f 4 -1392 -2831 2834 -2838
		mu 0 4 1605 827 1602 1601
		f 4 -1398 -2839 2842 -2846
		mu 0 4 1610 830 1607 1606
		f 4 -1405 -2847 2850 -2854
		mu 0 4 1615 532 1612 1611
		f 4 -1411 -2855 2858 -2862
		mu 0 4 1620 837 1617 1616
		f 4 -1417 -2863 2866 -2870
		mu 0 4 1625 840 1622 1621
		f 4 -1424 -2871 2874 -2878
		mu 0 4 1630 537 1627 1626
		f 4 -1430 -2879 2882 -2886
		mu 0 4 1635 847 1632 1631
		f 4 -1436 -2887 2890 -2894
		mu 0 4 1640 850 1637 1636
		f 4 -1443 -2895 2898 -2902
		mu 0 4 1645 542 1642 1641
		f 4 -1449 -2903 2906 -2910
		mu 0 4 1650 857 1647 1646
		f 4 -1455 -2911 2914 -2918
		mu 0 4 1655 860 1652 1651
		f 4 -1462 -2919 2922 -2926
		mu 0 4 1660 547 1657 1656
		f 4 -1468 -2927 2930 -2934
		mu 0 4 1665 867 1662 1661
		f 4 -1473 -2935 2938 -2942
		mu 0 4 1670 870 1667 1666
		f 4 -1480 -2943 2946 -2950
		mu 0 4 1675 552 1672 1671
		f 4 -1486 -2951 2954 -2958
		mu 0 4 1680 876 1677 1676
		f 4 -1491 -2959 2962 -2966
		mu 0 4 1685 879 1682 1681
		f 4 -1498 -2967 2970 -2974
		mu 0 4 1690 557 1687 1686
		f 4 -1504 -2975 2978 -2982
		mu 0 4 1695 885 1692 1691
		f 4 -1509 -2983 2986 -2990
		mu 0 4 1700 888 1697 1696
		f 4 -1515 -2991 2994 -2998
		mu 0 4 1705 562 1702 1701
		f 4 -1520 -2999 3002 -3006
		mu 0 4 1710 893 1707 1706
		f 4 -1525 -3007 3010 -3014
		mu 0 4 1715 895 1712 1711
		f 4 -1532 -3015 3018 -3022
		mu 0 4 1720 567 1717 1716
		f 4 -1538 -3023 3026 -3030
		mu 0 4 1725 901 1722 1721
		f 4 -1543 -3031 3034 -3038
		mu 0 4 1730 904 1727 1726
		f 4 -1550 -3039 3042 -3046
		mu 0 4 1735 572 1732 1731
		f 4 -1555 -3047 3050 -3054
		mu 0 4 1740 910 1737 1736
		f 4 -1559 -3055 3058 -3062
		mu 0 4 1745 912 1742 1741
		f 4 -1566 -3063 3066 -3070
		mu 0 4 1750 577 1747 1746
		f 4 -1572 -3071 3074 -3078
		mu 0 4 1755 917 1752 1751
		f 4 -1577 -3079 3082 -3086
		mu 0 4 1760 920 1757 1756
		f 4 -1584 -3087 3090 -3094
		mu 0 4 1765 582 1762 1761
		f 4 -1590 -3095 3098 -3102
		mu 0 4 1770 926 1767 1766
		f 4 -1595 -3103 3106 -3110
		mu 0 4 1775 929 1772 1771
		f 4 -1602 -3111 3114 -3118
		mu 0 4 1780 587 1777 1776
		f 4 -1608 -3119 3122 -3126
		mu 0 4 1785 935 1782 1781
		f 4 -1613 -3127 3130 -3134
		mu 0 4 1790 938 1787 1786
		f 4 -1619 -3135 3138 -3142
		mu 0 4 1795 592 1792 1791
		f 4 -1624 -3143 3146 -3150
		mu 0 4 1800 943 1797 1796
		f 4 -1629 -3151 3154 -3158
		mu 0 4 1805 945 1802 1801
		f 4 -1635 -3159 3162 -3166
		mu 0 4 1810 597 1807 1806
		f 4 -1641 -3167 3170 -3174
		mu 0 4 1815 950 1812 1811
		f 4 -1646 -3175 3178 -3182
		mu 0 4 1820 953 1817 1816
		f 4 -1653 -3183 3186 -3190
		mu 0 4 1825 602 1822 1821
		f 4 -1658 -3191 3194 -3198
		mu 0 4 1830 959 1827 1826
		f 4 -1662 -3199 3202 -3206
		mu 0 4 1835 961 1832 1831
		f 4 -1669 -3207 3210 -3214
		mu 0 4 1840 607 1837 1836
		f 4 -1675 -3215 3218 -3222
		mu 0 4 1845 966 1842 1841
		f 4 -1680 -3223 3226 -3230
		mu 0 4 1850 969 1847 1846
		f 4 -1687 -3231 3234 -3238
		mu 0 4 1855 612 1852 1851
		f 4 -1693 -3239 3242 -3246
		mu 0 4 1860 975 1857 1856
		f 4 -1698 -3247 3250 -3254
		mu 0 4 1865 978 1862 1861
		f 4 -1705 -3255 3258 -3262
		mu 0 4 1870 617 1867 1866
		f 4 -1711 -3263 3266 -3270
		mu 0 4 1875 984 1872 1871
		f 4 -1716 -3271 3274 -3278
		mu 0 4 1880 987 1877 1876
		f 4 -1722 -3279 3282 -3286
		mu 0 4 1885 622 1882 1881
		f 4 -1727 -3287 3290 -3294
		mu 0 4 1890 992 1887 1886
		f 4 -1732 -3295 3298 -3302
		mu 0 4 1895 994 1892 1891
		f 4 -1738 -3303 3306 -3310
		mu 0 4 1900 627 1897 1896
		f 4 -1744 -3311 3314 -3318
		mu 0 4 1905 999 1902 1901
		f 4 -1749 -3319 3322 -3326
		mu 0 4 1910 1002 1907 1906
		f 4 -1756 -3327 3330 -3334
		mu 0 4 1915 632 1912 1911
		f 4 -1761 -3335 3338 -3342
		mu 0 4 1920 1008 1917 1916
		f 4 -1765 -3343 3346 -3350
		mu 0 4 1925 1010 1922 1921
		f 4 -1772 -3351 3354 -3358
		mu 0 4 1930 637 1927 1926
		f 4 -1778 -3359 3362 -3366
		mu 0 4 1935 1015 1932 1931
		f 4 -1783 -3367 3370 -3374
		mu 0 4 1940 1018 1937 1936
		f 4 -1790 -3375 3378 -3382
		mu 0 4 1945 642 1942 1941
		f 4 -1796 -3383 3386 -3390
		mu 0 4 1950 1024 1947 1946
		f 4 -1801 -3391 3394 -3398
		mu 0 4 1955 1027 1952 1951
		f 4 -1808 -3399 3402 -3406
		mu 0 4 1960 647 1957 1956
		f 4 -1814 -3407 3410 -3414
		mu 0 4 1965 1033 1962 1961
		f 4 -1819 -3415 3418 -3422
		mu 0 4 1970 1036 1967 1966
		f 4 -1825 -3423 3426 -3430
		mu 0 4 1975 652 1972 1971
		f 4 -1830 -3431 3434 -3438
		mu 0 4 1980 1041 1977 1976
		f 4 -1835 -3439 3442 -3446
		mu 0 4 1985 1043 1982 1981
		f 4 -1841 -3447 3450 -3454
		mu 0 4 1990 657 1987 1986
		f 4 -1847 -3455 3458 -3462
		mu 0 4 1995 1048 1992 1991
		f 4 -1852 -3463 3466 -3470
		mu 0 4 2000 1051 1997 1996
		f 4 -1859 -3471 3474 -3478
		mu 0 4 2005 662 2002 2001
		f 4 -1864 -3479 3482 -3486
		mu 0 4 2010 1057 2007 2006
		f 4 -1868 -3487 3490 -3494
		mu 0 4 2015 1059 2012 2011
		f 4 -1875 -3495 3498 -3502
		mu 0 4 2020 667 2017 2016
		f 4 -1881 -3503 3506 -3510
		mu 0 4 2025 1064 2022 2021
		f 4 -1886 -3511 3514 -3518
		mu 0 4 2030 1067 2027 2026
		f 4 -1893 -3519 3522 -3526
		mu 0 4 2035 672 2032 2031
		f 4 -1899 -3527 3530 -3534
		mu 0 4 2040 1073 2037 2036
		f 4 -1904 -3535 3538 -3542
		mu 0 4 2045 1076 2042 2041
		f 4 -1911 -3543 3546 -3550
		mu 0 4 2050 677 2047 2046
		f 4 -1917 -3551 3554 -3558
		mu 0 4 2055 1082 2052 2051
		f 4 -1922 -3559 3562 -3566
		mu 0 4 2060 1085 2057 2056
		f 4 -1928 -3567 3570 -3574
		mu 0 4 2065 682 2062 2061
		f 4 -1933 -3575 3578 -3582
		mu 0 4 2070 1090 2067 2066
		f 4 -1938 -3583 3586 -3590
		mu 0 4 2075 1092 2072 2071
		f 4 -1944 -3591 3594 -3598
		mu 0 4 2080 687 2077 2076
		f 4 -1950 -3599 3602 -3606
		mu 0 4 2085 1097 2082 2081
		f 4 -1955 -3607 3610 -3614
		mu 0 4 2090 1100 2087 2086
		f 4 -1962 -3615 3618 -3622
		mu 0 4 2095 692 2092 2091
		f 4 -1967 -3623 3626 -3630
		mu 0 4 2100 1106 2097 2096
		f 4 -1971 -3631 3634 -3638
		mu 0 4 2105 1108 2102 2101
		f 4 -1978 -3639 3642 -3646
		mu 0 4 2110 697 2107 2106
		f 4 -1984 -3647 3650 -3654
		mu 0 4 2115 1113 2112 2111
		f 4 -1989 -3655 3658 -3662
		mu 0 4 2120 1116 2117 2116
		f 4 -1996 -3663 3666 -3670
		mu 0 4 2125 702 2122 2121
		f 4 -2002 -3671 3674 -3678
		mu 0 4 2130 1122 2127 2126
		f 4 -2007 -3679 3682 -3686
		mu 0 4 2135 1125 2132 2131
		f 4 -2014 -3687 3690 -3694
		mu 0 4 2140 707 2137 2136
		f 4 -2020 -3695 3698 -3702
		mu 0 4 2145 1131 2142 2141
		f 4 -2025 -3703 3706 -3710
		mu 0 4 2150 1134 2147 2146
		f 4 -2031 -3711 3714 -3718
		mu 0 4 2155 712 2152 2151
		f 4 -2036 -3719 3722 -3726
		mu 0 4 2160 1139 2157 2156
		f 4 -2041 -3727 3730 -3734
		mu 0 4 2165 1141 2162 2161
		f 4 -2047 -3735 3738 -3742
		mu 0 4 2170 717 2167 2166
		f 4 -2053 -3743 3746 -3750
		mu 0 4 2175 1146 2172 2171
		f 4 -2058 -3751 3754 -3758
		mu 0 4 2180 1149 2177 2176
		f 4 -2065 -3759 3762 -3766
		mu 0 4 2185 722 2182 2181
		f 4 -2070 -3767 3770 -3774
		mu 0 4 2190 1155 2187 2186
		f 4 -2074 -3775 3778 -3782
		mu 0 4 2195 1157 2192 2191
		f 4 -2081 -3783 3786 -3790
		mu 0 4 2200 727 2197 2196
		f 4 -2087 -3791 3794 -3798
		mu 0 4 2205 1162 2202 2201
		f 4 -2092 -3799 3802 -3806
		mu 0 4 2210 1165 2207 2206
		f 4 -2099 -3807 3810 -3814
		mu 0 4 2215 732 2212 2211
		f 4 -2105 -3815 3818 -3822
		mu 0 4 2220 1171 2217 2216
		f 4 -2110 -3823 3826 -3830
		mu 0 4 2225 1174 2222 2221
		f 4 -2117 -3831 3834 -3838
		mu 0 4 2230 737 2227 2226
		f 4 -2123 -3839 3842 -3846
		mu 0 4 2235 1180 2232 2231
		f 4 -2128 -3847 3850 -3854
		mu 0 4 2240 1183 2237 2236
		f 4 -2134 -3855 3858 -3862
		mu 0 4 2245 742 2242 2241
		f 4 -2139 -3863 3866 -3870
		mu 0 4 2250 1188 2247 2246
		f 4 -2144 -3871 3874 -3878
		mu 0 4 2255 1190 2252 2251
		f 4 -2150 -3879 3882 -3886
		mu 0 4 2260 747 2257 2256
		f 4 -2156 -3887 3890 -3894
		mu 0 4 2265 1195 2262 2261
		f 4 -2161 -3895 3898 -3902
		mu 0 4 2270 1198 2267 2266
		f 4 -2168 -3903 3906 -3910
		mu 0 4 2275 752 2272 2271
		f 4 -2172 -3911 3913 -3916
		mu 0 4 2280 1204 2277 2276
		f 4 -2176 -3917 3920 -3924
		mu 0 4 2285 1206 2282 2281
		f 4 -2183 -3925 3928 -3932
		mu 0 4 2290 757 2287 2286
		f 4 -2189 -3933 3936 -3940
		mu 0 4 2295 1211 2292 2291
		f 4 -2194 -3941 3944 -3948
		mu 0 4 2300 1214 2297 2296
		f 4 -2201 -3949 3952 -3956
		mu 0 4 2305 762 2302 2301
		f 4 -2207 -3957 3960 -3964
		mu 0 4 2310 1220 2307 2306
		f 4 -2212 -3965 3968 -3972
		mu 0 4 2315 1223 2312 2311
		f 4 -2219 -3973 3976 -3980
		mu 0 4 2320 767 2317 2316
		f 4 -2225 -3981 3984 -3988
		mu 0 4 2325 1229 2322 2321
		f 4 -2230 -3989 3992 -3996
		mu 0 4 2330 1232 2327 2326
		f 4 -2236 -3997 4000 -4004
		mu 0 4 2335 772 2332 2331
		f 4 -2241 -4005 4008 -4012
		mu 0 4 2340 1237 2337 2336
		f 4 -2246 -4013 4016 -4020
		mu 0 4 2345 1239 2342 2341
		f 4 -2252 -4021 4024 -4028
		mu 0 4 2350 777 2347 2346
		f 4 -2258 -4029 4032 -4036
		mu 0 4 2355 1244 2352 2351
		f 4 -2263 -4037 4040 -4044
		mu 0 4 2360 1247 2357 2356
		f 4 -2269 -4045 4048 -4052
		mu 0 4 2365 782 2362 2361
		f 4 -2274 -4053 4056 -4060
		mu 0 4 2370 1252 2367 2366
		f 4 -2278 -4061 4064 -4068
		mu 0 4 2375 1254 2372 2371
		f 4 -861 -4069 4071 -4075
		mu 0 4 1259 505 2377 2376
		f 4 -862 -3144 4076 -4080
		mu 0 4 2379 506 1798 2380
		f 4 -863 858 4082 -4086
		mu 0 4 2381 507 2383 2382
		f 4 -869 -4087 4089 -4093
		mu 0 4 1264 510 2386 2385
		f 4 -870 -3288 4094 -4098
		mu 0 4 2388 511 1888 2389
		f 4 -871 866 4100 -4104
		mu 0 4 2390 512 2392 2391
		f 4 -877 -4105 4107 -4111
		mu 0 4 1269 515 2395 2394
		f 4 -878 -3432 4112 -4116
		mu 0 4 2397 516 1978 2398
		f 4 -879 874 4118 -4122
		mu 0 4 2399 517 2401 2400
		f 4 -885 -4123 4125 -4129
		mu 0 4 1274 520 2404 2403
		f 4 -886 -3576 4130 -4134
		mu 0 4 2406 521 2068 2407
		f 4 -887 882 4136 -4140
		mu 0 4 2408 522 2410 2409
		f 4 -893 -4141 4143 -4147
		mu 0 4 1279 525 2413 2412
		f 4 -894 -3720 4148 -4152
		mu 0 4 2415 526 2158 2416
		f 4 -895 890 4154 -4158
		mu 0 4 2417 527 2419 2418
		f 4 -901 -4159 4161 -4165
		mu 0 4 1284 530 2422 2421
		f 4 -902 -3864 4166 -4170
		mu 0 4 2424 531 2248 2425
		f 4 -903 898 4172 -4176
		mu 0 4 2426 532 2428 2427
		f 4 -909 -4177 4179 -4183
		mu 0 4 1289 535 2431 2430
		f 4 -910 -4006 4184 -4188
		mu 0 4 2433 536 2338 2434
		f 4 -911 906 4190 -4194
		mu 0 4 2435 537 2437 2436
		f 4 -917 -4195 4197 -4201
		mu 0 4 1294 540 2440 2439
		f 4 -918 -3000 4202 -4206
		mu 0 4 2442 541 1708 2443
		f 4 -919 914 4208 -4212
		mu 0 4 2444 542 2446 2445
		f 4 -925 -4213 4215 -4219
		mu 0 4 1299 545 2449 2448
		f 4 -926 -4220 4221 -4225
		mu 0 4 2451 546 2453 2452
		f 4 -927 922 4227 -4231
		mu 0 4 2454 547 2456 2455
		f 4 -933 -4232 4234 -4238
		mu 0 4 1304 550 2459 2458
		f 4 -934 -4239 4240 -4244
		mu 0 4 2461 551 2463 2462
		f 4 -935 930 4246 -4250
		mu 0 4 2464 552 2466 2465
		f 4 -941 -4251 4253 -4257
		mu 0 4 1309 555 2469 2468
		f 4 -942 -4258 4259 -4263
		mu 0 4 2471 556 2473 2472
		f 4 -943 938 4265 -4269
		mu 0 4 2474 557 2476 2475
		f 4 -949 -4270 4272 -4276
		mu 0 4 1314 560 2479 2478
		f 4 -950 -4277 4278 -4282
		mu 0 4 2481 561 2483 2482
		f 4 -951 946 4284 -4288
		mu 0 4 2484 562 2486 2485
		f 4 -957 -4289 4291 -4295
		mu 0 4 1319 565 2489 2488
		f 4 -958 953 4296 -4300
		mu 0 4 2491 566 2368 2492
		f 4 -959 954 4302 -4306
		mu 0 4 2493 567 2495 2494
		f 4 -965 -4307 4309 -4313
		mu 0 4 1324 570 2498 2497
		f 4 -966 961 4315 -4319
		mu 0 4 2500 571 2502 2501
		f 4 -967 962 4321 -4325
		mu 0 4 2503 572 2505 2504
		f 4 -973 -4326 4328 -4332
		mu 0 4 1329 575 2508 2507
		f 4 -974 -4333 4334 -4338
		mu 0 4 2510 576 2512 2511
		f 4 -975 970 4340 -4344
		mu 0 4 2513 577 2515 2514
		f 4 -981 -4345 4347 -4351
		mu 0 4 1334 580 2518 2517
		f 4 -982 -4352 4353 -4357
		mu 0 4 2520 581 2522 2521
		f 4 -983 978 4359 -4363
		mu 0 4 2523 582 2525 2524
		f 4 -989 -4364 4366 -4370
		mu 0 4 1339 585 2528 2527
		f 4 -990 -4371 4372 -4376
		mu 0 4 2530 586 2532 2531
		f 4 -991 986 4378 -4382
		mu 0 4 2533 587 2535 2534
		f 4 -997 -4383 4385 -4389
		mu 0 4 1344 590 2538 2537
		f 4 -998 -4390 4391 -4395
		mu 0 4 2540 591 2542 2541
		f 4 -999 994 4397 -4401
		mu 0 4 2543 592 2545 2544
		f 4 -1005 -4402 4404 -4408
		mu 0 4 1349 595 2548 2547
		f 4 -1006 1001 4409 -4413
		mu 0 4 2550 596 1738 2551
		f 4 -1007 1002 4415 -4419
		mu 0 4 2552 597 2554 2553
		f 4 -1013 -4420 4422 -4426
		mu 0 4 1354 600 2557 2556
		f 4 -1014 1009 4428 -4432
		mu 0 4 2559 601 2561 2560
		f 4 -1015 1010 4434 -4438
		mu 0 4 2562 602 2564 2563
		f 4 -1021 -4439 4441 -4445
		mu 0 4 1359 605 2567 2566
		f 4 -1022 -4446 4447 -4451
		mu 0 4 2569 606 2571 2570
		f 4 -1023 1018 4453 -4457
		mu 0 4 2572 607 2574 2573
		f 4 -1029 -4458 4460 -4464
		mu 0 4 1364 610 2577 2576
		f 4 -1030 -4465 4466 -4470
		mu 0 4 2579 611 2581 2580
		f 4 -1031 1026 4472 -4476
		mu 0 4 2582 612 2584 2583
		f 4 -1037 -4477 4479 -4483
		mu 0 4 1369 615 2587 2586
		f 4 -1038 -4484 4485 -4489
		mu 0 4 2589 616 2591 2590
		f 4 -1039 1034 4491 -4495
		mu 0 4 2592 617 2594 2593
		f 4 -1045 -4496 4498 -4502
		mu 0 4 1374 620 2597 2596
		f 4 -1046 -4503 4504 -4508
		mu 0 4 2599 621 2601 2600
		f 4 -1047 1042 4510 -4514
		mu 0 4 2602 622 2604 2603
		f 4 -1053 -4515 4517 -4521
		mu 0 4 1379 625 2607 2606
		f 4 -1054 1049 4522 -4526
		mu 0 4 2609 626 1828 2610
		f 4 -1055 1050 4528 -4532
		mu 0 4 2611 627 2613 2612
		f 4 -1061 -4533 4535 -4539
		mu 0 4 1384 630 2616 2615
		f 4 -1062 1057 4541 -4545
		mu 0 4 2618 631 2620 2619
		f 4 -1063 1058 4547 -4551
		mu 0 4 2621 632 2623 2622
		f 4 -1069 -4552 4554 -4558
		mu 0 4 1389 635 2626 2625
		f 4 -1070 -4559 4560 -4564
		mu 0 4 2628 636 2630 2629
		f 4 -1071 1066 4566 -4570
		mu 0 4 2631 637 2633 2632
		f 4 -1077 -4571 4573 -4577
		mu 0 4 1394 640 2636 2635
		f 4 -1078 -4578 4579 -4583
		mu 0 4 2638 641 2640 2639
		f 4 -1079 1074 4585 -4589
		mu 0 4 2641 642 2643 2642
		f 4 -1085 -4590 4592 -4596
		mu 0 4 1399 645 2646 2645
		f 4 -1086 -4597 4598 -4602
		mu 0 4 2648 646 2650 2649
		f 4 -1087 1082 4604 -4608
		mu 0 4 2651 647 2653 2652
		f 4 -1093 -4609 4611 -4615
		mu 0 4 1404 650 2656 2655
		f 4 -1094 -4616 4617 -4621
		mu 0 4 2658 651 2660 2659
		f 4 -1095 1090 4623 -4627
		mu 0 4 2661 652 2663 2662
		f 4 -1101 -4628 4630 -4634
		mu 0 4 1409 655 2666 2665
		f 4 -1102 1097 4635 -4639
		mu 0 4 2668 656 1918 2669
		f 4 -1103 1098 4641 -4645
		mu 0 4 2670 657 2672 2671
		f 4 -1109 -4646 4648 -4652
		mu 0 4 1414 660 2675 2674
		f 4 -1110 1105 4654 -4658
		mu 0 4 2677 661 2679 2678
		f 4 -1111 1106 4660 -4664
		mu 0 4 2680 662 2682 2681
		f 4 -1117 -4665 4667 -4671
		mu 0 4 1419 665 2685 2684
		f 4 -1118 -4672 4673 -4677
		mu 0 4 2687 666 2689 2688
		f 4 -1119 1114 4679 -4683
		mu 0 4 2690 667 2692 2691
		f 4 -1125 -4684 4686 -4690
		mu 0 4 1424 670 2695 2694
		f 4 -1126 -4691 4692 -4696
		mu 0 4 2697 671 2699 2698
		f 4 -1127 1122 4698 -4702
		mu 0 4 2700 672 2702 2701
		f 4 -1133 -4703 4705 -4709
		mu 0 4 1429 675 2705 2704
		f 4 -1134 -4710 4711 -4715
		mu 0 4 2707 676 2709 2708;
	setAttr ".fc[500:999]"
		f 4 -1135 1130 4717 -4721
		mu 0 4 2710 677 2712 2711
		f 4 -1141 -4722 4724 -4728
		mu 0 4 1434 680 2715 2714
		f 4 -1142 -4729 4730 -4734
		mu 0 4 2717 681 2719 2718
		f 4 -1143 1138 4736 -4740
		mu 0 4 2720 682 2722 2721
		f 4 -1149 -4741 4743 -4747
		mu 0 4 1439 685 2725 2724
		f 4 -1150 1145 4748 -4752
		mu 0 4 2727 686 2008 2728
		f 4 -1151 1146 4754 -4758
		mu 0 4 2729 687 2731 2730
		f 4 -1157 -4759 4761 -4765
		mu 0 4 1444 690 2734 2733
		f 4 -1158 1153 4767 -4771
		mu 0 4 2736 691 2738 2737
		f 4 -1159 1154 4773 -4777
		mu 0 4 2739 692 2741 2740
		f 4 -1165 -4778 4780 -4784
		mu 0 4 1449 695 2744 2743
		f 4 -1166 -4785 4786 -4790
		mu 0 4 2746 696 2748 2747
		f 4 -1167 1162 4792 -4796
		mu 0 4 2749 697 2751 2750
		f 4 -1173 -4797 4799 -4803
		mu 0 4 1454 700 2754 2753
		f 4 -1174 -4804 4805 -4809
		mu 0 4 2756 701 2758 2757
		f 4 -1175 1170 4811 -4815
		mu 0 4 2759 702 2761 2760
		f 4 -1181 -4816 4818 -4822
		mu 0 4 1459 705 2764 2763
		f 4 -1182 -4823 4824 -4828
		mu 0 4 2766 706 2768 2767
		f 4 -1183 1178 4830 -4834
		mu 0 4 2769 707 2771 2770
		f 4 -1189 -4835 4837 -4841
		mu 0 4 1464 710 2774 2773
		f 4 -1190 -4842 4843 -4847
		mu 0 4 2776 711 2778 2777
		f 4 -1191 1186 4849 -4853
		mu 0 4 2779 712 2781 2780
		f 4 -1197 -4854 4856 -4860
		mu 0 4 1469 715 2784 2783
		f 4 -1198 1193 4861 -4865
		mu 0 4 2786 716 2098 2787
		f 4 -1199 1194 4867 -4871
		mu 0 4 2788 717 2790 2789
		f 4 -1205 -4872 4874 -4878
		mu 0 4 1474 720 2793 2792
		f 4 -1206 1201 4880 -4884
		mu 0 4 2795 721 2797 2796
		f 4 -1207 1202 4886 -4890
		mu 0 4 2798 722 2800 2799
		f 4 -1213 -4891 4893 -4897
		mu 0 4 1479 725 2803 2802
		f 4 -1214 -4898 4899 -4903
		mu 0 4 2805 726 2807 2806
		f 4 -1215 1210 4905 -4909
		mu 0 4 2808 727 2810 2809
		f 4 -1221 -4910 4912 -4916
		mu 0 4 1484 730 2813 2812
		f 4 -1222 -4917 4918 -4922
		mu 0 4 2815 731 2817 2816
		f 4 -1223 1218 4924 -4928
		mu 0 4 2818 732 2820 2819
		f 4 -1229 -4929 4931 -4935
		mu 0 4 1489 735 2823 2822
		f 4 -1230 -4936 4937 -4941
		mu 0 4 2825 736 2827 2826
		f 4 -1231 1226 4943 -4947
		mu 0 4 2828 737 2830 2829
		f 4 -1237 -4948 4950 -4954
		mu 0 4 1494 740 2833 2832
		f 4 -1238 -4955 4956 -4960
		mu 0 4 2835 741 2837 2836
		f 4 -1239 1234 4962 -4966
		mu 0 4 2838 742 2840 2839
		f 4 -1245 -4967 4969 -4973
		mu 0 4 1499 745 2843 2842
		f 4 -1246 1241 4974 -4978
		mu 0 4 2845 746 2188 2846
		f 4 -1247 1242 4980 -4984
		mu 0 4 2847 747 2849 2848
		f 4 -1253 -4985 4987 -4991
		mu 0 4 1504 750 2852 2851
		f 4 -1254 1249 4993 -4997
		mu 0 4 2854 751 2856 2855
		f 4 -1255 1250 4999 -5003
		mu 0 4 2857 752 2859 2858
		f 4 -1261 -5004 5006 -5010
		mu 0 4 1509 755 2862 2861
		f 4 -1262 -5011 5012 -5016
		mu 0 4 2864 756 2866 2865
		f 4 -1263 1258 5018 -5022
		mu 0 4 2867 757 2869 2868
		f 4 -1269 -5023 5025 -5029
		mu 0 4 1514 760 2872 2871
		f 4 -1270 -5030 5031 -5035
		mu 0 4 2874 761 2876 2875
		f 4 -1271 1266 5037 -5041
		mu 0 4 2877 762 2879 2878
		f 4 -1277 -5042 5044 -5048
		mu 0 4 1519 765 2882 2881
		f 4 -1278 -5049 5050 -5054
		mu 0 4 2884 766 2886 2885
		f 4 -1279 1274 5056 -5060
		mu 0 4 2887 767 2889 2888
		f 4 -1285 -5061 5063 -5067
		mu 0 4 1524 770 2892 2891
		f 4 -1286 -5068 5069 -5073
		mu 0 4 2894 771 2896 2895
		f 4 -1287 1282 5075 -5079
		mu 0 4 2897 772 2899 2898
		f 4 -1292 -5080 5082 -5086
		mu 0 4 1529 775 2902 2901
		f 4 -7809 7810 7812 -7814
		mu 0 4 4104 4105 4106 4107
		f 4 -1294 1289 5091 -5094
		mu 0 4 2906 777 2908 2907
		f 4 -1300 -5095 5097 -5101
		mu 0 4 1534 780 2911 2910
		f 4 -1301 1296 5103 -5107
		mu 0 4 2913 781 2915 2914
		f 4 -1302 1297 5109 -5113
		mu 0 4 2916 782 2918 2917
		f 4 -1307 -5114 5116 -5120
		mu 0 4 1539 785 2921 2920
		f 4 -1308 -5121 5122 -5126
		mu 0 4 2923 786 2925 2924
		f 4 -1309 1305 5127 -5131
		mu 0 4 2926 787 2928 2927
		f 4 -1313 -5132 5133 -5137
		mu 0 4 1544 789 2930 2929
		f 4 -1314 920 5138 -5142
		mu 0 4 2931 545 1298 2932
		f 4 -1315 1311 5143 -5147
		mu 0 4 2933 790 2935 2934
		f 4 -1319 1316 5150 -5154
		mu 0 4 1549 792 2937 2936
		f 4 -1320 -3056 5155 -5159
		mu 0 4 2939 793 1743 2940
		f 4 -1321 859 5159 -5163
		mu 0 4 2941 508 2384 2942
		f 4 -1326 -5164 5166 -5170
		mu 0 4 1554 795 2944 2943
		f 4 -1327 -5171 5172 -5176
		mu 0 4 2946 796 2948 2947
		f 4 -1328 1324 5177 -5181
		mu 0 4 2949 797 2951 2950
		f 4 -1332 -5182 5183 -5187
		mu 0 4 1559 799 2953 2952
		f 4 -1333 968 5188 -5192
		mu 0 4 2954 575 1328 2955
		f 4 -1334 1330 5193 -5197
		mu 0 4 2956 800 2958 2957
		f 4 -1338 1335 5200 -5204
		mu 0 4 1564 802 2960 2959
		f 4 -1339 -3200 5205 -5209
		mu 0 4 2962 803 1833 2963
		f 4 -1340 867 5209 -5213
		mu 0 4 2964 513 2393 2965
		f 4 -1345 -5214 5216 -5220
		mu 0 4 1569 805 2967 2966
		f 4 -1346 -5221 5222 -5226
		mu 0 4 2969 806 2971 2970
		f 4 -1347 1343 5227 -5231
		mu 0 4 2972 807 2974 2973
		f 4 -1351 -5232 5233 -5237
		mu 0 4 1574 809 2976 2975
		f 4 -1352 1016 5238 -5242
		mu 0 4 2977 605 1358 2978
		f 4 -1353 1349 5243 -5247
		mu 0 4 2979 810 2981 2980
		f 4 -1357 1354 5250 -5254
		mu 0 4 1579 812 2983 2982
		f 4 -1358 -3344 5255 -5259
		mu 0 4 2985 813 1923 2986
		f 4 -1359 875 5259 -5263
		mu 0 4 2987 518 2402 2988
		f 4 -1364 -5264 5266 -5270
		mu 0 4 1584 815 2990 2989
		f 4 -1365 -5271 5272 -5276
		mu 0 4 2992 816 2994 2993
		f 4 -1366 1362 5277 -5281
		mu 0 4 2995 817 2997 2996
		f 4 -1370 -5282 5283 -5287
		mu 0 4 1589 819 2999 2998
		f 4 -1371 1064 5288 -5292
		mu 0 4 3000 635 1388 3001
		f 4 -1372 1368 5293 -5297
		mu 0 4 3002 820 3004 3003
		f 4 -1376 1373 5300 -5304
		mu 0 4 1594 822 3006 3005
		f 4 -1377 -3488 5305 -5309
		mu 0 4 3008 823 2013 3009
		f 4 -1378 883 5309 -5313
		mu 0 4 3010 523 2411 3011
		f 4 -1383 -5314 5316 -5320
		mu 0 4 1599 825 3013 3012
		f 4 -1384 -5321 5322 -5326
		mu 0 4 3015 826 3017 3016
		f 4 -1385 1381 5327 -5331
		mu 0 4 3018 827 3020 3019
		f 4 -1389 -5332 5333 -5337
		mu 0 4 1604 829 3022 3021
		f 4 -1390 1112 5338 -5342
		mu 0 4 3023 665 1418 3024
		f 4 -1391 1387 5343 -5347
		mu 0 4 3025 830 3027 3026
		f 4 -1395 1392 5350 -5354
		mu 0 4 1609 832 3029 3028
		f 4 -1396 -3632 5355 -5359
		mu 0 4 3031 833 2103 3032
		f 4 -1397 891 5359 -5363
		mu 0 4 3033 528 2420 3034
		f 4 -1402 -5364 5366 -5370
		mu 0 4 1614 835 3036 3035
		f 4 -1403 -5371 5372 -5376
		mu 0 4 3038 836 3040 3039
		f 4 -1404 1400 5377 -5381
		mu 0 4 3041 837 3043 3042
		f 4 -1408 -5382 5383 -5387
		mu 0 4 1619 839 3045 3044
		f 4 -1409 1160 5388 -5392
		mu 0 4 3046 695 1448 3047
		f 4 -1410 1406 5393 -5397
		mu 0 4 3048 840 3050 3049
		f 4 -1414 1411 5400 -5404
		mu 0 4 1624 842 3052 3051
		f 4 -1415 -3776 5405 -5409
		mu 0 4 3054 843 2193 3055
		f 4 -1416 899 5409 -5413
		mu 0 4 3056 533 2429 3057
		f 4 -1421 -5414 5416 -5420
		mu 0 4 1629 845 3059 3058
		f 4 -1422 -5421 5422 -5426
		mu 0 4 3061 846 3063 3062
		f 4 -1423 1419 5427 -5431
		mu 0 4 3064 847 3066 3065
		f 4 -1427 -5432 5433 -5437
		mu 0 4 1634 849 3068 3067
		f 4 -1428 1208 5438 -5442
		mu 0 4 3069 725 1478 3070
		f 4 -1429 1425 5443 -5447
		mu 0 4 3071 850 3073 3072
		f 4 -1433 1430 5450 -5454
		mu 0 4 1639 852 3075 3074
		f 4 -1434 -3918 5455 -5459
		mu 0 4 3077 853 2283 3078
		f 4 -1435 907 5459 -5463
		mu 0 4 3079 538 2438 3080
		f 4 -1440 -5464 5466 -5470
		mu 0 4 1644 855 3082 3081
		f 4 -1441 -5471 5472 -5476
		mu 0 4 3084 856 3086 3085
		f 4 -1442 1438 5477 -5481
		mu 0 4 3087 857 3089 3088
		f 4 -1446 -5482 5483 -5487
		mu 0 4 1649 859 3091 3090
		f 4 -1447 1256 5488 -5492
		mu 0 4 3092 755 1508 3093
		f 4 -1448 1444 5493 -5497
		mu 0 4 3094 860 3096 3095
		f 4 -1452 1449 5500 -5504
		mu 0 4 1654 862 3098 3097
		f 4 -1453 -4062 5505 -5509
		mu 0 4 3100 863 2373 3101
		f 4 -1454 915 5509 -5513
		mu 0 4 3102 543 2447 3103
		f 4 -1459 -5514 5516 -5520
		mu 0 4 1659 865 3105 3104
		f 4 -1460 -5521 5522 -5526
		mu 0 4 3107 866 3109 3108
		f 4 -1461 1457 5527 -5531
		mu 0 4 3110 867 3112 3111
		f 4 -1465 -5532 5533 -5537
		mu 0 4 1664 869 3114 3113
		f 4 -1466 928 5538 -5542
		mu 0 4 3115 550 1303 3116
		f 4 -1467 1463 5543 -5547
		mu 0 4 3117 870 3119 3118
		f 4 -1470 1468 5549 -5553
		mu 0 4 1669 872 3121 3120
		f 4 -1471 -2744 5554 -5558
		mu 0 4 3122 792 1548 3123
		f 4 -1472 923 5558 -5562
		mu 0 4 3124 548 2457 3125
		f 4 -1477 -5563 5565 -5569
		mu 0 4 1674 874 3127 3126
		f 4 -1478 -5570 5571 -5575
		mu 0 4 3129 875 3131 3130
		f 4 -1479 1475 5576 -5580
		mu 0 4 3132 876 3134 3133
		f 4 -1483 -5581 5582 -5586
		mu 0 4 1679 878 3136 3135
		f 4 -1484 936 5587 -5591
		mu 0 4 3137 555 1308 3138
		f 4 -1485 1481 5592 -5596
		mu 0 4 3139 879 3141 3140
		f 4 -1488 1486 5598 -5602
		mu 0 4 1684 881 3143 3142
		f 4 -1489 -2936 5603 -5607
		mu 0 4 3144 872 1668 3145
		f 4 -1490 931 5607 -5611
		mu 0 4 3146 553 2467 3147
		f 4 -1495 -5612 5614 -5618
		mu 0 4 1689 883 3149 3148
		f 4 -1496 -5619 5620 -5624
		mu 0 4 3151 884 3153 3152
		f 4 -1497 1493 5625 -5629
		mu 0 4 3154 885 3156 3155
		f 4 -1501 -5630 5631 -5635
		mu 0 4 1694 887 3158 3157
		f 4 -1502 944 5636 -5640
		mu 0 4 3159 560 1313 3160
		f 4 -1503 1499 5641 -5645
		mu 0 4 3161 888 3163 3162
		f 4 -1506 1504 5647 -5651
		mu 0 4 1699 890 3165 3164
		f 4 -1507 -2960 5652 -5656
		mu 0 4 3166 881 1683 3167
		f 4 -1508 939 5656 -5660
		mu 0 4 3168 558 2477 3169
		f 4 -1512 -5661 5662 -5666
		mu 0 4 1704 892 3171 3170
		f 4 -1513 1436 5667 -5671
		mu 0 4 3172 855 1643 3173
		f 4 -1514 1510 5672 -5676
		mu 0 4 3174 893 3176 3175
		f 4 -1517 913 5677 -5681
		mu 0 4 1709 541 2441 3177
		f 4 -1518 952 5682 -5686
		mu 0 4 3178 565 1318 3179
		f 4 -1519 1515 5687 -5691
		mu 0 4 3180 895 3182 3181
		f 4 -1522 1520 5693 -5697
		mu 0 4 1714 897 3184 3183
		f 4 -1523 -2984 5698 -5702
		mu 0 4 3185 890 1698 3186
		f 4 -1524 947 5702 -5706
		mu 0 4 3187 563 2487 3188
		f 4 -1529 1525 5709 -5713
		mu 0 4 1719 899 3190 3189
		f 4 -1530 1526 5715 -5719
		mu 0 4 3192 900 3194 3193
		f 4 -1531 1527 5720 -5724
		mu 0 4 3195 901 3197 3196
		f 4 -1535 1532 5726 -5730
		mu 0 4 1724 903 3199 3198
		f 4 -1536 960 5731 -5735
		mu 0 4 3200 570 1323 3201
		f 4 -1537 1533 5736 -5740
		mu 0 4 3202 904 3204 3203
		f 4 -1540 1538 5742 -5746
		mu 0 4 1729 906 3206 3205
		f 4 -1541 -3008 5747 -5751
		mu 0 4 3207 897 1713 3208
		f 4 -1542 955 5751 -5755
		mu 0 4 3209 568 2496 3210
		f 4 -1547 1543 5758 -5762
		mu 0 4 1734 908 3212 3211
		f 4 -1548 -3160 5763 -5767
		mu 0 4 3214 909 1808 3215
		f 4 -1549 1545 5768 -5772
		mu 0 4 3216 910 3218 3217
		f 4 -1552 -4403 5773 -5777
		mu 0 4 1739 596 2549 3219
		f 4 -1553 856 5778 -5782
		mu 0 4 3220 505 1258 3221
		f 4 -1554 1550 5783 -5787
		mu 0 4 3222 912 3224 3223
		f 4 -1556 1317 5788 -5792
		mu 0 4 1744 793 2938 3225
		f 4 -1557 -3032 5793 -5797
		mu 0 4 3226 906 1728 3227
		f 4 -1558 963 5797 -5801
		mu 0 4 3228 573 2506 3229
		f 4 -1563 -5802 5804 -5808
		mu 0 4 1749 915 3231 3230
		f 4 -1564 -5809 5810 -5814
		mu 0 4 3233 916 3235 3234
		f 4 -1565 1561 5815 -5819
		mu 0 4 3236 917 3238 3237
		f 4 -1569 -5820 5821 -5825
		mu 0 4 1754 919 3240 3239
		f 4 -1570 976 5826 -5830
		mu 0 4 3241 580 1333 3242
		f 4 -1571 1567 5831 -5835
		mu 0 4 3243 920 3245 3244
		f 4 -1574 1572 5837 -5841
		mu 0 4 1759 922 3247 3246
		f 4 -1575 -2768 5842 -5846
		mu 0 4 3248 802 1563 3249
		f 4 -1576 971 5846 -5850
		mu 0 4 3250 578 2516 3251
		f 4 -1581 -5851 5853 -5857
		mu 0 4 1764 924 3253 3252
		f 4 -1582 -5858 5859 -5863
		mu 0 4 3255 925 3257 3256
		f 4 -1583 1579 5864 -5868
		mu 0 4 3258 926 3260 3259
		f 4 -1587 -5869 5870 -5874
		mu 0 4 1769 928 3262 3261
		f 4 -1588 984 5875 -5879
		mu 0 4 3263 585 1338 3264
		f 4 -1589 1585 5880 -5884
		mu 0 4 3265 929 3267 3266
		f 4 -1592 1590 5886 -5890
		mu 0 4 1774 931 3269 3268
		f 4 -1593 -3080 5891 -5895
		mu 0 4 3270 922 1758 3271
		f 4 -1594 979 5895 -5899
		mu 0 4 3272 583 2526 3273
		f 4 -1599 -5900 5902 -5906
		mu 0 4 1779 933 3275 3274
		f 4 -1600 -5907 5908 -5912
		mu 0 4 3277 934 3279 3278
		f 4 -1601 1597 5913 -5917
		mu 0 4 3280 935 3282 3281
		f 4 -1605 -5918 5919 -5923
		mu 0 4 1784 937 3284 3283
		f 4 -1606 992 5924 -5928
		mu 0 4 3285 590 1343 3286
		f 4 -1607 1603 5929 -5933
		mu 0 4 3287 938 3289 3288
		f 4 -1610 1608 5935 -5939
		mu 0 4 1789 940 3291 3290
		f 4 -1611 -3104 5940 -5944
		mu 0 4 3292 931 1773 3293
		f 4 -1612 987 5944 -5948
		mu 0 4 3294 588 2536 3295
		f 4 -1616 -5949 5950 -5954
		mu 0 4 1794 942 3297 3296
		f 4 -1617 1303 5955 -5959
		mu 0 4 3298 785 1538 3299
		f 4 -1618 1614 5960 -5964
		mu 0 4 3300 943 3302 3301
		f 4 -1621 857 5965 -5969
		mu 0 4 1799 506 2378 3303
		f 4 -1622 1000 5970 -5974
		mu 0 4 3304 595 1348 3305
		f 4 -1623 1619 5975 -5979
		mu 0 4 3306 945 3308 3307
		f 4 -1626 1624 5981 -5985
		mu 0 4 1804 947 3310 3309
		f 4 -1627 -3128 5986 -5990
		mu 0 4 3311 940 1788 3312
		f 4 -1628 995 5990 -5994
		mu 0 4 3313 593 2546 3314
		f 4 -1632 1544 5996 -6000
		mu 0 4 1809 909 3213 3315
		f 4 -1633 1629 6002 -6006
		mu 0 4 3317 949 3319 3318
		f 4 -1634 1630 6007 -6011
		mu 0 4 3320 950 3322 3321
		f 4 -1638 1635 6013 -6017
		mu 0 4 1814 952 3324 3323
		f 4 -1639 1008 6018 -6022
		mu 0 4 3325 600 1353 3326
		f 4 -1640 1636 6023 -6027
		mu 0 4 3327 953 3329 3328
		f 4 -1643 1641 6029 -6033
		mu 0 4 1819 955 3331 3330
		f 4 -1644 -3152 6034 -6038
		mu 0 4 3332 947 1803 3333
		f 4 -1645 1003 6038 -6042
		mu 0 4 3334 598 2555 3335
		f 4 -1650 1646 6045 -6049
		mu 0 4 1824 957 3337 3336
		f 4 -1651 -3304 6050 -6054
		mu 0 4 3339 958 1898 3340
		f 4 -1652 1648 6055 -6059
		mu 0 4 3341 959 3343 3342
		f 4 -1655 -4516 6060 -6064
		mu 0 4 1829 626 2608 3344
		f 4 -1656 864 6065 -6069
		mu 0 4 3345 510 1263 3346
		f 4 -1657 1653 6070 -6074
		mu 0 4 3347 961 3349 3348
		f 4 -1659 1336 6075 -6079
		mu 0 4 1834 803 2961 3350
		f 4 -1660 -3176 6080 -6084
		mu 0 4 3351 955 1818 3352
		f 4 -1661 1011 6084 -6088
		mu 0 4 3353 603 2565 3354
		f 4 -1666 -6089 6091 -6095
		mu 0 4 1839 964 3356 3355
		f 4 -1667 -6096 6097 -6101
		mu 0 4 3358 965 3360 3359
		f 4 -1668 1664 6102 -6106
		mu 0 4 3361 966 3363 3362
		f 4 -1672 -6107 6108 -6112
		mu 0 4 1844 968 3365 3364
		f 4 -1673 1024 6113 -6117
		mu 0 4 3366 610 1363 3367
		f 4 -1674 1670 6118 -6122
		mu 0 4 3368 969 3370 3369
		f 4 -1677 1675 6124 -6128
		mu 0 4 1849 971 3372 3371
		f 4 -1678 -2792 6129 -6133
		mu 0 4 3373 812 1578 3374
		f 4 -1679 1019 6133 -6137
		mu 0 4 3375 608 2575 3376
		f 4 -1684 -6138 6140 -6144
		mu 0 4 1854 973 3378 3377
		f 4 -1685 -6145 6146 -6150
		mu 0 4 3380 974 3382 3381
		f 4 -1686 1682 6151 -6155
		mu 0 4 3383 975 3385 3384
		f 4 -1690 -6156 6157 -6161
		mu 0 4 1859 977 3387 3386
		f 4 -1691 1032 6162 -6166
		mu 0 4 3388 615 1368 3389
		f 4 -1692 1688 6167 -6171
		mu 0 4 3390 978 3392 3391
		f 4 -1695 1693 6173 -6177
		mu 0 4 1864 980 3394 3393
		f 4 -1696 -3224 6178 -6182
		mu 0 4 3395 971 1848 3396
		f 4 -1697 1027 6182 -6186
		mu 0 4 3397 613 2585 3398
		f 4 -1702 -6187 6189 -6193
		mu 0 4 1869 982 3400 3399
		f 4 -1703 -6194 6195 -6199
		mu 0 4 3402 983 3404 3403
		f 4 -1704 1700 6200 -6204
		mu 0 4 3405 984 3407 3406
		f 4 -1708 -6205 6206 -6210
		mu 0 4 1874 986 3409 3408
		f 4 -1709 1040 6211 -6215
		mu 0 4 3410 620 1373 3411
		f 4 -1710 1706 6216 -6220
		mu 0 4 3412 987 3414 3413
		f 4 -1713 1711 6222 -6226
		mu 0 4 1879 989 3416 3415
		f 4 -1714 -3248 6227 -6231
		mu 0 4 3417 980 1863 3418
		f 4 -1715 1035 6231 -6235
		mu 0 4 3419 618 2595 3420
		f 4 -1719 -6236 6237 -6241
		mu 0 4 1884 991 3422 3421
		f 4 -1720 1322 6242 -6246
		mu 0 4 3423 795 1553 3424
		f 4 -1721 1717 6247 -6251
		mu 0 4 3425 992 3427 3426
		f 4 -1724 865 6252 -6256
		mu 0 4 1889 511 2387 3428
		f 4 -1725 1048 6257 -6261
		mu 0 4 3429 625 1378 3430
		f 4 -1726 1722 6262 -6266
		mu 0 4 3431 994 3433 3432
		f 4 -1729 1727 6268 -6272
		mu 0 4 1894 996 3435 3434
		f 4 -1730 -3272 6273 -6277
		mu 0 4 3436 989 1878 3437
		f 4 -1731 1043 6277 -6281
		mu 0 4 3438 623 2605 3439
		f 4 -1735 1647 6283 -6287
		mu 0 4 1899 958 3338 3440
		f 4 -1736 1732 6289 -6293
		mu 0 4 3442 998 3444 3443
		f 4 -1737 1733 6294 -6298
		mu 0 4 3445 999 3447 3446
		f 4 -1741 1738 6300 -6304
		mu 0 4 1904 1001 3449 3448
		f 4 -1742 1056 6305 -6309
		mu 0 4 3450 630 1383 3451
		f 4 -1743 1739 6310 -6314
		mu 0 4 3452 1002 3454 3453
		f 4 -1746 1744 6316 -6320
		mu 0 4 1909 1004 3456 3455
		f 4 -1747 -3296 6321 -6325
		mu 0 4 3457 996 1893 3458
		f 4 -1748 1051 6325 -6329
		mu 0 4 3459 628 2614 3460
		f 4 -1753 1749 6332 -6336
		mu 0 4 1914 1006 3462 3461
		f 4 -1754 -3448 6337 -6341
		mu 0 4 3464 1007 1988 3465
		f 4 -1755 1751 6342 -6346
		mu 0 4 3466 1008 3468 3467
		f 4 -1758 -4629 6347 -6351
		mu 0 4 1919 656 2667 3469
		f 4 -1759 872 6352 -6356
		mu 0 4 3470 515 1268 3471
		f 4 -1760 1756 6357 -6361
		mu 0 4 3472 1010 3474 3473
		f 4 -1762 1355 6362 -6366
		mu 0 4 1924 813 2984 3475
		f 4 -1763 -3320 6367 -6371
		mu 0 4 3476 1004 1908 3477
		f 4 -1764 1059 6371 -6375
		mu 0 4 3478 633 2624 3479
		f 4 -1769 -6376 6378 -6382
		mu 0 4 1929 1013 3481 3480
		f 4 -1770 -6383 6384 -6388
		mu 0 4 3483 1014 3485 3484
		f 4 -1771 1767 6389 -6393
		mu 0 4 3486 1015 3488 3487
		f 4 -1775 -6394 6395 -6399
		mu 0 4 1934 1017 3490 3489
		f 4 -1776 1072 6400 -6404
		mu 0 4 3491 640 1393 3492
		f 4 -1777 1773 6405 -6409
		mu 0 4 3493 1018 3495 3494
		f 4 -1780 1778 6411 -6415
		mu 0 4 1939 1020 3497 3496
		f 4 -1781 -2816 6416 -6420
		mu 0 4 3498 822 1593 3499
		f 4 -1782 1067 6420 -6424
		mu 0 4 3500 638 2634 3501
		f 4 -1787 -6425 6427 -6431
		mu 0 4 1944 1022 3503 3502
		f 4 -1788 -6432 6433 -6437
		mu 0 4 3505 1023 3507 3506
		f 4 -1789 1785 6438 -6442
		mu 0 4 3508 1024 3510 3509
		f 4 -1793 -6443 6444 -6448
		mu 0 4 1949 1026 3512 3511
		f 4 -1794 1080 6449 -6453
		mu 0 4 3513 645 1398 3514
		f 4 -1795 1791 6454 -6458
		mu 0 4 3515 1027 3517 3516
		f 4 -1798 1796 6460 -6464
		mu 0 4 1954 1029 3519 3518
		f 4 -1799 -3368 6465 -6469
		mu 0 4 3520 1020 1938 3521
		f 4 -1800 1075 6469 -6473
		mu 0 4 3522 643 2644 3523
		f 4 -1805 -6474 6476 -6480
		mu 0 4 1959 1031 3525 3524
		f 4 -1806 -6481 6482 -6486
		mu 0 4 3527 1032 3529 3528
		f 4 -1807 1803 6487 -6491
		mu 0 4 3530 1033 3532 3531
		f 4 -1811 -6492 6493 -6497
		mu 0 4 1964 1035 3534 3533
		f 4 -1812 1088 6498 -6502
		mu 0 4 3535 650 1403 3536
		f 4 -1813 1809 6503 -6507
		mu 0 4 3537 1036 3539 3538
		f 4 -1816 1814 6509 -6513
		mu 0 4 1969 1038 3541 3540
		f 4 -1817 -3392 6514 -6518
		mu 0 4 3542 1029 1953 3543
		f 4 -1818 1083 6518 -6522
		mu 0 4 3544 648 2654 3545
		f 4 -1822 -6523 6524 -6528
		mu 0 4 1974 1040 3547 3546
		f 4 -1823 1341 6529 -6533
		mu 0 4 3548 805 1568 3549
		f 4 -1824 1820 6534 -6538
		mu 0 4 3550 1041 3552 3551
		f 4 -1827 873 6539 -6543
		mu 0 4 1979 516 2396 3553
		f 4 -1828 1096 6544 -6548
		mu 0 4 3554 655 1408 3555
		f 4 -1829 1825 6549 -6553
		mu 0 4 3556 1043 3558 3557
		f 4 -1832 1830 6555 -6559
		mu 0 4 1984 1045 3560 3559
		f 4 -1833 -3416 6560 -6564
		mu 0 4 3561 1038 1968 3562
		f 4 -1834 1091 6564 -6568
		mu 0 4 3563 653 2664 3564
		f 4 -1838 1750 6570 -6574
		mu 0 4 1989 1007 3463 3565
		f 4 -1839 1835 6576 -6580
		mu 0 4 3567 1047 3569 3568
		f 4 -1840 1836 6581 -6585
		mu 0 4 3570 1048 3572 3571
		f 4 -1844 1841 6587 -6591
		mu 0 4 1994 1050 3574 3573
		f 4 -1845 1104 6592 -6596
		mu 0 4 3575 660 1413 3576
		f 4 -1846 1842 6597 -6601
		mu 0 4 3577 1051 3579 3578
		f 4 -1849 1847 6603 -6607
		mu 0 4 1999 1053 3581 3580
		f 4 -1850 -3440 6608 -6612
		mu 0 4 3582 1045 1983 3583
		f 4 -1851 1099 6612 -6616
		mu 0 4 3584 658 2673 3585
		f 4 -1856 1852 6619 -6623
		mu 0 4 2004 1055 3587 3586
		f 4 -1857 -3592 6624 -6628
		mu 0 4 3589 1056 2078 3590
		f 4 -1858 1854 6629 -6633
		mu 0 4 3591 1057 3593 3592
		f 4 -1861 -4742 6634 -6638
		mu 0 4 2009 686 2726 3594
		f 4 -1862 880 6639 -6643
		mu 0 4 3595 520 1273 3596
		f 4 -1863 1859 6644 -6648
		mu 0 4 3597 1059 3599 3598
		f 4 -1865 1374 6649 -6653
		mu 0 4 2014 823 3007 3600
		f 4 -1866 -3464 6654 -6658
		mu 0 4 3601 1053 1998 3602
		f 4 -1867 1107 6658 -6662
		mu 0 4 3603 663 2683 3604
		f 4 -1872 -6663 6665 -6669
		mu 0 4 2019 1062 3606 3605
		f 4 -1873 -6670 6671 -6675
		mu 0 4 3608 1063 3610 3609
		f 4 -1874 1870 6676 -6680
		mu 0 4 3611 1064 3613 3612
		f 4 -1878 -6681 6682 -6686
		mu 0 4 2024 1066 3615 3614
		f 4 -1879 1120 6687 -6691
		mu 0 4 3616 670 1423 3617
		f 4 -1880 1876 6692 -6696
		mu 0 4 3618 1067 3620 3619
		f 4 -1883 1881 6698 -6702
		mu 0 4 2029 1069 3622 3621
		f 4 -1884 -2840 6703 -6707
		mu 0 4 3623 832 1608 3624
		f 4 -1885 1115 6707 -6711
		mu 0 4 3625 668 2693 3626
		f 4 -1890 -6712 6714 -6718
		mu 0 4 2034 1071 3628 3627
		f 4 -1891 -6719 6720 -6724
		mu 0 4 3630 1072 3632 3631
		f 4 -1892 1888 6725 -6729
		mu 0 4 3633 1073 3635 3634
		f 4 -1896 -6730 6731 -6735
		mu 0 4 2039 1075 3637 3636
		f 4 -1897 1128 6736 -6740
		mu 0 4 3638 675 1428 3639
		f 4 -1898 1894 6741 -6745
		mu 0 4 3640 1076 3642 3641
		f 4 -1901 1899 6747 -6751
		mu 0 4 2044 1078 3644 3643
		f 4 -1902 -3512 6752 -6756
		mu 0 4 3645 1069 2028 3646
		f 4 -1903 1123 6756 -6760
		mu 0 4 3647 673 2703 3648
		f 4 -1908 -6761 6763 -6767
		mu 0 4 2049 1080 3650 3649
		f 4 -1909 -6768 6769 -6773
		mu 0 4 3652 1081 3654 3653
		f 4 -1910 1906 6774 -6778
		mu 0 4 3655 1082 3657 3656
		f 4 -1914 -6779 6780 -6784
		mu 0 4 2054 1084 3659 3658
		f 4 -1915 1136 6785 -6789
		mu 0 4 3660 680 1433 3661
		f 4 -1916 1912 6790 -6794
		mu 0 4 3662 1085 3664 3663
		f 4 -1919 1917 6796 -6800
		mu 0 4 2059 1087 3666 3665
		f 4 -1920 -3536 6801 -6805
		mu 0 4 3667 1078 2043 3668
		f 4 -1921 1131 6805 -6809
		mu 0 4 3669 678 2713 3670
		f 4 -1925 -6810 6811 -6815
		mu 0 4 2064 1089 3672 3671
		f 4 -1926 1360 6816 -6820
		mu 0 4 3673 815 1583 3674
		f 4 -1927 1923 6821 -6825
		mu 0 4 3675 1090 3677 3676
		f 4 -1930 881 6826 -6830
		mu 0 4 2069 521 2405 3678
		f 4 -1931 1144 6831 -6835
		mu 0 4 3679 685 1438 3680
		f 4 -1932 1928 6836 -6840
		mu 0 4 3681 1092 3683 3682
		f 4 -1935 1933 6842 -6846
		mu 0 4 2074 1094 3685 3684
		f 4 -1936 -3560 6847 -6851
		mu 0 4 3686 1087 2058 3687
		f 4 -1937 1139 6851 -6855
		mu 0 4 3688 683 2723 3689
		f 4 -1941 1853 6857 -6861
		mu 0 4 2079 1056 3588 3690
		f 4 -1942 1938 6863 -6867
		mu 0 4 3692 1096 3694 3693
		f 4 -1943 1939 6868 -6872
		mu 0 4 3695 1097 3697 3696
		f 4 -1947 1944 6874 -6878
		mu 0 4 2084 1099 3699 3698
		f 4 -1948 1152 6879 -6883
		mu 0 4 3700 690 1443 3701
		f 4 -1949 1945 6884 -6888
		mu 0 4 3702 1100 3704 3703
		f 4 -1952 1950 6890 -6894
		mu 0 4 2089 1102 3706 3705
		f 4 -1953 -3584 6895 -6899
		mu 0 4 3707 1094 2073 3708
		f 4 -1954 1147 6899 -6903
		mu 0 4 3709 688 2732 3710
		f 4 -1959 1955 6906 -6910
		mu 0 4 2094 1104 3712 3711
		f 4 -1960 -3736 6911 -6915
		mu 0 4 3714 1105 2168 3715
		f 4 -1961 1957 6916 -6920
		mu 0 4 3716 1106 3718 3717
		f 4 -1964 -4855 6921 -6925
		mu 0 4 2099 716 2785 3719
		f 4 -1965 888 6926 -6930
		mu 0 4 3720 525 1278 3721
		f 4 -1966 1962 6931 -6935
		mu 0 4 3722 1108 3724 3723
		f 4 -1968 1393 6936 -6940
		mu 0 4 2104 833 3030 3725
		f 4 -1969 -3608 6941 -6945
		mu 0 4 3726 1102 2088 3727
		f 4 -1970 1155 6945 -6949
		mu 0 4 3728 693 2742 3729
		f 4 -1975 -6950 6952 -6956
		mu 0 4 2109 1111 3731 3730
		f 4 -1976 -6957 6958 -6962
		mu 0 4 3733 1112 3735 3734
		f 4 -1977 1973 6963 -6967
		mu 0 4 3736 1113 3738 3737
		f 4 -1981 -6968 6969 -6973
		mu 0 4 2114 1115 3740 3739
		f 4 -1982 1168 6974 -6978
		mu 0 4 3741 700 1453 3742
		f 4 -1983 1979 6979 -6983
		mu 0 4 3743 1116 3745 3744
		f 4 -1986 1984 6985 -6989
		mu 0 4 2119 1118 3747 3746
		f 4 -1987 -2864 6990 -6994
		mu 0 4 3748 842 1623 3749
		f 4 -1988 1163 6994 -6998
		mu 0 4 3750 698 2752 3751
		f 4 -1993 -6999 7001 -7005
		mu 0 4 2124 1120 3753 3752
		f 4 -1994 -7006 7007 -7011
		mu 0 4 3755 1121 3757 3756
		f 4 -1995 1991 7012 -7016
		mu 0 4 3758 1122 3760 3759
		f 4 -1999 -7017 7018 -7022
		mu 0 4 2129 1124 3762 3761
		f 4 -2000 1176 7023 -7027
		mu 0 4 3763 705 1458 3764
		f 4 -2001 1997 7028 -7032
		mu 0 4 3765 1125 3767 3766
		f 4 -2004 2002 7034 -7038
		mu 0 4 2134 1127 3769 3768
		f 4 -2005 -3656 7039 -7043
		mu 0 4 3770 1118 2118 3771
		f 4 -2006 1171 7043 -7047
		mu 0 4 3772 703 2762 3773
		f 4 -2011 -7048 7050 -7054
		mu 0 4 2139 1129 3775 3774
		f 4 -2012 -7055 7056 -7060
		mu 0 4 3777 1130 3779 3778
		f 4 -2013 2009 7061 -7065
		mu 0 4 3780 1131 3782 3781
		f 4 -2017 -7066 7067 -7071
		mu 0 4 2144 1133 3784 3783
		f 4 -2018 1184 7072 -7076
		mu 0 4 3785 710 1463 3786
		f 4 -2019 2015 7077 -7081
		mu 0 4 3787 1134 3789 3788
		f 4 -2022 2020 7083 -7087
		mu 0 4 2149 1136 3791 3790
		f 4 -2023 -3680 7088 -7092
		mu 0 4 3792 1127 2133 3793
		f 4 -2024 1179 7092 -7096
		mu 0 4 3794 708 2772 3795
		f 4 -2028 -7097 7098 -7102
		mu 0 4 2154 1138 3797 3796
		f 4 -2029 1379 7103 -7107
		mu 0 4 3798 825 1598 3799
		f 4 -2030 2026 7108 -7112
		mu 0 4 3800 1139 3802 3801
		f 4 -2033 889 7113 -7117
		mu 0 4 2159 526 2414 3803
		f 4 -2034 1192 7118 -7122
		mu 0 4 3804 715 1468 3805
		f 4 -2035 2031 7123 -7127
		mu 0 4 3806 1141 3808 3807
		f 4 -2038 2036 7129 -7133
		mu 0 4 2164 1143 3810 3809
		f 4 -2039 -3704 7134 -7138
		mu 0 4 3811 1136 2148 3812
		f 4 -2040 1187 7138 -7142
		mu 0 4 3813 713 2782 3814
		f 4 -2044 1956 7144 -7148
		mu 0 4 2169 1105 3713 3815
		f 4 -2045 2041 7150 -7154
		mu 0 4 3817 1145 3819 3818
		f 4 -2046 2042 7155 -7159
		mu 0 4 3820 1146 3822 3821
		f 4 -2050 2047 7161 -7165
		mu 0 4 2174 1148 3824 3823
		f 4 -2051 1200 7166 -7170
		mu 0 4 3825 720 1473 3826
		f 4 -2052 2048 7171 -7175
		mu 0 4 3827 1149 3829 3828
		f 4 -2055 2053 7177 -7181
		mu 0 4 2179 1151 3831 3830
		f 4 -2056 -3728 7182 -7186
		mu 0 4 3832 1143 2163 3833
		f 4 -2057 1195 7186 -7190
		mu 0 4 3834 718 2791 3835
		f 4 -2062 2058 7193 -7197
		mu 0 4 2184 1153 3837 3836
		f 4 -2063 -3880 7198 -7202
		mu 0 4 3839 1154 2258 3840
		f 4 -2064 2060 7203 -7207
		mu 0 4 3841 1155 3843 3842
		f 4 -2067 -4968 7208 -7212
		mu 0 4 2189 746 2844 3844
		f 4 -2068 896 7213 -7217
		mu 0 4 3845 530 1283 3846
		f 4 -2069 2065 7218 -7222
		mu 0 4 3847 1157 3849 3848
		f 4 -2071 1412 7223 -7227
		mu 0 4 2194 843 3053 3850
		f 4 -2072 -3752 7228 -7232
		mu 0 4 3851 1151 2178 3852
		f 4 -2073 1203 7232 -7236
		mu 0 4 3853 723 2801 3854
		f 4 -2078 -7237 7239 -7243
		mu 0 4 2199 1160 3856 3855
		f 4 -2079 -7244 7245 -7249
		mu 0 4 3858 1161 3860 3859
		f 4 -2080 2076 7250 -7254
		mu 0 4 3861 1162 3863 3862
		f 4 -2084 -7255 7256 -7260
		mu 0 4 2204 1164 3865 3864
		f 4 -2085 1216 7261 -7265
		mu 0 4 3866 730 1483 3867
		f 4 -2086 2082 7266 -7270
		mu 0 4 3868 1165 3870 3869
		f 4 -2089 2087 7272 -7276
		mu 0 4 2209 1167 3872 3871
		f 4 -2090 -2888 7277 -7281
		mu 0 4 3873 852 1638 3874
		f 4 -2091 1211 7281 -7285
		mu 0 4 3875 728 2811 3876
		f 4 -2096 -7286 7288 -7292
		mu 0 4 2214 1169 3878 3877
		f 4 -2097 -7293 7294 -7298
		mu 0 4 3880 1170 3882 3881
		f 4 -2098 2094 7299 -7303
		mu 0 4 3883 1171 3885 3884
		f 4 -2102 -7304 7305 -7309
		mu 0 4 2219 1173 3887 3886
		f 4 -2103 1224 7310 -7314
		mu 0 4 3888 735 1488 3889
		f 4 -2104 2100 7315 -7319
		mu 0 4 3890 1174 3892 3891
		f 4 -2107 2105 7321 -7325
		mu 0 4 2224 1176 3894 3893
		f 4 -2108 -3800 7326 -7330
		mu 0 4 3895 1167 2208 3896
		f 4 -2109 1219 7330 -7334
		mu 0 4 3897 733 2821 3898
		f 4 -2114 -7335 7337 -7341
		mu 0 4 2229 1178 3900 3899
		f 4 -2115 -7342 7343 -7347
		mu 0 4 3902 1179 3904 3903
		f 4 -2116 2112 7348 -7352
		mu 0 4 3905 1180 3907 3906
		f 4 -2120 -7353 7354 -7358
		mu 0 4 2234 1182 3909 3908
		f 4 -2121 1232 7359 -7363
		mu 0 4 3910 740 1493 3911
		f 4 -2122 2118 7364 -7368
		mu 0 4 3912 1183 3914 3913
		f 4 -2125 2123 7370 -7374
		mu 0 4 2239 1185 3916 3915
		f 4 -2126 -3824 7375 -7379
		mu 0 4 3917 1176 2223 3918
		f 4 -2127 1227 7379 -7383
		mu 0 4 3919 738 2831 3920
		f 4 -2131 -7384 7385 -7389
		mu 0 4 2244 1187 3922 3921
		f 4 -2132 1398 7390 -7394
		mu 0 4 3923 835 1613 3924
		f 4 -2133 2129 7395 -7399
		mu 0 4 3925 1188 3927 3926
		f 4 -2136 897 7400 -7404
		mu 0 4 2249 531 2423 3928
		f 4 -2137 1240 7405 -7409
		mu 0 4 3929 745 1498 3930
		f 4 -2138 2134 7410 -7414
		mu 0 4 3931 1190 3933 3932
		f 4 -2141 2139 7416 -7420
		mu 0 4 2254 1192 3935 3934
		f 4 -2142 -3848 7421 -7425
		mu 0 4 3936 1185 2238 3937
		f 4 -2143 1235 7425 -7429
		mu 0 4 3938 743 2841 3939
		f 4 -2147 2059 7431 -7435
		mu 0 4 2259 1154 3838 3940
		f 4 -2148 2144 7437 -7441
		mu 0 4 3942 1194 3944 3943
		f 4 -2149 2145 7442 -7446
		mu 0 4 3945 1195 3947 3946
		f 4 -2153 2150 7448 -7452
		mu 0 4 2264 1197 3949 3948;
	setAttr ".fc[1000:1499]"
		f 4 -2154 1248 7453 -7457
		mu 0 4 3950 750 1503 3951
		f 4 -2155 2151 7458 -7462
		mu 0 4 3952 1198 3954 3953
		f 4 -2158 2156 7464 -7468
		mu 0 4 2269 1200 3956 3955
		f 4 -2159 -3872 7469 -7473
		mu 0 4 3957 1192 2253 3958
		f 4 -2160 1243 7473 -7477
		mu 0 4 3959 748 2850 3960
		f 4 -2165 2161 7480 -7484
		mu 0 4 2274 1202 3962 3961
		f 4 -2166 -4022 7485 -7489
		mu 0 4 3964 1203 2348 3965
		f 4 -2167 2163 7490 -7494
		mu 0 4 3966 1204 3968 3967
		f 4 -2169 -5081 7495 -7499
		mu 0 4 2279 776 2903 3969
		f 4 -2170 904 7500 -7504
		mu 0 4 3970 535 1288 3971
		f 4 -7817 7818 7820 -7822
		mu 0 4 4108 4109 4110 4111
		f 4 -2173 1431 7509 -7513
		mu 0 4 2284 853 3076 3975
		f 4 -2174 -3896 7513 -7517
		mu 0 4 3976 1200 2268 3977
		f 4 -2175 1251 7517 -7520
		mu 0 4 3978 753 2860 3979
		f 4 -2180 -7521 7523 -7527
		mu 0 4 2289 1209 3981 3980
		f 4 -2181 -7528 7529 -7533
		mu 0 4 3983 1210 3985 3984
		f 4 -2182 2178 7534 -7538
		mu 0 4 3986 1211 3988 3987
		f 4 -2186 -7539 7540 -7544
		mu 0 4 2294 1213 3990 3989
		f 4 -2187 1264 7545 -7549
		mu 0 4 3991 760 1513 3992
		f 4 -2188 2184 7550 -7554
		mu 0 4 3993 1214 3995 3994
		f 4 -2191 2189 7556 -7560
		mu 0 4 2299 1216 3997 3996
		f 4 -2192 -2912 7561 -7565
		mu 0 4 3998 862 1653 3999
		f 4 -2193 1259 7565 -7569
		mu 0 4 4000 758 2870 4001
		f 4 -2198 -7570 7572 -7576
		mu 0 4 2304 1218 4003 4002
		f 4 -2199 -7577 7578 -7582
		mu 0 4 4005 1219 4007 4006
		f 4 -2200 2196 7583 -7587
		mu 0 4 4008 1220 4010 4009
		f 4 -2204 -7588 7589 -7593
		mu 0 4 2309 1222 4012 4011
		f 4 -2205 1272 7594 -7598
		mu 0 4 4013 765 1518 4014
		f 4 -2206 2202 7599 -7603
		mu 0 4 4015 1223 4017 4016
		f 4 -2209 2207 7605 -7609
		mu 0 4 2314 1225 4019 4018
		f 4 -2210 -3942 7610 -7614
		mu 0 4 4020 1216 2298 4021
		f 4 -2211 1267 7614 -7618
		mu 0 4 4022 763 2880 4023
		f 4 -2216 -7619 7621 -7625
		mu 0 4 2319 1227 4025 4024
		f 4 -2217 -7626 7627 -7631
		mu 0 4 4027 1228 4029 4028
		f 4 -2218 2214 7632 -7636
		mu 0 4 4030 1229 4032 4031
		f 4 -2222 -7637 7638 -7642
		mu 0 4 2324 1231 4034 4033
		f 4 -2223 1280 7643 -7647
		mu 0 4 4035 770 1523 4036
		f 4 -2224 2220 7648 -7652
		mu 0 4 4037 1232 4039 4038
		f 4 -2227 2225 7654 -7658
		mu 0 4 2329 1234 4041 4040
		f 4 -2228 -3966 7659 -7663
		mu 0 4 4042 1225 2313 4043
		f 4 -2229 1275 7663 -7667
		mu 0 4 4044 768 2890 4045
		f 4 -2233 -7668 7669 -7673
		mu 0 4 2334 1236 4047 4046
		f 4 -2234 1417 7674 -7678
		mu 0 4 4048 845 1628 4049
		f 4 -2235 2231 7679 -7683
		mu 0 4 4050 1237 4052 4051
		f 4 -2238 905 7684 -7688
		mu 0 4 2339 536 2432 4053
		f 4 -2239 1288 7689 -7693
		mu 0 4 4054 775 1528 4055
		f 4 -2240 2236 7694 -7698
		mu 0 4 4056 1239 4058 4057
		f 4 -2243 2241 7700 -7704
		mu 0 4 2344 1241 4060 4059
		f 4 -2244 -3990 7705 -7709
		mu 0 4 4061 1234 2328 4062
		f 4 -2245 1283 7709 -7713
		mu 0 4 4063 773 2900 4064
		f 4 -2249 2162 7715 -7719
		mu 0 4 2349 1203 3963 4065
		f 4 -2250 2246 7721 -7725
		mu 0 4 4067 1243 4069 4068
		f 4 -2251 2247 7726 -7730
		mu 0 4 4070 1244 4072 4071
		f 4 -2255 2252 7732 -7736
		mu 0 4 2354 1246 4074 4073
		f 4 -2256 1295 7737 -7741
		mu 0 4 4075 780 1533 4076
		f 4 -2257 2253 7742 -7746
		mu 0 4 4077 1247 4079 4078
		f 4 -2260 2258 7748 -7752
		mu 0 4 2359 1249 4081 4080
		f 4 -2261 -4014 7753 -7757
		mu 0 4 4082 1241 2343 4083
		f 4 -2262 1290 7757 -7761
		mu 0 4 4084 778 2909 4085
		f 4 -2266 2263 7763 -7767
		mu 0 4 2364 1251 4087 4086
		f 4 -2267 -3016 7768 -7772
		mu 0 4 4088 899 1718 4089
		f 4 -2268 2264 7773 -7777
		mu 0 4 4090 1252 4092 4091
		f 4 -2271 -4290 7778 -7782
		mu 0 4 2369 566 2490 4093
		f 4 -2272 912 7783 -7787
		mu 0 4 4094 540 1293 4095
		f 4 -2273 2269 7788 -7792
		mu 0 4 4096 1254 4098 4097
		f 4 -2275 1450 7793 -7797
		mu 0 4 2374 863 3099 4099
		f 4 -2276 -4038 7798 -7802
		mu 0 4 4100 1249 2358 4101
		f 4 -2277 1298 7802 -7806
		mu 0 4 4102 783 2919 4103
		f 4 -488 -2280 2283 -2283
		mu 0 4 1257 300 1258 1256
		f 4 -857 860 2284 -2284
		mu 0 4 1258 505 1259 1256
		f 4 2280 -2282 2285 -2285
		mu 0 4 1259 504 1260 1256
		f 4 -496 -2288 2291 -2291
		mu 0 4 1262 305 1263 1261
		f 4 -865 868 2292 -2292
		mu 0 4 1263 510 1264 1261
		f 4 2288 -2290 2293 -2293
		mu 0 4 1264 509 1265 1261
		f 4 -504 -2296 2299 -2299
		mu 0 4 1267 310 1268 1266
		f 4 -873 876 2300 -2300
		mu 0 4 1268 515 1269 1266
		f 4 2296 -2298 2301 -2301
		mu 0 4 1269 514 1270 1266
		f 4 -512 -2304 2307 -2307
		mu 0 4 1272 315 1273 1271
		f 4 -881 884 2308 -2308
		mu 0 4 1273 520 1274 1271
		f 4 2304 -2306 2309 -2309
		mu 0 4 1274 519 1275 1271
		f 4 -520 -2312 2315 -2315
		mu 0 4 1277 320 1278 1276
		f 4 -889 892 2316 -2316
		mu 0 4 1278 525 1279 1276
		f 4 2312 -2314 2317 -2317
		mu 0 4 1279 524 1280 1276
		f 4 -528 -2320 2323 -2323
		mu 0 4 1282 325 1283 1281
		f 4 -897 900 2324 -2324
		mu 0 4 1283 530 1284 1281
		f 4 2320 -2322 2325 -2325
		mu 0 4 1284 529 1285 1281
		f 4 -536 -2328 2331 -2331
		mu 0 4 1287 330 1288 1286
		f 4 -905 908 2332 -2332
		mu 0 4 1288 535 1289 1286
		f 4 2328 -2330 2333 -2333
		mu 0 4 1289 534 1290 1286
		f 4 -544 -2336 2339 -2339
		mu 0 4 1292 335 1293 1291
		f 4 -913 916 2340 -2340
		mu 0 4 1293 540 1294 1291
		f 4 2336 -2338 2341 -2341
		mu 0 4 1294 539 1295 1291
		f 4 -551 -2344 2347 -2347
		mu 0 4 1297 299 1298 1296
		f 4 -921 924 2348 -2348
		mu 0 4 1298 545 1299 1296
		f 4 2344 -2346 2349 -2349
		mu 0 4 1299 544 1300 1296
		f 4 -558 -2352 2355 -2355
		mu 0 4 1302 339 1303 1301
		f 4 -929 932 2356 -2356
		mu 0 4 1303 550 1304 1301
		f 4 2352 -2354 2357 -2357
		mu 0 4 1304 549 1305 1301
		f 4 -565 -2360 2363 -2363
		mu 0 4 1307 343 1308 1306
		f 4 -937 940 2364 -2364
		mu 0 4 1308 555 1309 1306
		f 4 2360 -2362 2365 -2365
		mu 0 4 1309 554 1310 1306
		f 4 -571 -2368 2371 -2371
		mu 0 4 1312 347 1313 1311
		f 4 -945 948 2372 -2372
		mu 0 4 1313 560 1314 1311
		f 4 2368 -2370 2373 -2373
		mu 0 4 1314 559 1315 1311
		f 4 -578 -2376 2379 -2379
		mu 0 4 1317 350 1318 1316
		f 4 -953 956 2380 -2380
		mu 0 4 1318 565 1319 1316
		f 4 2376 -2378 2381 -2381
		mu 0 4 1319 564 1320 1316
		f 4 -584 -2384 2387 -2387
		mu 0 4 1322 354 1323 1321
		f 4 -961 964 2388 -2388
		mu 0 4 1323 570 1324 1321
		f 4 2384 -2386 2389 -2389
		mu 0 4 1324 569 1325 1321
		f 4 -591 -2392 2395 -2395
		mu 0 4 1327 304 1328 1326
		f 4 -969 972 2396 -2396
		mu 0 4 1328 575 1329 1326
		f 4 2392 -2394 2397 -2397
		mu 0 4 1329 574 1330 1326
		f 4 -598 -2400 2403 -2403
		mu 0 4 1332 361 1333 1331
		f 4 -977 980 2404 -2404
		mu 0 4 1333 580 1334 1331
		f 4 2400 -2402 2405 -2405
		mu 0 4 1334 579 1335 1331
		f 4 -605 -2408 2411 -2411
		mu 0 4 1337 365 1338 1336
		f 4 -985 988 2412 -2412
		mu 0 4 1338 585 1339 1336
		f 4 2408 -2410 2413 -2413
		mu 0 4 1339 584 1340 1336
		f 4 -611 -2416 2419 -2419
		mu 0 4 1342 369 1343 1341
		f 4 -993 996 2420 -2420
		mu 0 4 1343 590 1344 1341
		f 4 2416 -2418 2421 -2421
		mu 0 4 1344 589 1345 1341
		f 4 -617 -2424 2427 -2427
		mu 0 4 1347 372 1348 1346
		f 4 -1001 1004 2428 -2428
		mu 0 4 1348 595 1349 1346
		f 4 2424 -2426 2429 -2429
		mu 0 4 1349 594 1350 1346
		f 4 -623 -2432 2435 -2435
		mu 0 4 1352 375 1353 1351
		f 4 -1009 1012 2436 -2436
		mu 0 4 1353 600 1354 1351
		f 4 2432 -2434 2437 -2437
		mu 0 4 1354 599 1355 1351
		f 4 -630 -2440 2443 -2443
		mu 0 4 1357 309 1358 1356
		f 4 -1017 1020 2444 -2444
		mu 0 4 1358 605 1359 1356
		f 4 2440 -2442 2445 -2445
		mu 0 4 1359 604 1360 1356
		f 4 -637 -2448 2451 -2451
		mu 0 4 1362 382 1363 1361
		f 4 -1025 1028 2452 -2452
		mu 0 4 1363 610 1364 1361
		f 4 2448 -2450 2453 -2453
		mu 0 4 1364 609 1365 1361
		f 4 -644 -2456 2459 -2459
		mu 0 4 1367 386 1368 1366
		f 4 -1033 1036 2460 -2460
		mu 0 4 1368 615 1369 1366
		f 4 2456 -2458 2461 -2461
		mu 0 4 1369 614 1370 1366
		f 4 -650 -2464 2467 -2467
		mu 0 4 1372 390 1373 1371
		f 4 -1041 1044 2468 -2468
		mu 0 4 1373 620 1374 1371
		f 4 2464 -2466 2469 -2469
		mu 0 4 1374 619 1375 1371
		f 4 -656 -2472 2475 -2475
		mu 0 4 1377 393 1378 1376
		f 4 -1049 1052 2476 -2476
		mu 0 4 1378 625 1379 1376
		f 4 2472 -2474 2477 -2477
		mu 0 4 1379 624 1380 1376
		f 4 -662 -2480 2483 -2483
		mu 0 4 1382 396 1383 1381
		f 4 -1057 1060 2484 -2484
		mu 0 4 1383 630 1384 1381
		f 4 2480 -2482 2485 -2485
		mu 0 4 1384 629 1385 1381
		f 4 -669 -2488 2491 -2491
		mu 0 4 1387 314 1388 1386
		f 4 -1065 1068 2492 -2492
		mu 0 4 1388 635 1389 1386
		f 4 2488 -2490 2493 -2493
		mu 0 4 1389 634 1390 1386
		f 4 -676 -2496 2499 -2499
		mu 0 4 1392 403 1393 1391
		f 4 -1073 1076 2500 -2500
		mu 0 4 1393 640 1394 1391
		f 4 2496 -2498 2501 -2501
		mu 0 4 1394 639 1395 1391
		f 4 -683 -2504 2507 -2507
		mu 0 4 1397 407 1398 1396
		f 4 -1081 1084 2508 -2508
		mu 0 4 1398 645 1399 1396
		f 4 2504 -2506 2509 -2509
		mu 0 4 1399 644 1400 1396
		f 4 -689 -2512 2515 -2515
		mu 0 4 1402 411 1403 1401
		f 4 -1089 1092 2516 -2516
		mu 0 4 1403 650 1404 1401
		f 4 2512 -2514 2517 -2517
		mu 0 4 1404 649 1405 1401
		f 4 -695 -2520 2523 -2523
		mu 0 4 1407 414 1408 1406
		f 4 -1097 1100 2524 -2524
		mu 0 4 1408 655 1409 1406
		f 4 2520 -2522 2525 -2525
		mu 0 4 1409 654 1410 1406
		f 4 -701 -2528 2531 -2531
		mu 0 4 1412 417 1413 1411
		f 4 -1105 1108 2532 -2532
		mu 0 4 1413 660 1414 1411
		f 4 2528 -2530 2533 -2533
		mu 0 4 1414 659 1415 1411
		f 4 -708 -2536 2539 -2539
		mu 0 4 1417 319 1418 1416
		f 4 -1113 1116 2540 -2540
		mu 0 4 1418 665 1419 1416
		f 4 2536 -2538 2541 -2541
		mu 0 4 1419 664 1420 1416
		f 4 -715 -2544 2547 -2547
		mu 0 4 1422 424 1423 1421
		f 4 -1121 1124 2548 -2548
		mu 0 4 1423 670 1424 1421
		f 4 2544 -2546 2549 -2549
		mu 0 4 1424 669 1425 1421
		f 4 -722 -2552 2555 -2555
		mu 0 4 1427 428 1428 1426
		f 4 -1129 1132 2556 -2556
		mu 0 4 1428 675 1429 1426
		f 4 2552 -2554 2557 -2557
		mu 0 4 1429 674 1430 1426
		f 4 -728 -2560 2563 -2563
		mu 0 4 1432 432 1433 1431
		f 4 -1137 1140 2564 -2564
		mu 0 4 1433 680 1434 1431
		f 4 2560 -2562 2565 -2565
		mu 0 4 1434 679 1435 1431
		f 4 -734 -2568 2571 -2571
		mu 0 4 1437 435 1438 1436
		f 4 -1145 1148 2572 -2572
		mu 0 4 1438 685 1439 1436
		f 4 2568 -2570 2573 -2573
		mu 0 4 1439 684 1440 1436
		f 4 -740 -2576 2579 -2579
		mu 0 4 1442 438 1443 1441
		f 4 -1153 1156 2580 -2580
		mu 0 4 1443 690 1444 1441
		f 4 2576 -2578 2581 -2581
		mu 0 4 1444 689 1445 1441
		f 4 -747 -2584 2587 -2587
		mu 0 4 1447 324 1448 1446
		f 4 -1161 1164 2588 -2588
		mu 0 4 1448 695 1449 1446
		f 4 2584 -2586 2589 -2589
		mu 0 4 1449 694 1450 1446
		f 4 -754 -2592 2595 -2595
		mu 0 4 1452 445 1453 1451
		f 4 -1169 1172 2596 -2596
		mu 0 4 1453 700 1454 1451
		f 4 2592 -2594 2597 -2597
		mu 0 4 1454 699 1455 1451
		f 4 -761 -2600 2603 -2603
		mu 0 4 1457 449 1458 1456
		f 4 -1177 1180 2604 -2604
		mu 0 4 1458 705 1459 1456
		f 4 2600 -2602 2605 -2605
		mu 0 4 1459 704 1460 1456
		f 4 -767 -2608 2611 -2611
		mu 0 4 1462 453 1463 1461
		f 4 -1185 1188 2612 -2612
		mu 0 4 1463 710 1464 1461
		f 4 2608 -2610 2613 -2613
		mu 0 4 1464 709 1465 1461
		f 4 -773 -2616 2619 -2619
		mu 0 4 1467 456 1468 1466
		f 4 -1193 1196 2620 -2620
		mu 0 4 1468 715 1469 1466
		f 4 2616 -2618 2621 -2621
		mu 0 4 1469 714 1470 1466
		f 4 -779 -2624 2627 -2627
		mu 0 4 1472 459 1473 1471
		f 4 -1201 1204 2628 -2628
		mu 0 4 1473 720 1474 1471
		f 4 2624 -2626 2629 -2629
		mu 0 4 1474 719 1475 1471
		f 4 -786 -2632 2635 -2635
		mu 0 4 1477 329 1478 1476
		f 4 -1209 1212 2636 -2636
		mu 0 4 1478 725 1479 1476
		f 4 2632 -2634 2637 -2637
		mu 0 4 1479 724 1480 1476
		f 4 -793 -2640 2643 -2643
		mu 0 4 1482 466 1483 1481
		f 4 -1217 1220 2644 -2644
		mu 0 4 1483 730 1484 1481
		f 4 2640 -2642 2645 -2645
		mu 0 4 1484 729 1485 1481
		f 4 -800 -2648 2651 -2651
		mu 0 4 1487 470 1488 1486
		f 4 -1225 1228 2652 -2652
		mu 0 4 1488 735 1489 1486
		f 4 2648 -2650 2653 -2653
		mu 0 4 1489 734 1490 1486
		f 4 -806 -2656 2659 -2659
		mu 0 4 1492 474 1493 1491
		f 4 -1233 1236 2660 -2660
		mu 0 4 1493 740 1494 1491
		f 4 2656 -2658 2661 -2661
		mu 0 4 1494 739 1495 1491
		f 4 -812 -2664 2667 -2667
		mu 0 4 1497 477 1498 1496
		f 4 -1241 1244 2668 -2668
		mu 0 4 1498 745 1499 1496
		f 4 2664 -2666 2669 -2669
		mu 0 4 1499 744 1500 1496
		f 4 -818 -2672 2675 -2675
		mu 0 4 1502 480 1503 1501
		f 4 -1249 1252 2676 -2676
		mu 0 4 1503 750 1504 1501
		f 4 2672 -2674 2677 -2677
		mu 0 4 1504 749 1505 1501
		f 4 -825 -2680 2683 -2683
		mu 0 4 1507 334 1508 1506
		f 4 -1257 1260 2684 -2684
		mu 0 4 1508 755 1509 1506
		f 4 2680 -2682 2685 -2685
		mu 0 4 1509 754 1510 1506
		f 4 -832 -2688 2691 -2691
		mu 0 4 1512 487 1513 1511
		f 4 -1265 1268 2692 -2692
		mu 0 4 1513 760 1514 1511
		f 4 2688 -2690 2693 -2693
		mu 0 4 1514 759 1515 1511
		f 4 -839 -2696 2699 -2699
		mu 0 4 1517 491 1518 1516
		f 4 -1273 1276 2700 -2700
		mu 0 4 1518 765 1519 1516
		f 4 2696 -2698 2701 -2701
		mu 0 4 1519 764 1520 1516
		f 4 -845 -2704 2707 -2707
		mu 0 4 1522 495 1523 1521
		f 4 -1281 1284 2708 -2708
		mu 0 4 1523 770 1524 1521
		f 4 2704 -2706 2709 -2709
		mu 0 4 1524 769 1525 1521
		f 4 -851 -2712 2715 -2715
		mu 0 4 1527 498 1528 1526
		f 4 -1289 1291 2716 -2716
		mu 0 4 1528 775 1529 1526
		f 4 2712 -2714 2717 -2717
		mu 0 4 1529 774 1530 1526
		f 4 -856 -2720 2723 -2723
		mu 0 4 1532 501 1533 1531
		f 4 -1296 1299 2724 -2724
		mu 0 4 1533 780 1534 1531
		f 4 2720 -2722 2725 -2725
		mu 0 4 1534 779 1535 1531
		f 4 -485 -2728 2731 -2731
		mu 0 4 1537 297 1538 1536
		f 4 -1304 1306 2732 -2732
		mu 0 4 1538 785 1539 1536
		f 4 2728 -2730 2733 -2733
		mu 0 4 1539 784 1540 1536
		f 4 -486 -2736 2739 -2739
		mu 0 4 1542 298 1543 1541
		f 4 -1311 1312 2740 -2740
		mu 0 4 1543 789 1544 1541
		f 4 2736 -2738 2741 -2741
		mu 0 4 1544 788 1545 1541
		f 4 -487 482 2747 -2747
		mu 0 4 1547 299 1548 1546
		f 4 2743 1318 2748 -2748
		mu 0 4 1548 792 1549 1546
		f 4 2744 -2746 2749 -2749
		mu 0 4 1549 791 1550 1546
		f 4 -493 -2752 2755 -2755
		mu 0 4 1552 302 1553 1551
		f 4 -1323 1325 2756 -2756
		mu 0 4 1553 795 1554 1551
		f 4 2752 -2754 2757 -2757
		mu 0 4 1554 794 1555 1551
		f 4 -494 -2760 2763 -2763
		mu 0 4 1557 303 1558 1556
		f 4 -1330 1331 2764 -2764
		mu 0 4 1558 799 1559 1556
		f 4 2760 -2762 2765 -2765
		mu 0 4 1559 798 1560 1556
		f 4 -495 490 2771 -2771
		mu 0 4 1562 304 1563 1561
		f 4 2767 1337 2772 -2772
		mu 0 4 1563 802 1564 1561
		f 4 2768 -2770 2773 -2773
		mu 0 4 1564 801 1565 1561
		f 4 -501 -2776 2779 -2779
		mu 0 4 1567 307 1568 1566
		f 4 -1342 1344 2780 -2780
		mu 0 4 1568 805 1569 1566
		f 4 2776 -2778 2781 -2781
		mu 0 4 1569 804 1570 1566
		f 4 -502 -2784 2787 -2787
		mu 0 4 1572 308 1573 1571
		f 4 -1349 1350 2788 -2788
		mu 0 4 1573 809 1574 1571
		f 4 2784 -2786 2789 -2789
		mu 0 4 1574 808 1575 1571
		f 4 -503 498 2795 -2795
		mu 0 4 1577 309 1578 1576
		f 4 2791 1356 2796 -2796
		mu 0 4 1578 812 1579 1576
		f 4 2792 -2794 2797 -2797
		mu 0 4 1579 811 1580 1576
		f 4 -509 -2800 2803 -2803
		mu 0 4 1582 312 1583 1581
		f 4 -1361 1363 2804 -2804
		mu 0 4 1583 815 1584 1581
		f 4 2800 -2802 2805 -2805
		mu 0 4 1584 814 1585 1581
		f 4 -510 -2808 2811 -2811
		mu 0 4 1587 313 1588 1586
		f 4 -1368 1369 2812 -2812
		mu 0 4 1588 819 1589 1586
		f 4 2808 -2810 2813 -2813
		mu 0 4 1589 818 1590 1586
		f 4 -511 506 2819 -2819
		mu 0 4 1592 314 1593 1591
		f 4 2815 1375 2820 -2820
		mu 0 4 1593 822 1594 1591
		f 4 2816 -2818 2821 -2821
		mu 0 4 1594 821 1595 1591
		f 4 -517 -2824 2827 -2827
		mu 0 4 1597 317 1598 1596
		f 4 -1380 1382 2828 -2828
		mu 0 4 1598 825 1599 1596
		f 4 2824 -2826 2829 -2829
		mu 0 4 1599 824 1600 1596
		f 4 -518 -2832 2835 -2835
		mu 0 4 1602 318 1603 1601
		f 4 -1387 1388 2836 -2836
		mu 0 4 1603 829 1604 1601
		f 4 2832 -2834 2837 -2837
		mu 0 4 1604 828 1605 1601
		f 4 -519 514 2843 -2843
		mu 0 4 1607 319 1608 1606
		f 4 2839 1394 2844 -2844
		mu 0 4 1608 832 1609 1606
		f 4 2840 -2842 2845 -2845
		mu 0 4 1609 831 1610 1606
		f 4 -525 -2848 2851 -2851
		mu 0 4 1612 322 1613 1611
		f 4 -1399 1401 2852 -2852
		mu 0 4 1613 835 1614 1611
		f 4 2848 -2850 2853 -2853
		mu 0 4 1614 834 1615 1611
		f 4 -526 -2856 2859 -2859
		mu 0 4 1617 323 1618 1616
		f 4 -1406 1407 2860 -2860
		mu 0 4 1618 839 1619 1616
		f 4 2856 -2858 2861 -2861
		mu 0 4 1619 838 1620 1616
		f 4 -527 522 2867 -2867
		mu 0 4 1622 324 1623 1621
		f 4 2863 1413 2868 -2868
		mu 0 4 1623 842 1624 1621
		f 4 2864 -2866 2869 -2869
		mu 0 4 1624 841 1625 1621
		f 4 -533 -2872 2875 -2875
		mu 0 4 1627 327 1628 1626
		f 4 -1418 1420 2876 -2876
		mu 0 4 1628 845 1629 1626
		f 4 2872 -2874 2877 -2877
		mu 0 4 1629 844 1630 1626
		f 4 -534 -2880 2883 -2883
		mu 0 4 1632 328 1633 1631
		f 4 -1425 1426 2884 -2884
		mu 0 4 1633 849 1634 1631
		f 4 2880 -2882 2885 -2885
		mu 0 4 1634 848 1635 1631
		f 4 -535 530 2891 -2891
		mu 0 4 1637 329 1638 1636
		f 4 2887 1432 2892 -2892
		mu 0 4 1638 852 1639 1636
		f 4 2888 -2890 2893 -2893
		mu 0 4 1639 851 1640 1636
		f 4 -541 -2896 2899 -2899
		mu 0 4 1642 332 1643 1641
		f 4 -1437 1439 2900 -2900
		mu 0 4 1643 855 1644 1641
		f 4 2896 -2898 2901 -2901
		mu 0 4 1644 854 1645 1641
		f 4 -542 -2904 2907 -2907
		mu 0 4 1647 333 1648 1646
		f 4 -1444 1445 2908 -2908
		mu 0 4 1648 859 1649 1646
		f 4 2904 -2906 2909 -2909
		mu 0 4 1649 858 1650 1646
		f 4 -543 538 2915 -2915
		mu 0 4 1652 334 1653 1651
		f 4 2911 1451 2916 -2916
		mu 0 4 1653 862 1654 1651
		f 4 2912 -2914 2917 -2917
		mu 0 4 1654 861 1655 1651
		f 4 -548 -2920 2923 -2923
		mu 0 4 1657 337 1658 1656
		f 4 -1456 1458 2924 -2924
		mu 0 4 1658 865 1659 1656
		f 4 2920 -2922 2925 -2925
		mu 0 4 1659 864 1660 1656
		f 4 -549 -2928 2931 -2931
		mu 0 4 1662 338 1663 1661
		f 4 -1463 1464 2932 -2932
		mu 0 4 1663 869 1664 1661
		f 4 2928 -2930 2933 -2933
		mu 0 4 1664 868 1665 1661
		f 4 -550 546 2939 -2939
		mu 0 4 1667 339 1668 1666
		f 4 2935 1469 2940 -2940
		mu 0 4 1668 872 1669 1666
		f 4 2936 -2938 2941 -2941
		mu 0 4 1669 871 1670 1666
		f 4 -555 -2944 2947 -2947
		mu 0 4 1672 341 1673 1671
		f 4 -1474 1476 2948 -2948
		mu 0 4 1673 874 1674 1671
		f 4 2944 -2946 2949 -2949
		mu 0 4 1674 873 1675 1671
		f 4 -556 -2952 2955 -2955
		mu 0 4 1677 342 1678 1676
		f 4 -1481 1482 2956 -2956
		mu 0 4 1678 878 1679 1676
		f 4 2952 -2954 2957 -2957
		mu 0 4 1679 877 1680 1676
		f 4 -557 553 2963 -2963
		mu 0 4 1682 343 1683 1681
		f 4 2959 1487 2964 -2964
		mu 0 4 1683 881 1684 1681
		f 4 2960 -2962 2965 -2965
		mu 0 4 1684 880 1685 1681
		f 4 -562 -2968 2971 -2971
		mu 0 4 1687 345 1688 1686
		f 4 -1492 1494 2972 -2972
		mu 0 4 1688 883 1689 1686
		f 4 2968 -2970 2973 -2973
		mu 0 4 1689 882 1690 1686
		f 4 -563 -2976 2979 -2979
		mu 0 4 1692 346 1693 1691
		f 4 -1499 1500 2980 -2980
		mu 0 4 1693 887 1694 1691
		f 4 2976 -2978 2981 -2981
		mu 0 4 1694 886 1695 1691
		f 4 -564 560 2987 -2987
		mu 0 4 1697 347 1698 1696
		f 4 2983 1505 2988 -2988
		mu 0 4 1698 890 1699 1696
		f 4 2984 -2986 2989 -2989
		mu 0 4 1699 889 1700 1696
		f 4 -568 -2992 2995 -2995
		mu 0 4 1702 349 1703 1701
		f 4 -1510 1511 2996 -2996
		mu 0 4 1703 892 1704 1701
		f 4 2992 -2994 2997 -2997
		mu 0 4 1704 891 1705 1701
		f 4 -569 536 3003 -3003
		mu 0 4 1707 332 1708 1706
		f 4 2999 1516 3004 -3004
		mu 0 4 1708 541 1709 1706
		f 4 3000 -3002 3005 -3005
		mu 0 4 1709 894 1710 1706
		f 4 -570 566 3011 -3011
		mu 0 4 1712 350 1713 1711
		f 4 3007 1521 3012 -3012
		mu 0 4 1713 897 1714 1711
		f 4 3008 -3010 3013 -3013
		mu 0 4 1714 896 1715 1711
		f 4 -575 571 3019 -3019
		mu 0 4 1717 352 1718 1716
		f 4 3015 1528 3020 -3020
		mu 0 4 1718 899 1719 1716
		f 4 3016 -3018 3021 -3021
		mu 0 4 1719 898 1720 1716
		f 4 -576 572 3027 -3027
		mu 0 4 1722 353 1723 1721
		f 4 3023 1534 3028 -3028
		mu 0 4 1723 903 1724 1721
		f 4 3024 -3026 3029 -3029
		mu 0 4 1724 902 1725 1721
		f 4 -577 573 3035 -3035
		mu 0 4 1727 354 1728 1726
		f 4 3031 1539 3036 -3036
		mu 0 4 1728 906 1729 1726
		f 4 3032 -3034 3037 -3037
		mu 0 4 1729 905 1730 1726
		f 4 -581 578 3043 -3043
		mu 0 4 1732 356 1733 1731
		f 4 3039 1546 3044 -3044
		mu 0 4 1733 908 1734 1731
		f 4 3040 -3042 3045 -3045
		mu 0 4 1734 907 1735 1731
		f 4 -582 -3048 3051 -3051
		mu 0 4 1737 357 1738 1736
		f 4 -1002 1551 3052 -3052
		mu 0 4 1738 596 1739 1736
		f 4 3048 -3050 3053 -3053
		mu 0 4 1739 911 1740 1736
		f 4 -583 483 3059 -3059
		mu 0 4 1742 300 1743 1741
		f 4 3055 1555 3060 -3060
		mu 0 4 1743 793 1744 1741
		f 4 3056 -3058 3061 -3061
		mu 0 4 1744 913 1745 1741
		f 4 -588 -3064 3067 -3067
		mu 0 4 1747 359 1748 1746
		f 4 -1560 1562 3068 -3068
		mu 0 4 1748 915 1749 1746
		f 4 3064 -3066 3069 -3069
		mu 0 4 1749 914 1750 1746
		f 4 -589 -3072 3075 -3075
		mu 0 4 1752 360 1753 1751
		f 4 -1567 1568 3076 -3076
		mu 0 4 1753 919 1754 1751
		f 4 3072 -3074 3077 -3077
		mu 0 4 1754 918 1755 1751
		f 4 -590 586 3083 -3083
		mu 0 4 1757 361 1758 1756
		f 4 3079 1573 3084 -3084
		mu 0 4 1758 922 1759 1756
		f 4 3080 -3082 3085 -3085
		mu 0 4 1759 921 1760 1756
		f 4 -595 -3088 3091 -3091
		mu 0 4 1762 363 1763 1761
		f 4 -1578 1580 3092 -3092
		mu 0 4 1763 924 1764 1761
		f 4 3088 -3090 3093 -3093
		mu 0 4 1764 923 1765 1761
		f 4 -596 -3096 3099 -3099
		mu 0 4 1767 364 1768 1766
		f 4 -1585 1586 3100 -3100
		mu 0 4 1768 928 1769 1766
		f 4 3096 -3098 3101 -3101
		mu 0 4 1769 927 1770 1766
		f 4 -597 593 3107 -3107
		mu 0 4 1772 365 1773 1771
		f 4 3103 1591 3108 -3108
		mu 0 4 1773 931 1774 1771
		f 4 3104 -3106 3109 -3109
		mu 0 4 1774 930 1775 1771
		f 4 -602 -3112 3115 -3115
		mu 0 4 1777 367 1778 1776
		f 4 -1596 1598 3116 -3116
		mu 0 4 1778 933 1779 1776
		f 4 3112 -3114 3117 -3117
		mu 0 4 1779 932 1780 1776
		f 4 -603 -3120 3123 -3123
		mu 0 4 1782 368 1783 1781
		f 4 -1603 1604 3124 -3124
		mu 0 4 1783 937 1784 1781
		f 4 3120 -3122 3125 -3125
		mu 0 4 1784 936 1785 1781
		f 4 -604 600 3131 -3131
		mu 0 4 1787 369 1788 1786
		f 4 3127 1609 3132 -3132
		mu 0 4 1788 940 1789 1786
		f 4 3128 -3130 3133 -3133
		mu 0 4 1789 939 1790 1786
		f 4 -608 -3136 3139 -3139
		mu 0 4 1792 371 1793 1791
		f 4 -1614 1615 3140 -3140
		mu 0 4 1793 942 1794 1791
		f 4 3136 -3138 3141 -3141
		mu 0 4 1794 941 1795 1791
		f 4 -609 480 3147 -3147
		mu 0 4 1797 297 1798 1796
		f 4 3143 1620 3148 -3148
		mu 0 4 1798 506 1799 1796
		f 4 3144 -3146 3149 -3149
		mu 0 4 1799 944 1800 1796
		f 4 -610 606 3155 -3155
		mu 0 4 1802 372 1803 1801
		f 4 3151 1625 3156 -3156
		mu 0 4 1803 947 1804 1801
		f 4 3152 -3154 3157 -3157
		mu 0 4 1804 946 1805 1801
		f 4 -614 579 3163 -3163
		mu 0 4 1807 357 1808 1806
		f 4 3159 1631 3164 -3164
		mu 0 4 1808 909 1809 1806
		f 4 3160 -3162 3165 -3165
		mu 0 4 1809 948 1810 1806
		f 4 -615 611 3171 -3171
		mu 0 4 1812 374 1813 1811
		f 4 3167 1637 3172 -3172
		mu 0 4 1813 952 1814 1811
		f 4 3168 -3170 3173 -3173
		mu 0 4 1814 951 1815 1811
		f 4 -616 612 3179 -3179
		mu 0 4 1817 375 1818 1816
		f 4 3175 1642 3180 -3180
		mu 0 4 1818 955 1819 1816
		f 4 3176 -3178 3181 -3181
		mu 0 4 1819 954 1820 1816
		f 4 -620 617 3187 -3187
		mu 0 4 1822 377 1823 1821
		f 4 3183 1649 3188 -3188
		mu 0 4 1823 957 1824 1821
		f 4 3184 -3186 3189 -3189
		mu 0 4 1824 956 1825 1821
		f 4 -621 -3192 3195 -3195
		mu 0 4 1827 378 1828 1826
		f 4 -1050 1654 3196 -3196
		mu 0 4 1828 626 1829 1826
		f 4 3192 -3194 3197 -3197
		mu 0 4 1829 960 1830 1826
		f 4 -622 491 3203 -3203
		mu 0 4 1832 305 1833 1831
		f 4 3199 1658 3204 -3204
		mu 0 4 1833 803 1834 1831
		f 4 3200 -3202 3205 -3205
		mu 0 4 1834 962 1835 1831
		f 4 -627 -3208 3211 -3211
		mu 0 4 1837 380 1838 1836
		f 4 -1663 1665 3212 -3212
		mu 0 4 1838 964 1839 1836
		f 4 3208 -3210 3213 -3213
		mu 0 4 1839 963 1840 1836
		f 4 -628 -3216 3219 -3219
		mu 0 4 1842 381 1843 1841
		f 4 -1670 1671 3220 -3220
		mu 0 4 1843 968 1844 1841
		f 4 3216 -3218 3221 -3221
		mu 0 4 1844 967 1845 1841
		f 4 -629 625 3227 -3227
		mu 0 4 1847 382 1848 1846
		f 4 3223 1676 3228 -3228
		mu 0 4 1848 971 1849 1846
		f 4 3224 -3226 3229 -3229
		mu 0 4 1849 970 1850 1846
		f 4 -634 -3232 3235 -3235
		mu 0 4 1852 384 1853 1851
		f 4 -1681 1683 3236 -3236
		mu 0 4 1853 973 1854 1851
		f 4 3232 -3234 3237 -3237
		mu 0 4 1854 972 1855 1851
		f 4 -635 -3240 3243 -3243
		mu 0 4 1857 385 1858 1856
		f 4 -1688 1689 3244 -3244
		mu 0 4 1858 977 1859 1856
		f 4 3240 -3242 3245 -3245
		mu 0 4 1859 976 1860 1856
		f 4 -636 632 3251 -3251
		mu 0 4 1862 386 1863 1861
		f 4 3247 1694 3252 -3252
		mu 0 4 1863 980 1864 1861
		f 4 3248 -3250 3253 -3253
		mu 0 4 1864 979 1865 1861
		f 4 -641 -3256 3259 -3259
		mu 0 4 1867 388 1868 1866
		f 4 -1699 1701 3260 -3260
		mu 0 4 1868 982 1869 1866
		f 4 3256 -3258 3261 -3261
		mu 0 4 1869 981 1870 1866
		f 4 -642 -3264 3267 -3267
		mu 0 4 1872 389 1873 1871
		f 4 -1706 1707 3268 -3268
		mu 0 4 1873 986 1874 1871
		f 4 3264 -3266 3269 -3269
		mu 0 4 1874 985 1875 1871
		f 4 -643 639 3275 -3275
		mu 0 4 1877 390 1878 1876
		f 4 3271 1712 3276 -3276
		mu 0 4 1878 989 1879 1876
		f 4 3272 -3274 3277 -3277
		mu 0 4 1879 988 1880 1876
		f 4 -647 -3280 3283 -3283
		mu 0 4 1882 392 1883 1881
		f 4 -1717 1718 3284 -3284
		mu 0 4 1883 991 1884 1881
		f 4 3280 -3282 3285 -3285
		mu 0 4 1884 990 1885 1881
		f 4 -648 488 3291 -3291
		mu 0 4 1887 302 1888 1886
		f 4 3287 1723 3292 -3292
		mu 0 4 1888 511 1889 1886
		f 4 3288 -3290 3293 -3293
		mu 0 4 1889 993 1890 1886
		f 4 -649 645 3299 -3299
		mu 0 4 1892 393 1893 1891
		f 4 3295 1728 3300 -3300
		mu 0 4 1893 996 1894 1891
		f 4 3296 -3298 3301 -3301
		mu 0 4 1894 995 1895 1891
		f 4 -653 618 3307 -3307
		mu 0 4 1897 378 1898 1896
		f 4 3303 1734 3308 -3308
		mu 0 4 1898 958 1899 1896
		f 4 3304 -3306 3309 -3309
		mu 0 4 1899 997 1900 1896
		f 4 -654 650 3315 -3315
		mu 0 4 1902 395 1903 1901
		f 4 3311 1740 3316 -3316
		mu 0 4 1903 1001 1904 1901
		f 4 3312 -3314 3317 -3317
		mu 0 4 1904 1000 1905 1901
		f 4 -655 651 3323 -3323
		mu 0 4 1907 396 1908 1906
		f 4 3319 1745 3324 -3324
		mu 0 4 1908 1004 1909 1906
		f 4 3320 -3322 3325 -3325
		mu 0 4 1909 1003 1910 1906
		f 4 -659 656 3331 -3331
		mu 0 4 1912 398 1913 1911
		f 4 3327 1752 3332 -3332
		mu 0 4 1913 1006 1914 1911
		f 4 3328 -3330 3333 -3333
		mu 0 4 1914 1005 1915 1911
		f 4 -660 -3336 3339 -3339
		mu 0 4 1917 399 1918 1916
		f 4 -1098 1757 3340 -3340
		mu 0 4 1918 656 1919 1916
		f 4 3336 -3338 3341 -3341
		mu 0 4 1919 1009 1920 1916
		f 4 -661 499 3347 -3347
		mu 0 4 1922 310 1923 1921
		f 4 3343 1761 3348 -3348
		mu 0 4 1923 813 1924 1921
		f 4 3344 -3346 3349 -3349
		mu 0 4 1924 1011 1925 1921
		f 4 -666 -3352 3355 -3355
		mu 0 4 1927 401 1928 1926
		f 4 -1766 1768 3356 -3356
		mu 0 4 1928 1013 1929 1926
		f 4 3352 -3354 3357 -3357
		mu 0 4 1929 1012 1930 1926
		f 4 -667 -3360 3363 -3363
		mu 0 4 1932 402 1933 1931
		f 4 -1773 1774 3364 -3364
		mu 0 4 1933 1017 1934 1931
		f 4 3360 -3362 3365 -3365
		mu 0 4 1934 1016 1935 1931
		f 4 -668 664 3371 -3371
		mu 0 4 1937 403 1938 1936
		f 4 3367 1779 3372 -3372
		mu 0 4 1938 1020 1939 1936
		f 4 3368 -3370 3373 -3373
		mu 0 4 1939 1019 1940 1936
		f 4 -673 -3376 3379 -3379
		mu 0 4 1942 405 1943 1941
		f 4 -1784 1786 3380 -3380
		mu 0 4 1943 1022 1944 1941
		f 4 3376 -3378 3381 -3381
		mu 0 4 1944 1021 1945 1941
		f 4 -674 -3384 3387 -3387
		mu 0 4 1947 406 1948 1946
		f 4 -1791 1792 3388 -3388
		mu 0 4 1948 1026 1949 1946
		f 4 3384 -3386 3389 -3389
		mu 0 4 1949 1025 1950 1946
		f 4 -675 671 3395 -3395
		mu 0 4 1952 407 1953 1951
		f 4 3391 1797 3396 -3396
		mu 0 4 1953 1029 1954 1951
		f 4 3392 -3394 3397 -3397
		mu 0 4 1954 1028 1955 1951
		f 4 -680 -3400 3403 -3403
		mu 0 4 1957 409 1958 1956
		f 4 -1802 1804 3404 -3404
		mu 0 4 1958 1031 1959 1956
		f 4 3400 -3402 3405 -3405
		mu 0 4 1959 1030 1960 1956
		f 4 -681 -3408 3411 -3411
		mu 0 4 1962 410 1963 1961
		f 4 -1809 1810 3412 -3412
		mu 0 4 1963 1035 1964 1961
		f 4 3408 -3410 3413 -3413
		mu 0 4 1964 1034 1965 1961
		f 4 -682 678 3419 -3419
		mu 0 4 1967 411 1968 1966
		f 4 3415 1815 3420 -3420
		mu 0 4 1968 1038 1969 1966
		f 4 3416 -3418 3421 -3421
		mu 0 4 1969 1037 1970 1966
		f 4 -686 -3424 3427 -3427
		mu 0 4 1972 413 1973 1971
		f 4 -1820 1821 3428 -3428
		mu 0 4 1973 1040 1974 1971
		f 4 3424 -3426 3429 -3429
		mu 0 4 1974 1039 1975 1971;
	setAttr ".fc[1500:1999]"
		f 4 -687 496 3435 -3435
		mu 0 4 1977 307 1978 1976
		f 4 3431 1826 3436 -3436
		mu 0 4 1978 516 1979 1976
		f 4 3432 -3434 3437 -3437
		mu 0 4 1979 1042 1980 1976
		f 4 -688 684 3443 -3443
		mu 0 4 1982 414 1983 1981
		f 4 3439 1831 3444 -3444
		mu 0 4 1983 1045 1984 1981
		f 4 3440 -3442 3445 -3445
		mu 0 4 1984 1044 1985 1981
		f 4 -692 657 3451 -3451
		mu 0 4 1987 399 1988 1986
		f 4 3447 1837 3452 -3452
		mu 0 4 1988 1007 1989 1986
		f 4 3448 -3450 3453 -3453
		mu 0 4 1989 1046 1990 1986
		f 4 -693 689 3459 -3459
		mu 0 4 1992 416 1993 1991
		f 4 3455 1843 3460 -3460
		mu 0 4 1993 1050 1994 1991
		f 4 3456 -3458 3461 -3461
		mu 0 4 1994 1049 1995 1991
		f 4 -694 690 3467 -3467
		mu 0 4 1997 417 1998 1996
		f 4 3463 1848 3468 -3468
		mu 0 4 1998 1053 1999 1996
		f 4 3464 -3466 3469 -3469
		mu 0 4 1999 1052 2000 1996
		f 4 -698 695 3475 -3475
		mu 0 4 2002 419 2003 2001
		f 4 3471 1855 3476 -3476
		mu 0 4 2003 1055 2004 2001
		f 4 3472 -3474 3477 -3477
		mu 0 4 2004 1054 2005 2001
		f 4 -699 -3480 3483 -3483
		mu 0 4 2007 420 2008 2006
		f 4 -1146 1860 3484 -3484
		mu 0 4 2008 686 2009 2006
		f 4 3480 -3482 3485 -3485
		mu 0 4 2009 1058 2010 2006
		f 4 -700 507 3491 -3491
		mu 0 4 2012 315 2013 2011
		f 4 3487 1864 3492 -3492
		mu 0 4 2013 823 2014 2011
		f 4 3488 -3490 3493 -3493
		mu 0 4 2014 1060 2015 2011
		f 4 -705 -3496 3499 -3499
		mu 0 4 2017 422 2018 2016
		f 4 -1869 1871 3500 -3500
		mu 0 4 2018 1062 2019 2016
		f 4 3496 -3498 3501 -3501
		mu 0 4 2019 1061 2020 2016
		f 4 -706 -3504 3507 -3507
		mu 0 4 2022 423 2023 2021
		f 4 -1876 1877 3508 -3508
		mu 0 4 2023 1066 2024 2021
		f 4 3504 -3506 3509 -3509
		mu 0 4 2024 1065 2025 2021
		f 4 -707 703 3515 -3515
		mu 0 4 2027 424 2028 2026
		f 4 3511 1882 3516 -3516
		mu 0 4 2028 1069 2029 2026
		f 4 3512 -3514 3517 -3517
		mu 0 4 2029 1068 2030 2026
		f 4 -712 -3520 3523 -3523
		mu 0 4 2032 426 2033 2031
		f 4 -1887 1889 3524 -3524
		mu 0 4 2033 1071 2034 2031
		f 4 3520 -3522 3525 -3525
		mu 0 4 2034 1070 2035 2031
		f 4 -713 -3528 3531 -3531
		mu 0 4 2037 427 2038 2036
		f 4 -1894 1895 3532 -3532
		mu 0 4 2038 1075 2039 2036
		f 4 3528 -3530 3533 -3533
		mu 0 4 2039 1074 2040 2036
		f 4 -714 710 3539 -3539
		mu 0 4 2042 428 2043 2041
		f 4 3535 1900 3540 -3540
		mu 0 4 2043 1078 2044 2041
		f 4 3536 -3538 3541 -3541
		mu 0 4 2044 1077 2045 2041
		f 4 -719 -3544 3547 -3547
		mu 0 4 2047 430 2048 2046
		f 4 -1905 1907 3548 -3548
		mu 0 4 2048 1080 2049 2046
		f 4 3544 -3546 3549 -3549
		mu 0 4 2049 1079 2050 2046
		f 4 -720 -3552 3555 -3555
		mu 0 4 2052 431 2053 2051
		f 4 -1912 1913 3556 -3556
		mu 0 4 2053 1084 2054 2051
		f 4 3552 -3554 3557 -3557
		mu 0 4 2054 1083 2055 2051
		f 4 -721 717 3563 -3563
		mu 0 4 2057 432 2058 2056
		f 4 3559 1918 3564 -3564
		mu 0 4 2058 1087 2059 2056
		f 4 3560 -3562 3565 -3565
		mu 0 4 2059 1086 2060 2056
		f 4 -725 -3568 3571 -3571
		mu 0 4 2062 434 2063 2061
		f 4 -1923 1924 3572 -3572
		mu 0 4 2063 1089 2064 2061
		f 4 3568 -3570 3573 -3573
		mu 0 4 2064 1088 2065 2061
		f 4 -726 504 3579 -3579
		mu 0 4 2067 312 2068 2066
		f 4 3575 1929 3580 -3580
		mu 0 4 2068 521 2069 2066
		f 4 3576 -3578 3581 -3581
		mu 0 4 2069 1091 2070 2066
		f 4 -727 723 3587 -3587
		mu 0 4 2072 435 2073 2071
		f 4 3583 1934 3588 -3588
		mu 0 4 2073 1094 2074 2071
		f 4 3584 -3586 3589 -3589
		mu 0 4 2074 1093 2075 2071
		f 4 -731 696 3595 -3595
		mu 0 4 2077 420 2078 2076
		f 4 3591 1940 3596 -3596
		mu 0 4 2078 1056 2079 2076
		f 4 3592 -3594 3597 -3597
		mu 0 4 2079 1095 2080 2076
		f 4 -732 728 3603 -3603
		mu 0 4 2082 437 2083 2081
		f 4 3599 1946 3604 -3604
		mu 0 4 2083 1099 2084 2081
		f 4 3600 -3602 3605 -3605
		mu 0 4 2084 1098 2085 2081
		f 4 -733 729 3611 -3611
		mu 0 4 2087 438 2088 2086
		f 4 3607 1951 3612 -3612
		mu 0 4 2088 1102 2089 2086
		f 4 3608 -3610 3613 -3613
		mu 0 4 2089 1101 2090 2086
		f 4 -737 734 3619 -3619
		mu 0 4 2092 440 2093 2091
		f 4 3615 1958 3620 -3620
		mu 0 4 2093 1104 2094 2091
		f 4 3616 -3618 3621 -3621
		mu 0 4 2094 1103 2095 2091
		f 4 -738 -3624 3627 -3627
		mu 0 4 2097 441 2098 2096
		f 4 -1194 1963 3628 -3628
		mu 0 4 2098 716 2099 2096
		f 4 3624 -3626 3629 -3629
		mu 0 4 2099 1107 2100 2096
		f 4 -739 515 3635 -3635
		mu 0 4 2102 320 2103 2101
		f 4 3631 1967 3636 -3636
		mu 0 4 2103 833 2104 2101
		f 4 3632 -3634 3637 -3637
		mu 0 4 2104 1109 2105 2101
		f 4 -744 -3640 3643 -3643
		mu 0 4 2107 443 2108 2106
		f 4 -1972 1974 3644 -3644
		mu 0 4 2108 1111 2109 2106
		f 4 3640 -3642 3645 -3645
		mu 0 4 2109 1110 2110 2106
		f 4 -745 -3648 3651 -3651
		mu 0 4 2112 444 2113 2111
		f 4 -1979 1980 3652 -3652
		mu 0 4 2113 1115 2114 2111
		f 4 3648 -3650 3653 -3653
		mu 0 4 2114 1114 2115 2111
		f 4 -746 742 3659 -3659
		mu 0 4 2117 445 2118 2116
		f 4 3655 1985 3660 -3660
		mu 0 4 2118 1118 2119 2116
		f 4 3656 -3658 3661 -3661
		mu 0 4 2119 1117 2120 2116
		f 4 -751 -3664 3667 -3667
		mu 0 4 2122 447 2123 2121
		f 4 -1990 1992 3668 -3668
		mu 0 4 2123 1120 2124 2121
		f 4 3664 -3666 3669 -3669
		mu 0 4 2124 1119 2125 2121
		f 4 -752 -3672 3675 -3675
		mu 0 4 2127 448 2128 2126
		f 4 -1997 1998 3676 -3676
		mu 0 4 2128 1124 2129 2126
		f 4 3672 -3674 3677 -3677
		mu 0 4 2129 1123 2130 2126
		f 4 -753 749 3683 -3683
		mu 0 4 2132 449 2133 2131
		f 4 3679 2003 3684 -3684
		mu 0 4 2133 1127 2134 2131
		f 4 3680 -3682 3685 -3685
		mu 0 4 2134 1126 2135 2131
		f 4 -758 -3688 3691 -3691
		mu 0 4 2137 451 2138 2136
		f 4 -2008 2010 3692 -3692
		mu 0 4 2138 1129 2139 2136
		f 4 3688 -3690 3693 -3693
		mu 0 4 2139 1128 2140 2136
		f 4 -759 -3696 3699 -3699
		mu 0 4 2142 452 2143 2141
		f 4 -2015 2016 3700 -3700
		mu 0 4 2143 1133 2144 2141
		f 4 3696 -3698 3701 -3701
		mu 0 4 2144 1132 2145 2141
		f 4 -760 756 3707 -3707
		mu 0 4 2147 453 2148 2146
		f 4 3703 2021 3708 -3708
		mu 0 4 2148 1136 2149 2146
		f 4 3704 -3706 3709 -3709
		mu 0 4 2149 1135 2150 2146
		f 4 -764 -3712 3715 -3715
		mu 0 4 2152 455 2153 2151
		f 4 -2026 2027 3716 -3716
		mu 0 4 2153 1138 2154 2151
		f 4 3712 -3714 3717 -3717
		mu 0 4 2154 1137 2155 2151
		f 4 -765 512 3723 -3723
		mu 0 4 2157 317 2158 2156
		f 4 3719 2032 3724 -3724
		mu 0 4 2158 526 2159 2156
		f 4 3720 -3722 3725 -3725
		mu 0 4 2159 1140 2160 2156
		f 4 -766 762 3731 -3731
		mu 0 4 2162 456 2163 2161
		f 4 3727 2037 3732 -3732
		mu 0 4 2163 1143 2164 2161
		f 4 3728 -3730 3733 -3733
		mu 0 4 2164 1142 2165 2161
		f 4 -770 735 3739 -3739
		mu 0 4 2167 441 2168 2166
		f 4 3735 2043 3740 -3740
		mu 0 4 2168 1105 2169 2166
		f 4 3736 -3738 3741 -3741
		mu 0 4 2169 1144 2170 2166
		f 4 -771 767 3747 -3747
		mu 0 4 2172 458 2173 2171
		f 4 3743 2049 3748 -3748
		mu 0 4 2173 1148 2174 2171
		f 4 3744 -3746 3749 -3749
		mu 0 4 2174 1147 2175 2171
		f 4 -772 768 3755 -3755
		mu 0 4 2177 459 2178 2176
		f 4 3751 2054 3756 -3756
		mu 0 4 2178 1151 2179 2176
		f 4 3752 -3754 3757 -3757
		mu 0 4 2179 1150 2180 2176
		f 4 -776 773 3763 -3763
		mu 0 4 2182 461 2183 2181
		f 4 3759 2061 3764 -3764
		mu 0 4 2183 1153 2184 2181
		f 4 3760 -3762 3765 -3765
		mu 0 4 2184 1152 2185 2181
		f 4 -777 -3768 3771 -3771
		mu 0 4 2187 462 2188 2186
		f 4 -1242 2066 3772 -3772
		mu 0 4 2188 746 2189 2186
		f 4 3768 -3770 3773 -3773
		mu 0 4 2189 1156 2190 2186
		f 4 -778 523 3779 -3779
		mu 0 4 2192 325 2193 2191
		f 4 3775 2070 3780 -3780
		mu 0 4 2193 843 2194 2191
		f 4 3776 -3778 3781 -3781
		mu 0 4 2194 1158 2195 2191
		f 4 -783 -3784 3787 -3787
		mu 0 4 2197 464 2198 2196
		f 4 -2075 2077 3788 -3788
		mu 0 4 2198 1160 2199 2196
		f 4 3784 -3786 3789 -3789
		mu 0 4 2199 1159 2200 2196
		f 4 -784 -3792 3795 -3795
		mu 0 4 2202 465 2203 2201
		f 4 -2082 2083 3796 -3796
		mu 0 4 2203 1164 2204 2201
		f 4 3792 -3794 3797 -3797
		mu 0 4 2204 1163 2205 2201
		f 4 -785 781 3803 -3803
		mu 0 4 2207 466 2208 2206
		f 4 3799 2088 3804 -3804
		mu 0 4 2208 1167 2209 2206
		f 4 3800 -3802 3805 -3805
		mu 0 4 2209 1166 2210 2206
		f 4 -790 -3808 3811 -3811
		mu 0 4 2212 468 2213 2211
		f 4 -2093 2095 3812 -3812
		mu 0 4 2213 1169 2214 2211
		f 4 3808 -3810 3813 -3813
		mu 0 4 2214 1168 2215 2211
		f 4 -791 -3816 3819 -3819
		mu 0 4 2217 469 2218 2216
		f 4 -2100 2101 3820 -3820
		mu 0 4 2218 1173 2219 2216
		f 4 3816 -3818 3821 -3821
		mu 0 4 2219 1172 2220 2216
		f 4 -792 788 3827 -3827
		mu 0 4 2222 470 2223 2221
		f 4 3823 2106 3828 -3828
		mu 0 4 2223 1176 2224 2221
		f 4 3824 -3826 3829 -3829
		mu 0 4 2224 1175 2225 2221
		f 4 -797 -3832 3835 -3835
		mu 0 4 2227 472 2228 2226
		f 4 -2111 2113 3836 -3836
		mu 0 4 2228 1178 2229 2226
		f 4 3832 -3834 3837 -3837
		mu 0 4 2229 1177 2230 2226
		f 4 -798 -3840 3843 -3843
		mu 0 4 2232 473 2233 2231
		f 4 -2118 2119 3844 -3844
		mu 0 4 2233 1182 2234 2231
		f 4 3840 -3842 3845 -3845
		mu 0 4 2234 1181 2235 2231
		f 4 -799 795 3851 -3851
		mu 0 4 2237 474 2238 2236
		f 4 3847 2124 3852 -3852
		mu 0 4 2238 1185 2239 2236
		f 4 3848 -3850 3853 -3853
		mu 0 4 2239 1184 2240 2236
		f 4 -803 -3856 3859 -3859
		mu 0 4 2242 476 2243 2241
		f 4 -2129 2130 3860 -3860
		mu 0 4 2243 1187 2244 2241
		f 4 3856 -3858 3861 -3861
		mu 0 4 2244 1186 2245 2241
		f 4 -804 520 3867 -3867
		mu 0 4 2247 322 2248 2246
		f 4 3863 2135 3868 -3868
		mu 0 4 2248 531 2249 2246
		f 4 3864 -3866 3869 -3869
		mu 0 4 2249 1189 2250 2246
		f 4 -805 801 3875 -3875
		mu 0 4 2252 477 2253 2251
		f 4 3871 2140 3876 -3876
		mu 0 4 2253 1192 2254 2251
		f 4 3872 -3874 3877 -3877
		mu 0 4 2254 1191 2255 2251
		f 4 -809 774 3883 -3883
		mu 0 4 2257 462 2258 2256
		f 4 3879 2146 3884 -3884
		mu 0 4 2258 1154 2259 2256
		f 4 3880 -3882 3885 -3885
		mu 0 4 2259 1193 2260 2256
		f 4 -810 806 3891 -3891
		mu 0 4 2262 479 2263 2261
		f 4 3887 2152 3892 -3892
		mu 0 4 2263 1197 2264 2261
		f 4 3888 -3890 3893 -3893
		mu 0 4 2264 1196 2265 2261
		f 4 -811 807 3899 -3899
		mu 0 4 2267 480 2268 2266
		f 4 3895 2157 3900 -3900
		mu 0 4 2268 1200 2269 2266
		f 4 3896 -3898 3901 -3901
		mu 0 4 2269 1199 2270 2266
		f 4 -815 812 3907 -3907
		mu 0 4 2272 482 2273 2271
		f 4 3903 2164 3908 -3908
		mu 0 4 2273 1202 2274 2271
		f 4 3904 -3906 3909 -3909
		mu 0 4 2274 1201 2275 2271
		f 4 -816 -3912 3914 -3914
		mu 0 4 2277 483 2278 2276
		f 4 -7811 7823 7825 -7827
		mu 0 4 4106 4105 4112 4113
		f 4 7828 -7831 7831 -7826
		mu 0 4 4112 4114 4115 4113
		f 4 -817 531 3921 -3921
		mu 0 4 2282 330 2283 2281
		f 4 3917 2172 3922 -3922
		mu 0 4 2283 853 2284 2281
		f 4 3918 -3920 3923 -3923
		mu 0 4 2284 1207 2285 2281
		f 4 -822 -3926 3929 -3929
		mu 0 4 2287 485 2288 2286
		f 4 -2177 2179 3930 -3930
		mu 0 4 2288 1209 2289 2286
		f 4 3926 -3928 3931 -3931
		mu 0 4 2289 1208 2290 2286
		f 4 -823 -3934 3937 -3937
		mu 0 4 2292 486 2293 2291
		f 4 -2184 2185 3938 -3938
		mu 0 4 2293 1213 2294 2291
		f 4 3934 -3936 3939 -3939
		mu 0 4 2294 1212 2295 2291
		f 4 -824 820 3945 -3945
		mu 0 4 2297 487 2298 2296
		f 4 3941 2190 3946 -3946
		mu 0 4 2298 1216 2299 2296
		f 4 3942 -3944 3947 -3947
		mu 0 4 2299 1215 2300 2296
		f 4 -829 -3950 3953 -3953
		mu 0 4 2302 489 2303 2301
		f 4 -2195 2197 3954 -3954
		mu 0 4 2303 1218 2304 2301
		f 4 3950 -3952 3955 -3955
		mu 0 4 2304 1217 2305 2301
		f 4 -830 -3958 3961 -3961
		mu 0 4 2307 490 2308 2306
		f 4 -2202 2203 3962 -3962
		mu 0 4 2308 1222 2309 2306
		f 4 3958 -3960 3963 -3963
		mu 0 4 2309 1221 2310 2306
		f 4 -831 827 3969 -3969
		mu 0 4 2312 491 2313 2311
		f 4 3965 2208 3970 -3970
		mu 0 4 2313 1225 2314 2311
		f 4 3966 -3968 3971 -3971
		mu 0 4 2314 1224 2315 2311
		f 4 -836 -3974 3977 -3977
		mu 0 4 2317 493 2318 2316
		f 4 -2213 2215 3978 -3978
		mu 0 4 2318 1227 2319 2316
		f 4 3974 -3976 3979 -3979
		mu 0 4 2319 1226 2320 2316
		f 4 -837 -3982 3985 -3985
		mu 0 4 2322 494 2323 2321
		f 4 -2220 2221 3986 -3986
		mu 0 4 2323 1231 2324 2321
		f 4 3982 -3984 3987 -3987
		mu 0 4 2324 1230 2325 2321
		f 4 -838 834 3993 -3993
		mu 0 4 2327 495 2328 2326
		f 4 3989 2226 3994 -3994
		mu 0 4 2328 1234 2329 2326
		f 4 3990 -3992 3995 -3995
		mu 0 4 2329 1233 2330 2326
		f 4 -842 -3998 4001 -4001
		mu 0 4 2332 497 2333 2331
		f 4 -2231 2232 4002 -4002
		mu 0 4 2333 1236 2334 2331
		f 4 3998 -4000 4003 -4003
		mu 0 4 2334 1235 2335 2331
		f 4 -843 528 4009 -4009
		mu 0 4 2337 327 2338 2336
		f 4 4005 2237 4010 -4010
		mu 0 4 2338 536 2339 2336
		f 4 4006 -4008 4011 -4011
		mu 0 4 2339 1238 2340 2336
		f 4 -844 840 4017 -4017
		mu 0 4 2342 498 2343 2341
		f 4 4013 2242 4018 -4018
		mu 0 4 2343 1241 2344 2341
		f 4 4014 -4016 4019 -4019
		mu 0 4 2344 1240 2345 2341
		f 4 -848 813 4025 -4025
		mu 0 4 2347 483 2348 2346
		f 4 4021 2248 4026 -4026
		mu 0 4 2348 1203 2349 2346
		f 4 4022 -4024 4027 -4027
		mu 0 4 2349 1242 2350 2346
		f 4 -849 845 4033 -4033
		mu 0 4 2352 500 2353 2351
		f 4 4029 2254 4034 -4034
		mu 0 4 2353 1246 2354 2351
		f 4 4030 -4032 4035 -4035
		mu 0 4 2354 1245 2355 2351
		f 4 -850 846 4041 -4041
		mu 0 4 2357 501 2358 2356
		f 4 4037 2259 4042 -4042
		mu 0 4 2358 1249 2359 2356
		f 4 4038 -4040 4043 -4043
		mu 0 4 2359 1248 2360 2356
		f 4 -853 851 4049 -4049
		mu 0 4 2362 503 2363 2361
		f 4 4045 2265 4050 -4050
		mu 0 4 2363 1251 2364 2361
		f 4 4046 -4048 4051 -4051
		mu 0 4 2364 1250 2365 2361
		f 4 -854 -4054 4057 -4057
		mu 0 4 2367 352 2368 2366
		f 4 -954 2270 4058 -4058
		mu 0 4 2368 566 2369 2366
		f 4 4054 -4056 4059 -4059
		mu 0 4 2369 1253 2370 2366
		f 4 -855 539 4065 -4065
		mu 0 4 2372 335 2373 2371
		f 4 4061 2274 4066 -4066
		mu 0 4 2373 863 2374 2371
		f 4 4062 -4064 4067 -4067
		mu 0 4 2374 1255 2375 2371
		f 4 -390 -4070 4072 -4072
		mu 0 4 2377 247 2378 2376
		f 4 -858 861 4073 -4073
		mu 0 4 2378 506 2379 2376
		f 4 4070 -2281 4074 -4074
		mu 0 4 2379 504 1259 2376
		f 4 -481 484 4077 -4077
		mu 0 4 1798 297 1537 2380
		f 4 2726 862 4078 -4078
		mu 0 4 1537 507 2381 2380
		f 4 4075 -4071 4079 -4079
		mu 0 4 2381 504 2379 2380
		f 4 4080 -4082 4083 -4083
		mu 0 4 2383 296 2384 2382
		f 4 -860 863 4084 -4084
		mu 0 4 2384 508 1260 2382
		f 4 2281 -4076 4085 -4085
		mu 0 4 1260 504 2381 2382
		f 4 -403 -4088 4090 -4090
		mu 0 4 2386 254 2387 2385
		f 4 -866 869 4091 -4091
		mu 0 4 2387 511 2388 2385
		f 4 4088 -2289 4092 -4092
		mu 0 4 2388 509 1264 2385
		f 4 -489 492 4095 -4095
		mu 0 4 1888 302 1552 2389
		f 4 2750 870 4096 -4096
		mu 0 4 1552 512 2390 2389
		f 4 4093 -4089 4097 -4097
		mu 0 4 2390 509 2388 2389
		f 4 4098 -4100 4101 -4101
		mu 0 4 2392 301 2393 2391
		f 4 -868 871 4102 -4102
		mu 0 4 2393 513 1265 2391
		f 4 2289 -4094 4103 -4103
		mu 0 4 1265 509 2390 2391
		f 4 -416 -4106 4108 -4108
		mu 0 4 2395 261 2396 2394
		f 4 -874 877 4109 -4109
		mu 0 4 2396 516 2397 2394
		f 4 4106 -2297 4110 -4110
		mu 0 4 2397 514 1269 2394
		f 4 -497 500 4113 -4113
		mu 0 4 1978 307 1567 2398
		f 4 2774 878 4114 -4114
		mu 0 4 1567 517 2399 2398
		f 4 4111 -4107 4115 -4115
		mu 0 4 2399 514 2397 2398
		f 4 4116 -4118 4119 -4119
		mu 0 4 2401 306 2402 2400
		f 4 -876 879 4120 -4120
		mu 0 4 2402 518 1270 2400
		f 4 2297 -4112 4121 -4121
		mu 0 4 1270 514 2399 2400
		f 4 -429 -4124 4126 -4126
		mu 0 4 2404 268 2405 2403
		f 4 -882 885 4127 -4127
		mu 0 4 2405 521 2406 2403
		f 4 4124 -2305 4128 -4128
		mu 0 4 2406 519 1274 2403
		f 4 -505 508 4131 -4131
		mu 0 4 2068 312 1582 2407
		f 4 2798 886 4132 -4132
		mu 0 4 1582 522 2408 2407
		f 4 4129 -4125 4133 -4133
		mu 0 4 2408 519 2406 2407
		f 4 4134 -4136 4137 -4137
		mu 0 4 2410 311 2411 2409
		f 4 -884 887 4138 -4138
		mu 0 4 2411 523 1275 2409
		f 4 2305 -4130 4139 -4139
		mu 0 4 1275 519 2408 2409
		f 4 -442 -4142 4144 -4144
		mu 0 4 2413 275 2414 2412
		f 4 -890 893 4145 -4145
		mu 0 4 2414 526 2415 2412
		f 4 4142 -2313 4146 -4146
		mu 0 4 2415 524 1279 2412
		f 4 -513 516 4149 -4149
		mu 0 4 2158 317 1597 2416
		f 4 2822 894 4150 -4150
		mu 0 4 1597 527 2417 2416
		f 4 4147 -4143 4151 -4151
		mu 0 4 2417 524 2415 2416
		f 4 4152 -4154 4155 -4155
		mu 0 4 2419 316 2420 2418
		f 4 -892 895 4156 -4156
		mu 0 4 2420 528 1280 2418
		f 4 2313 -4148 4157 -4157
		mu 0 4 1280 524 2417 2418
		f 4 -455 -4160 4162 -4162
		mu 0 4 2422 282 2423 2421
		f 4 -898 901 4163 -4163
		mu 0 4 2423 531 2424 2421
		f 4 4160 -2321 4164 -4164
		mu 0 4 2424 529 1284 2421
		f 4 -521 524 4167 -4167
		mu 0 4 2248 322 1612 2425
		f 4 2846 902 4168 -4168
		mu 0 4 1612 532 2426 2425
		f 4 4165 -4161 4169 -4169
		mu 0 4 2426 529 2424 2425
		f 4 4170 -4172 4173 -4173
		mu 0 4 2428 321 2429 2427
		f 4 -900 903 4174 -4174
		mu 0 4 2429 533 1285 2427
		f 4 2321 -4166 4175 -4175
		mu 0 4 1285 529 2426 2427
		f 4 -468 -4178 4180 -4180
		mu 0 4 2431 289 2432 2430
		f 4 -906 909 4181 -4181
		mu 0 4 2432 536 2433 2430
		f 4 4178 -2329 4182 -4182
		mu 0 4 2433 534 1289 2430
		f 4 -529 532 4185 -4185
		mu 0 4 2338 327 1627 2434
		f 4 2870 910 4186 -4186
		mu 0 4 1627 537 2435 2434
		f 4 4183 -4179 4187 -4187
		mu 0 4 2435 534 2433 2434
		f 4 4188 -4190 4191 -4191
		mu 0 4 2437 326 2438 2436
		f 4 -908 911 4192 -4192
		mu 0 4 2438 538 1290 2436
		f 4 2329 -4184 4193 -4193
		mu 0 4 1290 534 2435 2436
		f 4 -480 -4196 4198 -4198
		mu 0 4 2440 245 2441 2439
		f 4 -914 917 4199 -4199
		mu 0 4 2441 541 2442 2439
		f 4 4196 -2337 4200 -4200
		mu 0 4 2442 539 1294 2439
		f 4 -537 540 4203 -4203
		mu 0 4 1708 332 1642 2443
		f 4 2894 918 4204 -4204
		mu 0 4 1642 542 2444 2443
		f 4 4201 -4197 4205 -4205
		mu 0 4 2444 539 2442 2443
		f 4 4206 -4208 4209 -4209
		mu 0 4 2446 331 2447 2445
		f 4 -916 919 4210 -4210
		mu 0 4 2447 543 1295 2445
		f 4 2337 -4202 4211 -4211
		mu 0 4 1295 539 2444 2445
		f 4 -384 -4214 4216 -4216
		mu 0 4 2449 241 2450 2448
		f 4 -922 925 4217 -4217
		mu 0 4 2450 546 2451 2448
		f 4 4214 -2345 4218 -4218
		mu 0 4 2451 544 1299 2448
		f 4 -545 547 4222 -4222
		mu 0 4 2453 337 1657 2452
		f 4 2918 926 4223 -4223
		mu 0 4 1657 547 2454 2452
		f 4 4220 -4215 4224 -4224
		mu 0 4 2454 544 2451 2452
		f 4 4225 -4227 4228 -4228
		mu 0 4 2456 336 2457 2455
		f 4 -924 927 4229 -4229
		mu 0 4 2457 548 1300 2455
		f 4 2345 -4221 4230 -4230
		mu 0 4 1300 544 2454 2455
		f 4 -385 -4233 4235 -4235
		mu 0 4 2459 242 2460 2458
		f 4 -930 933 4236 -4236
		mu 0 4 2460 551 2461 2458
		f 4 4233 -2353 4237 -4237
		mu 0 4 2461 549 1304 2458
		f 4 -552 554 4241 -4241
		mu 0 4 2463 341 1672 2462
		f 4 2942 934 4242 -4242
		mu 0 4 1672 552 2464 2462
		f 4 4239 -4234 4243 -4243
		mu 0 4 2464 549 2461 2462
		f 4 4244 -4246 4247 -4247
		mu 0 4 2466 340 2467 2465
		f 4 -932 935 4248 -4248
		mu 0 4 2467 553 1305 2465
		f 4 2353 -4240 4249 -4249
		mu 0 4 1305 549 2464 2465
		f 4 -386 -4252 4254 -4254
		mu 0 4 2469 243 2470 2468
		f 4 -938 941 4255 -4255
		mu 0 4 2470 556 2471 2468
		f 4 4252 -2361 4256 -4256
		mu 0 4 2471 554 1309 2468
		f 4 -559 561 4260 -4260
		mu 0 4 2473 345 1687 2472
		f 4 2966 942 4261 -4261
		mu 0 4 1687 557 2474 2472
		f 4 4258 -4253 4262 -4262
		mu 0 4 2474 554 2471 2472
		f 4 4263 -4265 4266 -4266
		mu 0 4 2476 344 2477 2475
		f 4 -940 943 4267 -4267
		mu 0 4 2477 558 1310 2475
		f 4 2361 -4259 4268 -4268
		mu 0 4 1310 554 2474 2475
		f 4 -387 -4271 4273 -4273
		mu 0 4 2479 244 2480 2478
		f 4 -946 949 4274 -4274
		mu 0 4 2480 561 2481 2478
		f 4 4271 -2369 4275 -4275
		mu 0 4 2481 559 1314 2478
		f 4 -566 567 4279 -4279
		mu 0 4 2483 349 1702 2482
		f 4 2990 950 4280 -4280
		mu 0 4 1702 562 2484 2482
		f 4 4277 -4272 4281 -4281
		mu 0 4 2484 559 2481 2482
		f 4 4282 -4284 4285 -4285
		mu 0 4 2486 348 2487 2485
		f 4 -948 951 4286 -4286
		mu 0 4 2487 563 1315 2485
		f 4 2369 -4278 4287 -4287
		mu 0 4 1315 559 2484 2485
		f 4 -388 380 4292 -4292
		mu 0 4 2489 245 2490 2488
		f 4 4289 957 4293 -4293
		mu 0 4 2490 566 2491 2488
		f 4 4290 -2377 4294 -4294
		mu 0 4 2491 564 1319 2488
		f 4 4053 574 4297 -4297
		mu 0 4 2368 352 1717 2492
		f 4 3014 958 4298 -4298
		mu 0 4 1717 567 2493 2492
		f 4 4295 -4291 4299 -4299
		mu 0 4 2493 564 2491 2492
		f 4 4300 -4302 4303 -4303
		mu 0 4 2495 351 2496 2494
		f 4 -956 959 4304 -4304
		mu 0 4 2496 568 1320 2494
		f 4 2377 -4296 4305 -4305
		mu 0 4 1320 564 2493 2494
		f 4 -389 381 4310 -4310
		mu 0 4 2498 246 2499 2497
		f 4 4307 965 4311 -4311
		mu 0 4 2499 571 2500 2497
		f 4 4308 -2385 4312 -4312
		mu 0 4 2500 569 1324 2497
		f 4 4313 580 4316 -4316
		mu 0 4 2502 356 1732 2501
		f 4 3038 966 4317 -4317
		mu 0 4 1732 572 2503 2501
		f 4 4314 -4309 4318 -4318
		mu 0 4 2503 569 2500 2501
		f 4 4319 -4321 4322 -4322
		mu 0 4 2505 355 2506 2504
		f 4 -964 967 4323 -4323
		mu 0 4 2506 573 1325 2504
		f 4 2385 -4315 4324 -4324
		mu 0 4 1325 569 2503 2504
		f 4 -397 -4327 4329 -4329
		mu 0 4 2508 249 2509 2507
		f 4 -970 973 4330 -4330
		mu 0 4 2509 576 2510 2507
		f 4 4327 -2393 4331 -4331
		mu 0 4 2510 574 1329 2507
		f 4 -585 587 4335 -4335
		mu 0 4 2512 359 1747 2511
		f 4 3062 974 4336 -4336
		mu 0 4 1747 577 2513 2511
		f 4 4333 -4328 4337 -4337
		mu 0 4 2513 574 2510 2511
		f 4 4338 -4340 4341 -4341
		mu 0 4 2515 358 2516 2514
		f 4 -972 975 4342 -4342
		mu 0 4 2516 578 1330 2514
		f 4 2393 -4334 4343 -4343
		mu 0 4 1330 574 2513 2514
		f 4 -398 -4346 4348 -4348
		mu 0 4 2518 250 2519 2517
		f 4 -978 981 4349 -4349
		mu 0 4 2519 581 2520 2517
		f 4 4346 -2401 4350 -4350
		mu 0 4 2520 579 1334 2517
		f 4 -592 594 4354 -4354
		mu 0 4 2522 363 1762 2521
		f 4 3086 982 4355 -4355
		mu 0 4 1762 582 2523 2521
		f 4 4352 -4347 4356 -4356
		mu 0 4 2523 579 2520 2521
		f 4 4357 -4359 4360 -4360
		mu 0 4 2525 362 2526 2524
		f 4 -980 983 4361 -4361
		mu 0 4 2526 583 1335 2524
		f 4 2401 -4353 4362 -4362
		mu 0 4 1335 579 2523 2524
		f 4 -399 -4365 4367 -4367
		mu 0 4 2528 251 2529 2527
		f 4 -986 989 4368 -4368
		mu 0 4 2529 586 2530 2527
		f 4 4365 -2409 4369 -4369
		mu 0 4 2530 584 1339 2527
		f 4 -599 601 4373 -4373
		mu 0 4 2532 367 1777 2531
		f 4 3110 990 4374 -4374
		mu 0 4 1777 587 2533 2531
		f 4 4371 -4366 4375 -4375
		mu 0 4 2533 584 2530 2531
		f 4 4376 -4378 4379 -4379
		mu 0 4 2535 366 2536 2534
		f 4 -988 991 4380 -4380
		mu 0 4 2536 588 1340 2534
		f 4 2409 -4372 4381 -4381
		mu 0 4 1340 584 2533 2534
		f 4 -400 -4384 4386 -4386
		mu 0 4 2538 252 2539 2537
		f 4 -994 997 4387 -4387
		mu 0 4 2539 591 2540 2537
		f 4 4384 -2417 4388 -4388
		mu 0 4 2540 589 1344 2537
		f 4 -606 607 4392 -4392
		mu 0 4 2542 371 1792 2541
		f 4 3134 998 4393 -4393
		mu 0 4 1792 592 2543 2541
		f 4 4390 -4385 4394 -4394
		mu 0 4 2543 589 2540 2541
		f 4 4395 -4397 4398 -4398
		mu 0 4 2545 370 2546 2544
		f 4 -996 999 4399 -4399
		mu 0 4 2546 593 1345 2544
		f 4 2417 -4391 4400 -4400
		mu 0 4 1345 589 2543 2544
		f 4 -401 382 4405 -4405
		mu 0 4 2548 247 2549 2547
		f 4 4402 1005 4406 -4406
		mu 0 4 2549 596 2550 2547
		f 4 4403 -2425 4407 -4407
		mu 0 4 2550 594 1349 2547
		f 4 3047 613 4410 -4410
		mu 0 4 1738 357 1807 2551
		f 4 3158 1006 4411 -4411
		mu 0 4 1807 597 2552 2551
		f 4 4408 -4404 4412 -4412
		mu 0 4 2552 594 2550 2551
		f 4 4413 -4415 4416 -4416
		mu 0 4 2554 373 2555 2553
		f 4 -1004 1007 4417 -4417
		mu 0 4 2555 598 1350 2553
		f 4 2425 -4409 4418 -4418
		mu 0 4 1350 594 2552 2553
		f 4 -402 394 4423 -4423
		mu 0 4 2557 253 2558 2556
		f 4 4420 1013 4424 -4424
		mu 0 4 2558 601 2559 2556
		f 4 4421 -2433 4425 -4425
		mu 0 4 2559 599 1354 2556
		f 4 4426 619 4429 -4429
		mu 0 4 2561 377 1822 2560
		f 4 3182 1014 4430 -4430
		mu 0 4 1822 602 2562 2560
		f 4 4427 -4422 4431 -4431
		mu 0 4 2562 599 2559 2560
		f 4 4432 -4434 4435 -4435
		mu 0 4 2564 376 2565 2563
		f 4 -1012 1015 4436 -4436
		mu 0 4 2565 603 1355 2563
		f 4 2433 -4428 4437 -4437
		mu 0 4 1355 599 2562 2563
		f 4 -410 -4440 4442 -4442
		mu 0 4 2567 256 2568 2566
		f 4 -1018 1021 4443 -4443
		mu 0 4 2568 606 2569 2566
		f 4 4440 -2441 4444 -4444
		mu 0 4 2569 604 1359 2566
		f 4 -624 626 4448 -4448
		mu 0 4 2571 380 1837 2570
		f 4 3206 1022 4449 -4449
		mu 0 4 1837 607 2572 2570
		f 4 4446 -4441 4450 -4450
		mu 0 4 2572 604 2569 2570
		f 4 4451 -4453 4454 -4454
		mu 0 4 2574 379 2575 2573
		f 4 -1020 1023 4455 -4455
		mu 0 4 2575 608 1360 2573
		f 4 2441 -4447 4456 -4456
		mu 0 4 1360 604 2572 2573
		f 4 -411 -4459 4461 -4461
		mu 0 4 2577 257 2578 2576
		f 4 -1026 1029 4462 -4462
		mu 0 4 2578 611 2579 2576
		f 4 4459 -2449 4463 -4463
		mu 0 4 2579 609 1364 2576
		f 4 -631 633 4467 -4467
		mu 0 4 2581 384 1852 2580
		f 4 3230 1030 4468 -4468
		mu 0 4 1852 612 2582 2580
		f 4 4465 -4460 4469 -4469
		mu 0 4 2582 609 2579 2580
		f 4 4470 -4472 4473 -4473
		mu 0 4 2584 383 2585 2583
		f 4 -1028 1031 4474 -4474
		mu 0 4 2585 613 1365 2583
		f 4 2449 -4466 4475 -4475
		mu 0 4 1365 609 2582 2583
		f 4 -412 -4478 4480 -4480
		mu 0 4 2587 258 2588 2586
		f 4 -1034 1037 4481 -4481
		mu 0 4 2588 616 2589 2586
		f 4 4478 -2457 4482 -4482
		mu 0 4 2589 614 1369 2586
		f 4 -638 640 4486 -4486
		mu 0 4 2591 388 1867 2590
		f 4 3254 1038 4487 -4487
		mu 0 4 1867 617 2592 2590
		f 4 4484 -4479 4488 -4488
		mu 0 4 2592 614 2589 2590
		f 4 4489 -4491 4492 -4492
		mu 0 4 2594 387 2595 2593
		f 4 -1036 1039 4493 -4493
		mu 0 4 2595 618 1370 2593
		f 4 2457 -4485 4494 -4494
		mu 0 4 1370 614 2592 2593
		f 4 -413 -4497 4499 -4499
		mu 0 4 2597 259 2598 2596
		f 4 -1042 1045 4500 -4500
		mu 0 4 2598 621 2599 2596
		f 4 4497 -2465 4501 -4501
		mu 0 4 2599 619 1374 2596
		f 4 -645 646 4505 -4505
		mu 0 4 2601 392 1882 2600
		f 4 3278 1046 4506 -4506
		mu 0 4 1882 622 2602 2600
		f 4 4503 -4498 4507 -4507
		mu 0 4 2602 619 2599 2600
		f 4 4508 -4510 4511 -4511
		mu 0 4 2604 391 2605 2603
		f 4 -1044 1047 4512 -4512
		mu 0 4 2605 623 1375 2603
		f 4 2465 -4504 4513 -4513
		mu 0 4 1375 619 2602 2603
		f 4 -414 395 4518 -4518
		mu 0 4 2607 254 2608 2606
		f 4 4515 1053 4519 -4519
		mu 0 4 2608 626 2609 2606
		f 4 4516 -2473 4520 -4520
		mu 0 4 2609 624 1379 2606
		f 4 3191 652 4523 -4523
		mu 0 4 1828 378 1897 2610
		f 4 3302 1054 4524 -4524
		mu 0 4 1897 627 2611 2610
		f 4 4521 -4517 4525 -4525
		mu 0 4 2611 624 2609 2610
		f 4 4526 -4528 4529 -4529
		mu 0 4 2613 394 2614 2612
		f 4 -1052 1055 4530 -4530
		mu 0 4 2614 628 1380 2612
		f 4 2473 -4522 4531 -4531
		mu 0 4 1380 624 2611 2612
		f 4 -415 407 4536 -4536
		mu 0 4 2616 260 2617 2615
		f 4 4533 1061 4537 -4537
		mu 0 4 2617 631 2618 2615
		f 4 4534 -2481 4538 -4538
		mu 0 4 2618 629 1384 2615
		f 4 4539 658 4542 -4542
		mu 0 4 2620 398 1912 2619
		f 4 3326 1062 4543 -4543
		mu 0 4 1912 632 2621 2619
		f 4 4540 -4535 4544 -4544
		mu 0 4 2621 629 2618 2619
		f 4 4545 -4547 4548 -4548
		mu 0 4 2623 397 2624 2622
		f 4 -1060 1063 4549 -4549
		mu 0 4 2624 633 1385 2622
		f 4 2481 -4541 4550 -4550
		mu 0 4 1385 629 2621 2622
		f 4 -423 -4553 4555 -4555
		mu 0 4 2626 263 2627 2625
		f 4 -1066 1069 4556 -4556
		mu 0 4 2627 636 2628 2625
		f 4 4553 -2489 4557 -4557
		mu 0 4 2628 634 1389 2625
		f 4 -663 665 4561 -4561
		mu 0 4 2630 401 1927 2629
		f 4 3350 1070 4562 -4562
		mu 0 4 1927 637 2631 2629
		f 4 4559 -4554 4563 -4563
		mu 0 4 2631 634 2628 2629
		f 4 4564 -4566 4567 -4567
		mu 0 4 2633 400 2634 2632
		f 4 -1068 1071 4568 -4568
		mu 0 4 2634 638 1390 2632
		f 4 2489 -4560 4569 -4569
		mu 0 4 1390 634 2631 2632
		f 4 -424 -4572 4574 -4574
		mu 0 4 2636 264 2637 2635
		f 4 -1074 1077 4575 -4575
		mu 0 4 2637 641 2638 2635
		f 4 4572 -2497 4576 -4576
		mu 0 4 2638 639 1394 2635
		f 4 -670 672 4580 -4580
		mu 0 4 2640 405 1942 2639
		f 4 3374 1078 4581 -4581
		mu 0 4 1942 642 2641 2639
		f 4 4578 -4573 4582 -4582
		mu 0 4 2641 639 2638 2639
		f 4 4583 -4585 4586 -4586
		mu 0 4 2643 404 2644 2642
		f 4 -1076 1079 4587 -4587
		mu 0 4 2644 643 1395 2642
		f 4 2497 -4579 4588 -4588
		mu 0 4 1395 639 2641 2642
		f 4 -425 -4591 4593 -4593
		mu 0 4 2646 265 2647 2645
		f 4 -1082 1085 4594 -4594
		mu 0 4 2647 646 2648 2645
		f 4 4591 -2505 4595 -4595
		mu 0 4 2648 644 1399 2645
		f 4 -677 679 4599 -4599
		mu 0 4 2650 409 1957 2649
		f 4 3398 1086 4600 -4600
		mu 0 4 1957 647 2651 2649
		f 4 4597 -4592 4601 -4601
		mu 0 4 2651 644 2648 2649
		f 4 4602 -4604 4605 -4605
		mu 0 4 2653 408 2654 2652
		f 4 -1084 1087 4606 -4606
		mu 0 4 2654 648 1400 2652;
	setAttr ".fc[2000:2499]"
		f 4 2505 -4598 4607 -4607
		mu 0 4 1400 644 2651 2652
		f 4 -426 -4610 4612 -4612
		mu 0 4 2656 266 2657 2655
		f 4 -1090 1093 4613 -4613
		mu 0 4 2657 651 2658 2655
		f 4 4610 -2513 4614 -4614
		mu 0 4 2658 649 1404 2655
		f 4 -684 685 4618 -4618
		mu 0 4 2660 413 1972 2659
		f 4 3422 1094 4619 -4619
		mu 0 4 1972 652 2661 2659
		f 4 4616 -4611 4620 -4620
		mu 0 4 2661 649 2658 2659
		f 4 4621 -4623 4624 -4624
		mu 0 4 2663 412 2664 2662
		f 4 -1092 1095 4625 -4625
		mu 0 4 2664 653 1405 2662
		f 4 2513 -4617 4626 -4626
		mu 0 4 1405 649 2661 2662
		f 4 -427 408 4631 -4631
		mu 0 4 2666 261 2667 2665
		f 4 4628 1101 4632 -4632
		mu 0 4 2667 656 2668 2665
		f 4 4629 -2521 4633 -4633
		mu 0 4 2668 654 1409 2665
		f 4 3335 691 4636 -4636
		mu 0 4 1918 399 1987 2669
		f 4 3446 1102 4637 -4637
		mu 0 4 1987 657 2670 2669
		f 4 4634 -4630 4638 -4638
		mu 0 4 2670 654 2668 2669
		f 4 4639 -4641 4642 -4642
		mu 0 4 2672 415 2673 2671
		f 4 -1100 1103 4643 -4643
		mu 0 4 2673 658 1410 2671
		f 4 2521 -4635 4644 -4644
		mu 0 4 1410 654 2670 2671
		f 4 -428 420 4649 -4649
		mu 0 4 2675 267 2676 2674
		f 4 4646 1109 4650 -4650
		mu 0 4 2676 661 2677 2674
		f 4 4647 -2529 4651 -4651
		mu 0 4 2677 659 1414 2674
		f 4 4652 697 4655 -4655
		mu 0 4 2679 419 2002 2678
		f 4 3470 1110 4656 -4656
		mu 0 4 2002 662 2680 2678
		f 4 4653 -4648 4657 -4657
		mu 0 4 2680 659 2677 2678
		f 4 4658 -4660 4661 -4661
		mu 0 4 2682 418 2683 2681
		f 4 -1108 1111 4662 -4662
		mu 0 4 2683 663 1415 2681
		f 4 2529 -4654 4663 -4663
		mu 0 4 1415 659 2680 2681
		f 4 -436 -4666 4668 -4668
		mu 0 4 2685 270 2686 2684
		f 4 -1114 1117 4669 -4669
		mu 0 4 2686 666 2687 2684
		f 4 4666 -2537 4670 -4670
		mu 0 4 2687 664 1419 2684
		f 4 -702 704 4674 -4674
		mu 0 4 2689 422 2017 2688
		f 4 3494 1118 4675 -4675
		mu 0 4 2017 667 2690 2688
		f 4 4672 -4667 4676 -4676
		mu 0 4 2690 664 2687 2688
		f 4 4677 -4679 4680 -4680
		mu 0 4 2692 421 2693 2691
		f 4 -1116 1119 4681 -4681
		mu 0 4 2693 668 1420 2691
		f 4 2537 -4673 4682 -4682
		mu 0 4 1420 664 2690 2691
		f 4 -437 -4685 4687 -4687
		mu 0 4 2695 271 2696 2694
		f 4 -1122 1125 4688 -4688
		mu 0 4 2696 671 2697 2694
		f 4 4685 -2545 4689 -4689
		mu 0 4 2697 669 1424 2694
		f 4 -709 711 4693 -4693
		mu 0 4 2699 426 2032 2698
		f 4 3518 1126 4694 -4694
		mu 0 4 2032 672 2700 2698
		f 4 4691 -4686 4695 -4695
		mu 0 4 2700 669 2697 2698
		f 4 4696 -4698 4699 -4699
		mu 0 4 2702 425 2703 2701
		f 4 -1124 1127 4700 -4700
		mu 0 4 2703 673 1425 2701
		f 4 2545 -4692 4701 -4701
		mu 0 4 1425 669 2700 2701
		f 4 -438 -4704 4706 -4706
		mu 0 4 2705 272 2706 2704
		f 4 -1130 1133 4707 -4707
		mu 0 4 2706 676 2707 2704
		f 4 4704 -2553 4708 -4708
		mu 0 4 2707 674 1429 2704
		f 4 -716 718 4712 -4712
		mu 0 4 2709 430 2047 2708
		f 4 3542 1134 4713 -4713
		mu 0 4 2047 677 2710 2708
		f 4 4710 -4705 4714 -4714
		mu 0 4 2710 674 2707 2708
		f 4 4715 -4717 4718 -4718
		mu 0 4 2712 429 2713 2711
		f 4 -1132 1135 4719 -4719
		mu 0 4 2713 678 1430 2711
		f 4 2553 -4711 4720 -4720
		mu 0 4 1430 674 2710 2711
		f 4 -439 -4723 4725 -4725
		mu 0 4 2715 273 2716 2714
		f 4 -1138 1141 4726 -4726
		mu 0 4 2716 681 2717 2714
		f 4 4723 -2561 4727 -4727
		mu 0 4 2717 679 1434 2714
		f 4 -723 724 4731 -4731
		mu 0 4 2719 434 2062 2718
		f 4 3566 1142 4732 -4732
		mu 0 4 2062 682 2720 2718
		f 4 4729 -4724 4733 -4733
		mu 0 4 2720 679 2717 2718
		f 4 4734 -4736 4737 -4737
		mu 0 4 2722 433 2723 2721
		f 4 -1140 1143 4738 -4738
		mu 0 4 2723 683 1435 2721
		f 4 2561 -4730 4739 -4739
		mu 0 4 1435 679 2720 2721
		f 4 -440 421 4744 -4744
		mu 0 4 2725 268 2726 2724
		f 4 4741 1149 4745 -4745
		mu 0 4 2726 686 2727 2724
		f 4 4742 -2569 4746 -4746
		mu 0 4 2727 684 1439 2724
		f 4 3479 730 4749 -4749
		mu 0 4 2008 420 2077 2728
		f 4 3590 1150 4750 -4750
		mu 0 4 2077 687 2729 2728
		f 4 4747 -4743 4751 -4751
		mu 0 4 2729 684 2727 2728
		f 4 4752 -4754 4755 -4755
		mu 0 4 2731 436 2732 2730
		f 4 -1148 1151 4756 -4756
		mu 0 4 2732 688 1440 2730
		f 4 2569 -4748 4757 -4757
		mu 0 4 1440 684 2729 2730
		f 4 -441 433 4762 -4762
		mu 0 4 2734 274 2735 2733
		f 4 4759 1157 4763 -4763
		mu 0 4 2735 691 2736 2733
		f 4 4760 -2577 4764 -4764
		mu 0 4 2736 689 1444 2733
		f 4 4765 736 4768 -4768
		mu 0 4 2738 440 2092 2737
		f 4 3614 1158 4769 -4769
		mu 0 4 2092 692 2739 2737
		f 4 4766 -4761 4770 -4770
		mu 0 4 2739 689 2736 2737
		f 4 4771 -4773 4774 -4774
		mu 0 4 2741 439 2742 2740
		f 4 -1156 1159 4775 -4775
		mu 0 4 2742 693 1445 2740
		f 4 2577 -4767 4776 -4776
		mu 0 4 1445 689 2739 2740
		f 4 -449 -4779 4781 -4781
		mu 0 4 2744 277 2745 2743
		f 4 -1162 1165 4782 -4782
		mu 0 4 2745 696 2746 2743
		f 4 4779 -2585 4783 -4783
		mu 0 4 2746 694 1449 2743
		f 4 -741 743 4787 -4787
		mu 0 4 2748 443 2107 2747
		f 4 3638 1166 4788 -4788
		mu 0 4 2107 697 2749 2747
		f 4 4785 -4780 4789 -4789
		mu 0 4 2749 694 2746 2747
		f 4 4790 -4792 4793 -4793
		mu 0 4 2751 442 2752 2750
		f 4 -1164 1167 4794 -4794
		mu 0 4 2752 698 1450 2750
		f 4 2585 -4786 4795 -4795
		mu 0 4 1450 694 2749 2750
		f 4 -450 -4798 4800 -4800
		mu 0 4 2754 278 2755 2753
		f 4 -1170 1173 4801 -4801
		mu 0 4 2755 701 2756 2753
		f 4 4798 -2593 4802 -4802
		mu 0 4 2756 699 1454 2753
		f 4 -748 750 4806 -4806
		mu 0 4 2758 447 2122 2757
		f 4 3662 1174 4807 -4807
		mu 0 4 2122 702 2759 2757
		f 4 4804 -4799 4808 -4808
		mu 0 4 2759 699 2756 2757
		f 4 4809 -4811 4812 -4812
		mu 0 4 2761 446 2762 2760
		f 4 -1172 1175 4813 -4813
		mu 0 4 2762 703 1455 2760
		f 4 2593 -4805 4814 -4814
		mu 0 4 1455 699 2759 2760
		f 4 -451 -4817 4819 -4819
		mu 0 4 2764 279 2765 2763
		f 4 -1178 1181 4820 -4820
		mu 0 4 2765 706 2766 2763
		f 4 4817 -2601 4821 -4821
		mu 0 4 2766 704 1459 2763
		f 4 -755 757 4825 -4825
		mu 0 4 2768 451 2137 2767
		f 4 3686 1182 4826 -4826
		mu 0 4 2137 707 2769 2767
		f 4 4823 -4818 4827 -4827
		mu 0 4 2769 704 2766 2767
		f 4 4828 -4830 4831 -4831
		mu 0 4 2771 450 2772 2770
		f 4 -1180 1183 4832 -4832
		mu 0 4 2772 708 1460 2770
		f 4 2601 -4824 4833 -4833
		mu 0 4 1460 704 2769 2770
		f 4 -452 -4836 4838 -4838
		mu 0 4 2774 280 2775 2773
		f 4 -1186 1189 4839 -4839
		mu 0 4 2775 711 2776 2773
		f 4 4836 -2609 4840 -4840
		mu 0 4 2776 709 1464 2773
		f 4 -762 763 4844 -4844
		mu 0 4 2778 455 2152 2777
		f 4 3710 1190 4845 -4845
		mu 0 4 2152 712 2779 2777
		f 4 4842 -4837 4846 -4846
		mu 0 4 2779 709 2776 2777
		f 4 4847 -4849 4850 -4850
		mu 0 4 2781 454 2782 2780
		f 4 -1188 1191 4851 -4851
		mu 0 4 2782 713 1465 2780
		f 4 2609 -4843 4852 -4852
		mu 0 4 1465 709 2779 2780
		f 4 -453 434 4857 -4857
		mu 0 4 2784 275 2785 2783
		f 4 4854 1197 4858 -4858
		mu 0 4 2785 716 2786 2783
		f 4 4855 -2617 4859 -4859
		mu 0 4 2786 714 1469 2783
		f 4 3623 769 4862 -4862
		mu 0 4 2098 441 2167 2787
		f 4 3734 1198 4863 -4863
		mu 0 4 2167 717 2788 2787
		f 4 4860 -4856 4864 -4864
		mu 0 4 2788 714 2786 2787
		f 4 4865 -4867 4868 -4868
		mu 0 4 2790 457 2791 2789
		f 4 -1196 1199 4869 -4869
		mu 0 4 2791 718 1470 2789
		f 4 2617 -4861 4870 -4870
		mu 0 4 1470 714 2788 2789
		f 4 -454 446 4875 -4875
		mu 0 4 2793 281 2794 2792
		f 4 4872 1205 4876 -4876
		mu 0 4 2794 721 2795 2792
		f 4 4873 -2625 4877 -4877
		mu 0 4 2795 719 1474 2792
		f 4 4878 775 4881 -4881
		mu 0 4 2797 461 2182 2796
		f 4 3758 1206 4882 -4882
		mu 0 4 2182 722 2798 2796
		f 4 4879 -4874 4883 -4883
		mu 0 4 2798 719 2795 2796
		f 4 4884 -4886 4887 -4887
		mu 0 4 2800 460 2801 2799
		f 4 -1204 1207 4888 -4888
		mu 0 4 2801 723 1475 2799
		f 4 2625 -4880 4889 -4889
		mu 0 4 1475 719 2798 2799
		f 4 -462 -4892 4894 -4894
		mu 0 4 2803 284 2804 2802
		f 4 -1210 1213 4895 -4895
		mu 0 4 2804 726 2805 2802
		f 4 4892 -2633 4896 -4896
		mu 0 4 2805 724 1479 2802
		f 4 -780 782 4900 -4900
		mu 0 4 2807 464 2197 2806
		f 4 3782 1214 4901 -4901
		mu 0 4 2197 727 2808 2806
		f 4 4898 -4893 4902 -4902
		mu 0 4 2808 724 2805 2806
		f 4 4903 -4905 4906 -4906
		mu 0 4 2810 463 2811 2809
		f 4 -1212 1215 4907 -4907
		mu 0 4 2811 728 1480 2809
		f 4 2633 -4899 4908 -4908
		mu 0 4 1480 724 2808 2809
		f 4 -463 -4911 4913 -4913
		mu 0 4 2813 285 2814 2812
		f 4 -1218 1221 4914 -4914
		mu 0 4 2814 731 2815 2812
		f 4 4911 -2641 4915 -4915
		mu 0 4 2815 729 1484 2812
		f 4 -787 789 4919 -4919
		mu 0 4 2817 468 2212 2816
		f 4 3806 1222 4920 -4920
		mu 0 4 2212 732 2818 2816
		f 4 4917 -4912 4921 -4921
		mu 0 4 2818 729 2815 2816
		f 4 4922 -4924 4925 -4925
		mu 0 4 2820 467 2821 2819
		f 4 -1220 1223 4926 -4926
		mu 0 4 2821 733 1485 2819
		f 4 2641 -4918 4927 -4927
		mu 0 4 1485 729 2818 2819
		f 4 -464 -4930 4932 -4932
		mu 0 4 2823 286 2824 2822
		f 4 -1226 1229 4933 -4933
		mu 0 4 2824 736 2825 2822
		f 4 4930 -2649 4934 -4934
		mu 0 4 2825 734 1489 2822
		f 4 -794 796 4938 -4938
		mu 0 4 2827 472 2227 2826
		f 4 3830 1230 4939 -4939
		mu 0 4 2227 737 2828 2826
		f 4 4936 -4931 4940 -4940
		mu 0 4 2828 734 2825 2826
		f 4 4941 -4943 4944 -4944
		mu 0 4 2830 471 2831 2829
		f 4 -1228 1231 4945 -4945
		mu 0 4 2831 738 1490 2829
		f 4 2649 -4937 4946 -4946
		mu 0 4 1490 734 2828 2829
		f 4 -465 -4949 4951 -4951
		mu 0 4 2833 287 2834 2832
		f 4 -1234 1237 4952 -4952
		mu 0 4 2834 741 2835 2832
		f 4 4949 -2657 4953 -4953
		mu 0 4 2835 739 1494 2832
		f 4 -801 802 4957 -4957
		mu 0 4 2837 476 2242 2836
		f 4 3854 1238 4958 -4958
		mu 0 4 2242 742 2838 2836
		f 4 4955 -4950 4959 -4959
		mu 0 4 2838 739 2835 2836
		f 4 4960 -4962 4963 -4963
		mu 0 4 2840 475 2841 2839
		f 4 -1236 1239 4964 -4964
		mu 0 4 2841 743 1495 2839
		f 4 2657 -4956 4965 -4965
		mu 0 4 1495 739 2838 2839
		f 4 -466 447 4970 -4970
		mu 0 4 2843 282 2844 2842
		f 4 4967 1245 4971 -4971
		mu 0 4 2844 746 2845 2842
		f 4 4968 -2665 4972 -4972
		mu 0 4 2845 744 1499 2842
		f 4 3767 808 4975 -4975
		mu 0 4 2188 462 2257 2846
		f 4 3878 1246 4976 -4976
		mu 0 4 2257 747 2847 2846
		f 4 4973 -4969 4977 -4977
		mu 0 4 2847 744 2845 2846
		f 4 4978 -4980 4981 -4981
		mu 0 4 2849 478 2850 2848
		f 4 -1244 1247 4982 -4982
		mu 0 4 2850 748 1500 2848
		f 4 2665 -4974 4983 -4983
		mu 0 4 1500 744 2847 2848
		f 4 -467 459 4988 -4988
		mu 0 4 2852 288 2853 2851
		f 4 4985 1253 4989 -4989
		mu 0 4 2853 751 2854 2851
		f 4 4986 -2673 4990 -4990
		mu 0 4 2854 749 1504 2851
		f 4 4991 814 4994 -4994
		mu 0 4 2856 482 2272 2855
		f 4 3902 1254 4995 -4995
		mu 0 4 2272 752 2857 2855
		f 4 4992 -4987 4996 -4996
		mu 0 4 2857 749 2854 2855
		f 4 4997 -4999 5000 -5000
		mu 0 4 2859 481 2860 2858
		f 4 -1252 1255 5001 -5001
		mu 0 4 2860 753 1505 2858
		f 4 2673 -4993 5002 -5002
		mu 0 4 1505 749 2857 2858
		f 4 -474 -5005 5007 -5007
		mu 0 4 2862 291 2863 2861
		f 4 -1258 1261 5008 -5008
		mu 0 4 2863 756 2864 2861
		f 4 5005 -2681 5009 -5009
		mu 0 4 2864 754 1509 2861
		f 4 -819 821 5013 -5013
		mu 0 4 2866 485 2287 2865
		f 4 3924 1262 5014 -5014
		mu 0 4 2287 757 2867 2865
		f 4 5011 -5006 5015 -5015
		mu 0 4 2867 754 2864 2865
		f 4 5016 -5018 5019 -5019
		mu 0 4 2869 484 2870 2868
		f 4 -1260 1263 5020 -5020
		mu 0 4 2870 758 1510 2868
		f 4 2681 -5012 5021 -5021
		mu 0 4 1510 754 2867 2868
		f 4 -475 -5024 5026 -5026
		mu 0 4 2872 292 2873 2871
		f 4 -1266 1269 5027 -5027
		mu 0 4 2873 761 2874 2871
		f 4 5024 -2689 5028 -5028
		mu 0 4 2874 759 1514 2871
		f 4 -826 828 5032 -5032
		mu 0 4 2876 489 2302 2875
		f 4 3948 1270 5033 -5033
		mu 0 4 2302 762 2877 2875
		f 4 5030 -5025 5034 -5034
		mu 0 4 2877 759 2874 2875
		f 4 5035 -5037 5038 -5038
		mu 0 4 2879 488 2880 2878
		f 4 -1268 1271 5039 -5039
		mu 0 4 2880 763 1515 2878
		f 4 2689 -5031 5040 -5040
		mu 0 4 1515 759 2877 2878
		f 4 -476 -5043 5045 -5045
		mu 0 4 2882 293 2883 2881
		f 4 -1274 1277 5046 -5046
		mu 0 4 2883 766 2884 2881
		f 4 5043 -2697 5047 -5047
		mu 0 4 2884 764 1519 2881
		f 4 -833 835 5051 -5051
		mu 0 4 2886 493 2317 2885
		f 4 3972 1278 5052 -5052
		mu 0 4 2317 767 2887 2885
		f 4 5049 -5044 5053 -5053
		mu 0 4 2887 764 2884 2885
		f 4 5054 -5056 5057 -5057
		mu 0 4 2889 492 2890 2888
		f 4 -1276 1279 5058 -5058
		mu 0 4 2890 768 1520 2888
		f 4 2697 -5050 5059 -5059
		mu 0 4 1520 764 2887 2888
		f 4 -477 -5062 5064 -5064
		mu 0 4 2892 294 2893 2891
		f 4 -1282 1285 5065 -5065
		mu 0 4 2893 771 2894 2891
		f 4 5062 -2705 5066 -5066
		mu 0 4 2894 769 1524 2891
		f 4 -840 841 5070 -5070
		mu 0 4 2896 497 2332 2895
		f 4 3996 1286 5071 -5071
		mu 0 4 2332 772 2897 2895
		f 4 5068 -5063 5072 -5072
		mu 0 4 2897 769 2894 2895
		f 4 5073 -5075 5076 -5076
		mu 0 4 2899 496 2900 2898
		f 4 -1284 1287 5077 -5077
		mu 0 4 2900 773 1525 2898
		f 4 2705 -5069 5078 -5078
		mu 0 4 1525 769 2897 2898
		f 4 -478 460 5083 -5083
		mu 0 4 2902 289 2903 2901
		f 4 5080 1292 5084 -5084
		mu 0 4 2903 776 2904 2901
		f 4 5081 -2713 5085 -5085
		mu 0 4 2904 774 1529 2901
		f 4 3911 847 5087 -5087
		mu 0 4 2278 483 2347 2905
		f 4 4020 1293 5088 -5088
		mu 0 4 2347 777 2906 2905
		f 4 7834 -7836 7813 -7837
		mu 0 4 4117 4116 4104 4107
		f 4 5089 -5091 5092 -5092
		mu 0 4 2908 499 2909 2907
		f 4 -7840 7841 7843 -7845
		mu 0 4 4118 4119 4120 4121
		f 4 7845 -7835 7846 -7844
		mu 0 4 4120 4116 4117 4121
		f 4 -479 472 5098 -5098
		mu 0 4 2911 295 2912 2910
		f 4 5095 1300 5099 -5099
		mu 0 4 2912 781 2913 2910
		f 4 5096 -2721 5100 -5100
		mu 0 4 2913 779 1534 2910
		f 4 5101 852 5104 -5104
		mu 0 4 2915 503 2362 2914
		f 4 4044 1301 5105 -5105
		mu 0 4 2362 782 2916 2914
		f 4 5102 -5097 5106 -5106
		mu 0 4 2916 779 2913 2914
		f 4 5107 -5109 5110 -5110
		mu 0 4 2918 502 2919 2917
		f 4 -1299 1302 5111 -5111
		mu 0 4 2919 783 1535 2917
		f 4 2721 -5103 5112 -5112
		mu 0 4 1535 779 2916 2917
		f 4 -50 -5115 5117 -5117
		mu 0 4 2921 36 2922 2920
		f 4 -1305 1307 5118 -5118
		mu 0 4 2922 786 2923 2920
		f 4 5115 -2729 5119 -5119
		mu 0 4 2923 784 1539 2920
		f 4 -482 485 5123 -5123
		mu 0 4 2925 298 1542 2924
		f 4 2734 1308 5124 -5124
		mu 0 4 1542 787 2926 2924
		f 4 5121 -5116 5125 -5125
		mu 0 4 2926 784 2923 2924
		f 4 5126 -4081 5128 -5128
		mu 0 4 2928 296 2383 2927
		f 4 -859 1309 5129 -5129
		mu 0 4 2383 507 1540 2927
		f 4 2729 -5122 5130 -5130
		mu 0 4 1540 784 2926 2927
		f 4 -377 383 5134 -5134
		mu 0 4 2930 241 2449 2929
		f 4 4212 1313 5135 -5135
		mu 0 4 2449 545 2931 2929
		f 4 5132 -2737 5136 -5136
		mu 0 4 2931 788 1544 2929
		f 4 2343 486 5139 -5139
		mu 0 4 1298 299 1547 2932
		f 4 2742 1314 5140 -5140
		mu 0 4 1547 790 2933 2932
		f 4 5137 -5133 5141 -5141
		mu 0 4 2933 788 2931 2932
		f 4 5142 -5127 5144 -5144
		mu 0 4 2935 296 2928 2934
		f 4 -1306 1315 5145 -5145
		mu 0 4 2928 787 1545 2934
		f 4 2737 -5138 5146 -5146
		mu 0 4 1545 788 2933 2934
		f 4 5147 -5149 5151 -5151
		mu 0 4 2937 240 2938 2936
		f 4 -1318 1319 5152 -5152
		mu 0 4 2938 793 2939 2936
		f 4 5149 -2745 5153 -5153
		mu 0 4 2939 791 1549 2936
		f 4 -484 487 5156 -5156
		mu 0 4 1743 300 1257 2940
		f 4 2278 1320 5157 -5157
		mu 0 4 1257 508 2941 2940
		f 4 5154 -5150 5158 -5158
		mu 0 4 2941 791 2939 2940
		f 4 4081 -5143 5160 -5160
		mu 0 4 2384 296 2935 2942
		f 4 -1312 1321 5161 -5161
		mu 0 4 2935 790 1550 2942
		f 4 2745 -5155 5162 -5162
		mu 0 4 1550 791 2941 2942
		f 4 -54 -5165 5167 -5167
		mu 0 4 2944 38 2945 2943
		f 4 -1324 1326 5168 -5168
		mu 0 4 2945 796 2946 2943
		f 4 5165 -2753 5169 -5169
		mu 0 4 2946 794 1554 2943
		f 4 -490 493 5173 -5173
		mu 0 4 2948 303 1557 2947
		f 4 2758 1327 5174 -5174
		mu 0 4 1557 797 2949 2947
		f 4 5171 -5166 5175 -5175
		mu 0 4 2949 794 2946 2947
		f 4 5176 -4099 5178 -5178
		mu 0 4 2951 301 2392 2950
		f 4 -867 1328 5179 -5179
		mu 0 4 2392 512 1555 2950
		f 4 2753 -5172 5180 -5180
		mu 0 4 1555 794 2949 2950
		f 4 -391 396 5184 -5184
		mu 0 4 2953 249 2508 2952
		f 4 4325 1332 5185 -5185
		mu 0 4 2508 575 2954 2952
		f 4 5182 -2761 5186 -5186
		mu 0 4 2954 798 1559 2952
		f 4 2391 494 5189 -5189
		mu 0 4 1328 304 1562 2955
		f 4 2766 1333 5190 -5190
		mu 0 4 1562 800 2956 2955
		f 4 5187 -5183 5191 -5191
		mu 0 4 2956 798 2954 2955
		f 4 5192 -5177 5194 -5194
		mu 0 4 2958 301 2951 2957
		f 4 -1325 1334 5195 -5195
		mu 0 4 2951 797 1560 2957
		f 4 2761 -5188 5196 -5196
		mu 0 4 1560 798 2956 2957
		f 4 5197 -5199 5201 -5201
		mu 0 4 2960 248 2961 2959
		f 4 -1337 1338 5202 -5202
		mu 0 4 2961 803 2962 2959
		f 4 5199 -2769 5203 -5203
		mu 0 4 2962 801 1564 2959
		f 4 -492 495 5206 -5206
		mu 0 4 1833 305 1262 2963
		f 4 2286 1339 5207 -5207
		mu 0 4 1262 513 2964 2963
		f 4 5204 -5200 5208 -5208
		mu 0 4 2964 801 2962 2963
		f 4 4099 -5193 5210 -5210
		mu 0 4 2393 301 2958 2965
		f 4 -1331 1340 5211 -5211
		mu 0 4 2958 800 1565 2965
		f 4 2769 -5205 5212 -5212
		mu 0 4 1565 801 2964 2965
		f 4 -57 -5215 5217 -5217
		mu 0 4 2967 39 2968 2966
		f 4 -1343 1345 5218 -5218
		mu 0 4 2968 806 2969 2966
		f 4 5215 -2777 5219 -5219
		mu 0 4 2969 804 1569 2966
		f 4 -498 501 5223 -5223
		mu 0 4 2971 308 1572 2970
		f 4 2782 1346 5224 -5224
		mu 0 4 1572 807 2972 2970
		f 4 5221 -5216 5225 -5225
		mu 0 4 2972 804 2969 2970
		f 4 5226 -4117 5228 -5228
		mu 0 4 2974 306 2401 2973
		f 4 -875 1347 5229 -5229
		mu 0 4 2401 517 1570 2973
		f 4 2777 -5222 5230 -5230
		mu 0 4 1570 804 2972 2973
		f 4 -404 409 5234 -5234
		mu 0 4 2976 256 2567 2975
		f 4 4438 1351 5235 -5235
		mu 0 4 2567 605 2977 2975
		f 4 5232 -2785 5236 -5236
		mu 0 4 2977 808 1574 2975
		f 4 2439 502 5239 -5239
		mu 0 4 1358 309 1577 2978
		f 4 2790 1352 5240 -5240
		mu 0 4 1577 810 2979 2978
		f 4 5237 -5233 5241 -5241
		mu 0 4 2979 808 2977 2978
		f 4 5242 -5227 5244 -5244
		mu 0 4 2981 306 2974 2980
		f 4 -1344 1353 5245 -5245
		mu 0 4 2974 807 1575 2980
		f 4 2785 -5238 5246 -5246
		mu 0 4 1575 808 2979 2980
		f 4 5247 -5249 5251 -5251
		mu 0 4 2983 255 2984 2982
		f 4 -1356 1357 5252 -5252
		mu 0 4 2984 813 2985 2982
		f 4 5249 -2793 5253 -5253
		mu 0 4 2985 811 1579 2982
		f 4 -500 503 5256 -5256
		mu 0 4 1923 310 1267 2986
		f 4 2294 1358 5257 -5257
		mu 0 4 1267 518 2987 2986
		f 4 5254 -5250 5258 -5258
		mu 0 4 2987 811 2985 2986
		f 4 4117 -5243 5260 -5260
		mu 0 4 2402 306 2981 2988
		f 4 -1350 1359 5261 -5261
		mu 0 4 2981 810 1580 2988
		f 4 2793 -5255 5262 -5262
		mu 0 4 1580 811 2987 2988
		f 4 -60 -5265 5267 -5267
		mu 0 4 2990 40 2991 2989
		f 4 -1362 1364 5268 -5268
		mu 0 4 2991 816 2992 2989
		f 4 5265 -2801 5269 -5269
		mu 0 4 2992 814 1584 2989
		f 4 -506 509 5273 -5273
		mu 0 4 2994 313 1587 2993
		f 4 2806 1365 5274 -5274
		mu 0 4 1587 817 2995 2993
		f 4 5271 -5266 5275 -5275
		mu 0 4 2995 814 2992 2993
		f 4 5276 -4135 5278 -5278
		mu 0 4 2997 311 2410 2996
		f 4 -883 1366 5279 -5279
		mu 0 4 2410 522 1585 2996
		f 4 2801 -5272 5280 -5280
		mu 0 4 1585 814 2995 2996
		f 4 -417 422 5284 -5284
		mu 0 4 2999 263 2626 2998
		f 4 4551 1370 5285 -5285
		mu 0 4 2626 635 3000 2998
		f 4 5282 -2809 5286 -5286
		mu 0 4 3000 818 1589 2998
		f 4 2487 510 5289 -5289
		mu 0 4 1388 314 1592 3001
		f 4 2814 1371 5290 -5290
		mu 0 4 1592 820 3002 3001
		f 4 5287 -5283 5291 -5291
		mu 0 4 3002 818 3000 3001
		f 4 5292 -5277 5294 -5294
		mu 0 4 3004 311 2997 3003
		f 4 -1363 1372 5295 -5295
		mu 0 4 2997 817 1590 3003
		f 4 2809 -5288 5296 -5296
		mu 0 4 1590 818 3002 3003
		f 4 5297 -5299 5301 -5301
		mu 0 4 3006 262 3007 3005
		f 4 -1375 1376 5302 -5302
		mu 0 4 3007 823 3008 3005
		f 4 5299 -2817 5303 -5303
		mu 0 4 3008 821 1594 3005
		f 4 -508 511 5306 -5306
		mu 0 4 2013 315 1272 3009
		f 4 2302 1377 5307 -5307
		mu 0 4 1272 523 3010 3009
		f 4 5304 -5300 5308 -5308
		mu 0 4 3010 821 3008 3009
		f 4 4135 -5293 5310 -5310
		mu 0 4 2411 311 3004 3011
		f 4 -1369 1378 5311 -5311
		mu 0 4 3004 820 1595 3011
		f 4 2817 -5305 5312 -5312
		mu 0 4 1595 821 3010 3011
		f 4 -63 -5315 5317 -5317
		mu 0 4 3013 41 3014 3012
		f 4 -1381 1383 5318 -5318
		mu 0 4 3014 826 3015 3012
		f 4 5315 -2825 5319 -5319
		mu 0 4 3015 824 1599 3012
		f 4 -514 517 5323 -5323
		mu 0 4 3017 318 1602 3016
		f 4 2830 1384 5324 -5324
		mu 0 4 1602 827 3018 3016
		f 4 5321 -5316 5325 -5325
		mu 0 4 3018 824 3015 3016
		f 4 5326 -4153 5328 -5328
		mu 0 4 3020 316 2419 3019
		f 4 -891 1385 5329 -5329
		mu 0 4 2419 527 1600 3019
		f 4 2825 -5322 5330 -5330
		mu 0 4 1600 824 3018 3019
		f 4 -430 435 5334 -5334
		mu 0 4 3022 270 2685 3021
		f 4 4664 1389 5335 -5335
		mu 0 4 2685 665 3023 3021
		f 4 5332 -2833 5336 -5336
		mu 0 4 3023 828 1604 3021
		f 4 2535 518 5339 -5339
		mu 0 4 1418 319 1607 3024
		f 4 2838 1390 5340 -5340
		mu 0 4 1607 830 3025 3024
		f 4 5337 -5333 5341 -5341
		mu 0 4 3025 828 3023 3024
		f 4 5342 -5327 5344 -5344
		mu 0 4 3027 316 3020 3026
		f 4 -1382 1391 5345 -5345
		mu 0 4 3020 827 1605 3026
		f 4 2833 -5338 5346 -5346
		mu 0 4 1605 828 3025 3026
		f 4 5347 -5349 5351 -5351
		mu 0 4 3029 269 3030 3028
		f 4 -1394 1395 5352 -5352
		mu 0 4 3030 833 3031 3028
		f 4 5349 -2841 5353 -5353
		mu 0 4 3031 831 1609 3028
		f 4 -516 519 5356 -5356
		mu 0 4 2103 320 1277 3032
		f 4 2310 1396 5357 -5357
		mu 0 4 1277 528 3033 3032
		f 4 5354 -5350 5358 -5358
		mu 0 4 3033 831 3031 3032
		f 4 4153 -5343 5360 -5360
		mu 0 4 2420 316 3027 3034
		f 4 -1388 1397 5361 -5361
		mu 0 4 3027 830 1610 3034
		f 4 2841 -5355 5362 -5362
		mu 0 4 1610 831 3033 3034
		f 4 -66 -5365 5367 -5367
		mu 0 4 3036 42 3037 3035
		f 4 -1400 1402 5368 -5368
		mu 0 4 3037 836 3038 3035
		f 4 5365 -2849 5369 -5369
		mu 0 4 3038 834 1614 3035
		f 4 -522 525 5373 -5373
		mu 0 4 3040 323 1617 3039
		f 4 2854 1403 5374 -5374
		mu 0 4 1617 837 3041 3039
		f 4 5371 -5366 5375 -5375
		mu 0 4 3041 834 3038 3039
		f 4 5376 -4171 5378 -5378
		mu 0 4 3043 321 2428 3042
		f 4 -899 1404 5379 -5379
		mu 0 4 2428 532 1615 3042
		f 4 2849 -5372 5380 -5380
		mu 0 4 1615 834 3041 3042
		f 4 -443 448 5384 -5384
		mu 0 4 3045 277 2744 3044
		f 4 4777 1408 5385 -5385
		mu 0 4 2744 695 3046 3044
		f 4 5382 -2857 5386 -5386
		mu 0 4 3046 838 1619 3044
		f 4 2583 526 5389 -5389
		mu 0 4 1448 324 1622 3047
		f 4 2862 1409 5390 -5390
		mu 0 4 1622 840 3048 3047
		f 4 5387 -5383 5391 -5391
		mu 0 4 3048 838 3046 3047
		f 4 5392 -5377 5394 -5394
		mu 0 4 3050 321 3043 3049
		f 4 -1401 1410 5395 -5395
		mu 0 4 3043 837 1620 3049
		f 4 2857 -5388 5396 -5396
		mu 0 4 1620 838 3048 3049
		f 4 5397 -5399 5401 -5401
		mu 0 4 3052 276 3053 3051
		f 4 -1413 1414 5402 -5402
		mu 0 4 3053 843 3054 3051
		f 4 5399 -2865 5403 -5403
		mu 0 4 3054 841 1624 3051
		f 4 -524 527 5406 -5406
		mu 0 4 2193 325 1282 3055
		f 4 2318 1415 5407 -5407
		mu 0 4 1282 533 3056 3055
		f 4 5404 -5400 5408 -5408
		mu 0 4 3056 841 3054 3055
		f 4 4171 -5393 5410 -5410
		mu 0 4 2429 321 3050 3057
		f 4 -1407 1416 5411 -5411
		mu 0 4 3050 840 1625 3057
		f 4 2865 -5405 5412 -5412
		mu 0 4 1625 841 3056 3057
		f 4 -69 -5415 5417 -5417
		mu 0 4 3059 43 3060 3058
		f 4 -1419 1421 5418 -5418
		mu 0 4 3060 846 3061 3058
		f 4 5415 -2873 5419 -5419
		mu 0 4 3061 844 1629 3058
		f 4 -530 533 5423 -5423
		mu 0 4 3063 328 1632 3062
		f 4 2878 1422 5424 -5424
		mu 0 4 1632 847 3064 3062
		f 4 5421 -5416 5425 -5425
		mu 0 4 3064 844 3061 3062
		f 4 5426 -4189 5428 -5428
		mu 0 4 3066 326 2437 3065
		f 4 -907 1423 5429 -5429
		mu 0 4 2437 537 1630 3065
		f 4 2873 -5422 5430 -5430
		mu 0 4 1630 844 3064 3065
		f 4 -456 461 5434 -5434
		mu 0 4 3068 284 2803 3067
		f 4 4890 1427 5435 -5435
		mu 0 4 2803 725 3069 3067
		f 4 5432 -2881 5436 -5436
		mu 0 4 3069 848 1634 3067
		f 4 2631 534 5439 -5439
		mu 0 4 1478 329 1637 3070
		f 4 2886 1428 5440 -5440
		mu 0 4 1637 850 3071 3070
		f 4 5437 -5433 5441 -5441
		mu 0 4 3071 848 3069 3070
		f 4 5442 -5427 5444 -5444
		mu 0 4 3073 326 3066 3072
		f 4 -1420 1429 5445 -5445
		mu 0 4 3066 847 1635 3072
		f 4 2881 -5438 5446 -5446
		mu 0 4 1635 848 3071 3072
		f 4 5447 -5449 5451 -5451
		mu 0 4 3075 283 3076 3074
		f 4 -1432 1433 5452 -5452
		mu 0 4 3076 853 3077 3074
		f 4 5449 -2889 5453 -5453
		mu 0 4 3077 851 1639 3074
		f 4 -532 535 5456 -5456
		mu 0 4 2283 330 1287 3078
		f 4 2326 1434 5457 -5457
		mu 0 4 1287 538 3079 3078
		f 4 5454 -5450 5458 -5458
		mu 0 4 3079 851 3077 3078
		f 4 4189 -5443 5460 -5460
		mu 0 4 2438 326 3073 3080
		f 4 -1426 1435 5461 -5461
		mu 0 4 3073 850 1640 3080
		f 4 2889 -5455 5462 -5462
		mu 0 4 1640 851 3079 3080
		f 4 -49 -5465 5467 -5467
		mu 0 4 3082 37 3083 3081
		f 4 -1438 1440 5468 -5468
		mu 0 4 3083 856 3084 3081
		f 4 5465 -2897 5469 -5469
		mu 0 4 3084 854 1644 3081
		f 4 -538 541 5473 -5473
		mu 0 4 3086 333 1647 3085
		f 4 2902 1441 5474 -5474
		mu 0 4 1647 857 3087 3085
		f 4 5471 -5466 5475 -5475
		mu 0 4 3087 854 3084 3085
		f 4 5476 -4207 5478 -5478
		mu 0 4 3089 331 2446 3088
		f 4 -915 1442 5479 -5479
		mu 0 4 2446 542 1645 3088
		f 4 2897 -5472 5480 -5480
		mu 0 4 1645 854 3087 3088
		f 4 -469 473 5484 -5484
		mu 0 4 3091 291 2862 3090
		f 4 5003 1446 5485 -5485
		mu 0 4 2862 755 3092 3090
		f 4 5482 -2905 5486 -5486
		mu 0 4 3092 858 1649 3090
		f 4 2679 542 5489 -5489
		mu 0 4 1508 334 1652 3093
		f 4 2910 1447 5490 -5490
		mu 0 4 1652 860 3094 3093
		f 4 5487 -5483 5491 -5491
		mu 0 4 3094 858 3092 3093
		f 4 5492 -5477 5494 -5494
		mu 0 4 3096 331 3089 3095
		f 4 -1439 1448 5495 -5495
		mu 0 4 3089 857 1650 3095
		f 4 2905 -5488 5496 -5496
		mu 0 4 1650 858 3094 3095
		f 4 5497 -5499 5501 -5501
		mu 0 4 3098 290 3099 3097
		f 4 -1451 1452 5502 -5502
		mu 0 4 3099 863 3100 3097
		f 4 5499 -2913 5503 -5503
		mu 0 4 3100 861 1654 3097
		f 4 -540 543 5506 -5506
		mu 0 4 2373 335 1292 3101
		f 4 2334 1453 5507 -5507
		mu 0 4 1292 543 3102 3101
		f 4 5504 -5500 5508 -5508
		mu 0 4 3102 861 3100 3101
		f 4 4207 -5493 5510 -5510
		mu 0 4 2447 331 3096 3103
		f 4 -1445 1454 5511 -5511
		mu 0 4 3096 860 1655 3103
		f 4 2913 -5505 5512 -5512
		mu 0 4 1655 861 3102 3103
		f 4 -205 -5515 5517 -5517
		mu 0 4 3105 149 3106 3104
		f 4 -1457 1459 5518 -5518
		mu 0 4 3106 866 3107 3104
		f 4 5515 -2921 5519 -5519
		mu 0 4 3107 864 1659 3104
		f 4 -546 548 5523 -5523
		mu 0 4 3109 338 1662 3108
		f 4 2926 1460 5524 -5524
		mu 0 4 1662 867 3110 3108
		f 4 5521 -5516 5525 -5525
		mu 0 4 3110 864 3107 3108
		f 4 5526 -4226 5528 -5528
		mu 0 4 3112 336 2456 3111
		f 4 -923 1461 5529 -5529
		mu 0 4 2456 547 1660 3111
		f 4 2921 -5522 5530 -5530
		mu 0 4 1660 864 3110 3111
		f 4 -378 384 5534 -5534
		mu 0 4 3114 242 2459 3113
		f 4 4231 1465 5535 -5535
		mu 0 4 2459 550 3115 3113
		f 4 5532 -2929 5536 -5536
		mu 0 4 3115 868 1664 3113
		f 4 2351 549 5539 -5539
		mu 0 4 1303 339 1667 3116
		f 4 2934 1466 5540 -5540
		mu 0 4 1667 870 3117 3116
		f 4 5537 -5533 5541 -5541
		mu 0 4 3117 868 3115 3116
		f 4 5542 -5527 5544 -5544
		mu 0 4 3119 336 3112 3118
		f 4 -1458 1467 5545 -5545
		mu 0 4 3112 867 1665 3118
		f 4 2929 -5538 5546 -5546
		mu 0 4 1665 868 3117 3118
		f 4 5547 -5148 5550 -5550
		mu 0 4 3121 240 2937 3120
		f 4 -1317 1470 5551 -5551
		mu 0 4 2937 792 3122 3120
		f 4 5548 -2937 5552 -5552
		mu 0 4 3122 871 1669 3120
		f 4 -483 550 5555 -5555
		mu 0 4 1548 299 1297 3123
		f 4 2342 1471 5556 -5556
		mu 0 4 1297 548 3124 3123
		f 4 5553 -5549 5557 -5557
		mu 0 4 3124 871 3122 3123
		f 4 4226 -5543 5559 -5559
		mu 0 4 2457 336 3119 3125
		f 4 -1464 1472 5560 -5560
		mu 0 4 3119 870 1670 3125
		f 4 2937 -5554 5561 -5561
		mu 0 4 1670 871 3124 3125
		f 4 -75 -5564 5566 -5566
		mu 0 4 3127 56 3128 3126
		f 4 -1475 1477 5567 -5567
		mu 0 4 3128 875 3129 3126
		f 4 5564 -2945 5568 -5568
		mu 0 4 3129 873 1674 3126
		f 4 -553 555 5572 -5572
		mu 0 4 3131 342 1677 3130
		f 4 2950 1478 5573 -5573
		mu 0 4 1677 876 3132 3130
		f 4 5570 -5565 5574 -5574
		mu 0 4 3132 873 3129 3130
		f 4 5575 -4245 5577 -5577
		mu 0 4 3134 340 2466 3133
		f 4 -931 1479 5578 -5578
		mu 0 4 2466 552 1675 3133
		f 4 2945 -5571 5579 -5579
		mu 0 4 1675 873 3132 3133
		f 4 -379 385 5583 -5583
		mu 0 4 3136 243 2469 3135
		f 4 4250 1483 5584 -5584
		mu 0 4 2469 555 3137 3135
		f 4 5581 -2953 5585 -5585
		mu 0 4 3137 877 1679 3135
		f 4 2359 556 5588 -5588
		mu 0 4 1308 343 1682 3138;
	setAttr ".fc[2500:2999]"
		f 4 2958 1484 5589 -5589
		mu 0 4 1682 879 3139 3138
		f 4 5586 -5582 5590 -5590
		mu 0 4 3139 877 3137 3138
		f 4 5591 -5576 5593 -5593
		mu 0 4 3141 340 3134 3140
		f 4 -1476 1485 5594 -5594
		mu 0 4 3134 876 1680 3140
		f 4 2953 -5587 5595 -5595
		mu 0 4 1680 877 3139 3140
		f 4 5596 -5548 5599 -5599
		mu 0 4 3143 240 3121 3142
		f 4 -1469 1488 5600 -5600
		mu 0 4 3121 872 3144 3142
		f 4 5597 -2961 5601 -5601
		mu 0 4 3144 880 1684 3142
		f 4 -547 557 5604 -5604
		mu 0 4 1668 339 1302 3145
		f 4 2350 1489 5605 -5605
		mu 0 4 1302 553 3146 3145
		f 4 5602 -5598 5606 -5606
		mu 0 4 3146 880 3144 3145
		f 4 4245 -5592 5608 -5608
		mu 0 4 2467 340 3141 3147
		f 4 -1482 1490 5609 -5609
		mu 0 4 3141 879 1685 3147
		f 4 2961 -5603 5610 -5610
		mu 0 4 1685 880 3146 3147
		f 4 -199 -5613 5615 -5615
		mu 0 4 3149 146 3150 3148
		f 4 -1493 1495 5616 -5616
		mu 0 4 3150 884 3151 3148
		f 4 5613 -2969 5617 -5617
		mu 0 4 3151 882 1689 3148
		f 4 -560 562 5621 -5621
		mu 0 4 3153 346 1692 3152
		f 4 2974 1496 5622 -5622
		mu 0 4 1692 885 3154 3152
		f 4 5619 -5614 5623 -5623
		mu 0 4 3154 882 3151 3152
		f 4 5624 -4264 5626 -5626
		mu 0 4 3156 344 2476 3155
		f 4 -939 1497 5627 -5627
		mu 0 4 2476 557 1690 3155
		f 4 2969 -5620 5628 -5628
		mu 0 4 1690 882 3154 3155
		f 4 -380 386 5632 -5632
		mu 0 4 3158 244 2479 3157
		f 4 4269 1501 5633 -5633
		mu 0 4 2479 560 3159 3157
		f 4 5630 -2977 5634 -5634
		mu 0 4 3159 886 1694 3157
		f 4 2367 563 5637 -5637
		mu 0 4 1313 347 1697 3160
		f 4 2982 1502 5638 -5638
		mu 0 4 1697 888 3161 3160
		f 4 5635 -5631 5639 -5639
		mu 0 4 3161 886 3159 3160
		f 4 5640 -5625 5642 -5642
		mu 0 4 3163 344 3156 3162
		f 4 -1494 1503 5643 -5643
		mu 0 4 3156 885 1695 3162
		f 4 2977 -5636 5644 -5644
		mu 0 4 1695 886 3161 3162
		f 4 5645 -5597 5648 -5648
		mu 0 4 3165 240 3143 3164
		f 4 -1487 1506 5649 -5649
		mu 0 4 3143 881 3166 3164
		f 4 5646 -2985 5650 -5650
		mu 0 4 3166 889 1699 3164
		f 4 -554 564 5653 -5653
		mu 0 4 1683 343 1307 3167
		f 4 2358 1507 5654 -5654
		mu 0 4 1307 558 3168 3167
		f 4 5651 -5647 5655 -5655
		mu 0 4 3168 889 3166 3167
		f 4 4264 -5641 5657 -5657
		mu 0 4 2477 344 3163 3169
		f 4 -1500 1508 5658 -5658
		mu 0 4 3163 888 1700 3169
		f 4 2985 -5652 5659 -5659
		mu 0 4 1700 889 3168 3169
		f 4 -35 48 5663 -5663
		mu 0 4 3171 37 3082 3170
		f 4 5463 1512 5664 -5664
		mu 0 4 3082 855 3172 3170
		f 4 5661 -2993 5665 -5665
		mu 0 4 3172 891 1704 3170
		f 4 2895 568 5668 -5668
		mu 0 4 1643 332 1707 3173
		f 4 2998 1513 5669 -5669
		mu 0 4 1707 893 3174 3173
		f 4 5666 -5662 5670 -5670
		mu 0 4 3174 891 3172 3173
		f 4 5671 -4283 5673 -5673
		mu 0 4 3176 348 2486 3175
		f 4 -947 1514 5674 -5674
		mu 0 4 2486 562 1705 3175
		f 4 2993 -5667 5675 -5675
		mu 0 4 1705 891 3174 3175
		f 4 4195 387 5678 -5678
		mu 0 4 2441 245 2489 3177
		f 4 4288 1517 5679 -5679
		mu 0 4 2489 565 3178 3177
		f 4 5676 -3001 5680 -5680
		mu 0 4 3178 894 1709 3177
		f 4 2375 569 5683 -5683
		mu 0 4 1318 350 1712 3179
		f 4 3006 1518 5684 -5684
		mu 0 4 1712 895 3180 3179
		f 4 5681 -5677 5685 -5685
		mu 0 4 3180 894 3178 3179
		f 4 5686 -5672 5688 -5688
		mu 0 4 3182 348 3176 3181
		f 4 -1511 1519 5689 -5689
		mu 0 4 3176 893 1710 3181
		f 4 3001 -5682 5690 -5690
		mu 0 4 1710 894 3180 3181
		f 4 5691 -5646 5694 -5694
		mu 0 4 3184 240 3165 3183
		f 4 -1505 1522 5695 -5695
		mu 0 4 3165 890 3185 3183
		f 4 5692 -3009 5696 -5696
		mu 0 4 3185 896 1714 3183
		f 4 -561 570 5699 -5699
		mu 0 4 1698 347 1312 3186
		f 4 2366 1523 5700 -5700
		mu 0 4 1312 563 3187 3186
		f 4 5697 -5693 5701 -5701
		mu 0 4 3187 896 3185 3186
		f 4 4283 -5687 5703 -5703
		mu 0 4 2487 348 3182 3188
		f 4 -1516 1524 5704 -5704
		mu 0 4 3182 895 1715 3188
		f 4 3009 -5698 5705 -5705
		mu 0 4 1715 896 3187 3188
		f 4 5706 50 5710 -5710
		mu 0 4 3190 45 3191 3189
		f 4 5707 1529 5711 -5711
		mu 0 4 3191 900 3192 3189
		f 4 5708 -3017 5712 -5712
		mu 0 4 3192 898 1719 3189
		f 4 5713 575 5716 -5716
		mu 0 4 3194 353 1722 3193
		f 4 3022 1530 5717 -5717
		mu 0 4 1722 901 3195 3193
		f 4 5714 -5709 5718 -5718
		mu 0 4 3195 898 3192 3193
		f 4 5719 -4301 5721 -5721
		mu 0 4 3197 351 2495 3196
		f 4 -955 1531 5722 -5722
		mu 0 4 2495 567 1720 3196
		f 4 3017 -5715 5723 -5723
		mu 0 4 1720 898 3195 3196
		f 4 5724 388 5727 -5727
		mu 0 4 3199 246 2498 3198
		f 4 4306 1535 5728 -5728
		mu 0 4 2498 570 3200 3198
		f 4 5725 -3025 5729 -5729
		mu 0 4 3200 902 1724 3198
		f 4 2383 576 5732 -5732
		mu 0 4 1323 354 1727 3201
		f 4 3030 1536 5733 -5733
		mu 0 4 1727 904 3202 3201
		f 4 5730 -5726 5734 -5734
		mu 0 4 3202 902 3200 3201
		f 4 5735 -5720 5737 -5737
		mu 0 4 3204 351 3197 3203
		f 4 -1528 1537 5738 -5738
		mu 0 4 3197 901 1725 3203
		f 4 3025 -5731 5739 -5739
		mu 0 4 1725 902 3202 3203
		f 4 5740 -5692 5743 -5743
		mu 0 4 3206 240 3184 3205
		f 4 -1521 1540 5744 -5744
		mu 0 4 3184 897 3207 3205
		f 4 5741 -3033 5745 -5745
		mu 0 4 3207 905 1729 3205
		f 4 -567 577 5748 -5748
		mu 0 4 1713 350 1317 3208
		f 4 2374 1541 5749 -5749
		mu 0 4 1317 568 3209 3208
		f 4 5746 -5742 5750 -5750
		mu 0 4 3209 905 3207 3208
		f 4 4301 -5736 5752 -5752
		mu 0 4 2496 351 3204 3210
		f 4 -1534 1542 5753 -5753
		mu 0 4 3204 904 1730 3210
		f 4 3033 -5747 5754 -5754
		mu 0 4 1730 905 3209 3210
		f 4 5755 -5757 5759 -5759
		mu 0 4 3212 44 3213 3211
		f 4 -1545 1547 5760 -5760
		mu 0 4 3213 909 3214 3211
		f 4 5757 -3041 5761 -5761
		mu 0 4 3214 907 1734 3211
		f 4 -580 581 5764 -5764
		mu 0 4 1808 357 1737 3215
		f 4 3046 1548 5765 -5765
		mu 0 4 1737 910 3216 3215
		f 4 5762 -5758 5766 -5766
		mu 0 4 3216 907 3214 3215
		f 4 5767 -4320 5769 -5769
		mu 0 4 3218 355 2505 3217
		f 4 -963 1549 5770 -5770
		mu 0 4 2505 572 1735 3217
		f 4 3041 -5763 5771 -5771
		mu 0 4 1735 907 3216 3217
		f 4 -383 389 5774 -5774
		mu 0 4 2549 247 2377 3219
		f 4 4068 1552 5775 -5775
		mu 0 4 2377 505 3220 3219
		f 4 5772 -3049 5776 -5776
		mu 0 4 3220 911 1739 3219
		f 4 2279 582 5779 -5779
		mu 0 4 1258 300 1742 3221
		f 4 3054 1553 5780 -5780
		mu 0 4 1742 912 3222 3221
		f 4 5777 -5773 5781 -5781
		mu 0 4 3222 911 3220 3221
		f 4 5782 -5768 5784 -5784
		mu 0 4 3224 355 3218 3223
		f 4 -1546 1554 5785 -5785
		mu 0 4 3218 910 1740 3223
		f 4 3049 -5778 5786 -5786
		mu 0 4 1740 911 3222 3223
		f 4 5148 -5741 5789 -5789
		mu 0 4 2938 240 3206 3225
		f 4 -1539 1556 5790 -5790
		mu 0 4 3206 906 3226 3225
		f 4 5787 -3057 5791 -5791
		mu 0 4 3226 913 1744 3225
		f 4 -574 583 5794 -5794
		mu 0 4 1728 354 1322 3227
		f 4 2382 1557 5795 -5795
		mu 0 4 1322 573 3228 3227
		f 4 5792 -5788 5796 -5796
		mu 0 4 3228 913 3226 3227
		f 4 4320 -5783 5798 -5798
		mu 0 4 2506 355 3224 3229
		f 4 -1551 1558 5799 -5799
		mu 0 4 3224 912 1745 3229
		f 4 3057 -5793 5800 -5800
		mu 0 4 1745 913 3228 3229
		f 4 -223 -5803 5805 -5805
		mu 0 4 3231 159 3232 3230
		f 4 -1561 1563 5806 -5806
		mu 0 4 3232 916 3233 3230
		f 4 5803 -3065 5807 -5807
		mu 0 4 3233 914 1749 3230
		f 4 -586 588 5811 -5811
		mu 0 4 3235 360 1752 3234
		f 4 3070 1564 5812 -5812
		mu 0 4 1752 917 3236 3234
		f 4 5809 -5804 5813 -5813
		mu 0 4 3236 914 3233 3234
		f 4 5814 -4339 5816 -5816
		mu 0 4 3238 358 2515 3237
		f 4 -971 1565 5817 -5817
		mu 0 4 2515 577 1750 3237
		f 4 3065 -5810 5818 -5818
		mu 0 4 1750 914 3236 3237
		f 4 -392 397 5822 -5822
		mu 0 4 3240 250 2518 3239
		f 4 4344 1569 5823 -5823
		mu 0 4 2518 580 3241 3239
		f 4 5820 -3073 5824 -5824
		mu 0 4 3241 918 1754 3239
		f 4 2399 589 5827 -5827
		mu 0 4 1333 361 1757 3242
		f 4 3078 1570 5828 -5828
		mu 0 4 1757 920 3243 3242
		f 4 5825 -5821 5829 -5829
		mu 0 4 3243 918 3241 3242
		f 4 5830 -5815 5832 -5832
		mu 0 4 3245 358 3238 3244
		f 4 -1562 1571 5833 -5833
		mu 0 4 3238 917 1755 3244
		f 4 3073 -5826 5834 -5834
		mu 0 4 1755 918 3243 3244
		f 4 5835 -5198 5838 -5838
		mu 0 4 3247 248 2960 3246
		f 4 -1336 1574 5839 -5839
		mu 0 4 2960 802 3248 3246
		f 4 5836 -3081 5840 -5840
		mu 0 4 3248 921 1759 3246
		f 4 -491 590 5843 -5843
		mu 0 4 1563 304 1327 3249
		f 4 2390 1575 5844 -5844
		mu 0 4 1327 578 3250 3249
		f 4 5841 -5837 5845 -5845
		mu 0 4 3250 921 3248 3249
		f 4 4339 -5831 5847 -5847
		mu 0 4 2516 358 3245 3251
		f 4 -1568 1576 5848 -5848
		mu 0 4 3245 920 1760 3251
		f 4 3081 -5842 5849 -5849
		mu 0 4 1760 921 3250 3251
		f 4 -82 -5852 5854 -5854
		mu 0 4 3253 61 3254 3252
		f 4 -1579 1581 5855 -5855
		mu 0 4 3254 925 3255 3252
		f 4 5852 -3089 5856 -5856
		mu 0 4 3255 923 1764 3252
		f 4 -593 595 5860 -5860
		mu 0 4 3257 364 1767 3256
		f 4 3094 1582 5861 -5861
		mu 0 4 1767 926 3258 3256
		f 4 5858 -5853 5862 -5862
		mu 0 4 3258 923 3255 3256
		f 4 5863 -4358 5865 -5865
		mu 0 4 3260 362 2525 3259
		f 4 -979 1583 5866 -5866
		mu 0 4 2525 582 1765 3259
		f 4 3089 -5859 5867 -5867
		mu 0 4 1765 923 3258 3259
		f 4 -393 398 5871 -5871
		mu 0 4 3262 251 2528 3261
		f 4 4363 1587 5872 -5872
		mu 0 4 2528 585 3263 3261
		f 4 5869 -3097 5873 -5873
		mu 0 4 3263 927 1769 3261
		f 4 2407 596 5876 -5876
		mu 0 4 1338 365 1772 3264
		f 4 3102 1588 5877 -5877
		mu 0 4 1772 929 3265 3264
		f 4 5874 -5870 5878 -5878
		mu 0 4 3265 927 3263 3264
		f 4 5879 -5864 5881 -5881
		mu 0 4 3267 362 3260 3266
		f 4 -1580 1589 5882 -5882
		mu 0 4 3260 926 1770 3266
		f 4 3097 -5875 5883 -5883
		mu 0 4 1770 927 3265 3266
		f 4 5884 -5836 5887 -5887
		mu 0 4 3269 248 3247 3268
		f 4 -1573 1592 5888 -5888
		mu 0 4 3247 922 3270 3268
		f 4 5885 -3105 5889 -5889
		mu 0 4 3270 930 1774 3268
		f 4 -587 597 5892 -5892
		mu 0 4 1758 361 1332 3271
		f 4 2398 1593 5893 -5893
		mu 0 4 1332 583 3272 3271
		f 4 5890 -5886 5894 -5894
		mu 0 4 3272 930 3270 3271
		f 4 4358 -5880 5896 -5896
		mu 0 4 2526 362 3267 3273
		f 4 -1586 1594 5897 -5897
		mu 0 4 3267 929 1775 3273
		f 4 3105 -5891 5898 -5898
		mu 0 4 1775 930 3272 3273
		f 4 -217 -5901 5903 -5903
		mu 0 4 3275 156 3276 3274
		f 4 -1597 1599 5904 -5904
		mu 0 4 3276 934 3277 3274
		f 4 5901 -3113 5905 -5905
		mu 0 4 3277 932 1779 3274
		f 4 -600 602 5909 -5909
		mu 0 4 3279 368 1782 3278
		f 4 3118 1600 5910 -5910
		mu 0 4 1782 935 3280 3278
		f 4 5907 -5902 5911 -5911
		mu 0 4 3280 932 3277 3278
		f 4 5912 -4377 5914 -5914
		mu 0 4 3282 366 2535 3281
		f 4 -987 1601 5915 -5915
		mu 0 4 2535 587 1780 3281
		f 4 3113 -5908 5916 -5916
		mu 0 4 1780 932 3280 3281
		f 4 -394 399 5920 -5920
		mu 0 4 3284 252 2538 3283
		f 4 4382 1605 5921 -5921
		mu 0 4 2538 590 3285 3283
		f 4 5918 -3121 5922 -5922
		mu 0 4 3285 936 1784 3283
		f 4 2415 603 5925 -5925
		mu 0 4 1343 369 1787 3286
		f 4 3126 1606 5926 -5926
		mu 0 4 1787 938 3287 3286
		f 4 5923 -5919 5927 -5927
		mu 0 4 3287 936 3285 3286
		f 4 5928 -5913 5930 -5930
		mu 0 4 3289 366 3282 3288
		f 4 -1598 1607 5931 -5931
		mu 0 4 3282 935 1785 3288
		f 4 3121 -5924 5932 -5932
		mu 0 4 1785 936 3287 3288
		f 4 5933 -5885 5936 -5936
		mu 0 4 3291 248 3269 3290
		f 4 -1591 1610 5937 -5937
		mu 0 4 3269 931 3292 3290
		f 4 5934 -3129 5938 -5938
		mu 0 4 3292 939 1789 3290
		f 4 -594 604 5941 -5941
		mu 0 4 1773 365 1337 3293
		f 4 2406 1611 5942 -5942
		mu 0 4 1337 588 3294 3293
		f 4 5939 -5935 5943 -5943
		mu 0 4 3294 939 3292 3293
		f 4 4377 -5929 5945 -5945
		mu 0 4 2536 366 3289 3295
		f 4 -1604 1612 5946 -5946
		mu 0 4 3289 938 1790 3295
		f 4 3129 -5940 5947 -5947
		mu 0 4 1790 939 3294 3295
		f 4 -37 49 5951 -5951
		mu 0 4 3297 36 2921 3296
		f 4 5113 1616 5952 -5952
		mu 0 4 2921 785 3298 3296
		f 4 5949 -3137 5953 -5953
		mu 0 4 3298 941 1794 3296
		f 4 2727 608 5956 -5956
		mu 0 4 1538 297 1797 3299
		f 4 3142 1617 5957 -5957
		mu 0 4 1797 943 3300 3299
		f 4 5954 -5950 5958 -5958
		mu 0 4 3300 941 3298 3299
		f 4 5959 -4396 5961 -5961
		mu 0 4 3302 370 2545 3301
		f 4 -995 1618 5962 -5962
		mu 0 4 2545 592 1795 3301
		f 4 3137 -5955 5963 -5963
		mu 0 4 1795 941 3300 3301
		f 4 4069 400 5966 -5966
		mu 0 4 2378 247 2548 3303
		f 4 4401 1621 5967 -5967
		mu 0 4 2548 595 3304 3303
		f 4 5964 -3145 5968 -5968
		mu 0 4 3304 944 1799 3303
		f 4 2423 609 5971 -5971
		mu 0 4 1348 372 1802 3305
		f 4 3150 1622 5972 -5972
		mu 0 4 1802 945 3306 3305
		f 4 5969 -5965 5973 -5973
		mu 0 4 3306 944 3304 3305
		f 4 5974 -5960 5976 -5976
		mu 0 4 3308 370 3302 3307
		f 4 -1615 1623 5977 -5977
		mu 0 4 3302 943 1800 3307
		f 4 3145 -5970 5978 -5978
		mu 0 4 1800 944 3306 3307
		f 4 5979 -5934 5982 -5982
		mu 0 4 3310 248 3291 3309
		f 4 -1609 1626 5983 -5983
		mu 0 4 3291 940 3311 3309
		f 4 5980 -3153 5984 -5984
		mu 0 4 3311 946 1804 3309
		f 4 -601 610 5987 -5987
		mu 0 4 1788 369 1342 3312
		f 4 2414 1627 5988 -5988
		mu 0 4 1342 593 3313 3312
		f 4 5985 -5981 5989 -5989
		mu 0 4 3313 946 3311 3312
		f 4 4396 -5975 5991 -5991
		mu 0 4 2546 370 3308 3314
		f 4 -1620 1628 5992 -5992
		mu 0 4 3308 945 1805 3314
		f 4 3153 -5986 5993 -5993
		mu 0 4 1805 946 3313 3314
		f 4 5756 54 5997 -5997
		mu 0 4 3213 44 3316 3315
		f 4 5994 1632 5998 -5998
		mu 0 4 3316 949 3317 3315
		f 4 5995 -3161 5999 -5999
		mu 0 4 3317 948 1809 3315
		f 4 6000 614 6003 -6003
		mu 0 4 3319 374 1812 3318
		f 4 3166 1633 6004 -6004
		mu 0 4 1812 950 3320 3318
		f 4 6001 -5996 6005 -6005
		mu 0 4 3320 948 3317 3318
		f 4 6006 -4414 6008 -6008
		mu 0 4 3322 373 2554 3321
		f 4 -1003 1634 6009 -6009
		mu 0 4 2554 597 1810 3321
		f 4 3161 -6002 6010 -6010
		mu 0 4 1810 948 3320 3321
		f 4 6011 401 6014 -6014
		mu 0 4 3324 253 2557 3323
		f 4 4419 1638 6015 -6015
		mu 0 4 2557 600 3325 3323
		f 4 6012 -3169 6016 -6016
		mu 0 4 3325 951 1814 3323
		f 4 2431 615 6019 -6019
		mu 0 4 1353 375 1817 3326
		f 4 3174 1639 6020 -6020
		mu 0 4 1817 953 3327 3326
		f 4 6017 -6013 6021 -6021
		mu 0 4 3327 951 3325 3326
		f 4 6022 -6007 6024 -6024
		mu 0 4 3329 373 3322 3328
		f 4 -1631 1640 6025 -6025
		mu 0 4 3322 950 1815 3328
		f 4 3169 -6018 6026 -6026
		mu 0 4 1815 951 3327 3328
		f 4 6027 -5980 6030 -6030
		mu 0 4 3331 248 3310 3330
		f 4 -1625 1643 6031 -6031
		mu 0 4 3310 947 3332 3330
		f 4 6028 -3177 6032 -6032
		mu 0 4 3332 954 1819 3330
		f 4 -607 616 6035 -6035
		mu 0 4 1803 372 1347 3333
		f 4 2422 1644 6036 -6036
		mu 0 4 1347 598 3334 3333
		f 4 6033 -6029 6037 -6037
		mu 0 4 3334 954 3332 3333
		f 4 4414 -6023 6039 -6039
		mu 0 4 2555 373 3329 3335
		f 4 -1637 1645 6040 -6040
		mu 0 4 3329 953 1820 3335
		f 4 3177 -6034 6041 -6041
		mu 0 4 1820 954 3334 3335
		f 4 6042 -6044 6046 -6046
		mu 0 4 3337 46 3338 3336
		f 4 -1648 1650 6047 -6047
		mu 0 4 3338 958 3339 3336
		f 4 6044 -3185 6048 -6048
		mu 0 4 3339 956 1824 3336
		f 4 -619 620 6051 -6051
		mu 0 4 1898 378 1827 3340
		f 4 3190 1651 6052 -6052
		mu 0 4 1827 959 3341 3340
		f 4 6049 -6045 6053 -6053
		mu 0 4 3341 956 3339 3340
		f 4 6054 -4433 6056 -6056
		mu 0 4 3343 376 2564 3342
		f 4 -1011 1652 6057 -6057
		mu 0 4 2564 602 1825 3342
		f 4 3185 -6050 6058 -6058
		mu 0 4 1825 956 3341 3342
		f 4 -396 402 6061 -6061
		mu 0 4 2608 254 2386 3344
		f 4 4086 1655 6062 -6062
		mu 0 4 2386 510 3345 3344
		f 4 6059 -3193 6063 -6063
		mu 0 4 3345 960 1829 3344
		f 4 2287 621 6066 -6066
		mu 0 4 1263 305 1832 3346
		f 4 3198 1656 6067 -6067
		mu 0 4 1832 961 3347 3346
		f 4 6064 -6060 6068 -6068
		mu 0 4 3347 960 3345 3346
		f 4 6069 -6055 6071 -6071
		mu 0 4 3349 376 3343 3348
		f 4 -1649 1657 6072 -6072
		mu 0 4 3343 959 1830 3348
		f 4 3193 -6065 6073 -6073
		mu 0 4 1830 960 3347 3348
		f 4 5198 -6028 6076 -6076
		mu 0 4 2961 248 3331 3350
		f 4 -1642 1659 6077 -6077
		mu 0 4 3331 955 3351 3350
		f 4 6074 -3201 6078 -6078
		mu 0 4 3351 962 1834 3350
		f 4 -613 622 6081 -6081
		mu 0 4 1818 375 1352 3352
		f 4 2430 1660 6082 -6082
		mu 0 4 1352 603 3353 3352
		f 4 6079 -6075 6083 -6083
		mu 0 4 3353 962 3351 3352
		f 4 4433 -6070 6085 -6085
		mu 0 4 2565 376 3349 3354
		f 4 -1654 1661 6086 -6086
		mu 0 4 3349 961 1835 3354
		f 4 3201 -6080 6087 -6087
		mu 0 4 1835 962 3353 3354
		f 4 -241 -6090 6092 -6092
		mu 0 4 3356 169 3357 3355
		f 4 -1664 1666 6093 -6093
		mu 0 4 3357 965 3358 3355
		f 4 6090 -3209 6094 -6094
		mu 0 4 3358 963 1839 3355
		f 4 -625 627 6098 -6098
		mu 0 4 3360 381 1842 3359
		f 4 3214 1667 6099 -6099
		mu 0 4 1842 966 3361 3359
		f 4 6096 -6091 6100 -6100
		mu 0 4 3361 963 3358 3359
		f 4 6101 -4452 6103 -6103
		mu 0 4 3363 379 2574 3362
		f 4 -1019 1668 6104 -6104
		mu 0 4 2574 607 1840 3362
		f 4 3209 -6097 6105 -6105
		mu 0 4 1840 963 3361 3362
		f 4 -405 410 6109 -6109
		mu 0 4 3365 257 2577 3364
		f 4 4457 1672 6110 -6110
		mu 0 4 2577 610 3366 3364
		f 4 6107 -3217 6111 -6111
		mu 0 4 3366 967 1844 3364
		f 4 2447 628 6114 -6114
		mu 0 4 1363 382 1847 3367
		f 4 3222 1673 6115 -6115
		mu 0 4 1847 969 3368 3367
		f 4 6112 -6108 6116 -6116
		mu 0 4 3368 967 3366 3367
		f 4 6117 -6102 6119 -6119
		mu 0 4 3370 379 3363 3369
		f 4 -1665 1674 6120 -6120
		mu 0 4 3363 966 1845 3369
		f 4 3217 -6113 6121 -6121
		mu 0 4 1845 967 3368 3369
		f 4 6122 -5248 6125 -6125
		mu 0 4 3372 255 2983 3371
		f 4 -1355 1677 6126 -6126
		mu 0 4 2983 812 3373 3371
		f 4 6123 -3225 6127 -6127
		mu 0 4 3373 970 1849 3371
		f 4 -499 629 6130 -6130
		mu 0 4 1578 309 1357 3374
		f 4 2438 1678 6131 -6131
		mu 0 4 1357 608 3375 3374
		f 4 6128 -6124 6132 -6132
		mu 0 4 3375 970 3373 3374
		f 4 4452 -6118 6134 -6134
		mu 0 4 2575 379 3370 3376
		f 4 -1671 1679 6135 -6135
		mu 0 4 3370 969 1850 3376
		f 4 3225 -6129 6136 -6136
		mu 0 4 1850 970 3375 3376
		f 4 -89 -6139 6141 -6141
		mu 0 4 3378 66 3379 3377
		f 4 -1682 1684 6142 -6142
		mu 0 4 3379 974 3380 3377
		f 4 6139 -3233 6143 -6143
		mu 0 4 3380 972 1854 3377
		f 4 -632 634 6147 -6147
		mu 0 4 3382 385 1857 3381
		f 4 3238 1685 6148 -6148
		mu 0 4 1857 975 3383 3381
		f 4 6145 -6140 6149 -6149
		mu 0 4 3383 972 3380 3381
		f 4 6150 -4471 6152 -6152
		mu 0 4 3385 383 2584 3384
		f 4 -1027 1686 6153 -6153
		mu 0 4 2584 612 1855 3384
		f 4 3233 -6146 6154 -6154
		mu 0 4 1855 972 3383 3384
		f 4 -406 411 6158 -6158
		mu 0 4 3387 258 2587 3386
		f 4 4476 1690 6159 -6159
		mu 0 4 2587 615 3388 3386
		f 4 6156 -3241 6160 -6160
		mu 0 4 3388 976 1859 3386
		f 4 2455 635 6163 -6163
		mu 0 4 1368 386 1862 3389
		f 4 3246 1691 6164 -6164
		mu 0 4 1862 978 3390 3389
		f 4 6161 -6157 6165 -6165
		mu 0 4 3390 976 3388 3389
		f 4 6166 -6151 6168 -6168
		mu 0 4 3392 383 3385 3391
		f 4 -1683 1692 6169 -6169
		mu 0 4 3385 975 1860 3391
		f 4 3241 -6162 6170 -6170
		mu 0 4 1860 976 3390 3391
		f 4 6171 -6123 6174 -6174
		mu 0 4 3394 255 3372 3393
		f 4 -1676 1695 6175 -6175
		mu 0 4 3372 971 3395 3393
		f 4 6172 -3249 6176 -6176
		mu 0 4 3395 979 1864 3393
		f 4 -626 636 6179 -6179
		mu 0 4 1848 382 1362 3396
		f 4 2446 1696 6180 -6180
		mu 0 4 1362 613 3397 3396
		f 4 6177 -6173 6181 -6181
		mu 0 4 3397 979 3395 3396
		f 4 4471 -6167 6183 -6183
		mu 0 4 2585 383 3392 3398
		f 4 -1689 1697 6184 -6184
		mu 0 4 3392 978 1865 3398
		f 4 3249 -6178 6185 -6185
		mu 0 4 1865 979 3397 3398
		f 4 -235 -6188 6190 -6190
		mu 0 4 3400 166 3401 3399
		f 4 -1700 1702 6191 -6191
		mu 0 4 3401 983 3402 3399
		f 4 6188 -3257 6192 -6192
		mu 0 4 3402 981 1869 3399
		f 4 -639 641 6196 -6196
		mu 0 4 3404 389 1872 3403
		f 4 3262 1703 6197 -6197
		mu 0 4 1872 984 3405 3403
		f 4 6194 -6189 6198 -6198
		mu 0 4 3405 981 3402 3403
		f 4 6199 -4490 6201 -6201
		mu 0 4 3407 387 2594 3406
		f 4 -1035 1704 6202 -6202
		mu 0 4 2594 617 1870 3406
		f 4 3257 -6195 6203 -6203
		mu 0 4 1870 981 3405 3406
		f 4 -407 412 6207 -6207
		mu 0 4 3409 259 2597 3408
		f 4 4495 1708 6208 -6208
		mu 0 4 2597 620 3410 3408
		f 4 6205 -3265 6209 -6209
		mu 0 4 3410 985 1874 3408
		f 4 2463 642 6212 -6212
		mu 0 4 1373 390 1877 3411
		f 4 3270 1709 6213 -6213
		mu 0 4 1877 987 3412 3411
		f 4 6210 -6206 6214 -6214
		mu 0 4 3412 985 3410 3411
		f 4 6215 -6200 6217 -6217
		mu 0 4 3414 387 3407 3413
		f 4 -1701 1710 6218 -6218
		mu 0 4 3407 984 1875 3413
		f 4 3265 -6211 6219 -6219
		mu 0 4 1875 985 3412 3413
		f 4 6220 -6172 6223 -6223
		mu 0 4 3416 255 3394 3415
		f 4 -1694 1713 6224 -6224
		mu 0 4 3394 980 3417 3415
		f 4 6221 -3273 6225 -6225
		mu 0 4 3417 988 1879 3415
		f 4 -633 643 6228 -6228
		mu 0 4 1863 386 1367 3418
		f 4 2454 1714 6229 -6229
		mu 0 4 1367 618 3419 3418
		f 4 6226 -6222 6230 -6230
		mu 0 4 3419 988 3417 3418
		f 4 4490 -6216 6232 -6232
		mu 0 4 2595 387 3414 3420
		f 4 -1707 1715 6233 -6233
		mu 0 4 3414 987 1880 3420
		f 4 3273 -6227 6234 -6234
		mu 0 4 1880 988 3419 3420
		f 4 -39 53 6238 -6238
		mu 0 4 3422 38 2944 3421
		f 4 5163 1719 6239 -6239
		mu 0 4 2944 795 3423 3421
		f 4 6236 -3281 6240 -6240
		mu 0 4 3423 990 1884 3421
		f 4 2751 647 6243 -6243
		mu 0 4 1553 302 1887 3424
		f 4 3286 1720 6244 -6244
		mu 0 4 1887 992 3425 3424
		f 4 6241 -6237 6245 -6245
		mu 0 4 3425 990 3423 3424
		f 4 6246 -4509 6248 -6248
		mu 0 4 3427 391 2604 3426
		f 4 -1043 1721 6249 -6249
		mu 0 4 2604 622 1885 3426
		f 4 3281 -6242 6250 -6250
		mu 0 4 1885 990 3425 3426
		f 4 4087 413 6253 -6253
		mu 0 4 2387 254 2607 3428
		f 4 4514 1724 6254 -6254
		mu 0 4 2607 625 3429 3428
		f 4 6251 -3289 6255 -6255
		mu 0 4 3429 993 1889 3428
		f 4 2471 648 6258 -6258
		mu 0 4 1378 393 1892 3430
		f 4 3294 1725 6259 -6259
		mu 0 4 1892 994 3431 3430
		f 4 6256 -6252 6260 -6260
		mu 0 4 3431 993 3429 3430
		f 4 6261 -6247 6263 -6263
		mu 0 4 3433 391 3427 3432
		f 4 -1718 1726 6264 -6264
		mu 0 4 3427 992 1890 3432
		f 4 3289 -6257 6265 -6265
		mu 0 4 1890 993 3431 3432
		f 4 6266 -6221 6269 -6269
		mu 0 4 3435 255 3416 3434
		f 4 -1712 1729 6270 -6270
		mu 0 4 3416 989 3436 3434
		f 4 6267 -3297 6271 -6271
		mu 0 4 3436 995 1894 3434
		f 4 -640 649 6274 -6274
		mu 0 4 1878 390 1372 3437
		f 4 2462 1730 6275 -6275
		mu 0 4 1372 623 3438 3437
		f 4 6272 -6268 6276 -6276
		mu 0 4 3438 995 3436 3437
		f 4 4509 -6262 6278 -6278
		mu 0 4 2605 391 3433 3439
		f 4 -1723 1731 6279 -6279
		mu 0 4 3433 994 1895 3439
		f 4 3297 -6273 6280 -6280
		mu 0 4 1895 995 3438 3439
		f 4 6043 57 6284 -6284
		mu 0 4 3338 46 3441 3440
		f 4 6281 1735 6285 -6285
		mu 0 4 3441 998 3442 3440
		f 4 6282 -3305 6286 -6286
		mu 0 4 3442 997 1899 3440
		f 4 6287 653 6290 -6290
		mu 0 4 3444 395 1902 3443
		f 4 3310 1736 6291 -6291
		mu 0 4 1902 999 3445 3443
		f 4 6288 -6283 6292 -6292
		mu 0 4 3445 997 3442 3443
		f 4 6293 -4527 6295 -6295
		mu 0 4 3447 394 2613 3446
		f 4 -1051 1737 6296 -6296
		mu 0 4 2613 627 1900 3446
		f 4 3305 -6289 6297 -6297
		mu 0 4 1900 997 3445 3446
		f 4 6298 414 6301 -6301
		mu 0 4 3449 260 2616 3448
		f 4 4532 1741 6302 -6302
		mu 0 4 2616 630 3450 3448
		f 4 6299 -3313 6303 -6303
		mu 0 4 3450 1000 1904 3448
		f 4 2479 654 6306 -6306
		mu 0 4 1383 396 1907 3451
		f 4 3318 1742 6307 -6307
		mu 0 4 1907 1002 3452 3451
		f 4 6304 -6300 6308 -6308
		mu 0 4 3452 1000 3450 3451
		f 4 6309 -6294 6311 -6311
		mu 0 4 3454 394 3447 3453
		f 4 -1734 1743 6312 -6312
		mu 0 4 3447 999 1905 3453
		f 4 3313 -6305 6313 -6313
		mu 0 4 1905 1000 3452 3453
		f 4 6314 -6267 6317 -6317
		mu 0 4 3456 255 3435 3455
		f 4 -1728 1746 6318 -6318
		mu 0 4 3435 996 3457 3455
		f 4 6315 -3321 6319 -6319
		mu 0 4 3457 1003 1909 3455
		f 4 -646 655 6322 -6322
		mu 0 4 1893 393 1377 3458
		f 4 2470 1747 6323 -6323
		mu 0 4 1377 628 3459 3458
		f 4 6320 -6316 6324 -6324
		mu 0 4 3459 1003 3457 3458
		f 4 4527 -6310 6326 -6326
		mu 0 4 2614 394 3454 3460
		f 4 -1740 1748 6327 -6327
		mu 0 4 3454 1002 1910 3460
		f 4 3321 -6321 6328 -6328
		mu 0 4 1910 1003 3459 3460
		f 4 6329 -6331 6333 -6333
		mu 0 4 3462 47 3463 3461
		f 4 -1751 1753 6334 -6334
		mu 0 4 3463 1007 3464 3461
		f 4 6331 -3329 6335 -6335
		mu 0 4 3464 1005 1914 3461
		f 4 -658 659 6338 -6338
		mu 0 4 1988 399 1917 3465
		f 4 3334 1754 6339 -6339
		mu 0 4 1917 1008 3466 3465
		f 4 6336 -6332 6340 -6340
		mu 0 4 3466 1005 3464 3465
		f 4 6341 -4546 6343 -6343
		mu 0 4 3468 397 2623 3467
		f 4 -1059 1755 6344 -6344
		mu 0 4 2623 632 1915 3467
		f 4 3329 -6337 6345 -6345
		mu 0 4 1915 1005 3466 3467
		f 4 -409 415 6348 -6348
		mu 0 4 2667 261 2395 3469
		f 4 4104 1758 6349 -6349
		mu 0 4 2395 515 3470 3469
		f 4 6346 -3337 6350 -6350
		mu 0 4 3470 1009 1919 3469
		f 4 2295 660 6353 -6353
		mu 0 4 1268 310 1922 3471
		f 4 3342 1759 6354 -6354
		mu 0 4 1922 1010 3472 3471
		f 4 6351 -6347 6355 -6355
		mu 0 4 3472 1009 3470 3471
		f 4 6356 -6342 6358 -6358
		mu 0 4 3474 397 3468 3473
		f 4 -1752 1760 6359 -6359
		mu 0 4 3468 1008 1920 3473
		f 4 3337 -6352 6360 -6360
		mu 0 4 1920 1009 3472 3473
		f 4 5248 -6315 6363 -6363
		mu 0 4 2984 255 3456 3475
		f 4 -1745 1762 6364 -6364
		mu 0 4 3456 1004 3476 3475
		f 4 6361 -3345 6365 -6365
		mu 0 4 3476 1011 1924 3475
		f 4 -652 661 6368 -6368
		mu 0 4 1908 396 1382 3477
		f 4 2478 1763 6369 -6369
		mu 0 4 1382 633 3478 3477
		f 4 6366 -6362 6370 -6370
		mu 0 4 3478 1011 3476 3477
		f 4 4546 -6357 6372 -6372
		mu 0 4 2624 397 3474 3479
		f 4 -1757 1764 6373 -6373
		mu 0 4 3474 1010 1925 3479
		f 4 3345 -6367 6374 -6374
		mu 0 4 1925 1011 3478 3479
		f 4 -259 -6377 6379 -6379
		mu 0 4 3481 179 3482 3480
		f 4 -1767 1769 6380 -6380
		mu 0 4 3482 1014 3483 3480
		f 4 6377 -3353 6381 -6381
		mu 0 4 3483 1012 1929 3480
		f 4 -664 666 6385 -6385
		mu 0 4 3485 402 1932 3484
		f 4 3358 1770 6386 -6386
		mu 0 4 1932 1015 3486 3484
		f 4 6383 -6378 6387 -6387
		mu 0 4 3486 1012 3483 3484
		f 4 6388 -4565 6390 -6390
		mu 0 4 3488 400 2633 3487
		f 4 -1067 1771 6391 -6391
		mu 0 4 2633 637 1930 3487
		f 4 3353 -6384 6392 -6392
		mu 0 4 1930 1012 3486 3487
		f 4 -418 423 6396 -6396
		mu 0 4 3490 264 2636 3489
		f 4 4570 1775 6397 -6397
		mu 0 4 2636 640 3491 3489
		f 4 6394 -3361 6398 -6398
		mu 0 4 3491 1016 1934 3489
		f 4 2495 667 6401 -6401
		mu 0 4 1393 403 1937 3492
		f 4 3366 1776 6402 -6402
		mu 0 4 1937 1018 3493 3492
		f 4 6399 -6395 6403 -6403
		mu 0 4 3493 1016 3491 3492
		f 4 6404 -6389 6406 -6406
		mu 0 4 3495 400 3488 3494
		f 4 -1768 1777 6407 -6407
		mu 0 4 3488 1015 1935 3494
		f 4 3361 -6400 6408 -6408
		mu 0 4 1935 1016 3493 3494
		f 4 6409 -5298 6412 -6412
		mu 0 4 3497 262 3006 3496
		f 4 -1374 1780 6413 -6413
		mu 0 4 3006 822 3498 3496
		f 4 6410 -3369 6414 -6414
		mu 0 4 3498 1019 1939 3496
		f 4 -507 668 6417 -6417
		mu 0 4 1593 314 1387 3499
		f 4 2486 1781 6418 -6418
		mu 0 4 1387 638 3500 3499
		f 4 6415 -6411 6419 -6419
		mu 0 4 3500 1019 3498 3499
		f 4 4565 -6405 6421 -6421
		mu 0 4 2634 400 3495 3501
		f 4 -1774 1782 6422 -6422
		mu 0 4 3495 1018 1940 3501
		f 4 3369 -6416 6423 -6423
		mu 0 4 1940 1019 3500 3501
		f 4 -96 -6426 6428 -6428
		mu 0 4 3503 71 3504 3502
		f 4 -1785 1787 6429 -6429
		mu 0 4 3504 1023 3505 3502
		f 4 6426 -3377 6430 -6430
		mu 0 4 3505 1021 1944 3502
		f 4 -671 673 6434 -6434
		mu 0 4 3507 406 1947 3506
		f 4 3382 1788 6435 -6435
		mu 0 4 1947 1024 3508 3506
		f 4 6432 -6427 6436 -6436
		mu 0 4 3508 1021 3505 3506
		f 4 6437 -4584 6439 -6439
		mu 0 4 3510 404 2643 3509
		f 4 -1075 1789 6440 -6440
		mu 0 4 2643 642 1945 3509
		f 4 3377 -6433 6441 -6441
		mu 0 4 1945 1021 3508 3509
		f 4 -419 424 6445 -6445
		mu 0 4 3512 265 2646 3511
		f 4 4589 1793 6446 -6446
		mu 0 4 2646 645 3513 3511
		f 4 6443 -3385 6447 -6447
		mu 0 4 3513 1025 1949 3511
		f 4 2503 674 6450 -6450
		mu 0 4 1398 407 1952 3514
		f 4 3390 1794 6451 -6451
		mu 0 4 1952 1027 3515 3514
		f 4 6448 -6444 6452 -6452
		mu 0 4 3515 1025 3513 3514
		f 4 6453 -6438 6455 -6455
		mu 0 4 3517 404 3510 3516
		f 4 -1786 1795 6456 -6456
		mu 0 4 3510 1024 1950 3516
		f 4 3385 -6449 6457 -6457
		mu 0 4 1950 1025 3515 3516
		f 4 6458 -6410 6461 -6461
		mu 0 4 3519 262 3497 3518
		f 4 -1779 1798 6462 -6462
		mu 0 4 3497 1020 3520 3518
		f 4 6459 -3393 6463 -6463
		mu 0 4 3520 1028 1954 3518
		f 4 -665 675 6466 -6466
		mu 0 4 1938 403 1392 3521
		f 4 2494 1799 6467 -6467
		mu 0 4 1392 643 3522 3521
		f 4 6464 -6460 6468 -6468
		mu 0 4 3522 1028 3520 3521
		f 4 4584 -6454 6470 -6470
		mu 0 4 2644 404 3517 3523
		f 4 -1792 1800 6471 -6471
		mu 0 4 3517 1027 1955 3523
		f 4 3393 -6465 6472 -6472
		mu 0 4 1955 1028 3522 3523;
	setAttr ".fc[3000:3499]"
		f 4 -253 -6475 6477 -6477
		mu 0 4 3525 176 3526 3524
		f 4 -1803 1805 6478 -6478
		mu 0 4 3526 1032 3527 3524
		f 4 6475 -3401 6479 -6479
		mu 0 4 3527 1030 1959 3524
		f 4 -678 680 6483 -6483
		mu 0 4 3529 410 1962 3528
		f 4 3406 1806 6484 -6484
		mu 0 4 1962 1033 3530 3528
		f 4 6481 -6476 6485 -6485
		mu 0 4 3530 1030 3527 3528
		f 4 6486 -4603 6488 -6488
		mu 0 4 3532 408 2653 3531
		f 4 -1083 1807 6489 -6489
		mu 0 4 2653 647 1960 3531
		f 4 3401 -6482 6490 -6490
		mu 0 4 1960 1030 3530 3531
		f 4 -420 425 6494 -6494
		mu 0 4 3534 266 2656 3533
		f 4 4608 1811 6495 -6495
		mu 0 4 2656 650 3535 3533
		f 4 6492 -3409 6496 -6496
		mu 0 4 3535 1034 1964 3533
		f 4 2511 681 6499 -6499
		mu 0 4 1403 411 1967 3536
		f 4 3414 1812 6500 -6500
		mu 0 4 1967 1036 3537 3536
		f 4 6497 -6493 6501 -6501
		mu 0 4 3537 1034 3535 3536
		f 4 6502 -6487 6504 -6504
		mu 0 4 3539 408 3532 3538
		f 4 -1804 1813 6505 -6505
		mu 0 4 3532 1033 1965 3538
		f 4 3409 -6498 6506 -6506
		mu 0 4 1965 1034 3537 3538
		f 4 6507 -6459 6510 -6510
		mu 0 4 3541 262 3519 3540
		f 4 -1797 1816 6511 -6511
		mu 0 4 3519 1029 3542 3540
		f 4 6508 -3417 6512 -6512
		mu 0 4 3542 1037 1969 3540
		f 4 -672 682 6515 -6515
		mu 0 4 1953 407 1397 3543
		f 4 2502 1817 6516 -6516
		mu 0 4 1397 648 3544 3543
		f 4 6513 -6509 6517 -6517
		mu 0 4 3544 1037 3542 3543
		f 4 4603 -6503 6519 -6519
		mu 0 4 2654 408 3539 3545
		f 4 -1810 1818 6520 -6520
		mu 0 4 3539 1036 1970 3545
		f 4 3417 -6514 6521 -6521
		mu 0 4 1970 1037 3544 3545
		f 4 -41 56 6525 -6525
		mu 0 4 3547 39 2967 3546
		f 4 5213 1822 6526 -6526
		mu 0 4 2967 805 3548 3546
		f 4 6523 -3425 6527 -6527
		mu 0 4 3548 1039 1974 3546
		f 4 2775 686 6530 -6530
		mu 0 4 1568 307 1977 3549
		f 4 3430 1823 6531 -6531
		mu 0 4 1977 1041 3550 3549
		f 4 6528 -6524 6532 -6532
		mu 0 4 3550 1039 3548 3549
		f 4 6533 -4622 6535 -6535
		mu 0 4 3552 412 2663 3551
		f 4 -1091 1824 6536 -6536
		mu 0 4 2663 652 1975 3551
		f 4 3425 -6529 6537 -6537
		mu 0 4 1975 1039 3550 3551
		f 4 4105 426 6540 -6540
		mu 0 4 2396 261 2666 3553
		f 4 4627 1827 6541 -6541
		mu 0 4 2666 655 3554 3553
		f 4 6538 -3433 6542 -6542
		mu 0 4 3554 1042 1979 3553
		f 4 2519 687 6545 -6545
		mu 0 4 1408 414 1982 3555
		f 4 3438 1828 6546 -6546
		mu 0 4 1982 1043 3556 3555
		f 4 6543 -6539 6547 -6547
		mu 0 4 3556 1042 3554 3555
		f 4 6548 -6534 6550 -6550
		mu 0 4 3558 412 3552 3557
		f 4 -1821 1829 6551 -6551
		mu 0 4 3552 1041 1980 3557
		f 4 3433 -6544 6552 -6552
		mu 0 4 1980 1042 3556 3557
		f 4 6553 -6508 6556 -6556
		mu 0 4 3560 262 3541 3559
		f 4 -1815 1832 6557 -6557
		mu 0 4 3541 1038 3561 3559
		f 4 6554 -3441 6558 -6558
		mu 0 4 3561 1044 1984 3559
		f 4 -679 688 6561 -6561
		mu 0 4 1968 411 1402 3562
		f 4 2510 1833 6562 -6562
		mu 0 4 1402 653 3563 3562
		f 4 6559 -6555 6563 -6563
		mu 0 4 3563 1044 3561 3562
		f 4 4622 -6549 6565 -6565
		mu 0 4 2664 412 3558 3564
		f 4 -1826 1834 6566 -6566
		mu 0 4 3558 1043 1985 3564
		f 4 3441 -6560 6567 -6567
		mu 0 4 1985 1044 3563 3564
		f 4 6330 60 6571 -6571
		mu 0 4 3463 47 3566 3565
		f 4 6568 1838 6572 -6572
		mu 0 4 3566 1047 3567 3565
		f 4 6569 -3449 6573 -6573
		mu 0 4 3567 1046 1989 3565
		f 4 6574 692 6577 -6577
		mu 0 4 3569 416 1992 3568
		f 4 3454 1839 6578 -6578
		mu 0 4 1992 1048 3570 3568
		f 4 6575 -6570 6579 -6579
		mu 0 4 3570 1046 3567 3568
		f 4 6580 -4640 6582 -6582
		mu 0 4 3572 415 2672 3571
		f 4 -1099 1840 6583 -6583
		mu 0 4 2672 657 1990 3571
		f 4 3449 -6576 6584 -6584
		mu 0 4 1990 1046 3570 3571
		f 4 6585 427 6588 -6588
		mu 0 4 3574 267 2675 3573
		f 4 4645 1844 6589 -6589
		mu 0 4 2675 660 3575 3573
		f 4 6586 -3457 6590 -6590
		mu 0 4 3575 1049 1994 3573
		f 4 2527 693 6593 -6593
		mu 0 4 1413 417 1997 3576
		f 4 3462 1845 6594 -6594
		mu 0 4 1997 1051 3577 3576
		f 4 6591 -6587 6595 -6595
		mu 0 4 3577 1049 3575 3576
		f 4 6596 -6581 6598 -6598
		mu 0 4 3579 415 3572 3578
		f 4 -1837 1846 6599 -6599
		mu 0 4 3572 1048 1995 3578
		f 4 3457 -6592 6600 -6600
		mu 0 4 1995 1049 3577 3578
		f 4 6601 -6554 6604 -6604
		mu 0 4 3581 262 3560 3580
		f 4 -1831 1849 6605 -6605
		mu 0 4 3560 1045 3582 3580
		f 4 6602 -3465 6606 -6606
		mu 0 4 3582 1052 1999 3580
		f 4 -685 694 6609 -6609
		mu 0 4 1983 414 1407 3583
		f 4 2518 1850 6610 -6610
		mu 0 4 1407 658 3584 3583
		f 4 6607 -6603 6611 -6611
		mu 0 4 3584 1052 3582 3583
		f 4 4640 -6597 6613 -6613
		mu 0 4 2673 415 3579 3585
		f 4 -1843 1851 6614 -6614
		mu 0 4 3579 1051 2000 3585
		f 4 3465 -6608 6615 -6615
		mu 0 4 2000 1052 3584 3585
		f 4 6616 -6618 6620 -6620
		mu 0 4 3587 48 3588 3586
		f 4 -1854 1856 6621 -6621
		mu 0 4 3588 1056 3589 3586
		f 4 6618 -3473 6622 -6622
		mu 0 4 3589 1054 2004 3586
		f 4 -697 698 6625 -6625
		mu 0 4 2078 420 2007 3590
		f 4 3478 1857 6626 -6626
		mu 0 4 2007 1057 3591 3590
		f 4 6623 -6619 6627 -6627
		mu 0 4 3591 1054 3589 3590
		f 4 6628 -4659 6630 -6630
		mu 0 4 3593 418 2682 3592
		f 4 -1107 1858 6631 -6631
		mu 0 4 2682 662 2005 3592
		f 4 3473 -6624 6632 -6632
		mu 0 4 2005 1054 3591 3592
		f 4 -422 428 6635 -6635
		mu 0 4 2726 268 2404 3594
		f 4 4122 1861 6636 -6636
		mu 0 4 2404 520 3595 3594
		f 4 6633 -3481 6637 -6637
		mu 0 4 3595 1058 2009 3594
		f 4 2303 699 6640 -6640
		mu 0 4 1273 315 2012 3596
		f 4 3486 1862 6641 -6641
		mu 0 4 2012 1059 3597 3596
		f 4 6638 -6634 6642 -6642
		mu 0 4 3597 1058 3595 3596
		f 4 6643 -6629 6645 -6645
		mu 0 4 3599 418 3593 3598
		f 4 -1855 1863 6646 -6646
		mu 0 4 3593 1057 2010 3598
		f 4 3481 -6639 6647 -6647
		mu 0 4 2010 1058 3597 3598
		f 4 5298 -6602 6650 -6650
		mu 0 4 3007 262 3581 3600
		f 4 -1848 1865 6651 -6651
		mu 0 4 3581 1053 3601 3600
		f 4 6648 -3489 6652 -6652
		mu 0 4 3601 1060 2014 3600
		f 4 -691 700 6655 -6655
		mu 0 4 1998 417 1412 3602
		f 4 2526 1866 6656 -6656
		mu 0 4 1412 663 3603 3602
		f 4 6653 -6649 6657 -6657
		mu 0 4 3603 1060 3601 3602
		f 4 4659 -6644 6659 -6659
		mu 0 4 2683 418 3599 3604
		f 4 -1860 1867 6660 -6660
		mu 0 4 3599 1059 2015 3604
		f 4 3489 -6654 6661 -6661
		mu 0 4 2015 1060 3603 3604
		f 4 -277 -6664 6666 -6666
		mu 0 4 3606 189 3607 3605
		f 4 -1870 1872 6667 -6667
		mu 0 4 3607 1063 3608 3605
		f 4 6664 -3497 6668 -6668
		mu 0 4 3608 1061 2019 3605
		f 4 -703 705 6672 -6672
		mu 0 4 3610 423 2022 3609
		f 4 3502 1873 6673 -6673
		mu 0 4 2022 1064 3611 3609
		f 4 6670 -6665 6674 -6674
		mu 0 4 3611 1061 3608 3609
		f 4 6675 -4678 6677 -6677
		mu 0 4 3613 421 2692 3612
		f 4 -1115 1874 6678 -6678
		mu 0 4 2692 667 2020 3612
		f 4 3497 -6671 6679 -6679
		mu 0 4 2020 1061 3611 3612
		f 4 -431 436 6683 -6683
		mu 0 4 3615 271 2695 3614
		f 4 4683 1878 6684 -6684
		mu 0 4 2695 670 3616 3614
		f 4 6681 -3505 6685 -6685
		mu 0 4 3616 1065 2024 3614
		f 4 2543 706 6688 -6688
		mu 0 4 1423 424 2027 3617
		f 4 3510 1879 6689 -6689
		mu 0 4 2027 1067 3618 3617
		f 4 6686 -6682 6690 -6690
		mu 0 4 3618 1065 3616 3617
		f 4 6691 -6676 6693 -6693
		mu 0 4 3620 421 3613 3619
		f 4 -1871 1880 6694 -6694
		mu 0 4 3613 1064 2025 3619
		f 4 3505 -6687 6695 -6695
		mu 0 4 2025 1065 3618 3619
		f 4 6696 -5348 6699 -6699
		mu 0 4 3622 269 3029 3621
		f 4 -1393 1883 6700 -6700
		mu 0 4 3029 832 3623 3621
		f 4 6697 -3513 6701 -6701
		mu 0 4 3623 1068 2029 3621
		f 4 -515 707 6704 -6704
		mu 0 4 1608 319 1417 3624
		f 4 2534 1884 6705 -6705
		mu 0 4 1417 668 3625 3624
		f 4 6702 -6698 6706 -6706
		mu 0 4 3625 1068 3623 3624
		f 4 4678 -6692 6708 -6708
		mu 0 4 2693 421 3620 3626
		f 4 -1877 1885 6709 -6709
		mu 0 4 3620 1067 2030 3626
		f 4 3513 -6703 6710 -6710
		mu 0 4 2030 1068 3625 3626
		f 4 -103 -6713 6715 -6715
		mu 0 4 3628 76 3629 3627
		f 4 -1888 1890 6716 -6716
		mu 0 4 3629 1072 3630 3627
		f 4 6713 -3521 6717 -6717
		mu 0 4 3630 1070 2034 3627
		f 4 -710 712 6721 -6721
		mu 0 4 3632 427 2037 3631
		f 4 3526 1891 6722 -6722
		mu 0 4 2037 1073 3633 3631
		f 4 6719 -6714 6723 -6723
		mu 0 4 3633 1070 3630 3631
		f 4 6724 -4697 6726 -6726
		mu 0 4 3635 425 2702 3634
		f 4 -1123 1892 6727 -6727
		mu 0 4 2702 672 2035 3634
		f 4 3521 -6720 6728 -6728
		mu 0 4 2035 1070 3633 3634
		f 4 -432 437 6732 -6732
		mu 0 4 3637 272 2705 3636
		f 4 4702 1896 6733 -6733
		mu 0 4 2705 675 3638 3636
		f 4 6730 -3529 6734 -6734
		mu 0 4 3638 1074 2039 3636
		f 4 2551 713 6737 -6737
		mu 0 4 1428 428 2042 3639
		f 4 3534 1897 6738 -6738
		mu 0 4 2042 1076 3640 3639
		f 4 6735 -6731 6739 -6739
		mu 0 4 3640 1074 3638 3639
		f 4 6740 -6725 6742 -6742
		mu 0 4 3642 425 3635 3641
		f 4 -1889 1898 6743 -6743
		mu 0 4 3635 1073 2040 3641
		f 4 3529 -6736 6744 -6744
		mu 0 4 2040 1074 3640 3641
		f 4 6745 -6697 6748 -6748
		mu 0 4 3644 269 3622 3643
		f 4 -1882 1901 6749 -6749
		mu 0 4 3622 1069 3645 3643
		f 4 6746 -3537 6750 -6750
		mu 0 4 3645 1077 2044 3643
		f 4 -704 714 6753 -6753
		mu 0 4 2028 424 1422 3646
		f 4 2542 1902 6754 -6754
		mu 0 4 1422 673 3647 3646
		f 4 6751 -6747 6755 -6755
		mu 0 4 3647 1077 3645 3646
		f 4 4697 -6741 6757 -6757
		mu 0 4 2703 425 3642 3648
		f 4 -1895 1903 6758 -6758
		mu 0 4 3642 1076 2045 3648
		f 4 3537 -6752 6759 -6759
		mu 0 4 2045 1077 3647 3648
		f 4 -271 -6762 6764 -6764
		mu 0 4 3650 186 3651 3649
		f 4 -1906 1908 6765 -6765
		mu 0 4 3651 1081 3652 3649
		f 4 6762 -3545 6766 -6766
		mu 0 4 3652 1079 2049 3649
		f 4 -717 719 6770 -6770
		mu 0 4 3654 431 2052 3653
		f 4 3550 1909 6771 -6771
		mu 0 4 2052 1082 3655 3653
		f 4 6768 -6763 6772 -6772
		mu 0 4 3655 1079 3652 3653
		f 4 6773 -4716 6775 -6775
		mu 0 4 3657 429 2712 3656
		f 4 -1131 1910 6776 -6776
		mu 0 4 2712 677 2050 3656
		f 4 3545 -6769 6777 -6777
		mu 0 4 2050 1079 3655 3656
		f 4 -433 438 6781 -6781
		mu 0 4 3659 273 2715 3658
		f 4 4721 1914 6782 -6782
		mu 0 4 2715 680 3660 3658
		f 4 6779 -3553 6783 -6783
		mu 0 4 3660 1083 2054 3658
		f 4 2559 720 6786 -6786
		mu 0 4 1433 432 2057 3661
		f 4 3558 1915 6787 -6787
		mu 0 4 2057 1085 3662 3661
		f 4 6784 -6780 6788 -6788
		mu 0 4 3662 1083 3660 3661
		f 4 6789 -6774 6791 -6791
		mu 0 4 3664 429 3657 3663
		f 4 -1907 1916 6792 -6792
		mu 0 4 3657 1082 2055 3663
		f 4 3553 -6785 6793 -6793
		mu 0 4 2055 1083 3662 3663
		f 4 6794 -6746 6797 -6797
		mu 0 4 3666 269 3644 3665
		f 4 -1900 1919 6798 -6798
		mu 0 4 3644 1078 3667 3665
		f 4 6795 -3561 6799 -6799
		mu 0 4 3667 1086 2059 3665
		f 4 -711 721 6802 -6802
		mu 0 4 2043 428 1427 3668
		f 4 2550 1920 6803 -6803
		mu 0 4 1427 678 3669 3668
		f 4 6800 -6796 6804 -6804
		mu 0 4 3669 1086 3667 3668
		f 4 4716 -6790 6806 -6806
		mu 0 4 2713 429 3664 3670
		f 4 -1913 1921 6807 -6807
		mu 0 4 3664 1085 2060 3670
		f 4 3561 -6801 6808 -6808
		mu 0 4 2060 1086 3669 3670
		f 4 -43 59 6812 -6812
		mu 0 4 3672 40 2990 3671
		f 4 5263 1925 6813 -6813
		mu 0 4 2990 815 3673 3671
		f 4 6810 -3569 6814 -6814
		mu 0 4 3673 1088 2064 3671
		f 4 2799 725 6817 -6817
		mu 0 4 1583 312 2067 3674
		f 4 3574 1926 6818 -6818
		mu 0 4 2067 1090 3675 3674
		f 4 6815 -6811 6819 -6819
		mu 0 4 3675 1088 3673 3674
		f 4 6820 -4735 6822 -6822
		mu 0 4 3677 433 2722 3676
		f 4 -1139 1927 6823 -6823
		mu 0 4 2722 682 2065 3676
		f 4 3569 -6816 6824 -6824
		mu 0 4 2065 1088 3675 3676
		f 4 4123 439 6827 -6827
		mu 0 4 2405 268 2725 3678
		f 4 4740 1930 6828 -6828
		mu 0 4 2725 685 3679 3678
		f 4 6825 -3577 6829 -6829
		mu 0 4 3679 1091 2069 3678
		f 4 2567 726 6832 -6832
		mu 0 4 1438 435 2072 3680
		f 4 3582 1931 6833 -6833
		mu 0 4 2072 1092 3681 3680
		f 4 6830 -6826 6834 -6834
		mu 0 4 3681 1091 3679 3680
		f 4 6835 -6821 6837 -6837
		mu 0 4 3683 433 3677 3682
		f 4 -1924 1932 6838 -6838
		mu 0 4 3677 1090 2070 3682
		f 4 3577 -6831 6839 -6839
		mu 0 4 2070 1091 3681 3682
		f 4 6840 -6795 6843 -6843
		mu 0 4 3685 269 3666 3684
		f 4 -1918 1935 6844 -6844
		mu 0 4 3666 1087 3686 3684
		f 4 6841 -3585 6845 -6845
		mu 0 4 3686 1093 2074 3684
		f 4 -718 727 6848 -6848
		mu 0 4 2058 432 1432 3687
		f 4 2558 1936 6849 -6849
		mu 0 4 1432 683 3688 3687
		f 4 6846 -6842 6850 -6850
		mu 0 4 3688 1093 3686 3687
		f 4 4735 -6836 6852 -6852
		mu 0 4 2723 433 3683 3689
		f 4 -1929 1937 6853 -6853
		mu 0 4 3683 1092 2075 3689
		f 4 3585 -6847 6854 -6854
		mu 0 4 2075 1093 3688 3689
		f 4 6617 63 6858 -6858
		mu 0 4 3588 48 3691 3690
		f 4 6855 1941 6859 -6859
		mu 0 4 3691 1096 3692 3690
		f 4 6856 -3593 6860 -6860
		mu 0 4 3692 1095 2079 3690
		f 4 6861 731 6864 -6864
		mu 0 4 3694 437 2082 3693
		f 4 3598 1942 6865 -6865
		mu 0 4 2082 1097 3695 3693
		f 4 6862 -6857 6866 -6866
		mu 0 4 3695 1095 3692 3693
		f 4 6867 -4753 6869 -6869
		mu 0 4 3697 436 2731 3696
		f 4 -1147 1943 6870 -6870
		mu 0 4 2731 687 2080 3696
		f 4 3593 -6863 6871 -6871
		mu 0 4 2080 1095 3695 3696
		f 4 6872 440 6875 -6875
		mu 0 4 3699 274 2734 3698
		f 4 4758 1947 6876 -6876
		mu 0 4 2734 690 3700 3698
		f 4 6873 -3601 6877 -6877
		mu 0 4 3700 1098 2084 3698
		f 4 2575 732 6880 -6880
		mu 0 4 1443 438 2087 3701
		f 4 3606 1948 6881 -6881
		mu 0 4 2087 1100 3702 3701
		f 4 6878 -6874 6882 -6882
		mu 0 4 3702 1098 3700 3701
		f 4 6883 -6868 6885 -6885
		mu 0 4 3704 436 3697 3703
		f 4 -1940 1949 6886 -6886
		mu 0 4 3697 1097 2085 3703
		f 4 3601 -6879 6887 -6887
		mu 0 4 2085 1098 3702 3703
		f 4 6888 -6841 6891 -6891
		mu 0 4 3706 269 3685 3705
		f 4 -1934 1952 6892 -6892
		mu 0 4 3685 1094 3707 3705
		f 4 6889 -3609 6893 -6893
		mu 0 4 3707 1101 2089 3705
		f 4 -724 733 6896 -6896
		mu 0 4 2073 435 1437 3708
		f 4 2566 1953 6897 -6897
		mu 0 4 1437 688 3709 3708
		f 4 6894 -6890 6898 -6898
		mu 0 4 3709 1101 3707 3708
		f 4 4753 -6884 6900 -6900
		mu 0 4 2732 436 3704 3710
		f 4 -1946 1954 6901 -6901
		mu 0 4 3704 1100 2090 3710
		f 4 3609 -6895 6902 -6902
		mu 0 4 2090 1101 3709 3710
		f 4 6903 -6905 6907 -6907
		mu 0 4 3712 49 3713 3711
		f 4 -1957 1959 6908 -6908
		mu 0 4 3713 1105 3714 3711
		f 4 6905 -3617 6909 -6909
		mu 0 4 3714 1103 2094 3711
		f 4 -736 737 6912 -6912
		mu 0 4 2168 441 2097 3715
		f 4 3622 1960 6913 -6913
		mu 0 4 2097 1106 3716 3715
		f 4 6910 -6906 6914 -6914
		mu 0 4 3716 1103 3714 3715
		f 4 6915 -4772 6917 -6917
		mu 0 4 3718 439 2741 3717
		f 4 -1155 1961 6918 -6918
		mu 0 4 2741 692 2095 3717
		f 4 3617 -6911 6919 -6919
		mu 0 4 2095 1103 3716 3717
		f 4 -435 441 6922 -6922
		mu 0 4 2785 275 2413 3719
		f 4 4140 1964 6923 -6923
		mu 0 4 2413 525 3720 3719
		f 4 6920 -3625 6924 -6924
		mu 0 4 3720 1107 2099 3719
		f 4 2311 738 6927 -6927
		mu 0 4 1278 320 2102 3721
		f 4 3630 1965 6928 -6928
		mu 0 4 2102 1108 3722 3721
		f 4 6925 -6921 6929 -6929
		mu 0 4 3722 1107 3720 3721
		f 4 6930 -6916 6932 -6932
		mu 0 4 3724 439 3718 3723
		f 4 -1958 1966 6933 -6933
		mu 0 4 3718 1106 2100 3723
		f 4 3625 -6926 6934 -6934
		mu 0 4 2100 1107 3722 3723
		f 4 5348 -6889 6937 -6937
		mu 0 4 3030 269 3706 3725
		f 4 -1951 1968 6938 -6938
		mu 0 4 3706 1102 3726 3725
		f 4 6935 -3633 6939 -6939
		mu 0 4 3726 1109 2104 3725
		f 4 -730 739 6942 -6942
		mu 0 4 2088 438 1442 3727
		f 4 2574 1969 6943 -6943
		mu 0 4 1442 693 3728 3727
		f 4 6940 -6936 6944 -6944
		mu 0 4 3728 1109 3726 3727
		f 4 4772 -6931 6946 -6946
		mu 0 4 2742 439 3724 3729
		f 4 -1963 1970 6947 -6947
		mu 0 4 3724 1108 2105 3729
		f 4 3633 -6941 6948 -6948
		mu 0 4 2105 1109 3728 3729
		f 4 -295 -6951 6953 -6953
		mu 0 4 3731 199 3732 3730
		f 4 -1973 1975 6954 -6954
		mu 0 4 3732 1112 3733 3730
		f 4 6951 -3641 6955 -6955
		mu 0 4 3733 1110 2109 3730
		f 4 -742 744 6959 -6959
		mu 0 4 3735 444 2112 3734
		f 4 3646 1976 6960 -6960
		mu 0 4 2112 1113 3736 3734
		f 4 6957 -6952 6961 -6961
		mu 0 4 3736 1110 3733 3734
		f 4 6962 -4791 6964 -6964
		mu 0 4 3738 442 2751 3737
		f 4 -1163 1977 6965 -6965
		mu 0 4 2751 697 2110 3737
		f 4 3641 -6958 6966 -6966
		mu 0 4 2110 1110 3736 3737
		f 4 -444 449 6970 -6970
		mu 0 4 3740 278 2754 3739
		f 4 4796 1981 6971 -6971
		mu 0 4 2754 700 3741 3739
		f 4 6968 -3649 6972 -6972
		mu 0 4 3741 1114 2114 3739
		f 4 2591 745 6975 -6975
		mu 0 4 1453 445 2117 3742
		f 4 3654 1982 6976 -6976
		mu 0 4 2117 1116 3743 3742
		f 4 6973 -6969 6977 -6977
		mu 0 4 3743 1114 3741 3742
		f 4 6978 -6963 6980 -6980
		mu 0 4 3745 442 3738 3744
		f 4 -1974 1983 6981 -6981
		mu 0 4 3738 1113 2115 3744
		f 4 3649 -6974 6982 -6982
		mu 0 4 2115 1114 3743 3744
		f 4 6983 -5398 6986 -6986
		mu 0 4 3747 276 3052 3746
		f 4 -1412 1986 6987 -6987
		mu 0 4 3052 842 3748 3746
		f 4 6984 -3657 6988 -6988
		mu 0 4 3748 1117 2119 3746
		f 4 -523 746 6991 -6991
		mu 0 4 1623 324 1447 3749
		f 4 2582 1987 6992 -6992
		mu 0 4 1447 698 3750 3749
		f 4 6989 -6985 6993 -6993
		mu 0 4 3750 1117 3748 3749
		f 4 4791 -6979 6995 -6995
		mu 0 4 2752 442 3745 3751
		f 4 -1980 1988 6996 -6996
		mu 0 4 3745 1116 2120 3751
		f 4 3657 -6990 6997 -6997
		mu 0 4 2120 1117 3750 3751
		f 4 -110 -7000 7002 -7002
		mu 0 4 3753 81 3754 3752
		f 4 -1991 1993 7003 -7003
		mu 0 4 3754 1121 3755 3752
		f 4 7000 -3665 7004 -7004
		mu 0 4 3755 1119 2124 3752
		f 4 -749 751 7008 -7008
		mu 0 4 3757 448 2127 3756
		f 4 3670 1994 7009 -7009
		mu 0 4 2127 1122 3758 3756
		f 4 7006 -7001 7010 -7010
		mu 0 4 3758 1119 3755 3756
		f 4 7011 -4810 7013 -7013
		mu 0 4 3760 446 2761 3759
		f 4 -1171 1995 7014 -7014
		mu 0 4 2761 702 2125 3759
		f 4 3665 -7007 7015 -7015
		mu 0 4 2125 1119 3758 3759
		f 4 -445 450 7019 -7019
		mu 0 4 3762 279 2764 3761
		f 4 4815 1999 7020 -7020
		mu 0 4 2764 705 3763 3761
		f 4 7017 -3673 7021 -7021
		mu 0 4 3763 1123 2129 3761
		f 4 2599 752 7024 -7024
		mu 0 4 1458 449 2132 3764
		f 4 3678 2000 7025 -7025
		mu 0 4 2132 1125 3765 3764
		f 4 7022 -7018 7026 -7026
		mu 0 4 3765 1123 3763 3764
		f 4 7027 -7012 7029 -7029
		mu 0 4 3767 446 3760 3766
		f 4 -1992 2001 7030 -7030
		mu 0 4 3760 1122 2130 3766
		f 4 3673 -7023 7031 -7031
		mu 0 4 2130 1123 3765 3766
		f 4 7032 -6984 7035 -7035
		mu 0 4 3769 276 3747 3768
		f 4 -1985 2004 7036 -7036
		mu 0 4 3747 1118 3770 3768
		f 4 7033 -3681 7037 -7037
		mu 0 4 3770 1126 2134 3768
		f 4 -743 753 7040 -7040
		mu 0 4 2118 445 1452 3771
		f 4 2590 2005 7041 -7041
		mu 0 4 1452 703 3772 3771
		f 4 7038 -7034 7042 -7042
		mu 0 4 3772 1126 3770 3771
		f 4 4810 -7028 7044 -7044
		mu 0 4 2762 446 3767 3773
		f 4 -1998 2006 7045 -7045
		mu 0 4 3767 1125 2135 3773
		f 4 3681 -7039 7046 -7046
		mu 0 4 2135 1126 3772 3773
		f 4 -289 -7049 7051 -7051
		mu 0 4 3775 196 3776 3774
		f 4 -2009 2011 7052 -7052
		mu 0 4 3776 1130 3777 3774
		f 4 7049 -3689 7053 -7053
		mu 0 4 3777 1128 2139 3774
		f 4 -756 758 7057 -7057
		mu 0 4 3779 452 2142 3778
		f 4 3694 2012 7058 -7058
		mu 0 4 2142 1131 3780 3778
		f 4 7055 -7050 7059 -7059
		mu 0 4 3780 1128 3777 3778
		f 4 7060 -4829 7062 -7062
		mu 0 4 3782 450 2771 3781
		f 4 -1179 2013 7063 -7063
		mu 0 4 2771 707 2140 3781
		f 4 3689 -7056 7064 -7064
		mu 0 4 2140 1128 3780 3781
		f 4 -446 451 7068 -7068
		mu 0 4 3784 280 2774 3783
		f 4 4834 2017 7069 -7069
		mu 0 4 2774 710 3785 3783
		f 4 7066 -3697 7070 -7070
		mu 0 4 3785 1132 2144 3783
		f 4 2607 759 7073 -7073
		mu 0 4 1463 453 2147 3786
		f 4 3702 2018 7074 -7074
		mu 0 4 2147 1134 3787 3786
		f 4 7071 -7067 7075 -7075
		mu 0 4 3787 1132 3785 3786
		f 4 7076 -7061 7078 -7078
		mu 0 4 3789 450 3782 3788
		f 4 -2010 2019 7079 -7079
		mu 0 4 3782 1131 2145 3788
		f 4 3697 -7072 7080 -7080
		mu 0 4 2145 1132 3787 3788
		f 4 7081 -7033 7084 -7084
		mu 0 4 3791 276 3769 3790
		f 4 -2003 2022 7085 -7085
		mu 0 4 3769 1127 3792 3790
		f 4 7082 -3705 7086 -7086
		mu 0 4 3792 1135 2149 3790
		f 4 -750 760 7089 -7089
		mu 0 4 2133 449 1457 3793
		f 4 2598 2023 7090 -7090
		mu 0 4 1457 708 3794 3793
		f 4 7087 -7083 7091 -7091
		mu 0 4 3794 1135 3792 3793
		f 4 4829 -7077 7093 -7093
		mu 0 4 2772 450 3789 3795
		f 4 -2016 2024 7094 -7094
		mu 0 4 3789 1134 2150 3795
		f 4 3705 -7088 7095 -7095
		mu 0 4 2150 1135 3794 3795
		f 4 -45 62 7099 -7099
		mu 0 4 3797 41 3013 3796
		f 4 5313 2028 7100 -7100
		mu 0 4 3013 825 3798 3796
		f 4 7097 -3713 7101 -7101
		mu 0 4 3798 1137 2154 3796
		f 4 2823 764 7104 -7104
		mu 0 4 1598 317 2157 3799
		f 4 3718 2029 7105 -7105
		mu 0 4 2157 1139 3800 3799
		f 4 7102 -7098 7106 -7106
		mu 0 4 3800 1137 3798 3799
		f 4 7107 -4848 7109 -7109
		mu 0 4 3802 454 2781 3801
		f 4 -1187 2030 7110 -7110
		mu 0 4 2781 712 2155 3801
		f 4 3713 -7103 7111 -7111
		mu 0 4 2155 1137 3800 3801
		f 4 4141 452 7114 -7114
		mu 0 4 2414 275 2784 3803
		f 4 4853 2033 7115 -7115
		mu 0 4 2784 715 3804 3803
		f 4 7112 -3721 7116 -7116
		mu 0 4 3804 1140 2159 3803
		f 4 2615 765 7119 -7119
		mu 0 4 1468 456 2162 3805
		f 4 3726 2034 7120 -7120
		mu 0 4 2162 1141 3806 3805
		f 4 7117 -7113 7121 -7121
		mu 0 4 3806 1140 3804 3805
		f 4 7122 -7108 7124 -7124
		mu 0 4 3808 454 3802 3807
		f 4 -2027 2035 7125 -7125
		mu 0 4 3802 1139 2160 3807
		f 4 3721 -7118 7126 -7126
		mu 0 4 2160 1140 3806 3807
		f 4 7127 -7082 7130 -7130
		mu 0 4 3810 276 3791 3809
		f 4 -2021 2038 7131 -7131
		mu 0 4 3791 1136 3811 3809
		f 4 7128 -3729 7132 -7132
		mu 0 4 3811 1142 2164 3809
		f 4 -757 766 7135 -7135
		mu 0 4 2148 453 1462 3812
		f 4 2606 2039 7136 -7136
		mu 0 4 1462 713 3813 3812
		f 4 7133 -7129 7137 -7137
		mu 0 4 3813 1142 3811 3812
		f 4 4848 -7123 7139 -7139
		mu 0 4 2782 454 3808 3814
		f 4 -2032 2040 7140 -7140
		mu 0 4 3808 1141 2165 3814
		f 4 3729 -7134 7141 -7141
		mu 0 4 2165 1142 3813 3814
		f 4 6904 66 7145 -7145
		mu 0 4 3713 49 3816 3815
		f 4 7142 2044 7146 -7146
		mu 0 4 3816 1145 3817 3815
		f 4 7143 -3737 7147 -7147
		mu 0 4 3817 1144 2169 3815
		f 4 7148 770 7151 -7151
		mu 0 4 3819 458 2172 3818
		f 4 3742 2045 7152 -7152
		mu 0 4 2172 1146 3820 3818
		f 4 7149 -7144 7153 -7153
		mu 0 4 3820 1144 3817 3818
		f 4 7154 -4866 7156 -7156
		mu 0 4 3822 457 2790 3821
		f 4 -1195 2046 7157 -7157
		mu 0 4 2790 717 2170 3821
		f 4 3737 -7150 7158 -7158
		mu 0 4 2170 1144 3820 3821
		f 4 7159 453 7162 -7162
		mu 0 4 3824 281 2793 3823
		f 4 4871 2050 7163 -7163
		mu 0 4 2793 720 3825 3823
		f 4 7160 -3745 7164 -7164
		mu 0 4 3825 1147 2174 3823
		f 4 2623 771 7167 -7167
		mu 0 4 1473 459 2177 3826
		f 4 3750 2051 7168 -7168
		mu 0 4 2177 1149 3827 3826
		f 4 7165 -7161 7169 -7169
		mu 0 4 3827 1147 3825 3826
		f 4 7170 -7155 7172 -7172
		mu 0 4 3829 457 3822 3828
		f 4 -2043 2052 7173 -7173
		mu 0 4 3822 1146 2175 3828
		f 4 3745 -7166 7174 -7174
		mu 0 4 2175 1147 3827 3828
		f 4 7175 -7128 7178 -7178
		mu 0 4 3831 276 3810 3830
		f 4 -2037 2055 7179 -7179
		mu 0 4 3810 1143 3832 3830
		f 4 7176 -3753 7180 -7180
		mu 0 4 3832 1150 2179 3830
		f 4 -763 772 7183 -7183
		mu 0 4 2163 456 1467 3833
		f 4 2614 2056 7184 -7184
		mu 0 4 1467 718 3834 3833
		f 4 7181 -7177 7185 -7185
		mu 0 4 3834 1150 3832 3833
		f 4 4866 -7171 7187 -7187
		mu 0 4 2791 457 3829 3835
		f 4 -2049 2057 7188 -7188
		mu 0 4 3829 1149 2180 3835
		f 4 3753 -7182 7189 -7189
		mu 0 4 2180 1150 3834 3835
		f 4 7190 -7192 7194 -7194
		mu 0 4 3837 50 3838 3836
		f 4 -2060 2062 7195 -7195
		mu 0 4 3838 1154 3839 3836
		f 4 7192 -3761 7196 -7196
		mu 0 4 3839 1152 2184 3836
		f 4 -775 776 7199 -7199
		mu 0 4 2258 462 2187 3840
		f 4 3766 2063 7200 -7200
		mu 0 4 2187 1155 3841 3840
		f 4 7197 -7193 7201 -7201
		mu 0 4 3841 1152 3839 3840
		f 4 7202 -4885 7204 -7204
		mu 0 4 3843 460 2800 3842
		f 4 -1203 2064 7205 -7205
		mu 0 4 2800 722 2185 3842
		f 4 3761 -7198 7206 -7206
		mu 0 4 2185 1152 3841 3842
		f 4 -448 454 7209 -7209
		mu 0 4 2844 282 2422 3844
		f 4 4158 2067 7210 -7210
		mu 0 4 2422 530 3845 3844
		f 4 7207 -3769 7211 -7211
		mu 0 4 3845 1156 2189 3844
		f 4 2319 777 7214 -7214
		mu 0 4 1283 325 2192 3846
		f 4 3774 2068 7215 -7215
		mu 0 4 2192 1157 3847 3846
		f 4 7212 -7208 7216 -7216
		mu 0 4 3847 1156 3845 3846
		f 4 7217 -7203 7219 -7219
		mu 0 4 3849 460 3843 3848
		f 4 -2061 2069 7220 -7220
		mu 0 4 3843 1155 2190 3848
		f 4 3769 -7213 7221 -7221
		mu 0 4 2190 1156 3847 3848
		f 4 5398 -7176 7224 -7224
		mu 0 4 3053 276 3831 3850
		f 4 -2054 2071 7225 -7225
		mu 0 4 3831 1151 3851 3850
		f 4 7222 -3777 7226 -7226
		mu 0 4 3851 1158 2194 3850
		f 4 -769 778 7229 -7229
		mu 0 4 2178 459 1472 3852
		f 4 2622 2072 7230 -7230
		mu 0 4 1472 723 3853 3852
		f 4 7227 -7223 7231 -7231
		mu 0 4 3853 1158 3851 3852
		f 4 4885 -7218 7233 -7233
		mu 0 4 2801 460 3849 3854
		f 4 -2066 2073 7234 -7234
		mu 0 4 3849 1157 2195 3854
		f 4 3777 -7228 7235 -7235
		mu 0 4 2195 1158 3853 3854
		f 4 -311 -7238 7240 -7240
		mu 0 4 3856 209 3857 3855
		f 4 -2076 2078 7241 -7241
		mu 0 4 3857 1161 3858 3855
		f 4 7238 -3785 7242 -7242
		mu 0 4 3858 1159 2199 3855
		f 4 -781 783 7246 -7246
		mu 0 4 3860 465 2202 3859
		f 4 3790 2079 7247 -7247
		mu 0 4 2202 1162 3861 3859
		f 4 7244 -7239 7248 -7248
		mu 0 4 3861 1159 3858 3859
		f 4 7249 -4904 7251 -7251
		mu 0 4 3863 463 2810 3862
		f 4 -1211 2080 7252 -7252
		mu 0 4 2810 727 2200 3862
		f 4 3785 -7245 7253 -7253
		mu 0 4 2200 1159 3861 3862
		f 4 -457 462 7257 -7257
		mu 0 4 3865 285 2813 3864
		f 4 4909 2084 7258 -7258
		mu 0 4 2813 730 3866 3864
		f 4 7255 -3793 7259 -7259
		mu 0 4 3866 1163 2204 3864
		f 4 2639 784 7262 -7262
		mu 0 4 1483 466 2207 3867
		f 4 3798 2085 7263 -7263
		mu 0 4 2207 1165 3868 3867
		f 4 7260 -7256 7264 -7264
		mu 0 4 3868 1163 3866 3867
		f 4 7265 -7250 7267 -7267
		mu 0 4 3870 463 3863 3869
		f 4 -2077 2086 7268 -7268
		mu 0 4 3863 1162 2205 3869
		f 4 3793 -7261 7269 -7269
		mu 0 4 2205 1163 3868 3869
		f 4 7270 -5448 7273 -7273
		mu 0 4 3872 283 3075 3871
		f 4 -1431 2089 7274 -7274
		mu 0 4 3075 852 3873 3871
		f 4 7271 -3801 7275 -7275
		mu 0 4 3873 1166 2209 3871
		f 4 -531 785 7278 -7278
		mu 0 4 1638 329 1477 3874
		f 4 2630 2090 7279 -7279
		mu 0 4 1477 728 3875 3874
		f 4 7276 -7272 7280 -7280
		mu 0 4 3875 1166 3873 3874
		f 4 4904 -7266 7282 -7282
		mu 0 4 2811 463 3870 3876
		f 4 -2083 2091 7283 -7283
		mu 0 4 3870 1165 2210 3876
		f 4 3801 -7277 7284 -7284
		mu 0 4 2210 1166 3875 3876
		f 4 -117 -7287 7289 -7289
		mu 0 4 3878 86 3879 3877
		f 4 -2094 2096 7290 -7290
		mu 0 4 3879 1170 3880 3877
		f 4 7287 -3809 7291 -7291
		mu 0 4 3880 1168 2214 3877
		f 4 -788 790 7295 -7295
		mu 0 4 3882 469 2217 3881
		f 4 3814 2097 7296 -7296
		mu 0 4 2217 1171 3883 3881
		f 4 7293 -7288 7297 -7297
		mu 0 4 3883 1168 3880 3881
		f 4 7298 -4923 7300 -7300
		mu 0 4 3885 467 2820 3884
		f 4 -1219 2098 7301 -7301
		mu 0 4 2820 732 2215 3884
		f 4 3809 -7294 7302 -7302
		mu 0 4 2215 1168 3883 3884
		f 4 -458 463 7306 -7306
		mu 0 4 3887 286 2823 3886
		f 4 4928 2102 7307 -7307
		mu 0 4 2823 735 3888 3886
		f 4 7304 -3817 7308 -7308
		mu 0 4 3888 1172 2219 3886
		f 4 2647 791 7311 -7311
		mu 0 4 1488 470 2222 3889
		f 4 3822 2103 7312 -7312
		mu 0 4 2222 1174 3890 3889
		f 4 7309 -7305 7313 -7313
		mu 0 4 3890 1172 3888 3889
		f 4 7314 -7299 7316 -7316
		mu 0 4 3892 467 3885 3891
		f 4 -2095 2104 7317 -7317
		mu 0 4 3885 1171 2220 3891
		f 4 3817 -7310 7318 -7318
		mu 0 4 2220 1172 3890 3891
		f 4 7319 -7271 7322 -7322
		mu 0 4 3894 283 3872 3893
		f 4 -2088 2107 7323 -7323
		mu 0 4 3872 1167 3895 3893
		f 4 7320 -3825 7324 -7324
		mu 0 4 3895 1175 2224 3893
		f 4 -782 792 7327 -7327
		mu 0 4 2208 466 1482 3896
		f 4 2638 2108 7328 -7328
		mu 0 4 1482 733 3897 3896
		f 4 7325 -7321 7329 -7329
		mu 0 4 3897 1175 3895 3896
		f 4 4923 -7315 7331 -7331
		mu 0 4 2821 467 3892 3898
		f 4 -2101 2109 7332 -7332
		mu 0 4 3892 1174 2225 3898
		f 4 3825 -7326 7333 -7333
		mu 0 4 2225 1175 3897 3898
		f 4 -307 -7336 7338 -7338
		mu 0 4 3900 206 3901 3899
		f 4 -2112 2114 7339 -7339
		mu 0 4 3901 1179 3902 3899
		f 4 7336 -3833 7340 -7340
		mu 0 4 3902 1177 2229 3899
		f 4 -795 797 7344 -7344
		mu 0 4 3904 473 2232 3903
		f 4 3838 2115 7345 -7345
		mu 0 4 2232 1180 3905 3903
		f 4 7342 -7337 7346 -7346
		mu 0 4 3905 1177 3902 3903
		f 4 7347 -4942 7349 -7349
		mu 0 4 3907 471 2830 3906
		f 4 -1227 2116 7350 -7350
		mu 0 4 2830 737 2230 3906
		f 4 3833 -7343 7351 -7351
		mu 0 4 2230 1177 3905 3906
		f 4 -459 464 7355 -7355
		mu 0 4 3909 287 2833 3908
		f 4 4947 2120 7356 -7356
		mu 0 4 2833 740 3910 3908
		f 4 7353 -3841 7357 -7357
		mu 0 4 3910 1181 2234 3908
		f 4 2655 798 7360 -7360
		mu 0 4 1493 474 2237 3911
		f 4 3846 2121 7361 -7361
		mu 0 4 2237 1183 3912 3911;
	setAttr ".fc[3500:3779]"
		f 4 7358 -7354 7362 -7362
		mu 0 4 3912 1181 3910 3911
		f 4 7363 -7348 7365 -7365
		mu 0 4 3914 471 3907 3913
		f 4 -2113 2122 7366 -7366
		mu 0 4 3907 1180 2235 3913
		f 4 3841 -7359 7367 -7367
		mu 0 4 2235 1181 3912 3913
		f 4 7368 -7320 7371 -7371
		mu 0 4 3916 283 3894 3915
		f 4 -2106 2125 7372 -7372
		mu 0 4 3894 1176 3917 3915
		f 4 7369 -3849 7373 -7373
		mu 0 4 3917 1184 2239 3915
		f 4 -789 799 7376 -7376
		mu 0 4 2223 470 1487 3918
		f 4 2646 2126 7377 -7377
		mu 0 4 1487 738 3919 3918
		f 4 7374 -7370 7378 -7378
		mu 0 4 3919 1184 3917 3918
		f 4 4942 -7364 7380 -7380
		mu 0 4 2831 471 3914 3920
		f 4 -2119 2127 7381 -7381
		mu 0 4 3914 1183 2240 3920
		f 4 3849 -7375 7382 -7382
		mu 0 4 2240 1184 3919 3920
		f 4 -47 65 7386 -7386
		mu 0 4 3922 42 3036 3921
		f 4 5363 2131 7387 -7387
		mu 0 4 3036 835 3923 3921
		f 4 7384 -3857 7388 -7388
		mu 0 4 3923 1186 2244 3921
		f 4 2847 803 7391 -7391
		mu 0 4 1613 322 2247 3924
		f 4 3862 2132 7392 -7392
		mu 0 4 2247 1188 3925 3924
		f 4 7389 -7385 7393 -7393
		mu 0 4 3925 1186 3923 3924
		f 4 7394 -4961 7396 -7396
		mu 0 4 3927 475 2840 3926
		f 4 -1235 2133 7397 -7397
		mu 0 4 2840 742 2245 3926
		f 4 3857 -7390 7398 -7398
		mu 0 4 2245 1186 3925 3926
		f 4 4159 465 7401 -7401
		mu 0 4 2423 282 2843 3928
		f 4 4966 2136 7402 -7402
		mu 0 4 2843 745 3929 3928
		f 4 7399 -3865 7403 -7403
		mu 0 4 3929 1189 2249 3928
		f 4 2663 804 7406 -7406
		mu 0 4 1498 477 2252 3930
		f 4 3870 2137 7407 -7407
		mu 0 4 2252 1190 3931 3930
		f 4 7404 -7400 7408 -7408
		mu 0 4 3931 1189 3929 3930
		f 4 7409 -7395 7411 -7411
		mu 0 4 3933 475 3927 3932
		f 4 -2130 2138 7412 -7412
		mu 0 4 3927 1188 2250 3932
		f 4 3865 -7405 7413 -7413
		mu 0 4 2250 1189 3931 3932
		f 4 7414 -7369 7417 -7417
		mu 0 4 3935 283 3916 3934
		f 4 -2124 2141 7418 -7418
		mu 0 4 3916 1185 3936 3934
		f 4 7415 -3873 7419 -7419
		mu 0 4 3936 1191 2254 3934
		f 4 -796 805 7422 -7422
		mu 0 4 2238 474 1492 3937
		f 4 2654 2142 7423 -7423
		mu 0 4 1492 743 3938 3937
		f 4 7420 -7416 7424 -7424
		mu 0 4 3938 1191 3936 3937
		f 4 4961 -7410 7426 -7426
		mu 0 4 2841 475 3933 3939
		f 4 -2135 2143 7427 -7427
		mu 0 4 3933 1190 2255 3939
		f 4 3873 -7421 7428 -7428
		mu 0 4 2255 1191 3938 3939
		f 4 7191 69 7432 -7432
		mu 0 4 3838 50 3941 3940
		f 4 7429 2147 7433 -7433
		mu 0 4 3941 1194 3942 3940
		f 4 7430 -3881 7434 -7434
		mu 0 4 3942 1193 2259 3940
		f 4 7435 809 7438 -7438
		mu 0 4 3944 479 2262 3943
		f 4 3886 2148 7439 -7439
		mu 0 4 2262 1195 3945 3943
		f 4 7436 -7431 7440 -7440
		mu 0 4 3945 1193 3942 3943
		f 4 7441 -4979 7443 -7443
		mu 0 4 3947 478 2849 3946
		f 4 -1243 2149 7444 -7444
		mu 0 4 2849 747 2260 3946
		f 4 3881 -7437 7445 -7445
		mu 0 4 2260 1193 3945 3946
		f 4 7446 466 7449 -7449
		mu 0 4 3949 288 2852 3948
		f 4 4984 2153 7450 -7450
		mu 0 4 2852 750 3950 3948
		f 4 7447 -3889 7451 -7451
		mu 0 4 3950 1196 2264 3948
		f 4 2671 810 7454 -7454
		mu 0 4 1503 480 2267 3951
		f 4 3894 2154 7455 -7455
		mu 0 4 2267 1198 3952 3951
		f 4 7452 -7448 7456 -7456
		mu 0 4 3952 1196 3950 3951
		f 4 7457 -7442 7459 -7459
		mu 0 4 3954 478 3947 3953
		f 4 -2146 2155 7460 -7460
		mu 0 4 3947 1195 2265 3953
		f 4 3889 -7453 7461 -7461
		mu 0 4 2265 1196 3952 3953
		f 4 7462 -7415 7465 -7465
		mu 0 4 3956 283 3935 3955
		f 4 -2140 2158 7466 -7466
		mu 0 4 3935 1192 3957 3955
		f 4 7463 -3897 7467 -7467
		mu 0 4 3957 1199 2269 3955
		f 4 -802 811 7470 -7470
		mu 0 4 2253 477 1497 3958
		f 4 2662 2159 7471 -7471
		mu 0 4 1497 748 3959 3958
		f 4 7468 -7464 7472 -7472
		mu 0 4 3959 1199 3957 3958
		f 4 4979 -7458 7474 -7474
		mu 0 4 2850 478 3954 3960
		f 4 -2152 2160 7475 -7475
		mu 0 4 3954 1198 2270 3960
		f 4 3897 -7469 7476 -7476
		mu 0 4 2270 1199 3959 3960
		f 4 7477 -7479 7481 -7481
		mu 0 4 3962 51 3963 3961
		f 4 -2163 2165 7482 -7482
		mu 0 4 3963 1203 3964 3961
		f 4 7479 -3905 7483 -7483
		mu 0 4 3964 1201 2274 3961
		f 4 -814 815 7486 -7486
		mu 0 4 2348 483 2277 3965
		f 4 3910 2166 7487 -7487
		mu 0 4 2277 1204 3966 3965
		f 4 7484 -7480 7488 -7488
		mu 0 4 3966 1201 3964 3965
		f 4 7489 -4998 7491 -7491
		mu 0 4 3968 481 2859 3967
		f 4 -1251 2167 7492 -7492
		mu 0 4 2859 752 2275 3967
		f 4 3905 -7485 7493 -7493
		mu 0 4 2275 1201 3966 3967
		f 4 -461 467 7496 -7496
		mu 0 4 2903 289 2431 3969
		f 4 4176 2169 7497 -7497
		mu 0 4 2431 535 3970 3969
		f 4 7494 -3913 7498 -7498
		mu 0 4 3970 1205 2279 3969
		f 4 2327 816 7501 -7501
		mu 0 4 1288 330 2282 3971
		f 4 3916 2170 7502 -7502
		mu 0 4 2282 1206 3972 3971
		f 4 7499 -7495 7503 -7503
		mu 0 4 3972 1205 3970 3971
		f 4 7504 -7490 7506 -7506
		mu 0 4 3974 481 3968 3973
		f 4 -2164 2171 7507 -7507
		mu 0 4 3968 1204 2280 3973
		f 4 7830 -7848 7821 -7849
		mu 0 4 4115 4114 4108 4111
		f 4 5448 -7463 7510 -7510
		mu 0 4 3076 283 3956 3975
		f 4 -2157 2173 7511 -7511
		mu 0 4 3956 1200 3976 3975
		f 4 7508 -3919 7512 -7512
		mu 0 4 3976 1207 2284 3975
		f 4 -808 817 7514 -7514
		mu 0 4 2268 480 1502 3977
		f 4 2670 2174 7515 -7515
		mu 0 4 1502 753 3978 3977
		f 4 7851 -7854 7855 -7857
		mu 0 4 4125 4122 4123 4124
		f 4 4998 -7505 7518 -7518
		mu 0 4 2860 481 3974 3979
		f 4 -7819 7858 7860 -7862
		mu 0 4 4110 4109 4126 4127
		f 4 7862 -7852 7863 -7861
		mu 0 4 4126 4122 4125 4127
		f 4 -326 -7522 7524 -7524
		mu 0 4 3981 219 3982 3980
		f 4 -2178 2180 7525 -7525
		mu 0 4 3982 1210 3983 3980
		f 4 7522 -3927 7526 -7526
		mu 0 4 3983 1208 2289 3980
		f 4 -820 822 7530 -7530
		mu 0 4 3985 486 2292 3984
		f 4 3932 2181 7531 -7531
		mu 0 4 2292 1211 3986 3984
		f 4 7528 -7523 7532 -7532
		mu 0 4 3986 1208 3983 3984
		f 4 7533 -5017 7535 -7535
		mu 0 4 3988 484 2869 3987
		f 4 -1259 2182 7536 -7536
		mu 0 4 2869 757 2290 3987
		f 4 3927 -7529 7537 -7537
		mu 0 4 2290 1208 3986 3987
		f 4 -470 474 7541 -7541
		mu 0 4 3990 292 2872 3989
		f 4 5022 2186 7542 -7542
		mu 0 4 2872 760 3991 3989
		f 4 7539 -3935 7543 -7543
		mu 0 4 3991 1212 2294 3989
		f 4 2687 823 7546 -7546
		mu 0 4 1513 487 2297 3992
		f 4 3940 2187 7547 -7547
		mu 0 4 2297 1214 3993 3992
		f 4 7544 -7540 7548 -7548
		mu 0 4 3993 1212 3991 3992
		f 4 7549 -7534 7551 -7551
		mu 0 4 3995 484 3988 3994
		f 4 -2179 2188 7552 -7552
		mu 0 4 3988 1211 2295 3994
		f 4 3935 -7545 7553 -7553
		mu 0 4 2295 1212 3993 3994
		f 4 7554 -5498 7557 -7557
		mu 0 4 3997 290 3098 3996
		f 4 -1450 2191 7558 -7558
		mu 0 4 3098 862 3998 3996
		f 4 7555 -3943 7559 -7559
		mu 0 4 3998 1215 2299 3996
		f 4 -539 824 7562 -7562
		mu 0 4 1653 334 1507 3999
		f 4 2678 2192 7563 -7563
		mu 0 4 1507 758 4000 3999
		f 4 7560 -7556 7564 -7564
		mu 0 4 4000 1215 3998 3999
		f 4 5017 -7550 7566 -7566
		mu 0 4 2870 484 3995 4001
		f 4 -2185 2193 7567 -7567
		mu 0 4 3995 1214 2300 4001
		f 4 3943 -7561 7568 -7568
		mu 0 4 2300 1215 4000 4001
		f 4 -123 -7571 7573 -7573
		mu 0 4 4003 91 4004 4002
		f 4 -2196 2198 7574 -7574
		mu 0 4 4004 1219 4005 4002
		f 4 7571 -3951 7575 -7575
		mu 0 4 4005 1217 2304 4002
		f 4 -827 829 7579 -7579
		mu 0 4 4007 490 2307 4006
		f 4 3956 2199 7580 -7580
		mu 0 4 2307 1220 4008 4006
		f 4 7577 -7572 7581 -7581
		mu 0 4 4008 1217 4005 4006
		f 4 7582 -5036 7584 -7584
		mu 0 4 4010 488 2879 4009
		f 4 -1267 2200 7585 -7585
		mu 0 4 2879 762 2305 4009
		f 4 3951 -7578 7586 -7586
		mu 0 4 2305 1217 4008 4009
		f 4 -471 475 7590 -7590
		mu 0 4 4012 293 2882 4011
		f 4 5041 2204 7591 -7591
		mu 0 4 2882 765 4013 4011
		f 4 7588 -3959 7592 -7592
		mu 0 4 4013 1221 2309 4011
		f 4 2695 830 7595 -7595
		mu 0 4 1518 491 2312 4014
		f 4 3964 2205 7596 -7596
		mu 0 4 2312 1223 4015 4014
		f 4 7593 -7589 7597 -7597
		mu 0 4 4015 1221 4013 4014
		f 4 7598 -7583 7600 -7600
		mu 0 4 4017 488 4010 4016
		f 4 -2197 2206 7601 -7601
		mu 0 4 4010 1220 2310 4016
		f 4 3959 -7594 7602 -7602
		mu 0 4 2310 1221 4015 4016
		f 4 7603 -7555 7606 -7606
		mu 0 4 4019 290 3997 4018
		f 4 -2190 2209 7607 -7607
		mu 0 4 3997 1216 4020 4018
		f 4 7604 -3967 7608 -7608
		mu 0 4 4020 1224 2314 4018
		f 4 -821 831 7611 -7611
		mu 0 4 2298 487 1512 4021
		f 4 2686 2210 7612 -7612
		mu 0 4 1512 763 4022 4021
		f 4 7609 -7605 7613 -7613
		mu 0 4 4022 1224 4020 4021
		f 4 5036 -7599 7615 -7615
		mu 0 4 2880 488 4017 4023
		f 4 -2203 2211 7616 -7616
		mu 0 4 4017 1223 2315 4023
		f 4 3967 -7610 7617 -7617
		mu 0 4 2315 1224 4022 4023
		f 4 -322 -7620 7622 -7622
		mu 0 4 4025 216 4026 4024
		f 4 -2214 2216 7623 -7623
		mu 0 4 4026 1228 4027 4024
		f 4 7620 -3975 7624 -7624
		mu 0 4 4027 1226 2319 4024
		f 4 -834 836 7628 -7628
		mu 0 4 4029 494 2322 4028
		f 4 3980 2217 7629 -7629
		mu 0 4 2322 1229 4030 4028
		f 4 7626 -7621 7630 -7630
		mu 0 4 4030 1226 4027 4028
		f 4 7631 -5055 7633 -7633
		mu 0 4 4032 492 2889 4031
		f 4 -1275 2218 7634 -7634
		mu 0 4 2889 767 2320 4031
		f 4 3975 -7627 7635 -7635
		mu 0 4 2320 1226 4030 4031
		f 4 -472 476 7639 -7639
		mu 0 4 4034 294 2892 4033
		f 4 5060 2222 7640 -7640
		mu 0 4 2892 770 4035 4033
		f 4 7637 -3983 7641 -7641
		mu 0 4 4035 1230 2324 4033
		f 4 2703 837 7644 -7644
		mu 0 4 1523 495 2327 4036
		f 4 3988 2223 7645 -7645
		mu 0 4 2327 1232 4037 4036
		f 4 7642 -7638 7646 -7646
		mu 0 4 4037 1230 4035 4036
		f 4 7647 -7632 7649 -7649
		mu 0 4 4039 492 4032 4038
		f 4 -2215 2224 7650 -7650
		mu 0 4 4032 1229 2325 4038
		f 4 3983 -7643 7651 -7651
		mu 0 4 2325 1230 4037 4038
		f 4 7652 -7604 7655 -7655
		mu 0 4 4041 290 4019 4040
		f 4 -2208 2227 7656 -7656
		mu 0 4 4019 1225 4042 4040
		f 4 7653 -3991 7657 -7657
		mu 0 4 4042 1233 2329 4040
		f 4 -828 838 7660 -7660
		mu 0 4 2313 491 1517 4043
		f 4 2694 2228 7661 -7661
		mu 0 4 1517 768 4044 4043
		f 4 7658 -7654 7662 -7662
		mu 0 4 4044 1233 4042 4043
		f 4 5055 -7648 7664 -7664
		mu 0 4 2890 492 4039 4045
		f 4 -2221 2229 7665 -7665
		mu 0 4 4039 1232 2330 4045
		f 4 3991 -7659 7666 -7666
		mu 0 4 2330 1233 4044 4045
		f 4 -48 68 7670 -7670
		mu 0 4 4047 43 3059 4046
		f 4 5413 2233 7671 -7671
		mu 0 4 3059 845 4048 4046
		f 4 7668 -3999 7672 -7672
		mu 0 4 4048 1235 2334 4046
		f 4 2871 842 7675 -7675
		mu 0 4 1628 327 2337 4049
		f 4 4004 2234 7676 -7676
		mu 0 4 2337 1237 4050 4049
		f 4 7673 -7669 7677 -7677
		mu 0 4 4050 1235 4048 4049
		f 4 7678 -5074 7680 -7680
		mu 0 4 4052 496 2899 4051
		f 4 -1283 2235 7681 -7681
		mu 0 4 2899 772 2335 4051
		f 4 3999 -7674 7682 -7682
		mu 0 4 2335 1235 4050 4051
		f 4 4177 477 7685 -7685
		mu 0 4 2432 289 2902 4053
		f 4 5079 2238 7686 -7686
		mu 0 4 2902 775 4054 4053
		f 4 7683 -4007 7687 -7687
		mu 0 4 4054 1238 2339 4053
		f 4 2711 843 7690 -7690
		mu 0 4 1528 498 2342 4055
		f 4 4012 2239 7691 -7691
		mu 0 4 2342 1239 4056 4055
		f 4 7688 -7684 7692 -7692
		mu 0 4 4056 1238 4054 4055
		f 4 7693 -7679 7695 -7695
		mu 0 4 4058 496 4052 4057
		f 4 -2232 2240 7696 -7696
		mu 0 4 4052 1237 2340 4057
		f 4 4007 -7689 7697 -7697
		mu 0 4 2340 1238 4056 4057
		f 4 7698 -7653 7701 -7701
		mu 0 4 4060 290 4041 4059
		f 4 -2226 2243 7702 -7702
		mu 0 4 4041 1234 4061 4059
		f 4 7699 -4015 7703 -7703
		mu 0 4 4061 1240 2344 4059
		f 4 -835 844 7706 -7706
		mu 0 4 2328 495 1522 4062
		f 4 2702 2244 7707 -7707
		mu 0 4 1522 773 4063 4062
		f 4 7704 -7700 7708 -7708
		mu 0 4 4063 1240 4061 4062
		f 4 5074 -7694 7710 -7710
		mu 0 4 2900 496 4058 4064
		f 4 -2237 2245 7711 -7711
		mu 0 4 4058 1239 2345 4064
		f 4 4015 -7705 7712 -7712
		mu 0 4 2345 1240 4063 4064
		f 4 7478 71 7716 -7716
		mu 0 4 3963 51 4066 4065
		f 4 7713 2249 7717 -7717
		mu 0 4 4066 1243 4067 4065
		f 4 7714 -4023 7718 -7718
		mu 0 4 4067 1242 2349 4065
		f 4 7719 848 7722 -7722
		mu 0 4 4069 500 2352 4068
		f 4 4028 2250 7723 -7723
		mu 0 4 2352 1244 4070 4068
		f 4 7720 -7715 7724 -7724
		mu 0 4 4070 1242 4067 4068
		f 4 7725 -5090 7727 -7727
		mu 0 4 4072 499 2908 4071
		f 4 -1290 2251 7728 -7728
		mu 0 4 2908 777 2350 4071
		f 4 4023 -7721 7729 -7729
		mu 0 4 2350 1242 4070 4071
		f 4 7730 478 7733 -7733
		mu 0 4 4074 295 2911 4073
		f 4 5094 2255 7734 -7734
		mu 0 4 2911 780 4075 4073
		f 4 7731 -4031 7735 -7735
		mu 0 4 4075 1245 2354 4073
		f 4 2719 849 7738 -7738
		mu 0 4 1533 501 2357 4076
		f 4 4036 2256 7739 -7739
		mu 0 4 2357 1247 4077 4076
		f 4 7736 -7732 7740 -7740
		mu 0 4 4077 1245 4075 4076
		f 4 7741 -7726 7743 -7743
		mu 0 4 4079 499 4072 4078
		f 4 -2248 2257 7744 -7744
		mu 0 4 4072 1244 2355 4078
		f 4 4031 -7737 7745 -7745
		mu 0 4 2355 1245 4077 4078
		f 4 7746 -7699 7749 -7749
		mu 0 4 4081 290 4060 4080
		f 4 -2242 2260 7750 -7750
		mu 0 4 4060 1241 4082 4080
		f 4 7747 -4039 7751 -7751
		mu 0 4 4082 1248 2359 4080
		f 4 -841 850 7754 -7754
		mu 0 4 2343 498 1527 4083
		f 4 2710 2261 7755 -7755
		mu 0 4 1527 778 4084 4083
		f 4 7752 -7748 7756 -7756
		mu 0 4 4084 1248 4082 4083
		f 4 5090 -7742 7758 -7758
		mu 0 4 2909 499 4079 4085
		f 4 -2254 2262 7759 -7759
		mu 0 4 4079 1247 2360 4085
		f 4 4039 -7753 7760 -7760
		mu 0 4 2360 1248 4084 4085
		f 4 7761 -5707 7764 -7764
		mu 0 4 4087 45 3190 4086
		f 4 -1526 2266 7765 -7765
		mu 0 4 3190 899 4088 4086
		f 4 7762 -4047 7766 -7766
		mu 0 4 4088 1250 2364 4086
		f 4 -572 853 7769 -7769
		mu 0 4 1718 352 2367 4089
		f 4 4052 2267 7770 -7770
		mu 0 4 2367 1252 4090 4089
		f 4 7767 -7763 7771 -7771
		mu 0 4 4090 1250 4088 4089
		f 4 7772 -5108 7774 -7774
		mu 0 4 4092 502 2918 4091
		f 4 -1298 2268 7775 -7775
		mu 0 4 2918 782 2365 4091
		f 4 4047 -7768 7776 -7776
		mu 0 4 2365 1250 4090 4091
		f 4 -381 479 7779 -7779
		mu 0 4 2490 245 2440 4093
		f 4 4194 2271 7780 -7780
		mu 0 4 2440 540 4094 4093
		f 4 7777 -4055 7781 -7781
		mu 0 4 4094 1253 2369 4093
		f 4 2335 854 7784 -7784
		mu 0 4 1293 335 2372 4095
		f 4 4060 2272 7785 -7785
		mu 0 4 2372 1254 4096 4095
		f 4 7782 -7778 7786 -7786
		mu 0 4 4096 1253 4094 4095
		f 4 7787 -7773 7789 -7789
		mu 0 4 4098 502 4092 4097
		f 4 -2265 2273 7790 -7790
		mu 0 4 4092 1252 2370 4097
		f 4 4055 -7783 7791 -7791
		mu 0 4 2370 1253 4096 4097
		f 4 5498 -7747 7794 -7794
		mu 0 4 3099 290 4081 4099
		f 4 -2259 2275 7795 -7795
		mu 0 4 4081 1249 4100 4099
		f 4 7792 -4063 7796 -7796
		mu 0 4 4100 1255 2374 4099
		f 4 -847 855 7799 -7799
		mu 0 4 2358 501 1532 4101
		f 4 2718 2276 7800 -7800
		mu 0 4 1532 783 4102 4101
		f 4 7797 -7793 7801 -7801
		mu 0 4 4102 1255 4100 4101
		f 4 5108 -7788 7803 -7803
		mu 0 4 2919 502 4098 4103
		f 4 -2270 2277 7804 -7804
		mu 0 4 4098 1254 2375 4103
		f 4 4063 -7798 7805 -7805
		mu 0 4 2375 1255 4102 4103
		f 4 -1293 7806 7808 -7808
		mu 0 4 2904 776 4105 4104
		f 4 5086 7811 -7813 -7810
		mu 0 4 2278 2905 4107 4106
		f 4 -2171 7814 7816 -7816
		mu 0 4 3972 1206 4109 4108
		f 4 7505 7819 -7821 -7818
		mu 0 4 3974 3973 4111 4110
		f 4 2168 7822 -7824 -7807
		mu 0 4 776 2279 4112 4105
		f 4 -3915 7809 7826 -7825
		mu 0 4 2276 2278 4106 4113
		f 4 3912 7827 -7829 -7823
		mu 0 4 2279 1205 4114 4112
		f 4 3915 7824 -7832 -7830
		mu 0 4 2280 2276 4113 4115
		f 4 -5082 7807 7835 -7834
		mu 0 4 774 2904 4104 4116
		f 4 -5089 7832 7836 -7812
		mu 0 4 2905 2906 4117 4107
		f 4 -1291 7837 7839 -7839
		mu 0 4 2909 778 4119 4118
		f 4 1294 7840 -7842 -7838
		mu 0 4 778 1530 4120 4119
		f 4 -5093 7838 7844 -7843
		mu 0 4 2907 2909 4118 4121
		f 4 2713 7833 -7846 -7841
		mu 0 4 1530 774 4116 4120
		f 4 5093 7842 -7847 -7833
		mu 0 4 2906 2907 4121 4117
		f 4 -7500 7815 7847 -7828
		mu 0 4 1205 3972 4108 4114
		f 4 -7508 7829 7848 -7820
		mu 0 4 3973 2280 4115 4111
		f 4 -7509 7852 7853 -7851
		mu 0 4 1207 3976 4123 4122
		f 4 7516 7854 -7856 -7853
		mu 0 4 3976 3977 4124 4123
		f 4 -7516 7849 7856 -7855
		mu 0 4 3977 3978 4125 4124
		f 4 2175 7857 -7859 -7815
		mu 0 4 1206 2285 4126 4109
		f 4 -7519 7817 7861 -7860
		mu 0 4 3979 3974 4110 4127
		f 4 3919 7850 -7863 -7858
		mu 0 4 2285 1207 4122 4126
		f 4 7519 7859 -7864 -7850
		mu 0 4 3978 3979 4127 4125;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "transform2" -p "pCylinder4";
	rename -uid "5BA17FB0-B94C-CE8A-321E-E98E0A05DEE2";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape4" -p "transform2";
	rename -uid "518AEBE9-CC48-97F3-370C-1D9209D13C1E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.64623898267745972 0.16660803556442261 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
createNode transform -n "pSphere20";
	rename -uid "7ACA77B9-DC41-9991-7830-CC81D5653691";
	setAttr ".t" -type "double3" 0.1086148160232039 2.0594821331844044 0.36701110816289911 ;
	setAttr ".s" -type "double3" 0.01965487033740658 0.01965487033740658 0.01965487033740658 ;
createNode mesh -n "polySurfaceShape16" -p "pSphere20";
	rename -uid "9D98F6BE-1B47-E394-F291-8CBAF1E6BCB9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0.33333334 0.33333334
		 0.33333334 0.66666669 0.33333334 1 0.33333334 0 0.66666669 0.33333334 0.66666669
		 0.66666669 0.66666669 1 0.66666669 0.16666667 0 0.5 0 0.83333337 0 0.16666667 1 0.5
		 1 0.83333337 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.43301266 -0.49999997 -0.75000012 -0.43301278 -0.49999997 0.75
		 0.86602545 -0.49999997 0 -0.43301266 0.49999997 -0.75000012 -0.43301278 0.49999997 0.75
		 0.86602545 0.49999997 0 0 -1 0 0 1 0;
	setAttr -s 15 ".ed[0:14]"  0 1 0 1 2 0 2 0 0 3 4 0 4 5 0 5 3 0 0 3 0
		 1 4 0 2 5 0 6 0 0 6 1 0 6 2 0 3 7 0 4 7 0 5 7 0;
	setAttr -s 9 -ch 30 ".fc[0:8]" -type "polyFaces" 
		f 4 0 7 -4 -7
		mu 0 4 0 1 5 4
		f 4 1 8 -5 -8
		mu 0 4 1 2 6 5
		f 4 2 6 -6 -9
		mu 0 4 2 3 7 6
		f 3 -1 -10 10
		mu 0 3 1 0 8
		f 3 -2 -11 11
		mu 0 3 2 1 9
		f 3 -3 -12 9
		mu 0 3 3 2 10
		f 3 3 13 -13
		mu 0 3 4 5 11
		f 3 4 14 -14
		mu 0 3 5 6 12
		f 3 5 12 -15
		mu 0 3 6 7 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "transform1" -p "pSphere20";
	rename -uid "C046BC63-9E49-6BD9-755C-668BB2354B06";
	setAttr ".v" no;
createNode mesh -n "pSphereShape20" -p "transform1";
	rename -uid "D5F283FD-B446-ED23-CF21-BA936EAB5F30";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere21";
	rename -uid "BF088DE1-2541-F6A2-E3C0-0EB6B5EA0284";
createNode mesh -n "pSphere21Shape" -p "pSphere21";
	rename -uid "B67620F2-8141-B537-DBB2-AE87C069C858";
	setAttr -k off ".v";
	setAttr -s 8 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.77272731065750122 0.9285714328289032 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus1";
	rename -uid "80179231-9B4F-5275-1440-139D5EB79869";
	setAttr ".t" -type "double3" 0 1.8845069377379275 0 ;
	setAttr ".r" -type "double3" 0 28.076610128495282 0 ;
	setAttr ".s" -type "double3" 0.14216324543824099 -0.13530719138986866 0.14216324543824099 ;
createNode mesh -n "pTorusShape1" -p "pTorus1";
	rename -uid "D754053A-4C4A-8695-3A76-089A5E0ABC0B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.65625 0.53125 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[73:76]" -type "float3"  -0.25203219 1.436911 -1.2548271 
		-0.25203219 1.436911 -1.2548271 -0.25203219 1.436911 -1.2548271 -0.25203219 1.436911 
		-1.2548271;
	setAttr ".dr" 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "55E02663-F749-6EE3-E18D-A4A00743CF6D";
	setAttr -s 7 ".lnk";
	setAttr -s 7 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "21E8C9A6-0545-53C6-D5E4-6396DC65EF74";
createNode displayLayer -n "defaultLayer";
	rename -uid "0DC9656E-F14F-FFA8-2824-789F0770E232";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "748EAB56-F247-3417-CC22-8993E13F0884";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "B536C2B7-8A46-FAA3-9EDE-B2A8F56EAA0C";
	setAttr ".g" yes;
createNode polySphere -n "polySphere1";
	rename -uid "9AE6F170-C84A-E959-3AC5-9E8EF0F952B5";
	setAttr ".sa" 11;
	setAttr ".sh" 7;
createNode polySphere -n "polySphere2";
	rename -uid "1F391564-FC44-8BB6-5E56-4BAE5CD39965";
	setAttr ".sa" 3;
	setAttr ".sh" 3;
createNode polyPyramid -n "polyPyramid1";
	rename -uid "BDC86C4D-4840-729A-C020-1C86FF019EFA";
	setAttr ".w" 0.69958620561209728;
	setAttr ".cuv" 3;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "E574EC15-364E-2BCA-9CBD-3BAFA2B46EE2";
	setAttr ".r" 0.4;
	setAttr ".h" 0.03;
	setAttr ".sa" 8;
	setAttr ".sc" 8;
	setAttr ".cuv" 3;
createNode polyCylinder -n "polyCylinder2";
	rename -uid "C9EEDCEF-F348-FEE4-BE89-2CA1D3DB0341";
	setAttr ".r" 0.3;
	setAttr ".h" 0.69958620561209728;
	setAttr ".sa" 10;
	setAttr ".sh" 6;
	setAttr ".sc" 7;
	setAttr ".cuv" 3;
createNode polyCylinder -n "polyCylinder3";
	rename -uid "1B7FF249-994C-5D14-1A5C-A9A735EF4AFB";
	setAttr -av ".sa" 8;
	setAttr ".cuv" 3;
createNode animCurveTL -n "pCylinder3_translateX";
	rename -uid "7DC866A0-0344-6E65-2868-4AA77E85DA5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "pCylinder3_translateY";
	rename -uid "99A4A70A-BD4D-5C8A-6EEA-898280839A2C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 3.9089124750880702;
createNode animCurveTL -n "pCylinder3_translateZ";
	rename -uid "826D59AE-944B-B295-E9E6-8D8D72FA6ACE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder3_axisX";
	rename -uid "FF1CF00C-A94C-9F27-1E0B-6BA3723DA78E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder3_axisY";
	rename -uid "1537DFCD-AE43-2F05-E6DB-4DA000BFFA22";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "polyCylinder3_axisZ";
	rename -uid "242EB267-C647-B3AF-B071-8E8609FE8344";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTL -n "polyCylinder3_radius";
	rename -uid "66CD2A8C-1243-9FA9-39F7-F9AFC5EDCC42";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTL -n "polyCylinder3_height";
	rename -uid "A7A22354-4349-9028-42E2-CAA3209A29F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 2;
createNode animCurveTU -n "polyCylinder3_subdivisionsAxis";
	rename -uid "47173988-FD43-1F8E-35C3-ECA0C4250E4C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 20;
createNode animCurveTU -n "polyCylinder3_subdivisionsHeight";
	rename -uid "E3010BD7-064C-7613-B589-C89F3CC09107";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "polyCylinder3_subdivisionsCaps";
	rename -uid "FB851370-0245-ACD7-8AFA-599F2E59EA76";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder3_visibility";
	rename -uid "6F3CD5E3-9B44-6599-BCE0-C5A6B4626280";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
	setAttr ".kot[0]"  5;
createNode animCurveTA -n "pCylinder3_rotateX";
	rename -uid "7F383CCC-FF4B-6503-FACE-348B26FFA7D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder3_rotateY";
	rename -uid "5D8B522E-D248-14DB-D87D-16B327BA2E5E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 0;
createNode animCurveTA -n "pCylinder3_rotateZ";
	rename -uid "1586A498-2543-8993-A6E8-E0BEE1537ABD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 90;
createNode animCurveTU -n "pCylinder3_scaleX";
	rename -uid "9A5AFF2A-6C40-4E12-E7AF-1688D5C36E36";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder3_scaleY";
	rename -uid "8B8730AD-D141-30B8-1CB2-1A80AF74E8EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode animCurveTU -n "pCylinder3_scaleZ";
	rename -uid "FA879060-BD45-81AC-1B13-F198F681C45F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  1 1;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "1108EE2B-0B47-2A7E-4314-629D0B98F27A";
	setAttr ".ics" -type "componentList" 1 "f[8:15]";
	setAttr ".ix" -type "matrix" 3.8477436797353431e-18 0.017328697002273272 0 0 -0.063514905407715816 1.4103142078106993e-17 0 0
		 0 0 0.017328697002273272 0 0 3.9089124750880702 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.063514903 3.9089124 5.1643539e-10 ;
	setAttr ".rs" 86705299;
	setAttr ".lt" -type "double3" -3.3665930632589951e-17 -0.1866935464010924 0.1530696273479869 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.063514905407715816 3.8915837801515387 -0.017328694936531615 ;
	setAttr ".cbx" -type "double3" 0.063514905407715816 3.9262411720903434 0.017328695969402442 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "46659731-EC45-3A50-6C74-419CDCB7997C";
	setAttr ".ics" -type "componentList" 1 "f[8:15]";
	setAttr ".ix" -type "matrix" 3.8477436797353431e-18 0.017328697002273272 0 0 -0.063514905407715816 1.4103142078106993e-17 0 0
		 0 0 0.017328697002273272 0 0.76518680883826495 1.2698037643652178 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.98177135 1.4564971 0 ;
	setAttr ".rs" 1803622074;
	setAttr ".lt" -type "double3" -2.237462941160652e-16 -2.7730400788441661e-17 0.33348450312639411 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.9817712637049556 1.439168403282425 -0.017328694936531615 ;
	setAttr ".cbx" -type "double3" 0.98177146056569109 1.4738257972869715 0.017328694936531615 ;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "08CEA08E-9A43-22E0-A9FB-5ABFC14E02D5";
	setAttr ".ics" -type "componentList" 1 "f[24:31]";
createNode polyTweak -n "polyTweak1";
	rename -uid "A3ABB6A1-6046-86EF-32B3-6AA7C344D4C7";
	setAttr ".uopa" yes;
	setAttr -s 13 ".tk";
	setAttr ".tk[17]" -type "float3" 2.2351742e-08 0 0 ;
	setAttr ".tk[20]" -type "float3" -2.2351742e-08 0 0 ;
	setAttr ".tk[22]" -type "float3" -2.2351742e-08 0 0 ;
	setAttr ".tk[24]" -type "float3" 2.2351742e-08 0 0 ;
	setAttr ".tk[25]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[26]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[27]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[28]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[29]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[30]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[31]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[32]" -type "float3" 18.624914 2.2024465 0 ;
	setAttr ".tk[33]" -type "float3" 18.624914 2.2024465 0 ;
createNode polySubdFace -n "polySubdFace2";
	rename -uid "277D74FC-CF4F-EB05-B274-81B498C2563F";
	setAttr ".ics" -type "componentList" 2 "f[24:31]" "f[40:63]";
createNode polyExtrudeFace -n "polyExtrudeFace3";
	rename -uid "A85C932A-484E-51CC-B415-8CA067F36AE0";
	setAttr ".ics" -type "componentList" 1 "f[138]";
	setAttr ".ix" -type "matrix" 3.8477436797353431e-18 0.017328697002273272 0 0 -0.063514905407715816 1.4103142078106993e-17 0 0
		 0 0 0.017328697002273272 0 0.76518680883826495 1.2698037643652178 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.92437017 1.391082 0.015425399 ;
	setAttr ".rs" 1650137008;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.90523645868920122 1.3662137454192205 0.014790966426694979 ;
	setAttr ".cbx" -type "double3" 0.94350389905491205 1.415950392493353 0.016059831714484125 ;
createNode polyExtrudeFace -n "polyExtrudeFace4";
	rename -uid "9C7245E5-8849-60B3-DCF3-B09A397707DB";
	setAttr ".ics" -type "componentList" 6 "f[59]" "f[62]" "f[147]" "f[149:150]" "f[156]" "f[159]";
	setAttr ".ix" -type "matrix" 3.8477436797353431e-18 0.017328697002273272 0 0 -0.063514905407715816 1.4103142078106993e-17 0 0
		 0 0 0.017328697002273272 0 0.76518680883826495 1.2698037643652178 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.92437017 1.401278 0.0015316545 ;
	setAttr ".rs" 1772312771;
	setAttr ".lt" -type "double3" 5.9327542878406803e-16 1.1796119636642288e-16 0.15779608407058249 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.90523644354606769 1.3754036847020079 -0.0091899299869500021 ;
	setAttr ".cbx" -type "double3" 0.94350386876864512 1.4271523310996352 0.012253238949729173 ;
createNode polySubdFace -n "polySubdFace3";
	rename -uid "767E47DD-AF4A-AC7B-82C7-2888A6963B17";
	setAttr ".ics" -type "componentList" 1 "f[32:39]";
createNode polyTweak -n "polyTweak2";
	rename -uid "5CF56BDA-C54A-C329-4786-34B61C8813EB";
	setAttr ".uopa" yes;
	setAttr -s 16 ".tk[182:197]" -type "float3"  6.45278645 -0.96291786 -2.45492887
		 6.39149523 -0.96290785 -2.30704355 4.1974082 -0.45840889 -2.45492887 4.13612413 -0.45839971
		 -2.30704355 6.20754862 -0.96290785 0.073873989 6.26887083 -0.96290785 2.15873694
		 3.95219445 -0.45839876 0.073873989 4.013482571 -0.45839971 2.15873694 4.074832916
		 -0.45839626 -2.1590848 6.33018732 -0.96290404 -2.1590848 6.26887083 -0.96290785 -2.010990858
		 4.013482571 -0.45839781 -2.010990858 4.074832916 -0.45839709 2.30683184 6.33020878
		 -0.96290594 2.30683184 6.39151955 -0.96290308 2.45492887 4.13616276 -0.45839411 2.45492887;
createNode polySubdFace -n "polySubdFace4";
	rename -uid "7827053A-F542-2A02-B6C8-3FB418F28D51";
	setAttr ".ics" -type "componentList" 2 "f[32:39]" "f[180:227]";
createNode polySubdFace -n "polySubdFace5";
	rename -uid "056D6BB0-A84D-08B5-3976-4C9FA2A3E4C0";
	setAttr ".ics" -type "componentList" 2 "f[32:39]" "f[180:395]";
createNode polySubdFace -n "polySubdFace6";
	rename -uid "F24DD9CC-9A4F-8AF8-0121-D3AC42D6E3D0";
	setAttr ".ics" -type "componentList" 2 "f[32:39]" "f[180:1067]";
createNode polyExtrudeFace -n "polyExtrudeFace5";
	rename -uid "6DE8BE13-8B4F-4E6D-E430-3DACC32D72EA";
	setAttr ".ics" -type "componentList" 8 "f[559]" "f[1010]" "f[1681:1682]" "f[2231]" "f[2233:2234]" "f[3584]" "f[3590]" "f[3592:3593]";
	setAttr ".ix" -type "matrix" 3.8477436797353431e-18 0.017328697002273272 0 0 -0.063514905407715816 1.4103142078106993e-17 0 0
		 0 0 0.017328697002273272 0 0.76518680883826495 1.2698037643652178 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 1.1088188 1.6845176 0.0011487093 ;
	setAttr ".rs" 89050283;
	setAttr ".lt" -type "double3" 1.5005358067199381e-16 1.2836953722228372e-16 0.083577195518615879 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 1.1027688072910298 1.6733219059165245 -0.0030634628605327283 ;
	setAttr ".cbx" -type "double3" 1.1148687161095987 1.6957133556272714 0.0053608815774965233 ;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "C44CC7C4-C94B-C4F9-077C-02AFF6565155";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 464\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 464\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 463\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 463\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 464\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 464\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 463\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 463\n            -height 347\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n"
		+ "                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n"
		+ "            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n"
		+ "                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n"
		+ "                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n"
		+ "                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n"
		+ "            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n"
		+ "            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n"
		+ "            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n"
		+ "\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 464\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 464\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 463\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 463\\n    -height 347\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 463\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 463\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 464\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 464\\n    -height 346\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "76875898-274A-8EEF-3112-C69A472BCDAC";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode lambert -n "snowman_base";
	rename -uid "B156A4FD-E245-07AE-DD29-DCBA729012B9";
	setAttr ".c" -type "float3" 0.91136032 0.91136032 0.91136032 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "C053BC81-F04B-1086-CD5D-B3915A362D84";
	setAttr ".ihi" 0;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 7 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "3B909A84-7640-2815-82C2-FCA5C4A95CE9";
createNode lambert -n "sbow_man_buttons";
	rename -uid "5B959D40-E444-5256-1D36-ABB5214DF4EF";
	setAttr ".c" -type "float3" 0 0 0 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "F353F671-7A48-97D5-EEFC-58A6C33D33A3";
	setAttr ".ihi" 0;
	setAttr -s 33 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 33 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "C5D3A5CF-334A-6570-F612-F98ACD27FA79";
createNode lambert -n "sn_man_nose";
	rename -uid "65A94851-A349-D8D2-8F8C-F8B1A001BAB2";
	setAttr ".c" -type "float3" 0.6631 0.1988 0.090999998 ;
createNode shadingEngine -n "lambert4SG";
	rename -uid "1041E544-164B-38C6-7D99-C29CD0AAE2D8";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo3";
	rename -uid "C47193CC-B943-7B24-DE97-24847CC3E804";
createNode lambert -n "snow_man_arm";
	rename -uid "2272BCB2-0746-7D10-0566-20BEDECBBC1D";
	setAttr ".c" -type "float3" 0.167 0.11166041 0.093353003 ;
createNode shadingEngine -n "lambert5SG";
	rename -uid "8E4F0C70-CF4C-8A2E-C6D5-D297BDDEC857";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo4";
	rename -uid "0B5F6540-C344-3AD1-F7C7-A1A247F4197D";
createNode polyConnectComponents -n "polyConnectComponents1";
	rename -uid "0E6A7F00-774C-CEE0-F34E-798AB40E6EA8";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents2";
	rename -uid "13CCE942-744F-ECC9-F86D-3CABCC2E9C81";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents3";
	rename -uid "52765C69-CC4A-BCB1-65A3-909374DA1709";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents4";
	rename -uid "9AF11254-7A4F-0D93-40BE-12BBCEAD80B4";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents5";
	rename -uid "B72738B8-4E49-E940-D1FB-27B2977541D9";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents6";
	rename -uid "5A025D23-E74F-EE64-B61A-3C8AA9FDF0DD";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents7";
	rename -uid "6BF0C326-EB49-673C-1BDA-7FA50548B467";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents8";
	rename -uid "B459B868-2845-D4A6-4B77-66A5562FC757";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents9";
	rename -uid "5AC4CF12-1A42-B61F-48C7-8C8DA3FD64BF";
	setAttr ".uopa" yes;
createNode polyTweak -n "polyTweak3";
	rename -uid "F366F430-FD4B-9377-92C8-E686E3071F3A";
	setAttr ".uopa" yes;
	setAttr -s 4 ".tk[0:3]" -type "float3"  -4.8621622e-08 1.1920929e-07
		 0.37077767 0.37077767 1.1920929e-07 3.2414412e-08 1.6207206e-08 1.1920929e-07 -0.37077767
		 -0.37077767 1.1920929e-07 0;
createNode polyConnectComponents -n "polyConnectComponents10";
	rename -uid "EE873217-4940-E0B9-27EA-4D9B3480AC87";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents11";
	rename -uid "D3F3CD1A-EA42-F938-76FF-4DA2413F266C";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents12";
	rename -uid "B123401C-EB41-FEB9-E627-838BDA083E53";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents13";
	rename -uid "4166E537-104A-0E17-A0D7-1ABE51FD977E";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents14";
	rename -uid "49458C29-7A4C-4ABF-AA75-E1AC656C6576";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents15";
	rename -uid "6CE7540A-BC44-F998-674F-2BB304341A3E";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents16";
	rename -uid "9D475881-7942-EDCE-1B4F-0CB5A30245C0";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents17";
	rename -uid "9943996C-4249-8063-7F01-87BE1F6B979F";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents18";
	rename -uid "192D060A-334C-F2DD-8431-CB940CEE6D38";
	setAttr ".uopa" yes;
createNode polyTweak -n "polyTweak4";
	rename -uid "122AD8E9-744A-7FE7-0354-F6BC6384A572";
	setAttr ".uopa" yes;
	setAttr -s 47 ".tk";
	setAttr ".tk[34]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[35]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[37]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[40]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[42]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[43]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[44]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[45]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[48]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[49]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[50]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[51]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[52]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[53]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[54]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[55]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[57]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[58]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[59]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[60]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[61]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[62]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[63]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[65]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[66]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[67]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[68]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[69]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[70]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[71]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[72]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[73]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[74]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[75]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[76]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[77]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[79]" -type "float3" 0 -0.041524369 0 ;
	setAttr ".tk[80]" -type "float3" 0 -0.044545017 0 ;
	setAttr ".tk[82]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[83]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[84]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[85]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[90]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[91]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.035910141 0 ;
	setAttr ".tk[98]" -type "float3" 0 0.033349205 0 ;
	setAttr ".tk[99]" -type "float3" 0 0.033349205 0 ;
createNode polyConnectComponents -n "polyConnectComponents19";
	rename -uid "4371DB48-494C-821F-5351-55831E90D998";
	setAttr ".uopa" yes;
createNode polyTweak -n "polyTweak5";
	rename -uid "4DB8CB52-B744-EA3B-6935-53A48CF729F4";
	setAttr ".uopa" yes;
	setAttr -s 48 ".tk";
	setAttr ".tk[32]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[42]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[43]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[52]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[53]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[63]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[70]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[74]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[80]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[90]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[93]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[95]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[99]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[100]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[103]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[104]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[106]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[107]" -type "float3" 0 0 -0.024714712 ;
	setAttr ".tk[108]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[109]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[113]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[114]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[115]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[118]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[119]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[120]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[121]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[123]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[124]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[125]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[126]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[128]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[130]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[131]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[133]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[134]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[135]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[136]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[138]" -type "float3" -0.033368036 0 0 ;
	setAttr ".tk[141]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[143]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[144]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[145]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[146]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[151]" -type "float3" 0 -0.062980101 0 ;
	setAttr ".tk[154]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[155]" -type "float3" 0 -0.069423482 0 ;
	setAttr ".tk[161]" -type "float3" 0 -0.062980101 0 ;
createNode polyConnectComponents -n "polyConnectComponents20";
	rename -uid "DD8331A3-2B49-5525-3DAA-F0873F232E0C";
	setAttr ".uopa" yes;
createNode polyTweak -n "polyTweak6";
	rename -uid "E98ABBE7-7E43-73D4-FC8D-128D6DAA4CC2";
	setAttr ".uopa" yes;
	setAttr -s 24 ".tk[4062:4085]" -type "float3"  4.1242404 -0.074261501 0.051871866
		 4.13850451 -0.074262924 0.86977482 3.21658897 0.07428088 0.051871866 3.23085499 0.07427913
		 0.86977488 4.18128395 -0.07428088 -0.86904645 4.16701841 -0.07428088 -0.83458573
		 3.27363229 0.074260153 -0.86904669 3.25936103 0.074260153 -0.83458579 4.13850451
		 -0.074261501 -0.76603019 3.23085499 0.07428088 -0.76603019 4.15278864 -0.074262924
		 -0.80048966 3.24513316 0.07427913 -0.80048978 3.24512601 0.07427913 0.90423435 4.15278244
		 -0.074262924 0.90423411 4.18127298 -0.07428088 0.97279209 3.27362132 0.074260153
		 0.97279263 4.16702986 -0.074270301 0.9385125 3.2593739 0.074270301 0.93851274 3.30219913
		 0.07427913 -0.93833166 4.20985603 -0.074262924 -0.9383319 4.22412205 -0.074262924
		 -0.97279251 3.31646562 0.07427913 -0.97279263 4.19559193 -0.074262924 -0.90387118
		 3.28793383 0.07427913 -0.90387142;
createNode polyConnectComponents -n "polyConnectComponents21";
	rename -uid "51C92554-5647-1EF4-A03F-48969376D4AB";
	setAttr ".uopa" yes;
createNode polyConnectComponents -n "polyConnectComponents22";
	rename -uid "45898DD8-6E4D-BA4B-B2FF-229261FF86C3";
	setAttr ".uopa" yes;
createNode polyUnite -n "polyUnite1";
	rename -uid "2DE84525-114A-BE94-6DC8-F7ADCB95604A";
	setAttr -s 22 ".ip";
	setAttr -s 22 ".im";
createNode groupId -n "groupId1";
	rename -uid "2945B856-D24C-A784-0379-87988924B948";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "5F80C56F-4C47-1E42-4218-7898097E823E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:76]";
createNode groupId -n "groupId2";
	rename -uid "EFA2EA7C-B24B-482C-B1D2-369F1576CA9D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "3B823E7E-BD45-4961-DE89-A495D68C3E23";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "4C4DE363-6845-AA5E-6273-2F9985F2EF28";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:76]";
createNode groupId -n "groupId4";
	rename -uid "2F30C9D4-4744-356F-49C1-F9A691446C6D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "86CA352C-FE4C-C7BA-644D-348A0C07679A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "8BD1AE79-F144-6E11-7692-7FB19F3D1E27";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:76]";
createNode groupId -n "groupId6";
	rename -uid "9A5BF564-D240-2F2E-436C-859A034384A3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "040897B7-EA46-AC60-1366-708E0FDF7363";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "B24E6440-8648-ADE8-032A-28A8EFAC4F8A";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId8";
	rename -uid "285F5AA8-774E-BEF6-C403-18B10302E77B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "104E7CAB-3349-D29B-4EB2-99A8F99DE9E5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "C7482EA6-354B-B6BE-33F6-ABA607875F08";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId10";
	rename -uid "FA04C1B3-E146-4E01-92BD-4CBA97E4A1A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "5003DF3D-FE47-CD4C-BA28-B2A6F8FAACC7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "549BA1C0-7C40-BFA2-F591-AA93787FBEC6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId12";
	rename -uid "AE45A50A-FB4A-AEA8-9A1B-11AE6C0E0A5F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "619993C3-C642-7F0C-6E1B-949D5396716A";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "F694A546-EB41-CFDE-24CB-6C99C8D2D57D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId14";
	rename -uid "D40A4BA6-DF4E-0D26-0A25-CA81A93F7139";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "4BA1F283-2C42-7075-C74C-B3B7712768EF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "2ED9B656-D24C-C5EF-23F0-59BAB83A1A27";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId16";
	rename -uid "BF92D3A0-E148-4BA0-9800-E4A7B2F79E55";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "9D82DF65-634C-0386-33E4-0A8DB7C339F2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "55C649B5-6D48-60A2-ADA8-F7A913A53F09";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:4]";
createNode groupId -n "groupId18";
	rename -uid "CD6ED2A0-154A-327B-039D-EBB3DF52B101";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "77E42F93-1740-9100-629B-FD9DDA7BDD5B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "556CFBC3-384D-8C19-71E4-B7B95C1F39A7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId20";
	rename -uid "87BFC407-764B-B84E-6C53-B6BDDF06B392";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "23FD6D9C-4C41-5322-1F41-86A24075418D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "59340DE3-8348-DCE3-6C2E-9AA69AADA2E1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId22";
	rename -uid "AC3C53F8-D848-2A28-DDE6-938B4BE73AEF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "047BD473-E246-519B-190F-7B8D4972CA66";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "0F191F66-084D-0147-F485-6EB9C9B86976";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId24";
	rename -uid "E5026E79-C649-45F7-33A8-BEABC3D5AB2C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "CC93489E-C249-E696-7842-999FB2C8E2DB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts13";
	rename -uid "60D7AA48-4D4E-D8F7-F910-1D8C2E40B040";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId26";
	rename -uid "221581B5-5246-69F1-C859-968B667CD311";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "090F4ECF-CF45-1940-8CFE-75A327FD8169";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts14";
	rename -uid "322BD469-D144-146C-216E-85AEFB6F5906";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId28";
	rename -uid "9AAABDAB-444C-B09D-2001-5F83C6179EA7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "C9D6EF14-CA43-EA74-2664-64882C746609";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts15";
	rename -uid "92A67B7D-1943-C173-5E3F-4FB0FEE80329";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId30";
	rename -uid "D31AA8BD-9E42-B79F-145D-4DBA685D246C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "CB2B1C81-D646-774A-9399-609DB283F00E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts16";
	rename -uid "8EDB9739-1945-FD17-F8C6-6398F980D9AF";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId32";
	rename -uid "0BDEDDF8-6340-089F-7CB2-DBAA55BB2C63";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "FD46C6CA-DC40-8BD8-8A2A-FC82F53C8363";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts17";
	rename -uid "3173776F-184E-BA4F-F6E8-1FAB1D8D1B65";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId34";
	rename -uid "1C5EB569-7842-B267-632C-559FECAECDB4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "D966B777-7647-B0BF-2E51-5593D9F2E6C7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts18";
	rename -uid "973902BA-4646-7FDD-265C-16BA49189C16";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:135]";
createNode groupId -n "groupId36";
	rename -uid "8CEDC8FB-564C-421D-C712-EC9082A064F3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId37";
	rename -uid "1E320536-D94A-587B-0C26-3190B59A5DB0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts19";
	rename -uid "86B57409-C34A-53E0-44C2-1EAFBCA92DEB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:199]";
createNode groupId -n "groupId38";
	rename -uid "17F27B38-6346-8533-D765-2ABAB985CD41";
	setAttr ".ihi" 0;
createNode groupId -n "groupId39";
	rename -uid "8BDE880E-5542-A919-A652-88A9B7683819";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts20";
	rename -uid "064FF7DD-3F40-B6CC-0216-679526518A57";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:3790]";
createNode groupId -n "groupId40";
	rename -uid "825A2288-1642-CD68-D963-C8A56922A911";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "BBDC3CF1-774C-38BA-1BDB-9BB526E62C4B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts21";
	rename -uid "225C58F7-9E41-8242-109B-109AF3B38D19";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:3779]";
createNode groupId -n "groupId42";
	rename -uid "05AEB43D-464C-779F-00B8-19B139C0B4BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "B496DE09-1A48-05DA-D835-EE9819450D55";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts22";
	rename -uid "3EC75C44-EB46-43D7-C467-0B83E634EC49";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:8]";
createNode groupId -n "groupId44";
	rename -uid "ACF57271-8246-11C6-85A2-34BBE68DCDB5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "88CBBA92-0C4E-5A12-7D26-4D8691D657F4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts23";
	rename -uid "DD070629-B04A-80A9-D27C-43891719DD31";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:230]";
createNode groupId -n "groupId46";
	rename -uid "D0F4AAF0-D348-3997-39B2-C39EB12D8B3F";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts24";
	rename -uid "E6194DD1-874B-BA88-FC69-31A945ED5723";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[231:275]" "f[281:688]" "f[8260:8268]";
createNode groupId -n "groupId47";
	rename -uid "72E57882-7748-52FB-42A4-1485EC119460";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts25";
	rename -uid "4D9F8760-1B4D-96DB-70E3-CD9B7A31479F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[276:280]";
createNode groupId -n "groupId48";
	rename -uid "2C7F15EB-0249-E61E-5501-3EAEE5323DC0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts26";
	rename -uid "E5A056C4-EE4C-0B24-ECFF-01A903B0453D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[689:8259]";
createNode polyTorus -n "polyTorus1";
	rename -uid "0BCF650F-8B44-E3E6-60E8-7BB1F74DFBD0";
	setAttr ".r" 2.0787586168362919;
	setAttr ".sr" 1;
	setAttr ".sa" 8;
	setAttr ".sh" 8;
createNode polySubdFace -n "polySubdFace7";
	rename -uid "621827F8-1941-BD2F-E135-B0A8073FBA53";
	setAttr ".ics" -type "componentList" 1 "f[29]";
createNode polyExtrudeFace -n "polyExtrudeFace6";
	rename -uid "6397C28E-5E4E-AEA0-C27F-F69A33909862";
	setAttr ".ics" -type "componentList" 1 "f[65]";
	setAttr ".ix" -type "matrix" 0.12543334304565948 0 -0.066909377563413766 0 -0 -0.13530719138986866 -0 0
		 0.066909377563413766 0 0.12543334304565948 0 0 1.8845069377379275 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.25515532 1.8605878 0.31393844 ;
	setAttr ".rs" 1021787211;
	setAttr ".lt" -type "double3" -3.6082248300317588e-16 -7.4940054162198066e-16 -0.46931837607865945 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.19619915557338449 1.836668626301573 0.24503322545840589 ;
	setAttr ".cbx" -type "double3" 0.31236502521840914 1.8845069377379275 0.38617896834636195 ;
createNode polyExtrudeFace -n "polyExtrudeFace7";
	rename -uid "8093C9AF-D041-A25F-0CD8-D0A0CF72F193";
	setAttr ".ics" -type "componentList" 1 "f[65]";
	setAttr ".ix" -type "matrix" 0.12543334304565948 0 -0.066909377563413766 0 -0 -0.13530719138986866 -0 0
		 0.066909377563413766 0 0.12543334304565948 0 0 1.8845069377379275 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0.40944007 1.6201943 0.47672004 ;
	setAttr ".rs" 294637507;
	setAttr ".lt" -type "double3" -9.7144514654701197e-17 -6.4531713306337224e-16 -0.19077358450222415 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" 0.35048391123015549 1.5962750870275169 0.40781477330536514 ;
	setAttr ".cbx" -type "double3" 0.46664979482838087 1.6441134549184309 0.54896059993325808 ;
createNode polyTweak -n "polyTweak7";
	rename -uid "62EE092B-8B45-E4E6-06B3-A79E4F070F83";
	setAttr ".uopa" yes;
	setAttr -s 5 ".tk";
	setAttr ".tk[69]" -type "float3" -0.75350249 0.482714 -1.3087317 ;
	setAttr ".tk[70]" -type "float3" -0.75350249 0.482714 -1.3087317 ;
	setAttr ".tk[71]" -type "float3" -0.75350249 0.482714 -1.3087317 ;
	setAttr ".tk[72]" -type "float3" -0.75350249 0.482714 -1.3087317 ;
createNode lambert -n "scarf";
	rename -uid "AF07736A-9D47-D992-A6A6-A79F64044357";
	setAttr ".c" -type "float3" 0.54000002 0.0317 0.0317 ;
createNode shadingEngine -n "lambert6SG";
	rename -uid "6C82EEDB-2644-A7C1-AF9B-11B9FC8D4694";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
	rename -uid "71CFB7C3-E94A-569D-C953-51954D8F752A";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "650F29D1-5648-4802-F838-808CAD8B0696";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -740.47616105231157 -184.52380219149239 ;
	setAttr ".tgi[0].vh" -type "double2" 705.95235290035475 191.66665905051792 ;
	setAttr -s 10 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 262.85714721679688;
	setAttr ".tgi[0].ni[0].y" -71.428573608398438;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 582.85711669921875;
	setAttr ".tgi[0].ni[1].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 262.85714721679688;
	setAttr ".tgi[0].ni[2].y" -62.857143402099609;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[3].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" 844.28570556640625;
	setAttr ".tgi[0].ni[4].y" -67.142860412597656;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[5].y" -10;
	setAttr ".tgi[0].ni[5].nvs" 1923;
	setAttr ".tgi[0].ni[6].x" 582.85711669921875;
	setAttr ".tgi[0].ni[6].y" -10;
	setAttr ".tgi[0].ni[6].nvs" 1923;
	setAttr ".tgi[0].ni[7].x" 844.28570556640625;
	setAttr ".tgi[0].ni[7].y" -67.142860412597656;
	setAttr ".tgi[0].ni[7].nvs" 1923;
	setAttr ".tgi[0].ni[8].x" 1161.4285888671875;
	setAttr ".tgi[0].ni[8].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[8].nvs" 1923;
	setAttr ".tgi[0].ni[9].x" 1422.857177734375;
	setAttr ".tgi[0].ni[9].y" -58.571430206298828;
	setAttr ".tgi[0].ni[9].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 7 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 9 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId1.id" "pSphereShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pSphereShape1.i";
connectAttr "groupId2.id" "pSphereShape1.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pSphereShape2.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape2.iog.og[0].gco";
connectAttr "groupParts2.og" "pSphereShape2.i";
connectAttr "groupId4.id" "pSphereShape2.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pSphereShape3.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape3.iog.og[0].gco";
connectAttr "groupParts3.og" "pSphereShape3.i";
connectAttr "groupId6.id" "pSphereShape3.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pSphereShape4.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape4.iog.og[0].gco";
connectAttr "groupParts4.og" "pSphereShape4.i";
connectAttr "groupId8.id" "pSphereShape4.ciog.cog[0].cgid";
connectAttr "groupId9.id" "pSphereShape5.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape5.iog.og[0].gco";
connectAttr "groupParts5.og" "pSphereShape5.i";
connectAttr "groupId10.id" "pSphereShape5.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pSphereShape6.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape6.iog.og[0].gco";
connectAttr "groupParts6.og" "pSphereShape6.i";
connectAttr "groupId12.id" "pSphereShape6.ciog.cog[0].cgid";
connectAttr "groupId13.id" "pSphereShape7.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape7.iog.og[0].gco";
connectAttr "groupParts7.og" "pSphereShape7.i";
connectAttr "groupId14.id" "pSphereShape7.ciog.cog[0].cgid";
connectAttr "groupId15.id" "pSphereShape8.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape8.iog.og[0].gco";
connectAttr "groupParts8.og" "pSphereShape8.i";
connectAttr "groupId16.id" "pSphereShape8.ciog.cog[0].cgid";
connectAttr "groupId17.id" "pPyramidShape1.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pPyramidShape1.iog.og[0].gco";
connectAttr "groupParts9.og" "pPyramidShape1.i";
connectAttr "groupId18.id" "pPyramidShape1.ciog.cog[0].cgid";
connectAttr "groupId19.id" "pSphereShape9.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape9.iog.og[0].gco";
connectAttr "groupParts10.og" "pSphereShape9.i";
connectAttr "groupId20.id" "pSphereShape9.ciog.cog[0].cgid";
connectAttr "groupId21.id" "pSphereShape10.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape10.iog.og[0].gco";
connectAttr "groupParts11.og" "pSphereShape10.i";
connectAttr "groupId22.id" "pSphereShape10.ciog.cog[0].cgid";
connectAttr "groupId23.id" "pSphereShape13.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape13.iog.og[0].gco";
connectAttr "groupParts12.og" "pSphereShape13.i";
connectAttr "groupId24.id" "pSphereShape13.ciog.cog[0].cgid";
connectAttr "groupId25.id" "pSphereShape14.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape14.iog.og[0].gco";
connectAttr "groupParts13.og" "pSphereShape14.i";
connectAttr "groupId26.id" "pSphereShape14.ciog.cog[0].cgid";
connectAttr "groupId27.id" "pSphereShape15.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape15.iog.og[0].gco";
connectAttr "groupParts14.og" "pSphereShape15.i";
connectAttr "groupId28.id" "pSphereShape15.ciog.cog[0].cgid";
connectAttr "groupId29.id" "pSphereShape16.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape16.iog.og[0].gco";
connectAttr "groupParts15.og" "pSphereShape16.i";
connectAttr "groupId30.id" "pSphereShape16.ciog.cog[0].cgid";
connectAttr "groupId31.id" "pSphereShape17.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape17.iog.og[0].gco";
connectAttr "groupParts16.og" "pSphereShape17.i";
connectAttr "groupId32.id" "pSphereShape17.ciog.cog[0].cgid";
connectAttr "groupId33.id" "pSphereShape19.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape19.iog.og[0].gco";
connectAttr "groupParts17.og" "pSphereShape19.i";
connectAttr "groupId34.id" "pSphereShape19.ciog.cog[0].cgid";
connectAttr "groupId35.id" "pCylinderShape1.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts18.og" "pCylinderShape1.i";
connectAttr "groupId36.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupId37.id" "pCylinderShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "groupParts19.og" "pCylinderShape2.i";
connectAttr "groupId38.id" "pCylinderShape2.ciog.cog[0].cgid";
connectAttr "pCylinder3_translateX.o" "pCylinder3.tx";
connectAttr "pCylinder3_translateY.o" "pCylinder3.ty";
connectAttr "pCylinder3_translateZ.o" "pCylinder3.tz";
connectAttr "pCylinder3_scaleX.o" "pCylinder3.sx";
connectAttr "pCylinder3_scaleY.o" "pCylinder3.sy";
connectAttr "pCylinder3_scaleZ.o" "pCylinder3.sz";
connectAttr "pCylinder3_visibility.o" "pCylinder3.v";
connectAttr "pCylinder3_rotateX.o" "pCylinder3.rx";
connectAttr "pCylinder3_rotateY.o" "pCylinder3.ry";
connectAttr "pCylinder3_rotateZ.o" "pCylinder3.rz";
connectAttr "groupId39.id" "pCylinderShape3.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCylinderShape3.iog.og[0].gco";
connectAttr "groupParts20.og" "pCylinderShape3.i";
connectAttr "groupId40.id" "pCylinderShape3.ciog.cog[0].cgid";
connectAttr "groupId41.id" "pCylinderShape4.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCylinderShape4.iog.og[0].gco";
connectAttr "groupParts21.og" "pCylinderShape4.i";
connectAttr "groupId42.id" "pCylinderShape4.ciog.cog[0].cgid";
connectAttr "groupId43.id" "pSphereShape20.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape20.iog.og[0].gco";
connectAttr "groupParts22.og" "pSphereShape20.i";
connectAttr "groupId44.id" "pSphereShape20.ciog.cog[0].cgid";
connectAttr "groupParts26.og" "pSphere21Shape.i";
connectAttr "groupId45.id" "pSphere21Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphere21Shape.iog.og[0].gco";
connectAttr "groupId46.id" "pSphere21Shape.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pSphere21Shape.iog.og[1].gco";
connectAttr "groupId47.id" "pSphere21Shape.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pSphere21Shape.iog.og[2].gco";
connectAttr "groupId48.id" "pSphere21Shape.iog.og[3].gid";
connectAttr "lambert5SG.mwc" "pSphere21Shape.iog.og[3].gco";
connectAttr "polyExtrudeFace7.out" "pTorusShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCylinder3_subdivisionsAxis.o" "polyCylinder3.sa";
connectAttr "polyCylinder3_axisX.o" "polyCylinder3.axx";
connectAttr "polyCylinder3_axisY.o" "polyCylinder3.axy";
connectAttr "polyCylinder3_axisZ.o" "polyCylinder3.axz";
connectAttr "polyCylinder3_radius.o" "polyCylinder3.r";
connectAttr "polyCylinder3_height.o" "polyCylinder3.h";
connectAttr "polyCylinder3_subdivisionsHeight.o" "polyCylinder3.sh";
connectAttr "polyCylinder3_subdivisionsCaps.o" "polyCylinder3.sc";
connectAttr "polyCylinder3.out" "polyExtrudeFace1.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace1.mp";
connectAttr "polyExtrudeFace1.out" "polyExtrudeFace2.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace2.mp";
connectAttr "polyTweak1.out" "polySubdFace1.ip";
connectAttr "polyExtrudeFace2.out" "polyTweak1.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polyExtrudeFace3.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace3.mp";
connectAttr "polyExtrudeFace3.out" "polyExtrudeFace4.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace4.mp";
connectAttr "polyTweak2.out" "polySubdFace3.ip";
connectAttr "polyExtrudeFace4.out" "polyTweak2.ip";
connectAttr "polySubdFace3.out" "polySubdFace4.ip";
connectAttr "polySubdFace4.out" "polySubdFace5.ip";
connectAttr "polySubdFace5.out" "polySubdFace6.ip";
connectAttr "polySubdFace6.out" "polyExtrudeFace5.ip";
connectAttr "pCylinderShape3.wm" "polyExtrudeFace5.mp";
connectAttr "snowman_base.oc" "lambert2SG.ss";
connectAttr "pSphereShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape2.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape2.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape3.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape3.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphere21Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "groupId1.msg" "lambert2SG.gn" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "groupId3.msg" "lambert2SG.gn" -na;
connectAttr "groupId4.msg" "lambert2SG.gn" -na;
connectAttr "groupId5.msg" "lambert2SG.gn" -na;
connectAttr "groupId6.msg" "lambert2SG.gn" -na;
connectAttr "groupId45.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "snowman_base.msg" "materialInfo1.m";
connectAttr "sbow_man_buttons.oc" "lambert3SG.ss";
connectAttr "pSphereShape4.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape4.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape5.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape5.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape6.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape6.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape7.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape7.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape8.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape8.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape9.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape9.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape10.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape10.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape13.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape13.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape14.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape14.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape15.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape15.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape16.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape16.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape17.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape17.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape19.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape19.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape1.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape20.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape20.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphere21Shape.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "groupId7.msg" "lambert3SG.gn" -na;
connectAttr "groupId8.msg" "lambert3SG.gn" -na;
connectAttr "groupId9.msg" "lambert3SG.gn" -na;
connectAttr "groupId10.msg" "lambert3SG.gn" -na;
connectAttr "groupId11.msg" "lambert3SG.gn" -na;
connectAttr "groupId12.msg" "lambert3SG.gn" -na;
connectAttr "groupId13.msg" "lambert3SG.gn" -na;
connectAttr "groupId14.msg" "lambert3SG.gn" -na;
connectAttr "groupId15.msg" "lambert3SG.gn" -na;
connectAttr "groupId16.msg" "lambert3SG.gn" -na;
connectAttr "groupId19.msg" "lambert3SG.gn" -na;
connectAttr "groupId20.msg" "lambert3SG.gn" -na;
connectAttr "groupId21.msg" "lambert3SG.gn" -na;
connectAttr "groupId22.msg" "lambert3SG.gn" -na;
connectAttr "groupId23.msg" "lambert3SG.gn" -na;
connectAttr "groupId24.msg" "lambert3SG.gn" -na;
connectAttr "groupId25.msg" "lambert3SG.gn" -na;
connectAttr "groupId26.msg" "lambert3SG.gn" -na;
connectAttr "groupId27.msg" "lambert3SG.gn" -na;
connectAttr "groupId28.msg" "lambert3SG.gn" -na;
connectAttr "groupId29.msg" "lambert3SG.gn" -na;
connectAttr "groupId30.msg" "lambert3SG.gn" -na;
connectAttr "groupId31.msg" "lambert3SG.gn" -na;
connectAttr "groupId32.msg" "lambert3SG.gn" -na;
connectAttr "groupId33.msg" "lambert3SG.gn" -na;
connectAttr "groupId34.msg" "lambert3SG.gn" -na;
connectAttr "groupId35.msg" "lambert3SG.gn" -na;
connectAttr "groupId36.msg" "lambert3SG.gn" -na;
connectAttr "groupId37.msg" "lambert3SG.gn" -na;
connectAttr "groupId38.msg" "lambert3SG.gn" -na;
connectAttr "groupId43.msg" "lambert3SG.gn" -na;
connectAttr "groupId44.msg" "lambert3SG.gn" -na;
connectAttr "groupId46.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "sbow_man_buttons.msg" "materialInfo2.m";
connectAttr "sn_man_nose.oc" "lambert4SG.ss";
connectAttr "pPyramidShape1.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pPyramidShape1.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pSphere21Shape.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "groupId17.msg" "lambert4SG.gn" -na;
connectAttr "groupId18.msg" "lambert4SG.gn" -na;
connectAttr "groupId47.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "sn_man_nose.msg" "materialInfo3.m";
connectAttr "snow_man_arm.oc" "lambert5SG.ss";
connectAttr "pCylinderShape3.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape3.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape4.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape4.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pSphere21Shape.iog.og[3]" "lambert5SG.dsm" -na;
connectAttr "groupId39.msg" "lambert5SG.gn" -na;
connectAttr "groupId40.msg" "lambert5SG.gn" -na;
connectAttr "groupId41.msg" "lambert5SG.gn" -na;
connectAttr "groupId42.msg" "lambert5SG.gn" -na;
connectAttr "groupId48.msg" "lambert5SG.gn" -na;
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "snow_man_arm.msg" "materialInfo4.m";
connectAttr "polySphere1.out" "polyConnectComponents1.ip";
connectAttr "polySurfaceShape1.o" "polyConnectComponents2.ip";
connectAttr "polySurfaceShape2.o" "polyConnectComponents3.ip";
connectAttr "polySphere2.out" "polyConnectComponents4.ip";
connectAttr "polySurfaceShape3.o" "polyConnectComponents5.ip";
connectAttr "polySurfaceShape4.o" "polyConnectComponents6.ip";
connectAttr "polySurfaceShape5.o" "polyConnectComponents7.ip";
connectAttr "polySurfaceShape6.o" "polyConnectComponents8.ip";
connectAttr "polyTweak3.out" "polyConnectComponents9.ip";
connectAttr "polyPyramid1.out" "polyTweak3.ip";
connectAttr "polySurfaceShape7.o" "polyConnectComponents10.ip";
connectAttr "polySurfaceShape8.o" "polyConnectComponents11.ip";
connectAttr "polySurfaceShape9.o" "polyConnectComponents12.ip";
connectAttr "polySurfaceShape10.o" "polyConnectComponents13.ip";
connectAttr "polySurfaceShape11.o" "polyConnectComponents14.ip";
connectAttr "polySurfaceShape12.o" "polyConnectComponents15.ip";
connectAttr "polySurfaceShape13.o" "polyConnectComponents16.ip";
connectAttr "polySurfaceShape14.o" "polyConnectComponents17.ip";
connectAttr "polyTweak4.out" "polyConnectComponents18.ip";
connectAttr "polyCylinder1.out" "polyTweak4.ip";
connectAttr "polyTweak5.out" "polyConnectComponents19.ip";
connectAttr "polyCylinder2.out" "polyTweak5.ip";
connectAttr "polyTweak6.out" "polyConnectComponents20.ip";
connectAttr "polyExtrudeFace5.out" "polyTweak6.ip";
connectAttr "polySurfaceShape15.o" "polyConnectComponents21.ip";
connectAttr "polySurfaceShape16.o" "polyConnectComponents22.ip";
connectAttr "pSphereShape1.o" "polyUnite1.ip[0]";
connectAttr "pSphereShape2.o" "polyUnite1.ip[1]";
connectAttr "pSphereShape3.o" "polyUnite1.ip[2]";
connectAttr "pSphereShape4.o" "polyUnite1.ip[3]";
connectAttr "pSphereShape5.o" "polyUnite1.ip[4]";
connectAttr "pSphereShape6.o" "polyUnite1.ip[5]";
connectAttr "pSphereShape7.o" "polyUnite1.ip[6]";
connectAttr "pSphereShape8.o" "polyUnite1.ip[7]";
connectAttr "pPyramidShape1.o" "polyUnite1.ip[8]";
connectAttr "pSphereShape9.o" "polyUnite1.ip[9]";
connectAttr "pSphereShape10.o" "polyUnite1.ip[10]";
connectAttr "pSphereShape13.o" "polyUnite1.ip[11]";
connectAttr "pSphereShape14.o" "polyUnite1.ip[12]";
connectAttr "pSphereShape15.o" "polyUnite1.ip[13]";
connectAttr "pSphereShape16.o" "polyUnite1.ip[14]";
connectAttr "pSphereShape17.o" "polyUnite1.ip[15]";
connectAttr "pSphereShape19.o" "polyUnite1.ip[16]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[17]";
connectAttr "pCylinderShape2.o" "polyUnite1.ip[18]";
connectAttr "pCylinderShape3.o" "polyUnite1.ip[19]";
connectAttr "pCylinderShape4.o" "polyUnite1.ip[20]";
connectAttr "pSphereShape20.o" "polyUnite1.ip[21]";
connectAttr "pSphereShape1.wm" "polyUnite1.im[0]";
connectAttr "pSphereShape2.wm" "polyUnite1.im[1]";
connectAttr "pSphereShape3.wm" "polyUnite1.im[2]";
connectAttr "pSphereShape4.wm" "polyUnite1.im[3]";
connectAttr "pSphereShape5.wm" "polyUnite1.im[4]";
connectAttr "pSphereShape6.wm" "polyUnite1.im[5]";
connectAttr "pSphereShape7.wm" "polyUnite1.im[6]";
connectAttr "pSphereShape8.wm" "polyUnite1.im[7]";
connectAttr "pPyramidShape1.wm" "polyUnite1.im[8]";
connectAttr "pSphereShape9.wm" "polyUnite1.im[9]";
connectAttr "pSphereShape10.wm" "polyUnite1.im[10]";
connectAttr "pSphereShape13.wm" "polyUnite1.im[11]";
connectAttr "pSphereShape14.wm" "polyUnite1.im[12]";
connectAttr "pSphereShape15.wm" "polyUnite1.im[13]";
connectAttr "pSphereShape16.wm" "polyUnite1.im[14]";
connectAttr "pSphereShape17.wm" "polyUnite1.im[15]";
connectAttr "pSphereShape19.wm" "polyUnite1.im[16]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[17]";
connectAttr "pCylinderShape2.wm" "polyUnite1.im[18]";
connectAttr "pCylinderShape3.wm" "polyUnite1.im[19]";
connectAttr "pCylinderShape4.wm" "polyUnite1.im[20]";
connectAttr "pSphereShape20.wm" "polyUnite1.im[21]";
connectAttr "polyConnectComponents1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyConnectComponents2.out" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polyConnectComponents3.out" "groupParts3.ig";
connectAttr "groupId5.id" "groupParts3.gi";
connectAttr "polyConnectComponents4.out" "groupParts4.ig";
connectAttr "groupId7.id" "groupParts4.gi";
connectAttr "polyConnectComponents5.out" "groupParts5.ig";
connectAttr "groupId9.id" "groupParts5.gi";
connectAttr "polyConnectComponents6.out" "groupParts6.ig";
connectAttr "groupId11.id" "groupParts6.gi";
connectAttr "polyConnectComponents7.out" "groupParts7.ig";
connectAttr "groupId13.id" "groupParts7.gi";
connectAttr "polyConnectComponents8.out" "groupParts8.ig";
connectAttr "groupId15.id" "groupParts8.gi";
connectAttr "polyConnectComponents9.out" "groupParts9.ig";
connectAttr "groupId17.id" "groupParts9.gi";
connectAttr "polyConnectComponents10.out" "groupParts10.ig";
connectAttr "groupId19.id" "groupParts10.gi";
connectAttr "polyConnectComponents11.out" "groupParts11.ig";
connectAttr "groupId21.id" "groupParts11.gi";
connectAttr "polyConnectComponents12.out" "groupParts12.ig";
connectAttr "groupId23.id" "groupParts12.gi";
connectAttr "polyConnectComponents13.out" "groupParts13.ig";
connectAttr "groupId25.id" "groupParts13.gi";
connectAttr "polyConnectComponents14.out" "groupParts14.ig";
connectAttr "groupId27.id" "groupParts14.gi";
connectAttr "polyConnectComponents15.out" "groupParts15.ig";
connectAttr "groupId29.id" "groupParts15.gi";
connectAttr "polyConnectComponents16.out" "groupParts16.ig";
connectAttr "groupId31.id" "groupParts16.gi";
connectAttr "polyConnectComponents17.out" "groupParts17.ig";
connectAttr "groupId33.id" "groupParts17.gi";
connectAttr "polyConnectComponents18.out" "groupParts18.ig";
connectAttr "groupId35.id" "groupParts18.gi";
connectAttr "polyConnectComponents19.out" "groupParts19.ig";
connectAttr "groupId37.id" "groupParts19.gi";
connectAttr "polyConnectComponents20.out" "groupParts20.ig";
connectAttr "groupId39.id" "groupParts20.gi";
connectAttr "polyConnectComponents21.out" "groupParts21.ig";
connectAttr "groupId41.id" "groupParts21.gi";
connectAttr "polyConnectComponents22.out" "groupParts22.ig";
connectAttr "groupId43.id" "groupParts22.gi";
connectAttr "polyUnite1.out" "groupParts23.ig";
connectAttr "groupId45.id" "groupParts23.gi";
connectAttr "groupParts23.og" "groupParts24.ig";
connectAttr "groupId46.id" "groupParts24.gi";
connectAttr "groupParts24.og" "groupParts25.ig";
connectAttr "groupId47.id" "groupParts25.gi";
connectAttr "groupParts25.og" "groupParts26.ig";
connectAttr "groupId48.id" "groupParts26.gi";
connectAttr "polyTorus1.out" "polySubdFace7.ip";
connectAttr "polySubdFace7.out" "polyExtrudeFace6.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace6.mp";
connectAttr "polyTweak7.out" "polyExtrudeFace7.ip";
connectAttr "pTorusShape1.wm" "polyExtrudeFace7.mp";
connectAttr "polyExtrudeFace6.out" "polyTweak7.ip";
connectAttr "scarf.oc" "lambert6SG.ss";
connectAttr "pTorusShape1.iog" "lambert6SG.dsm" -na;
connectAttr "lambert6SG.msg" "materialInfo5.sg";
connectAttr "scarf.msg" "materialInfo5.m";
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "snow_man_arm.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "snowman_base.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "lambert4SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "sbow_man_buttons.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "sn_man_nose.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "lambert5SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "scarf.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "lambert6SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "snowman_base.msg" ":defaultShaderList1.s" -na;
connectAttr "sbow_man_buttons.msg" ":defaultShaderList1.s" -na;
connectAttr "sn_man_nose.msg" ":defaultShaderList1.s" -na;
connectAttr "snow_man_arm.msg" ":defaultShaderList1.s" -na;
connectAttr "scarf.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of snow_man.ma
