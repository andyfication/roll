//Maya ASCII 2016 scene
//Name: start_menu_post.ma
//Last modified: Fri, Feb 10, 2017 11:29:47 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "6547C701-EB4C-ABDB-276C-35848F8BBDD1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 31.68815805013519 8.3583545190210131 -2.327032865642316 ;
	setAttr ".r" -type "double3" -14.738352729591558 94.199999999992741 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "35F64174-A244-BC24-3930-BEA162BD2675";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 32.8544750807825;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "C5CE77F1-9545-6B36-B79F-FB94CB8A7CF1";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "D611B1B4-6F4A-2218-FA6E-F786BEB81FA6";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "256E1A11-DF47-7143-1423-FA8F9641568E";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "F31EF405-A94F-9E7D-7661-DDB1DC6DAA9E";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 21.243617296155151;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "51067FE3-6F46-8205-B8A7-10A08BC6AF63";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.18897177332728 3.0157385259849225 -1.9949382039596857 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "83473B97-024B-598E-5443-858E7880AE8D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 16.326681575088923;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCylinder1";
	rename -uid "3D48C704-C847-6739-2D5B-189FFE2006D2";
	setAttr ".t" -type "double3" 0 -1.0351146216057483 0 ;
	setAttr ".s" -type "double3" 0.23383486158191019 4.7300025574355979 0.23383486158191019 ;
createNode mesh -n "pCylinderShape1" -p "pCylinder1";
	rename -uid "6F27A5DC-8544-73B5-7D06-85B092472632";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube1";
	rename -uid "789C206F-F14F-523B-6179-3E8DF9AD6C14";
	setAttr ".t" -type "double3" 0.16248190715424515 0 0 ;
	setAttr ".s" -type "double3" 0.15830326123693186 1.1485190073595 5.319835210559221 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "C034A572-9D43-4D11-9A1F-C080553B0DEC";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.9375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 173 ".pt";
	setAttr ".pt[2]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[3]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[4]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[5]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[11]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[13]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[14]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[15]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[16]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[17]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[18]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[20]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[33]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[35]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[36]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[37]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[38]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[39]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[40]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[41]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[44]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[48]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[52]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[58]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[59]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[60]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[61]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[62]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[63]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[66]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[69]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[73]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[74]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[75]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[76]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[77]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[79]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[80]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[81]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[82]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[84]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[85]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[86]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[87]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[103]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[104]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[105]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[107]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[108]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[109]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[110]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[111]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[112]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[113]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[115]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[116]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[117]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[125]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[133]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[135]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[136]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[137]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[138]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[139]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[140]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[141]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[142]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[145]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[149]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[155]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[158]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[159]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[160]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[161]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[162]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[163]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[164]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[165]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[166]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[167]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[172]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[177]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[178]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[180]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[181]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[182]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[183]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[184]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[186]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[187]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[190]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[195]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[198]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[199]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[200]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[203]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[205]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[206]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[210]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[214]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[229]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[233]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[234]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[235]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[236]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[237]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[238]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[239]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[240]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[241]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[242]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[243]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[244]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[250]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[251]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[252]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[254]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[256]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[257]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[258]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[278]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[279]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[280]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[281]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[282]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[283]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[284]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[285]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[286]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[287]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[288]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[289]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[290]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[291]" -type "float3" 0 0 -0.057211809 ;
	setAttr ".pt[295]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[296]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[297]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[298]" -type "float3" 0 -0.12230762 0 ;
	setAttr ".pt[301]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[313]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[314]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[315]" -type "float3" 0 0 0.074815437 ;
	setAttr ".pt[316]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[317]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[318]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[319]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[325]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[326]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[327]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[329]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[330]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[331]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[332]" -type "float3" 0 0 0.052810907 ;
	setAttr ".pt[335]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[336]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[337]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[338]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[340]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[341]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[342]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[343]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[344]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[345]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[346]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[347]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[348]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[349]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[350]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[351]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[369]" -type "float3" 0 0.091417499 0 ;
	setAttr ".pt[377]" -type "float3" 0 -0.12230762 0 ;
createNode transform -n "pCube2";
	rename -uid "0C3F2643-D740-6A9F-1623-45A4A7284F0B";
	setAttr ".t" -type "double3" 0.16248190715424515 1.3019397453173671 0 ;
	setAttr ".s" -type "double3" 0.15830326123693186 1.1485190073595 5.319835210559221 ;
createNode mesh -n "pCubeShape2" -p "pCube2";
	rename -uid "A799D07D-A24C-9023-4F0A-AFB6F4465739";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1.6875 0.9375 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 277 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[1]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[2]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[3]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[4]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[5]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[9]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[11]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[13]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[14]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[15]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[16]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[17]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[18]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[20]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[21]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[22]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[23]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[26]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[27]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[28]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[29]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[31]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[32]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[33]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[34]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[35]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[36]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[37]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[38]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[39]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[40]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[41]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[42]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[44]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[45]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[46]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[47]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[48]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[49]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[51]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[52]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[53]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[55]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[56]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[58]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[59]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[60]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[61]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[62]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[63]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[64]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[65]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[66]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[67]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[68]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[69]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[70]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[71]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[72]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[73]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[74]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[75]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[76]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[77]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[79]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[80]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[81]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[82]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[83]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[84]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[85]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[86]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[87]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[88]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[90]" -type "float3" 0 -7.4505806e-09 0 ;
	setAttr ".pt[93]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[97]" -type "float3" 0 -7.4505806e-09 0 ;
	setAttr ".pt[98]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[100]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[101]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[102]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[103]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[104]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[105]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[106]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[107]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[108]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[109]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[110]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[111]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[112]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[113]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[114]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[115]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[116]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[117]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[118]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[120]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[121]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[125]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[126]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[128]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[129]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[130]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[131]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[132]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[136]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[138]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[139]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[140]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[141]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[142]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[145]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[147]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[148]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[149]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[150]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[151]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[152]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[153]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[154]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[155]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[156]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[157]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[158]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[159]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[160]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[161]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[162]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[163]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[164]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[165]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[166]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[167]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[172]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[173]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[174]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[176]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[177]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[178]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[179]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[180]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[181]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[182]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[183]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[184]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[185]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[186]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[187]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[189]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[190]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[191]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[195]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[200]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[203]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[204]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[205]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[206]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[207]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[213]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[215]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[216]" -type "float3" 0 -0.054008909 0 ;
	setAttr ".pt[218]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[219]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[220]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[221]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[222]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[223]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[224]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[225]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[229]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[232]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[233]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[234]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[235]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[236]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[237]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[238]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[239]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[240]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[241]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[242]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[243]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[244]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[245]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[246]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[248]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[249]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[250]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[251]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[252]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[253]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[254]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[255]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[256]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[257]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[258]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[259]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[263]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[265]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[271]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[272]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[273]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[274]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[275]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[276]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[278]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[279]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[280]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[281]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[282]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[283]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[286]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[287]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[288]" -type "float3" 0 0 -0.051861849 ;
	setAttr ".pt[294]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[295]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[296]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[297]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[298]" -type "float3" 0 -2.9802322e-08 0 ;
	setAttr ".pt[299]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[300]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[301]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[302]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[303]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[304]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[305]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[306]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[307]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[308]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[309]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[310]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[311]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[312]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[313]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[314]" -type "float3" 0 -1.4901161e-08 0 ;
	setAttr ".pt[315]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[316]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[317]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[318]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[319]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[325]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[326]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[327]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[329]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[330]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[331]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[332]" -type "float3" 0 0 -0.080469437 ;
	setAttr ".pt[333]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[334]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[335]" -type "float3" 0 0.080073111 0 ;
	setAttr ".pt[336]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[337]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[338]" -type "float3" 0 7.4505806e-09 0 ;
	setAttr ".pt[339]" -type "float3" 0 0.080073178 -7.4505806e-09 ;
	setAttr ".pt[340]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[341]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[342]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[343]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[344]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[345]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[346]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[347]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[348]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[349]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[350]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[351]" -type "float3" 0 0.080073118 -7.4505806e-09 ;
	setAttr ".pt[352]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[353]" -type "float3" 0 0.080073103 0 ;
	setAttr ".pt[358]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[359]" -type "float3" 0 -0.054008909 0 ;
	setAttr ".pt[360]" -type "float3" 0 -0.054008894 0 ;
	setAttr ".pt[369]" -type "float3" 0 -1.8626451e-08 -3.7252903e-09 ;
	setAttr ".pt[370]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[371]" -type "float3" 0 0 -3.7252903e-09 ;
	setAttr ".pt[381]" -type "float3" 0 -0.054008894 0 ;
createNode mesh -n "polySurfaceShape2" -p "pCube2";
	rename -uid "C9B58199-714D-91DF-47B2-8193E7D5C157";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "48C2ED35-2348-7AE3-60DF-F4B1323322E9";
	setAttr ".t" -type "double3" 0.16248190715424515 2.58504389822511 0 ;
	setAttr ".s" -type "double3" 0.15830326123693186 1.1485190073595 5.319835210559221 ;
createNode mesh -n "pCubeShape3" -p "pCube3";
	rename -uid "091B61F2-4E4E-7D20-FA18-C08B9958B419";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1.75 0.625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 131 ".pt";
	setAttr ".pt[4]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[5]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[6]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[7]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[15]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[19]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[21]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[22]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[23]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[31]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[32]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[34]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[36]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[37]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[38]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[39]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[43]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[45]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[48]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[64]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[65]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[67]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[68]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[69]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[70]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[71]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[72]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[73]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[74]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[78]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[83]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[84]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[85]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[86]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[87]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[90]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[97]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[103]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[105]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[106]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[107]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[108]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[110]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[111]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[112]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[113]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[114]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[115]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[117]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[147]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[151]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[155]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[157]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[158]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[159]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[160]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[161]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[162]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[168]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[169]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[170]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[171]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[175]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[179]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[181]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[183]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[184]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[186]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[187]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[188]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[189]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[190]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[195]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[197]" -type "float3" 1.6653345e-16 -0.073872611 0 ;
	setAttr ".pt[198]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[200]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[201]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[204]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[216]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[232]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[233]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[234]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[236]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[237]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[238]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[239]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[240]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[241]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[242]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[243]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[247]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[251]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[252]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[253]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[256]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[264]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[266]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[294]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[295]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[296]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[297]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[301]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[304]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[309]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[313]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[314]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[315]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[316]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[317]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[318]" -type "float3" 0 0 -0.036764108 ;
	setAttr ".pt[320]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[321]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[322]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[323]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[324]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[328]" -type "float3" 0 0 0.036764108 ;
	setAttr ".pt[336]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[337]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[338]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[339]" -type "float3" 0 0.091693528 0 ;
	setAttr ".pt[340]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[341]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[342]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[343]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[346]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[347]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[348]" -type "float3" 0 0.11062638 0 ;
	setAttr ".pt[359]" -type "float3" 0 -0.15718894 0 ;
	setAttr ".pt[376]" -type "float3" 0 -0.11062638 0 ;
	setAttr ".pt[377]" -type "float3" 0 -0.11062638 0 ;
createNode mesh -n "polySurfaceShape1" -p "pCube3";
	rename -uid "FC180611-154D-7FCF-93F3-BC95FDA1381C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "F0628E03-5D4E-20B0-DC90-5ABC974E08AC";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "4CDB66B6-ED45-858B-8A0C-F79B02E0E4E4";
createNode displayLayer -n "defaultLayer";
	rename -uid "428186BB-1945-8FB4-E7F2-25B463ACA46A";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "BB326326-B843-4F9F-3D88-EF876FDAE374";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "0CF7995F-9741-02B9-4632-A7AA475B3B9D";
	setAttr ".g" yes;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "ABBEA782-7847-A3C9-190C-0CB0C5160F42";
	setAttr ".sa" 6;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyCube -n "polyCube1";
	rename -uid "E41C92EA-D444-983C-A54C-018086A34C28";
	setAttr ".cuv" 1;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "5F4830BB-C649-5DDF-F8DE-DF81BDE5ED54";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polySubdFace -n "polySubdFace2";
	rename -uid "D7660368-AF49-CDE1-4D5F-39817DD16CF3";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace3";
	rename -uid "86C3CD0A-3344-2FCF-B558-F29BFFA41604";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode polySubdFace -n "polySubdFace4";
	rename -uid "6F6B7113-D74E-C240-B30D-CBB52A70E377";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polySubdFace -n "polySubdFace5";
	rename -uid "9B4CED1C-8E4D-37B0-A1DE-1B96FBA045BD";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace6";
	rename -uid "67001621-7A46-D6F9-B1F4-2091A8092C70";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode polySubdFace -n "polySubdFace7";
	rename -uid "C5398CF7-6643-8350-ED61-BFA65E971058";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polySubdFace -n "polySubdFace8";
	rename -uid "F315BF4D-C04F-026B-05B0-65B1D4B623D6";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace9";
	rename -uid "09198433-9549-C8AF-C5CA-86BB8DAF74C4";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode lambert -n "start_menu_post";
	rename -uid "C2A2E07D-FC46-8908-A558-A6BDAFA7BD3A";
	setAttr ".c" -type "float3" 0.2071 0.1543 0.1543 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "3B948496-7C46-23CA-C492-F9B7055DCDD0";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "14448183-634C-A58A-F806-E69D7C6F1BBE";
createNode lambert -n "start_menu_details";
	rename -uid "60F24D5D-7741-31F8-13C5-39825DE029C5";
	setAttr ".c" -type "float3" 0.1806 0.1097 0.1097 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "83563D80-3046-FBCA-62F2-5DAB4AD96549";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
	rename -uid "C3953939-264F-51A1-110D-16BA1FFB02FA";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "18EB2EC1-3549-71D7-0D0D-8383D317D7D7";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 623\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 623\n            -height 308\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 622\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 622\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 623\n                -height 307\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 623\n            -height 307\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 622\n                -height 308\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 622\n            -height 308\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 432\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 432\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 308\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 622\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 623\\n    -height 307\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "A8D1F18C-0B4E-1959-E22F-54BF70500EEC";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "94EFEC93-6441-8AE6-9B6F-C6882CB838DC";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -742.85711333865333 -278.57141750199497 ;
	setAttr ".tgi[0].vh" -type "double2" 707.14282904352581 290.4761789337042 ;
	setAttr -s 4 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -62.857143402099609;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[2].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 262.85714721679688;
	setAttr ".tgi[0].ni[3].y" -62.857143402099609;
	setAttr ".tgi[0].ni[3].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "polyCylinder1.out" "pCylinderShape1.i";
connectAttr "polySubdFace9.out" "pCubeShape1.i";
connectAttr "polySubdFace6.out" "pCubeShape2.i";
connectAttr "polySubdFace3.out" "pCubeShape3.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polySurfaceShape1.o" "polySubdFace1.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polySubdFace3.ip";
connectAttr "polySurfaceShape2.o" "polySubdFace4.ip";
connectAttr "polySubdFace4.out" "polySubdFace5.ip";
connectAttr "polySubdFace5.out" "polySubdFace6.ip";
connectAttr "polyCube1.out" "polySubdFace7.ip";
connectAttr "polySubdFace7.out" "polySubdFace8.ip";
connectAttr "polySubdFace8.out" "polySubdFace9.ip";
connectAttr "start_menu_post.oc" "lambert2SG.ss";
connectAttr "pCylinderShape1.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "start_menu_post.msg" "materialInfo1.m";
connectAttr "start_menu_details.oc" "lambert3SG.ss";
connectAttr "pCubeShape2.iog" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.iog" "lambert3SG.dsm" -na;
connectAttr "pCubeShape1.iog" "lambert3SG.dsm" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "start_menu_details.msg" "materialInfo2.m";
connectAttr "start_menu_post.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "start_menu_details.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "start_menu_post.msg" ":defaultShaderList1.s" -na;
connectAttr "start_menu_details.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of start_menu_post.ma
