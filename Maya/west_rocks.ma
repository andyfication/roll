//Maya ASCII 2016 scene
//Name: west_rocks.ma
//Last modified: Fri, Jan 27, 2017 01:52:25 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "DAE8713B-8E41-4FFF-0738-00B24447421A";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 8.6654303865046067 55.973805658795996 -17.143126154146067 ;
	setAttr ".r" -type "double3" -67.538352729499834 -222.19999999998367 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "7733C4F6-9C4C-6DFB-3D7B-9C9A606AC48A";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 60.568830507335107;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -6.8790071199945775 0 -7.6828122863581427e-08 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "A0991903-F843-0C4B-2A52-568FE803D52B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "DA8909DE-044D-91AF-79B1-AF893DD0662D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "C9A96A66-8846-39C2-90B0-419E23EABC8D";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "AAD48EBB-0442-E4F3-42C3-748165BDE01F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "FBF9CE71-8B49-AA6F-0A56-9BB1F64564EC";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "A77982FC-CD46-9929-EB0F-439654596E26";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pSphere1";
	rename -uid "9995A873-6D41-D9D7-5A29-59ADA5D8F262";
	setAttr ".s" -type "double3" 2.3753666357244643 1.1464582411384097 2.3753666357244643 ;
createNode transform -n "transform3" -p "pSphere1";
	rename -uid "A0D3616E-F049-67E3-6F42-3490F128DD85";
	setAttr ".v" no;
createNode mesh -n "pSphereShape1" -p "transform3";
	rename -uid "FEB0A4FD-5B44-8CC9-07E5-EDB0CDEB687D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.40000000596046448 0.50000001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 22 ".pt[0:21]" -type "float3"  -3.7252903e-09 2.9802322e-08 
		-7.4505806e-08 0 2.9802322e-08 -2.9802322e-08 0 2.9802322e-08 1.1920929e-07 3.7252903e-09 
		2.9802322e-08 -1.3411045e-07 1.4901161e-08 2.9802322e-08 3.1974423e-14 1.4901161e-08 
		2.2351742e-08 1.4901161e-07 -2.9802322e-08 2.2351742e-08 1.1920929e-07 -5.9604645e-08 
		1.1175871e-07 -1.3411045e-07 1.4901161e-08 2.2351742e-08 -1.7881393e-07 2.9802322e-08 
		2.2351742e-08 3.1974423e-14 1.4901161e-08 -2.2351742e-08 5.9604645e-08 -2.9802322e-08 
		-2.2351742e-08 1.1920929e-07 -5.9604645e-08 -1.1175871e-07 -1.3411045e-07 1.4901161e-08 
		-2.2351742e-08 -1.7881393e-07 2.9802322e-08 -2.2351742e-08 3.1974423e-14 -3.7252903e-09 
		-2.9802322e-08 -7.4505806e-08 0 -2.9802322e-08 -2.9802322e-08 0 -2.9802322e-08 1.1920929e-07 
		3.7252903e-09 -2.9802322e-08 -1.3411045e-07 1.4901161e-08 -2.9802322e-08 3.1974423e-14 
		3.7252903e-09 2.9802322e-08 3.1974423e-14 3.7252903e-09 -2.9802322e-08 3.1974423e-14;
createNode transform -n "pSphere2";
	rename -uid "608F3A7D-4444-637A-1D2C-E98111AB7B99";
	setAttr ".t" -type "double3" 1.1123234604277583 0.20263352614620622 0 ;
	setAttr ".s" -type "double3" 1.2023968760672505 1 1.8071131709678889 ;
createNode transform -n "transform2" -p "pSphere2";
	rename -uid "9586C1B0-AB4F-E7BD-79EF-EF908A65B31F";
	setAttr ".v" no;
createNode mesh -n "pSphereShape2" -p "transform2";
	rename -uid "9EF9340C-E04F-7051-A51E-DFAE495DDC7F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere3";
	rename -uid "88C6FBB7-7A40-3D03-56BB-D19FF222241C";
	setAttr ".t" -type "double3" -0.49224697853533828 0.77449530740897243 1.6217069591881084 ;
	setAttr ".r" -type "double3" 62.612775387942158 0 0 ;
	setAttr ".s" -type "double3" 1.4284332899289032 1.435677900858461 2.2651171688247969 ;
createNode transform -n "transform1" -p "pSphere3";
	rename -uid "CCE66C9C-C746-56D3-D442-ED9C4655A34D";
	setAttr ".v" no;
createNode mesh -n "pSphereShape3" -p "transform1";
	rename -uid "A7ACA1E6-0D4B-E952-D475-2493582C747F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 0.1666666716337204 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -0.065186992 0.079751819 ;
	setAttr ".pt[5]" -type "float3" 0 -0.49081209 0.60047477 ;
	setAttr ".pt[6]" -type "float3" 0 -0.49081209 0.60047477 ;
	setAttr ".pt[10]" -type "float3" 0 -0.49081209 0.60047477 ;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere4";
	rename -uid "8A71B433-8B49-D36B-FA47-C287668F4DDF";
	setAttr ".t" -type "double3" 3.4262172843490806 0 9.4488137106619927 ;
	setAttr ".s" -type "double3" 1.2889620128358945 1.2889620128358945 1.2889620128358945 ;
createNode mesh -n "pSphereShape4" -p "pSphere4";
	rename -uid "122DB021-4942-0EB2-FD2B-9FA5E486E74D";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.3333333432674408 0.50000001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".pt[0:13]" -type "float3"  0 0.48167726 0 0 0.48167726 
		0 0 0.48167726 0 0 0.18398435 0 -0.61147499 0.22644925 -1.110223e-15 -0.34873059 
		0.18398438 0 0 -0.18398435 0 -0.61147499 -0.22644925 -1.110223e-15 -0.34873059 -0.18398435 
		0 -0.44086182 -0.48167726 0 -0.44353655 -0.48167726 0 -0.34873065 -0.48167729 0 0 
		0.59538591 0 -0.75918335 -0.59538591 0;
createNode transform -n "pCube1";
	rename -uid "2E97416E-A741-579D-6EFF-A587AA14194A";
	setAttr ".t" -type "double3" -6.7769578185328792 0 -6.3534996985790908 ;
	setAttr ".s" -type "double3" 2.2476421442863859 0.48241800767873422 6.1713774752973096 ;
createNode transform -n "transform8" -p "pCube1";
	rename -uid "332B5B85-244B-690B-B1D5-01B07252CBB3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform8";
	rename -uid "0883F594-344B-CA3D-FACD-4DB437C26734";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1.3125 1.5625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 186 ".pt";
	setAttr ".pt[0]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[1]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[2]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[3]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[5]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[7]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[10]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[12]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[14]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[16]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[18]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[22]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[23]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[24]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[25]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[27]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[33]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[35]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[42]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[44]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[46]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[47]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[48]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[49]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[50]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[51]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[53]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[54]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[56]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[59]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[63]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[65]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[67]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[68]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[70]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[72]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[73]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[74]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[75]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[76]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[77]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[78]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[81]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[82]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[83]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[88]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[89]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[90]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[91]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[92]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[96]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[97]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[98]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[99]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[100]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[101]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[104]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[105]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[114]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[118]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[119]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[120]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[121]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[122]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[123]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[124]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[126]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[127]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[131]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[133]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[134]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[135]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[136]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[141]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[148]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[149]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[150]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[151]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[152]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[156]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[161]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[163]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[164]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[165]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[166]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[167]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[175]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[177]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[179]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[181]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[188]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[189]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[190]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[191]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[192]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[193]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[194]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[195]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[196]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[197]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[199]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[201]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[203]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[204]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[207]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[210]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[214]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[216]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[218]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[219]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[229]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[245]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[246]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[250]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[252]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[254]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[255]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[256]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[257]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[261]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[263]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[265]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[269]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[271]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[272]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[273]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[274]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[275]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[278]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[279]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[280]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[286]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[287]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[288]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[289]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[293]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[294]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[300]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[301]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[304]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[307]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[308]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[309]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[313]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[314]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[315]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[316]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[317]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[320]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[321]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[322]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[323]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[333]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[334]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[335]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[336]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[337]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[338]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[340]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[352]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[353]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[354]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[355]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[356]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[357]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[358]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[359]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[360]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[361]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[362]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[363]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[364]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[365]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[366]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[367]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[373]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[374]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[377]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[378]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[379]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[380]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[381]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[382]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[383]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[384]" -type "float3" -0.15769573 0 0 ;
createNode transform -n "pCube2";
	rename -uid "BCC50CCD-C649-B426-7DE0-CCBAB39F13A8";
	setAttr ".t" -type "double3" -8.0321200831894686 0.25537877778449913 -6.3534996985790908 ;
	setAttr ".s" -type "double3" 2.2476421442863859 0.48241800767873422 6.1713774752973096 ;
createNode transform -n "transform7" -p "pCube2";
	rename -uid "02CA9B4A-FE4B-AACC-CD9F-40A2C9CEC7B9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform7";
	rename -uid "9B323781-6B43-E6CF-D679-59840C777AA5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:383]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.6875 2 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 490 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0 0 1 0 0 1 1 1 0 2 1 2 0 3
		 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1 0.5 0.5 0.5 0 0.5 4 1 0.5 0.5 1 0 0.5 0.5 1.5 1 1.5
		 1.5 1 0.5 2 -0.5 1 0 1.5 0.5 2.5 1 2.5 2 0.5 0.5 3 -1 0.5 0 2.5 0.5 3.5 1 3.5 1.5
		 0 0.5 4 -0.5 0 0 3.5 1.5 0.5 1.5 0 2 0.5 1.5 1 -0.5 0.5 -0.5 0 -0.5 1 -1 0.5 0.25
		 0.25 0 0.25 0.25 0 0.25 4 0.5 0.25 0.25 0.5 0.25 1.25 -0.25 1 0 1.25 0.25 1 0.5 1.25
		 0.25 1.5 0.25 2.25 -1 0.75 0 2.25 0.25 2 0.5 2.25 0.25 2.5 0.25 3.25 -0.75 0 0 3.25
		 0.25 3 0.5 3.25 0.25 3.5 1.25 0.25 1 0.25 1 3.75 1.25 0 1.5 0.25 1.25 0.5 -0.75 0.25
		 -1 0.25 0 2.75 -0.75 0 -0.5 0.25 -0.75 0.5 0.75 0.25 0.75 0 0.75 4 0.75 0.5 0.75
		 0.75 1 0.75 0.75 1 0.5 0.75 0.25 0.75 0 0.75 0.75 1.25 1 1.25 1.25 1 0.75 1.5 0.75
		 1.75 1 1.75 1.75 1 0.75 2 0.5 1.75 0.25 1.75 -0.75 1 0 1.75 0.75 2.25 1 2.25 2 0.75
		 0.75 2.5 0.75 2.75 1 2.75 2 0.25 0.75 3 0.5 2.75 0.25 2.75 0 2.75 0.75 3.25 1 3.25
		 1.75 0 0.75 3.5 0.75 3.75 1 3.75 0.75 4 0.5 3.75 0.25 3.75 0.25 4 -0.25 0 0 3.75
		 1.75 0.25 1.75 0 2 0.25 1.75 0.5 1.75 0.75 2 0.75 1.75 1 1.5 0.75 1.25 0.75 1.25
		 1 -0.25 0.25 -0.25 0 -0.25 0.5 -0.25 0.75 -0.25 1 -0.5 0.75 -0.75 0.75 -0.75 1 -1
		 0.75 0.125 0.375 0.125 0.5 0 0.375 0.125 0.25 0.25 0.375 0.125 1.375 0.125 1.5 -0.375
		 1 0 1.375 0.125 1.25 0.25 1.375 0.125 2.375 0.125 2.5 -1 0.625 0 2.375 0.125 2.25
		 0.25 2.375 0.125 3.375 0.125 3.5 -0.625 0 0 3.375 0.125 3.25 0.25 3.375 1.125 0.375
		 1.125 0.5 1 0.375 1.125 0.25 1.25 0.375 -0.875 0.375 -0.875 0.5 -1 0.375 0 2.625
		 -0.875 0.25 -0.75 0.375 0.625 0.125 0.5 0.125 0.625 0 0.625 4 0.75 0.125 0.625 0.25
		 0.875 0.625 0.875 0.5 1 0.625 0.875 0.75 0.75 0.625 0.375 0.875 0.5 0.875 0.375 1
		 0.25 0.875 0.375 0.75 0.625 1.125 0.5 1.125 0.625 1 0.75 1.125 0.625 1.25 0.875 1.625
		 0.875 1.5 1 1.625 1.625 1 0.875 1.75 0.75 1.625 0.375 1.875 0.5 1.875 0.375 2 0.25
		 1.875 0.375 1.75 0.625 2.125 0.5 2.125 0.625 2 0.75 2.125 0.625 2.25 0.875 2.625
		 0.875 2.5 1 2.625 2 0.375 0.875 2.75 0.75 2.625 0.375 2.875 0.5 2.875 0.375 3 0.25
		 2.875 0.375 2.75 0.625 3.125 0.5 3.125 0.625 3 0.75 3.125 0.625 3.25 0.875 3.625
		 0.875 3.5 1 3.625 1.375 0 0.875 3.75 0.75 3.625 0.375 3.875 0.5 3.875 0.375 0 0.375
		 4 0.25 3.875 0.375 3.75 1.625 0.125 1.5 0.125 1 3.375 1.625 0 1.75 0.125;
	setAttr ".uvst[0].uvsp[250:489]" 1.625 0.25 1.875 0.625 1.875 0.5 1 2.375 2
		 0.625 1.875 0.75 1.75 0.625 1.375 0.875 1.5 0.875 1 1.375 1.375 1 1.25 0.875 1.375
		 0.75 -0.375 0.125 -0.5 0.125 -0.375 0 0 3.625 -0.25 0.125 -0.375 0.25 -0.125 0.625
		 -0.125 0.5 0 0.625 -0.125 0.75 -0.25 0.625 -0.625 0.875 -0.5 0.875 -0.625 1 0 1.625
		 -0.75 0.875 -0.625 0.75 0.125 0.125 0 0.125 0.125 0 0.125 4 0.25 0.125 0.375 0.125
		 0.375 0 0.375 0.25 0.375 0.375 0.5 0.375 0.375 0.5 0.125 1.125 -0.125 1 0 1.125 0.125
		 1 0.25 1.125 0.375 1.125 0.375 1.25 0.375 1.375 0.5 1.375 0.375 1.5 0.125 2.125 -1
		 0.875 0 2.125 0.125 2 0.25 2.125 0.375 2.125 0.375 2.25 0.375 2.375 0.5 2.375 0.375
		 2.5 0.125 3.125 -0.875 0 0 3.125 0.125 3 0.25 3.125 0.375 3.125 0.375 3.25 0.375
		 3.375 0.5 3.375 0.375 3.5 1.125 0.125 1 0.125 1 3.875 1.125 0 1.25 0.125 1.375 0.125
		 1.375 0 1.375 0.25 1.375 0.375 1.5 0.375 1.375 0.5 -0.875 0.125 -1 0.125 0 2.875
		 -0.875 0 -0.75 0.125 -0.625 0.125 -0.625 0 -0.625 0.25 -0.625 0.375 -0.5 0.375 -0.625
		 0.5 0.875 0.125 0.875 0 0.875 4 0.875 0.25 0.875 0.375 0.75 0.375 0.625 0.375 0.625
		 0.5 0.875 0.875 1 0.875 0.875 1 0.75 0.875 0.625 0.875 0.625 0.75 0.625 0.625 0.5
		 0.625 0.125 0.875 0 0.875 0.125 0.75 0.125 0.625 0.25 0.625 0.375 0.625 0.875 1.125
		 1 1.125 1.125 1 0.875 1.25 0.875 1.375 1 1.375 0.75 1.375 0.625 1.375 0.625 1.5 0.875
		 1.875 1 1.875 1.875 1 0.875 2 0.75 1.875 0.625 1.875 0.625 1.75 0.625 1.625 0.5 1.625
		 0.125 1.875 -0.875 1 0 1.875 0.125 1.75 0.125 1.625 0 1.625 0.25 1.625 0.375 1.625
		 0.875 2.125 1 2.125 2 0.875 0.875 2.25 0.875 2.375 1 2.375 0.75 2.375 0.625 2.375
		 0.625 2.5 0.875 2.875 1 2.875 2 0.125 0.875 3 0.75 2.875 0.625 2.875 0.625 2.75 0.625
		 2.625 0.5 2.625 0.125 2.875 0 2.875 0.125 2.75 0.125 2.625 0 2.625 0.25 2.625 0.375
		 2.625 0.875 3.125 1 3.125 1.875 0 0.875 3.25 0.875 3.375 1 3.375 0.75 3.375 0.625
		 3.375 0.625 3.5 0.875 3.875 1 3.875 0.875 4 0.75 3.875 0.625 3.875 0.625 4 0.625
		 3.75 0.625 3.625 0.5 3.625 0.125 3.875 0.125 4 -0.125 0 0 3.875 0.125 3.75 0.125
		 3.625 0 3.625 0.25 3.625 0.375 3.625 1.875 0.125 1.875 0 2 0.125 1.875 0.25 1.875
		 0.375 2 0.375 1.75 0.375 1.625 0.375 1.625 0.5 1.875 0.875 2 0.875 1.875 1 1.75 0.875
		 1.625 0.875 1.625 1 1.625 0.75 1.625 0.625 1.5 0.625 1.125 0.875 1.125 1 1.125 0.75
		 1.125 0.625 1.25 0.625 1.375 0.625 -0.125 0.125 -0.125 0 -0.125 0.25 -0.125 0.375
		 -0.25 0.375 -0.375 0.375 -0.375 0.5 -0.125 0.875 -0.125 1 -0.25 0.875 -0.375 0.875
		 -0.375 1 -0.375 0.75 -0.375 0.625 -0.5 0.625 -0.875 0.875 -0.875 1 -1 0.875 -0.875
		 0.75 -0.875 0.625 -1 0.625 -0.75 0.625 -0.625 0.625;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 248 ".pt";
	setAttr ".pt[0]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[1]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[2]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[3]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[5]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[7]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[8]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[9]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[10]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[11]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[12]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[14]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[16]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[18]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[19]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[22]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[23]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[24]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[25]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[26]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[27]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[28]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[29]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[33]" -type "float3" 0.14255016 0 0.067911103 ;
	setAttr ".pt[35]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[42]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[44]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[46]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[47]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[48]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[49]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[50]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[51]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[53]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[54]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[56]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[57]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[58]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[59]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[60]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[61]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[63]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[65]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[67]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[68]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[69]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[70]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[72]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[73]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[74]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[75]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[76]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[77]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[78]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[79]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[81]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[82]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[83]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[88]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[89]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[90]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[91]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[92]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[96]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[97]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[98]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[99]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[100]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[101]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[104]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[105]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[114]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[118]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[119]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[120]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[121]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[122]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[123]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[124]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[126]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[127]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[128]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[129]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[130]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[131]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[132]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[133]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[134]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[135]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[136]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[137]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[139]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[141]" -type "float3" 0.14255016 0 0.067911103 ;
	setAttr ".pt[142]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[143]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[145]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[146]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[148]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[149]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[150]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[151]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[152]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[156]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[158]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[160]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[161]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[162]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[163]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[164]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[165]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[166]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[167]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[169]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[175]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[177]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[178]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[179]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[180]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[181]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[182]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[185]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[188]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[189]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[190]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[191]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[192]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[193]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[194]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[195]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[196]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[197]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[198]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[199]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[200]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[201]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[202]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[203]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[204]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[207]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[210]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[214]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[216]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[218]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[219]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[221]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[222]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[223]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[224]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[225]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[226]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[229]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[245]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[246]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[250]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[252]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[254]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[255]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[256]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[257]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[258]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[259]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[260]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[261]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[262]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[263]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[265]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[269]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[271]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[272]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[273]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[274]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[275]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[276]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[277]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[278]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[279]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[280]" -type "float3" -0.10608101 1.9073486e-06 0.054779429 ;
	setAttr ".pt[281]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[282]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[283]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[284]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[285]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[286]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[287]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[288]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[289]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[291]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[292]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[293]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[294]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[300]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[301]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[304]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[307]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[308]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[309]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[313]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[314]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[315]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[316]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[317]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[318]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[319]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[320]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[321]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[322]" -type "float3" -0.077783279 0 0.04946316 ;
	setAttr ".pt[323]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[324]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[325]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[326]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[327]" -type "float3" 0 0 0.075522237 ;
	setAttr ".pt[333]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[334]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[335]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[336]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[337]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[338]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[340]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[341]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[342]" -type "float3" 0 0 0.10622251 ;
	setAttr ".pt[352]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[353]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[354]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[355]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[356]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[357]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[358]" -type "float3" -0.077783279 0 -0.0167301 ;
	setAttr ".pt[359]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[360]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[361]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[362]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[363]" -type "float3" 0.20669153 0 0 ;
	setAttr ".pt[364]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[365]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[366]" -type "float3" -0.09371458 0 0.10622251 ;
	setAttr ".pt[367]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[368]" -type "float3" 0.34486887 0 0 ;
	setAttr ".pt[373]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[374]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[377]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[378]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[379]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[380]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[381]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[382]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[383]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[384]" -type "float3" -0.15769573 0 0 ;
	setAttr -s 386 ".vt";
	setAttr ".vt[0:165]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 0 0 0.5 0 -0.5 0.5 0.5 0 0.5
		 0 0.5 0.5 -0.5 0 0.5 0 0.5 0 0.5 0.5 0 0 0.5 -0.5 -0.5 0.5 0 0 0 -0.5 0.5 0 -0.5
		 0 -0.5 -0.5 -0.5 0 -0.5 0 -0.5 0 0.5 -0.5 0 -0.5 -0.5 0 0.5 0 0 -0.5 0 0 -0.25 -0.25 0.5
		 -0.5 -0.25 0.5 -0.25 -0.5 0.5 0 -0.25 0.5 -0.25 0 0.5 -0.25 0.5 0.25 -0.5 0.5 0.25
		 -0.25 0.5 0.5 0 0.5 0.25 -0.25 0.5 0 -0.25 0.25 -0.5 -0.5 0.25 -0.5 -0.25 0.5 -0.5
		 0 0.25 -0.5 -0.25 0 -0.5 -0.25 -0.5 -0.25 -0.5 -0.5 -0.25 -0.25 -0.5 -0.5 0 -0.5 -0.25
		 -0.25 -0.5 0 0.5 -0.25 0.25 0.5 -0.25 0.5 0.5 -0.5 0.25 0.5 -0.25 0 0.5 0 0.25 -0.5 -0.25 -0.25
		 -0.5 -0.25 -0.5 -0.5 -0.25 0 -0.5 0 -0.25 0.25 -0.25 0.5 0.25 -0.5 0.5 0.25 0 0.5
		 0.25 0.25 0.5 0.5 0.25 0.5 0.25 0.5 0.5 0 0.25 0.5 -0.25 0.25 0.5 -0.5 0.25 0.5 0.25 0.5 0.25
		 0.5 0.5 0.25 0.25 0.5 0 0.25 0.5 -0.25 0.5 0.5 -0.25 0.25 0.5 -0.5 0 0.5 -0.25 -0.25 0.5 -0.25
		 -0.5 0.5 -0.25 0.25 0.25 -0.5 0.5 0.25 -0.5 0.25 0 -0.5 0.25 -0.25 -0.5 0.5 -0.25 -0.5
		 0.25 -0.5 -0.5 0 -0.25 -0.5 -0.25 -0.25 -0.5 0.25 -0.5 -0.25 0.5 -0.5 -0.25 0.25 -0.5 0
		 0.25 -0.5 0.25 0 -0.5 0.25 -0.25 -0.5 0.25 -0.5 -0.5 0.25 0.5 -0.25 -0.25 0.5 0 -0.25
		 0.5 0.25 -0.25 0.5 0.25 0 0.5 0.25 0.25 -0.5 -0.25 0.25 -0.5 0 0.25 -0.5 0.25 0.25
		 -0.5 0.25 0 -0.5 0.25 -0.25 -0.375 -0.125 0.5 -0.375 0 0.5 -0.5 -0.125 0.5 -0.375 -0.25 0.5
		 -0.25 -0.125 0.5 -0.375 0.5 0.125 -0.375 0.5 0 -0.5 0.5 0.125 -0.375 0.5 0.25 -0.25 0.5 0.125
		 -0.375 0.125 -0.5 -0.375 0 -0.5 -0.5 0.125 -0.5 -0.375 0.25 -0.5 -0.25 0.125 -0.5
		 -0.375 -0.5 -0.125 -0.375 -0.5 0 -0.5 -0.5 -0.125 -0.375 -0.5 -0.25 -0.25 -0.5 -0.125
		 0.5 -0.125 0.375 0.5 0 0.375 0.5 -0.125 0.5 0.5 -0.25 0.375 0.5 -0.125 0.25 -0.5 -0.125 -0.375
		 -0.5 0 -0.375 -0.5 -0.125 -0.5 -0.5 -0.25 -0.375 -0.5 -0.125 -0.25 0.125 -0.375 0.5
		 0 -0.375 0.5 0.125 -0.5 0.5 0.25 -0.375 0.5 0.125 -0.25 0.5 0.375 0.125 0.5 0.375 0 0.5
		 0.5 0.125 0.5 0.375 0.25 0.5 0.25 0.125 0.5 -0.125 0.375 0.5 0 0.375 0.5 -0.125 0.5 0.5
		 -0.25 0.375 0.5 -0.125 0.25 0.5 0.125 0.5 0.375 0 0.5 0.375 0.125 0.5 0.5 0.25 0.5 0.375
		 0.125 0.5 0.25 0.375 0.5 -0.125 0.375 0.5 0 0.5 0.5 -0.125 0.375 0.5 -0.25 0.25 0.5 -0.125
		 -0.125 0.5 -0.375 0 0.5 -0.375 -0.125 0.5 -0.5 -0.25 0.5 -0.375 -0.125 0.5 -0.25
		 0.125 0.375 -0.5 0 0.375 -0.5 0.125 0.5 -0.5 0.25 0.375 -0.5 0.125 0.25 -0.5 0.375 -0.125 -0.5
		 0.375 0 -0.5 0.5 -0.125 -0.5;
	setAttr ".vt[166:331]" 0.375 -0.25 -0.5 0.25 -0.125 -0.5 -0.125 -0.375 -0.5
		 0 -0.375 -0.5 -0.125 -0.5 -0.5 -0.25 -0.375 -0.5 -0.125 -0.25 -0.5 0.125 -0.5 -0.375
		 0 -0.5 -0.375 0.125 -0.5 -0.5 0.25 -0.5 -0.375 0.125 -0.5 -0.25 0.375 -0.5 0.125
		 0.375 -0.5 0 0.5 -0.5 0.125 0.375 -0.5 0.25 0.25 -0.5 0.125 -0.125 -0.5 0.375 0 -0.5 0.375
		 -0.125 -0.5 0.5 -0.25 -0.5 0.375 -0.125 -0.5 0.25 0.5 -0.375 -0.125 0.5 -0.375 0
		 0.5 -0.5 -0.125 0.5 -0.375 -0.25 0.5 -0.25 -0.125 0.5 0.125 -0.375 0.5 0 -0.375 0.5 0.125 -0.5
		 0.5 0.25 -0.375 0.5 0.125 -0.25 0.5 0.375 0.125 0.5 0.375 0 0.5 0.5 0.125 0.5 0.375 0.25
		 0.5 0.25 0.125 -0.5 -0.375 0.125 -0.5 -0.375 0 -0.5 -0.5 0.125 -0.5 -0.375 0.25 -0.5 -0.25 0.125
		 -0.5 0.125 0.375 -0.5 0 0.375 -0.5 0.125 0.5 -0.5 0.25 0.375 -0.5 0.125 0.25 -0.5 0.375 -0.125
		 -0.5 0.375 0 -0.5 0.5 -0.125 -0.5 0.375 -0.25 -0.5 0.25 -0.125 -0.375 -0.375 0.5
		 -0.5 -0.375 0.5 -0.375 -0.5 0.5 -0.25 -0.375 0.5 -0.125 -0.375 0.5 -0.125 -0.25 0.5
		 -0.125 -0.125 0.5 0 -0.125 0.5 -0.125 0 0.5 -0.375 0.5 0.375 -0.5 0.5 0.375 -0.375 0.5 0.5
		 -0.25 0.5 0.375 -0.125 0.5 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 0 0.5 0.125 -0.125 0.5 0
		 -0.375 0.375 -0.5 -0.5 0.375 -0.5 -0.375 0.5 -0.5 -0.25 0.375 -0.5 -0.125 0.375 -0.5
		 -0.125 0.25 -0.5 -0.125 0.125 -0.5 0 0.125 -0.5 -0.125 0 -0.5 -0.375 -0.5 -0.375
		 -0.5 -0.5 -0.375 -0.375 -0.5 -0.5 -0.25 -0.5 -0.375 -0.125 -0.5 -0.375 -0.125 -0.5 -0.25
		 -0.125 -0.5 -0.125 0 -0.5 -0.125 -0.125 -0.5 0 0.5 -0.375 0.375 0.5 -0.375 0.5 0.5 -0.5 0.375
		 0.5 -0.375 0.25 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5 -0.125 0.125 0.5 -0.125 0 0.5 0 0.125
		 -0.5 -0.375 -0.375 -0.5 -0.375 -0.5 -0.5 -0.375 -0.25 -0.5 -0.375 -0.125 -0.5 -0.25 -0.125
		 -0.5 -0.125 -0.125 -0.5 -0.125 0 -0.5 0 -0.125 0.375 -0.375 0.5 0.375 -0.5 0.5 0.375 -0.25 0.5
		 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 -0.125 0.5 0.125 0 0.5 0.375 0.375 0.5 0.5 0.375 0.5
		 0.375 0.5 0.5 0.25 0.375 0.5 0.125 0.375 0.5 0.125 0.25 0.5 0.125 0.125 0.5 0 0.125 0.5
		 -0.375 0.375 0.5 -0.5 0.375 0.5 -0.375 0.25 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5
		 -0.125 0.125 0.5 0.375 0.5 0.375 0.5 0.5 0.375 0.375 0.5 0.25 0.375 0.5 0.125 0.25 0.5 0.125
		 0.125 0.5 0.125 0.125 0.5 0 0.375 0.5 -0.375 0.5 0.5 -0.375 0.375 0.5 -0.5 0.25 0.5 -0.375
		 0.125 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5 -0.125 0 0.5 -0.125 -0.375 0.5 -0.375
		 -0.5 0.5 -0.375 -0.375 0.5 -0.25 -0.375 0.5 -0.125 -0.25 0.5 -0.125 -0.125 0.5 -0.125
		 0.375 0.375 -0.5 0.5 0.375 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5
		 0.125 0.125 -0.5 0.125 0 -0.5 0.375 -0.375 -0.5 0.5 -0.375 -0.5 0.375 -0.5 -0.5 0.25 -0.375 -0.5
		 0.125 -0.375 -0.5 0.125 -0.25 -0.5 0.125 -0.125 -0.5 0 -0.125 -0.5 -0.375 -0.375 -0.5
		 -0.375 -0.25 -0.5 -0.375 -0.125 -0.5 -0.25 -0.125 -0.5;
	setAttr ".vt[332:385]" -0.125 -0.125 -0.5 0.375 -0.5 -0.375 0.5 -0.5 -0.375
		 0.375 -0.5 -0.25 0.375 -0.5 -0.125 0.25 -0.5 -0.125 0.125 -0.5 -0.125 0.125 -0.5 0
		 0.375 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5 0.125
		 0 -0.5 0.125 -0.375 -0.5 0.375 -0.5 -0.5 0.375 -0.375 -0.5 0.25 -0.375 -0.5 0.125
		 -0.25 -0.5 0.125 -0.125 -0.5 0.125 0.5 -0.375 -0.375 0.5 -0.25 -0.375 0.5 -0.125 -0.375
		 0.5 -0.125 -0.25 0.5 -0.125 -0.125 0.5 0 -0.125 0.5 0.375 -0.375 0.5 0.375 -0.25
		 0.5 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 -0.125 0.5 0.125 0 0.5 0.375 0.375 0.5 0.25 0.375
		 0.5 0.125 0.375 0.5 0.125 0.25 0.5 0.125 0.125 -0.5 -0.375 0.375 -0.5 -0.25 0.375
		 -0.5 -0.125 0.375 -0.5 -0.125 0.25 -0.5 -0.125 0.125 -0.5 0 0.125 -0.5 0.375 0.375
		 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5 0.125 0.125 -0.5 0.125 0 -0.5 0.375 -0.375
		 -0.5 0.25 -0.375 -0.5 0.125 -0.375 -0.5 0.125 -0.25 -0.5 0.125 -0.125;
	setAttr -s 768 ".ed";
	setAttr ".ed[0:165]"  0 220 0 2 229 0 4 238 0 6 247 0 0 219 0 1 255 0 2 228 0
		 3 293 0 4 237 0 5 314 0 6 246 0 7 334 0 9 130 0 10 135 0 11 145 0 12 210 0 9 129 0
		 10 134 0 11 139 0 12 99 0 14 150 0 15 160 0 16 215 0 11 144 0 14 149 0 15 154 0 16 104 0
		 18 165 0 19 175 0 20 125 0 15 159 0 18 164 0 19 169 0 20 109 0 22 180 0 23 205 0
		 19 174 0 22 179 0 9 184 0 23 114 0 22 189 0 18 194 0 14 199 0 10 119 0 23 204 0 12 209 0
		 16 214 0 20 124 0 27 100 0 28 185 0 29 225 0 30 226 0 27 101 0 28 221 0 29 223 0
		 30 102 0 32 105 0 33 140 0 34 234 0 35 235 0 32 106 0 33 230 0 34 232 0 35 107 0
		 37 110 0 38 155 0 39 243 0 40 244 0 37 111 0 38 239 0 39 241 0 40 112 0 42 115 0
		 43 170 0 44 252 0 45 253 0 42 116 0 43 248 0 44 250 0 45 117 0 47 120 0 48 256 0
		 49 261 0 50 262 0 47 121 0 48 257 0 49 259 0 50 122 0 52 264 0 53 269 0 54 270 0
		 52 126 0 42 265 0 53 267 0 54 127 0 56 272 0 57 277 0 56 131 0 47 273 0 57 275 0
		 29 132 0 59 279 0 60 280 0 61 285 0 59 136 0 60 281 0 61 283 0 57 137 0 63 287 0
		 33 141 0 63 288 0 30 290 0 61 142 0 65 200 0 66 298 0 60 146 0 65 294 0 66 296 0
		 34 147 0 68 300 0 69 301 0 70 306 0 68 151 0 69 302 0 70 304 0 66 152 0 72 308 0
		 38 156 0 72 309 0 35 311 0 70 157 0 74 195 0 75 319 0 69 161 0 74 315 0 75 317 0
		 39 162 0 77 321 0 78 322 0 79 327 0 77 166 0 78 323 0 79 325 0 75 167 0 43 171 0
		 52 329 0 40 331 0 79 172 0 82 190 0 83 339 0 78 176 0 82 335 0 83 337 0 44 177 0
		 85 345 0 48 181 0 56 341 0 85 343 0 83 182 0 87 347 0 28 186 0 87 348 0 45 350 0
		 85 187 0 89 357 0 82 191 0;
	setAttr ".ed[166:331]" 77 353 0 89 355 0 49 192 0 91 363 0 74 196 0 68 359 0
		 91 361 0 89 197 0 65 201 0 59 365 0 50 367 0 91 202 0 94 374 0 87 206 0 27 370 0
		 94 372 0 53 207 0 96 380 0 63 211 0 32 376 0 96 378 0 94 212 0 72 216 0 37 382 0
		 54 384 0 96 217 0 99 30 0 100 12 0 101 26 0 102 26 0 99 98 0 100 98 0 101 98 0 102 98 0
		 104 35 0 105 16 0 106 31 0 107 31 0 104 103 0 105 103 0 106 103 0 107 103 0 109 40 0
		 110 20 0 111 36 0 112 36 0 109 108 0 110 108 0 111 108 0 112 108 0 114 45 0 115 23 0
		 116 41 0 117 41 0 114 113 0 115 113 0 116 113 0 117 113 0 119 50 0 120 10 0 121 46 0
		 122 46 0 119 118 0 120 118 0 121 118 0 122 118 0 124 54 0 125 52 0 126 51 0 127 51 0
		 124 123 0 125 123 0 126 123 0 127 123 0 129 29 0 130 56 0 131 55 0 132 55 0 129 128 0
		 130 128 0 131 128 0 132 128 0 134 57 0 135 59 0 136 58 0 137 58 0 134 133 0 135 133 0
		 136 133 0 137 133 0 139 61 0 140 11 0 141 62 0 142 62 0 139 138 0 140 138 0 141 138 0
		 142 138 0 144 34 0 145 60 0 146 64 0 147 64 0 144 143 0 145 143 0 146 143 0 147 143 0
		 149 66 0 150 68 0 151 67 0 152 67 0 149 148 0 150 148 0 151 148 0 152 148 0 154 70 0
		 155 15 0 156 71 0 157 71 0 154 153 0 155 153 0 156 153 0 157 153 0 159 39 0 160 69 0
		 161 73 0 162 73 0 159 158 0 160 158 0 161 158 0 162 158 0 164 75 0 165 77 0 166 76 0
		 167 76 0 164 163 0 165 163 0 166 163 0 167 163 0 169 79 0 170 19 0 171 80 0 172 80 0
		 169 168 0 170 168 0 171 168 0 172 168 0 174 44 0 175 78 0 176 81 0 177 81 0 174 173 0
		 175 173 0 176 173 0 177 173 0 179 83 0 180 48 0 181 84 0 182 84 0 179 178 0 180 178 0
		 181 178 0 182 178 0 184 85 0 185 9 0 186 86 0 187 86 0;
	setAttr ".ed[332:497]" 184 183 0 185 183 0 186 183 0 187 183 0 189 49 0 190 22 0
		 191 88 0 192 88 0 189 188 0 190 188 0 191 188 0 192 188 0 194 89 0 195 18 0 196 90 0
		 197 90 0 194 193 0 195 193 0 196 193 0 197 193 0 199 91 0 200 14 0 201 92 0 202 92 0
		 199 198 0 200 198 0 201 198 0 202 198 0 204 53 0 205 87 0 206 93 0 207 93 0 204 203 0
		 205 203 0 206 203 0 207 203 0 209 94 0 210 63 0 211 95 0 212 95 0 209 208 0 210 208 0
		 211 208 0 212 208 0 214 96 0 215 72 0 216 97 0 217 97 0 214 213 0 215 213 0 216 213 0
		 217 213 0 219 27 0 220 28 0 221 26 0 219 218 0 220 218 0 221 218 0 101 218 0 223 26 0
		 185 222 0 129 222 0 223 222 0 221 222 0 225 8 0 226 8 0 225 224 0 226 224 0 102 224 0
		 223 224 0 228 32 0 229 33 0 230 31 0 228 227 0 229 227 0 230 227 0 106 227 0 232 31 0
		 140 231 0 144 231 0 232 231 0 230 231 0 234 13 0 235 13 0 234 233 0 235 233 0 107 233 0
		 232 233 0 237 37 0 238 38 0 239 36 0 237 236 0 238 236 0 239 236 0 111 236 0 241 36 0
		 155 240 0 159 240 0 241 240 0 239 240 0 243 17 0 244 17 0 243 242 0 244 242 0 112 242 0
		 241 242 0 246 42 0 247 43 0 248 41 0 246 245 0 247 245 0 248 245 0 116 245 0 250 41 0
		 170 249 0 174 249 0 250 249 0 248 249 0 252 21 0 253 21 0 252 251 0 253 251 0 117 251 0
		 250 251 0 255 47 0 256 1 0 257 46 0 255 254 0 256 254 0 257 254 0 121 254 0 259 46 0
		 180 258 0 189 258 0 259 258 0 257 258 0 261 24 0 262 24 0 261 260 0 262 260 0 122 260 0
		 259 260 0 264 6 0 265 51 0 264 263 0 246 263 0 265 263 0 126 263 0 267 51 0 115 266 0
		 204 266 0 267 266 0 265 266 0 269 25 0 270 25 0 269 268 0 270 268 0 127 268 0 267 268 0
		 272 1 0 273 55 0 272 271 0 255 271 0 273 271 0 131 271 0 275 55 0;
	setAttr ".ed[498:663]" 120 274 0 134 274 0 275 274 0 273 274 0 277 8 0 277 276 0
		 225 276 0 132 276 0 275 276 0 279 3 0 280 3 0 281 58 0 279 278 0 280 278 0 281 278 0
		 136 278 0 283 58 0 145 282 0 139 282 0 283 282 0 281 282 0 285 8 0 285 284 0 277 284 0
		 137 284 0 283 284 0 287 2 0 288 62 0 229 286 0 287 286 0 288 286 0 141 286 0 290 62 0
		 210 289 0 99 289 0 290 289 0 288 289 0 226 291 0 285 291 0 142 291 0 290 291 0 293 65 0
		 294 64 0 280 292 0 293 292 0 294 292 0 146 292 0 296 64 0 200 295 0 149 295 0 296 295 0
		 294 295 0 298 13 0 298 297 0 234 297 0 147 297 0 296 297 0 300 5 0 301 5 0 302 67 0
		 300 299 0 301 299 0 302 299 0 151 299 0 304 67 0 160 303 0 154 303 0 304 303 0 302 303 0
		 306 13 0 306 305 0 298 305 0 152 305 0 304 305 0 308 4 0 309 71 0 238 307 0 308 307 0
		 309 307 0 156 307 0 311 71 0 215 310 0 104 310 0 311 310 0 309 310 0 235 312 0 306 312 0
		 157 312 0 311 312 0 314 74 0 315 73 0 301 313 0 314 313 0 315 313 0 161 313 0 317 73 0
		 195 316 0 164 316 0 317 316 0 315 316 0 319 17 0 319 318 0 243 318 0 162 318 0 317 318 0
		 321 7 0 322 7 0 323 76 0 321 320 0 322 320 0 323 320 0 166 320 0 325 76 0 175 324 0
		 169 324 0 325 324 0 323 324 0 327 17 0 327 326 0 319 326 0 167 326 0 325 326 0 329 80 0
		 247 328 0 264 328 0 329 328 0 171 328 0 331 80 0 125 330 0 109 330 0 331 330 0 329 330 0
		 244 332 0 327 332 0 172 332 0 331 332 0 334 82 0 335 81 0 322 333 0 334 333 0 335 333 0
		 176 333 0 337 81 0 190 336 0 179 336 0 337 336 0 335 336 0 339 21 0 339 338 0 252 338 0
		 177 338 0 337 338 0 341 84 0 256 340 0 272 340 0 341 340 0 181 340 0 343 84 0 130 342 0
		 184 342 0 343 342 0 341 342 0 345 21 0 345 344 0 339 344 0 182 344 0;
	setAttr ".ed[664:767]" 343 344 0 347 0 0 348 86 0 220 346 0 347 346 0 348 346 0
		 186 346 0 350 86 0 205 349 0 114 349 0 350 349 0 348 349 0 253 351 0 345 351 0 187 351 0
		 350 351 0 353 88 0 334 352 0 321 352 0 353 352 0 191 352 0 355 88 0 165 354 0 194 354 0
		 355 354 0 353 354 0 357 24 0 357 356 0 261 356 0 192 356 0 355 356 0 359 90 0 314 358 0
		 300 358 0 359 358 0 196 358 0 361 90 0 150 360 0 199 360 0 361 360 0 359 360 0 363 24 0
		 363 362 0 357 362 0 197 362 0 361 362 0 365 92 0 293 364 0 279 364 0 365 364 0 201 364 0
		 367 92 0 135 366 0 119 366 0 367 366 0 365 366 0 262 368 0 363 368 0 202 368 0 367 368 0
		 370 93 0 347 369 0 219 369 0 370 369 0 206 369 0 372 93 0 100 371 0 209 371 0 372 371 0
		 370 371 0 374 25 0 374 373 0 269 373 0 207 373 0 372 373 0 376 95 0 287 375 0 228 375 0
		 376 375 0 211 375 0 378 95 0 105 377 0 214 377 0 378 377 0 376 377 0 380 25 0 380 379 0
		 374 379 0 212 379 0 378 379 0 382 97 0 308 381 0 237 381 0 382 381 0 216 381 0 384 97 0
		 110 383 0 124 383 0 384 383 0 382 383 0 270 385 0 380 385 0 217 385 0 384 385 0;
	setAttr -s 384 -ch 1536 ".fc[0:383]" -type "polyFaces" 
		f 4 -56 -193 196 -200
		mu 0 4 150 51 147 146
		f 4 -64 -201 204 -208
		mu 0 4 156 57 152 151
		f 4 -72 -209 212 -216
		mu 0 4 162 63 158 157
		f 4 -80 -217 220 -224
		mu 0 4 168 69 164 163
		f 4 -88 -225 228 -232
		mu 0 4 173 75 170 169
		f 4 -95 -233 236 -240
		mu 0 4 179 81 175 174
		f 4 -101 -241 244 -248
		mu 0 4 185 50 181 180
		f 4 -108 -249 252 -256
		mu 0 4 190 85 187 186
		f 4 -113 -257 260 -264
		mu 0 4 195 89 192 191
		f 4 -119 -265 268 -272
		mu 0 4 200 56 197 196
		f 4 -126 -273 276 -280
		mu 0 4 206 95 202 201
		f 4 -131 -281 284 -288
		mu 0 4 211 100 208 207
		f 4 -137 -289 292 -296
		mu 0 4 216 62 213 212
		f 4 -144 -297 300 -304
		mu 0 4 222 107 218 217
		f 4 -148 -305 308 -312
		mu 0 4 227 112 224 223
		f 4 -154 -313 316 -320
		mu 0 4 232 68 229 228
		f 4 -159 -321 324 -328
		mu 0 4 238 118 234 233
		f 4 -164 -329 332 -336
		mu 0 4 244 122 240 239
		f 4 -169 -337 340 -344
		mu 0 4 250 74 246 245
		f 4 -174 -345 348 -352
		mu 0 4 256 130 252 251
		f 4 -178 -353 356 -360
		mu 0 4 262 134 258 257
		f 4 -183 -361 364 -368
		mu 0 4 268 80 264 263
		f 4 -188 -369 372 -376
		mu 0 4 273 139 270 269
		f 4 -192 -377 380 -384
		mu 0 4 279 142 275 274
		f 4 -53 -385 387 -391
		mu 0 4 149 47 281 280
		f 4 -54 49 392 -396
		mu 0 4 284 48 286 285
		f 4 -55 50 398 -402
		mu 0 4 287 50 289 288
		f 4 -61 -403 405 -409
		mu 0 4 155 54 293 291
		f 4 -62 57 410 -414
		mu 0 4 295 55 193 296
		f 4 -63 58 416 -420
		mu 0 4 297 56 299 298
		f 4 -69 -421 423 -427
		mu 0 4 161 60 303 301
		f 4 -70 65 428 -432
		mu 0 4 305 61 209 306
		f 4 -71 66 434 -438
		mu 0 4 307 62 309 308
		f 4 -77 -439 441 -445
		mu 0 4 167 66 313 311
		f 4 -78 73 446 -450
		mu 0 4 315 67 225 316
		f 4 -79 74 452 -456
		mu 0 4 317 68 319 318
		f 4 -85 -457 459 -463
		mu 0 4 172 71 322 321
		f 4 -86 -322 464 -468
		mu 0 4 325 73 327 326
		f 4 -87 82 470 -474
		mu 0 4 328 74 330 329
		f 4 -92 88 476 -480
		mu 0 4 178 77 333 332
		f 4 -93 72 481 -485
		mu 0 4 336 79 338 337
		f 4 -94 89 487 -491
		mu 0 4 339 80 341 340
		f 4 -98 95 493 -497
		mu 0 4 184 83 344 343
		f 4 -99 80 498 -502
		mu 0 4 346 71 171 347
		f 4 -100 96 503 -507
		mu 0 4 348 85 350 349
		f 4 -105 101 510 -514
		mu 0 4 189 87 352 351
		f 4 -106 -266 515 -519
		mu 0 4 354 88 198 355
		f 4 -107 103 520 -524
		mu 0 4 356 89 358 357
		f 4 -110 -404 526 -530
		mu 0 4 194 55 294 359
		f 4 -111 -370 531 -535
		mu 0 4 361 91 271 362
		f 4 -112 51 535 -539
		mu 0 4 363 51 290 364
		f 4 -116 102 541 -545
		mu 0 4 199 88 353 365
		f 4 -117 113 546 -550
		mu 0 4 368 93 370 369
		f 4 -118 114 551 -555
		mu 0 4 371 95 373 372
		f 4 -123 119 558 -562
		mu 0 4 205 97 375 374
		f 4 -124 -290 563 -567
		mu 0 4 378 99 214 379
		f 4 -125 121 568 -572
		mu 0 4 380 100 382 381
		f 4 -128 -422 574 -578
		mu 0 4 210 61 304 383
		f 4 -129 -378 579 -583
		mu 0 4 386 103 388 387
		f 4 -130 59 583 -587
		mu 0 4 389 57 300 390
		f 4 -134 120 589 -593
		mu 0 4 215 99 377 391
		f 4 -135 131 594 -598
		mu 0 4 394 105 396 395
		f 4 -136 132 599 -603
		mu 0 4 397 107 399 398
		f 4 -141 137 606 -610
		mu 0 4 221 109 401 400
		f 4 -142 -314 611 -615
		mu 0 4 404 111 230 405
		f 4 -143 139 616 -620
		mu 0 4 406 112 408 407
		f 4 -145 -440 621 -625
		mu 0 4 226 67 314 409
		f 4 -146 -234 626 -630
		mu 0 4 411 114 413 412
		f 4 -147 67 630 -634
		mu 0 4 414 63 310 415
		f 4 -151 138 636 -640
		mu 0 4 231 111 403 416
		f 4 -152 148 641 -645
		mu 0 4 419 116 421 420
		f 4 -153 149 646 -650
		mu 0 4 422 118 424 423
		f 4 -156 81 651 -655
		mu 0 4 237 120 426 425
		f 4 -157 -242 656 -660
		mu 0 4 428 121 430 429
		f 4 -158 154 661 -665
		mu 0 4 431 122 433 432
		f 4 -161 -386 667 -671
		mu 0 4 243 124 435 434
		f 4 -162 -362 672 -676
		mu 0 4 438 126 440 439
		f 4 -163 75 676 -680
		mu 0 4 441 69 320 442
		f 4 -166 -635 681 -685
		mu 0 4 249 128 444 443
		f 4 -167 -298 686 -690
		mu 0 4 446 129 448 447
		f 4 -168 164 691 -695
		mu 0 4 449 130 451 450
		f 4 -171 -588 696 -700
		mu 0 4 255 132 453 452
		f 4 -172 -274 701 -705
		mu 0 4 455 133 457 456
		f 4 -173 169 706 -710
		mu 0 4 458 134 460 459
		f 4 -175 -540 711 -715
		mu 0 4 261 136 462 461
		f 4 -176 -250 716 -720
		mu 0 4 463 87 188 464
		f 4 -177 83 720 -724
		mu 0 4 465 75 331 466
		f 4 -180 159 725 -729
		mu 0 4 267 138 468 467
		f 4 -181 48 730 -734
		mu 0 4 469 47 148 470
		f 4 -182 178 735 -739
		mu 0 4 471 139 473 472
		f 4 -185 108 740 -744
		mu 0 4 272 91 360 474
		f 4 -186 56 745 -749
		mu 0 4 476 141 478 477
		f 4 -187 183 750 -754
		mu 0 4 479 142 481 480
		f 4 -189 126 755 -759
		mu 0 4 278 144 483 482
		f 4 -190 64 760 -764
		mu 0 4 485 145 487 486
		f 4 -191 90 764 -768
		mu 0 4 488 81 342 489
		f 4 -20 -194 197 -197
		mu 0 4 147 19 148 146
		f 4 -49 52 198 -198
		mu 0 4 148 47 149 146
		f 4 194 -196 199 -199
		mu 0 4 149 46 150 146
		f 4 -27 -202 205 -205
		mu 0 4 152 25 154 151
		f 4 -57 60 206 -206
		mu 0 4 154 54 155 151
		f 4 202 -204 207 -207
		mu 0 4 155 52 156 151
		f 4 -34 -210 213 -213
		mu 0 4 158 31 160 157
		f 4 -65 68 214 -214
		mu 0 4 160 60 161 157
		f 4 210 -212 215 -215
		mu 0 4 161 58 162 157
		f 4 -40 -218 221 -221
		mu 0 4 164 37 166 163
		f 4 -73 76 222 -222
		mu 0 4 166 66 167 163
		f 4 218 -220 223 -223
		mu 0 4 167 64 168 163
		f 4 -44 -226 229 -229
		mu 0 4 170 17 171 169
		f 4 -81 84 230 -230
		mu 0 4 171 71 172 169
		f 4 226 -228 231 -231
		mu 0 4 172 70 173 169
		f 4 -48 29 237 -237
		mu 0 4 175 45 176 174
		f 4 233 91 238 -238
		mu 0 4 176 77 178 174
		f 4 234 -236 239 -239
		mu 0 4 178 76 179 174
		f 4 -17 12 245 -245
		mu 0 4 181 15 182 180
		f 4 241 97 246 -246
		mu 0 4 182 83 184 180
		f 4 242 -244 247 -247
		mu 0 4 184 82 185 180
		f 4 -18 13 253 -253
		mu 0 4 187 17 188 186
		f 4 249 104 254 -254
		mu 0 4 188 87 189 186
		f 4 250 -252 255 -255
		mu 0 4 189 86 190 186
		f 4 -19 -258 261 -261
		mu 0 4 192 18 193 191
		f 4 -58 109 262 -262
		mu 0 4 193 55 194 191
		f 4 258 -260 263 -263
		mu 0 4 194 90 195 191
		f 4 -24 14 269 -269
		mu 0 4 197 18 198 196
		f 4 265 115 270 -270
		mu 0 4 198 88 199 196
		f 4 266 -268 271 -271
		mu 0 4 199 92 200 196
		f 4 -25 20 277 -277
		mu 0 4 202 21 203 201
		f 4 273 122 278 -278
		mu 0 4 203 97 205 201
		f 4 274 -276 279 -279
		mu 0 4 205 96 206 201
		f 4 -26 -282 285 -285
		mu 0 4 208 23 209 207
		f 4 -66 127 286 -286
		mu 0 4 209 61 210 207
		f 4 282 -284 287 -287
		mu 0 4 210 101 211 207
		f 4 -31 21 293 -293
		mu 0 4 213 23 214 212
		f 4 289 133 294 -294
		mu 0 4 214 99 215 212
		f 4 290 -292 295 -295
		mu 0 4 215 104 216 212
		f 4 -32 27 301 -301
		mu 0 4 218 27 219 217
		f 4 297 140 302 -302
		mu 0 4 219 109 221 217
		f 4 298 -300 303 -303
		mu 0 4 221 108 222 217
		f 4 -33 -306 309 -309
		mu 0 4 224 29 225 223
		f 4 -74 144 310 -310
		mu 0 4 225 67 226 223
		f 4 306 -308 311 -311
		mu 0 4 226 113 227 223
		f 4 -37 28 317 -317
		mu 0 4 229 29 230 228
		f 4 313 150 318 -318
		mu 0 4 230 111 231 228
		f 4 314 -316 319 -319
		mu 0 4 231 115 232 228
		f 4 -38 34 325 -325
		mu 0 4 234 33 235 233
		f 4 321 155 326 -326
		mu 0 4 235 120 237 233
		f 4 322 -324 327 -327
		mu 0 4 237 119 238 233
		f 4 -39 -330 333 -333
		mu 0 4 240 35 242 239
		f 4 -50 160 334 -334
		mu 0 4 242 124 243 239
		f 4 330 -332 335 -335
		mu 0 4 243 123 244 239
		f 4 -41 -338 341 -341
		mu 0 4 246 39 248 245
		f 4 -149 165 342 -342
		mu 0 4 248 128 249 245
		f 4 338 -340 343 -343
		mu 0 4 249 127 250 245
		f 4 -42 -346 349 -349
		mu 0 4 252 40 254 251
		f 4 -132 170 350 -350
		mu 0 4 254 132 255 251
		f 4 346 -348 351 -351
		mu 0 4 255 131 256 251
		f 4 -43 -354 357 -357
		mu 0 4 258 41 260 257
		f 4 -114 174 358 -358
		mu 0 4 260 136 261 257
		f 4 354 -356 359 -359
		mu 0 4 261 135 262 257
		f 4 -45 35 365 -365
		mu 0 4 264 43 265 263
		f 4 361 179 366 -366
		mu 0 4 265 138 267 263
		f 4 362 -364 367 -367
		mu 0 4 267 137 268 263
		f 4 -46 15 373 -373
		mu 0 4 270 19 271 269
		f 4 369 184 374 -374
		mu 0 4 271 91 272 269
		f 4 370 -372 375 -375
		mu 0 4 272 140 273 269
		f 4 -47 22 381 -381
		mu 0 4 275 44 276 274
		f 4 377 188 382 -382
		mu 0 4 276 144 278 274
		f 4 378 -380 383 -383
		mu 0 4 278 143 279 274
		f 4 -5 0 388 -388
		mu 0 4 281 0 282 280
		f 4 385 53 389 -389
		mu 0 4 282 48 284 280
		f 4 386 -195 390 -390
		mu 0 4 284 46 149 280
		f 4 329 16 393 -393
		mu 0 4 286 15 181 285
		f 4 240 54 394 -394
		mu 0 4 181 50 287 285
		f 4 391 -387 395 -395
		mu 0 4 287 46 284 285
		f 4 396 -398 399 -399
		mu 0 4 289 14 290 288
		f 4 -52 55 400 -400
		mu 0 4 290 51 150 288
		f 4 195 -392 401 -401
		mu 0 4 150 46 287 288
		f 4 -7 1 406 -406
		mu 0 4 293 2 294 291
		f 4 403 61 407 -407
		mu 0 4 294 55 295 291
		f 4 404 -203 408 -408
		mu 0 4 295 52 155 291
		f 4 257 23 411 -411
		mu 0 4 193 18 197 296
		f 4 264 62 412 -412
		mu 0 4 197 56 297 296
		f 4 409 -405 413 -413
		mu 0 4 297 52 295 296
		f 4 414 -416 417 -417
		mu 0 4 299 20 300 298
		f 4 -60 63 418 -418
		mu 0 4 300 57 156 298
		f 4 203 -410 419 -419
		mu 0 4 156 52 297 298
		f 4 -9 2 424 -424
		mu 0 4 303 4 304 301
		f 4 421 69 425 -425
		mu 0 4 304 61 305 301
		f 4 422 -211 426 -426
		mu 0 4 305 58 161 301
		f 4 281 30 429 -429
		mu 0 4 209 23 213 306
		f 4 288 70 430 -430
		mu 0 4 213 62 307 306
		f 4 427 -423 431 -431
		mu 0 4 307 58 305 306
		f 4 432 -434 435 -435
		mu 0 4 309 26 310 308
		f 4 -68 71 436 -436
		mu 0 4 310 63 162 308
		f 4 211 -428 437 -437
		mu 0 4 162 58 307 308
		f 4 -11 3 442 -442
		mu 0 4 313 6 314 311
		f 4 439 77 443 -443
		mu 0 4 314 67 315 311
		f 4 440 -219 444 -444
		mu 0 4 315 64 167 311
		f 4 305 36 447 -447
		mu 0 4 225 29 229 316
		f 4 312 78 448 -448
		mu 0 4 229 68 317 316
		f 4 445 -441 449 -449
		mu 0 4 317 64 315 316
		f 4 450 -452 453 -453
		mu 0 4 319 32 320 318
		f 4 -76 79 454 -454
		mu 0 4 320 69 168 318
		f 4 219 -446 455 -455
		mu 0 4 168 64 317 318
		f 4 -6 -458 460 -460
		mu 0 4 322 1 324 321
		f 4 -82 85 461 -461
		mu 0 4 324 73 325 321
		f 4 458 -227 462 -462
		mu 0 4 325 70 172 321
		f 4 -35 40 465 -465
		mu 0 4 327 39 246 326
		f 4 336 86 466 -466
		mu 0 4 246 74 328 326
		f 4 463 -459 467 -467
		mu 0 4 328 70 325 326
		f 4 468 -470 471 -471
		mu 0 4 330 38 331 329
		f 4 -84 87 472 -472
		mu 0 4 331 75 173 329
		f 4 227 -464 473 -473
		mu 0 4 173 70 328 329
		f 4 474 10 477 -477
		mu 0 4 333 12 335 332
		f 4 438 92 478 -478
		mu 0 4 335 79 336 332
		f 4 475 -235 479 -479
		mu 0 4 336 76 178 332
		f 4 217 44 482 -482
		mu 0 4 338 43 264 337
		f 4 360 93 483 -483
		mu 0 4 264 80 339 337
		f 4 480 -476 484 -484
		mu 0 4 339 76 336 337
		f 4 485 -487 488 -488
		mu 0 4 341 42 342 340
		f 4 -91 94 489 -489
		mu 0 4 342 81 179 340
		f 4 235 -481 490 -490
		mu 0 4 179 76 339 340
		f 4 491 5 494 -494
		mu 0 4 344 1 322 343
		f 4 456 98 495 -495
		mu 0 4 322 71 346 343
		f 4 492 -243 496 -496
		mu 0 4 346 82 184 343
		f 4 225 17 499 -499
		mu 0 4 171 17 187 347
		f 4 248 99 500 -500
		mu 0 4 187 85 348 347
		f 4 497 -493 501 -501
		mu 0 4 348 82 346 347
		f 4 502 -397 504 -504
		mu 0 4 350 14 289 349
		f 4 -51 100 505 -505
		mu 0 4 289 50 185 349
		f 4 243 -498 506 -506
		mu 0 4 185 82 348 349
		f 4 507 -509 511 -511
		mu 0 4 352 3 353 351
		f 4 -103 105 512 -512
		mu 0 4 353 88 354 351
		f 4 509 -251 513 -513
		mu 0 4 354 86 189 351
		f 4 -15 18 516 -516
		mu 0 4 198 18 192 355
		f 4 256 106 517 -517
		mu 0 4 192 89 356 355
		f 4 514 -510 518 -518
		mu 0 4 356 86 354 355
		f 4 519 -503 521 -521
		mu 0 4 358 14 350 357
		f 4 -97 107 522 -522
		mu 0 4 350 85 190 357
		f 4 251 -515 523 -523
		mu 0 4 190 86 356 357
		f 4 -2 -525 527 -527
		mu 0 4 294 2 360 359
		f 4 -109 110 528 -528
		mu 0 4 360 91 361 359
		f 4 525 -259 529 -529
		mu 0 4 361 90 194 359
		f 4 -16 19 532 -532
		mu 0 4 271 19 147 362
		f 4 192 111 533 -533
		mu 0 4 147 51 363 362
		f 4 530 -526 534 -534
		mu 0 4 363 90 361 362
		f 4 397 -520 536 -536
		mu 0 4 290 14 358 364
		f 4 -104 112 537 -537
		mu 0 4 358 89 195 364
		f 4 259 -531 538 -538
		mu 0 4 195 90 363 364
		f 4 508 7 542 -542
		mu 0 4 353 3 366 365
		f 4 539 116 543 -543
		mu 0 4 366 93 368 365
		f 4 540 -267 544 -544
		mu 0 4 368 92 199 365
		f 4 353 24 547 -547
		mu 0 4 370 21 202 369
		f 4 272 117 548 -548
		mu 0 4 202 95 371 369
		f 4 545 -541 549 -549
		mu 0 4 371 92 368 369
		f 4 550 -415 552 -552
		mu 0 4 373 20 299 372
		f 4 -59 118 553 -553
		mu 0 4 299 56 200 372
		f 4 267 -546 554 -554
		mu 0 4 200 92 371 372
		f 4 555 -557 559 -559
		mu 0 4 375 5 377 374
		f 4 -121 123 560 -560
		mu 0 4 377 99 378 374
		f 4 557 -275 561 -561
		mu 0 4 378 96 205 374
		f 4 -22 25 564 -564
		mu 0 4 214 23 208 379
		f 4 280 124 565 -565
		mu 0 4 208 100 380 379
		f 4 562 -558 566 -566
		mu 0 4 380 96 378 379
		f 4 567 -551 569 -569
		mu 0 4 382 20 373 381
		f 4 -115 125 570 -570
		mu 0 4 373 95 206 381
		f 4 275 -563 571 -571
		mu 0 4 206 96 380 381
		f 4 -3 -573 575 -575
		mu 0 4 304 4 385 383
		f 4 -127 128 576 -576
		mu 0 4 385 103 386 383
		f 4 573 -283 577 -577
		mu 0 4 386 101 210 383
		f 4 -23 26 580 -580
		mu 0 4 388 25 152 387
		f 4 200 129 581 -581
		mu 0 4 152 57 389 387
		f 4 578 -574 582 -582
		mu 0 4 389 101 386 387
		f 4 415 -568 584 -584
		mu 0 4 300 20 382 390
		f 4 -122 130 585 -585
		mu 0 4 382 100 211 390
		f 4 283 -579 586 -586
		mu 0 4 211 101 389 390
		f 4 556 9 590 -590
		mu 0 4 377 5 392 391
		f 4 587 134 591 -591
		mu 0 4 392 105 394 391
		f 4 588 -291 592 -592
		mu 0 4 394 104 215 391
		f 4 345 31 595 -595
		mu 0 4 396 27 218 395
		f 4 296 135 596 -596
		mu 0 4 218 107 397 395
		f 4 593 -589 597 -597
		mu 0 4 397 104 394 395
		f 4 598 -433 600 -600
		mu 0 4 399 26 309 398
		f 4 -67 136 601 -601
		mu 0 4 309 62 216 398
		f 4 291 -594 602 -602
		mu 0 4 216 104 397 398
		f 4 603 -605 607 -607
		mu 0 4 401 7 403 400
		f 4 -139 141 608 -608
		mu 0 4 403 111 404 400
		f 4 605 -299 609 -609
		mu 0 4 404 108 221 400
		f 4 -29 32 612 -612
		mu 0 4 230 29 224 405
		f 4 304 142 613 -613
		mu 0 4 224 112 406 405
		f 4 610 -606 614 -614
		mu 0 4 406 108 404 405
		f 4 615 -599 617 -617
		mu 0 4 408 26 399 407
		f 4 -133 143 618 -618
		mu 0 4 399 107 222 407
		f 4 299 -611 619 -619
		mu 0 4 222 108 406 407
		f 4 -4 -475 622 -622
		mu 0 4 314 6 410 409
		f 4 -89 145 623 -623
		mu 0 4 410 114 411 409
		f 4 620 -307 624 -624
		mu 0 4 411 113 226 409
		f 4 -30 33 627 -627
		mu 0 4 413 31 158 412
		f 4 208 146 628 -628
		mu 0 4 158 63 414 412
		f 4 625 -621 629 -629
		mu 0 4 414 113 411 412
		f 4 433 -616 631 -631
		mu 0 4 310 26 408 415
		f 4 -140 147 632 -632
		mu 0 4 408 112 227 415
		f 4 307 -626 633 -633
		mu 0 4 227 113 414 415
		f 4 604 11 637 -637
		mu 0 4 403 7 417 416
		f 4 634 151 638 -638
		mu 0 4 417 116 419 416
		f 4 635 -315 639 -639
		mu 0 4 419 115 231 416
		f 4 337 37 642 -642
		mu 0 4 421 33 234 420
		f 4 320 152 643 -643
		mu 0 4 234 118 422 420
		f 4 640 -636 644 -644
		mu 0 4 422 115 419 420
		f 4 645 -451 647 -647
		mu 0 4 424 32 319 423
		f 4 -75 153 648 -648
		mu 0 4 319 68 232 423
		f 4 315 -641 649 -649
		mu 0 4 232 115 422 423
		f 4 457 -492 652 -652
		mu 0 4 426 9 427 425
		f 4 -96 156 653 -653
		mu 0 4 427 121 428 425
		f 4 650 -323 654 -654
		mu 0 4 428 119 237 425
		f 4 -13 38 657 -657
		mu 0 4 430 35 240 429
		f 4 328 157 658 -658
		mu 0 4 240 122 431 429
		f 4 655 -651 659 -659
		mu 0 4 431 119 428 429
		f 4 660 -646 662 -662
		mu 0 4 433 32 424 432
		f 4 -150 158 663 -663
		mu 0 4 424 118 238 432
		f 4 323 -656 664 -664
		mu 0 4 238 119 431 432
		f 4 -1 -666 668 -668
		mu 0 4 435 8 437 434
		f 4 -160 161 669 -669
		mu 0 4 437 126 438 434
		f 4 666 -331 670 -670
		mu 0 4 438 123 243 434
		f 4 -36 39 673 -673
		mu 0 4 440 37 164 439
		f 4 216 162 674 -674
		mu 0 4 164 69 441 439
		f 4 671 -667 675 -675
		mu 0 4 441 123 438 439
		f 4 451 -661 677 -677
		mu 0 4 320 32 433 442
		f 4 -155 163 678 -678
		mu 0 4 433 122 244 442
		f 4 331 -672 679 -679
		mu 0 4 244 123 441 442
		f 4 -12 -604 682 -682
		mu 0 4 444 10 445 443
		f 4 -138 166 683 -683
		mu 0 4 445 129 446 443
		f 4 680 -339 684 -684
		mu 0 4 446 127 249 443
		f 4 -28 41 687 -687
		mu 0 4 448 40 252 447
		f 4 344 167 688 -688
		mu 0 4 252 130 449 447
		f 4 685 -681 689 -689
		mu 0 4 449 127 446 447
		f 4 690 -469 692 -692
		mu 0 4 451 38 330 450
		f 4 -83 168 693 -693
		mu 0 4 330 74 250 450
		f 4 339 -686 694 -694
		mu 0 4 250 127 449 450
		f 4 -10 -556 697 -697
		mu 0 4 453 11 454 452
		f 4 -120 171 698 -698
		mu 0 4 454 133 455 452
		f 4 695 -347 699 -699
		mu 0 4 455 131 255 452
		f 4 -21 42 702 -702
		mu 0 4 457 41 258 456
		f 4 352 172 703 -703
		mu 0 4 258 134 458 456
		f 4 700 -696 704 -704
		mu 0 4 458 131 455 456
		f 4 705 -691 707 -707
		mu 0 4 460 38 451 459
		f 4 -165 173 708 -708
		mu 0 4 451 130 256 459
		f 4 347 -701 709 -709
		mu 0 4 256 131 458 459
		f 4 -8 -508 712 -712
		mu 0 4 462 3 352 461
		f 4 -102 175 713 -713
		mu 0 4 352 87 463 461
		f 4 710 -355 714 -714
		mu 0 4 463 135 261 461
		f 4 -14 43 717 -717
		mu 0 4 188 17 170 464
		f 4 224 176 718 -718
		mu 0 4 170 75 465 464
		f 4 715 -711 719 -719
		mu 0 4 465 135 463 464
		f 4 469 -706 721 -721
		mu 0 4 331 38 460 466
		f 4 -170 177 722 -722
		mu 0 4 460 134 262 466
		f 4 355 -716 723 -723
		mu 0 4 262 135 465 466
		f 4 665 4 726 -726
		mu 0 4 468 0 281 467
		f 4 384 180 727 -727
		mu 0 4 281 47 469 467
		f 4 724 -363 728 -728
		mu 0 4 469 137 267 467
		f 4 193 45 731 -731
		mu 0 4 148 19 270 470
		f 4 368 181 732 -732
		mu 0 4 270 139 471 470
		f 4 729 -725 733 -733
		mu 0 4 471 137 469 470
		f 4 734 -486 736 -736
		mu 0 4 473 42 341 472
		f 4 -90 182 737 -737
		mu 0 4 341 80 268 472
		f 4 363 -730 738 -738
		mu 0 4 268 137 471 472
		f 4 524 6 741 -741
		mu 0 4 360 2 475 474
		f 4 402 185 742 -742
		mu 0 4 475 141 476 474
		f 4 739 -371 743 -743
		mu 0 4 476 140 272 474
		f 4 201 46 746 -746
		mu 0 4 478 44 275 477
		f 4 376 186 747 -747
		mu 0 4 275 142 479 477
		f 4 744 -740 748 -748
		mu 0 4 479 140 476 477
		f 4 749 -735 751 -751
		mu 0 4 481 42 473 480
		f 4 -179 187 752 -752
		mu 0 4 473 139 273 480
		f 4 371 -745 753 -753
		mu 0 4 273 140 479 480
		f 4 572 8 756 -756
		mu 0 4 483 13 484 482
		f 4 420 189 757 -757
		mu 0 4 484 145 485 482
		f 4 754 -379 758 -758
		mu 0 4 485 143 278 482
		f 4 209 47 761 -761
		mu 0 4 487 45 175 486
		f 4 232 190 762 -762
		mu 0 4 175 81 488 486
		f 4 759 -755 763 -763
		mu 0 4 488 143 485 486
		f 4 486 -750 765 -765
		mu 0 4 342 42 481 489
		f 4 -184 191 766 -766
		mu 0 4 481 142 279 489
		f 4 379 -760 767 -767
		mu 0 4 279 143 488 489;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere5";
	rename -uid "9F6209E0-1E47-99B0-6CBD-E9A417291F90";
	setAttr ".t" -type "double3" -10.265129361379298 -0.37376313609585821 -4.406742767204797 ;
	setAttr ".s" -type "double3" 2.3796923589698187 1.9791238702758591 3.5765008129524478 ;
createNode transform -n "transform6" -p "pSphere5";
	rename -uid "E31AD52D-604C-7E83-44F1-D8ADF62D5B15";
	setAttr ".v" no;
createNode mesh -n "pSphereShape5" -p "transform6";
	rename -uid "F8793C31-2A4B-3997-7B59-9CAFF7A565AB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere6";
	rename -uid "6365B839-494A-67A3-46B9-1BB16D4679AE";
	setAttr ".t" -type "double3" -8.637014321996153 -0.37376313609585821 -9.1732254243269225 ;
	setAttr ".r" -type "double3" 0 -43.117021306612351 0 ;
	setAttr ".s" -type "double3" 2.3796923589698187 1.9791238702758591 3.5765008129524478 ;
createNode transform -n "transform5" -p "pSphere6";
	rename -uid "406F0C1C-B741-9E40-131C-97B4D71204DD";
	setAttr ".v" no;
createNode mesh -n "pSphereShape6" -p "transform5";
	rename -uid "C5FF3FC8-794C-C1DF-A9FC-FB95C228798A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "A4C632A4-6141-B257-7E43-A4BCEA18567E";
	setAttr ".t" -type "double3" -6.235557097784862 -0.45805346895957322 -6.3534996985790908 ;
	setAttr ".s" -type "double3" 2.2476421442863859 0.48241800767873422 6.1713774752973096 ;
createNode transform -n "transform4" -p "pCube3";
	rename -uid "A358A0D5-5E4C-8A56-8CCA-B0A385895815";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform4";
	rename -uid "8923FE6E-5047-B471-0519-D0A3FF73B67C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:383]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1.3125 1.5625 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 490 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0 0 1 0 0 1 1 1 0 2 1 2 0 3
		 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1 0.5 0.5 0.5 0 0.5 4 1 0.5 0.5 1 0 0.5 0.5 1.5 1 1.5
		 1.5 1 0.5 2 -0.5 1 0 1.5 0.5 2.5 1 2.5 2 0.5 0.5 3 -1 0.5 0 2.5 0.5 3.5 1 3.5 1.5
		 0 0.5 4 -0.5 0 0 3.5 1.5 0.5 1.5 0 2 0.5 1.5 1 -0.5 0.5 -0.5 0 -0.5 1 -1 0.5 0.25
		 0.25 0 0.25 0.25 0 0.25 4 0.5 0.25 0.25 0.5 0.25 1.25 -0.25 1 0 1.25 0.25 1 0.5 1.25
		 0.25 1.5 0.25 2.25 -1 0.75 0 2.25 0.25 2 0.5 2.25 0.25 2.5 0.25 3.25 -0.75 0 0 3.25
		 0.25 3 0.5 3.25 0.25 3.5 1.25 0.25 1 0.25 1 3.75 1.25 0 1.5 0.25 1.25 0.5 -0.75 0.25
		 -1 0.25 0 2.75 -0.75 0 -0.5 0.25 -0.75 0.5 0.75 0.25 0.75 0 0.75 4 0.75 0.5 0.75
		 0.75 1 0.75 0.75 1 0.5 0.75 0.25 0.75 0 0.75 0.75 1.25 1 1.25 1.25 1 0.75 1.5 0.75
		 1.75 1 1.75 1.75 1 0.75 2 0.5 1.75 0.25 1.75 -0.75 1 0 1.75 0.75 2.25 1 2.25 2 0.75
		 0.75 2.5 0.75 2.75 1 2.75 2 0.25 0.75 3 0.5 2.75 0.25 2.75 0 2.75 0.75 3.25 1 3.25
		 1.75 0 0.75 3.5 0.75 3.75 1 3.75 0.75 4 0.5 3.75 0.25 3.75 0.25 4 -0.25 0 0 3.75
		 1.75 0.25 1.75 0 2 0.25 1.75 0.5 1.75 0.75 2 0.75 1.75 1 1.5 0.75 1.25 0.75 1.25
		 1 -0.25 0.25 -0.25 0 -0.25 0.5 -0.25 0.75 -0.25 1 -0.5 0.75 -0.75 0.75 -0.75 1 -1
		 0.75 0.125 0.375 0.125 0.5 0 0.375 0.125 0.25 0.25 0.375 0.125 1.375 0.125 1.5 -0.375
		 1 0 1.375 0.125 1.25 0.25 1.375 0.125 2.375 0.125 2.5 -1 0.625 0 2.375 0.125 2.25
		 0.25 2.375 0.125 3.375 0.125 3.5 -0.625 0 0 3.375 0.125 3.25 0.25 3.375 1.125 0.375
		 1.125 0.5 1 0.375 1.125 0.25 1.25 0.375 -0.875 0.375 -0.875 0.5 -1 0.375 0 2.625
		 -0.875 0.25 -0.75 0.375 0.625 0.125 0.5 0.125 0.625 0 0.625 4 0.75 0.125 0.625 0.25
		 0.875 0.625 0.875 0.5 1 0.625 0.875 0.75 0.75 0.625 0.375 0.875 0.5 0.875 0.375 1
		 0.25 0.875 0.375 0.75 0.625 1.125 0.5 1.125 0.625 1 0.75 1.125 0.625 1.25 0.875 1.625
		 0.875 1.5 1 1.625 1.625 1 0.875 1.75 0.75 1.625 0.375 1.875 0.5 1.875 0.375 2 0.25
		 1.875 0.375 1.75 0.625 2.125 0.5 2.125 0.625 2 0.75 2.125 0.625 2.25 0.875 2.625
		 0.875 2.5 1 2.625 2 0.375 0.875 2.75 0.75 2.625 0.375 2.875 0.5 2.875 0.375 3 0.25
		 2.875 0.375 2.75 0.625 3.125 0.5 3.125 0.625 3 0.75 3.125 0.625 3.25 0.875 3.625
		 0.875 3.5 1 3.625 1.375 0 0.875 3.75 0.75 3.625 0.375 3.875 0.5 3.875 0.375 0 0.375
		 4 0.25 3.875 0.375 3.75 1.625 0.125 1.5 0.125 1 3.375 1.625 0 1.75 0.125;
	setAttr ".uvst[0].uvsp[250:489]" 1.625 0.25 1.875 0.625 1.875 0.5 1 2.375 2
		 0.625 1.875 0.75 1.75 0.625 1.375 0.875 1.5 0.875 1 1.375 1.375 1 1.25 0.875 1.375
		 0.75 -0.375 0.125 -0.5 0.125 -0.375 0 0 3.625 -0.25 0.125 -0.375 0.25 -0.125 0.625
		 -0.125 0.5 0 0.625 -0.125 0.75 -0.25 0.625 -0.625 0.875 -0.5 0.875 -0.625 1 0 1.625
		 -0.75 0.875 -0.625 0.75 0.125 0.125 0 0.125 0.125 0 0.125 4 0.25 0.125 0.375 0.125
		 0.375 0 0.375 0.25 0.375 0.375 0.5 0.375 0.375 0.5 0.125 1.125 -0.125 1 0 1.125 0.125
		 1 0.25 1.125 0.375 1.125 0.375 1.25 0.375 1.375 0.5 1.375 0.375 1.5 0.125 2.125 -1
		 0.875 0 2.125 0.125 2 0.25 2.125 0.375 2.125 0.375 2.25 0.375 2.375 0.5 2.375 0.375
		 2.5 0.125 3.125 -0.875 0 0 3.125 0.125 3 0.25 3.125 0.375 3.125 0.375 3.25 0.375
		 3.375 0.5 3.375 0.375 3.5 1.125 0.125 1 0.125 1 3.875 1.125 0 1.25 0.125 1.375 0.125
		 1.375 0 1.375 0.25 1.375 0.375 1.5 0.375 1.375 0.5 -0.875 0.125 -1 0.125 0 2.875
		 -0.875 0 -0.75 0.125 -0.625 0.125 -0.625 0 -0.625 0.25 -0.625 0.375 -0.5 0.375 -0.625
		 0.5 0.875 0.125 0.875 0 0.875 4 0.875 0.25 0.875 0.375 0.75 0.375 0.625 0.375 0.625
		 0.5 0.875 0.875 1 0.875 0.875 1 0.75 0.875 0.625 0.875 0.625 0.75 0.625 0.625 0.5
		 0.625 0.125 0.875 0 0.875 0.125 0.75 0.125 0.625 0.25 0.625 0.375 0.625 0.875 1.125
		 1 1.125 1.125 1 0.875 1.25 0.875 1.375 1 1.375 0.75 1.375 0.625 1.375 0.625 1.5 0.875
		 1.875 1 1.875 1.875 1 0.875 2 0.75 1.875 0.625 1.875 0.625 1.75 0.625 1.625 0.5 1.625
		 0.125 1.875 -0.875 1 0 1.875 0.125 1.75 0.125 1.625 0 1.625 0.25 1.625 0.375 1.625
		 0.875 2.125 1 2.125 2 0.875 0.875 2.25 0.875 2.375 1 2.375 0.75 2.375 0.625 2.375
		 0.625 2.5 0.875 2.875 1 2.875 2 0.125 0.875 3 0.75 2.875 0.625 2.875 0.625 2.75 0.625
		 2.625 0.5 2.625 0.125 2.875 0 2.875 0.125 2.75 0.125 2.625 0 2.625 0.25 2.625 0.375
		 2.625 0.875 3.125 1 3.125 1.875 0 0.875 3.25 0.875 3.375 1 3.375 0.75 3.375 0.625
		 3.375 0.625 3.5 0.875 3.875 1 3.875 0.875 4 0.75 3.875 0.625 3.875 0.625 4 0.625
		 3.75 0.625 3.625 0.5 3.625 0.125 3.875 0.125 4 -0.125 0 0 3.875 0.125 3.75 0.125
		 3.625 0 3.625 0.25 3.625 0.375 3.625 1.875 0.125 1.875 0 2 0.125 1.875 0.25 1.875
		 0.375 2 0.375 1.75 0.375 1.625 0.375 1.625 0.5 1.875 0.875 2 0.875 1.875 1 1.75 0.875
		 1.625 0.875 1.625 1 1.625 0.75 1.625 0.625 1.5 0.625 1.125 0.875 1.125 1 1.125 0.75
		 1.125 0.625 1.25 0.625 1.375 0.625 -0.125 0.125 -0.125 0 -0.125 0.25 -0.125 0.375
		 -0.25 0.375 -0.375 0.375 -0.375 0.5 -0.125 0.875 -0.125 1 -0.25 0.875 -0.375 0.875
		 -0.375 1 -0.375 0.75 -0.375 0.625 -0.5 0.625 -0.875 0.875 -0.875 1 -1 0.875 -0.875
		 0.75 -0.875 0.625 -1 0.625 -0.75 0.625 -0.625 0.625;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 186 ".pt";
	setAttr ".pt[0]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[1]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[2]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[3]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[5]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[7]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[10]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[12]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[14]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[16]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[18]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[22]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[23]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[24]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[25]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[27]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[33]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[35]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[42]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[44]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[46]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[47]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[48]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[49]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[50]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[51]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[53]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[54]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[55]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[56]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[59]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[63]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[65]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[67]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[68]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[70]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[72]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[73]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[74]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[75]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[76]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[77]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[78]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[81]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[82]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[83]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[88]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[89]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[90]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[91]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[92]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[96]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[97]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[98]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[99]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[100]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[101]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[104]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[105]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[114]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[118]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[119]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[120]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[121]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[122]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[123]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[124]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[126]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[127]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[131]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[133]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[134]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[135]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[136]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[141]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[148]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[149]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[150]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[151]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[152]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[156]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[161]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[163]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[164]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[165]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[166]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[167]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[175]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[177]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[179]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[181]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[188]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[189]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[190]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[191]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[192]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[193]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[194]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[195]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[196]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[197]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[199]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[201]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[203]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[204]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[207]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[210]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[214]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[216]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[218]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[219]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[229]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[245]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[246]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[250]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[252]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[254]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[255]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[256]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[257]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[261]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[263]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[265]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[269]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[271]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[272]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[273]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[274]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[275]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[278]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[279]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[280]" -type "float3" -0.10608101 1.9073486e-06 -0.051443078 ;
	setAttr ".pt[286]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[287]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[288]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[289]" -type "float3" 0.14255016 0 -0.038311396 ;
	setAttr ".pt[293]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[294]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[300]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[301]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[304]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[307]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[308]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[309]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[313]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[314]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[315]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[316]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[317]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[320]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[321]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[322]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[323]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[333]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[334]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[335]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[336]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[337]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[338]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[340]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[352]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[353]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[354]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[355]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[356]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[357]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[358]" -type "float3" -0.077783279 0 -0.092252329 ;
	setAttr ".pt[359]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[360]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[361]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[362]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[363]" -type "float3" -0.13817732 0 0 ;
	setAttr ".pt[364]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[365]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[366]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[367]" -type "float3" -0.09371458 0 0 ;
	setAttr ".pt[373]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[374]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[377]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[378]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[379]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[380]" -type "float3" 0.12238468 0 0 ;
	setAttr ".pt[381]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[382]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[383]" -type "float3" -0.15769573 0 0 ;
	setAttr ".pt[384]" -type "float3" -0.15769573 0 0 ;
	setAttr -s 386 ".vt";
	setAttr ".vt[0:165]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 0 0 0.5 0 -0.5 0.5 0.5 0 0.5
		 0 0.5 0.5 -0.5 0 0.5 0 0.5 0 0.5 0.5 0 0 0.5 -0.5 -0.5 0.5 0 0 0 -0.5 0.5 0 -0.5
		 0 -0.5 -0.5 -0.5 0 -0.5 0 -0.5 0 0.5 -0.5 0 -0.5 -0.5 0 0.5 0 0 -0.5 0 0 -0.25 -0.25 0.5
		 -0.5 -0.25 0.5 -0.25 -0.5 0.5 0 -0.25 0.5 -0.25 0 0.5 -0.25 0.5 0.25 -0.5 0.5 0.25
		 -0.25 0.5 0.5 0 0.5 0.25 -0.25 0.5 0 -0.25 0.25 -0.5 -0.5 0.25 -0.5 -0.25 0.5 -0.5
		 0 0.25 -0.5 -0.25 0 -0.5 -0.25 -0.5 -0.25 -0.5 -0.5 -0.25 -0.25 -0.5 -0.5 0 -0.5 -0.25
		 -0.25 -0.5 0 0.5 -0.25 0.25 0.5 -0.25 0.5 0.5 -0.5 0.25 0.5 -0.25 0 0.5 0 0.25 -0.5 -0.25 -0.25
		 -0.5 -0.25 -0.5 -0.5 -0.25 0 -0.5 0 -0.25 0.25 -0.25 0.5 0.25 -0.5 0.5 0.25 0 0.5
		 0.25 0.25 0.5 0.5 0.25 0.5 0.25 0.5 0.5 0 0.25 0.5 -0.25 0.25 0.5 -0.5 0.25 0.5 0.25 0.5 0.25
		 0.5 0.5 0.25 0.25 0.5 0 0.25 0.5 -0.25 0.5 0.5 -0.25 0.25 0.5 -0.5 0 0.5 -0.25 -0.25 0.5 -0.25
		 -0.5 0.5 -0.25 0.25 0.25 -0.5 0.5 0.25 -0.5 0.25 0 -0.5 0.25 -0.25 -0.5 0.5 -0.25 -0.5
		 0.25 -0.5 -0.5 0 -0.25 -0.5 -0.25 -0.25 -0.5 0.25 -0.5 -0.25 0.5 -0.5 -0.25 0.25 -0.5 0
		 0.25 -0.5 0.25 0 -0.5 0.25 -0.25 -0.5 0.25 -0.5 -0.5 0.25 0.5 -0.25 -0.25 0.5 0 -0.25
		 0.5 0.25 -0.25 0.5 0.25 0 0.5 0.25 0.25 -0.5 -0.25 0.25 -0.5 0 0.25 -0.5 0.25 0.25
		 -0.5 0.25 0 -0.5 0.25 -0.25 -0.375 -0.125 0.5 -0.375 0 0.5 -0.5 -0.125 0.5 -0.375 -0.25 0.5
		 -0.25 -0.125 0.5 -0.375 0.5 0.125 -0.375 0.5 0 -0.5 0.5 0.125 -0.375 0.5 0.25 -0.25 0.5 0.125
		 -0.375 0.125 -0.5 -0.375 0 -0.5 -0.5 0.125 -0.5 -0.375 0.25 -0.5 -0.25 0.125 -0.5
		 -0.375 -0.5 -0.125 -0.375 -0.5 0 -0.5 -0.5 -0.125 -0.375 -0.5 -0.25 -0.25 -0.5 -0.125
		 0.5 -0.125 0.375 0.5 0 0.375 0.5 -0.125 0.5 0.5 -0.25 0.375 0.5 -0.125 0.25 -0.5 -0.125 -0.375
		 -0.5 0 -0.375 -0.5 -0.125 -0.5 -0.5 -0.25 -0.375 -0.5 -0.125 -0.25 0.125 -0.375 0.5
		 0 -0.375 0.5 0.125 -0.5 0.5 0.25 -0.375 0.5 0.125 -0.25 0.5 0.375 0.125 0.5 0.375 0 0.5
		 0.5 0.125 0.5 0.375 0.25 0.5 0.25 0.125 0.5 -0.125 0.375 0.5 0 0.375 0.5 -0.125 0.5 0.5
		 -0.25 0.375 0.5 -0.125 0.25 0.5 0.125 0.5 0.375 0 0.5 0.375 0.125 0.5 0.5 0.25 0.5 0.375
		 0.125 0.5 0.25 0.375 0.5 -0.125 0.375 0.5 0 0.5 0.5 -0.125 0.375 0.5 -0.25 0.25 0.5 -0.125
		 -0.125 0.5 -0.375 0 0.5 -0.375 -0.125 0.5 -0.5 -0.25 0.5 -0.375 -0.125 0.5 -0.25
		 0.125 0.375 -0.5 0 0.375 -0.5 0.125 0.5 -0.5 0.25 0.375 -0.5 0.125 0.25 -0.5 0.375 -0.125 -0.5
		 0.375 0 -0.5 0.5 -0.125 -0.5;
	setAttr ".vt[166:331]" 0.375 -0.25 -0.5 0.25 -0.125 -0.5 -0.125 -0.375 -0.5
		 0 -0.375 -0.5 -0.125 -0.5 -0.5 -0.25 -0.375 -0.5 -0.125 -0.25 -0.5 0.125 -0.5 -0.375
		 0 -0.5 -0.375 0.125 -0.5 -0.5 0.25 -0.5 -0.375 0.125 -0.5 -0.25 0.375 -0.5 0.125
		 0.375 -0.5 0 0.5 -0.5 0.125 0.375 -0.5 0.25 0.25 -0.5 0.125 -0.125 -0.5 0.375 0 -0.5 0.375
		 -0.125 -0.5 0.5 -0.25 -0.5 0.375 -0.125 -0.5 0.25 0.5 -0.375 -0.125 0.5 -0.375 0
		 0.5 -0.5 -0.125 0.5 -0.375 -0.25 0.5 -0.25 -0.125 0.5 0.125 -0.375 0.5 0 -0.375 0.5 0.125 -0.5
		 0.5 0.25 -0.375 0.5 0.125 -0.25 0.5 0.375 0.125 0.5 0.375 0 0.5 0.5 0.125 0.5 0.375 0.25
		 0.5 0.25 0.125 -0.5 -0.375 0.125 -0.5 -0.375 0 -0.5 -0.5 0.125 -0.5 -0.375 0.25 -0.5 -0.25 0.125
		 -0.5 0.125 0.375 -0.5 0 0.375 -0.5 0.125 0.5 -0.5 0.25 0.375 -0.5 0.125 0.25 -0.5 0.375 -0.125
		 -0.5 0.375 0 -0.5 0.5 -0.125 -0.5 0.375 -0.25 -0.5 0.25 -0.125 -0.375 -0.375 0.5
		 -0.5 -0.375 0.5 -0.375 -0.5 0.5 -0.25 -0.375 0.5 -0.125 -0.375 0.5 -0.125 -0.25 0.5
		 -0.125 -0.125 0.5 0 -0.125 0.5 -0.125 0 0.5 -0.375 0.5 0.375 -0.5 0.5 0.375 -0.375 0.5 0.5
		 -0.25 0.5 0.375 -0.125 0.5 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 0 0.5 0.125 -0.125 0.5 0
		 -0.375 0.375 -0.5 -0.5 0.375 -0.5 -0.375 0.5 -0.5 -0.25 0.375 -0.5 -0.125 0.375 -0.5
		 -0.125 0.25 -0.5 -0.125 0.125 -0.5 0 0.125 -0.5 -0.125 0 -0.5 -0.375 -0.5 -0.375
		 -0.5 -0.5 -0.375 -0.375 -0.5 -0.5 -0.25 -0.5 -0.375 -0.125 -0.5 -0.375 -0.125 -0.5 -0.25
		 -0.125 -0.5 -0.125 0 -0.5 -0.125 -0.125 -0.5 0 0.5 -0.375 0.375 0.5 -0.375 0.5 0.5 -0.5 0.375
		 0.5 -0.375 0.25 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5 -0.125 0.125 0.5 -0.125 0 0.5 0 0.125
		 -0.5 -0.375 -0.375 -0.5 -0.375 -0.5 -0.5 -0.375 -0.25 -0.5 -0.375 -0.125 -0.5 -0.25 -0.125
		 -0.5 -0.125 -0.125 -0.5 -0.125 0 -0.5 0 -0.125 0.375 -0.375 0.5 0.375 -0.5 0.5 0.375 -0.25 0.5
		 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 -0.125 0.5 0.125 0 0.5 0.375 0.375 0.5 0.5 0.375 0.5
		 0.375 0.5 0.5 0.25 0.375 0.5 0.125 0.375 0.5 0.125 0.25 0.5 0.125 0.125 0.5 0 0.125 0.5
		 -0.375 0.375 0.5 -0.5 0.375 0.5 -0.375 0.25 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5
		 -0.125 0.125 0.5 0.375 0.5 0.375 0.5 0.5 0.375 0.375 0.5 0.25 0.375 0.5 0.125 0.25 0.5 0.125
		 0.125 0.5 0.125 0.125 0.5 0 0.375 0.5 -0.375 0.5 0.5 -0.375 0.375 0.5 -0.5 0.25 0.5 -0.375
		 0.125 0.5 -0.375 0.125 0.5 -0.25 0.125 0.5 -0.125 0 0.5 -0.125 -0.375 0.5 -0.375
		 -0.5 0.5 -0.375 -0.375 0.5 -0.25 -0.375 0.5 -0.125 -0.25 0.5 -0.125 -0.125 0.5 -0.125
		 0.375 0.375 -0.5 0.5 0.375 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5
		 0.125 0.125 -0.5 0.125 0 -0.5 0.375 -0.375 -0.5 0.5 -0.375 -0.5 0.375 -0.5 -0.5 0.25 -0.375 -0.5
		 0.125 -0.375 -0.5 0.125 -0.25 -0.5 0.125 -0.125 -0.5 0 -0.125 -0.5 -0.375 -0.375 -0.5
		 -0.375 -0.25 -0.5 -0.375 -0.125 -0.5 -0.25 -0.125 -0.5;
	setAttr ".vt[332:385]" -0.125 -0.125 -0.5 0.375 -0.5 -0.375 0.5 -0.5 -0.375
		 0.375 -0.5 -0.25 0.375 -0.5 -0.125 0.25 -0.5 -0.125 0.125 -0.5 -0.125 0.125 -0.5 0
		 0.375 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5 0.125
		 0 -0.5 0.125 -0.375 -0.5 0.375 -0.5 -0.5 0.375 -0.375 -0.5 0.25 -0.375 -0.5 0.125
		 -0.25 -0.5 0.125 -0.125 -0.5 0.125 0.5 -0.375 -0.375 0.5 -0.25 -0.375 0.5 -0.125 -0.375
		 0.5 -0.125 -0.25 0.5 -0.125 -0.125 0.5 0 -0.125 0.5 0.375 -0.375 0.5 0.375 -0.25
		 0.5 0.375 -0.125 0.5 0.25 -0.125 0.5 0.125 -0.125 0.5 0.125 0 0.5 0.375 0.375 0.5 0.25 0.375
		 0.5 0.125 0.375 0.5 0.125 0.25 0.5 0.125 0.125 -0.5 -0.375 0.375 -0.5 -0.25 0.375
		 -0.5 -0.125 0.375 -0.5 -0.125 0.25 -0.5 -0.125 0.125 -0.5 0 0.125 -0.5 0.375 0.375
		 -0.5 0.375 0.25 -0.5 0.375 0.125 -0.5 0.25 0.125 -0.5 0.125 0.125 -0.5 0.125 0 -0.5 0.375 -0.375
		 -0.5 0.25 -0.375 -0.5 0.125 -0.375 -0.5 0.125 -0.25 -0.5 0.125 -0.125;
	setAttr -s 768 ".ed";
	setAttr ".ed[0:165]"  0 220 0 2 229 0 4 238 0 6 247 0 0 219 0 1 255 0 2 228 0
		 3 293 0 4 237 0 5 314 0 6 246 0 7 334 0 9 130 0 10 135 0 11 145 0 12 210 0 9 129 0
		 10 134 0 11 139 0 12 99 0 14 150 0 15 160 0 16 215 0 11 144 0 14 149 0 15 154 0 16 104 0
		 18 165 0 19 175 0 20 125 0 15 159 0 18 164 0 19 169 0 20 109 0 22 180 0 23 205 0
		 19 174 0 22 179 0 9 184 0 23 114 0 22 189 0 18 194 0 14 199 0 10 119 0 23 204 0 12 209 0
		 16 214 0 20 124 0 27 100 0 28 185 0 29 225 0 30 226 0 27 101 0 28 221 0 29 223 0
		 30 102 0 32 105 0 33 140 0 34 234 0 35 235 0 32 106 0 33 230 0 34 232 0 35 107 0
		 37 110 0 38 155 0 39 243 0 40 244 0 37 111 0 38 239 0 39 241 0 40 112 0 42 115 0
		 43 170 0 44 252 0 45 253 0 42 116 0 43 248 0 44 250 0 45 117 0 47 120 0 48 256 0
		 49 261 0 50 262 0 47 121 0 48 257 0 49 259 0 50 122 0 52 264 0 53 269 0 54 270 0
		 52 126 0 42 265 0 53 267 0 54 127 0 56 272 0 57 277 0 56 131 0 47 273 0 57 275 0
		 29 132 0 59 279 0 60 280 0 61 285 0 59 136 0 60 281 0 61 283 0 57 137 0 63 287 0
		 33 141 0 63 288 0 30 290 0 61 142 0 65 200 0 66 298 0 60 146 0 65 294 0 66 296 0
		 34 147 0 68 300 0 69 301 0 70 306 0 68 151 0 69 302 0 70 304 0 66 152 0 72 308 0
		 38 156 0 72 309 0 35 311 0 70 157 0 74 195 0 75 319 0 69 161 0 74 315 0 75 317 0
		 39 162 0 77 321 0 78 322 0 79 327 0 77 166 0 78 323 0 79 325 0 75 167 0 43 171 0
		 52 329 0 40 331 0 79 172 0 82 190 0 83 339 0 78 176 0 82 335 0 83 337 0 44 177 0
		 85 345 0 48 181 0 56 341 0 85 343 0 83 182 0 87 347 0 28 186 0 87 348 0 45 350 0
		 85 187 0 89 357 0 82 191 0;
	setAttr ".ed[166:331]" 77 353 0 89 355 0 49 192 0 91 363 0 74 196 0 68 359 0
		 91 361 0 89 197 0 65 201 0 59 365 0 50 367 0 91 202 0 94 374 0 87 206 0 27 370 0
		 94 372 0 53 207 0 96 380 0 63 211 0 32 376 0 96 378 0 94 212 0 72 216 0 37 382 0
		 54 384 0 96 217 0 99 30 0 100 12 0 101 26 0 102 26 0 99 98 0 100 98 0 101 98 0 102 98 0
		 104 35 0 105 16 0 106 31 0 107 31 0 104 103 0 105 103 0 106 103 0 107 103 0 109 40 0
		 110 20 0 111 36 0 112 36 0 109 108 0 110 108 0 111 108 0 112 108 0 114 45 0 115 23 0
		 116 41 0 117 41 0 114 113 0 115 113 0 116 113 0 117 113 0 119 50 0 120 10 0 121 46 0
		 122 46 0 119 118 0 120 118 0 121 118 0 122 118 0 124 54 0 125 52 0 126 51 0 127 51 0
		 124 123 0 125 123 0 126 123 0 127 123 0 129 29 0 130 56 0 131 55 0 132 55 0 129 128 0
		 130 128 0 131 128 0 132 128 0 134 57 0 135 59 0 136 58 0 137 58 0 134 133 0 135 133 0
		 136 133 0 137 133 0 139 61 0 140 11 0 141 62 0 142 62 0 139 138 0 140 138 0 141 138 0
		 142 138 0 144 34 0 145 60 0 146 64 0 147 64 0 144 143 0 145 143 0 146 143 0 147 143 0
		 149 66 0 150 68 0 151 67 0 152 67 0 149 148 0 150 148 0 151 148 0 152 148 0 154 70 0
		 155 15 0 156 71 0 157 71 0 154 153 0 155 153 0 156 153 0 157 153 0 159 39 0 160 69 0
		 161 73 0 162 73 0 159 158 0 160 158 0 161 158 0 162 158 0 164 75 0 165 77 0 166 76 0
		 167 76 0 164 163 0 165 163 0 166 163 0 167 163 0 169 79 0 170 19 0 171 80 0 172 80 0
		 169 168 0 170 168 0 171 168 0 172 168 0 174 44 0 175 78 0 176 81 0 177 81 0 174 173 0
		 175 173 0 176 173 0 177 173 0 179 83 0 180 48 0 181 84 0 182 84 0 179 178 0 180 178 0
		 181 178 0 182 178 0 184 85 0 185 9 0 186 86 0 187 86 0;
	setAttr ".ed[332:497]" 184 183 0 185 183 0 186 183 0 187 183 0 189 49 0 190 22 0
		 191 88 0 192 88 0 189 188 0 190 188 0 191 188 0 192 188 0 194 89 0 195 18 0 196 90 0
		 197 90 0 194 193 0 195 193 0 196 193 0 197 193 0 199 91 0 200 14 0 201 92 0 202 92 0
		 199 198 0 200 198 0 201 198 0 202 198 0 204 53 0 205 87 0 206 93 0 207 93 0 204 203 0
		 205 203 0 206 203 0 207 203 0 209 94 0 210 63 0 211 95 0 212 95 0 209 208 0 210 208 0
		 211 208 0 212 208 0 214 96 0 215 72 0 216 97 0 217 97 0 214 213 0 215 213 0 216 213 0
		 217 213 0 219 27 0 220 28 0 221 26 0 219 218 0 220 218 0 221 218 0 101 218 0 223 26 0
		 185 222 0 129 222 0 223 222 0 221 222 0 225 8 0 226 8 0 225 224 0 226 224 0 102 224 0
		 223 224 0 228 32 0 229 33 0 230 31 0 228 227 0 229 227 0 230 227 0 106 227 0 232 31 0
		 140 231 0 144 231 0 232 231 0 230 231 0 234 13 0 235 13 0 234 233 0 235 233 0 107 233 0
		 232 233 0 237 37 0 238 38 0 239 36 0 237 236 0 238 236 0 239 236 0 111 236 0 241 36 0
		 155 240 0 159 240 0 241 240 0 239 240 0 243 17 0 244 17 0 243 242 0 244 242 0 112 242 0
		 241 242 0 246 42 0 247 43 0 248 41 0 246 245 0 247 245 0 248 245 0 116 245 0 250 41 0
		 170 249 0 174 249 0 250 249 0 248 249 0 252 21 0 253 21 0 252 251 0 253 251 0 117 251 0
		 250 251 0 255 47 0 256 1 0 257 46 0 255 254 0 256 254 0 257 254 0 121 254 0 259 46 0
		 180 258 0 189 258 0 259 258 0 257 258 0 261 24 0 262 24 0 261 260 0 262 260 0 122 260 0
		 259 260 0 264 6 0 265 51 0 264 263 0 246 263 0 265 263 0 126 263 0 267 51 0 115 266 0
		 204 266 0 267 266 0 265 266 0 269 25 0 270 25 0 269 268 0 270 268 0 127 268 0 267 268 0
		 272 1 0 273 55 0 272 271 0 255 271 0 273 271 0 131 271 0 275 55 0;
	setAttr ".ed[498:663]" 120 274 0 134 274 0 275 274 0 273 274 0 277 8 0 277 276 0
		 225 276 0 132 276 0 275 276 0 279 3 0 280 3 0 281 58 0 279 278 0 280 278 0 281 278 0
		 136 278 0 283 58 0 145 282 0 139 282 0 283 282 0 281 282 0 285 8 0 285 284 0 277 284 0
		 137 284 0 283 284 0 287 2 0 288 62 0 229 286 0 287 286 0 288 286 0 141 286 0 290 62 0
		 210 289 0 99 289 0 290 289 0 288 289 0 226 291 0 285 291 0 142 291 0 290 291 0 293 65 0
		 294 64 0 280 292 0 293 292 0 294 292 0 146 292 0 296 64 0 200 295 0 149 295 0 296 295 0
		 294 295 0 298 13 0 298 297 0 234 297 0 147 297 0 296 297 0 300 5 0 301 5 0 302 67 0
		 300 299 0 301 299 0 302 299 0 151 299 0 304 67 0 160 303 0 154 303 0 304 303 0 302 303 0
		 306 13 0 306 305 0 298 305 0 152 305 0 304 305 0 308 4 0 309 71 0 238 307 0 308 307 0
		 309 307 0 156 307 0 311 71 0 215 310 0 104 310 0 311 310 0 309 310 0 235 312 0 306 312 0
		 157 312 0 311 312 0 314 74 0 315 73 0 301 313 0 314 313 0 315 313 0 161 313 0 317 73 0
		 195 316 0 164 316 0 317 316 0 315 316 0 319 17 0 319 318 0 243 318 0 162 318 0 317 318 0
		 321 7 0 322 7 0 323 76 0 321 320 0 322 320 0 323 320 0 166 320 0 325 76 0 175 324 0
		 169 324 0 325 324 0 323 324 0 327 17 0 327 326 0 319 326 0 167 326 0 325 326 0 329 80 0
		 247 328 0 264 328 0 329 328 0 171 328 0 331 80 0 125 330 0 109 330 0 331 330 0 329 330 0
		 244 332 0 327 332 0 172 332 0 331 332 0 334 82 0 335 81 0 322 333 0 334 333 0 335 333 0
		 176 333 0 337 81 0 190 336 0 179 336 0 337 336 0 335 336 0 339 21 0 339 338 0 252 338 0
		 177 338 0 337 338 0 341 84 0 256 340 0 272 340 0 341 340 0 181 340 0 343 84 0 130 342 0
		 184 342 0 343 342 0 341 342 0 345 21 0 345 344 0 339 344 0 182 344 0;
	setAttr ".ed[664:767]" 343 344 0 347 0 0 348 86 0 220 346 0 347 346 0 348 346 0
		 186 346 0 350 86 0 205 349 0 114 349 0 350 349 0 348 349 0 253 351 0 345 351 0 187 351 0
		 350 351 0 353 88 0 334 352 0 321 352 0 353 352 0 191 352 0 355 88 0 165 354 0 194 354 0
		 355 354 0 353 354 0 357 24 0 357 356 0 261 356 0 192 356 0 355 356 0 359 90 0 314 358 0
		 300 358 0 359 358 0 196 358 0 361 90 0 150 360 0 199 360 0 361 360 0 359 360 0 363 24 0
		 363 362 0 357 362 0 197 362 0 361 362 0 365 92 0 293 364 0 279 364 0 365 364 0 201 364 0
		 367 92 0 135 366 0 119 366 0 367 366 0 365 366 0 262 368 0 363 368 0 202 368 0 367 368 0
		 370 93 0 347 369 0 219 369 0 370 369 0 206 369 0 372 93 0 100 371 0 209 371 0 372 371 0
		 370 371 0 374 25 0 374 373 0 269 373 0 207 373 0 372 373 0 376 95 0 287 375 0 228 375 0
		 376 375 0 211 375 0 378 95 0 105 377 0 214 377 0 378 377 0 376 377 0 380 25 0 380 379 0
		 374 379 0 212 379 0 378 379 0 382 97 0 308 381 0 237 381 0 382 381 0 216 381 0 384 97 0
		 110 383 0 124 383 0 384 383 0 382 383 0 270 385 0 380 385 0 217 385 0 384 385 0;
	setAttr -s 384 -ch 1536 ".fc[0:383]" -type "polyFaces" 
		f 4 -56 -193 196 -200
		mu 0 4 150 51 147 146
		f 4 -64 -201 204 -208
		mu 0 4 156 57 152 151
		f 4 -72 -209 212 -216
		mu 0 4 162 63 158 157
		f 4 -80 -217 220 -224
		mu 0 4 168 69 164 163
		f 4 -88 -225 228 -232
		mu 0 4 173 75 170 169
		f 4 -95 -233 236 -240
		mu 0 4 179 81 175 174
		f 4 -101 -241 244 -248
		mu 0 4 185 50 181 180
		f 4 -108 -249 252 -256
		mu 0 4 190 85 187 186
		f 4 -113 -257 260 -264
		mu 0 4 195 89 192 191
		f 4 -119 -265 268 -272
		mu 0 4 200 56 197 196
		f 4 -126 -273 276 -280
		mu 0 4 206 95 202 201
		f 4 -131 -281 284 -288
		mu 0 4 211 100 208 207
		f 4 -137 -289 292 -296
		mu 0 4 216 62 213 212
		f 4 -144 -297 300 -304
		mu 0 4 222 107 218 217
		f 4 -148 -305 308 -312
		mu 0 4 227 112 224 223
		f 4 -154 -313 316 -320
		mu 0 4 232 68 229 228
		f 4 -159 -321 324 -328
		mu 0 4 238 118 234 233
		f 4 -164 -329 332 -336
		mu 0 4 244 122 240 239
		f 4 -169 -337 340 -344
		mu 0 4 250 74 246 245
		f 4 -174 -345 348 -352
		mu 0 4 256 130 252 251
		f 4 -178 -353 356 -360
		mu 0 4 262 134 258 257
		f 4 -183 -361 364 -368
		mu 0 4 268 80 264 263
		f 4 -188 -369 372 -376
		mu 0 4 273 139 270 269
		f 4 -192 -377 380 -384
		mu 0 4 279 142 275 274
		f 4 -53 -385 387 -391
		mu 0 4 149 47 281 280
		f 4 -54 49 392 -396
		mu 0 4 284 48 286 285
		f 4 -55 50 398 -402
		mu 0 4 287 50 289 288
		f 4 -61 -403 405 -409
		mu 0 4 155 54 293 291
		f 4 -62 57 410 -414
		mu 0 4 295 55 193 296
		f 4 -63 58 416 -420
		mu 0 4 297 56 299 298
		f 4 -69 -421 423 -427
		mu 0 4 161 60 303 301
		f 4 -70 65 428 -432
		mu 0 4 305 61 209 306
		f 4 -71 66 434 -438
		mu 0 4 307 62 309 308
		f 4 -77 -439 441 -445
		mu 0 4 167 66 313 311
		f 4 -78 73 446 -450
		mu 0 4 315 67 225 316
		f 4 -79 74 452 -456
		mu 0 4 317 68 319 318
		f 4 -85 -457 459 -463
		mu 0 4 172 71 322 321
		f 4 -86 -322 464 -468
		mu 0 4 325 73 327 326
		f 4 -87 82 470 -474
		mu 0 4 328 74 330 329
		f 4 -92 88 476 -480
		mu 0 4 178 77 333 332
		f 4 -93 72 481 -485
		mu 0 4 336 79 338 337
		f 4 -94 89 487 -491
		mu 0 4 339 80 341 340
		f 4 -98 95 493 -497
		mu 0 4 184 83 344 343
		f 4 -99 80 498 -502
		mu 0 4 346 71 171 347
		f 4 -100 96 503 -507
		mu 0 4 348 85 350 349
		f 4 -105 101 510 -514
		mu 0 4 189 87 352 351
		f 4 -106 -266 515 -519
		mu 0 4 354 88 198 355
		f 4 -107 103 520 -524
		mu 0 4 356 89 358 357
		f 4 -110 -404 526 -530
		mu 0 4 194 55 294 359
		f 4 -111 -370 531 -535
		mu 0 4 361 91 271 362
		f 4 -112 51 535 -539
		mu 0 4 363 51 290 364
		f 4 -116 102 541 -545
		mu 0 4 199 88 353 365
		f 4 -117 113 546 -550
		mu 0 4 368 93 370 369
		f 4 -118 114 551 -555
		mu 0 4 371 95 373 372
		f 4 -123 119 558 -562
		mu 0 4 205 97 375 374
		f 4 -124 -290 563 -567
		mu 0 4 378 99 214 379
		f 4 -125 121 568 -572
		mu 0 4 380 100 382 381
		f 4 -128 -422 574 -578
		mu 0 4 210 61 304 383
		f 4 -129 -378 579 -583
		mu 0 4 386 103 388 387
		f 4 -130 59 583 -587
		mu 0 4 389 57 300 390
		f 4 -134 120 589 -593
		mu 0 4 215 99 377 391
		f 4 -135 131 594 -598
		mu 0 4 394 105 396 395
		f 4 -136 132 599 -603
		mu 0 4 397 107 399 398
		f 4 -141 137 606 -610
		mu 0 4 221 109 401 400
		f 4 -142 -314 611 -615
		mu 0 4 404 111 230 405
		f 4 -143 139 616 -620
		mu 0 4 406 112 408 407
		f 4 -145 -440 621 -625
		mu 0 4 226 67 314 409
		f 4 -146 -234 626 -630
		mu 0 4 411 114 413 412
		f 4 -147 67 630 -634
		mu 0 4 414 63 310 415
		f 4 -151 138 636 -640
		mu 0 4 231 111 403 416
		f 4 -152 148 641 -645
		mu 0 4 419 116 421 420
		f 4 -153 149 646 -650
		mu 0 4 422 118 424 423
		f 4 -156 81 651 -655
		mu 0 4 237 120 426 425
		f 4 -157 -242 656 -660
		mu 0 4 428 121 430 429
		f 4 -158 154 661 -665
		mu 0 4 431 122 433 432
		f 4 -161 -386 667 -671
		mu 0 4 243 124 435 434
		f 4 -162 -362 672 -676
		mu 0 4 438 126 440 439
		f 4 -163 75 676 -680
		mu 0 4 441 69 320 442
		f 4 -166 -635 681 -685
		mu 0 4 249 128 444 443
		f 4 -167 -298 686 -690
		mu 0 4 446 129 448 447
		f 4 -168 164 691 -695
		mu 0 4 449 130 451 450
		f 4 -171 -588 696 -700
		mu 0 4 255 132 453 452
		f 4 -172 -274 701 -705
		mu 0 4 455 133 457 456
		f 4 -173 169 706 -710
		mu 0 4 458 134 460 459
		f 4 -175 -540 711 -715
		mu 0 4 261 136 462 461
		f 4 -176 -250 716 -720
		mu 0 4 463 87 188 464
		f 4 -177 83 720 -724
		mu 0 4 465 75 331 466
		f 4 -180 159 725 -729
		mu 0 4 267 138 468 467
		f 4 -181 48 730 -734
		mu 0 4 469 47 148 470
		f 4 -182 178 735 -739
		mu 0 4 471 139 473 472
		f 4 -185 108 740 -744
		mu 0 4 272 91 360 474
		f 4 -186 56 745 -749
		mu 0 4 476 141 478 477
		f 4 -187 183 750 -754
		mu 0 4 479 142 481 480
		f 4 -189 126 755 -759
		mu 0 4 278 144 483 482
		f 4 -190 64 760 -764
		mu 0 4 485 145 487 486
		f 4 -191 90 764 -768
		mu 0 4 488 81 342 489
		f 4 -20 -194 197 -197
		mu 0 4 147 19 148 146
		f 4 -49 52 198 -198
		mu 0 4 148 47 149 146
		f 4 194 -196 199 -199
		mu 0 4 149 46 150 146
		f 4 -27 -202 205 -205
		mu 0 4 152 25 154 151
		f 4 -57 60 206 -206
		mu 0 4 154 54 155 151
		f 4 202 -204 207 -207
		mu 0 4 155 52 156 151
		f 4 -34 -210 213 -213
		mu 0 4 158 31 160 157
		f 4 -65 68 214 -214
		mu 0 4 160 60 161 157
		f 4 210 -212 215 -215
		mu 0 4 161 58 162 157
		f 4 -40 -218 221 -221
		mu 0 4 164 37 166 163
		f 4 -73 76 222 -222
		mu 0 4 166 66 167 163
		f 4 218 -220 223 -223
		mu 0 4 167 64 168 163
		f 4 -44 -226 229 -229
		mu 0 4 170 17 171 169
		f 4 -81 84 230 -230
		mu 0 4 171 71 172 169
		f 4 226 -228 231 -231
		mu 0 4 172 70 173 169
		f 4 -48 29 237 -237
		mu 0 4 175 45 176 174
		f 4 233 91 238 -238
		mu 0 4 176 77 178 174
		f 4 234 -236 239 -239
		mu 0 4 178 76 179 174
		f 4 -17 12 245 -245
		mu 0 4 181 15 182 180
		f 4 241 97 246 -246
		mu 0 4 182 83 184 180
		f 4 242 -244 247 -247
		mu 0 4 184 82 185 180
		f 4 -18 13 253 -253
		mu 0 4 187 17 188 186
		f 4 249 104 254 -254
		mu 0 4 188 87 189 186
		f 4 250 -252 255 -255
		mu 0 4 189 86 190 186
		f 4 -19 -258 261 -261
		mu 0 4 192 18 193 191
		f 4 -58 109 262 -262
		mu 0 4 193 55 194 191
		f 4 258 -260 263 -263
		mu 0 4 194 90 195 191
		f 4 -24 14 269 -269
		mu 0 4 197 18 198 196
		f 4 265 115 270 -270
		mu 0 4 198 88 199 196
		f 4 266 -268 271 -271
		mu 0 4 199 92 200 196
		f 4 -25 20 277 -277
		mu 0 4 202 21 203 201
		f 4 273 122 278 -278
		mu 0 4 203 97 205 201
		f 4 274 -276 279 -279
		mu 0 4 205 96 206 201
		f 4 -26 -282 285 -285
		mu 0 4 208 23 209 207
		f 4 -66 127 286 -286
		mu 0 4 209 61 210 207
		f 4 282 -284 287 -287
		mu 0 4 210 101 211 207
		f 4 -31 21 293 -293
		mu 0 4 213 23 214 212
		f 4 289 133 294 -294
		mu 0 4 214 99 215 212
		f 4 290 -292 295 -295
		mu 0 4 215 104 216 212
		f 4 -32 27 301 -301
		mu 0 4 218 27 219 217
		f 4 297 140 302 -302
		mu 0 4 219 109 221 217
		f 4 298 -300 303 -303
		mu 0 4 221 108 222 217
		f 4 -33 -306 309 -309
		mu 0 4 224 29 225 223
		f 4 -74 144 310 -310
		mu 0 4 225 67 226 223
		f 4 306 -308 311 -311
		mu 0 4 226 113 227 223
		f 4 -37 28 317 -317
		mu 0 4 229 29 230 228
		f 4 313 150 318 -318
		mu 0 4 230 111 231 228
		f 4 314 -316 319 -319
		mu 0 4 231 115 232 228
		f 4 -38 34 325 -325
		mu 0 4 234 33 235 233
		f 4 321 155 326 -326
		mu 0 4 235 120 237 233
		f 4 322 -324 327 -327
		mu 0 4 237 119 238 233
		f 4 -39 -330 333 -333
		mu 0 4 240 35 242 239
		f 4 -50 160 334 -334
		mu 0 4 242 124 243 239
		f 4 330 -332 335 -335
		mu 0 4 243 123 244 239
		f 4 -41 -338 341 -341
		mu 0 4 246 39 248 245
		f 4 -149 165 342 -342
		mu 0 4 248 128 249 245
		f 4 338 -340 343 -343
		mu 0 4 249 127 250 245
		f 4 -42 -346 349 -349
		mu 0 4 252 40 254 251
		f 4 -132 170 350 -350
		mu 0 4 254 132 255 251
		f 4 346 -348 351 -351
		mu 0 4 255 131 256 251
		f 4 -43 -354 357 -357
		mu 0 4 258 41 260 257
		f 4 -114 174 358 -358
		mu 0 4 260 136 261 257
		f 4 354 -356 359 -359
		mu 0 4 261 135 262 257
		f 4 -45 35 365 -365
		mu 0 4 264 43 265 263
		f 4 361 179 366 -366
		mu 0 4 265 138 267 263
		f 4 362 -364 367 -367
		mu 0 4 267 137 268 263
		f 4 -46 15 373 -373
		mu 0 4 270 19 271 269
		f 4 369 184 374 -374
		mu 0 4 271 91 272 269
		f 4 370 -372 375 -375
		mu 0 4 272 140 273 269
		f 4 -47 22 381 -381
		mu 0 4 275 44 276 274
		f 4 377 188 382 -382
		mu 0 4 276 144 278 274
		f 4 378 -380 383 -383
		mu 0 4 278 143 279 274
		f 4 -5 0 388 -388
		mu 0 4 281 0 282 280
		f 4 385 53 389 -389
		mu 0 4 282 48 284 280
		f 4 386 -195 390 -390
		mu 0 4 284 46 149 280
		f 4 329 16 393 -393
		mu 0 4 286 15 181 285
		f 4 240 54 394 -394
		mu 0 4 181 50 287 285
		f 4 391 -387 395 -395
		mu 0 4 287 46 284 285
		f 4 396 -398 399 -399
		mu 0 4 289 14 290 288
		f 4 -52 55 400 -400
		mu 0 4 290 51 150 288
		f 4 195 -392 401 -401
		mu 0 4 150 46 287 288
		f 4 -7 1 406 -406
		mu 0 4 293 2 294 291
		f 4 403 61 407 -407
		mu 0 4 294 55 295 291
		f 4 404 -203 408 -408
		mu 0 4 295 52 155 291
		f 4 257 23 411 -411
		mu 0 4 193 18 197 296
		f 4 264 62 412 -412
		mu 0 4 197 56 297 296
		f 4 409 -405 413 -413
		mu 0 4 297 52 295 296
		f 4 414 -416 417 -417
		mu 0 4 299 20 300 298
		f 4 -60 63 418 -418
		mu 0 4 300 57 156 298
		f 4 203 -410 419 -419
		mu 0 4 156 52 297 298
		f 4 -9 2 424 -424
		mu 0 4 303 4 304 301
		f 4 421 69 425 -425
		mu 0 4 304 61 305 301
		f 4 422 -211 426 -426
		mu 0 4 305 58 161 301
		f 4 281 30 429 -429
		mu 0 4 209 23 213 306
		f 4 288 70 430 -430
		mu 0 4 213 62 307 306
		f 4 427 -423 431 -431
		mu 0 4 307 58 305 306
		f 4 432 -434 435 -435
		mu 0 4 309 26 310 308
		f 4 -68 71 436 -436
		mu 0 4 310 63 162 308
		f 4 211 -428 437 -437
		mu 0 4 162 58 307 308
		f 4 -11 3 442 -442
		mu 0 4 313 6 314 311
		f 4 439 77 443 -443
		mu 0 4 314 67 315 311
		f 4 440 -219 444 -444
		mu 0 4 315 64 167 311
		f 4 305 36 447 -447
		mu 0 4 225 29 229 316
		f 4 312 78 448 -448
		mu 0 4 229 68 317 316
		f 4 445 -441 449 -449
		mu 0 4 317 64 315 316
		f 4 450 -452 453 -453
		mu 0 4 319 32 320 318
		f 4 -76 79 454 -454
		mu 0 4 320 69 168 318
		f 4 219 -446 455 -455
		mu 0 4 168 64 317 318
		f 4 -6 -458 460 -460
		mu 0 4 322 1 324 321
		f 4 -82 85 461 -461
		mu 0 4 324 73 325 321
		f 4 458 -227 462 -462
		mu 0 4 325 70 172 321
		f 4 -35 40 465 -465
		mu 0 4 327 39 246 326
		f 4 336 86 466 -466
		mu 0 4 246 74 328 326
		f 4 463 -459 467 -467
		mu 0 4 328 70 325 326
		f 4 468 -470 471 -471
		mu 0 4 330 38 331 329
		f 4 -84 87 472 -472
		mu 0 4 331 75 173 329
		f 4 227 -464 473 -473
		mu 0 4 173 70 328 329
		f 4 474 10 477 -477
		mu 0 4 333 12 335 332
		f 4 438 92 478 -478
		mu 0 4 335 79 336 332
		f 4 475 -235 479 -479
		mu 0 4 336 76 178 332
		f 4 217 44 482 -482
		mu 0 4 338 43 264 337
		f 4 360 93 483 -483
		mu 0 4 264 80 339 337
		f 4 480 -476 484 -484
		mu 0 4 339 76 336 337
		f 4 485 -487 488 -488
		mu 0 4 341 42 342 340
		f 4 -91 94 489 -489
		mu 0 4 342 81 179 340
		f 4 235 -481 490 -490
		mu 0 4 179 76 339 340
		f 4 491 5 494 -494
		mu 0 4 344 1 322 343
		f 4 456 98 495 -495
		mu 0 4 322 71 346 343
		f 4 492 -243 496 -496
		mu 0 4 346 82 184 343
		f 4 225 17 499 -499
		mu 0 4 171 17 187 347
		f 4 248 99 500 -500
		mu 0 4 187 85 348 347
		f 4 497 -493 501 -501
		mu 0 4 348 82 346 347
		f 4 502 -397 504 -504
		mu 0 4 350 14 289 349
		f 4 -51 100 505 -505
		mu 0 4 289 50 185 349
		f 4 243 -498 506 -506
		mu 0 4 185 82 348 349
		f 4 507 -509 511 -511
		mu 0 4 352 3 353 351
		f 4 -103 105 512 -512
		mu 0 4 353 88 354 351
		f 4 509 -251 513 -513
		mu 0 4 354 86 189 351
		f 4 -15 18 516 -516
		mu 0 4 198 18 192 355
		f 4 256 106 517 -517
		mu 0 4 192 89 356 355
		f 4 514 -510 518 -518
		mu 0 4 356 86 354 355
		f 4 519 -503 521 -521
		mu 0 4 358 14 350 357
		f 4 -97 107 522 -522
		mu 0 4 350 85 190 357
		f 4 251 -515 523 -523
		mu 0 4 190 86 356 357
		f 4 -2 -525 527 -527
		mu 0 4 294 2 360 359
		f 4 -109 110 528 -528
		mu 0 4 360 91 361 359
		f 4 525 -259 529 -529
		mu 0 4 361 90 194 359
		f 4 -16 19 532 -532
		mu 0 4 271 19 147 362
		f 4 192 111 533 -533
		mu 0 4 147 51 363 362
		f 4 530 -526 534 -534
		mu 0 4 363 90 361 362
		f 4 397 -520 536 -536
		mu 0 4 290 14 358 364
		f 4 -104 112 537 -537
		mu 0 4 358 89 195 364
		f 4 259 -531 538 -538
		mu 0 4 195 90 363 364
		f 4 508 7 542 -542
		mu 0 4 353 3 366 365
		f 4 539 116 543 -543
		mu 0 4 366 93 368 365
		f 4 540 -267 544 -544
		mu 0 4 368 92 199 365
		f 4 353 24 547 -547
		mu 0 4 370 21 202 369
		f 4 272 117 548 -548
		mu 0 4 202 95 371 369
		f 4 545 -541 549 -549
		mu 0 4 371 92 368 369
		f 4 550 -415 552 -552
		mu 0 4 373 20 299 372
		f 4 -59 118 553 -553
		mu 0 4 299 56 200 372
		f 4 267 -546 554 -554
		mu 0 4 200 92 371 372
		f 4 555 -557 559 -559
		mu 0 4 375 5 377 374
		f 4 -121 123 560 -560
		mu 0 4 377 99 378 374
		f 4 557 -275 561 -561
		mu 0 4 378 96 205 374
		f 4 -22 25 564 -564
		mu 0 4 214 23 208 379
		f 4 280 124 565 -565
		mu 0 4 208 100 380 379
		f 4 562 -558 566 -566
		mu 0 4 380 96 378 379
		f 4 567 -551 569 -569
		mu 0 4 382 20 373 381
		f 4 -115 125 570 -570
		mu 0 4 373 95 206 381
		f 4 275 -563 571 -571
		mu 0 4 206 96 380 381
		f 4 -3 -573 575 -575
		mu 0 4 304 4 385 383
		f 4 -127 128 576 -576
		mu 0 4 385 103 386 383
		f 4 573 -283 577 -577
		mu 0 4 386 101 210 383
		f 4 -23 26 580 -580
		mu 0 4 388 25 152 387
		f 4 200 129 581 -581
		mu 0 4 152 57 389 387
		f 4 578 -574 582 -582
		mu 0 4 389 101 386 387
		f 4 415 -568 584 -584
		mu 0 4 300 20 382 390
		f 4 -122 130 585 -585
		mu 0 4 382 100 211 390
		f 4 283 -579 586 -586
		mu 0 4 211 101 389 390
		f 4 556 9 590 -590
		mu 0 4 377 5 392 391
		f 4 587 134 591 -591
		mu 0 4 392 105 394 391
		f 4 588 -291 592 -592
		mu 0 4 394 104 215 391
		f 4 345 31 595 -595
		mu 0 4 396 27 218 395
		f 4 296 135 596 -596
		mu 0 4 218 107 397 395
		f 4 593 -589 597 -597
		mu 0 4 397 104 394 395
		f 4 598 -433 600 -600
		mu 0 4 399 26 309 398
		f 4 -67 136 601 -601
		mu 0 4 309 62 216 398
		f 4 291 -594 602 -602
		mu 0 4 216 104 397 398
		f 4 603 -605 607 -607
		mu 0 4 401 7 403 400
		f 4 -139 141 608 -608
		mu 0 4 403 111 404 400
		f 4 605 -299 609 -609
		mu 0 4 404 108 221 400
		f 4 -29 32 612 -612
		mu 0 4 230 29 224 405
		f 4 304 142 613 -613
		mu 0 4 224 112 406 405
		f 4 610 -606 614 -614
		mu 0 4 406 108 404 405
		f 4 615 -599 617 -617
		mu 0 4 408 26 399 407
		f 4 -133 143 618 -618
		mu 0 4 399 107 222 407
		f 4 299 -611 619 -619
		mu 0 4 222 108 406 407
		f 4 -4 -475 622 -622
		mu 0 4 314 6 410 409
		f 4 -89 145 623 -623
		mu 0 4 410 114 411 409
		f 4 620 -307 624 -624
		mu 0 4 411 113 226 409
		f 4 -30 33 627 -627
		mu 0 4 413 31 158 412
		f 4 208 146 628 -628
		mu 0 4 158 63 414 412
		f 4 625 -621 629 -629
		mu 0 4 414 113 411 412
		f 4 433 -616 631 -631
		mu 0 4 310 26 408 415
		f 4 -140 147 632 -632
		mu 0 4 408 112 227 415
		f 4 307 -626 633 -633
		mu 0 4 227 113 414 415
		f 4 604 11 637 -637
		mu 0 4 403 7 417 416
		f 4 634 151 638 -638
		mu 0 4 417 116 419 416
		f 4 635 -315 639 -639
		mu 0 4 419 115 231 416
		f 4 337 37 642 -642
		mu 0 4 421 33 234 420
		f 4 320 152 643 -643
		mu 0 4 234 118 422 420
		f 4 640 -636 644 -644
		mu 0 4 422 115 419 420
		f 4 645 -451 647 -647
		mu 0 4 424 32 319 423
		f 4 -75 153 648 -648
		mu 0 4 319 68 232 423
		f 4 315 -641 649 -649
		mu 0 4 232 115 422 423
		f 4 457 -492 652 -652
		mu 0 4 426 9 427 425
		f 4 -96 156 653 -653
		mu 0 4 427 121 428 425
		f 4 650 -323 654 -654
		mu 0 4 428 119 237 425
		f 4 -13 38 657 -657
		mu 0 4 430 35 240 429
		f 4 328 157 658 -658
		mu 0 4 240 122 431 429
		f 4 655 -651 659 -659
		mu 0 4 431 119 428 429
		f 4 660 -646 662 -662
		mu 0 4 433 32 424 432
		f 4 -150 158 663 -663
		mu 0 4 424 118 238 432
		f 4 323 -656 664 -664
		mu 0 4 238 119 431 432
		f 4 -1 -666 668 -668
		mu 0 4 435 8 437 434
		f 4 -160 161 669 -669
		mu 0 4 437 126 438 434
		f 4 666 -331 670 -670
		mu 0 4 438 123 243 434
		f 4 -36 39 673 -673
		mu 0 4 440 37 164 439
		f 4 216 162 674 -674
		mu 0 4 164 69 441 439
		f 4 671 -667 675 -675
		mu 0 4 441 123 438 439
		f 4 451 -661 677 -677
		mu 0 4 320 32 433 442
		f 4 -155 163 678 -678
		mu 0 4 433 122 244 442
		f 4 331 -672 679 -679
		mu 0 4 244 123 441 442
		f 4 -12 -604 682 -682
		mu 0 4 444 10 445 443
		f 4 -138 166 683 -683
		mu 0 4 445 129 446 443
		f 4 680 -339 684 -684
		mu 0 4 446 127 249 443
		f 4 -28 41 687 -687
		mu 0 4 448 40 252 447
		f 4 344 167 688 -688
		mu 0 4 252 130 449 447
		f 4 685 -681 689 -689
		mu 0 4 449 127 446 447
		f 4 690 -469 692 -692
		mu 0 4 451 38 330 450
		f 4 -83 168 693 -693
		mu 0 4 330 74 250 450
		f 4 339 -686 694 -694
		mu 0 4 250 127 449 450
		f 4 -10 -556 697 -697
		mu 0 4 453 11 454 452
		f 4 -120 171 698 -698
		mu 0 4 454 133 455 452
		f 4 695 -347 699 -699
		mu 0 4 455 131 255 452
		f 4 -21 42 702 -702
		mu 0 4 457 41 258 456
		f 4 352 172 703 -703
		mu 0 4 258 134 458 456
		f 4 700 -696 704 -704
		mu 0 4 458 131 455 456
		f 4 705 -691 707 -707
		mu 0 4 460 38 451 459
		f 4 -165 173 708 -708
		mu 0 4 451 130 256 459
		f 4 347 -701 709 -709
		mu 0 4 256 131 458 459
		f 4 -8 -508 712 -712
		mu 0 4 462 3 352 461
		f 4 -102 175 713 -713
		mu 0 4 352 87 463 461
		f 4 710 -355 714 -714
		mu 0 4 463 135 261 461
		f 4 -14 43 717 -717
		mu 0 4 188 17 170 464
		f 4 224 176 718 -718
		mu 0 4 170 75 465 464
		f 4 715 -711 719 -719
		mu 0 4 465 135 463 464
		f 4 469 -706 721 -721
		mu 0 4 331 38 460 466
		f 4 -170 177 722 -722
		mu 0 4 460 134 262 466
		f 4 355 -716 723 -723
		mu 0 4 262 135 465 466
		f 4 665 4 726 -726
		mu 0 4 468 0 281 467
		f 4 384 180 727 -727
		mu 0 4 281 47 469 467
		f 4 724 -363 728 -728
		mu 0 4 469 137 267 467
		f 4 193 45 731 -731
		mu 0 4 148 19 270 470
		f 4 368 181 732 -732
		mu 0 4 270 139 471 470
		f 4 729 -725 733 -733
		mu 0 4 471 137 469 470
		f 4 734 -486 736 -736
		mu 0 4 473 42 341 472
		f 4 -90 182 737 -737
		mu 0 4 341 80 268 472
		f 4 363 -730 738 -738
		mu 0 4 268 137 471 472
		f 4 524 6 741 -741
		mu 0 4 360 2 475 474
		f 4 402 185 742 -742
		mu 0 4 475 141 476 474
		f 4 739 -371 743 -743
		mu 0 4 476 140 272 474
		f 4 201 46 746 -746
		mu 0 4 478 44 275 477
		f 4 376 186 747 -747
		mu 0 4 275 142 479 477
		f 4 744 -740 748 -748
		mu 0 4 479 140 476 477
		f 4 749 -735 751 -751
		mu 0 4 481 42 473 480
		f 4 -179 187 752 -752
		mu 0 4 473 139 273 480
		f 4 371 -745 753 -753
		mu 0 4 273 140 479 480
		f 4 572 8 756 -756
		mu 0 4 483 13 484 482
		f 4 420 189 757 -757
		mu 0 4 484 145 485 482
		f 4 754 -379 758 -758
		mu 0 4 485 143 278 482
		f 4 209 47 761 -761
		mu 0 4 487 45 175 486
		f 4 232 190 762 -762
		mu 0 4 175 81 488 486
		f 4 759 -755 763 -763
		mu 0 4 488 143 485 486
		f 4 486 -750 765 -765
		mu 0 4 342 42 481 489
		f 4 -184 191 766 -766
		mu 0 4 481 142 279 489
		f 4 379 -760 767 -767
		mu 0 4 279 143 488 489;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere7";
	rename -uid "FE88EE51-6444-4708-8FDB-999A4396A9F5";
	setAttr ".t" -type "double3" -10.723567889569477 0 0 ;
	setAttr ".s" -type "double3" 1.2023968760672505 1 1.8071131709678889 ;
createNode mesh -n "pSphereShape7" -p "pSphere7";
	rename -uid "AF39B724-5444-E742-B300-E2B0FAE9EFF7";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere8";
	rename -uid "530D5908-7946-158F-C626-4D8D2B6FDCAB";
	setAttr ".t" -type "double3" 6.2666024372367488 0.45299019592278178 9.1077120796040401 ;
	setAttr ".r" -type "double3" -91.409045768379528 1.2664497373224466 41.941127302046745 ;
	setAttr ".s" -type "double3" 1.2023968760672505 1 2.4173839566808182 ;
createNode transform -n "transform11" -p "pSphere8";
	rename -uid "1871ECCB-3048-502D-1B50-54A066198894";
	setAttr ".v" no;
createNode mesh -n "pSphereShape8" -p "transform11";
	rename -uid "13A5A255-A344-39AC-9784-C4BC40A6579A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pSphere9";
	rename -uid "A010E574-A840-6130-7526-CC836BED5270";
	setAttr ".t" -type "double3" 0.51664597701735815 0.20194862072944098 9.1077120796040401 ;
	setAttr ".r" -type "double3" -91.602688074010771 -1.0102666504017659 -32.217512720808955 ;
	setAttr ".s" -type "double3" 1.2023968760672505 1 4.752865370015809 ;
createNode transform -n "transform10" -p "pSphere9";
	rename -uid "36D26A42-1F48-9BD3-A85A-4CA3C91EB8A9";
	setAttr ".v" no;
createNode mesh -n "pSphereShape9" -p "transform10";
	rename -uid "ACA9E125-914D-CC10-7D2E-1796DE442304";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.60000002384185791 0.50000001490116119 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 3 ".pt";
	setAttr ".pt[8]" -type "float3" 1.0603092 0.07906688 -0.4254379 ;
	setAttr ".pt[13]" -type "float3" 1.0603092 0.07906688 -0.4254379 ;
	setAttr ".pt[18]" -type "float3" 1.0603092 0.07906688 -0.4254379 ;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
	setAttr ".dr" 1;
createNode transform -n "pSphere10";
	rename -uid "88BA1FA6-0940-30FC-313C-3483746434BA";
	setAttr ".t" -type "double3" 3.5148872009447256 -0.20706214523196032 8.6717666533546609 ;
	setAttr ".r" -type "double3" -84.824482260061828 34.292542649127981 -87.07859481937912 ;
	setAttr ".s" -type "double3" 2.3796923589698187 1.9791238702758591 3.5765008129524478 ;
createNode transform -n "transform9" -p "pSphere10";
	rename -uid "44F29ED6-EF43-3FAC-A6E2-84A5F02AF4FF";
	setAttr ".v" no;
createNode mesh -n "pSphereShape10" -p "transform9";
	rename -uid "F7D241FB-9D4C-B7C2-6009-68BF7FDBD723";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:29]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere11";
	rename -uid "2E0E6881-AF47-6495-D89B-3489D667BA46";
	setAttr ".t" -type "double3" 3.7460566253332281 0 -5.572697901733429 ;
	setAttr ".r" -type "double3" 0 -43.117021306612351 0 ;
	setAttr ".s" -type "double3" 2.3796923589698187 1.9791238702758591 3.5765008129524478 ;
createNode mesh -n "pSphereShape11" -p "pSphere11";
	rename -uid "5A925087-6442-A986-5558-8190068ED705";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 40 ".uvst[0].uvsp[0:39]" -type "float2" 0 0.16666667 0.2
		 0.16666667 0.40000001 0.16666667 0.60000002 0.16666667 0.80000001 0.16666667 1 0.16666667
		 0 0.33333334 0.2 0.33333334 0.40000001 0.33333334 0.60000002 0.33333334 0.80000001
		 0.33333334 1 0.33333334 0 0.5 0.2 0.5 0.40000001 0.5 0.60000002 0.5 0.80000001 0.5
		 1 0.5 0 0.66666669 0.2 0.66666669 0.40000001 0.66666669 0.60000002 0.66666669 0.80000001
		 0.66666669 1 0.66666669 0 0.83333337 0.2 0.83333337 0.40000001 0.83333337 0.60000002
		 0.83333337 0.80000001 0.83333337 1 0.83333337 0.1 0 0.30000001 0 0.5 0 0.69999999
		 0 0.90000004 0 0.1 1 0.30000001 1 0.5 1 0.69999999 1 0.90000004 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 27 ".vt[0:26]"  0.15450856 -0.86602539 -0.4755283 -0.4045085 -0.86602539 -0.29389268
		 -0.40450853 -0.86602539 0.29389262 0.15450849 -0.86602539 0.47552827 0.5 -0.86602539 0
		 0.26761669 -0.49999997 -0.82363921 -0.70062929 -0.49999997 -0.50903708 -0.70062935 -0.49999997 0.50903696
		 0.26761657 -0.49999997 0.82363915 0.86602545 -0.49999997 0 0.30901712 0 -0.9510566
		 -0.809017 0 -0.58778536 -0.80901706 0 0.58778524 0.30901697 0 0.95105654 1 0 0 0.26761669 0.49999997 -0.82363921
		 -0.70062929 0.49999997 -0.50903708 -0.70062935 0.49999997 0.50903696 0.26761657 0.49999997 0.82363915
		 0.86602545 0.49999997 0 0.15450856 0.86602539 -0.4755283 -0.4045085 0.86602539 -0.29389268
		 -0.40450853 0.86602539 0.29389262 0.15450849 0.86602539 0.47552827 0.5 0.86602539 0
		 0 -1 0 0 1 0;
	setAttr -s 55 ".ed[0:54]"  0 1 0 1 2 0 2 3 0 3 4 0 4 0 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 5 0 10 11 0 11 12 0 12 13 0 13 14 0 14 10 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 15 0 20 21 0 21 22 0 22 23 0 23 24 0 24 20 0 0 5 0 1 6 0 2 7 0 3 8 0 4 9 0
		 5 10 0 6 11 0 7 12 0 8 13 0 9 14 0 10 15 0 11 16 0 12 17 0 13 18 0 14 19 0 15 20 0
		 16 21 0 17 22 0 18 23 0 19 24 0 25 0 0 25 1 0 25 2 0 25 3 0 25 4 0 20 26 0 21 26 0
		 22 26 0 23 26 0 24 26 0;
	setAttr -s 30 -ch 110 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -6 -26
		mu 0 4 0 1 7 6
		f 4 1 27 -7 -27
		mu 0 4 1 2 8 7
		f 4 2 28 -8 -28
		mu 0 4 2 3 9 8
		f 4 3 29 -9 -29
		mu 0 4 3 4 10 9
		f 4 4 25 -10 -30
		mu 0 4 4 5 11 10
		f 4 5 31 -11 -31
		mu 0 4 6 7 13 12
		f 4 6 32 -12 -32
		mu 0 4 7 8 14 13
		f 4 7 33 -13 -33
		mu 0 4 8 9 15 14
		f 4 8 34 -14 -34
		mu 0 4 9 10 16 15
		f 4 9 30 -15 -35
		mu 0 4 10 11 17 16
		f 4 10 36 -16 -36
		mu 0 4 12 13 19 18
		f 4 11 37 -17 -37
		mu 0 4 13 14 20 19
		f 4 12 38 -18 -38
		mu 0 4 14 15 21 20
		f 4 13 39 -19 -39
		mu 0 4 15 16 22 21
		f 4 14 35 -20 -40
		mu 0 4 16 17 23 22
		f 4 15 41 -21 -41
		mu 0 4 18 19 25 24
		f 4 16 42 -22 -42
		mu 0 4 19 20 26 25
		f 4 17 43 -23 -43
		mu 0 4 20 21 27 26
		f 4 18 44 -24 -44
		mu 0 4 21 22 28 27
		f 4 19 40 -25 -45
		mu 0 4 22 23 29 28
		f 3 -1 -46 46
		mu 0 3 1 0 30
		f 3 -2 -47 47
		mu 0 3 2 1 31
		f 3 -3 -48 48
		mu 0 3 3 2 32
		f 3 -4 -49 49
		mu 0 3 4 3 33
		f 3 -5 -50 45
		mu 0 3 5 4 34
		f 3 20 51 -51
		mu 0 3 24 25 35
		f 3 21 52 -52
		mu 0 3 25 26 36
		f 3 22 53 -53
		mu 0 3 26 27 37
		f 3 23 54 -54
		mu 0 3 27 28 38
		f 3 24 50 -55
		mu 0 3 28 29 39;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pSphere12";
	rename -uid "08AE63F5-924B-FE41-4AFA-F0985F7D8779";
	setAttr ".t" -type "double3" -5.1734862327939695 6.5409010249468409 2.5552723217520175 ;
createNode mesh -n "pSphere12Shape" -p "pSphere12";
	rename -uid "28E4F2AD-9742-6D95-4781-819BD30F3EEA";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube4";
	rename -uid "C9E28324-9A4A-E025-85D8-0E88BBE64BDA";
	setAttr ".t" -type "double3" 3.6062310808177482 0 1.4175917484693841 ;
	setAttr ".rp" -type "double3" -7.7833772872481575 0 -6.3894380421447341 ;
	setAttr ".sp" -type "double3" -7.7833772872481575 0 -6.3894380421447341 ;
createNode mesh -n "pCube4Shape" -p "pCube4";
	rename -uid "837F0BBE-2449-3813-8A58-2382464751DF";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pSphere13";
	rename -uid "A5F0C9CF-4846-6443-8772-AA939E04FFF4";
	setAttr ".t" -type "double3" 2.3146161775687109 0 -7.7456193369221005 ;
	setAttr ".rp" -type "double3" 4.0715442963251345 0 8.3105811072955316 ;
	setAttr ".sp" -type "double3" 4.0715442963251345 0 8.3105811072955316 ;
createNode mesh -n "pSphere13Shape" -p "pSphere13";
	rename -uid "819A3AE0-5444-D0CF-81EE-7985683CB2C4";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "46E127F0-9948-CABA-F82A-E38EB770CB80";
	setAttr -s 4 ".lnk";
	setAttr -s 4 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "4137666A-3C4F-AFF1-5268-03A933316067";
createNode displayLayer -n "defaultLayer";
	rename -uid "AE654165-314A-4AF4-2AA2-D38F37A6AB11";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "51970C4A-894D-2275-8A22-3F9579CDE6C2";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "179C8F70-3747-AD65-1382-9E99E27B1640";
	setAttr ".g" yes;
createNode polySphere -n "polySphere1";
	rename -uid "D2226D46-D141-7038-FD29-A0A0FA17FEFA";
	setAttr ".sa" 5;
	setAttr ".sh" 5;
createNode polySphere -n "polySphere2";
	rename -uid "FED02941-B141-23CB-B04C-07BB84CB8863";
	setAttr ".sa" 5;
	setAttr ".sh" 6;
createNode polySphere -n "polySphere3";
	rename -uid "E3968E78-0A45-D032-F188-D3B2303306CB";
	setAttr ".sa" 3;
	setAttr ".sh" 5;
createNode polyCube -n "polyCube1";
	rename -uid "FAF7EDC9-DA4B-7BC7-7FE1-76841AB8AF01";
	setAttr ".cuv" 1;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "D4439C91-8445-F263-0CAD-2D930B7D771D";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polySubdFace -n "polySubdFace2";
	rename -uid "71A6328D-B840-E1CB-F371-DB8D9D77C9AE";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace3";
	rename -uid "2F2AFE08-EB48-9387-67C2-3FA15D1C1C71";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode lambert -n "west_rock01";
	rename -uid "B6DF6D16-CA4D-8201-C807-EE8E6B486F79";
	setAttr ".c" -type "float3" 0.38699999 0.17998426 0.114939 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "3519BC43-0841-E7F1-4AB3-608AB9B4A2FD";
	setAttr ".ihi" 0;
	setAttr -s 13 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 11 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "960E710A-794B-828F-48BB-7A8D139C44EB";
createNode lambert -n "west_rock02";
	rename -uid "638402F3-0847-DAB0-8940-A18281AB47FB";
	setAttr ".c" -type "float3" 0.241 0.07977099 0.07977099 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "11590208-BF4C-1CD0-EA85-979012A8D03E";
	setAttr ".ihi" 0;
	setAttr -s 18 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 17 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "9F8C69B3-FE42-30B1-481C-12B301C3E3F2";
createNode vectorRenderGlobals -s -n "vectorRenderGlobals";
	rename -uid "A8E70F93-AC41-E09D-1C16-FFBBB4E9E6A7";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "505723C0-1C44-627C-DB4F-AD8A32ADA6E9";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 1\n                -height 1\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 1270\n                -height 738\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1270\n            -height 738\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1270\\n    -height 738\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1270\\n    -height 738\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "2E38AADA-D14A-4A56-A69A-F1BC3D79C5C3";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyUnite -n "polyUnite1";
	rename -uid "21B05DB8-A541-C542-9BDD-4B91EDA9CD95";
	setAttr -s 3 ".ip";
	setAttr -s 3 ".im";
createNode groupId -n "groupId1";
	rename -uid "F19035D4-1147-0EC7-B4F2-EC97379B8E4E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "237F50D3-2A49-19CA-4BD0-7CB15A07B682";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:24]";
createNode groupId -n "groupId2";
	rename -uid "239504E4-E648-355F-E9CD-61952920E1DB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "37D4B5D6-0942-B855-C37E-3D87E0CDE551";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "886B2FA9-274D-1181-9ED3-EBA26B86665B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId4";
	rename -uid "DF7D6EBF-774E-56CA-0879-8ABB1D8B150C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "E266EEED-C040-DBF7-AAFA-9C801B224BBF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "E5FB8D55-FD4D-9864-2988-1CABED447299";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "2FD9677A-EB40-CC18-58F8-EB9317E143E9";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "E7392047-0F49-57A4-8F8A-9D9CBF7A50E1";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:24]";
createNode groupId -n "groupId8";
	rename -uid "F760ADBC-AB4E-40F2-3D49-96B89370E22E";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "8AF1F83F-414A-4D27-5E83-F9864ED0CEDE";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[25:84]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "B6DD42A9-9144-D117-9C0D-BA9CC09A801F";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -740.47616105231157 -119.04761431709188 ;
	setAttr ".tgi[0].vh" -type "double2" 705.95235290035475 122.61904274660465 ;
	setAttr -s 4 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -58.571430206298828;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[2].y" -10;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 262.85714721679688;
	setAttr ".tgi[0].ni[3].y" -67.142860412597656;
	setAttr ".tgi[0].ni[3].nvs" 1923;
createNode polyUnite -n "polyUnite2";
	rename -uid "04919002-0445-0532-5809-5B8D3C6A2863";
	setAttr -s 5 ".ip";
	setAttr -s 5 ".im";
createNode groupId -n "groupId9";
	rename -uid "64C94249-9546-26B0-BB4D-EAABCA3DA348";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "5376B2FB-0940-DED4-C300-A08C91B3E03C";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:383]";
createNode groupId -n "groupId10";
	rename -uid "3A5B8EA1-4F41-86AC-B2F8-A885DA4E2A2A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "247F7287-384A-6525-A093-6290B74B9813";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "418EDD47-C445-3D90-8F49-25AA25364E1A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "8885499F-1849-891A-77E6-AD8DB7FB1987";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "188C8D78-F844-F84F-9B0C-39A1E7CB1BE6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "81F13102-A143-7D81-F1DD-88A269BAF14C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "97337B36-774B-2DC3-BD94-48B3E089FD18";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "C75A50E5-234F-3FC8-E851-14945B3B80FC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "FD8D3801-3848-E07D-7CF8-9E8DEC75F9B1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "A5BA07C6-0A42-B55D-C103-C5B43540796B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "3F7DC7AA-0C46-6DC7-7927-14826C87105E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:383]" "f[768:797]";
createNode groupId -n "groupId20";
	rename -uid "1424BFE8-2C47-A53D-242E-6AB949FC64EC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "A76F5960-504B-4D08-8CF1-2286AAC38C32";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[384:767]" "f[798:1211]";
createNode polyUnite -n "polyUnite3";
	rename -uid "58A8CA71-2947-B419-FB50-328C7F1D0222";
	setAttr -s 3 ".ip";
	setAttr -s 3 ".im";
createNode groupId -n "groupId21";
	rename -uid "0F34F6B8-BE47-DB3F-D482-28985A6FF0B8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "311D1C39-2C47-2AC3-53C9-A8A2CD5EA262";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "ADBE45BA-5B40-C02C-E893-A0BE9AA7FFED";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "9990B454-B145-F8F0-9DDD-61B7B70E7A9E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "E47701B5-854D-DCE4-7F65-858F52524EA7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "FE1ADEF5-4D41-7B4D-47F6-C1AA99503C26";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "F0818410-6248-CF15-5163-5F91ED00B3C1";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "B10ECF66-3749-2327-83CD-B5A85ABF47A6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:59]";
createNode groupId -n "groupId28";
	rename -uid "69280FAF-1B4F-FA54-0D1E-D19947D86DEE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "A105927F-614B-328F-EA93-B187B0E71A35";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[60:89]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 4 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mayaHardware";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId1.id" "pSphereShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pSphereShape1.i";
connectAttr "groupId2.id" "pSphereShape1.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pSphereShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape2.iog.og[0].gco";
connectAttr "groupParts2.og" "pSphereShape2.i";
connectAttr "groupId4.id" "pSphereShape2.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pSphereShape3.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape3.iog.og[0].gco";
connectAttr "groupId6.id" "pSphereShape3.ciog.cog[0].cgid";
connectAttr "polySphere3.out" "pSphereShape4.i";
connectAttr "groupId9.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts5.og" "pCubeShape1.i";
connectAttr "groupId10.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pCubeShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId12.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId13.id" "pSphereShape5.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape5.iog.og[0].gco";
connectAttr "groupId14.id" "pSphereShape5.ciog.cog[0].cgid";
connectAttr "groupId15.id" "pSphereShape6.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape6.iog.og[0].gco";
connectAttr "groupId16.id" "pSphereShape6.ciog.cog[0].cgid";
connectAttr "groupId17.id" "pCubeShape3.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId18.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId21.id" "pSphereShape8.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape8.iog.og[0].gco";
connectAttr "groupId22.id" "pSphereShape8.ciog.cog[0].cgid";
connectAttr "groupId23.id" "pSphereShape9.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphereShape9.iog.og[0].gco";
connectAttr "groupId24.id" "pSphereShape9.ciog.cog[0].cgid";
connectAttr "groupId25.id" "pSphereShape10.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphereShape10.iog.og[0].gco";
connectAttr "groupId26.id" "pSphereShape10.ciog.cog[0].cgid";
connectAttr "groupParts4.og" "pSphere12Shape.i";
connectAttr "groupId7.id" "pSphere12Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pSphere12Shape.iog.og[0].gco";
connectAttr "groupId8.id" "pSphere12Shape.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pSphere12Shape.iog.og[1].gco";
connectAttr "groupParts7.og" "pCube4Shape.i";
connectAttr "groupId19.id" "pCube4Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCube4Shape.iog.og[0].gco";
connectAttr "groupId20.id" "pCube4Shape.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pCube4Shape.iog.og[1].gco";
connectAttr "groupParts9.og" "pSphere13Shape.i";
connectAttr "groupId27.id" "pSphere13Shape.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pSphere13Shape.iog.og[0].gco";
connectAttr "groupId28.id" "pSphere13Shape.iog.og[1].gid";
connectAttr "lambert2SG.mwc" "pSphere13Shape.iog.og[1].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polySubdFace1.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polySubdFace3.ip";
connectAttr "west_rock01.oc" "lambert2SG.ss";
connectAttr "pSphereShape4.iog" "lambert2SG.dsm" -na;
connectAttr "pSphereShape11.iog" "lambert2SG.dsm" -na;
connectAttr "pSphereShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphere12Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape5.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape5.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCube4Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape10.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pSphereShape10.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pSphere13Shape.iog.og[1]" "lambert2SG.dsm" -na;
connectAttr "groupId1.msg" "lambert2SG.gn" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "groupId7.msg" "lambert2SG.gn" -na;
connectAttr "groupId9.msg" "lambert2SG.gn" -na;
connectAttr "groupId10.msg" "lambert2SG.gn" -na;
connectAttr "groupId13.msg" "lambert2SG.gn" -na;
connectAttr "groupId14.msg" "lambert2SG.gn" -na;
connectAttr "groupId19.msg" "lambert2SG.gn" -na;
connectAttr "groupId25.msg" "lambert2SG.gn" -na;
connectAttr "groupId26.msg" "lambert2SG.gn" -na;
connectAttr "groupId28.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "west_rock01.msg" "materialInfo1.m";
connectAttr "west_rock02.oc" "lambert3SG.ss";
connectAttr "pSphereShape7.iog" "lambert3SG.dsm" -na;
connectAttr "pSphereShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape3.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape3.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphere12Shape.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape6.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape6.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCube4Shape.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape8.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape8.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape9.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pSphereShape9.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pSphere13Shape.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "groupId3.msg" "lambert3SG.gn" -na;
connectAttr "groupId4.msg" "lambert3SG.gn" -na;
connectAttr "groupId5.msg" "lambert3SG.gn" -na;
connectAttr "groupId6.msg" "lambert3SG.gn" -na;
connectAttr "groupId8.msg" "lambert3SG.gn" -na;
connectAttr "groupId11.msg" "lambert3SG.gn" -na;
connectAttr "groupId12.msg" "lambert3SG.gn" -na;
connectAttr "groupId15.msg" "lambert3SG.gn" -na;
connectAttr "groupId16.msg" "lambert3SG.gn" -na;
connectAttr "groupId17.msg" "lambert3SG.gn" -na;
connectAttr "groupId18.msg" "lambert3SG.gn" -na;
connectAttr "groupId20.msg" "lambert3SG.gn" -na;
connectAttr "groupId21.msg" "lambert3SG.gn" -na;
connectAttr "groupId22.msg" "lambert3SG.gn" -na;
connectAttr "groupId23.msg" "lambert3SG.gn" -na;
connectAttr "groupId24.msg" "lambert3SG.gn" -na;
connectAttr "groupId27.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "west_rock02.msg" "materialInfo2.m";
connectAttr "pSphereShape1.o" "polyUnite1.ip[0]";
connectAttr "pSphereShape2.o" "polyUnite1.ip[1]";
connectAttr "pSphereShape3.o" "polyUnite1.ip[2]";
connectAttr "pSphereShape1.wm" "polyUnite1.im[0]";
connectAttr "pSphereShape2.wm" "polyUnite1.im[1]";
connectAttr "pSphereShape3.wm" "polyUnite1.im[2]";
connectAttr "polySphere1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySphere2.out" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "polyUnite1.out" "groupParts3.ig";
connectAttr "groupId7.id" "groupParts3.gi";
connectAttr "groupParts3.og" "groupParts4.ig";
connectAttr "groupId8.id" "groupParts4.gi";
connectAttr "west_rock01.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "west_rock02.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "pCubeShape1.o" "polyUnite2.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite2.ip[1]";
connectAttr "pSphereShape5.o" "polyUnite2.ip[2]";
connectAttr "pSphereShape6.o" "polyUnite2.ip[3]";
connectAttr "pCubeShape3.o" "polyUnite2.ip[4]";
connectAttr "pCubeShape1.wm" "polyUnite2.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite2.im[1]";
connectAttr "pSphereShape5.wm" "polyUnite2.im[2]";
connectAttr "pSphereShape6.wm" "polyUnite2.im[3]";
connectAttr "pCubeShape3.wm" "polyUnite2.im[4]";
connectAttr "polySubdFace3.out" "groupParts5.ig";
connectAttr "groupId9.id" "groupParts5.gi";
connectAttr "polyUnite2.out" "groupParts6.ig";
connectAttr "groupId19.id" "groupParts6.gi";
connectAttr "groupParts6.og" "groupParts7.ig";
connectAttr "groupId20.id" "groupParts7.gi";
connectAttr "pSphereShape8.o" "polyUnite3.ip[0]";
connectAttr "pSphereShape9.o" "polyUnite3.ip[1]";
connectAttr "pSphereShape10.o" "polyUnite3.ip[2]";
connectAttr "pSphereShape8.wm" "polyUnite3.im[0]";
connectAttr "pSphereShape9.wm" "polyUnite3.im[1]";
connectAttr "pSphereShape10.wm" "polyUnite3.im[2]";
connectAttr "polyUnite3.out" "groupParts8.ig";
connectAttr "groupId27.id" "groupParts8.gi";
connectAttr "groupParts8.og" "groupParts9.ig";
connectAttr "groupId28.id" "groupParts9.gi";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "west_rock01.msg" ":defaultShaderList1.s" -na;
connectAttr "west_rock02.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of west_rocks.ma
