//Maya ASCII 2016 scene
//Name: west_tower.ma
//Last modified: Tue, Jan 24, 2017 02:03:15 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "1DF25CB7-EE42-CDEA-86A6-838ACDA70E2C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -5.2314837220847501 12.341655710997516 6.7994381464019913 ;
	setAttr ".r" -type "double3" -37.538352730307352 2482.9999999997226 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "322AEA05-5D47-1D2A-CA08-65B7310FCA0D";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 10.864189093709289;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.047020807508839968 5.7221884227717377 -0.080576517199760223 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "44EA3E45-A142-32E5-3DF4-63BF99236B94";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.10000000000002 2.1853162288308026e-14 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "222C35A8-754C-7C37-CB09-83B522B64991";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 7.9631753307920379;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "ADD72C39-074B-47C4-4AA1-7A90A0E33CE7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.67965051006557187 4.3125475494611063 100.46550863551921 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "85D43C2C-A545-E23A-517D-099AEF426515";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.8903591424241017;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "DEB4BF4C-334F-4C94-BC40-42B0CDD2D692";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.2637458120856 4.2119746467946246 -0.060207429915507982 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "279D1B1D-4C45-5367-82AD-07B727557575";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.9026624488314721;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "6E9E8E00-9B44-B8BE-44C0-26B2F5963848";
	setAttr ".t" -type "double3" -0.8063026064900225 2.5368836015026721 -0.80576563600021123 ;
	setAttr ".r" -type "double3" 2.1742747829347486 0.067800639591272846 -3.4930943732971698 ;
	setAttr ".s" -type "double3" 0.14630192674673059 5.0861181333101788 0.14630192674673059 ;
createNode transform -n "transform34" -p "pCube1";
	rename -uid "57CDCE1D-A242-9477-DD05-3080537382D9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform34";
	rename -uid "742D7E17-9145-BD10-2744-2C8C1F2D0E05";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "BA1651C8-664F-B03F-85BC-009F66796F25";
	setAttr ".t" -type "double3" 0.73915547015666982 2.5368836015026721 -0.83262449053355159 ;
	setAttr ".r" -type "double3" 2.2869606477633595 -0.045401969784512698 4.1943967424902295 ;
	setAttr ".s" -type "double3" 0.14630192674673059 5.0861181333101788 0.14630192674673059 ;
createNode transform -n "transform33" -p "pCube2";
	rename -uid "1AF6AA5D-BF46-599A-8CA2-C3A5797BD0A3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform33";
	rename -uid "903EE808-7547-5A1E-EA2F-DC8AC36A8188";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "4124D3E0-4D49-BB20-F481-A0BFC012F124";
	setAttr ".t" -type "double3" -0.8197320337566929 2.5368836015026721 0.72518907240019037 ;
	setAttr ".r" -type "double3" -4.9475041669501572 -0.040295932498079695 -3.4484874544018909 ;
	setAttr ".s" -type "double3" 0.14630192674673059 5.0861181333101788 0.14630192674673059 ;
createNode transform -n "transform32" -p "pCube3";
	rename -uid "4588D5AC-A24D-D6BA-B438-5FA3D5A5F18D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform32";
	rename -uid "CF51C9F2-2A41-C014-1F3B-CD8256D2789B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube4";
	rename -uid "13EB9FA7-1144-CB96-4BD3-7E8BAA293455";
	setAttr ".t" -type "double3" 0.71122267464370781 2.5368836015026721 0.72518907240019037 ;
	setAttr ".r" -type "double3" -4.9538561969206993 0.15337671362582939 4.4108717317705057 ;
	setAttr ".s" -type "double3" 0.14630192674673059 5.0861181333101788 0.14630192674673059 ;
createNode transform -n "transform31" -p "pCube4";
	rename -uid "68A6B374-F949-7F9C-C998-4794E8B08C54";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform31";
	rename -uid "28378C3D-0745-92EA-9D8D-9DBF3DA88789";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "69645627-A044-EB98-1A3C-FAAB1D854E3F";
	setAttr ".t" -type "double3" -0.053717709066680497 5.0733456419628986 -0.10743541813336144 ;
	setAttr ".s" -type "double3" 1.3516761890806368 0.064292433365357241 1.3742041378199246 ;
createNode transform -n "transform30" -p "pCube5";
	rename -uid "C9FD749F-F141-0FA1-9B95-1AA77614777F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform30";
	rename -uid "4F141284-ED49-BD85-364F-C8B1A434812F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube6";
	rename -uid "6719628E-BB45-B425-8052-1C88A0052CB8";
	setAttr ".t" -type "double3" -0.053717709066680497 5.1710619531411419 -0.10743541813336144 ;
	setAttr ".s" -type "double3" 1.7803363029760388 0.12931865975258233 1.8100085908332508 ;
createNode transform -n "transform29" -p "pCube6";
	rename -uid "7BBB5202-414F-3BE4-F4F1-6092CE442451";
	setAttr ".v" no;
createNode mesh -n "pCubeShape6" -p "transform29";
	rename -uid "CCE8E4AC-4F40-011E-5C69-D18ABE1F572B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube7";
	rename -uid "9D250166-CF49-CF5D-940A-3FADDE70B449";
	setAttr ".t" -type "double3" 0 1.6014552027981663 -0.83137066737610588 ;
	setAttr ".s" -type "double3" 1.6915623308999868 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform28" -p "pCube7";
	rename -uid "A0791962-2744-B615-1889-198DDF04C803";
	setAttr ".v" no;
createNode mesh -n "pCubeShape7" -p "transform28";
	rename -uid "BEA16412-004D-7068-A00D-84B7636390AD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube8";
	rename -uid "069830EF-7346-A82E-58D9-168743E08152";
	setAttr ".t" -type "double3" 0 3.4164377659694214 -0.83137066737610588 ;
	setAttr ".s" -type "double3" 1.4503681421843158 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform27" -p "pCube8";
	rename -uid "5CC345C2-7D45-E489-A17D-2EB355BEAA2C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape8" -p "transform27";
	rename -uid "13ABE6BF-A44D-12E3-C639-58B7517F36D0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "99A8839A-FE42-F4DC-5847-B08809E2F859";
	setAttr ".t" -type "double3" -0.058813331267495883 3.4164377659694214 0.68258179440769773 ;
	setAttr ".s" -type "double3" 1.4503681421843158 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform26" -p "pCube9";
	rename -uid "0F88E42F-C544-17CC-EE3B-859FC2F52D95";
	setAttr ".v" no;
createNode mesh -n "pCubeShape9" -p "transform26";
	rename -uid "54067F80-B94D-463A-26AB-1381A7594BCC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "937638BF-294A-9768-B686-4FBA51A34483";
	setAttr ".t" -type "double3" -0.058813331267495883 1.6014552027981663 0.83014457434853428 ;
	setAttr ".s" -type "double3" 1.6915623308999868 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform25" -p "pCube10";
	rename -uid "5DD3C039-8A48-881F-7A52-CAB05CE91D3B";
	setAttr ".v" no;
createNode mesh -n "pCubeShape10" -p "transform25";
	rename -uid "CD2A791D-5340-3D10-348F-889B201C0D3E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11";
	rename -uid "1AD3D38C-664F-D9A4-7D74-CD85BCE15009";
	setAttr ".t" -type "double3" 0.67138257730294104 3.4164377659694214 -0.076230920864239549 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1.4503681421843158 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform24" -p "pCube11";
	rename -uid "A8E5CF99-7944-6CDE-465B-0E81FD76A29A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape11" -p "transform24";
	rename -uid "C500308B-C54F-2470-DB7D-998F5CE178B8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube12";
	rename -uid "84812AB3-7F46-1931-5900-BA84450BB759";
	setAttr ".t" -type "double3" -0.74996636925143612 3.4164377659694214 -0.076230920864239549 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1.4503681421843158 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform23" -p "pCube12";
	rename -uid "BA20D39D-9447-4DEA-3FFB-FAABC02D00EF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape12" -p "transform23";
	rename -uid "761E9370-074E-0353-A7F0-A99204F86E36";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube13";
	rename -uid "F70C6238-EA40-83F1-3D26-E89FA75E1FDD";
	setAttr ".t" -type "double3" 0.81131197311642911 1.6014552027981663 0.0066468198537563694 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1.6915623308999868 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform22" -p "pCube13";
	rename -uid "EBD1CFD2-D940-846B-D477-F790BFCB0EAF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape13" -p "transform22";
	rename -uid "6C6F9303-B046-FD0B-979D-D88B0DAFCA18";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube14";
	rename -uid "86FC42C8-E342-536A-382B-399E583916F0";
	setAttr ".t" -type "double3" -0.86565351119123157 1.6014552027981663 0.0066468198537563694 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1.6915623308999868 0.07083626024989513 0.056616237043284043 ;
createNode transform -n "transform21" -p "pCube14";
	rename -uid "B0A8D221-8A48-B3EC-C52A-45AB1703C948";
	setAttr ".v" no;
createNode mesh -n "pCubeShape14" -p "transform21";
	rename -uid "6A189C13-D246-1FA0-F604-959B358097A4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pPipe1";
	rename -uid "B5F7E5F5-4A48-09D2-C6BF-35B45BDFF723";
	setAttr ".t" -type "double3" -0.047020761108579334 5.7221884227717377 -0.080576563600020856 ;
	setAttr ".s" -type "double3" 0.77846719541798404 1.2801460233124917 0.77846719541798404 ;
createNode transform -n "transform20" -p "pPipe1";
	rename -uid "D2C24E2E-4641-A0D4-2D61-54A3E3577EDD";
	setAttr ".v" no;
createNode mesh -n "pPipeShape1" -p "transform20";
	rename -uid "97AB4AE9-7F4A-F89E-5886-1ABB0FA5B783";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder1";
	rename -uid "A02FD200-B047-7573-F2BD-6BB794FEA6C8";
	setAttr ".t" -type "double3" -0.022960579220061339 6.1281348431414404 -0.09161376573573754 ;
	setAttr ".s" -type "double3" 0.70464753765307298 0.032754829939044536 0.7519102014249579 ;
createNode transform -n "transform19" -p "pCylinder1";
	rename -uid "D78249CB-BE4E-83BA-73FA-A3B3878E771F";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform19";
	rename -uid "AF128581-1843-D5AF-8FC2-FCBF9C5FC7DC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus1";
	rename -uid "3A971D23-7043-DBA6-63DC-4AB2DB0310D5";
	setAttr ".t" -type "double3" -0.042047451481803222 6.0172118233456597 -0.083335831019481721 ;
	setAttr ".s" -type "double3" 0.73067180144743415 0.44444440031113436 0.72222208444576619 ;
createNode transform -n "transform18" -p "pTorus1";
	rename -uid "7046AA54-C342-ADD2-63A7-83AE54FBE525";
	setAttr ".v" no;
createNode mesh -n "pTorusShape1" -p "transform18";
	rename -uid "F7E609C6-0C4C-6FBC-1F6F-4C976CB27C88";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pTorus2";
	rename -uid "12CA83B7-AA4F-2A88-7101-C780DA0ECC6C";
	setAttr ".t" -type "double3" -0.042047451481803222 5.516203971711489 -0.083335831019481721 ;
	setAttr ".s" -type "double3" 0.73067180144743415 0.44444440031113436 0.72222208444576619 ;
createNode transform -n "transform17" -p "pTorus2";
	rename -uid "EED43F94-C54E-27CE-EA7D-37A4C30CEAA2";
	setAttr ".v" no;
createNode mesh -n "pTorusShape2" -p "transform17";
	rename -uid "A0C1205E-A34F-C735-2107-C8A09C65AB0E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:199]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 231 ".uvst[0].uvsp[0:230]" -type "float2" 0 1 0.1 1 0.2 1 0.30000001
		 1 0.40000001 1 0.5 1 0.60000002 1 0.70000005 1 0.80000007 1 0.9000001 1 1.000000119209
		 1 0 0.94999999 0.1 0.94999999 0.2 0.94999999 0.30000001 0.94999999 0.40000001 0.94999999
		 0.5 0.94999999 0.60000002 0.94999999 0.70000005 0.94999999 0.80000007 0.94999999
		 0.9000001 0.94999999 1.000000119209 0.94999999 0 0.89999998 0.1 0.89999998 0.2 0.89999998
		 0.30000001 0.89999998 0.40000001 0.89999998 0.5 0.89999998 0.60000002 0.89999998
		 0.70000005 0.89999998 0.80000007 0.89999998 0.9000001 0.89999998 1.000000119209 0.89999998
		 0 0.84999996 0.1 0.84999996 0.2 0.84999996 0.30000001 0.84999996 0.40000001 0.84999996
		 0.5 0.84999996 0.60000002 0.84999996 0.70000005 0.84999996 0.80000007 0.84999996
		 0.9000001 0.84999996 1.000000119209 0.84999996 0 0.79999995 0.1 0.79999995 0.2 0.79999995
		 0.30000001 0.79999995 0.40000001 0.79999995 0.5 0.79999995 0.60000002 0.79999995
		 0.70000005 0.79999995 0.80000007 0.79999995 0.9000001 0.79999995 1.000000119209 0.79999995
		 0 0.74999994 0.1 0.74999994 0.2 0.74999994 0.30000001 0.74999994 0.40000001 0.74999994
		 0.5 0.74999994 0.60000002 0.74999994 0.70000005 0.74999994 0.80000007 0.74999994
		 0.9000001 0.74999994 1.000000119209 0.74999994 0 0.69999993 0.1 0.69999993 0.2 0.69999993
		 0.30000001 0.69999993 0.40000001 0.69999993 0.5 0.69999993 0.60000002 0.69999993
		 0.70000005 0.69999993 0.80000007 0.69999993 0.9000001 0.69999993 1.000000119209 0.69999993
		 0 0.64999992 0.1 0.64999992 0.2 0.64999992 0.30000001 0.64999992 0.40000001 0.64999992
		 0.5 0.64999992 0.60000002 0.64999992 0.70000005 0.64999992 0.80000007 0.64999992
		 0.9000001 0.64999992 1.000000119209 0.64999992 0 0.5999999 0.1 0.5999999 0.2 0.5999999
		 0.30000001 0.5999999 0.40000001 0.5999999 0.5 0.5999999 0.60000002 0.5999999 0.70000005
		 0.5999999 0.80000007 0.5999999 0.9000001 0.5999999 1.000000119209 0.5999999 0 0.54999989
		 0.1 0.54999989 0.2 0.54999989 0.30000001 0.54999989 0.40000001 0.54999989 0.5 0.54999989
		 0.60000002 0.54999989 0.70000005 0.54999989 0.80000007 0.54999989 0.9000001 0.54999989
		 1.000000119209 0.54999989 0 0.49999988 0.1 0.49999988 0.2 0.49999988 0.30000001 0.49999988
		 0.40000001 0.49999988 0.5 0.49999988 0.60000002 0.49999988 0.70000005 0.49999988
		 0.80000007 0.49999988 0.9000001 0.49999988 1.000000119209 0.49999988 0 0.44999987
		 0.1 0.44999987 0.2 0.44999987 0.30000001 0.44999987 0.40000001 0.44999987 0.5 0.44999987
		 0.60000002 0.44999987 0.70000005 0.44999987 0.80000007 0.44999987 0.9000001 0.44999987
		 1.000000119209 0.44999987 0 0.39999986 0.1 0.39999986 0.2 0.39999986 0.30000001 0.39999986
		 0.40000001 0.39999986 0.5 0.39999986 0.60000002 0.39999986 0.70000005 0.39999986
		 0.80000007 0.39999986 0.9000001 0.39999986 1.000000119209 0.39999986 0 0.34999985
		 0.1 0.34999985 0.2 0.34999985 0.30000001 0.34999985 0.40000001 0.34999985 0.5 0.34999985
		 0.60000002 0.34999985 0.70000005 0.34999985 0.80000007 0.34999985 0.9000001 0.34999985
		 1.000000119209 0.34999985 0 0.29999983 0.1 0.29999983 0.2 0.29999983 0.30000001 0.29999983
		 0.40000001 0.29999983 0.5 0.29999983 0.60000002 0.29999983 0.70000005 0.29999983
		 0.80000007 0.29999983 0.9000001 0.29999983 1.000000119209 0.29999983 0 0.24999984
		 0.1 0.24999984 0.2 0.24999984 0.30000001 0.24999984 0.40000001 0.24999984 0.5 0.24999984
		 0.60000002 0.24999984 0.70000005 0.24999984 0.80000007 0.24999984 0.9000001 0.24999984
		 1.000000119209 0.24999984 0 0.19999984 0.1 0.19999984 0.2 0.19999984 0.30000001 0.19999984
		 0.40000001 0.19999984 0.5 0.19999984 0.60000002 0.19999984 0.70000005 0.19999984
		 0.80000007 0.19999984 0.9000001 0.19999984 1.000000119209 0.19999984 0 0.14999984
		 0.1 0.14999984 0.2 0.14999984 0.30000001 0.14999984 0.40000001 0.14999984 0.5 0.14999984
		 0.60000002 0.14999984 0.70000005 0.14999984 0.80000007 0.14999984 0.9000001 0.14999984
		 1.000000119209 0.14999984 0 0.099999845 0.1 0.099999845 0.2 0.099999845 0.30000001
		 0.099999845 0.40000001 0.099999845 0.5 0.099999845 0.60000002 0.099999845 0.70000005
		 0.099999845 0.80000007 0.099999845 0.9000001 0.099999845 1.000000119209 0.099999845
		 0 0.049999844 0.1 0.049999844 0.2 0.049999844 0.30000001 0.049999844 0.40000001 0.049999844
		 0.5 0.049999844 0.60000002 0.049999844 0.70000005 0.049999844 0.80000007 0.049999844
		 0.9000001 0.049999844 1.000000119209 0.049999844 0 -1.5646219e-07 0.1 -1.5646219e-07
		 0.2 -1.5646219e-07 0.30000001 -1.5646219e-07 0.40000001 -1.5646219e-07 0.5 -1.5646219e-07
		 0.60000002 -1.5646219e-07 0.70000005 -1.5646219e-07 0.80000007 -1.5646219e-07 0.9000001
		 -1.5646219e-07 1.000000119209 -1.5646219e-07;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 200 ".vt";
	setAttr ".vt[0:165]"  0.72811532 0 -0.52900684 0.27811524 0 -0.85595101 -0.27811542 0 -0.85595089
		 -0.72811544 0 -0.52900672 -0.9000001 0 5.3644179e-08 -0.72811532 0 0.52900684 -0.27811527 0 0.85595089
		 0.27811533 0 0.85595089 0.72811526 0 0.52900672 0.89999998 0 0 0.73207498 0.0309017 -0.53188372
		 0.27962768 0.0309017 -0.86060584 -0.27962789 0.0309017 -0.86060572 -0.7320751 0.0309017 -0.53188354
		 -0.90489447 0.0309017 5.3935906e-08 -0.73207498 0.0309017 0.53188366 -0.27962771 0.0309017 0.86060572
		 0.27962777 0.0309017 0.86060572 0.73207492 0.0309017 0.53188354 0.90489435 0.0309017 0
		 0.74356622 0.058778532 -0.5402326 0.28401697 0.058778532 -0.87411463 -0.28401715 0.058778532 -0.87411451
		 -0.74356633 0.058778532 -0.54023242 -0.91909844 0.058778532 5.4782529e-08 -0.74356622 0.058778532 0.54023254
		 -0.28401697 0.058778532 0.87411451 0.28401703 0.058778532 0.87411445 0.74356616 0.058778532 0.54023242
		 0.91909832 0.058778532 0 0.76146424 0.080901705 -0.55323625 0.29085338 0.080901705 -0.89515501
		 -0.29085359 0.080901705 -0.89515489 -0.76146436 0.080901705 -0.55323607 -0.94122159 0.080901705 5.6101172e-08
		 -0.76146424 0.080901705 0.55323619 -0.29085341 0.080901705 0.89515489 0.29085347 0.080901705 0.89515483
		 0.76146418 0.080901705 0.55323607 0.94122148 0.080901705 0 0.78401709 0.095105663 -0.56962186
		 0.2994678 0.095105663 -0.92166746 -0.29946801 0.095105663 -0.92166734 -0.78401721 0.095105663 -0.56962168
		 -0.96909845 0.095105663 5.7762762e-08 -0.78401709 0.095105663 0.5696218 -0.29946783 0.095105663 0.92166734
		 0.29946789 0.095105663 0.92166728 0.78401703 0.095105663 0.56962168 0.96909833 0.095105663 0
		 0.80901706 0.10000002 -0.58778542 0.30901694 0.10000002 -0.95105672 -0.30901715 0.10000002 -0.9510566
		 -0.80901718 0.10000002 -0.58778524 -1.000000119209 0.10000002 5.9604645e-08 -0.80901706 0.10000002 0.58778536
		 -0.30901697 0.10000002 0.9510566 0.30901703 0.10000002 0.95105654 0.809017 0.10000002 0.58778524
		 1 0.10000002 0 0.83401704 0.09510567 -0.60594898 0.31856608 0.09510567 -0.98044598
		 -0.31856629 0.09510567 -0.98044586 -0.83401716 0.09510567 -0.60594881 -1.03090179 0.09510567 6.1446528e-08
		 -0.83401704 0.09510567 0.60594893 -0.31856611 0.09510567 0.98044586 0.31856617 0.09510567 0.9804458
		 0.83401698 0.09510567 0.60594881 1.03090167 0.09510567 0 0.85656989 0.080901712 -0.6223346
		 0.3271805 0.080901712 -1.0069584846 -0.32718071 0.080901712 -1.0069582462 -0.85657001 0.080901712 -0.62233442
		 -1.058778644 0.080901712 6.3108118e-08 -0.85656989 0.080901712 0.62233454 -0.32718053 0.080901712 1.0069582462
		 0.32718059 0.080901712 1.0069582462 0.85656983 0.080901712 0.62233442 1.058778524 0.080901712 0
		 0.87446797 0.058778536 -0.63533831 0.33401695 0.058778536 -1.027998805 -0.33401719 0.058778536 -1.027998686
		 -0.87446809 0.058778536 -0.63533807 -1.080901861 0.058778536 6.4426764e-08 -0.87446797 0.058778536 0.63533825
		 -0.33401698 0.058778536 1.027998686 0.33401704 0.058778536 1.027998686 0.87446791 0.058778536 0.63533807
		 1.080901742 0.058778536 0 0.88595915 0.030901706 -0.64368713 0.33840621 0.030901706 -1.041507602
		 -0.33840641 0.030901706 -1.041507483 -0.88595927 0.030901706 -0.64368695 -1.095105767 0.030901706 6.5273383e-08
		 -0.88595915 0.030901706 0.64368707 -0.33840623 0.030901706 1.041507483 0.33840629 0.030901706 1.041507363
		 0.88595909 0.030901706 0.64368695 1.095105648 0.030901706 0 0.8899188 0 -0.64656401
		 0.33991864 0 -1.046162367 -0.33991888 0 -1.046162248 -0.88991892 0 -0.64656377 -1.10000014 0 6.5565111e-08
		 -0.8899188 0 0.64656389 -0.33991867 0 1.046162248 0.33991873 0 1.046162248 0.88991874 0 0.64656377
		 1.10000002 0 0 0.88595915 -0.030901706 -0.64368713 0.33840621 -0.030901706 -1.041507602
		 -0.33840641 -0.030901706 -1.041507483 -0.88595927 -0.030901706 -0.64368695 -1.095105767 -0.030901706 6.5273383e-08
		 -0.88595915 -0.030901706 0.64368707 -0.33840623 -0.030901706 1.041507483 0.33840629 -0.030901706 1.041507363
		 0.88595909 -0.030901706 0.64368695 1.095105648 -0.030901706 0 0.87446797 -0.058778543 -0.63533831
		 0.33401695 -0.058778543 -1.027998805 -0.33401719 -0.058778543 -1.027998686 -0.87446809 -0.058778543 -0.63533807
		 -1.080901861 -0.058778543 6.4426764e-08 -0.87446797 -0.058778543 0.63533825 -0.33401698 -0.058778543 1.027998686
		 0.33401704 -0.058778543 1.027998686 0.87446791 -0.058778543 0.63533807 1.080901742 -0.058778543 0
		 0.85656989 -0.080901735 -0.6223346 0.3271805 -0.080901735 -1.0069584846 -0.32718071 -0.080901735 -1.0069582462
		 -0.85657001 -0.080901735 -0.62233442 -1.058778644 -0.080901735 6.3108118e-08 -0.85656989 -0.080901735 0.62233454
		 -0.32718053 -0.080901735 1.0069582462 0.32718059 -0.080901735 1.0069582462 0.85656983 -0.080901735 0.62233442
		 1.058778524 -0.080901735 0 0.83401704 -0.0951057 -0.60594898 0.31856608 -0.0951057 -0.98044598
		 -0.31856629 -0.0951057 -0.98044586 -0.83401716 -0.0951057 -0.60594881 -1.03090179 -0.0951057 6.1446528e-08
		 -0.83401704 -0.0951057 0.60594893 -0.31856611 -0.0951057 0.98044586 0.31856617 -0.0951057 0.9804458
		 0.83401698 -0.0951057 0.60594881 1.03090167 -0.0951057 0 0.80901706 -0.10000005 -0.58778542
		 0.30901694 -0.10000005 -0.95105672 -0.30901715 -0.10000005 -0.9510566 -0.80901718 -0.10000005 -0.58778524
		 -1.000000119209 -0.10000005 5.9604645e-08 -0.80901706 -0.10000005 0.58778536 -0.30901697 -0.10000005 0.9510566
		 0.30901703 -0.10000005 0.95105654 0.809017 -0.10000005 0.58778524 1 -0.10000005 0
		 0.78401703 -0.0951057 -0.56962186 0.29946777 -0.0951057 -0.9216674 -0.29946798 -0.0951057 -0.92166728
		 -0.78401715 -0.0951057 -0.56962168 -0.96909839 -0.0951057 5.7762758e-08 -0.78401703 -0.0951057 0.5696218;
	setAttr ".vt[166:199]" -0.2994678 -0.0951057 0.92166728 0.29946786 -0.0951057 0.92166722
		 0.78401697 -0.0951057 0.56962168 0.96909827 -0.0951057 0 0.76146418 -0.080901749 -0.55323625
		 0.29085335 -0.080901749 -0.89515495 -0.29085356 -0.080901749 -0.89515483 -0.7614643 -0.080901749 -0.55323607
		 -0.94122154 -0.080901749 5.6101168e-08 -0.76146418 -0.080901749 0.55323619 -0.29085338 -0.080901749 0.89515483
		 0.29085344 -0.080901749 0.89515477 0.76146412 -0.080901749 0.55323607 0.94122142 -0.080901749 0
		 0.74356616 -0.058778562 -0.54023254 0.28401694 -0.058778562 -0.87411457 -0.28401712 -0.058778562 -0.87411445
		 -0.74356627 -0.058778562 -0.54023242 -0.91909838 -0.058778562 5.4782525e-08 -0.74356616 -0.058778562 0.54023248
		 -0.28401697 -0.058778562 0.87411445 0.28401703 -0.058778562 0.87411439 0.7435661 -0.058778562 0.54023242
		 0.91909826 -0.058778562 0 0.73207492 -0.030901719 -0.53188366 0.27962768 -0.030901719 -0.86060578
		 -0.27962786 -0.030901719 -0.86060572 -0.73207504 -0.030901719 -0.53188354 -0.90489441 -0.030901719 5.3935903e-08
		 -0.73207492 -0.030901719 0.5318836 -0.27962768 -0.030901719 0.86060572 0.27962774 -0.030901719 0.86060566
		 0.73207486 -0.030901719 0.53188354 0.90489429 -0.030901719 0;
	setAttr -s 400 ".ed";
	setAttr ".ed[0:165]"  0 1 1 1 2 1 2 3 1 3 4 1 4 5 1 5 6 1 6 7 1 7 8 1 8 9 1
		 9 0 1 10 11 1 11 12 1 12 13 1 13 14 1 14 15 1 15 16 1 16 17 1 17 18 1 18 19 1 19 10 1
		 20 21 1 21 22 1 22 23 1 23 24 1 24 25 1 25 26 1 26 27 1 27 28 1 28 29 1 29 20 1 30 31 1
		 31 32 1 32 33 1 33 34 1 34 35 1 35 36 1 36 37 1 37 38 1 38 39 1 39 30 1 40 41 1 41 42 1
		 42 43 1 43 44 1 44 45 1 45 46 1 46 47 1 47 48 1 48 49 1 49 40 1 50 51 1 51 52 1 52 53 1
		 53 54 1 54 55 1 55 56 1 56 57 1 57 58 1 58 59 1 59 50 1 60 61 1 61 62 1 62 63 1 63 64 1
		 64 65 1 65 66 1 66 67 1 67 68 1 68 69 1 69 60 1 70 71 1 71 72 1 72 73 1 73 74 1 74 75 1
		 75 76 1 76 77 1 77 78 1 78 79 1 79 70 1 80 81 1 81 82 1 82 83 1 83 84 1 84 85 1 85 86 1
		 86 87 1 87 88 1 88 89 1 89 80 1 90 91 1 91 92 1 92 93 1 93 94 1 94 95 1 95 96 1 96 97 1
		 97 98 1 98 99 1 99 90 1 100 101 1 101 102 1 102 103 1 103 104 1 104 105 1 105 106 1
		 106 107 1 107 108 1 108 109 1 109 100 1 110 111 1 111 112 1 112 113 1 113 114 1 114 115 1
		 115 116 1 116 117 1 117 118 1 118 119 1 119 110 1 120 121 1 121 122 1 122 123 1 123 124 1
		 124 125 1 125 126 1 126 127 1 127 128 1 128 129 1 129 120 1 130 131 1 131 132 1 132 133 1
		 133 134 1 134 135 1 135 136 1 136 137 1 137 138 1 138 139 1 139 130 1 140 141 1 141 142 1
		 142 143 1 143 144 1 144 145 1 145 146 1 146 147 1 147 148 1 148 149 1 149 140 1 150 151 1
		 151 152 1 152 153 1 153 154 1 154 155 1 155 156 1 156 157 1 157 158 1 158 159 1 159 150 1
		 160 161 1 161 162 1 162 163 1 163 164 1 164 165 1 165 166 1;
	setAttr ".ed[166:331]" 166 167 1 167 168 1 168 169 1 169 160 1 170 171 1 171 172 1
		 172 173 1 173 174 1 174 175 1 175 176 1 176 177 1 177 178 1 178 179 1 179 170 1 180 181 1
		 181 182 1 182 183 1 183 184 1 184 185 1 185 186 1 186 187 1 187 188 1 188 189 1 189 180 1
		 190 191 1 191 192 1 192 193 1 193 194 1 194 195 1 195 196 1 196 197 1 197 198 1 198 199 1
		 199 190 1 0 10 0 1 11 0 2 12 0 3 13 0 4 14 0 5 15 0 6 16 0 7 17 0 8 18 0 9 19 0 10 20 0
		 11 21 0 12 22 0 13 23 0 14 24 0 15 25 0 16 26 0 17 27 0 18 28 0 19 29 0 20 30 0 21 31 0
		 22 32 0 23 33 0 24 34 0 25 35 0 26 36 0 27 37 0 28 38 0 29 39 0 30 40 0 31 41 0 32 42 0
		 33 43 0 34 44 0 35 45 0 36 46 0 37 47 0 38 48 0 39 49 0 40 50 0 41 51 0 42 52 0 43 53 0
		 44 54 0 45 55 0 46 56 0 47 57 0 48 58 0 49 59 0 50 60 0 51 61 0 52 62 0 53 63 0 54 64 0
		 55 65 0 56 66 0 57 67 0 58 68 0 59 69 0 60 70 0 61 71 0 62 72 0 63 73 0 64 74 0 65 75 0
		 66 76 0 67 77 0 68 78 0 69 79 0 70 80 0 71 81 0 72 82 0 73 83 0 74 84 0 75 85 0 76 86 0
		 77 87 0 78 88 0 79 89 0 80 90 0 81 91 0 82 92 0 83 93 0 84 94 0 85 95 0 86 96 0 87 97 0
		 88 98 0 89 99 0 90 100 0 91 101 0 92 102 0 93 103 0 94 104 0 95 105 0 96 106 0 97 107 0
		 98 108 0 99 109 0 100 110 0 101 111 0 102 112 0 103 113 0 104 114 0 105 115 0 106 116 0
		 107 117 0 108 118 0 109 119 0 110 120 0 111 121 0 112 122 0 113 123 0 114 124 0 115 125 0
		 116 126 0 117 127 0 118 128 0 119 129 0 120 130 0 121 131 0 122 132 0 123 133 0 124 134 0
		 125 135 0 126 136 0 127 137 0 128 138 0 129 139 0 130 140 0 131 141 0;
	setAttr ".ed[332:399]" 132 142 0 133 143 0 134 144 0 135 145 0 136 146 0 137 147 0
		 138 148 0 139 149 0 140 150 0 141 151 0 142 152 0 143 153 0 144 154 0 145 155 0 146 156 0
		 147 157 0 148 158 0 149 159 0 150 160 0 151 161 0 152 162 0 153 163 0 154 164 0 155 165 0
		 156 166 0 157 167 0 158 168 0 159 169 0 160 170 0 161 171 0 162 172 0 163 173 0 164 174 0
		 165 175 0 166 176 0 167 177 0 168 178 0 169 179 0 170 180 0 171 181 0 172 182 0 173 183 0
		 174 184 0 175 185 0 176 186 0 177 187 0 178 188 0 179 189 0 180 190 0 181 191 0 182 192 0
		 183 193 0 184 194 0 185 195 0 186 196 0 187 197 0 188 198 0 189 199 0 190 0 0 191 1 0
		 192 2 0 193 3 0 194 4 0 195 5 0 196 6 0 197 7 0 198 8 0 199 9 0;
	setAttr -s 200 -ch 800 ".fc[0:199]" -type "polyFaces" 
		f 4 -1 200 10 -202
		mu 0 4 1 0 11 12
		f 4 -2 201 11 -203
		mu 0 4 2 1 12 13
		f 4 -3 202 12 -204
		mu 0 4 3 2 13 14
		f 4 -4 203 13 -205
		mu 0 4 4 3 14 15
		f 4 -5 204 14 -206
		mu 0 4 5 4 15 16
		f 4 -6 205 15 -207
		mu 0 4 6 5 16 17
		f 4 -7 206 16 -208
		mu 0 4 7 6 17 18
		f 4 -8 207 17 -209
		mu 0 4 8 7 18 19
		f 4 -9 208 18 -210
		mu 0 4 9 8 19 20
		f 4 -10 209 19 -201
		mu 0 4 10 9 20 21
		f 4 -11 210 20 -212
		mu 0 4 12 11 22 23
		f 4 -12 211 21 -213
		mu 0 4 13 12 23 24
		f 4 -13 212 22 -214
		mu 0 4 14 13 24 25
		f 4 -14 213 23 -215
		mu 0 4 15 14 25 26
		f 4 -15 214 24 -216
		mu 0 4 16 15 26 27
		f 4 -16 215 25 -217
		mu 0 4 17 16 27 28
		f 4 -17 216 26 -218
		mu 0 4 18 17 28 29
		f 4 -18 217 27 -219
		mu 0 4 19 18 29 30
		f 4 -19 218 28 -220
		mu 0 4 20 19 30 31
		f 4 -20 219 29 -211
		mu 0 4 21 20 31 32
		f 4 -21 220 30 -222
		mu 0 4 23 22 33 34
		f 4 -22 221 31 -223
		mu 0 4 24 23 34 35
		f 4 -23 222 32 -224
		mu 0 4 25 24 35 36
		f 4 -24 223 33 -225
		mu 0 4 26 25 36 37
		f 4 -25 224 34 -226
		mu 0 4 27 26 37 38
		f 4 -26 225 35 -227
		mu 0 4 28 27 38 39
		f 4 -27 226 36 -228
		mu 0 4 29 28 39 40
		f 4 -28 227 37 -229
		mu 0 4 30 29 40 41
		f 4 -29 228 38 -230
		mu 0 4 31 30 41 42
		f 4 -30 229 39 -221
		mu 0 4 32 31 42 43
		f 4 -31 230 40 -232
		mu 0 4 34 33 44 45
		f 4 -32 231 41 -233
		mu 0 4 35 34 45 46
		f 4 -33 232 42 -234
		mu 0 4 36 35 46 47
		f 4 -34 233 43 -235
		mu 0 4 37 36 47 48
		f 4 -35 234 44 -236
		mu 0 4 38 37 48 49
		f 4 -36 235 45 -237
		mu 0 4 39 38 49 50
		f 4 -37 236 46 -238
		mu 0 4 40 39 50 51
		f 4 -38 237 47 -239
		mu 0 4 41 40 51 52
		f 4 -39 238 48 -240
		mu 0 4 42 41 52 53
		f 4 -40 239 49 -231
		mu 0 4 43 42 53 54
		f 4 -41 240 50 -242
		mu 0 4 45 44 55 56
		f 4 -42 241 51 -243
		mu 0 4 46 45 56 57
		f 4 -43 242 52 -244
		mu 0 4 47 46 57 58
		f 4 -44 243 53 -245
		mu 0 4 48 47 58 59
		f 4 -45 244 54 -246
		mu 0 4 49 48 59 60
		f 4 -46 245 55 -247
		mu 0 4 50 49 60 61
		f 4 -47 246 56 -248
		mu 0 4 51 50 61 62
		f 4 -48 247 57 -249
		mu 0 4 52 51 62 63
		f 4 -49 248 58 -250
		mu 0 4 53 52 63 64
		f 4 -50 249 59 -241
		mu 0 4 54 53 64 65
		f 4 -51 250 60 -252
		mu 0 4 56 55 66 67
		f 4 -52 251 61 -253
		mu 0 4 57 56 67 68
		f 4 -53 252 62 -254
		mu 0 4 58 57 68 69
		f 4 -54 253 63 -255
		mu 0 4 59 58 69 70
		f 4 -55 254 64 -256
		mu 0 4 60 59 70 71
		f 4 -56 255 65 -257
		mu 0 4 61 60 71 72
		f 4 -57 256 66 -258
		mu 0 4 62 61 72 73
		f 4 -58 257 67 -259
		mu 0 4 63 62 73 74
		f 4 -59 258 68 -260
		mu 0 4 64 63 74 75
		f 4 -60 259 69 -251
		mu 0 4 65 64 75 76
		f 4 -61 260 70 -262
		mu 0 4 67 66 77 78
		f 4 -62 261 71 -263
		mu 0 4 68 67 78 79
		f 4 -63 262 72 -264
		mu 0 4 69 68 79 80
		f 4 -64 263 73 -265
		mu 0 4 70 69 80 81
		f 4 -65 264 74 -266
		mu 0 4 71 70 81 82
		f 4 -66 265 75 -267
		mu 0 4 72 71 82 83
		f 4 -67 266 76 -268
		mu 0 4 73 72 83 84
		f 4 -68 267 77 -269
		mu 0 4 74 73 84 85
		f 4 -69 268 78 -270
		mu 0 4 75 74 85 86
		f 4 -70 269 79 -261
		mu 0 4 76 75 86 87
		f 4 -71 270 80 -272
		mu 0 4 78 77 88 89
		f 4 -72 271 81 -273
		mu 0 4 79 78 89 90
		f 4 -73 272 82 -274
		mu 0 4 80 79 90 91
		f 4 -74 273 83 -275
		mu 0 4 81 80 91 92
		f 4 -75 274 84 -276
		mu 0 4 82 81 92 93
		f 4 -76 275 85 -277
		mu 0 4 83 82 93 94
		f 4 -77 276 86 -278
		mu 0 4 84 83 94 95
		f 4 -78 277 87 -279
		mu 0 4 85 84 95 96
		f 4 -79 278 88 -280
		mu 0 4 86 85 96 97
		f 4 -80 279 89 -271
		mu 0 4 87 86 97 98
		f 4 -81 280 90 -282
		mu 0 4 89 88 99 100
		f 4 -82 281 91 -283
		mu 0 4 90 89 100 101
		f 4 -83 282 92 -284
		mu 0 4 91 90 101 102
		f 4 -84 283 93 -285
		mu 0 4 92 91 102 103
		f 4 -85 284 94 -286
		mu 0 4 93 92 103 104
		f 4 -86 285 95 -287
		mu 0 4 94 93 104 105
		f 4 -87 286 96 -288
		mu 0 4 95 94 105 106
		f 4 -88 287 97 -289
		mu 0 4 96 95 106 107
		f 4 -89 288 98 -290
		mu 0 4 97 96 107 108
		f 4 -90 289 99 -281
		mu 0 4 98 97 108 109
		f 4 -91 290 100 -292
		mu 0 4 100 99 110 111
		f 4 -92 291 101 -293
		mu 0 4 101 100 111 112
		f 4 -93 292 102 -294
		mu 0 4 102 101 112 113
		f 4 -94 293 103 -295
		mu 0 4 103 102 113 114
		f 4 -95 294 104 -296
		mu 0 4 104 103 114 115
		f 4 -96 295 105 -297
		mu 0 4 105 104 115 116
		f 4 -97 296 106 -298
		mu 0 4 106 105 116 117
		f 4 -98 297 107 -299
		mu 0 4 107 106 117 118
		f 4 -99 298 108 -300
		mu 0 4 108 107 118 119
		f 4 -100 299 109 -291
		mu 0 4 109 108 119 120
		f 4 -101 300 110 -302
		mu 0 4 111 110 121 122
		f 4 -102 301 111 -303
		mu 0 4 112 111 122 123
		f 4 -103 302 112 -304
		mu 0 4 113 112 123 124
		f 4 -104 303 113 -305
		mu 0 4 114 113 124 125
		f 4 -105 304 114 -306
		mu 0 4 115 114 125 126
		f 4 -106 305 115 -307
		mu 0 4 116 115 126 127
		f 4 -107 306 116 -308
		mu 0 4 117 116 127 128
		f 4 -108 307 117 -309
		mu 0 4 118 117 128 129
		f 4 -109 308 118 -310
		mu 0 4 119 118 129 130
		f 4 -110 309 119 -301
		mu 0 4 120 119 130 131
		f 4 -111 310 120 -312
		mu 0 4 122 121 132 133
		f 4 -112 311 121 -313
		mu 0 4 123 122 133 134
		f 4 -113 312 122 -314
		mu 0 4 124 123 134 135
		f 4 -114 313 123 -315
		mu 0 4 125 124 135 136
		f 4 -115 314 124 -316
		mu 0 4 126 125 136 137
		f 4 -116 315 125 -317
		mu 0 4 127 126 137 138
		f 4 -117 316 126 -318
		mu 0 4 128 127 138 139
		f 4 -118 317 127 -319
		mu 0 4 129 128 139 140
		f 4 -119 318 128 -320
		mu 0 4 130 129 140 141
		f 4 -120 319 129 -311
		mu 0 4 131 130 141 142
		f 4 -121 320 130 -322
		mu 0 4 133 132 143 144
		f 4 -122 321 131 -323
		mu 0 4 134 133 144 145
		f 4 -123 322 132 -324
		mu 0 4 135 134 145 146
		f 4 -124 323 133 -325
		mu 0 4 136 135 146 147
		f 4 -125 324 134 -326
		mu 0 4 137 136 147 148
		f 4 -126 325 135 -327
		mu 0 4 138 137 148 149
		f 4 -127 326 136 -328
		mu 0 4 139 138 149 150
		f 4 -128 327 137 -329
		mu 0 4 140 139 150 151
		f 4 -129 328 138 -330
		mu 0 4 141 140 151 152
		f 4 -130 329 139 -321
		mu 0 4 142 141 152 153
		f 4 -131 330 140 -332
		mu 0 4 144 143 154 155
		f 4 -132 331 141 -333
		mu 0 4 145 144 155 156
		f 4 -133 332 142 -334
		mu 0 4 146 145 156 157
		f 4 -134 333 143 -335
		mu 0 4 147 146 157 158
		f 4 -135 334 144 -336
		mu 0 4 148 147 158 159
		f 4 -136 335 145 -337
		mu 0 4 149 148 159 160
		f 4 -137 336 146 -338
		mu 0 4 150 149 160 161
		f 4 -138 337 147 -339
		mu 0 4 151 150 161 162
		f 4 -139 338 148 -340
		mu 0 4 152 151 162 163
		f 4 -140 339 149 -331
		mu 0 4 153 152 163 164
		f 4 -141 340 150 -342
		mu 0 4 155 154 165 166
		f 4 -142 341 151 -343
		mu 0 4 156 155 166 167
		f 4 -143 342 152 -344
		mu 0 4 157 156 167 168
		f 4 -144 343 153 -345
		mu 0 4 158 157 168 169
		f 4 -145 344 154 -346
		mu 0 4 159 158 169 170
		f 4 -146 345 155 -347
		mu 0 4 160 159 170 171
		f 4 -147 346 156 -348
		mu 0 4 161 160 171 172
		f 4 -148 347 157 -349
		mu 0 4 162 161 172 173
		f 4 -149 348 158 -350
		mu 0 4 163 162 173 174
		f 4 -150 349 159 -341
		mu 0 4 164 163 174 175
		f 4 -151 350 160 -352
		mu 0 4 166 165 176 177
		f 4 -152 351 161 -353
		mu 0 4 167 166 177 178
		f 4 -153 352 162 -354
		mu 0 4 168 167 178 179
		f 4 -154 353 163 -355
		mu 0 4 169 168 179 180
		f 4 -155 354 164 -356
		mu 0 4 170 169 180 181
		f 4 -156 355 165 -357
		mu 0 4 171 170 181 182
		f 4 -157 356 166 -358
		mu 0 4 172 171 182 183
		f 4 -158 357 167 -359
		mu 0 4 173 172 183 184
		f 4 -159 358 168 -360
		mu 0 4 174 173 184 185
		f 4 -160 359 169 -351
		mu 0 4 175 174 185 186
		f 4 -161 360 170 -362
		mu 0 4 177 176 187 188
		f 4 -162 361 171 -363
		mu 0 4 178 177 188 189
		f 4 -163 362 172 -364
		mu 0 4 179 178 189 190
		f 4 -164 363 173 -365
		mu 0 4 180 179 190 191
		f 4 -165 364 174 -366
		mu 0 4 181 180 191 192
		f 4 -166 365 175 -367
		mu 0 4 182 181 192 193
		f 4 -167 366 176 -368
		mu 0 4 183 182 193 194
		f 4 -168 367 177 -369
		mu 0 4 184 183 194 195
		f 4 -169 368 178 -370
		mu 0 4 185 184 195 196
		f 4 -170 369 179 -361
		mu 0 4 186 185 196 197
		f 4 -171 370 180 -372
		mu 0 4 188 187 198 199
		f 4 -172 371 181 -373
		mu 0 4 189 188 199 200
		f 4 -173 372 182 -374
		mu 0 4 190 189 200 201
		f 4 -174 373 183 -375
		mu 0 4 191 190 201 202
		f 4 -175 374 184 -376
		mu 0 4 192 191 202 203
		f 4 -176 375 185 -377
		mu 0 4 193 192 203 204
		f 4 -177 376 186 -378
		mu 0 4 194 193 204 205
		f 4 -178 377 187 -379
		mu 0 4 195 194 205 206
		f 4 -179 378 188 -380
		mu 0 4 196 195 206 207
		f 4 -180 379 189 -371
		mu 0 4 197 196 207 208
		f 4 -181 380 190 -382
		mu 0 4 199 198 209 210
		f 4 -182 381 191 -383
		mu 0 4 200 199 210 211
		f 4 -183 382 192 -384
		mu 0 4 201 200 211 212
		f 4 -184 383 193 -385
		mu 0 4 202 201 212 213
		f 4 -185 384 194 -386
		mu 0 4 203 202 213 214
		f 4 -186 385 195 -387
		mu 0 4 204 203 214 215
		f 4 -187 386 196 -388
		mu 0 4 205 204 215 216
		f 4 -188 387 197 -389
		mu 0 4 206 205 216 217
		f 4 -189 388 198 -390
		mu 0 4 207 206 217 218
		f 4 -190 389 199 -381
		mu 0 4 208 207 218 219
		f 4 -191 390 0 -392
		mu 0 4 210 209 220 221
		f 4 -192 391 1 -393
		mu 0 4 211 210 221 222
		f 4 -193 392 2 -394
		mu 0 4 212 211 222 223
		f 4 -194 393 3 -395
		mu 0 4 213 212 223 224
		f 4 -195 394 4 -396
		mu 0 4 214 213 224 225
		f 4 -196 395 5 -397
		mu 0 4 215 214 225 226
		f 4 -197 396 6 -398
		mu 0 4 216 215 226 227
		f 4 -198 397 7 -399
		mu 0 4 217 216 227 228
		f 4 -199 398 8 -400
		mu 0 4 218 217 228 229
		f 4 -200 399 9 -391
		mu 0 4 219 218 229 230;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube15";
	rename -uid "5A518CD4-8146-8369-679C-1B8D3627D1D7";
	setAttr ".t" -type "double3" -0.026296682932929971 2.498732193637224 0.69704380263482224 ;
	setAttr ".r" -type "double3" -2.1380358186785049 -2.6298509894453943 -50.866451340631947 ;
	setAttr ".s" -type "double3" 2.330169535243269 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform16" -p "pCube15";
	rename -uid "F7583553-6D4C-0D10-B6EF-6CB08F16D9BE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape15" -p "transform16";
	rename -uid "09E53E51-3A4F-DCCF-3BCE-8D8582BCF9C5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube16";
	rename -uid "8A7AD00B-7647-0BDB-C4EE-3D9C7D892354";
	setAttr ".t" -type "double3" 0.049329453262231704 2.5096272565107576 -0.84618483608526518 ;
	setAttr ".r" -type "double3" 1.0082339920127206 1.2410608874274978 -50.904612914966847 ;
	setAttr ".s" -type "double3" 2.2831646159945462 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform15" -p "pCube16";
	rename -uid "69E93289-9C4C-BEBD-53C6-E794C0CEE566";
	setAttr ".v" no;
createNode mesh -n "pCubeShape16" -p "transform15";
	rename -uid "3DB1F5D2-F84B-C48F-52A2-7C88E28F0326";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube17";
	rename -uid "8D53234D-7347-73B2-512C-478A6922B612";
	setAttr ".t" -type "double3" -0.10129586358492959 2.498732193637224 0.7404904233180023 ;
	setAttr ".r" -type "double3" -2.1380358186785169 -2.6298509894453832 -130.5257682258588 ;
	setAttr ".s" -type "double3" 2.2867319063607781 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform14" -p "pCube17";
	rename -uid "FCB03864-E243-14F5-1220-3C8C643BC03F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape17" -p "transform14";
	rename -uid "38CAD865-F248-CB81-32CE-178DB70C6BA5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube18";
	rename -uid "ABD9368F-A548-B5C6-BD4E-FDB3AEEDBDE7";
	setAttr ".t" -type "double3" -0.10129586358492959 2.498732193637224 -0.82924685564887979 ;
	setAttr ".r" -type "double3" -0.91243293890275279 -1.0630184564700611 -130.15258009251093 ;
	setAttr ".s" -type "double3" 2.2867319063607781 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform13" -p "pCube18";
	rename -uid "2D20C3D5-2343-B9CC-D87A-978D7DAB6200";
	setAttr ".v" no;
createNode mesh -n "pCubeShape18" -p "transform13";
	rename -uid "BA0FAABB-A242-C220-14FD-AEA910EE8562";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube19";
	rename -uid "1F2E46FA-F849-7E8A-D502-C0A678FB2E10";
	setAttr ".t" -type "double3" 0.024307685324365558 4.3125475494611063 -0.76640915339742643 ;
	setAttr ".r" -type "double3" 1.0082339920127206 1.2410608874274978 -50.904612914966847 ;
	setAttr ".s" -type "double3" 2.0312832108178211 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform12" -p "pCube19";
	rename -uid "0B8EDE53-0C44-4190-660B-1BA66DB0E39E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape19" -p "transform12";
	rename -uid "476CB3B9-E940-EA52-7E89-24849782F537";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube20";
	rename -uid "B46D2411-FD42-7E60-9D21-B39F7972848E";
	setAttr ".t" -type "double3" -0.10129586358492959 4.3016524865875727 -0.75954563044222156 ;
	setAttr ".r" -type "double3" -0.91243293890275279 -1.0630184564700611 -130.15258009251093 ;
	setAttr ".s" -type "double3" 1.9715181330848024 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform11" -p "pCube20";
	rename -uid "CBBA9AD3-A64F-2876-0E39-A58D70593791";
	setAttr ".v" no;
createNode mesh -n "pCubeShape20" -p "transform11";
	rename -uid "17C7C167-CF4E-93F7-0BEA-A5AAC1C77922";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube21";
	rename -uid "31F38986-3A4B-E371-2CE3-BC9D83A9C7A1";
	setAttr ".t" -type "double3" -0.10129586358492959 4.3016524865875727 0.55593137580133256 ;
	setAttr ".r" -type "double3" -0.91243293890275279 -1.0630184564700611 -130.15258009251093 ;
	setAttr ".s" -type "double3" 1.9715181330848024 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform10" -p "pCube21";
	rename -uid "CC0CBE6C-7040-2454-B0C7-ADA8B4CC3F11";
	setAttr ".v" no;
createNode mesh -n "pCubeShape21" -p "transform10";
	rename -uid "32D77554-DE41-E896-330C-9E8492DDB344";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube22";
	rename -uid "0061FC37-CD47-CE17-537E-679FBAFDD918";
	setAttr ".t" -type "double3" -0.005525338179509065 4.3125475494611063 0.57097202073704922 ;
	setAttr ".r" -type "double3" -0.93394771114528019 -1.7625011424986519 -50.58467978535986 ;
	setAttr ".s" -type "double3" 2.0312832108178211 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform9" -p "pCube22";
	rename -uid "94EFA0D5-A34E-89BD-E23C-1FA7D53A35BE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape22" -p "transform9";
	rename -uid "767A7F52-3B4F-3884-ED08-2689D1F16F31";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube23";
	rename -uid "ACF393F6-C84B-6D1E-5111-CBAA6D2D729D";
	setAttr ".t" -type "double3" -0.70775818509880184 4.2119746467946246 -0.060207429915530408 ;
	setAttr ".r" -type "double3" -97.665177770776978 -38.323044420979073 -92.057295060152555 ;
	setAttr ".s" -type "double3" 2.0312832108178211 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform8" -p "pCube23";
	rename -uid "BA786870-744B-51D3-4EC5-AE9672DBEF29";
	setAttr ".v" no;
createNode mesh -n "pCubeShape23" -p "transform8";
	rename -uid "E1EA42B3-294D-8627-719C-B6B9FFD0A7D9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube24";
	rename -uid "A1601782-2043-9F00-FACC-7399B56C041A";
	setAttr ".t" -type "double3" -0.68335995378158854 4.237686301508564 -0.1270577321717713 ;
	setAttr ".r" -type "double3" -100.21221953787033 38.212181447421166 -92.054155984798712 ;
	setAttr ".s" -type "double3" 2.0312832108178211 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform7" -p "pCube24";
	rename -uid "A592CE1A-1D4C-A586-3752-C19CEE03ED8A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape24" -p "transform7";
	rename -uid "D9D3C779-3042-1964-58AC-3CB3F08314D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube25";
	rename -uid "7C9E3CAC-1A45-283F-F2D6-4887239B331F";
	setAttr ".t" -type "double3" 0.59679249143146429 4.237686301508564 -0.1270577321717713 ;
	setAttr ".r" -type "double3" -98.571351394684314 36.934011675528325 -85.089592539209349 ;
	setAttr ".s" -type "double3" 2.0419973179112509 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform6" -p "pCube25";
	rename -uid "3EF2974B-8D4E-C2C3-641D-CFA2D922DB72";
	setAttr ".v" no;
createNode mesh -n "pCubeShape25" -p "transform6";
	rename -uid "A2FA2AA8-D84A-0D5D-97EF-C59856F37234";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube26";
	rename -uid "B63A5229-444B-18AC-B5C0-F492838CC9A5";
	setAttr ".t" -type "double3" 0.62384984829128365 4.2415504079817596 -0.060207429915530408 ;
	setAttr ".r" -type "double3" -101.29305325150429 -37.545652412787177 -86.159366697073267 ;
	setAttr ".s" -type "double3" 2.0312832108178211 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform5" -p "pCube26";
	rename -uid "0E4C8381-ED4F-583E-2A4F-E19A630843C8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape26" -p "transform5";
	rename -uid "273E45E2-5F41-512D-3D51-97B69C2D5076";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube27";
	rename -uid "438F7C67-1248-F36E-2821-D78559D63EFE";
	setAttr ".t" -type "double3" 0.73386874050987039 2.5139003356186982 -0.08662337266021429 ;
	setAttr ".r" -type "double3" -98.571351394684314 36.934011675528325 -85.089592539209349 ;
	setAttr ".s" -type "double3" 2.2450064338617426 0.15555556294020917 0.024730325552759117 ;
	setAttr ".rp" -type "double3" -4.8685786816667819e-17 1.1085653893505691e-15 3.2007710114824217e-14 ;
	setAttr ".rpt" -type "double3" 3.2889396642533393e-14 4.4991759015632508e-15 -3.5710260210390671e-14 ;
	setAttr ".spt" -type "double3" -4.8572257327350599e-17 1.1102230246251565e-15 3.1974423109204508e-14 ;
createNode transform -n "transform4" -p "pCube27";
	rename -uid "62BE9BFB-884B-69AE-BCF8-4F9FA46C67C7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape27" -p "transform4";
	rename -uid "2C21E5F2-554A-A658-E113-598BB3C40F95";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube28";
	rename -uid "FD0C8CBB-9447-36D3-CA1B-EFBE5A3C687F";
	setAttr ".t" -type "double3" 0.71933522506341607 2.5104023220135643 -0.0035276275127025247 ;
	setAttr ".r" -type "double3" -85.586485377439175 -40.188392850695635 -86.748759915389087 ;
	setAttr ".s" -type "double3" 2.3978513242567381 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform3" -p "pCube28";
	rename -uid "2A7D67AD-534D-CC40-D63A-AEB2C4FDDC5F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape28" -p "transform3";
	rename -uid "773A55D3-8049-243C-39F4-07B70DB4231A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube29";
	rename -uid "191BD8A0-7642-23AC-A9C0-B7ADD6A9DBD6";
	setAttr ".t" -type "double3" -0.80315334104911373 2.5011437665556429 -0.092526431282426144 ;
	setAttr ".r" -type "double3" -103.60299189822568 36.916883646607964 -94.30672260680754 ;
	setAttr ".s" -type "double3" 2.2450064338617426 0.15555556294020917 0.024730325552759117 ;
	setAttr ".rp" -type "double3" -4.8685786816667819e-17 1.1085653893505691e-15 3.2007710114824217e-14 ;
	setAttr ".rpt" -type "double3" 3.2889396642533393e-14 4.4991759015632508e-15 -3.5710260210390671e-14 ;
	setAttr ".spt" -type "double3" -4.8572257327350599e-17 1.1102230246251565e-15 3.1974423109204508e-14 ;
createNode transform -n "transform2" -p "pCube29";
	rename -uid "21ED8516-8646-919E-90BA-0F8A49FA2273";
	setAttr ".v" no;
createNode mesh -n "pCubeShape29" -p "transform2";
	rename -uid "46E96DE5-C846-DFA5-DE78-2AB352CC9980";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube30";
	rename -uid "CDF2A4E5-2C4A-5B23-1961-E2A8AEA6C4E7";
	setAttr ".t" -type "double3" -0.83559007370185767 2.5104114721982498 0.011685137883466368 ;
	setAttr ".r" -type "double3" -88.388200886028216 -39.977964456117185 -92.618103559155728 ;
	setAttr ".s" -type "double3" 2.3978513242567381 0.15555556294020917 0.024730325552759117 ;
createNode transform -n "transform1" -p "pCube30";
	rename -uid "197A44D6-1542-236E-D8CA-A8B959EB98B2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape30" -p "transform1";
	rename -uid "0C3B38CA-2442-6CBA-18CF-F4B3CD7A8BEC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube31";
	rename -uid "53D544F2-694C-C4C4-CE14-6DBD32DD5667";
	setAttr ".t" -type "double3" 0 0 0.01120204571105754 ;
createNode mesh -n "pCube31Shape" -p "pCube31";
	rename -uid "1840BBFD-984F-04B0-76DF-BDB908D50FEB";
	setAttr -k off ".v";
	setAttr -s 10 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "61BD2981-6C42-21EA-8792-7EA0AA3A903E";
	setAttr -s 7 ".lnk";
	setAttr -s 7 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "69816C44-F94F-2780-94F9-269BDED7FBB6";
createNode displayLayer -n "defaultLayer";
	rename -uid "BF78116C-444E-E1C6-90E5-768A70604189";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "204D794C-F642-1D70-65F4-1EB12A59206D";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "0A1A2160-1B48-079D-991F-EFB8439FD0CE";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "9FC6DC8B-B343-085A-48B9-65959ADC32B5";
	setAttr ".cuv" 1;
createNode polyCube -n "polyCube2";
	rename -uid "1981620C-464A-8BC1-5060-A4B06DEFF344";
	setAttr ".cuv" 1;
createNode polyCube -n "polyCube3";
	rename -uid "5A991555-7C47-053E-EEAD-B8A9741101AA";
	setAttr ".cuv" 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "49414926-234B-BFF1-8DDF-A9A024ED2F92";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 371\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 371\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 370\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 370\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 371\n                -height 346\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 371\n            -height 346\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 747\n                -height 738\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 747\n            -height 738\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n"
		+ "                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n"
		+ "                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n"
		+ "            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 747\\n    -height 738\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 747\\n    -height 738\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "89F99AFC-B84E-4E75-8530-C2ACF43A5530";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyPipe -n "polyPipe1";
	rename -uid "AE1B53BE-3846-E1DA-0AE3-7E824ACC1FAE";
	setAttr ".t" 0.1;
	setAttr ".sa" 10;
	setAttr ".sc" 0;
createNode polyCylinder -n "polyCylinder1";
	rename -uid "50527A90-5C46-AFCE-D377-F1BBAD2947D9";
	setAttr ".sa" 10;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode polyTorus -n "polyTorus1";
	rename -uid "C8055175-1240-2E8C-EB4B-3D8884DFA2C1";
	setAttr ".sr" 0.1;
	setAttr ".sa" 10;
createNode polyCube -n "polyCube4";
	rename -uid "81DE44F5-7D48-7176-30D7-73B0784FE4C1";
	setAttr ".cuv" 1;
createNode lambert -n "west_tower_structure";
	rename -uid "C194D12A-574E-EC52-91C5-7A9014A6AFED";
	setAttr ".c" -type "float3" 0.2543 0.1693 0.1595 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "B98D6D8B-1948-BE95-D002-D38632E80369";
	setAttr ".ihi" 0;
	setAttr -s 27 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 27 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "0916F7DA-0745-4F38-396C-7496563FE2D2";
createNode lambert -n "west_tower_details";
	rename -uid "A6832976-1E44-3B40-D1C3-0BBD124F1241";
	setAttr ".c" -type "float3" 0.2071 0.103 0.077100001 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "CFD0F78A-3C41-508A-3DF6-3A91754D334E";
	setAttr ".ihi" 0;
	setAttr -s 35 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 35 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "023CA011-C448-C9EF-6ECC-B99E40470608";
createNode lambert -n "west_tower_boiler";
	rename -uid "3B794FCE-7E47-F8B5-E125-439D41EAEB9A";
	setAttr ".c" -type "float3" 0.4709 0.1397 0.1015 ;
createNode shadingEngine -n "lambert4SG";
	rename -uid "D9E86A9B-B84B-774C-8105-6D8AC265CE3D";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo3";
	rename -uid "AEA8B844-BE48-B50D-165C-18B95534317A";
createNode lambert -n "west_tower_water";
	rename -uid "D1561AED-0046-2DA8-9172-BBA3783EB76A";
	setAttr ".c" -type "float3" 0.249392 0.43599999 0.43599999 ;
createNode shadingEngine -n "lambert5SG";
	rename -uid "4A5C2858-7946-8403-35E4-9FB50435314E";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo4";
	rename -uid "75C8419E-A142-5486-3376-13A38CB39C06";
createNode lambert -n "west_tower_boiler_details";
	rename -uid "E605CED8-314D-3A5B-73C5-1AAF0924D9D3";
	setAttr ".c" -type "float3" 0.44 0.26725051 0.2244 ;
createNode shadingEngine -n "lambert6SG";
	rename -uid "BB3064F4-3943-C10C-CE4B-F4AFD232D1E2";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo5";
	rename -uid "F8F1671D-7740-9D0D-38F6-10906B4F7B64";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "798BFBCA-A446-DB30-B71A-978154E1B6C6";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -586.91168688760058 -205.99869479748611 ;
	setAttr ".tgi[0].vh" -type "double2" 1045.2450020084045 55.9523787290332 ;
	setAttr -s 2 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -58.571430206298828;
	setAttr ".tgi[0].ni[1].nvs" 1923;
createNode polyUnite -n "polyUnite1";
	rename -uid "0EA043E6-2940-A966-A9C3-28AF4DC062B4";
	setAttr -s 34 ".ip";
	setAttr -s 34 ".im";
createNode groupId -n "groupId1";
	rename -uid "F0F8FCD5-0B46-F307-821C-08B4C47DAF2C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "0CE98050-814B-4F6B-AEE1-B8ACF4063869";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId2";
	rename -uid "ACC9D6A0-F84B-CBC3-429D-BC9EBDC5023A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "737CCD64-0340-5EA3-B4E5-60AA532A7281";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "731B0AC6-3D4B-77E0-7655-7AAB95ABE19E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "162D69CD-3047-07D7-8148-7A9D5BE05BEC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "83DC5C05-9D43-AB9C-00DE-248965CAA72C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "558ACF4A-D348-FB38-BC56-64BC338B413D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "C046FE94-7440-4C77-85CF-42A1082078E5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "3BC66B24-2F41-789B-C172-7395EED54AEF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "78F32DAF-4140-1F8B-7DD9-46853F941E71";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId10";
	rename -uid "7757F081-3640-5065-6721-318EA7F3C1A0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "0A42A9B5-6949-B890-F523-499F5C9DDE0A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "ABB5492E-714E-42CB-16D1-35A0FE8AAB22";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "E7E3D1CB-DD42-5579-9115-E49FF6D67BA5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "AC475B1F-DB45-770F-E1C0-EBBB38EF4A86";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId14";
	rename -uid "97E91119-AA42-E6B5-1A40-979A6EB56E0C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "87B26706-FA4D-42B7-9E78-3DBAF4F0F08F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "06FC70DA-0946-870F-B654-0E9C8D3C3B9C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "CDB0F542-EA4D-B3E0-8327-47ADA5CFDFA6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "E9E035D0-3243-A9BD-4281-1F875A8A0AD2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "5425F5D3-4142-2588-340A-F0854D620241";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "1504CE95-D24B-7E34-EE80-30A79FF0990D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "A2A607C1-1742-9E1A-27BC-BDA04DF7C029";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "366208FC-0D48-52E4-386C-EF960A1982D9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "9D41C3F0-7442-B104-F118-74A1C29C7E6A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "C0E7C0A4-D148-0A9F-DE78-1682DCA8A33E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "31211206-2744-F9F5-BB99-A9AB614A9C83";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "F587A4FF-A94B-6770-406C-9E9695A2E54A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "ACDE32A0-6440-1C7D-6CDE-84B113B62C39";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "526D547A-BA49-BE5B-221C-40AA6FC4AC1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "FF9894EF-C742-2659-49C4-B8BE93121115";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "BDD211EA-854D-FB47-135D-BE873E5E0528";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:39]";
createNode groupId -n "groupId30";
	rename -uid "84E0CE7F-F644-4B1E-8F51-A6AE225E3C33";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "C9487B54-E544-B4C4-9017-048BB5C69730";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "40D271EF-5A4A-F26D-0731-CBB3013D54E4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:29]";
createNode groupId -n "groupId32";
	rename -uid "CD669ED1-1C4F-5D6E-69F0-24BB90E8C666";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "E0DEFF74-B341-E1BF-1E2B-418E95B69F4C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "155B7D24-9348-938B-2B1E-9984473A2EB3";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:199]";
createNode groupId -n "groupId34";
	rename -uid "BFC3E4A5-9444-C02E-9F6A-51BD8B7CC59A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "D3B5B757-104B-297D-228D-59A615E6E1CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId36";
	rename -uid "0BCFDD46-0F4C-FFD1-8724-22A0779CB775";
	setAttr ".ihi" 0;
createNode groupId -n "groupId37";
	rename -uid "7333A5F6-4F4C-8CFE-AFFA-5EAF9B7E2BAF";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "EE88C9F8-B249-33F4-DB65-9CACC08491ED";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId38";
	rename -uid "6247AE04-6C49-5422-2A2D-5692A7AE2CE3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId39";
	rename -uid "FDC99F79-304D-971D-BD84-E9A9DF8EB71A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId40";
	rename -uid "F28FFEFB-574C-677B-36EB-3A80460DC655";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "B810FD64-4B49-39C3-C3A2-E895D0A2D4DE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "3C89060D-D349-76B4-883B-25808A285F5C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "87BEF595-7F47-6193-813A-A6AB7CC1DBA7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId44";
	rename -uid "1183EE43-274A-A686-6126-6898D2CD2B1A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "CE14F27E-1B46-085A-1BA9-00BF0655A058";
	setAttr ".ihi" 0;
createNode groupId -n "groupId46";
	rename -uid "FF17D536-4F4A-C5E1-0D64-26AD0481AB4C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId47";
	rename -uid "C68E19B3-4343-EA0F-7793-9FBB4B54B3B3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId48";
	rename -uid "9574D36F-5640-FBD0-0E95-619366C2E859";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "B880AE71-794B-91C0-8BC8-75BE7F83AA71";
	setAttr ".ihi" 0;
createNode groupId -n "groupId50";
	rename -uid "A567CB4C-5743-EEF1-8AE4-FB9D37E05134";
	setAttr ".ihi" 0;
createNode groupId -n "groupId51";
	rename -uid "995E2798-2646-5C18-2164-A28E8E81F279";
	setAttr ".ihi" 0;
createNode groupId -n "groupId52";
	rename -uid "FA7ECB91-B74A-4A8C-08F8-78815437A138";
	setAttr ".ihi" 0;
createNode groupId -n "groupId53";
	rename -uid "D46A2243-304D-72B3-2BAD-A5AA0AED5353";
	setAttr ".ihi" 0;
createNode groupId -n "groupId54";
	rename -uid "E36170DC-6E4B-AC09-2D4A-05BE3F788FC4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId55";
	rename -uid "C71AA020-9D49-BB08-1351-2D87A886944C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId56";
	rename -uid "15B984D4-FE45-C192-3E2F-339CB5DEB979";
	setAttr ".ihi" 0;
createNode groupId -n "groupId57";
	rename -uid "20BF5525-5645-F87B-FBE6-50B18A2A48A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "4C1DB272-BF42-3D74-B0CA-41B1B65DA593";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "F6065D10-E248-EFDC-ABE4-81B57B925DD3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "0DC596D1-4447-DDE0-EF48-0098801BE748";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "A8B752D0-9B42-1F7A-7184-F6B7B5A6BA44";
	setAttr ".ihi" 0;
createNode groupId -n "groupId62";
	rename -uid "4EF3DEFE-F249-D325-2C4C-F0864260F213";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "6FF84192-9947-D903-D6AF-3D9870DC7A9D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId64";
	rename -uid "ECFF4A4B-5840-EAE9-F10A-1A8F31EA235F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId65";
	rename -uid "BA39A323-A24A-4FAB-D762-ACAE929598D5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId66";
	rename -uid "E1F773DF-874B-9307-AFB7-B89186CE1C7E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "FC31A1B5-E544-75B3-789F-4DACC74B5B28";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "AD1C9ABA-714D-BAA1-4ABA-37981BBF5650";
	setAttr ".ihi" 0;
createNode groupId -n "groupId69";
	rename -uid "12B359B0-2543-DD0F-7743-F6B24A70E9EE";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "4ECB6326-D94F-21E5-6BEA-57982D35CC82";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[0:23]" "f[30:83]";
createNode groupId -n "groupId70";
	rename -uid "9BA84C80-7F41-3CCF-FE46-3BAAFC00A3DD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "D5A0BC9A-2E49-2CFC-225D-E6B11D233778";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[24:29]" "f[554:649]";
createNode groupId -n "groupId71";
	rename -uid "63BDCF49-A542-AA1F-24C5-49BEDEF17688";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "A738D80D-544E-D7BD-2C34-ECA3B85D75FC";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[84:123]";
createNode groupId -n "groupId72";
	rename -uid "D51A397F-FD40-3406-CE2B-0F937134B4A5";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "93F3E6FA-5448-65CC-8095-87B896C6D56D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[124:153]";
createNode groupId -n "groupId73";
	rename -uid "8828D440-0245-95D6-C30B-F7B7E015BEF0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "47E29CA5-B846-3CAB-E867-3FA387AA8B04";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[154:553]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 7 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 9 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupParts1.og" "pCubeShape1.i";
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pCubeShape2.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pCubeShape3.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId6.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pCubeShape4.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape4.ciog.cog[0].cgid";
connectAttr "groupParts2.og" "pCubeShape5.i";
connectAttr "groupId9.id" "pCubeShape5.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupId10.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pCubeShape6.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape6.iog.og[0].gco";
connectAttr "groupId12.id" "pCubeShape6.ciog.cog[0].cgid";
connectAttr "groupParts3.og" "pCubeShape7.i";
connectAttr "groupId13.id" "pCubeShape7.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape7.iog.og[0].gco";
connectAttr "groupId14.id" "pCubeShape7.ciog.cog[0].cgid";
connectAttr "groupId15.id" "pCubeShape8.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupId16.id" "pCubeShape8.ciog.cog[0].cgid";
connectAttr "groupId17.id" "pCubeShape9.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape9.iog.og[0].gco";
connectAttr "groupId18.id" "pCubeShape9.ciog.cog[0].cgid";
connectAttr "groupId19.id" "pCubeShape10.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape10.iog.og[0].gco";
connectAttr "groupId20.id" "pCubeShape10.ciog.cog[0].cgid";
connectAttr "groupId21.id" "pCubeShape11.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape11.iog.og[0].gco";
connectAttr "groupId22.id" "pCubeShape11.ciog.cog[0].cgid";
connectAttr "groupId23.id" "pCubeShape12.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape12.iog.og[0].gco";
connectAttr "groupId24.id" "pCubeShape12.ciog.cog[0].cgid";
connectAttr "groupId25.id" "pCubeShape13.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape13.iog.og[0].gco";
connectAttr "groupId26.id" "pCubeShape13.ciog.cog[0].cgid";
connectAttr "groupId27.id" "pCubeShape14.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape14.iog.og[0].gco";
connectAttr "groupId28.id" "pCubeShape14.ciog.cog[0].cgid";
connectAttr "groupParts4.og" "pPipeShape1.i";
connectAttr "groupId29.id" "pPipeShape1.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pPipeShape1.iog.og[0].gco";
connectAttr "groupId30.id" "pPipeShape1.ciog.cog[0].cgid";
connectAttr "groupParts5.og" "pCylinderShape1.i";
connectAttr "groupId31.id" "pCylinderShape1.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupId32.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupParts6.og" "pTorusShape1.i";
connectAttr "groupId33.id" "pTorusShape1.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "pTorusShape1.iog.og[0].gco";
connectAttr "groupId34.id" "pTorusShape1.ciog.cog[0].cgid";
connectAttr "groupId35.id" "pTorusShape2.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "pTorusShape2.iog.og[0].gco";
connectAttr "groupId36.id" "pTorusShape2.ciog.cog[0].cgid";
connectAttr "groupParts7.og" "pCubeShape15.i";
connectAttr "groupId37.id" "pCubeShape15.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape15.iog.og[0].gco";
connectAttr "groupId38.id" "pCubeShape15.ciog.cog[0].cgid";
connectAttr "groupId39.id" "pCubeShape16.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape16.iog.og[0].gco";
connectAttr "groupId40.id" "pCubeShape16.ciog.cog[0].cgid";
connectAttr "groupId41.id" "pCubeShape17.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape17.iog.og[0].gco";
connectAttr "groupId42.id" "pCubeShape17.ciog.cog[0].cgid";
connectAttr "groupId43.id" "pCubeShape18.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape18.iog.og[0].gco";
connectAttr "groupId44.id" "pCubeShape18.ciog.cog[0].cgid";
connectAttr "groupId45.id" "pCubeShape19.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape19.iog.og[0].gco";
connectAttr "groupId46.id" "pCubeShape19.ciog.cog[0].cgid";
connectAttr "groupId47.id" "pCubeShape20.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape20.iog.og[0].gco";
connectAttr "groupId48.id" "pCubeShape20.ciog.cog[0].cgid";
connectAttr "groupId49.id" "pCubeShape21.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape21.iog.og[0].gco";
connectAttr "groupId50.id" "pCubeShape21.ciog.cog[0].cgid";
connectAttr "groupId51.id" "pCubeShape22.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape22.iog.og[0].gco";
connectAttr "groupId52.id" "pCubeShape22.ciog.cog[0].cgid";
connectAttr "groupId53.id" "pCubeShape23.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape23.iog.og[0].gco";
connectAttr "groupId54.id" "pCubeShape23.ciog.cog[0].cgid";
connectAttr "groupId55.id" "pCubeShape24.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape24.iog.og[0].gco";
connectAttr "groupId56.id" "pCubeShape24.ciog.cog[0].cgid";
connectAttr "groupId57.id" "pCubeShape25.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape25.iog.og[0].gco";
connectAttr "groupId58.id" "pCubeShape25.ciog.cog[0].cgid";
connectAttr "groupId59.id" "pCubeShape26.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape26.iog.og[0].gco";
connectAttr "groupId60.id" "pCubeShape26.ciog.cog[0].cgid";
connectAttr "groupId61.id" "pCubeShape27.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape27.iog.og[0].gco";
connectAttr "groupId62.id" "pCubeShape27.ciog.cog[0].cgid";
connectAttr "groupId63.id" "pCubeShape28.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape28.iog.og[0].gco";
connectAttr "groupId64.id" "pCubeShape28.ciog.cog[0].cgid";
connectAttr "groupId65.id" "pCubeShape29.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape29.iog.og[0].gco";
connectAttr "groupId66.id" "pCubeShape29.ciog.cog[0].cgid";
connectAttr "groupId67.id" "pCubeShape30.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape30.iog.og[0].gco";
connectAttr "groupId68.id" "pCubeShape30.ciog.cog[0].cgid";
connectAttr "groupParts12.og" "pCube31Shape.i";
connectAttr "groupId69.id" "pCube31Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCube31Shape.iog.og[0].gco";
connectAttr "groupId70.id" "pCube31Shape.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pCube31Shape.iog.og[1].gco";
connectAttr "groupId71.id" "pCube31Shape.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCube31Shape.iog.og[2].gco";
connectAttr "groupId72.id" "pCube31Shape.iog.og[3].gid";
connectAttr "lambert5SG.mwc" "pCube31Shape.iog.og[3].gco";
connectAttr "groupId73.id" "pCube31Shape.iog.og[4].gid";
connectAttr "lambert6SG.mwc" "pCube31Shape.iog.og[4].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "west_tower_structure.oc" "lambert2SG.ss";
connectAttr "pCubeShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape6.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape6.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape7.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape7.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape8.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape8.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape9.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape9.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape10.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape10.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape11.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape11.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape12.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape12.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape13.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape13.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape14.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape14.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCube31Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "groupId1.msg" "lambert2SG.gn" -na;
connectAttr "groupId2.msg" "lambert2SG.gn" -na;
connectAttr "groupId3.msg" "lambert2SG.gn" -na;
connectAttr "groupId4.msg" "lambert2SG.gn" -na;
connectAttr "groupId5.msg" "lambert2SG.gn" -na;
connectAttr "groupId6.msg" "lambert2SG.gn" -na;
connectAttr "groupId7.msg" "lambert2SG.gn" -na;
connectAttr "groupId8.msg" "lambert2SG.gn" -na;
connectAttr "groupId11.msg" "lambert2SG.gn" -na;
connectAttr "groupId12.msg" "lambert2SG.gn" -na;
connectAttr "groupId13.msg" "lambert2SG.gn" -na;
connectAttr "groupId14.msg" "lambert2SG.gn" -na;
connectAttr "groupId15.msg" "lambert2SG.gn" -na;
connectAttr "groupId16.msg" "lambert2SG.gn" -na;
connectAttr "groupId17.msg" "lambert2SG.gn" -na;
connectAttr "groupId18.msg" "lambert2SG.gn" -na;
connectAttr "groupId19.msg" "lambert2SG.gn" -na;
connectAttr "groupId20.msg" "lambert2SG.gn" -na;
connectAttr "groupId21.msg" "lambert2SG.gn" -na;
connectAttr "groupId22.msg" "lambert2SG.gn" -na;
connectAttr "groupId23.msg" "lambert2SG.gn" -na;
connectAttr "groupId24.msg" "lambert2SG.gn" -na;
connectAttr "groupId25.msg" "lambert2SG.gn" -na;
connectAttr "groupId26.msg" "lambert2SG.gn" -na;
connectAttr "groupId27.msg" "lambert2SG.gn" -na;
connectAttr "groupId28.msg" "lambert2SG.gn" -na;
connectAttr "groupId69.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "west_tower_structure.msg" "materialInfo1.m";
connectAttr "west_tower_details.oc" "lambert3SG.ss";
connectAttr "pCubeShape5.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape15.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape15.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape16.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape16.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape17.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape17.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape18.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape18.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape19.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape19.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape20.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape20.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape21.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape21.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape22.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape22.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape23.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape23.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape24.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape24.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape25.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape25.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape26.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape26.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape27.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape27.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape28.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape28.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape29.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape29.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape30.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape30.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCube31Shape.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "groupId9.msg" "lambert3SG.gn" -na;
connectAttr "groupId10.msg" "lambert3SG.gn" -na;
connectAttr "groupId37.msg" "lambert3SG.gn" -na;
connectAttr "groupId38.msg" "lambert3SG.gn" -na;
connectAttr "groupId39.msg" "lambert3SG.gn" -na;
connectAttr "groupId40.msg" "lambert3SG.gn" -na;
connectAttr "groupId41.msg" "lambert3SG.gn" -na;
connectAttr "groupId42.msg" "lambert3SG.gn" -na;
connectAttr "groupId43.msg" "lambert3SG.gn" -na;
connectAttr "groupId44.msg" "lambert3SG.gn" -na;
connectAttr "groupId45.msg" "lambert3SG.gn" -na;
connectAttr "groupId46.msg" "lambert3SG.gn" -na;
connectAttr "groupId47.msg" "lambert3SG.gn" -na;
connectAttr "groupId48.msg" "lambert3SG.gn" -na;
connectAttr "groupId49.msg" "lambert3SG.gn" -na;
connectAttr "groupId50.msg" "lambert3SG.gn" -na;
connectAttr "groupId51.msg" "lambert3SG.gn" -na;
connectAttr "groupId52.msg" "lambert3SG.gn" -na;
connectAttr "groupId53.msg" "lambert3SG.gn" -na;
connectAttr "groupId54.msg" "lambert3SG.gn" -na;
connectAttr "groupId55.msg" "lambert3SG.gn" -na;
connectAttr "groupId56.msg" "lambert3SG.gn" -na;
connectAttr "groupId57.msg" "lambert3SG.gn" -na;
connectAttr "groupId58.msg" "lambert3SG.gn" -na;
connectAttr "groupId59.msg" "lambert3SG.gn" -na;
connectAttr "groupId60.msg" "lambert3SG.gn" -na;
connectAttr "groupId61.msg" "lambert3SG.gn" -na;
connectAttr "groupId62.msg" "lambert3SG.gn" -na;
connectAttr "groupId63.msg" "lambert3SG.gn" -na;
connectAttr "groupId64.msg" "lambert3SG.gn" -na;
connectAttr "groupId65.msg" "lambert3SG.gn" -na;
connectAttr "groupId66.msg" "lambert3SG.gn" -na;
connectAttr "groupId67.msg" "lambert3SG.gn" -na;
connectAttr "groupId68.msg" "lambert3SG.gn" -na;
connectAttr "groupId70.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "west_tower_details.msg" "materialInfo2.m";
connectAttr "west_tower_boiler.oc" "lambert4SG.ss";
connectAttr "pPipeShape1.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pPipeShape1.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCube31Shape.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "groupId29.msg" "lambert4SG.gn" -na;
connectAttr "groupId30.msg" "lambert4SG.gn" -na;
connectAttr "groupId71.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "west_tower_boiler.msg" "materialInfo3.m";
connectAttr "west_tower_water.oc" "lambert5SG.ss";
connectAttr "pCylinderShape1.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCube31Shape.iog.og[3]" "lambert5SG.dsm" -na;
connectAttr "groupId31.msg" "lambert5SG.gn" -na;
connectAttr "groupId32.msg" "lambert5SG.gn" -na;
connectAttr "groupId72.msg" "lambert5SG.gn" -na;
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "west_tower_water.msg" "materialInfo4.m";
connectAttr "west_tower_boiler_details.oc" "lambert6SG.ss";
connectAttr "pTorusShape1.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "pTorusShape1.ciog.cog[0]" "lambert6SG.dsm" -na;
connectAttr "pTorusShape2.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "pTorusShape2.ciog.cog[0]" "lambert6SG.dsm" -na;
connectAttr "pCube31Shape.iog.og[4]" "lambert6SG.dsm" -na;
connectAttr "groupId33.msg" "lambert6SG.gn" -na;
connectAttr "groupId34.msg" "lambert6SG.gn" -na;
connectAttr "groupId35.msg" "lambert6SG.gn" -na;
connectAttr "groupId36.msg" "lambert6SG.gn" -na;
connectAttr "groupId73.msg" "lambert6SG.gn" -na;
connectAttr "lambert6SG.msg" "materialInfo5.sg";
connectAttr "west_tower_boiler_details.msg" "materialInfo5.m";
connectAttr "west_tower_boiler.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert4SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "pCubeShape1.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape3.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape4.o" "polyUnite1.ip[3]";
connectAttr "pCubeShape5.o" "polyUnite1.ip[4]";
connectAttr "pCubeShape6.o" "polyUnite1.ip[5]";
connectAttr "pCubeShape7.o" "polyUnite1.ip[6]";
connectAttr "pCubeShape8.o" "polyUnite1.ip[7]";
connectAttr "pCubeShape9.o" "polyUnite1.ip[8]";
connectAttr "pCubeShape10.o" "polyUnite1.ip[9]";
connectAttr "pCubeShape11.o" "polyUnite1.ip[10]";
connectAttr "pCubeShape12.o" "polyUnite1.ip[11]";
connectAttr "pCubeShape13.o" "polyUnite1.ip[12]";
connectAttr "pCubeShape14.o" "polyUnite1.ip[13]";
connectAttr "pPipeShape1.o" "polyUnite1.ip[14]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[15]";
connectAttr "pTorusShape1.o" "polyUnite1.ip[16]";
connectAttr "pTorusShape2.o" "polyUnite1.ip[17]";
connectAttr "pCubeShape15.o" "polyUnite1.ip[18]";
connectAttr "pCubeShape16.o" "polyUnite1.ip[19]";
connectAttr "pCubeShape17.o" "polyUnite1.ip[20]";
connectAttr "pCubeShape18.o" "polyUnite1.ip[21]";
connectAttr "pCubeShape19.o" "polyUnite1.ip[22]";
connectAttr "pCubeShape20.o" "polyUnite1.ip[23]";
connectAttr "pCubeShape21.o" "polyUnite1.ip[24]";
connectAttr "pCubeShape22.o" "polyUnite1.ip[25]";
connectAttr "pCubeShape23.o" "polyUnite1.ip[26]";
connectAttr "pCubeShape24.o" "polyUnite1.ip[27]";
connectAttr "pCubeShape25.o" "polyUnite1.ip[28]";
connectAttr "pCubeShape26.o" "polyUnite1.ip[29]";
connectAttr "pCubeShape27.o" "polyUnite1.ip[30]";
connectAttr "pCubeShape28.o" "polyUnite1.ip[31]";
connectAttr "pCubeShape29.o" "polyUnite1.ip[32]";
connectAttr "pCubeShape30.o" "polyUnite1.ip[33]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape3.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape4.wm" "polyUnite1.im[3]";
connectAttr "pCubeShape5.wm" "polyUnite1.im[4]";
connectAttr "pCubeShape6.wm" "polyUnite1.im[5]";
connectAttr "pCubeShape7.wm" "polyUnite1.im[6]";
connectAttr "pCubeShape8.wm" "polyUnite1.im[7]";
connectAttr "pCubeShape9.wm" "polyUnite1.im[8]";
connectAttr "pCubeShape10.wm" "polyUnite1.im[9]";
connectAttr "pCubeShape11.wm" "polyUnite1.im[10]";
connectAttr "pCubeShape12.wm" "polyUnite1.im[11]";
connectAttr "pCubeShape13.wm" "polyUnite1.im[12]";
connectAttr "pCubeShape14.wm" "polyUnite1.im[13]";
connectAttr "pPipeShape1.wm" "polyUnite1.im[14]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[15]";
connectAttr "pTorusShape1.wm" "polyUnite1.im[16]";
connectAttr "pTorusShape2.wm" "polyUnite1.im[17]";
connectAttr "pCubeShape15.wm" "polyUnite1.im[18]";
connectAttr "pCubeShape16.wm" "polyUnite1.im[19]";
connectAttr "pCubeShape17.wm" "polyUnite1.im[20]";
connectAttr "pCubeShape18.wm" "polyUnite1.im[21]";
connectAttr "pCubeShape19.wm" "polyUnite1.im[22]";
connectAttr "pCubeShape20.wm" "polyUnite1.im[23]";
connectAttr "pCubeShape21.wm" "polyUnite1.im[24]";
connectAttr "pCubeShape22.wm" "polyUnite1.im[25]";
connectAttr "pCubeShape23.wm" "polyUnite1.im[26]";
connectAttr "pCubeShape24.wm" "polyUnite1.im[27]";
connectAttr "pCubeShape25.wm" "polyUnite1.im[28]";
connectAttr "pCubeShape26.wm" "polyUnite1.im[29]";
connectAttr "pCubeShape27.wm" "polyUnite1.im[30]";
connectAttr "pCubeShape28.wm" "polyUnite1.im[31]";
connectAttr "pCubeShape29.wm" "polyUnite1.im[32]";
connectAttr "pCubeShape30.wm" "polyUnite1.im[33]";
connectAttr "polyCube1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polyCube2.out" "groupParts2.ig";
connectAttr "groupId9.id" "groupParts2.gi";
connectAttr "polyCube3.out" "groupParts3.ig";
connectAttr "groupId13.id" "groupParts3.gi";
connectAttr "polyPipe1.out" "groupParts4.ig";
connectAttr "groupId29.id" "groupParts4.gi";
connectAttr "polyCylinder1.out" "groupParts5.ig";
connectAttr "groupId31.id" "groupParts5.gi";
connectAttr "polyTorus1.out" "groupParts6.ig";
connectAttr "groupId33.id" "groupParts6.gi";
connectAttr "polyCube4.out" "groupParts7.ig";
connectAttr "groupId37.id" "groupParts7.gi";
connectAttr "polyUnite1.out" "groupParts8.ig";
connectAttr "groupId69.id" "groupParts8.gi";
connectAttr "groupParts8.og" "groupParts9.ig";
connectAttr "groupId70.id" "groupParts9.gi";
connectAttr "groupParts9.og" "groupParts10.ig";
connectAttr "groupId71.id" "groupParts10.gi";
connectAttr "groupParts10.og" "groupParts11.ig";
connectAttr "groupId72.id" "groupParts11.gi";
connectAttr "groupParts11.og" "groupParts12.ig";
connectAttr "groupId73.id" "groupParts12.gi";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "west_tower_structure.msg" ":defaultShaderList1.s" -na;
connectAttr "west_tower_details.msg" ":defaultShaderList1.s" -na;
connectAttr "west_tower_boiler.msg" ":defaultShaderList1.s" -na;
connectAttr "west_tower_water.msg" ":defaultShaderList1.s" -na;
connectAttr "west_tower_boiler_details.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of west_tower.ma
