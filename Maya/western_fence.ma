//Maya ASCII 2016 scene
//Name: western_fence.ma
//Last modified: Fri, Jan 27, 2017 04:09:23 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "373B387F-734C-A30D-8150-E4BA21DAED23";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 3.2810196355614258 3.0688554155594927 6.6860675807530194 ;
	setAttr ".r" -type "double3" -20.738352729656803 13.799999999998267 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "AAEE36D2-7D4A-78EB-3BA7-E0A0B61308D8";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 7.2159654111734914;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 1.6712949898053764 0.51367532818554318 0.13244277449606756 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "48927FFC-E844-DFF5-A164-739D71201A89";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1.6712949898053764 100.10812386902344 0.13244277449608968 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "8004BDF7-0142-904B-0CDA-B280B7E643F7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.0526315789473684;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "C2692ACF-B743-D011-F10A-DC99EB8AA33C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1.7469926532862285 1.0634317239881754 100.17412202193688 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "F28D2850-1A42-54E3-63AA-D79250721C01";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.0526315789473684;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "A0E5D594-A947-C267-67E0-FDB2A843E9FE";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.10536460572733 1.0482923402757565 0.1109043015104583 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "AC5B16E9-8949-B64A-1806-9EBDF0B74803";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 1.0526315789473684;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "2263D81A-3A4B-86A0-1174-9E858BCD727E";
	setAttr ".t" -type "double3" -1.6676452748894306 0.072380605814202553 0 ;
	setAttr ".s" -type "double3" 0.30903614905345378 0.15377612396638923 0.29699248418166169 ;
createNode transform -n "transform8" -p "pCube1";
	rename -uid "A398648B-0745-1224-F6AC-99BF584D8C7E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform8";
	rename -uid "4C74563D-564C-973D-5590-E59C69E4E146";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.10832764 1.1302439 -0.60718578 
		-0.062401369 1.1302439 -0.19646275 0.10832764 1.1302439 -0.10832764 -0.10832764 1.1302439 
		-0.10832764;
createNode transform -n "pCube2";
	rename -uid "498F73F8-7A4A-E23D-A2C6-B3A4215B317D";
	setAttr ".t" -type "double3" 1.6680077681891632 0.072380605814202553 -0.034237931945505284 ;
	setAttr ".r" -type "double3" 0 180 0 ;
	setAttr ".s" -type "double3" 0.30903614905345378 0.15377612396638923 0.29699248418166169 ;
createNode transform -n "transform7" -p "pCube2";
	rename -uid "45D31CC4-4143-8835-FA74-35B1C0EFD807";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform7";
	rename -uid "687A0B46-4A46-1FB8-D6E5-7CABCBA25E27";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:13]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 22 ".uvst[0].uvsp[0:21]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1 0 1 1 1 1 2 0 2 0 1 1 1 1 2 0 2;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[12:15]" -type "float3"  -0.10832764 1.1302439 -0.60718578 
		-0.062401369 1.1302439 -0.19646275 0.10832764 1.1302439 -0.10832764 -0.10832764 1.1302439 
		-0.10832764;
	setAttr -s 16 ".vt[0:15]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.54925305 0.49999997 0.41668087
		 0.2841087 0.49999997 0.41668087 -0.54925305 0.49999997 -0.41668087 0.2841087 0.49999997 -0.41668087
		 -0.5 -0.5 -0.5 0.5 -0.5 -0.5 -0.616081 3.47301865 0.40505153 0.19402206 3.47301865 0.40505153
		 0.19402206 3.47301865 -0.40505153 -0.616081 3.47301865 -0.40505153 -0.8932538 7.56597853 0.40505153
		 -0.083150737 7.56597853 0.40505153 -0.083150737 7.56597853 -0.40505153 -0.8932538 7.56597853 -0.40505153;
	setAttr -s 28 ".ed[0:27]"  0 1 0 2 3 1 4 5 1 6 7 0 0 2 0 1 3 0 2 4 1
		 3 5 1 4 6 0 5 7 0 6 0 0 7 1 0 2 8 0 3 9 0 8 9 1 5 10 0 9 10 1 4 11 0 11 10 1 8 11 1
		 8 12 0 9 13 0 12 13 0 10 14 0 13 14 0 11 15 0 15 14 0 12 15 0;
	setAttr -s 14 -ch 56 ".fc[0:13]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 22 24 -27 -28
		mu 0 4 18 19 20 21
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13
		f 4 1 13 -15 -13
		mu 0 4 2 3 15 14
		f 4 7 15 -17 -14
		mu 0 4 3 5 16 15
		f 4 -3 17 18 -16
		mu 0 4 5 4 17 16
		f 4 -7 12 19 -18
		mu 0 4 4 2 14 17
		f 4 14 21 -23 -21
		mu 0 4 14 15 19 18
		f 4 16 23 -25 -22
		mu 0 4 15 16 20 19
		f 4 -19 25 26 -24
		mu 0 4 16 17 21 20
		f 4 -20 20 27 -26
		mu 0 4 17 14 18 21;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube3";
	rename -uid "CF4A4B8A-B54F-260D-8335-9AAB58831ACD";
	setAttr ".t" -type "double3" 0 1.0482923402757565 0.11090430151043607 ;
	setAttr ".r" -type "double3" 0 -0.64278307913615207 0 ;
	setAttr ".s" -type "double3" 4.2363057549999859 0.24617636344003213 0.044971702717988414 ;
createNode transform -n "transform6" -p "pCube3";
	rename -uid "87AB5E62-2646-94C7-4318-C08C6E701FDD";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform6";
	rename -uid "633AB628-9949-60D8-A8E6-CF8CABB66CC3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.59375 1.531290739774704 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 573 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0.18815485 -0.18815485 ;
	setAttr ".pt[3]" -type "float3" 0 -0.18815485 -0.18815485 ;
	setAttr ".pt[5]" -type "float3" 0 -0.18815485 0.18815485 ;
	setAttr ".pt[7]" -type "float3" 0 0.18815485 0.18815485 ;
	setAttr ".pt[10]" -type "float3" 0 3.3423015e-16 -0.18815485 ;
	setAttr ".pt[11]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[13]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[14]" -type "float3" 0 -0.18815485 0 ;
	setAttr ".pt[15]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[18]" -type "float3" 0 3.3423015e-16 0.18815485 ;
	setAttr ".pt[22]" -type "float3" 0 0.18815485 0 ;
	setAttr ".pt[24]" -type "float3" 0 3.3423015e-16 0 ;
	setAttr ".pt[34]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[39]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[46]" -type "float3" 0 0.094077423 -0.094077423 ;
	setAttr ".pt[47]" -type "float3" 0 0.094077423 -0.18815485 ;
	setAttr ".pt[48]" -type "float3" 0 0.18815485 -0.094077423 ;
	setAttr ".pt[49]" -type "float3" 0 0.094077423 0 ;
	setAttr ".pt[50]" -type "float3" 0 3.3423015e-16 -0.094077423 ;
	setAttr ".pt[59]" -type "float3" 0 -0.094077423 -0.18815485 ;
	setAttr ".pt[60]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[61]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[64]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[65]" -type "float3" 0 -0.18815485 -0.094077423 ;
	setAttr ".pt[66]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[67]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[68]" -type "float3" 0 -0.18815485 0.094077423 ;
	setAttr ".pt[69]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[70]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[74]" -type "float3" 0 -0.094077423 0.18815485 ;
	setAttr ".pt[77]" -type "float3" 0 0.094077423 0.18815485 ;
	setAttr ".pt[82]" -type "float3" 0 0.18815485 0.094077423 ;
	setAttr ".pt[88]" -type "float3" 0 0.094077423 0.094077423 ;
	setAttr ".pt[89]" -type "float3" 0 3.3423015e-16 0.094077423 ;
	setAttr ".pt[90]" -type "float3" 0 -0.094077423 0.094077423 ;
	setAttr ".pt[91]" -type "float3" 0 -0.094077423 0 ;
	setAttr ".pt[92]" -type "float3" 0 -0.094077423 -0.094077423 ;
	setAttr ".pt[118]" -type "float3" 0 0.047038712 -0.14111614 ;
	setAttr ".pt[119]" -type "float3" 0 3.3423015e-16 -0.14111614 ;
	setAttr ".pt[120]" -type "float3" 0 0.047038712 -0.18815485 ;
	setAttr ".pt[121]" -type "float3" 0 0.094077423 -0.14111614 ;
	setAttr ".pt[122]" -type "float3" 0 0.047038712 -0.094077423 ;
	setAttr ".pt[135]" -type "float3" 0 -0.047038712 -0.18815485 ;
	setAttr ".pt[138]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[139]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[140]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[142]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[143]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[144]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[145]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[146]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[147]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[150]" -type "float3" 0 -0.18815485 0.047038712 ;
	setAttr ".pt[152]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[153]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[154]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[155]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[157]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[158]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[159]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[160]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[161]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[162]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[165]" -type "float3" 0 0.047038712 0.18815485 ;
	setAttr ".pt[180]" -type "float3" 0 0.18815485 -0.047038712 ;
	setAttr ".pt[188]" -type "float3" 0 0.14111614 0.047038712 ;
	setAttr ".pt[189]" -type "float3" 0 0.14111614 0 ;
	setAttr ".pt[190]" -type "float3" 0 0.18815485 0.047038712 ;
	setAttr ".pt[191]" -type "float3" 0 0.14111614 0.094077423 ;
	setAttr ".pt[192]" -type "float3" 0 0.094077423 0.047038712 ;
	setAttr ".pt[193]" -type "float3" 0 -0.047038712 0.14111614 ;
	setAttr ".pt[194]" -type "float3" 0 3.3423015e-16 0.14111614 ;
	setAttr ".pt[195]" -type "float3" 0 -0.047038712 0.18815485 ;
	setAttr ".pt[196]" -type "float3" 0 -0.094077423 0.14111614 ;
	setAttr ".pt[197]" -type "float3" 0 -0.047038712 0.094077423 ;
	setAttr ".pt[198]" -type "float3" 0 -0.14111614 -0.047038712 ;
	setAttr ".pt[199]" -type "float3" 0 -0.14111614 0 ;
	setAttr ".pt[200]" -type "float3" 0 -0.18815485 -0.047038712 ;
	setAttr ".pt[201]" -type "float3" 0 -0.14111614 -0.094077423 ;
	setAttr ".pt[202]" -type "float3" 0 -0.094077423 -0.047038712 ;
	setAttr ".pt[231]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[232]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[233]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[234]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[235]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[240]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[241]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[242]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[243]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[254]" -type "float3" 0 0.14111614 -0.14111614 ;
	setAttr ".pt[255]" -type "float3" 0 0.14111614 -0.18815485 ;
	setAttr ".pt[256]" -type "float3" 0 0.18815485 -0.14111614 ;
	setAttr ".pt[257]" -type "float3" 0 0.14111614 -0.094077423 ;
	setAttr ".pt[258]" -type "float3" 0 0.14111614 -0.047038712 ;
	setAttr ".pt[259]" -type "float3" 0 0.094077423 -0.047038712 ;
	setAttr ".pt[260]" -type "float3" 0 0.047038712 -0.047038712 ;
	setAttr ".pt[261]" -type "float3" 0 0.047038712 0 ;
	setAttr ".pt[262]" -type "float3" 0 3.3423015e-16 -0.047038712 ;
	setAttr ".pt[279]" -type "float3" 0 -0.14111614 -0.18815485 ;
	setAttr ".pt[281]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[282]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[283]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[285]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[291]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[293]" -type "float3" 0 -0.18815485 -0.14111614 ;
	setAttr ".pt[296]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[297]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[298]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[300]" -type "float3" 0 -0.18815485 0.14111614 ;
	setAttr ".pt[302]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[303]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[304]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[305]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[306]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[312]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[314]" -type "float3" 0 -0.14111614 0.18815485 ;
	setAttr ".pt[321]" -type "float3" 0 0.14111614 0.18815485 ;
	setAttr ".pt[334]" -type "float3" 0 0.18815485 0.14111614 ;
	setAttr ".pt[352]" -type "float3" 0 0.14111614 0.14111614 ;
	setAttr ".pt[353]" -type "float3" 0 0.094077423 0.14111614 ;
	setAttr ".pt[354]" -type "float3" 0 0.047038712 0.14111614 ;
	setAttr ".pt[355]" -type "float3" 0 0.047038712 0.094077423 ;
	setAttr ".pt[356]" -type "float3" 0 0.047038712 0.047038712 ;
	setAttr ".pt[357]" -type "float3" 0 3.3423015e-16 0.047038712 ;
	setAttr ".pt[358]" -type "float3" 0 -0.14111614 0.14111614 ;
	setAttr ".pt[359]" -type "float3" 0 -0.14111614 0.094077423 ;
	setAttr ".pt[360]" -type "float3" 0 -0.14111614 0.047038712 ;
	setAttr ".pt[361]" -type "float3" 0 -0.094077423 0.047038712 ;
	setAttr ".pt[362]" -type "float3" 0 -0.047038712 0.047038712 ;
	setAttr ".pt[363]" -type "float3" 0 -0.047038712 0 ;
	setAttr ".pt[364]" -type "float3" 0 -0.14111614 -0.14111614 ;
	setAttr ".pt[365]" -type "float3" 0 -0.094077423 -0.14111614 ;
	setAttr ".pt[366]" -type "float3" 0 -0.047038712 -0.14111614 ;
	setAttr ".pt[367]" -type "float3" 0 -0.047038712 -0.094077423 ;
	setAttr ".pt[368]" -type "float3" 0 -0.047038712 -0.047038712 ;
	setAttr ".pt[406]" -type "float3" 0 0.023519356 -0.11759678 ;
	setAttr ".pt[407]" -type "float3" 0 0.023519356 -0.094077423 ;
	setAttr ".pt[408]" -type "float3" 0 3.3423015e-16 -0.11759678 ;
	setAttr ".pt[409]" -type "float3" 0 0.023519356 -0.14111614 ;
	setAttr ".pt[410]" -type "float3" 0 0.047038712 -0.11759678 ;
	setAttr ".pt[426]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[427]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[428]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[429]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[430]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[431]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[432]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[433]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[434]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[435]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[436]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[437]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[438]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[440]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[441]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[442]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[443]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[444]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[445]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[446]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[447]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[448]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[449]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[450]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[476]" -type "float3" 0 0.11759678 0.023519356 ;
	setAttr ".pt[477]" -type "float3" 0 0.094077423 0.023519356 ;
	setAttr ".pt[478]" -type "float3" 0 0.11759678 0 ;
	setAttr ".pt[479]" -type "float3" 0 0.14111614 0.023519356 ;
	setAttr ".pt[480]" -type "float3" 0 0.11759678 0.047038712 ;
	setAttr ".pt[481]" -type "float3" 0 -0.023519356 0.11759678 ;
	setAttr ".pt[482]" -type "float3" 0 -0.023519356 0.094077423 ;
	setAttr ".pt[483]" -type "float3" 0 3.3423015e-16 0.11759678 ;
	setAttr ".pt[484]" -type "float3" 0 -0.023519356 0.14111614 ;
	setAttr ".pt[485]" -type "float3" 0 -0.047038712 0.11759678 ;
	setAttr ".pt[486]" -type "float3" 0 -0.11759678 -0.023519356 ;
	setAttr ".pt[487]" -type "float3" 0 -0.094077423 -0.023519356 ;
	setAttr ".pt[488]" -type "float3" 0 -0.11759678 0 ;
	setAttr ".pt[489]" -type "float3" 0 -0.14111614 -0.023519356 ;
	setAttr ".pt[490]" -type "float3" 0 -0.11759678 -0.047038712 ;
	setAttr ".pt[529]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[531]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[532]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[533]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[534]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[535]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[544]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[546]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[547]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[548]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[549]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[550]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[566]" -type "float3" 0 0.11759678 -0.16463549 ;
	setAttr ".pt[567]" -type "float3" 0 0.094077423 -0.16463549 ;
	setAttr ".pt[568]" -type "float3" 0 0.11759678 -0.18815485 ;
	setAttr ".pt[569]" -type "float3" 0 0.14111614 -0.16463549 ;
	setAttr ".pt[570]" -type "float3" 0 0.11759678 -0.14111614 ;
	setAttr ".pt[571]" -type "float3" 0 0.16463549 -0.070558071 ;
	setAttr ".pt[572]" -type "float3" 0 0.16463549 -0.094077423 ;
	setAttr ".pt[573]" -type "float3" 0 0.18815485 -0.070558071 ;
	setAttr ".pt[574]" -type "float3" 0 0.16463549 -0.047038712 ;
	setAttr ".pt[575]" -type "float3" 0 0.14111614 -0.070558071 ;
	setAttr ".pt[576]" -type "float3" 0 0.070558071 -0.023519356 ;
	setAttr ".pt[577]" -type "float3" 0 0.094077423 -0.023519356 ;
	setAttr ".pt[578]" -type "float3" 0 0.070558071 0 ;
	setAttr ".pt[579]" -type "float3" 0 0.047038712 -0.023519356 ;
	setAttr ".pt[580]" -type "float3" 0 0.070558071 -0.047038712 ;
	setAttr ".pt[603]" -type "float3" 0 0.070558071 -0.18815485 ;
	setAttr ".pt[613]" -type "float3" 0 -0.11759678 -0.18815485 ;
	setAttr ".pt[616]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[617]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[618]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[619]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[620]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[621]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[622]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[623]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[639]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[642]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[648]" -type "float3" 0 -0.18815485 -0.070558071 ;
	setAttr ".pt[651]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[652]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[653]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[654]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[655]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[658]" -type "float3" 0 -0.18815485 0.11759678 ;
	setAttr ".pt[661]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[662]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[663]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[664]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[665]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[666]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[667]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[668]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[669]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[670]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[684]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[686]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[687]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[688]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[690]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[693]" -type "float3" 0 -0.070558071 0.18815485 ;
	setAttr ".pt[703]" -type "float3" 0 0.11759678 0.18815485 ;
	setAttr ".pt[738]" -type "float3" 0 0.18815485 0.070558071 ;
	setAttr ".pt[748]" -type "float3" 0 0.18815485 -0.11759678 ;
	setAttr ".pt[776]" -type "float3" 0 0.16463549 0.11759678 ;
	setAttr ".pt[777]" -type "float3" 0 0.16463549 0.094077423 ;
	setAttr ".pt[778]" -type "float3" 0 0.18815485 0.11759678 ;
	setAttr ".pt[779]" -type "float3" 0 0.16463549 0.14111614 ;
	setAttr ".pt[780]" -type "float3" 0 0.14111614 0.11759678 ;
	setAttr ".pt[781]" -type "float3" 0 0.070558071 0.16463549 ;
	setAttr ".pt[782]" -type "float3" 0 0.094077423 0.16463549 ;
	setAttr ".pt[783]" -type "float3" 0 0.070558071 0.18815485 ;
	setAttr ".pt[784]" -type "float3" 0 0.047038712 0.16463549 ;
	setAttr ".pt[785]" -type "float3" 0 0.070558071 0.14111614 ;
	setAttr ".pt[786]" -type "float3" 0 0.023519356 0.070558071 ;
	setAttr ".pt[787]" -type "float3" 0 0.023519356 0.094077423 ;
	setAttr ".pt[788]" -type "float3" 0 3.3423015e-16 0.070558071 ;
	setAttr ".pt[789]" -type "float3" 0 0.023519356 0.047038712 ;
	setAttr ".pt[790]" -type "float3" 0 0.047038712 0.070558071 ;
	setAttr ".pt[791]" -type "float3" 0 -0.11759678 0.16463549 ;
	setAttr ".pt[792]" -type "float3" 0 -0.094077423 0.16463549 ;
	setAttr ".pt[793]" -type "float3" 0 -0.11759678 0.18815485 ;
	setAttr ".pt[794]" -type "float3" 0 -0.14111614 0.16463549 ;
	setAttr ".pt[795]" -type "float3" 0 -0.11759678 0.14111614 ;
	setAttr ".pt[796]" -type "float3" 0 -0.16463549 0.070558071 ;
	setAttr ".pt[797]" -type "float3" 0 -0.16463549 0.094077423 ;
	setAttr ".pt[798]" -type "float3" 0 -0.18815485 0.070558071 ;
	setAttr ".pt[799]" -type "float3" 0 -0.16463549 0.047038712 ;
	setAttr ".pt[800]" -type "float3" 0 -0.14111614 0.070558071 ;
	setAttr ".pt[801]" -type "float3" 0 -0.070558071 0.023519356 ;
	setAttr ".pt[802]" -type "float3" 0 -0.094077423 0.023519356 ;
	setAttr ".pt[803]" -type "float3" 0 -0.070558071 0 ;
	setAttr ".pt[804]" -type "float3" 0 -0.047038712 0.023519356 ;
	setAttr ".pt[805]" -type "float3" 0 -0.070558071 0.047038712 ;
	setAttr ".pt[806]" -type "float3" 0 -0.16463549 -0.11759678 ;
	setAttr ".pt[807]" -type "float3" 0 -0.16463549 -0.094077423 ;
	setAttr ".pt[808]" -type "float3" 0 -0.18815485 -0.11759678 ;
	setAttr ".pt[809]" -type "float3" 0 -0.16463549 -0.14111614 ;
	setAttr ".pt[810]" -type "float3" 0 -0.14111614 -0.11759678 ;
	setAttr ".pt[811]" -type "float3" 0 -0.070558071 -0.16463549 ;
	setAttr ".pt[812]" -type "float3" 0 -0.094077423 -0.16463549 ;
	setAttr ".pt[813]" -type "float3" 0 -0.070558071 -0.18815485 ;
	setAttr ".pt[814]" -type "float3" 0 -0.047038712 -0.16463549 ;
	setAttr ".pt[815]" -type "float3" 0 -0.070558071 -0.14111614 ;
	setAttr ".pt[816]" -type "float3" 0 -0.023519356 -0.070558071 ;
	setAttr ".pt[817]" -type "float3" 0 -0.023519356 -0.094077423 ;
	setAttr ".pt[818]" -type "float3" 0 3.3423015e-16 -0.070558071 ;
	setAttr ".pt[819]" -type "float3" 0 -0.023519356 -0.047038712 ;
	setAttr ".pt[820]" -type "float3" 0 -0.047038712 -0.070558071 ;
	setAttr ".pt[902]" -type "float3" 0 0.023519356 -0.16463549 ;
	setAttr ".pt[903]" -type "float3" 0 3.3423015e-16 -0.16463549 ;
	setAttr ".pt[904]" -type "float3" 0 0.023519356 -0.18815485 ;
	setAttr ".pt[905]" -type "float3" 0 0.047038712 -0.16463549 ;
	setAttr ".pt[906]" -type "float3" 0 0.070558071 -0.16463549 ;
	setAttr ".pt[907]" -type "float3" 0 0.070558071 -0.14111614 ;
	setAttr ".pt[908]" -type "float3" 0 0.070558071 -0.11759678 ;
	setAttr ".pt[909]" -type "float3" 0 0.094077423 -0.11759678 ;
	setAttr ".pt[910]" -type "float3" 0 0.070558071 -0.094077423 ;
	setAttr ".pt[931]" -type "float3" 0 -0.023519356 -0.18815485 ;
	setAttr ".pt[938]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[939]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[940]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[941]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[947]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[948]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[949]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[950]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[951]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[952]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[953]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[954]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[955]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[958]" -type "float3" 0 -0.18815485 0.023519356 ;
	setAttr ".pt[962]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[963]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[964]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[965]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[966]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[967]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[968]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[974]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[975]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[976]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[977]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[978]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[979]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[980]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[981]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[982]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[985]" -type "float3" 0 0.023519356 0.18815485 ;
	setAttr ".pt[1012]" -type "float3" 0 0.18815485 -0.023519356 ;
	setAttr ".pt[1028]" -type "float3" 0 0.16463549 0.023519356 ;
	setAttr ".pt[1029]" -type "float3" 0 0.16463549 0 ;
	setAttr ".pt[1030]" -type "float3" 0 0.18815485 0.023519356 ;
	setAttr ".pt[1031]" -type "float3" 0 0.16463549 0.047038712 ;
	setAttr ".pt[1032]" -type "float3" 0 0.16463549 0.070558071 ;
	setAttr ".pt[1033]" -type "float3" 0 0.14111614 0.070558071 ;
	setAttr ".pt[1034]" -type "float3" 0 0.11759678 0.070558071 ;
	setAttr ".pt[1035]" -type "float3" 0 0.11759678 0.094077423 ;
	setAttr ".pt[1036]" -type "float3" 0 0.094077423 0.070558071 ;
	setAttr ".pt[1037]" -type "float3" 0 -0.023519356 0.16463549 ;
	setAttr ".pt[1038]" -type "float3" 0 3.3423015e-16 0.16463549 ;
	setAttr ".pt[1039]" -type "float3" 0 -0.023519356 0.18815485 ;
	setAttr ".pt[1040]" -type "float3" 0 -0.047038712 0.16463549 ;
	setAttr ".pt[1041]" -type "float3" 0 -0.070558071 0.16463549 ;
	setAttr ".pt[1042]" -type "float3" 0 -0.070558071 0.14111614 ;
	setAttr ".pt[1043]" -type "float3" 0 -0.070558071 0.11759678 ;
	setAttr ".pt[1044]" -type "float3" 0 -0.094077423 0.11759678 ;
	setAttr ".pt[1045]" -type "float3" 0 -0.070558071 0.094077423 ;
	setAttr ".pt[1046]" -type "float3" 0 -0.16463549 -0.023519356 ;
	setAttr ".pt[1047]" -type "float3" 0 -0.16463549 0 ;
	setAttr ".pt[1048]" -type "float3" 0 -0.18815485 -0.023519356 ;
	setAttr ".pt[1049]" -type "float3" 0 -0.16463549 -0.047038712 ;
	setAttr ".pt[1050]" -type "float3" 0 -0.16463549 -0.070558071 ;
	setAttr ".pt[1051]" -type "float3" 0 -0.14111614 -0.070558071 ;
	setAttr ".pt[1052]" -type "float3" 0 -0.11759678 -0.070558071 ;
	setAttr ".pt[1053]" -type "float3" 0 -0.11759678 -0.094077423 ;
	setAttr ".pt[1054]" -type "float3" 0 -0.094077423 -0.070558071 ;
	setAttr ".pt[1111]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1112]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1113]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1114]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1117]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1118]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1119]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1120]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1132]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1133]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1134]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1135]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1138]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1139]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1140]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1141]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1166]" -type "float3" 0 0.16463549 -0.16463549 ;
	setAttr ".pt[1167]" -type "float3" 0 0.16463549 -0.18815485 ;
	setAttr ".pt[1168]" -type "float3" 0 0.18815485 -0.16463549 ;
	setAttr ".pt[1169]" -type "float3" 0 0.16463549 -0.14111614 ;
	setAttr ".pt[1170]" -type "float3" 0 0.16463549 -0.11759678 ;
	setAttr ".pt[1171]" -type "float3" 0 0.14111614 -0.11759678 ;
	setAttr ".pt[1172]" -type "float3" 0 0.11759678 -0.11759678 ;
	setAttr ".pt[1173]" -type "float3" 0 0.11759678 -0.094077423 ;
	setAttr ".pt[1174]" -type "float3" 0 0.16463549 -0.023519356 ;
	setAttr ".pt[1175]" -type "float3" 0 0.14111614 -0.023519356 ;
	setAttr ".pt[1176]" -type "float3" 0 0.11759678 -0.023519356 ;
	setAttr ".pt[1177]" -type "float3" 0 0.11759678 -0.047038712 ;
	setAttr ".pt[1178]" -type "float3" 0 0.11759678 -0.070558071 ;
	setAttr ".pt[1179]" -type "float3" 0 0.094077423 -0.070558071 ;
	setAttr ".pt[1180]" -type "float3" 0 0.023519356 -0.023519356 ;
	setAttr ".pt[1181]" -type "float3" 0 0.023519356 0 ;
	setAttr ".pt[1182]" -type "float3" 0 3.3423015e-16 -0.023519356 ;
	setAttr ".pt[1183]" -type "float3" 0 0.023519356 -0.047038712 ;
	setAttr ".pt[1184]" -type "float3" 0 0.023519356 -0.070558071 ;
	setAttr ".pt[1185]" -type "float3" 0 0.047038712 -0.070558071 ;
	setAttr ".pt[1186]" -type "float3" 0 0.070558071 -0.070558071 ;
	setAttr ".pt[1227]" -type "float3" 0 -0.16463549 -0.18815485 ;
	setAttr ".pt[1231]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1233]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1234]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1235]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1236]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1237]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1238]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1259]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1260]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1261]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1262]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1265]" -type "float3" 0 -0.18815485 -0.16463549 ;
	setAttr ".pt[1276]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1277]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1278]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1279]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1280]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1281]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1282]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1284]" -type "float3" 0 -0.18815485 0.16463549 ;
	setAttr ".pt[1287]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1288]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1289]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1290]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1291]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1292]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1293]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1294]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1295]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1296]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1297]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1298]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1299]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1300]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1301]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1302]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1316]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1317]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1318]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1319]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1322]" -type "float3" 0 -0.16463549 0.18815485 ;
	setAttr ".pt[1336]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1337]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1338]" -type "float3" 0 -0.043870997 0 ;
	setAttr ".pt[1341]" -type "float3" 0 0.16463549 0.18815485 ;
	setAttr ".pt[1378]" -type "float3" 0 0.18815485 0.16463549 ;
	setAttr ".pt[1432]" -type "float3" 0 0.16463549 0.16463549 ;
	setAttr ".pt[1433]" -type "float3" 0 0.14111614 0.16463549 ;
	setAttr ".pt[1434]" -type "float3" 0 0.11759678 0.16463549 ;
	setAttr ".pt[1435]" -type "float3" 0 0.11759678 0.14111614 ;
	setAttr ".pt[1436]" -type "float3" 0 0.11759678 0.11759678 ;
	setAttr ".pt[1437]" -type "float3" 0 0.094077423 0.11759678 ;
	setAttr ".pt[1438]" -type "float3" 0 0.023519356 0.16463549 ;
	setAttr ".pt[1439]" -type "float3" 0 0.023519356 0.14111614 ;
	setAttr ".pt[1440]" -type "float3" 0 0.023519356 0.11759678 ;
	setAttr ".pt[1441]" -type "float3" 0 0.047038712 0.11759678 ;
	setAttr ".pt[1442]" -type "float3" 0 0.070558071 0.11759678 ;
	setAttr ".pt[1443]" -type "float3" 0 0.070558071 0.094077423 ;
	setAttr ".pt[1444]" -type "float3" 0 0.023519356 0.023519356 ;
	setAttr ".pt[1445]" -type "float3" 0 3.3423015e-16 0.023519356 ;
	setAttr ".pt[1446]" -type "float3" 0 0.047038712 0.023519356 ;
	setAttr ".pt[1447]" -type "float3" 0 0.070558071 0.023519356 ;
	setAttr ".pt[1448]" -type "float3" 0 0.070558071 0.047038712 ;
	setAttr ".pt[1449]" -type "float3" 0 0.070558071 0.070558071 ;
	setAttr ".pt[1450]" -type "float3" 0 -0.16463549 0.16463549 ;
	setAttr ".pt[1451]" -type "float3" 0 -0.16463549 0.14111614 ;
	setAttr ".pt[1452]" -type "float3" 0 -0.16463549 0.11759678 ;
	setAttr ".pt[1453]" -type "float3" 0 -0.14111614 0.11759678 ;
	setAttr ".pt[1454]" -type "float3" 0 -0.11759678 0.11759678 ;
	setAttr ".pt[1455]" -type "float3" 0 -0.11759678 0.094077423 ;
	setAttr ".pt[1456]" -type "float3" 0 -0.16463549 0.023519356 ;
	setAttr ".pt[1457]" -type "float3" 0 -0.14111614 0.023519356 ;
	setAttr ".pt[1458]" -type "float3" 0 -0.11759678 0.023519356 ;
	setAttr ".pt[1459]" -type "float3" 0 -0.11759678 0.047038712 ;
	setAttr ".pt[1460]" -type "float3" 0 -0.11759678 0.070558071 ;
	setAttr ".pt[1461]" -type "float3" 0 -0.094077423 0.070558071 ;
	setAttr ".pt[1462]" -type "float3" 0 -0.023519356 0.023519356 ;
	setAttr ".pt[1463]" -type "float3" 0 -0.023519356 0 ;
	setAttr ".pt[1464]" -type "float3" 0 -0.023519356 0.047038712 ;
	setAttr ".pt[1465]" -type "float3" 0 -0.023519356 0.070558071 ;
	setAttr ".pt[1466]" -type "float3" 0 -0.047038712 0.070558071 ;
	setAttr ".pt[1467]" -type "float3" 0 -0.070558071 0.070558071 ;
	setAttr ".pt[1468]" -type "float3" 0 -0.16463549 -0.16463549 ;
	setAttr ".pt[1469]" -type "float3" 0 -0.14111614 -0.16463549 ;
	setAttr ".pt[1470]" -type "float3" 0 -0.11759678 -0.16463549 ;
	setAttr ".pt[1471]" -type "float3" 0 -0.11759678 -0.14111614 ;
	setAttr ".pt[1472]" -type "float3" 0 -0.11759678 -0.11759678 ;
	setAttr ".pt[1473]" -type "float3" 0 -0.094077423 -0.11759678 ;
	setAttr ".pt[1474]" -type "float3" 0 -0.023519356 -0.16463549 ;
	setAttr ".pt[1475]" -type "float3" 0 -0.023519356 -0.14111614 ;
	setAttr ".pt[1476]" -type "float3" 0 -0.023519356 -0.11759678 ;
	setAttr ".pt[1477]" -type "float3" 0 -0.047038712 -0.11759678 ;
	setAttr ".pt[1478]" -type "float3" 0 -0.070558071 -0.11759678 ;
	setAttr ".pt[1479]" -type "float3" 0 -0.070558071 -0.094077423 ;
	setAttr ".pt[1480]" -type "float3" 0 -0.023519356 -0.023519356 ;
	setAttr ".pt[1481]" -type "float3" 0 -0.047038712 -0.023519356 ;
	setAttr ".pt[1482]" -type "float3" 0 -0.070558071 -0.023519356 ;
	setAttr ".pt[1483]" -type "float3" 0 -0.070558071 -0.047038712 ;
	setAttr ".pt[1484]" -type "float3" 0 -0.070558071 -0.070558071 ;
createNode transform -n "pCube4";
	rename -uid "A0E56B38-494D-1A03-056A-3B8086C56ACA";
	setAttr ".t" -type "double3" 0 0.49842227443917819 0.1031813116468443 ;
	setAttr ".r" -type "double3" 0 0.51612126961295601 0 ;
	setAttr ".s" -type "double3" 4.2363057549999859 0.24617636344003213 0.044971702717988414 ;
createNode transform -n "transform5" -p "pCube4";
	rename -uid "44270340-BD40-57F6-EF7D-6D9882602FB9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape4" -p "transform5";
	rename -uid "782B5B1C-B54F-6D79-DB96-11B8150A7820";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 1.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 6 ".pt";
	setAttr ".pt[0]" -type "float3" 0 0.18377817 -0.18377817 ;
	setAttr ".pt[2]" -type "float3" 0 -0.18377817 -0.18377817 ;
	setAttr ".pt[3]" -type "float3" 0 0.2125672 0 ;
	setAttr ".pt[4]" -type "float3" 0 -0.18377817 0.18377817 ;
	setAttr ".pt[5]" -type "float3" 0 0.2125672 0 ;
	setAttr ".pt[6]" -type "float3" 0 0.18377817 0.18377817 ;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder1";
	rename -uid "EE9743EC-8C4C-C45D-91D6-00B55319B5A7";
	setAttr ".t" -type "double3" -1.8118595726779656 1.0634317214977533 0.12083341791919261 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.045309898087641534 0.015699158469583845 0.045309898087641534 ;
createNode transform -n "transform4" -p "pCylinder1";
	rename -uid "74829E48-C84F-7CA8-8D97-7FAA622B45C0";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape1" -p "transform4";
	rename -uid "9889F1DD-C740-02A7-CC53-C0A2F5417B60";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCylinder2";
	rename -uid "A216FE48-8042-F722-752E-8287F39681C1";
	setAttr ".t" -type "double3" -1.715376572651178 0.51367532683520301 0.13244277449606759 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.045309898087641534 0.015699158469583845 0.045309898087641534 ;
createNode transform -n "transform3" -p "pCylinder2";
	rename -uid "DF32E4BE-C143-56F9-0BE5-148B20FC7F97";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape2" -p "transform3";
	rename -uid "C05AB06D-C74F-DA7F-AA4E-488FA51F531D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:17]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 28 ".uvst[0].uvsp[0:27]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".vt[0:13]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0.50000024 1 -0.86602533
		 -0.49999985 1 -0.86602551 -1 1 -1.4901161e-07 -0.50000012 1 0.86602539 0.49999997 1 0.86602545
		 1 1 0 0 -1 0 0 1 0;
	setAttr -s 30 ".ed[0:29]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0 12 0 1 12 1 1
		 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1;
	setAttr -s 18 -ch 60 ".fc[0:17]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder3";
	rename -uid "29323497-F049-8F64-9485-F99A4145B039";
	setAttr ".t" -type "double3" 1.7401937804032379 0.51367532683520301 0.13244277449606759 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.045309898087641534 0.015699158469583845 0.045309898087641534 ;
createNode transform -n "transform2" -p "pCylinder3";
	rename -uid "92A5DA02-924E-091D-CF5C-92ADFCE36B30";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape3" -p "transform2";
	rename -uid "35070608-4143-DAA9-A6A3-BBA16DDF4CCD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:17]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 28 ".uvst[0].uvsp[0:27]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".vt[0:13]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0.50000024 1 -0.86602533
		 -0.49999985 1 -0.86602551 -1 1 -1.4901161e-07 -0.50000012 1 0.86602539 0.49999997 1 0.86602545
		 1 1 0 0 -1 0 0 1 0;
	setAttr -s 30 ".ed[0:29]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0 12 0 1 12 1 1
		 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1;
	setAttr -s 18 -ch 60 ".fc[0:17]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCylinder4";
	rename -uid "A0D2B231-AF40-BF26-45AE-16951490C9EF";
	setAttr ".t" -type "double3" 1.7778292800344266 1.0722731965877679 0.1571766816854461 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.045309898087641534 0.015699158469583845 0.045309898087641534 ;
createNode transform -n "transform1" -p "pCylinder4";
	rename -uid "3C7D2B01-8A4C-CBEE-2C5F-61921439419C";
	setAttr ".v" no;
createNode mesh -n "pCylinderShape4" -p "transform1";
	rename -uid "C422CFE4-7A41-25E2-09AF-E8B01058F4FA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:17]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 28 ".uvst[0].uvsp[0:27]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 14 ".vt[0:13]"  0.50000024 -1 -0.86602533 -0.49999985 -1 -0.86602551
		 -1 -1 -1.4901161e-07 -0.50000012 -1 0.86602539 0.49999997 -1 0.86602545 1 -1 0 0.50000024 1 -0.86602533
		 -0.49999985 1 -0.86602551 -1 1 -1.4901161e-07 -0.50000012 1 0.86602539 0.49999997 1 0.86602545
		 1 1 0 0 -1 0 0 1 0;
	setAttr -s 30 ".ed[0:29]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0 12 0 1 12 1 1
		 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1;
	setAttr -s 18 -ch 60 ".fc[0:17]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "3F83B006-8242-9C1B-AED3-66BE97931A44";
createNode mesh -n "pCube5Shape" -p "pCube5";
	rename -uid "208938D4-AE42-1555-76AC-0381F65DD403";
	setAttr -k off ".v";
	setAttr -s 6 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "A3180457-474B-D300-26DD-A2B0C536F7D4";
	setAttr -s 5 ".lnk";
	setAttr -s 5 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "47691628-154A-58CF-1F23-4EAAC36D7BB0";
createNode displayLayer -n "defaultLayer";
	rename -uid "5142FCE8-744D-E0D5-BCDA-8AA2ACB1858F";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "C097F50B-1D40-CBB1-BE1B-27B2A02EB970";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "58D0B87D-EE45-4FDB-8843-48A04EA4EE1A";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "331696DC-024C-8CE1-E79E-14AE61005391";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 473\n                -height 314\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 473\n            -height 314\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 473\n                -height 364\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 473\n            -height 364\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 473\n                -height 364\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 473\n            -height 364\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 473\n                -height 314\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 473\n            -height 314\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\tscriptedPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n"
		+ "                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n"
		+ "                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n"
		+ "                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 431\n                -height 347\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n"
		+ "            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n"
		+ "            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 431\n            -height 347\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 47 -ps 2 50 47 -ps 3 50 53 -ps 4 50 53 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 314\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 314\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 314\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 314\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 364\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 364\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 364\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 473\\n    -height 364\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "16331BFA-B94B-1243-5CD9-ED9CE96B6487";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube1";
	rename -uid "62FC0BE3-814C-588D-09B3-D2AC46C1AAEA";
	setAttr ".cuv" 1;
createNode polyExtrudeFace -n "polyExtrudeFace1";
	rename -uid "BFAE1CFC-9F41-6004-1AC1-F7886F76BBB7";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 0.30903614905345378 0 0 0 0 0.15377612396638923 0 0
		 0 0 0.29699248418166169 0 0 0.072380605814202553 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" 0 0.14926867 0 ;
	setAttr ".rs" 1882139983;
	setAttr ".lt" -type "double3" 0 1.2325951644078309e-32 0.45717927593555613 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.15451807452672689 0.14926866779739717 -0.14849624209083084 ;
	setAttr ".cbx" -type "double3" 0.15451807452672689 0.14926866779739717 0.14849624209083084 ;
createNode polyExtrudeFace -n "polyExtrudeFace2";
	rename -uid "9AA07B8A-0947-26CC-5320-6A9C41264ECB";
	setAttr ".ics" -type "componentList" 1 "f[1]";
	setAttr ".ix" -type "matrix" 0.30903614905345378 0 0 0 0 0.15377612396638923 0 0
		 0 0 0.29699248418166169 0 0 0.072380605814202553 0 1;
	setAttr ".ws" yes;
	setAttr ".pvt" -type "float3" -0.065215737 0.60644794 0 ;
	setAttr ".rs" 705953150;
	setAttr ".lt" -type "double3" -0.085656406838930485 9.8607613152626476e-32 0.62939947600442925 ;
	setAttr ".c[0]"  0 1 1;
	setAttr ".cbn" -type "double3" -0.19039129955166464 0.60644795169602217 -0.12029725994031518 ;
	setAttr ".cbx" -type "double3" 0.059959830080947282 0.60644795169602217 0.12029725994031518 ;
createNode polyTweak -n "polyTweak1";
	rename -uid "77BC0A0F-D54E-FF43-F768-0A8C8ED921DC";
	setAttr ".uopa" yes;
	setAttr -s 9 ".tk";
	setAttr ".tk[2]" -type "float3" -0.049253024 -5.5511151e-17 -0.083319135 ;
	setAttr ".tk[3]" -type "float3" -0.2158913 -5.5511151e-17 -0.083319135 ;
	setAttr ".tk[4]" -type "float3" -0.049253024 -5.5511151e-17 0.083319135 ;
	setAttr ".tk[5]" -type "float3" -0.2158913 -5.5511151e-17 0.083319135 ;
	setAttr ".tk[8]" -type "float3" -0.11608099 -8.8817842e-16 -0.094948485 ;
	setAttr ".tk[9]" -type "float3" -0.30597794 -8.8817842e-16 -0.094948485 ;
	setAttr ".tk[10]" -type "float3" -0.30597794 -8.8817842e-16 0.094948485 ;
	setAttr ".tk[11]" -type "float3" -0.11608099 -8.8817842e-16 0.094948485 ;
createNode polyCube -n "polyCube2";
	rename -uid "FBB4AC2C-1447-6C81-CA6A-2D8D50F90FA1";
	setAttr ".cuv" 1;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "9151C745-7343-72D6-B56E-3CA5CCE16BE4";
	setAttr ".ics" -type "componentList" 1 "f[*]";
createNode polyTweak -n "polyTweak2";
	rename -uid "08FDB949-DC49-E0D1-5B9C-CB8071E48A75";
	setAttr ".uopa" yes;
	setAttr -s 2 ".tk";
	setAttr ".tk[0]" -type "float3" -0.01376142 0 0 ;
	setAttr ".tk[6]" -type "float3" -0.01376142 0 0 ;
createNode polySubdFace -n "polySubdFace2";
	rename -uid "AFEDD2A4-6B49-876D-E1E0-2B99184BB358";
	setAttr ".ics" -type "componentList" 1 "f[0:23]";
createNode polySubdFace -n "polySubdFace3";
	rename -uid "3C911F28-8B47-EB35-7C57-9D9B2630A366";
	setAttr ".ics" -type "componentList" 1 "f[0:95]";
createNode polySubdFace -n "polySubdFace4";
	rename -uid "F7B6F8C7-EA4E-0CA6-7B58-AD941C8ACB07";
	setAttr ".ics" -type "componentList" 1 "f[0:383]";
createNode polyCylinder -n "polyCylinder1";
	rename -uid "F47D309E-4244-A237-DCD9-41809FD67B06";
	setAttr ".sa" 6;
	setAttr ".sc" 1;
	setAttr ".cuv" 3;
createNode lambert -n "western_fence_body";
	rename -uid "AB883C40-6848-854D-A10E-2EBDA93AB7EA";
	setAttr ".c" -type "float3" 0.2375 0.086300001 0.0605 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "25F25B46-FC4F-0CD6-882C-4C838DF55F74";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "445E2B26-A041-DA62-C6AF-6EAFCF89D088";
createNode lambert -n "western_fence_posts";
	rename -uid "70BCD0F0-0B44-998C-F390-7ABA8B6DF26A";
	setAttr ".c" -type "float3" 0.124 0.067012385 0.057288002 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "9379A539-2B41-DA87-4805-918D07636A23";
	setAttr ".ihi" 0;
	setAttr -s 6 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "E9B89CBF-824A-E8FF-065B-D5947CA5DDBC";
createNode lambert -n "western_fence_screws";
	rename -uid "A2DA62E2-8146-3A53-7954-3D9D48D243FC";
createNode shadingEngine -n "lambert4SG";
	rename -uid "A712C403-644F-E2A1-BF73-A49F54D47B60";
	setAttr ".ihi" 0;
	setAttr -s 9 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 9 ".gn";
createNode materialInfo -n "materialInfo3";
	rename -uid "96BEAE2A-134B-E44C-A8EC-BA95D2CE1999";
createNode polyUnite -n "polyUnite1";
	rename -uid "ABCDBB4D-9140-411F-D058-A9B0FD298BB0";
	setAttr -s 8 ".ip";
	setAttr -s 8 ".im";
createNode groupId -n "groupId1";
	rename -uid "E70400C1-BA42-2F21-F69F-F2AEEAA515D7";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "F02673AC-BB44-9FBF-E2B3-98AC56DC3872";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:13]";
createNode groupId -n "groupId2";
	rename -uid "19F09777-AD40-0616-8648-12BAAD129290";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "AFD9B670-A04D-7137-2099-88BEEDD67262";
	setAttr ".ihi" 0;
createNode groupId -n "groupId4";
	rename -uid "CBEF1C82-AB48-4214-0782-869B31347337";
	setAttr ".ihi" 0;
createNode groupId -n "groupId5";
	rename -uid "4593462D-FC4C-7F42-BB58-9CA54EEBEC42";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "B06E2C05-FA40-2BBF-2814-3EBA48FB1EDD";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:1535]";
createNode groupId -n "groupId6";
	rename -uid "2DE60B38-A047-85BF-D4A6-F0A5FD4B15D7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId7";
	rename -uid "92C3789F-0E43-71F9-E45D-01B9462C3D00";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "F18DDE72-6F42-0FF4-F547-F19683147539";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "51185B73-284C-00F4-86D2-05966C2020CD";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "14DB1815-A749-80C2-4844-74A6BDF7FCC5";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:17]";
createNode groupId -n "groupId10";
	rename -uid "B98A42E5-5F40-D6E5-5BFF-C8A7BFF88C3D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "4ACD2B4B-2D4A-B1D8-DF55-4E8C73467CFB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "19C18902-A047-52B5-F159-51ADCEDD820D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "67D239F4-794E-26CE-44F2-E782A3625338";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "FB15F42A-C04C-6DC1-1E06-399A3B6A69FF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "5E176CE1-E440-CB95-987A-99AF4FDBAD0E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "6DE36F5A-8147-D0A0-A671-E891A03EB52B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "2A2ABB09-8542-128F-014E-C7A647D10CFB";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "F03F2050-F34B-C33C-F54A-2582B484B2CA";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:27]";
createNode groupId -n "groupId18";
	rename -uid "769EC9CA-7848-FA70-AA93-1BBACE0EE882";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "6C84D76C-B248-CFC5-54CB-9EB2EA3BF4AB";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[28:1569]";
createNode groupId -n "groupId19";
	rename -uid "BA3BFC6C-B947-6B91-0EB8-5CA2A1E27047";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "524D8636-844B-300A-B9FD-EEB7A32D8A6F";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[1570:1641]";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "96456038-F94C-60D2-B827-3694D2CCEB14";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -740.47616105231157 -119.04761431709188 ;
	setAttr ".tgi[0].vh" -type "double2" 705.95235290035475 122.61904274660465 ;
	setAttr -s 6 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -62.857143402099609;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[2].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 262.85714721679688;
	setAttr ".tgi[0].ni[3].y" -62.857143402099609;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[4].y" -10;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" 262.85714721679688;
	setAttr ".tgi[0].ni[5].y" -67.142860412597656;
	setAttr ".tgi[0].ni[5].nvs" 1923;
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 5 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 7 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId1.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts1.og" "pCubeShape1.i";
connectAttr "groupId2.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId3.id" "pCubeShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupId4.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId5.id" "pCubeShape3.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupParts2.og" "pCubeShape3.i";
connectAttr "groupId6.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId7.id" "pCubeShape4.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape4.iog.og[0].gco";
connectAttr "groupId8.id" "pCubeShape4.ciog.cog[0].cgid";
connectAttr "groupId9.id" "pCylinderShape1.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape1.iog.og[0].gco";
connectAttr "groupParts3.og" "pCylinderShape1.i";
connectAttr "groupId10.id" "pCylinderShape1.ciog.cog[0].cgid";
connectAttr "groupId11.id" "pCylinderShape2.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape2.iog.og[0].gco";
connectAttr "groupId12.id" "pCylinderShape2.ciog.cog[0].cgid";
connectAttr "groupId13.id" "pCylinderShape3.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape3.iog.og[0].gco";
connectAttr "groupId14.id" "pCylinderShape3.ciog.cog[0].cgid";
connectAttr "groupId15.id" "pCylinderShape4.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCylinderShape4.iog.og[0].gco";
connectAttr "groupId16.id" "pCylinderShape4.ciog.cog[0].cgid";
connectAttr "groupParts6.og" "pCube5Shape.i";
connectAttr "groupId17.id" "pCube5Shape.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCube5Shape.iog.og[0].gco";
connectAttr "groupId18.id" "pCube5Shape.iog.og[1].gid";
connectAttr "lambert2SG.mwc" "pCube5Shape.iog.og[1].gco";
connectAttr "groupId19.id" "pCube5Shape.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCube5Shape.iog.og[2].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube1.out" "polyExtrudeFace1.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace1.mp";
connectAttr "polyTweak1.out" "polyExtrudeFace2.ip";
connectAttr "pCubeShape1.wm" "polyExtrudeFace2.mp";
connectAttr "polyExtrudeFace1.out" "polyTweak1.ip";
connectAttr "polyTweak2.out" "polySubdFace1.ip";
connectAttr "polyCube2.out" "polyTweak2.ip";
connectAttr "polySubdFace1.out" "polySubdFace2.ip";
connectAttr "polySubdFace2.out" "polySubdFace3.ip";
connectAttr "polySubdFace3.out" "polySubdFace4.ip";
connectAttr "western_fence_body.oc" "lambert2SG.ss";
connectAttr "pCubeShape3.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape4.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape4.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCube5Shape.iog.og[1]" "lambert2SG.dsm" -na;
connectAttr "groupId5.msg" "lambert2SG.gn" -na;
connectAttr "groupId6.msg" "lambert2SG.gn" -na;
connectAttr "groupId7.msg" "lambert2SG.gn" -na;
connectAttr "groupId8.msg" "lambert2SG.gn" -na;
connectAttr "groupId18.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "western_fence_body.msg" "materialInfo1.m";
connectAttr "western_fence_posts.oc" "lambert3SG.ss";
connectAttr "pCubeShape1.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCube5Shape.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "groupId1.msg" "lambert3SG.gn" -na;
connectAttr "groupId2.msg" "lambert3SG.gn" -na;
connectAttr "groupId3.msg" "lambert3SG.gn" -na;
connectAttr "groupId4.msg" "lambert3SG.gn" -na;
connectAttr "groupId17.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "western_fence_posts.msg" "materialInfo2.m";
connectAttr "western_fence_screws.oc" "lambert4SG.ss";
connectAttr "pCylinderShape1.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape1.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape2.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape2.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape3.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape3.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape4.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCylinderShape4.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCube5Shape.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "groupId9.msg" "lambert4SG.gn" -na;
connectAttr "groupId10.msg" "lambert4SG.gn" -na;
connectAttr "groupId11.msg" "lambert4SG.gn" -na;
connectAttr "groupId12.msg" "lambert4SG.gn" -na;
connectAttr "groupId13.msg" "lambert4SG.gn" -na;
connectAttr "groupId14.msg" "lambert4SG.gn" -na;
connectAttr "groupId15.msg" "lambert4SG.gn" -na;
connectAttr "groupId16.msg" "lambert4SG.gn" -na;
connectAttr "groupId19.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "western_fence_screws.msg" "materialInfo3.m";
connectAttr "pCubeShape1.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape3.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape4.o" "polyUnite1.ip[3]";
connectAttr "pCylinderShape1.o" "polyUnite1.ip[4]";
connectAttr "pCylinderShape2.o" "polyUnite1.ip[5]";
connectAttr "pCylinderShape3.o" "polyUnite1.ip[6]";
connectAttr "pCylinderShape4.o" "polyUnite1.ip[7]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape3.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape4.wm" "polyUnite1.im[3]";
connectAttr "pCylinderShape1.wm" "polyUnite1.im[4]";
connectAttr "pCylinderShape2.wm" "polyUnite1.im[5]";
connectAttr "pCylinderShape3.wm" "polyUnite1.im[6]";
connectAttr "pCylinderShape4.wm" "polyUnite1.im[7]";
connectAttr "polyExtrudeFace2.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "polySubdFace4.out" "groupParts2.ig";
connectAttr "groupId5.id" "groupParts2.gi";
connectAttr "polyCylinder1.out" "groupParts3.ig";
connectAttr "groupId9.id" "groupParts3.gi";
connectAttr "polyUnite1.out" "groupParts4.ig";
connectAttr "groupId17.id" "groupParts4.gi";
connectAttr "groupParts4.og" "groupParts5.ig";
connectAttr "groupId18.id" "groupParts5.gi";
connectAttr "groupParts5.og" "groupParts6.ig";
connectAttr "groupId19.id" "groupParts6.gi";
connectAttr "western_fence_body.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "western_fence_posts.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "western_fence_screws.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "lambert4SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "western_fence_body.msg" ":defaultShaderList1.s" -na;
connectAttr "western_fence_posts.msg" ":defaultShaderList1.s" -na;
connectAttr "western_fence_screws.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of western_fence.ma
