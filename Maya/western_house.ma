//Maya ASCII 2016 scene
//Name: western_house.ma
//Last modified: Thu, Feb 16, 2017 03:42:15 PM
//Codeset: UTF-8
requires maya "2016";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2016";
fileInfo "version" "2016";
fileInfo "cutIdentifier" "201502261600-953408";
fileInfo "osv" "Mac OS X 10.9";
fileInfo "license" "student";
createNode transform -s -n "persp";
	rename -uid "6BDC831A-9E4B-87F5-1E60-7495CD006EA3";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 23.41182722812907 20.340676098170142 -41.080173509264206 ;
	setAttr ".r" -type "double3" -15.938352731545615 3033.3999999990301 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "88F3C9ED-F345-576A-559B-C8AD43D0DAA0";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999986;
	setAttr ".coi" 47.917331748908396;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 2.781201959715919 7.1824364475224067 0.11823316216747903 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "A2873D3B-C344-4CEB-609E-4990C2F78789";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.5243819150992346 100.78539889755196 -5.2580389351252395 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "B2696A74-B641-0E7E-CFCE-ACB1A0C613D7";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 49.229103478136061;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	rename -uid "855DA87D-204E-76D9-3F88-4CB80EDF8327";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0.10762903428508308 5.3489298980794366 101.08920229482023 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "96B3E731-3B48-571D-056B-18A65712BB4F";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 4.8480823593391591;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	rename -uid "8D71AED0-D347-1B80-4087-02B7288CBB12";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.43753540309869 7.613025399280211 -0.044908341976384866 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "DFE31139-5940-8126-29BD-E0A4E4788836";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 15.433048880796051;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCube1";
	rename -uid "C4EECD3E-494C-44B3-13BE-1492A3C91A07";
	setAttr ".s" -type "double3" 5.6308752895129306 0.1427688332806494 5.6308752895129306 ;
createNode transform -n "transform199" -p "pCube1";
	rename -uid "88B42E9A-4241-B1B3-9433-48A0A6348FC9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape1" -p "transform199";
	rename -uid "37EF1621-F346-FF24-C8FF-C29CC880D6F9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube2";
	rename -uid "0E8F4C21-2F41-5C6C-7139-C7AA7638C2EF";
	setAttr ".t" -type "double3" 2.7751002287685198 2.782071097372488 2.7751002287685202 ;
	setAttr ".s" -type "double3" 0.38847142591309242 5.8234604877116869 0.38847142591309242 ;
createNode transform -n "transform198" -p "pCube2";
	rename -uid "3AA123DD-4848-8501-885F-18BFE29519F0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape2" -p "transform198";
	rename -uid "5297DD90-A042-C72F-EA63-6BB5F25DA34E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 0.39581114 0 0 0.39581114 
		0 0 0.39581114 0 0 0.39581114 0;
createNode transform -n "pCube3";
	rename -uid "CEB6F3D1-A440-10C4-2517-FB9320F6B04C";
	setAttr ".t" -type "double3" -2.7751002287685198 2.782071097372488 2.7751002287685202 ;
	setAttr ".s" -type "double3" 0.38847142591309242 5.8234604877116869 0.38847142591309242 ;
createNode transform -n "transform197" -p "pCube3";
	rename -uid "5F0D898B-0B46-2992-97F3-379D987415C4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape3" -p "transform197";
	rename -uid "9E5F53BF-CA44-A9A4-59D4-7D9A772EA446";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 0.39685401 0 0 0.39685401 
		0 0 0.39685401 0 0 0.39685401 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube5";
	rename -uid "E610B5D1-E14D-8B8A-5005-0791B4105BD0";
	setAttr ".t" -type "double3" 2.7751002287685198 2.782071097372488 -2.7751002287685211 ;
	setAttr ".s" -type "double3" 0.38847142591309242 5.8234604877116869 0.38847142591309242 ;
createNode transform -n "transform196" -p "pCube5";
	rename -uid "A561093D-5948-B141-66EE-B1939A24C5C8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape5" -p "transform196";
	rename -uid "864A583B-7B47-FE2D-2989-7E8631F1854B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 0.3998372 0 0 0.3998372 
		0 0 0.3998372 0 0 0.3998372 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube8";
	rename -uid "69EC7CEA-9046-AC08-EFFE-3E800B3FBA64";
	setAttr ".t" -type "double3" 0 4.5976412833765812 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform195" -p "pCube8";
	rename -uid "C367FBEF-F645-1E8E-D301-CF9719F76CA0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape8" -p "transform195";
	rename -uid "12F3A7E2-4349-D423-7B6A-19905CFBD233";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube9";
	rename -uid "EEC06F54-914A-6783-2E4C-22B382F08CA0";
	setAttr ".t" -type "double3" 0 3.9559502864345921 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform194" -p "pCube9";
	rename -uid "56CC56DD-FA49-11F8-3690-7197365E3E37";
	setAttr ".v" no;
createNode mesh -n "pCubeShape9" -p "transform194";
	rename -uid "ECC5026E-2A4A-DB93-8C5A-1AA50F559F2E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube10";
	rename -uid "A4B38CCD-4742-187F-1FEB-098B9B116349";
	setAttr ".t" -type "double3" 0 3.3196528525772582 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform193" -p "pCube10";
	rename -uid "FA9A49A4-2C41-DB27-5856-A99D584F245E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape10" -p "transform193";
	rename -uid "DE9E4CE5-4B47-F3A3-E736-9883C16A4C46";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube11";
	rename -uid "B4D4B95A-2940-FCE8-45B1-9ABBD6D70168";
	setAttr ".t" -type "double3" 0 2.6656804900016651 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform192" -p "pCube11";
	rename -uid "AFACF16E-2E46-5A24-564E-F7A6A720D7DC";
	setAttr ".v" no;
createNode mesh -n "pCubeShape11" -p "transform192";
	rename -uid "95E3B183-0743-176B-2057-4B8D05B84846";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube12";
	rename -uid "974F54A1-B441-9229-1C7F-57889A0D0C1A";
	setAttr ".t" -type "double3" 0 0.72666836819125358 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform191" -p "pCube12";
	rename -uid "13391B39-E34F-F276-FD72-C3A5C7B20CC4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape12" -p "transform191";
	rename -uid "23C1A596-1449-C953-04DE-24BAA8C50977";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube13";
	rename -uid "A04FFA09-9942-8AA8-B43D-41AB6200F3FD";
	setAttr ".t" -type "double3" 0 1.3806407307668467 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform190" -p "pCube13";
	rename -uid "28FE9374-CC48-F857-327E-138A6924F501";
	setAttr ".v" no;
createNode mesh -n "pCubeShape13" -p "transform190";
	rename -uid "76C64389-3345-56DF-FB56-F58694F153FB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube14";
	rename -uid "9F0E128D-4840-B2B8-3E77-C3BC91F1AD3A";
	setAttr ".t" -type "double3" 0 2.0169381646241806 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform189" -p "pCube14";
	rename -uid "31ACE1F5-7A40-3C7B-572B-EDAD4A04F2D7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape14" -p "transform189";
	rename -uid "78A545D8-174E-BF6C-3138-9ABCFFF1EC7E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube15";
	rename -uid "83243A29-0241-A511-0069-3FA681E03DB8";
	setAttr ".t" -type "double3" 0 0.1418012398697519 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform188" -p "pCube15";
	rename -uid "4277DE16-0343-10AA-010B-5782156CB325";
	setAttr ".v" no;
createNode mesh -n "pCubeShape15" -p "transform188";
	rename -uid "4B015EE0-7044-2D22-E72A-C1AEEBF45A3C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube16";
	rename -uid "F071A2C2-6141-BC5D-DA08-15873C3D10B3";
	setAttr ".s" -type "double3" 5.6308752895129306 0.1427688332806494 5.6308752895129306 ;
createNode transform -n "transform187" -p "pCube16";
	rename -uid "BCED288C-B040-991B-0B22-8C84C952F9EE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape16" -p "transform187";
	rename -uid "104CC255-A548-C27A-36AF-7EA853DED3DD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube18";
	rename -uid "D76DA667-124F-6681-0BBC-E0845399D8EA";
	setAttr ".t" -type "double3" 0 4.5976412833765812 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform186" -p "pCube18";
	rename -uid "0837F07A-9F40-4C68-EE13-E09144CC8576";
	setAttr ".v" no;
createNode mesh -n "pCubeShape18" -p "transform186";
	rename -uid "906CD58A-B941-48B1-E654-0CA37DBD1DC1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube19";
	rename -uid "8F8FBC1A-244D-7B70-C1D8-4B8631853157";
	setAttr ".t" -type "double3" 0 3.9559502864345921 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform185" -p "pCube19";
	rename -uid "134BE901-1C44-132A-F258-099F870E1851";
	setAttr ".v" no;
createNode mesh -n "pCubeShape19" -p "transform185";
	rename -uid "A8C65B99-4349-F026-00B3-03BF87DF5998";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube20";
	rename -uid "F2AF1C9A-9845-1BE7-DBB6-CBAE7E1FC1EC";
	setAttr ".t" -type "double3" 0 3.3196528525772582 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform184" -p "pCube20";
	rename -uid "18C5A279-5746-6696-6BB4-2595D285CC43";
	setAttr ".v" no;
createNode mesh -n "pCubeShape20" -p "transform184";
	rename -uid "44B76D61-A246-9D27-D92B-D6B77058E683";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube21";
	rename -uid "DFE4F214-4D42-D108-6326-2EB70B0BFB1C";
	setAttr ".t" -type "double3" 0 2.6656804900016651 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform183" -p "pCube21";
	rename -uid "5AFD0741-E141-93B4-081C-B682F24E2636";
	setAttr ".v" no;
createNode mesh -n "pCubeShape21" -p "transform183";
	rename -uid "44FFE4A9-E54A-4BD0-6716-03B8E0F1D23A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube22";
	rename -uid "653EB5F5-344E-07E2-60A2-938AFD2CD887";
	setAttr ".t" -type "double3" 0 0.72666836819125358 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform182" -p "pCube22";
	rename -uid "1C89F1E4-C849-238C-6AF4-85B500A261D0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape22" -p "transform182";
	rename -uid "F5768088-DC4B-E730-45C5-08B6F5B496F6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube23";
	rename -uid "5AB9ACE3-E144-1576-A030-8581FBDB63C3";
	setAttr ".t" -type "double3" 0 1.3806407307668467 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform181" -p "pCube23";
	rename -uid "85D95299-2645-0664-F03E-06B8881A40DC";
	setAttr ".v" no;
createNode mesh -n "pCubeShape23" -p "transform181";
	rename -uid "BDA0BF09-884E-7EE4-51E6-EB9E48C1254D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube24";
	rename -uid "12613409-3944-C759-B0E1-8192471A3A8B";
	setAttr ".t" -type "double3" 0 2.0169381646241806 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform180" -p "pCube24";
	rename -uid "CFA5A42E-AA41-17CE-DAFA-DEB2224831EF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape24" -p "transform180";
	rename -uid "30DC8353-9944-DB68-5F3A-F2B566E14653";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube25";
	rename -uid "58AC9FC6-3943-E9EF-8209-BF8740FE82EF";
	setAttr ".t" -type "double3" 0 0.1418012398697519 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform179" -p "pCube25";
	rename -uid "F52B9E69-024E-BFDA-FFE0-ABB00C4E62D6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape25" -p "transform179";
	rename -uid "1B1C9634-594E-3FA5-4CCC-3ABE61440125";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube26";
	rename -uid "C83B2D55-AB4F-4D5F-6A00-42B316A2FB9B";
	setAttr ".t" -type "double3" 0 5.2516136459521743 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform178" -p "pCube26";
	rename -uid "A42D53FE-A947-DA88-68BF-A5A839E69A32";
	setAttr ".v" no;
createNode mesh -n "pCubeShape26" -p "transform178";
	rename -uid "747353A0-394F-B525-E597-56BC1813326D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube27";
	rename -uid "F307E1B9-DE48-62C4-8997-968F57AC062B";
	setAttr ".t" -type "double3" 0 4.5976412833765812 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform177" -p "pCube27";
	rename -uid "835D55CC-FB40-0275-CAB0-459E3E64D788";
	setAttr ".v" no;
createNode mesh -n "pCubeShape27" -p "transform177";
	rename -uid "BA1CA634-6C48-A8BA-C24B-C1A82A5DB2A7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube28";
	rename -uid "D39CBE32-FE41-70FF-BF89-40B9A402EB08";
	setAttr ".t" -type "double3" 0 3.9559502864345921 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform176" -p "pCube28";
	rename -uid "60F67CDF-F64F-1525-53E4-BBB9F2B05E6F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape28" -p "transform176";
	rename -uid "B1BEDE6D-7F4E-C035-293A-63B5CDB35825";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube29";
	rename -uid "4A1B26EC-CA41-66FE-2D6D-9BA8A3F7009A";
	setAttr ".t" -type "double3" 0 3.3196528525772582 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform175" -p "pCube29";
	rename -uid "03A5C24E-1A41-E15C-4075-BBBD473A4B79";
	setAttr ".v" no;
createNode mesh -n "pCubeShape29" -p "transform175";
	rename -uid "B031373D-EF43-9F1E-F0B1-E592325CB3EE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube30";
	rename -uid "F4BD3725-E744-34F9-30DA-F883C54483DE";
	setAttr ".t" -type "double3" 0 2.6656804900016651 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform174" -p "pCube30";
	rename -uid "4700667B-2649-B78B-89A7-17868C4AAB1F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape30" -p "transform174";
	rename -uid "46FCF240-1042-A76A-841A-B9987A2E2CA1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube31";
	rename -uid "FC35F32F-9245-9236-461D-19AD7A50EDAC";
	setAttr ".t" -type "double3" 0 0.72666836819125358 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform173" -p "pCube31";
	rename -uid "0CACC4C8-B04F-A95D-709C-968C79290993";
	setAttr ".v" no;
createNode mesh -n "pCubeShape31" -p "transform173";
	rename -uid "E3016465-774C-CE52-309E-15B50102092B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube32";
	rename -uid "DB0D7317-6740-3ED4-DDCE-27A5F2B59B99";
	setAttr ".t" -type "double3" 0 1.3806407307668467 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform172" -p "pCube32";
	rename -uid "C4018EA8-C140-7491-4F53-CDA109AAA98F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape32" -p "transform172";
	rename -uid "8E6D22A2-7240-8A4A-4248-179E1657AACF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube33";
	rename -uid "90997FD9-0E4A-52CB-19F2-87822CB78576";
	setAttr ".t" -type "double3" 0 2.0169381646241806 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform171" -p "pCube33";
	rename -uid "4847EA6F-1A48-8F4C-FF63-58BB89C50F50";
	setAttr ".v" no;
createNode mesh -n "pCubeShape33" -p "transform171";
	rename -uid "EEDD6033-1745-83D9-73D6-A38CACCAED72";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube34";
	rename -uid "C1033924-F44E-9236-EECE-C7A358C4A164";
	setAttr ".t" -type "double3" 0 0.1418012398697519 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform170" -p "pCube34";
	rename -uid "6B3BF97C-1E45-D37A-0F91-34874D5ADD54";
	setAttr ".v" no;
createNode mesh -n "pCubeShape34" -p "transform170";
	rename -uid "9C209484-4A47-26E0-BE8B-829464905330";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube35";
	rename -uid "7D1275B4-F44A-E57C-00C6-7A8B1F17A02B";
	setAttr ".t" -type "double3" 0 5.2516136459521743 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform169" -p "pCube35";
	rename -uid "F61BAFE0-9A48-4251-922E-BD98B63436F3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape35" -p "transform169";
	rename -uid "54B567A4-D447-9A0E-0846-2696A9F2660C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube36";
	rename -uid "82400E9C-A74B-70C5-79FF-D380E265B583";
	setAttr ".t" -type "double3" 0 4.5976412833765812 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform168" -p "pCube36";
	rename -uid "75AF99E7-2D49-9938-0E08-8397CE0F6FA4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape36" -p "transform168";
	rename -uid "8C6395C6-DA4E-64B5-3E6F-4DA2A336872B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube37";
	rename -uid "A6EC69D5-1F4F-32A4-F679-E3AE8776B690";
	setAttr ".t" -type "double3" 0 3.9559502864345921 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform167" -p "pCube37";
	rename -uid "5AED6D8D-8A40-E41C-5565-8BB32646ABE5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape37" -p "transform167";
	rename -uid "4246B433-7941-73DB-CC9F-6384650B55BE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube38";
	rename -uid "6CF73726-8C41-7AE7-88E2-C2AEB1763D6B";
	setAttr ".t" -type "double3" 0 3.3196528525772582 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform166" -p "pCube38";
	rename -uid "236F6F81-AE4A-9E02-C2B0-BF87A9BD12DF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape38" -p "transform166";
	rename -uid "CC82AEFA-6041-57DD-8733-828C422A3C29";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube39";
	rename -uid "D2319E88-C34C-9D9E-B940-C88AF08994C7";
	setAttr ".t" -type "double3" 0 2.6656804900016651 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform165" -p "pCube39";
	rename -uid "4BC3A513-194F-9745-548A-929541E9E116";
	setAttr ".v" no;
createNode mesh -n "pCubeShape39" -p "transform165";
	rename -uid "1BB51DEA-5847-C5EE-26C5-18875F58F812";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube40";
	rename -uid "3139B116-8147-1D22-E312-EAB0177686E9";
	setAttr ".t" -type "double3" 0 0.72666836819125358 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform164" -p "pCube40";
	rename -uid "47BBFB19-034B-797D-CB87-1794A38461AE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape40" -p "transform164";
	rename -uid "3B3004F4-7D49-90CC-617E-DFB8F8024C4B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube41";
	rename -uid "E8241474-E740-28E7-A5B4-DB8C8FA7C31F";
	setAttr ".t" -type "double3" 0 1.3806407307668467 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform163" -p "pCube41";
	rename -uid "88AA43D1-A548-5C6D-AFF8-259DD6A10465";
	setAttr ".v" no;
createNode mesh -n "pCubeShape41" -p "transform163";
	rename -uid "89E1F8A3-FA43-5369-46EB-048917686998";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube42";
	rename -uid "97E0D80E-EE45-EE5C-A2E7-F193EC17B2ED";
	setAttr ".t" -type "double3" 0 2.0169381646241806 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform162" -p "pCube42";
	rename -uid "C6EAB94B-F941-AC27-BC06-EDA8A0C9F9D7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape42" -p "transform162";
	rename -uid "40F5BB28-9646-A0D2-E77E-FAB3CCC163A3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube43";
	rename -uid "1C7ED314-954E-C4F9-2408-199D8E55C8C5";
	setAttr ".t" -type "double3" 0 0.1418012398697519 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform161" -p "pCube43";
	rename -uid "B9C66ECC-E040-12BF-4D49-7792D20F3439";
	setAttr ".v" no;
createNode mesh -n "pCubeShape43" -p "transform161";
	rename -uid "23360CF0-F143-FB2D-2900-639F4DE88A91";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube45";
	rename -uid "63B8386F-9042-64E1-D667-8397D0CB4C9D";
	setAttr ".t" -type "double3" 0 4.5976412833765812 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform160" -p "pCube45";
	rename -uid "B6C53B39-1D49-07DE-DCA2-EBA439A241F4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape45" -p "transform160";
	rename -uid "823D0F31-B245-1ADC-3D91-DB8248D861D3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube46";
	rename -uid "052A45DE-5D4A-3FAD-3781-45AE8C4AFB22";
	setAttr ".t" -type "double3" 0 3.9559502864345921 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform159" -p "pCube46";
	rename -uid "38246118-4449-B1DF-BCE0-AE89B332CEFA";
	setAttr ".v" no;
createNode mesh -n "pCubeShape46" -p "transform159";
	rename -uid "A5FC5D6A-D94E-5B8E-0F76-5BADB370F88B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube47";
	rename -uid "DBFF7619-5242-9892-5B19-30870162D619";
	setAttr ".t" -type "double3" 0 3.3196528525772582 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform158" -p "pCube47";
	rename -uid "0A271DD4-864F-FA1E-63B8-9E9ADAB1F543";
	setAttr ".v" no;
createNode mesh -n "pCubeShape47" -p "transform158";
	rename -uid "9EF98B96-9A4C-F539-EB70-41BAF1EA2CC6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube48";
	rename -uid "275C4D4B-3147-E778-D38F-CC95FEFB2E77";
	setAttr ".t" -type "double3" 0 2.6656804900016651 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform157" -p "pCube48";
	rename -uid "CECDBE80-4D43-70E0-6D1E-C6AE7E2A3FCD";
	setAttr ".v" no;
createNode mesh -n "pCubeShape48" -p "transform157";
	rename -uid "461F259D-CE4D-9936-364C-8A838BAB0C98";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube49";
	rename -uid "ADBA776B-0647-BAE3-74C2-4A8760EB2BCE";
	setAttr ".t" -type "double3" 0 0.72666836819125358 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform156" -p "pCube49";
	rename -uid "FD00F540-E242-178C-D132-0C839A7E17D5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape49" -p "transform156";
	rename -uid "4D980AC5-8848-B5F3-8EDF-3F85842DC2D8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube50";
	rename -uid "7F4E3C1B-8D45-232E-E10F-E4B5C1238D65";
	setAttr ".t" -type "double3" 0 1.3806407307668467 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform155" -p "pCube50";
	rename -uid "F307D724-A24B-7C68-18A9-C69B2D41D714";
	setAttr ".v" no;
createNode mesh -n "pCubeShape50" -p "transform155";
	rename -uid "E368B24E-BF4F-79D6-89D0-FD8BFAFC159F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube51";
	rename -uid "C1E5C21C-BB44-844B-0692-CD919601D0DA";
	setAttr ".t" -type "double3" 0 2.0169381646241806 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform154" -p "pCube51";
	rename -uid "0B938149-814B-F241-38C6-C888FC0AF73C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape51" -p "transform154";
	rename -uid "E782E4C8-F547-107F-3C7A-CC8E2FD123FE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube52";
	rename -uid "5F98CF16-1443-3DED-A2AF-A8A048A3F549";
	setAttr ".t" -type "double3" 0 0.1418012398697519 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform153" -p "pCube52";
	rename -uid "7BF9929C-2C4F-54E4-12CD-51A2CB825201";
	setAttr ".v" no;
createNode mesh -n "pCubeShape52" -p "transform153";
	rename -uid "842F90A1-D740-B830-4C4A-B7A0DF0141CB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube53";
	rename -uid "1331AB3A-8F4C-9CCA-CBF7-E19B3D751404";
	setAttr ".t" -type "double3" 0 5.2516136459521743 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform152" -p "pCube53";
	rename -uid "6698B525-7F4C-BF72-F65F-F3A2EAC95B56";
	setAttr ".v" no;
createNode mesh -n "pCubeShape53" -p "transform152";
	rename -uid "E03D31EE-B447-DAE6-A851-B9B00E3099C8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube54";
	rename -uid "CF18EB3C-DF42-1D81-21AA-6EA77109B84E";
	setAttr ".t" -type "double3" 0 4.5976412833765812 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform151" -p "pCube54";
	rename -uid "02E75C1E-1940-13DF-9A86-5BB3B8CDB8A3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape54" -p "transform151";
	rename -uid "FE89D50F-1947-60C3-2CCC-C1A0176D55E3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube55";
	rename -uid "38EC66FC-9B48-FFAA-20E1-4384519CA6B9";
	setAttr ".t" -type "double3" 0 3.9559502864345921 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform150" -p "pCube55";
	rename -uid "46D2034E-3A4C-8F69-00E3-36827D6D66B9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape55" -p "transform150";
	rename -uid "E734003A-F241-7465-35AB-CE99C6BF523A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube56";
	rename -uid "65F37321-C640-D87A-2711-DE82E05368EB";
	setAttr ".t" -type "double3" 0 3.3196528525772582 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform149" -p "pCube56";
	rename -uid "D0A8A48E-3941-18B2-2166-48A186F02A91";
	setAttr ".v" no;
createNode mesh -n "pCubeShape56" -p "transform149";
	rename -uid "8ADA4CEF-2948-7890-CDC4-89AE461838A2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube57";
	rename -uid "C86DD03D-F94E-06A0-18D1-18869C4B6FD4";
	setAttr ".t" -type "double3" 0 2.6656804900016651 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform148" -p "pCube57";
	rename -uid "6594849F-7B4E-62CF-1F6D-94A15169EB26";
	setAttr ".v" no;
createNode mesh -n "pCubeShape57" -p "transform148";
	rename -uid "FEA192DD-0349-C7F3-E62B-F18042AC06D7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube58";
	rename -uid "A6017E46-ED4D-934E-30C7-8F8C18AF466D";
	setAttr ".t" -type "double3" 0 0.72666836819125358 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform147" -p "pCube58";
	rename -uid "A62B3BEC-0B48-C3D4-3CA9-D6A40E2DEA81";
	setAttr ".v" no;
createNode mesh -n "pCubeShape58" -p "transform147";
	rename -uid "B43A3B03-DE49-8FD2-6E22-A8895BF15DFC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube59";
	rename -uid "9E4A19A8-F242-02F8-3287-9285E276C7D1";
	setAttr ".t" -type "double3" 0 1.3806407307668467 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform146" -p "pCube59";
	rename -uid "35BDA008-AD4F-36E5-AE8C-15B822CD2D07";
	setAttr ".v" no;
createNode mesh -n "pCubeShape59" -p "transform146";
	rename -uid "AB0B0FF5-AA41-8F12-AA91-AE8A29695DFC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube60";
	rename -uid "12C98683-F341-1738-E992-57AA7655A63B";
	setAttr ".t" -type "double3" 0 2.0169381646241806 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform145" -p "pCube60";
	rename -uid "98CE9A45-3342-D086-E308-389B0DD31774";
	setAttr ".v" no;
createNode mesh -n "pCubeShape60" -p "transform145";
	rename -uid "45948C81-C84D-1BB3-8D4C-669149A4AA2F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube61";
	rename -uid "1D0E4566-114F-4C2A-E2C6-CDA137ADC85F";
	setAttr ".t" -type "double3" 0 0.1418012398697519 2.8429915448841276 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform144" -p "pCube61";
	rename -uid "71CAFBDB-0642-ED76-B2ED-F68DADE114AE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape61" -p "transform144";
	rename -uid "CF09A751-E946-6A4B-CDD6-6EA8E1BE3262";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube62";
	rename -uid "B8743711-944C-D0C8-9648-6A9C2B428FF5";
	setAttr ".t" -type "double3" 0 5.2516136459521743 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform143" -p "pCube62";
	rename -uid "965CB59D-9F47-B9B8-6D22-07BD4EA1A84C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape62" -p "transform143";
	rename -uid "EE8540D8-BF44-52AE-72C5-41B219FFFC40";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube63";
	rename -uid "551C68CB-4B4A-BC95-416F-AABDA56E8D64";
	setAttr ".t" -type "double3" 0 4.5976412833765812 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform142" -p "pCube63";
	rename -uid "C18E3E29-A34F-870D-FAE1-7B8B130D4204";
	setAttr ".v" no;
createNode mesh -n "pCubeShape63" -p "transform142";
	rename -uid "092F168A-1A4E-9819-6C26-4D9CA0E10E22";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube64";
	rename -uid "91B99340-5D47-A494-0FF6-A9ACB1267554";
	setAttr ".t" -type "double3" 0 3.9559502864345921 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform141" -p "pCube64";
	rename -uid "ECE81F46-4443-099A-2BB8-CFB6B755DA38";
	setAttr ".v" no;
createNode mesh -n "pCubeShape64" -p "transform141";
	rename -uid "A460ECF6-0C49-EBAE-D291-778097CDD58A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube65";
	rename -uid "73996607-1A45-5B6B-201E-C0926EDE77BC";
	setAttr ".t" -type "double3" 0 3.3196528525772582 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform140" -p "pCube65";
	rename -uid "E2E735D0-B74B-91B2-9907-6E871180621E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape65" -p "transform140";
	rename -uid "A0C1C41A-EB4E-1131-1448-D690815BE702";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube66";
	rename -uid "80DE1CB0-5048-152E-C552-EB856A9B1908";
	setAttr ".t" -type "double3" 0 2.6656804900016651 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform139" -p "pCube66";
	rename -uid "4A46119F-3148-EC29-DD11-4A983548B566";
	setAttr ".v" no;
createNode mesh -n "pCubeShape66" -p "transform139";
	rename -uid "DB795CCC-024B-B32F-9381-9A9216D02F01";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube67";
	rename -uid "A2F2DFFA-FB42-8D32-4A8C-FCB8B9D67707";
	setAttr ".t" -type "double3" 0 0.72666836819125358 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform138" -p "pCube67";
	rename -uid "52D40435-204B-26EE-04BC-63A047A2D0DC";
	setAttr ".v" no;
createNode mesh -n "pCubeShape67" -p "transform138";
	rename -uid "EC414A30-E741-4678-C7F9-EF9EB44ECEE7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube68";
	rename -uid "27E4E575-1742-D98D-32A2-40A95608E09B";
	setAttr ".t" -type "double3" 0 1.3806407307668467 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform137" -p "pCube68";
	rename -uid "202FBB65-4D44-C294-D70E-D49D78634562";
	setAttr ".v" no;
createNode mesh -n "pCubeShape68" -p "transform137";
	rename -uid "E89D0BE5-824F-9DB4-3D64-28AF4DF5A8FA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube69";
	rename -uid "EBE00ABE-D940-1395-EAE5-A89C99A01530";
	setAttr ".t" -type "double3" 0 2.0169381646241806 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform136" -p "pCube69";
	rename -uid "7CAE41C9-4545-E0DF-8661-5DAAABC05FFE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape69" -p "transform136";
	rename -uid "EE23275F-D04C-271B-C5B6-BF95FB501242";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube70";
	rename -uid "8802A2DE-FC47-BB0B-93D4-A3B9EB60D681";
	setAttr ".t" -type "double3" 0 0.1418012398697519 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform135" -p "pCube70";
	rename -uid "90126CDB-DC4B-7DFB-3FE8-839743C63E67";
	setAttr ".v" no;
createNode mesh -n "pCubeShape70" -p "transform135";
	rename -uid "BE285779-394F-DD30-3DE9-9F94F7B91ACE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube71";
	rename -uid "D5E35323-3A40-96AE-9A46-64AF46F47167";
	setAttr ".t" -type "double3" 0 5.2516136459521743 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform134" -p "pCube71";
	rename -uid "9CB4D4A5-834F-F422-5928-259F12E26EF9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape71" -p "transform134";
	rename -uid "E5AD0087-704E-747E-735A-22B983F8FB0E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube72";
	rename -uid "0947E6C1-8742-326B-EF19-23AB7165C33E";
	setAttr ".t" -type "double3" 0 4.5976412833765812 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform133" -p "pCube72";
	rename -uid "838A7D71-EA40-A4DC-5A9B-33BA52C21C5C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape72" -p "transform133";
	rename -uid "2FF42E49-E34C-B11F-A7A3-06A1434CDBD0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube73";
	rename -uid "4EA2AF24-2E45-247C-501E-F59EC592CD3E";
	setAttr ".t" -type "double3" 0 3.9559502864345921 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform132" -p "pCube73";
	rename -uid "6C1998B0-A44F-AAF9-C5F7-8DA2BF27B51D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape73" -p "transform132";
	rename -uid "A7467E7D-E14E-7026-6EDA-2EBE456B6B3B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube74";
	rename -uid "4C76768F-2848-5CE2-AEE7-C68CB109CB00";
	setAttr ".t" -type "double3" 0 3.3196528525772582 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform131" -p "pCube74";
	rename -uid "F22114AF-6846-6239-7CDF-1C9192F2CA76";
	setAttr ".v" no;
createNode mesh -n "pCubeShape74" -p "transform131";
	rename -uid "5D067352-884A-5D3B-6AAB-BFB8B1AE2502";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube75";
	rename -uid "20CD2E49-8548-748E-CB1C-38A167344521";
	setAttr ".t" -type "double3" 0 2.6656804900016651 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform130" -p "pCube75";
	rename -uid "A54C482E-EC46-1B01-B02E-D697E53D17E5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape75" -p "transform130";
	rename -uid "731548EF-FE44-34C7-D160-1DBA548C4045";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube76";
	rename -uid "8CA3E042-3D46-7FE6-EFA8-9ABD2D29EDBA";
	setAttr ".t" -type "double3" 0 0.72666836819125358 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform129" -p "pCube76";
	rename -uid "4DE340B2-B24D-42E6-FAD3-1A8CB8FC4403";
	setAttr ".v" no;
createNode mesh -n "pCubeShape76" -p "transform129";
	rename -uid "0B28CA96-1C40-94D4-5383-EAA759043D57";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube77";
	rename -uid "404EDB2F-1A4B-7843-30E4-A98DE95CFFF0";
	setAttr ".t" -type "double3" 0 1.3806407307668467 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform128" -p "pCube77";
	rename -uid "AADFB865-C74F-2E04-ED82-0D97B6C02CF8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape77" -p "transform128";
	rename -uid "C1B79BE8-474C-A926-39D4-93B9D22AAE39";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube78";
	rename -uid "6851295B-1F46-D2E1-6327-D9A7DE037123";
	setAttr ".t" -type "double3" 0 2.0169381646241806 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform127" -p "pCube78";
	rename -uid "7375BC5D-4642-CCCE-B837-E4ABF3DB5A6E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape78" -p "transform127";
	rename -uid "2902BD09-464C-D8BA-2AB8-84AECC4722EC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube79";
	rename -uid "6507E3F1-F148-618E-A82A-5BAEB6EB8F19";
	setAttr ".t" -type "double3" 0 0.1418012398697519 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform126" -p "pCube79";
	rename -uid "0DE7FE2B-7B42-4079-20B6-F5A0038891A8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape79" -p "transform126";
	rename -uid "27B807BB-1344-EABF-8F64-2583E559C66F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube81";
	rename -uid "CAE4C8E6-FD49-950D-BB33-BEA71B2F3DC2";
	setAttr ".t" -type "double3" 2.7839880294892545 4.5976412833765812 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform125" -p "pCube81";
	rename -uid "8187943A-C14D-23E3-060E-A4BB0FF2AB8F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape81" -p "transform125";
	rename -uid "17E0C8F4-CE42-896F-ED94-C28A9ED86D42";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube82";
	rename -uid "66CB6E62-1045-B7DD-FA06-85AD4DEF32FF";
	setAttr ".t" -type "double3" 2.7839880294892545 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform124" -p "pCube82";
	rename -uid "3A57F2A8-CC4E-1307-DD29-C58E9D0BACF4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape82" -p "transform124";
	rename -uid "558B2636-9448-4A94-C404-579098FC6A5F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube83";
	rename -uid "718EB82A-DA41-8F31-5242-C9A5B036D673";
	setAttr ".t" -type "double3" 2.7839880294892545 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform123" -p "pCube83";
	rename -uid "7E0784DE-AD4E-3AD5-41A4-4880D4ED69DC";
	setAttr ".v" no;
createNode mesh -n "pCubeShape83" -p "transform123";
	rename -uid "89C60557-8A4F-F3A8-C528-62BA4E715A22";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube84";
	rename -uid "F409A2DF-2041-151D-C970-C69AB844D08D";
	setAttr ".t" -type "double3" 2.7839880294892545 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform122" -p "pCube84";
	rename -uid "454FA860-364F-CA51-FA87-A1A0B9385F22";
	setAttr ".v" no;
createNode mesh -n "pCubeShape84" -p "transform122";
	rename -uid "5A02E6DB-CC4A-E01D-F79E-0780A451C2E1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube85";
	rename -uid "FA2D38F2-BB40-0412-C207-F4BFA39B6F2B";
	setAttr ".t" -type "double3" 2.7839880294892545 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform121" -p "pCube85";
	rename -uid "944AD20E-B347-5809-46EA-21A03BCC4471";
	setAttr ".v" no;
createNode mesh -n "pCubeShape85" -p "transform121";
	rename -uid "3AF23C03-0D43-415E-477D-C791FC6AAF23";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube86";
	rename -uid "F34232B4-AA48-FCE9-C4BB-2D9FF65DEC1B";
	setAttr ".t" -type "double3" 2.7839880294892545 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform120" -p "pCube86";
	rename -uid "FB58819E-A54F-0B06-9FF3-A7B7B1EEC141";
	setAttr ".v" no;
createNode mesh -n "pCubeShape86" -p "transform120";
	rename -uid "1768932A-BF49-763E-7DA6-A5B6DB6E7DF2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube87";
	rename -uid "8CE3916B-6F4D-43E5-092E-C3844063F950";
	setAttr ".t" -type "double3" 2.7839880294892545 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform119" -p "pCube87";
	rename -uid "2B844D26-924B-8EEA-E8DB-D8B1894BCE57";
	setAttr ".v" no;
createNode mesh -n "pCubeShape87" -p "transform119";
	rename -uid "DFC50FE0-D346-047E-6CA7-4CAAAB896A9F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube88";
	rename -uid "85617BDC-FF43-B886-8985-D3805188AEF0";
	setAttr ".t" -type "double3" 2.7839880294892545 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform118" -p "pCube88";
	rename -uid "A83AECF0-A140-58BE-CD5A-10BB34156622";
	setAttr ".v" no;
createNode mesh -n "pCubeShape88" -p "transform118";
	rename -uid "02F098E3-D540-DBCA-17CC-9E8836F6256C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube90";
	rename -uid "C457C7B2-464C-086F-59FE-F1BE440E779F";
	setAttr ".t" -type "double3" 2.7839880294892545 4.5976412833765812 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform117" -p "pCube90";
	rename -uid "F81979D2-6440-9CC0-115F-0A98354B4D3F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape90" -p "transform117";
	rename -uid "BA66C1D6-7E4C-2D55-C3AD-BD8C8A9C1BF4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube91";
	rename -uid "73C28B22-C94F-F037-0E0B-C49BB086E94D";
	setAttr ".t" -type "double3" 2.7839880294892545 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform116" -p "pCube91";
	rename -uid "568DBEBE-E241-9CF2-AF2C-30903BFAE40F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape91" -p "transform116";
	rename -uid "B22C563E-6B46-B35C-AC41-15A1369AC5C3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube92";
	rename -uid "82368411-3744-A098-F178-049B56057357";
	setAttr ".t" -type "double3" 2.7839880294892545 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform115" -p "pCube92";
	rename -uid "B7964EA2-C44A-A932-B967-88934385E0E9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape92" -p "transform115";
	rename -uid "81249158-1E46-2521-1B9F-D9911692B07D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube93";
	rename -uid "553EA5F4-5846-B60A-6A1B-7D8D6E5BF130";
	setAttr ".t" -type "double3" 2.7839880294892545 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform114" -p "pCube93";
	rename -uid "586184B1-D04E-AB32-558F-1EA4FA94CDCD";
	setAttr ".v" no;
createNode mesh -n "pCubeShape93" -p "transform114";
	rename -uid "AF873870-2F42-93E8-7628-FEB2DD1FA2E4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube94";
	rename -uid "8FF03C10-8948-A93A-FD82-46B097BCE529";
	setAttr ".t" -type "double3" 2.7839880294892545 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform113" -p "pCube94";
	rename -uid "66E30B0B-EC4B-BE49-53C8-1BA1495D4443";
	setAttr ".v" no;
createNode mesh -n "pCubeShape94" -p "transform113";
	rename -uid "7BDE0F70-8A49-BAC0-CEA5-57BC5068652B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube95";
	rename -uid "D50D9277-4B48-183A-0520-6D8E50990BC5";
	setAttr ".t" -type "double3" 2.7839880294892545 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform112" -p "pCube95";
	rename -uid "8F1C00A1-1848-2C9A-B01B-FA867A90F8D2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape95" -p "transform112";
	rename -uid "E12C7B60-C04A-B197-BE90-E5BA7289A2DF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube96";
	rename -uid "D60E41B1-C245-B6AA-2E1D-EF8125F17783";
	setAttr ".t" -type "double3" 2.7839880294892545 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform111" -p "pCube96";
	rename -uid "927A44D7-9A4A-5A8B-7375-A786A1AAF378";
	setAttr ".v" no;
createNode mesh -n "pCubeShape96" -p "transform111";
	rename -uid "64AF5D33-D74E-A352-66F7-1EB430706A91";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube97";
	rename -uid "6896AFB2-5D44-9365-C484-9C8BBE225D79";
	setAttr ".t" -type "double3" 2.7839880294892545 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform110" -p "pCube97";
	rename -uid "D9069CA7-5347-56E3-231C-F1BC0DE94CBB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape97" -p "transform110";
	rename -uid "AE9224F5-AB48-3AFE-F89B-988FCAB02FB7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube99";
	rename -uid "A89168CA-1448-AF76-E373-22ADD0CE6254";
	setAttr ".t" -type "double3" 2.7839880294892545 4.5976412833765812 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform109" -p "pCube99";
	rename -uid "6303FFBE-E846-B28F-53D1-4E8A8382494A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape99" -p "transform109";
	rename -uid "1FAD0DF2-634E-C32E-7E97-8E8975EF6F4C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube100";
	rename -uid "DE4CE737-4640-F018-B271-A0B07ADBA774";
	setAttr ".t" -type "double3" 2.7839880294892545 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform108" -p "pCube100";
	rename -uid "42AC6FD4-0242-BD17-0444-07B0F82F738D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape100" -p "transform108";
	rename -uid "499A5842-C44F-1B5D-2DC9-A5B13129290A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube101";
	rename -uid "766F00F4-484F-E8D6-6395-6A99BE12561E";
	setAttr ".t" -type "double3" 2.7839880294892545 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform107" -p "pCube101";
	rename -uid "063DBC3E-444F-37DE-C4AD-CFA69D591C8A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape101" -p "transform107";
	rename -uid "30F20183-F448-B44C-35EF-1BB9EA733CF3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube102";
	rename -uid "162D45A7-7E48-558A-2DD4-C19C1D4DD09E";
	setAttr ".t" -type "double3" 2.7839880294892545 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform106" -p "pCube102";
	rename -uid "A94C2B7A-EC41-F051-13A6-4FAB603019B0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape102" -p "transform106";
	rename -uid "11FA4EDB-464C-27A9-BD92-F2BD4889EC1F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube103";
	rename -uid "42E40DFD-DD4A-11FA-033D-D59D6319380A";
	setAttr ".t" -type "double3" 2.7839880294892545 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform105" -p "pCube103";
	rename -uid "93C14284-AA4C-DB10-FE16-0589D4C6F36E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape103" -p "transform105";
	rename -uid "B78505E8-C44A-870F-DBA3-87B87B337D44";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube104";
	rename -uid "9C52B3D3-2342-C55B-AEF4-9281BD9B0E3A";
	setAttr ".t" -type "double3" 2.7839880294892545 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform104" -p "pCube104";
	rename -uid "29DD72CC-934E-6AE4-E14A-BCABAE356B8E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape104" -p "transform104";
	rename -uid "37DFB5E8-8F49-6DC1-9513-A1BB2944B855";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube105";
	rename -uid "7FECC838-3041-E515-1B06-26905850339D";
	setAttr ".t" -type "double3" 2.7839880294892545 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform103" -p "pCube105";
	rename -uid "D3DAB727-DB42-A947-18F0-37AE89896426";
	setAttr ".v" no;
createNode mesh -n "pCubeShape105" -p "transform103";
	rename -uid "B2EE08D3-294A-E11D-99F2-D8B5B946D56A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube106";
	rename -uid "F60B0716-984F-3B54-5F59-35A958BBCF06";
	setAttr ".t" -type "double3" 2.7839880294892545 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform102" -p "pCube106";
	rename -uid "B4D23BF8-534A-7908-5B5F-6083A37D2012";
	setAttr ".v" no;
createNode mesh -n "pCubeShape106" -p "transform102";
	rename -uid "B74A8C1E-244C-35D5-30AE-7BA7B23C2701";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube107";
	rename -uid "CAC89929-D949-28B0-5D17-4E97A5E3857F";
	setAttr ".t" -type "double3" 2.7839880294892545 5.2516136459521743 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform101" -p "pCube107";
	rename -uid "5732DD24-914D-CFE4-0C77-06AB99784CFF";
	setAttr ".v" no;
createNode mesh -n "pCubeShape107" -p "transform101";
	rename -uid "1A5B60B2-A446-35A8-9EF7-7F9B727829B4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube108";
	rename -uid "4C015976-024B-DB62-0A67-4EB929296A67";
	setAttr ".t" -type "double3" 2.7839880294892545 4.5976412833765812 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform100" -p "pCube108";
	rename -uid "10B133F6-6D4B-667B-9D01-18B560F6F115";
	setAttr ".v" no;
createNode mesh -n "pCubeShape108" -p "transform100";
	rename -uid "11AB4D20-6844-A2CA-A4C0-9C84B1D45A01";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube109";
	rename -uid "486B44C3-A84C-C4F3-8D69-5D83B5AB042E";
	setAttr ".t" -type "double3" 2.7839880294892545 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform99" -p "pCube109";
	rename -uid "971B424A-D84D-5DD9-A856-D2A13836020F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape109" -p "transform99";
	rename -uid "A9B8B946-2949-1787-D747-7B8C4A1C224E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube110";
	rename -uid "52B95343-E241-07ED-3DFD-8C90B1A96046";
	setAttr ".t" -type "double3" 2.7839880294892545 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform98" -p "pCube110";
	rename -uid "76E27870-4E47-AF49-A9BA-67BB160D1FE1";
	setAttr ".v" no;
createNode mesh -n "pCubeShape110" -p "transform98";
	rename -uid "56F0743A-D04A-8F99-2F65-549DBA58F14B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube111";
	rename -uid "FF88E169-AF4D-BF1B-B881-34B2F5152991";
	setAttr ".t" -type "double3" 2.7839880294892545 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform97" -p "pCube111";
	rename -uid "06E48CDB-3B4A-5000-FAD9-528C7F1000D6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape111" -p "transform97";
	rename -uid "C5CB0F8C-C347-3D47-EA73-778B48CB7955";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube112";
	rename -uid "3F7E3598-AA45-DC78-E20E-91A4AC0A83D9";
	setAttr ".t" -type "double3" 2.7839880294892545 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform96" -p "pCube112";
	rename -uid "0B8C1C10-A145-8B7A-1296-46BB5032FFEB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape112" -p "transform96";
	rename -uid "FA3961DE-C749-E6E8-C4D4-2E80053D79EE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube113";
	rename -uid "8EF40C13-AC48-4E6F-3634-11A95AB7C1FC";
	setAttr ".t" -type "double3" 2.7839880294892545 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform95" -p "pCube113";
	rename -uid "CA453FEA-884A-4AAF-B073-66804C769E37";
	setAttr ".v" no;
createNode mesh -n "pCubeShape113" -p "transform95";
	rename -uid "734D9474-3B43-1570-BC1F-5BABF726EDAC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube114";
	rename -uid "F8CC3AD0-FB4B-654D-873C-09A34B3D89E9";
	setAttr ".t" -type "double3" 2.7839880294892545 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform94" -p "pCube114";
	rename -uid "F0035CE8-F548-82F3-C7FC-238EA9719DA0";
	setAttr ".v" no;
createNode mesh -n "pCubeShape114" -p "transform94";
	rename -uid "020EECDB-2B47-3848-E8DA-9E83FC5DBB39";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube115";
	rename -uid "A92AB63A-B844-3634-6F7B-7388A27F0E42";
	setAttr ".t" -type "double3" 2.7839880294892545 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform93" -p "pCube115";
	rename -uid "546AED3C-0A40-82F1-7A5F-FB97EB7C0EFB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape115" -p "transform93";
	rename -uid "6D8A5690-7D43-9652-1A6F-4287530FBC84";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube118";
	rename -uid "A93C5154-1346-ECF2-7042-8B956E150043";
	setAttr ".t" -type "double3" -2.8526219121160175 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform92" -p "pCube118";
	rename -uid "36C93B09-8B49-69A4-4D47-9D85A74CDAB5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape118" -p "transform92";
	rename -uid "9E00B801-DA4B-E238-7CAD-4881A9CCD6E3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube119";
	rename -uid "00FF8201-0C4A-A564-9CAC-1295CF02CA50";
	setAttr ".t" -type "double3" -2.8526219121160175 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform91" -p "pCube119";
	rename -uid "276A6BBD-C343-B728-C0D4-2297B67195B5";
	setAttr ".v" no;
createNode mesh -n "pCubeShape119" -p "transform91";
	rename -uid "466ADFB3-4146-F801-E774-03BF06907E23";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube120";
	rename -uid "4CFBF934-B640-58AB-F5D4-DA9089F1D17C";
	setAttr ".t" -type "double3" -2.8526219121160175 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform90" -p "pCube120";
	rename -uid "2F61ED79-CB4C-C09E-C58C-EE8F55BCFF4C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape120" -p "transform90";
	rename -uid "61CBFF9A-054A-5F0D-981F-18A2FDC8AFAE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube121";
	rename -uid "04ED2116-A848-EBB1-2B8B-B9831A0D297F";
	setAttr ".t" -type "double3" -2.8526219121160175 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform89" -p "pCube121";
	rename -uid "4CA3C4F1-0A41-DC5D-25A1-B8B93FF9C698";
	setAttr ".v" no;
createNode mesh -n "pCubeShape121" -p "transform89";
	rename -uid "EFB1AB69-7148-F368-E260-70A9B18C63B9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube122";
	rename -uid "90935982-EA4B-90B6-C6B7-D2BCF0C8D87B";
	setAttr ".t" -type "double3" -2.8526219121160175 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform88" -p "pCube122";
	rename -uid "1F8CFF7F-314B-736D-14DB-A3BAD2D23F4C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape122" -p "transform88";
	rename -uid "9F7CD0B0-664A-B86B-BB17-8E8DFF92AEB8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube123";
	rename -uid "120E7655-DD42-5AC1-F14A-8693F73FD23C";
	setAttr ".t" -type "double3" -2.8526219121160175 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform87" -p "pCube123";
	rename -uid "5EA3206F-C449-8858-FD01-1CA2A24EF1D2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape123" -p "transform87";
	rename -uid "1D1F4770-F343-A38B-070E-8A84F8673CF3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube124";
	rename -uid "D9F6F5C5-084D-94ED-EF35-07AB1CFF40EB";
	setAttr ".t" -type "double3" -2.8526219121160175 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform86" -p "pCube124";
	rename -uid "A4AA7EBE-4C4F-07E7-DD6B-658911480A2F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape124" -p "transform86";
	rename -uid "08C90477-254C-8919-77D2-D5A04CC9340B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube127";
	rename -uid "A1AD4661-F344-C09B-EB93-609B9CEBFDB8";
	setAttr ".t" -type "double3" -2.8526219121160175 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform85" -p "pCube127";
	rename -uid "2CC13366-0540-84B6-B977-E6B3886C5F88";
	setAttr ".v" no;
createNode mesh -n "pCubeShape127" -p "transform85";
	rename -uid "41A8CA2F-5547-5C2F-3F03-8E80E2EB0E49";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube128";
	rename -uid "E4512729-9A43-8578-8596-10BC305D9818";
	setAttr ".t" -type "double3" -2.8526219121160175 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform84" -p "pCube128";
	rename -uid "AF9CE1AA-4642-A34E-0E9D-5B87CE1D3561";
	setAttr ".v" no;
createNode mesh -n "pCubeShape128" -p "transform84";
	rename -uid "373D517D-B04C-A44C-6486-12B1ED727E39";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube129";
	rename -uid "9B570290-0544-5FE1-3F16-F89A1F0C0272";
	setAttr ".t" -type "double3" -2.8526219121160175 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform83" -p "pCube129";
	rename -uid "74B3DD31-2142-586A-33E7-D0B1A6BACDCE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape129" -p "transform83";
	rename -uid "40713A21-8C41-C7CD-8066-5CAF02A8645A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube130";
	rename -uid "088A559D-B445-79C7-2C91-908DED15DFAB";
	setAttr ".t" -type "double3" -2.8526219121160175 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform82" -p "pCube130";
	rename -uid "F0CC0763-4A4B-DF9E-9621-399124D3C56D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape130" -p "transform82";
	rename -uid "D92E5E9F-F04A-6405-72D4-BCBAE3238AA8";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube131";
	rename -uid "8824C5E2-CB47-D73C-A134-B692526547B2";
	setAttr ".t" -type "double3" -2.8526219121160175 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform81" -p "pCube131";
	rename -uid "C5B44DA1-C644-515A-60B1-BCA6C47023F2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape131" -p "transform81";
	rename -uid "C0EFD6FB-D149-4EF7-1289-9E99D008F5CB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube132";
	rename -uid "52664A72-C146-D5A6-AA5B-9EA0A2AB5A8D";
	setAttr ".t" -type "double3" -2.8526219121160175 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform80" -p "pCube132";
	rename -uid "75F0EE6E-724F-5939-24FE-48A578CB5422";
	setAttr ".v" no;
createNode mesh -n "pCubeShape132" -p "transform80";
	rename -uid "D9A93644-3343-2DF3-4520-978F99FEE945";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube133";
	rename -uid "51B9C2E1-024B-4677-C0EC-87B194C68A6C";
	setAttr ".t" -type "double3" -2.8526219121160175 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform79" -p "pCube133";
	rename -uid "98928B44-4E4E-448F-183A-79AB61E495E6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape133" -p "transform79";
	rename -uid "654C5032-7A45-1D0B-7EBE-16A0DF0F03D6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube136";
	rename -uid "70828D19-4A47-C134-4B11-E2A70400D5F3";
	setAttr ".t" -type "double3" -2.8526219121160175 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform78" -p "pCube136";
	rename -uid "CBABC319-E849-C262-53E0-FD9A557410FD";
	setAttr ".v" no;
createNode mesh -n "pCubeShape136" -p "transform78";
	rename -uid "E2B46F4A-7A4B-97D8-C0DF-769F668D6DCB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube137";
	rename -uid "5CA5865E-D140-09A6-6484-D88796C7F3B8";
	setAttr ".t" -type "double3" -2.8526219121160175 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform77" -p "pCube137";
	rename -uid "3C969959-594A-A394-0976-1B92BCDEF364";
	setAttr ".v" no;
createNode mesh -n "pCubeShape137" -p "transform77";
	rename -uid "9ADCBEFC-E54A-FCAE-6E53-7EBEAB61579D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube138";
	rename -uid "EAC8E1E2-1444-5E97-5BEF-CBB09E88FD29";
	setAttr ".t" -type "double3" -2.8526219121160175 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform76" -p "pCube138";
	rename -uid "2D7CA12A-4E40-8796-2729-2F9E63F61807";
	setAttr ".v" no;
createNode mesh -n "pCubeShape138" -p "transform76";
	rename -uid "CDEC3CE7-DC4B-FDEA-B07E-49BEE4D83CF2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube139";
	rename -uid "500634BE-7C45-D174-715B-3482D3B4FFA9";
	setAttr ".t" -type "double3" -2.8526219121160175 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform75" -p "pCube139";
	rename -uid "EF5635F4-C84D-0F9A-FB1C-A0B67C59A069";
	setAttr ".v" no;
createNode mesh -n "pCubeShape139" -p "transform75";
	rename -uid "3A6BC6F6-014C-8928-0636-00AB0248DA39";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube140";
	rename -uid "D681AC58-264F-799A-7561-3CAB3A493F80";
	setAttr ".t" -type "double3" -2.8526219121160175 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform74" -p "pCube140";
	rename -uid "1EBB30F3-4041-AAC7-2A0B-2A8E9B43CB01";
	setAttr ".v" no;
createNode mesh -n "pCubeShape140" -p "transform74";
	rename -uid "4F565C33-FC49-AABB-8394-09A5370E7E2E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube141";
	rename -uid "C21E57C5-8F4B-B2B1-765C-B79F02BC7847";
	setAttr ".t" -type "double3" -2.8526219121160175 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform73" -p "pCube141";
	rename -uid "0C794D49-D147-9D6F-C6D4-39A47B4126D2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape141" -p "transform73";
	rename -uid "A7A696D0-CD41-BB45-E6B2-0E9CAFE003E9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube142";
	rename -uid "E48C0F5B-8F4C-574C-38A1-F0B8AB344971";
	setAttr ".t" -type "double3" -2.8526219121160175 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform72" -p "pCube142";
	rename -uid "4B7981C3-A14B-AC1D-BB4D-BD953FB0F4B1";
	setAttr ".v" no;
createNode mesh -n "pCubeShape142" -p "transform72";
	rename -uid "C883F346-A14E-1639-A542-25BF66D77820";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube143";
	rename -uid "E1E17DB3-974E-0670-7795-DC94C432C388";
	setAttr ".t" -type "double3" -2.8526219121160175 5.2516136459521743 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform71" -p "pCube143";
	rename -uid "BC1AAB06-6E49-88C8-BC56-A99DF803C78A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape143" -p "transform71";
	rename -uid "FC9A4665-8D47-83A1-621D-7CAC8B97B3E6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube144";
	rename -uid "4F75F48F-524E-374E-D41D-25AE7890EC13";
	setAttr ".t" -type "double3" -2.8526219121160175 4.5976412833765812 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform70" -p "pCube144";
	rename -uid "AD38D570-8847-9BF5-2B9A-6589F858F294";
	setAttr ".v" no;
createNode mesh -n "pCubeShape144" -p "transform70";
	rename -uid "FEFA956C-2A41-A9BB-D098-168C5E6506B7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube145";
	rename -uid "8A6983F5-8046-5868-4FB8-B0BDA0A27DF8";
	setAttr ".t" -type "double3" -2.8526219121160175 3.9559502864345921 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform69" -p "pCube145";
	rename -uid "DF598B4B-C440-2842-839C-C48C7C77DA89";
	setAttr ".v" no;
createNode mesh -n "pCubeShape145" -p "transform69";
	rename -uid "6E2BA741-B84B-F553-B4C1-26800A5156B3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube146";
	rename -uid "869CA3B0-B343-6EBF-272E-CE93166490BC";
	setAttr ".t" -type "double3" -2.8526219121160175 3.3196528525772582 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform68" -p "pCube146";
	rename -uid "14E36FDB-2C47-E6E3-EA06-28935AB9520A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape146" -p "transform68";
	rename -uid "157BDBF3-A649-1A75-D0DC-00BB8BC14499";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube147";
	rename -uid "3CFEEC8E-C24F-C533-BCA8-8A9592318573";
	setAttr ".t" -type "double3" -2.8526219121160175 2.6656804900016651 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform67" -p "pCube147";
	rename -uid "267E186A-3546-BF64-ED83-46B514F31721";
	setAttr ".v" no;
createNode mesh -n "pCubeShape147" -p "transform67";
	rename -uid "EC6FE57A-AF49-D3BC-9304-A487029B69B3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube148";
	rename -uid "A0E5D013-B247-07D9-D200-99ABE907EC1B";
	setAttr ".t" -type "double3" -2.8526219121160175 0.72666836819125358 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform66" -p "pCube148";
	rename -uid "A2D3BA09-E74F-EBBE-75CD-AB977FED773F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape148" -p "transform66";
	rename -uid "37502CC6-DF43-D6B8-51C5-7F9B501BD6BE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube149";
	rename -uid "88ACCA50-D545-69EF-A131-33A060261D3F";
	setAttr ".t" -type "double3" -2.8526219121160175 1.3806407307668467 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform65" -p "pCube149";
	rename -uid "25945BED-1843-9F64-1BB0-829C2E8ACD34";
	setAttr ".v" no;
createNode mesh -n "pCubeShape149" -p "transform65";
	rename -uid "E6F17028-E146-95E8-61B1-A4943FF647DF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube150";
	rename -uid "F5A80EC0-EC45-B12F-1284-B397017C56D0";
	setAttr ".t" -type "double3" -2.8526219121160175 2.0169381646241806 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform64" -p "pCube150";
	rename -uid "16BC0BDD-9F4D-6E8B-349E-37AE18EF5254";
	setAttr ".v" no;
createNode mesh -n "pCubeShape150" -p "transform64";
	rename -uid "265C51B9-AB4C-BD0F-E70C-AD8378022A5D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube151";
	rename -uid "4064B87B-6542-2226-5FAE-E4B140423D0F";
	setAttr ".t" -type "double3" -2.8526219121160175 0.1418012398697519 -0.016987711171077446 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.44742862852437754 0.073663989170420135 ;
createNode transform -n "transform63" -p "pCube151";
	rename -uid "F53BD937-FE47-BF1A-0F14-AEA0B05C3044";
	setAttr ".v" no;
createNode mesh -n "pCubeShape151" -p "transform63";
	rename -uid "6D0C24C9-9345-D3A2-43E2-E0B38A00E4E6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube152";
	rename -uid "21267B71-1141-FF08-90DF-2AB991902EED";
	setAttr ".t" -type "double3" 6.3803131279043113 1.5797145381126754 2.7751002287685202 ;
	setAttr ".s" -type "double3" 0.27133653896950105 3.3731634215999273 0.27133653896950105 ;
createNode transform -n "transform62" -p "pCube152";
	rename -uid "2CE35C80-E147-AF51-EB08-0B97230490C1";
	setAttr ".v" no;
createNode mesh -n "pCubeShape152" -p "transform62";
	rename -uid "8D97424D-374D-0033-64FD-0B9C8A092A3F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube153";
	rename -uid "AC137FFB-BB42-3A2C-7E14-1F93E402AE47";
	setAttr ".t" -type "double3" 6.3803131279043113 1.5797145381126754 -2.7818356687800656 ;
	setAttr ".s" -type "double3" 0.27133653896950105 3.3731634215999273 0.27133653896950105 ;
createNode transform -n "transform61" -p "pCube153";
	rename -uid "AAB289FF-3E40-613C-00AF-4883A6490704";
	setAttr ".v" no;
createNode mesh -n "pCubeShape153" -p "transform61";
	rename -uid "4D57687F-1D41-8547-D029-1DAA1C93833F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube154";
	rename -uid "FFF51A30-DA4D-F96C-B015-94B35E869BEC";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 2.7751002287685202 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.27133653896950105 3.8406589556613118 0.27133653896950105 ;
createNode transform -n "transform60" -p "pCube154";
	rename -uid "B34C9E26-8A45-AE25-0EF5-3DACB06439C1";
	setAttr ".v" no;
createNode mesh -n "pCubeShape154" -p "transform60";
	rename -uid "F08C6B56-EB45-7048-5A5B-1FAB0ED49607";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube155";
	rename -uid "2ABDA4B2-A94C-7F8A-49CE-78B903F1BC29";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -2.7775688179398861 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.27133653896950105 3.8406589556613118 0.27133653896950105 ;
createNode transform -n "transform59" -p "pCube155";
	rename -uid "036D5E02-E74E-AECA-1966-4683E2BC50CD";
	setAttr ".v" no;
createNode mesh -n "pCubeShape155" -p "transform59";
	rename -uid "B78DA539-0B41-260D-D022-4A9588269E11";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube156";
	rename -uid "EB53D4AD-864C-6078-B183-AD8211DDB46D";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 2.4028737936738267 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform58" -p "pCube156";
	rename -uid "E2107DD3-C74A-3D7B-D1C4-1EBFABDDD6AA";
	setAttr ".v" no;
createNode mesh -n "pCubeShape156" -p "transform58";
	rename -uid "FE72DB74-9149-1198-BD31-B6B4158A0BF7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube157";
	rename -uid "B34F03BC-BF48-E00D-6696-CD8412FFEE8B";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 1.8842458048673696 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform57" -p "pCube157";
	rename -uid "379A6659-2646-31D5-3B90-618DA50A1CDE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape157" -p "transform57";
	rename -uid "C0EEBBF0-8F40-0FFC-8CC6-8DBF09151F98";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube158";
	rename -uid "32C197A9-A348-89AC-AA47-31A5DCE33552";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 1.3718663460465323 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform56" -p "pCube158";
	rename -uid "441DEEA5-454C-8EB3-2E56-068F890D56C4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape158" -p "transform56";
	rename -uid "35BAFBB4-084A-290B-A6CC-469112B17DC9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube159";
	rename -uid "4382C477-1E42-C173-7D7D-B48E70E6F40B";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 0.85695298987832325 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform55" -p "pCube159";
	rename -uid "5E56CB72-8E4D-89C2-9599-479871CA057A";
	setAttr ".v" no;
createNode mesh -n "pCubeShape159" -p "transform55";
	rename -uid "EB3AEFE3-AD44-2F4F-B26C-DAA88AC05191";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube160";
	rename -uid "F92EABF3-2C46-BF65-09BE-CB98DA92C4C0";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 0.31577916564318786 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform54" -p "pCube160";
	rename -uid "F48C6C38-7B4A-5DA4-5DF5-0BB07874BE71";
	setAttr ".v" no;
createNode mesh -n "pCubeShape160" -p "transform54";
	rename -uid "954F3D06-3F4E-0EFF-C231-48919C7074AF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube161";
	rename -uid "B2C92941-BA46-B218-3D48-C7938330A5F8";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -0.22539465859194929 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform53" -p "pCube161";
	rename -uid "3488C10D-2C4C-591C-E724-D597E28EFDD3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape161" -p "transform53";
	rename -uid "00B304D6-C24B-70DE-DA2E-5087D0B8583D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube162";
	rename -uid "E080FAA2-174E-06A0-C9CD-578BE85153E1";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -0.76394279414124444 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform52" -p "pCube162";
	rename -uid "9047019E-3844-5CAE-0816-0E9D23F8E0B7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape162" -p "transform52";
	rename -uid "50D43E84-B348-F1C3-F70D-7BB346B76B09";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube163";
	rename -uid "B5BC3046-224E-A674-87CB-018EA32925C5";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -1.3051166183763798 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform51" -p "pCube163";
	rename -uid "E650FAC9-F643-6C92-44B7-72B8F9A74039";
	setAttr ".v" no;
createNode mesh -n "pCubeShape163" -p "transform51";
	rename -uid "4AADC66B-6440-0B1E-0845-3F85CBF78934";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube164";
	rename -uid "122EA98B-D845-8978-AF71-3E8AECC85B85";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -1.846290442611517 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform50" -p "pCube164";
	rename -uid "D6A8E5D9-2C4B-590C-320F-73977CC04A73";
	setAttr ".v" no;
createNode mesh -n "pCubeShape164" -p "transform50";
	rename -uid "5E962C71-F546-C3E0-E785-CE95DC983C95";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube165";
	rename -uid "6D86750C-EB4B-5B1D-B768-E8B354AF04E0";
	setAttr ".t" -type "double3" 4.6492430113553143 3.232979115884147 -2.4076920475365258 ;
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr ".s" -type "double3" 0.066311097516831438 3.8406589556613118 0.49154319992362916 ;
createNode transform -n "transform49" -p "pCube165";
	rename -uid "1C00A625-D845-8FCE-33C2-DCA25DFA3224";
	setAttr ".v" no;
createNode mesh -n "pCubeShape165" -p "transform49";
	rename -uid "1BA9CBC3-2148-821B-EBDA-B6886B46F860";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube166";
	rename -uid "B0C81919-DC44-EB8E-90DF-6E8FC189A13D";
	setAttr ".t" -type "double3" 6.5076271203952354 3.232979115884147 0.015744838423153773 ;
	setAttr ".r" -type "double3" 90 0 90 ;
	setAttr ".s" -type "double3" 0.27133653896950105 5.8955306309502591 0.27133653896950105 ;
createNode transform -n "transform48" -p "pCube166";
	rename -uid "797B40CC-4B47-3F0B-C358-14A03340EB27";
	setAttr ".v" no;
createNode mesh -n "pCubeShape166" -p "transform48";
	rename -uid "1681EC30-EC49-58B3-3143-BBAC334709A5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube167";
	rename -uid "C3E10763-9B47-8BA5-CD4C-C188A8700225";
	setAttr ".t" -type "double3" 0.0038442673928154036 3.232979115884147 -3.1300801784850645 ;
	setAttr ".r" -type "double3" 0 18.178228127336403 89.999999999999858 ;
	setAttr ".s" -type "double3" 0.060324599883780888 5.6612979240269183 0.50090324951939524 ;
createNode transform -n "transform47" -p "pCube167";
	rename -uid "4D3F2681-AD4F-306F-FB45-D3BCEB3D8F03";
	setAttr ".v" no;
createNode mesh -n "pCubeShape167" -p "transform47";
	rename -uid "9C1D0C4D-F542-2371-3CDA-07BF52FE62BB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube168";
	rename -uid "1810A8D2-8A47-F701-EE04-D6837E0C8891";
	setAttr ".t" -type "double3" 0.0038442673928154036 3.0668304894817489 -3.6185347449954919 ;
	setAttr ".r" -type "double3" 0 18.178228127336403 89.999999999999858 ;
	setAttr ".s" -type "double3" 0.060324599883780888 5.6612979240269183 0.50090324951939524 ;
createNode transform -n "transform46" -p "pCube168";
	rename -uid "62944877-3147-A49E-CE5B-6189F9396877";
	setAttr ".v" no;
createNode mesh -n "pCubeShape168" -p "transform46";
	rename -uid "D19F2EA9-6648-5A2D-A276-3ABB5E24779D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube169";
	rename -uid "A35D137A-EC47-5A93-0223-F9987CA8685C";
	setAttr ".t" -type "double3" 0.0038442673928154036 2.9184232326917603 -4.1132256009621209 ;
	setAttr ".r" -type "double3" 0 13.685007388860038 89.999999999999872 ;
	setAttr ".s" -type "double3" 0.060324599883780888 5.6612979240269183 0.50090324951939524 ;
createNode transform -n "transform45" -p "pCube169";
	rename -uid "5604A9E4-2A48-7C43-7036-C591A00D8321";
	setAttr ".v" no;
createNode mesh -n "pCubeShape169" -p "transform45";
	rename -uid "297770F2-8640-D978-CF45-EABB68ADC3CD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube170";
	rename -uid "35E80A42-5644-3547-AFAA-D1AF1F5B5FCC";
	setAttr ".t" -type "double3" 0.0038442673928154036 2.8029953662995464 -4.6285285759273602 ;
	setAttr ".r" -type "double3" 0 9.5522085747681587 89.999999999999858 ;
	setAttr ".s" -type "double3" 0.060324599883780888 5.6612979240269183 0.50090324951939524 ;
createNode transform -n "transform44" -p "pCube170";
	rename -uid "3C5ADE28-0245-003C-04AE-DB94800F3194";
	setAttr ".v" no;
createNode mesh -n "pCubeShape170" -p "transform44";
	rename -uid "7AEB3C0E-404B-05FF-C2BA-95B203409374";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube171";
	rename -uid "F8FE9322-3B45-A7A7-C152-6B90185F1040";
	setAttr ".t" -type "double3" 0.0038442673928154036 2.732928558412957 -5.1422996346008905 ;
	setAttr ".r" -type "double3" -3.9941996776008093e-16 5.5176058430170905 89.999999999999815 ;
	setAttr ".s" -type "double3" 0.060324599883780888 5.6612979240269183 0.50090324951939524 ;
createNode transform -n "transform43" -p "pCube171";
	rename -uid "DB75237C-A846-0038-729C-D2AE1151A8A7";
	setAttr ".v" no;
createNode mesh -n "pCubeShape171" -p "transform43";
	rename -uid "9C6187EB-9D43-CDDB-9B03-44BEEE031110";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 2.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[4:7]" -type "float3"  -0.85247672 -2.3283064e-10 
		0 0.85247672 -2.3283064e-10 0 -0.85247672 2.3283064e-10 0 0.85247672 2.3283064e-10 
		0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube172";
	rename -uid "70E22950-E547-3CDC-EA82-C483E3ECD4E1";
	setAttr ".t" -type "double3" 2.6915316909470683 1.3358950243715144 -5.2580389351252617 ;
	setAttr ".s" -type "double3" 0.27133653896950105 2.8213249759789187 0.27133653896950105 ;
createNode transform -n "transform42" -p "pCube172";
	rename -uid "3852143D-1642-32E2-9652-2199B32F9222";
	setAttr ".v" no;
createNode mesh -n "pCubeShape172" -p "transform42";
	rename -uid "8EE818C3-3242-78C9-AA47-A4BD8842E2D9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube173";
	rename -uid "52C284B7-1044-3074-06EE-73891BF749AD";
	setAttr ".t" -type "double3" -2.6813677863708199 1.3358950243715144 -5.2580389351252617 ;
	setAttr ".s" -type "double3" 0.27133653896950105 2.8213249759789187 0.27133653896950105 ;
createNode transform -n "transform41" -p "pCube173";
	rename -uid "DE92A6C8-A347-3A01-8EF5-9A81ECA73E5C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape173" -p "transform41";
	rename -uid "FEE07700-2043-096A-C949-478FF80BEA8E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube174";
	rename -uid "FF45F531-6E44-E430-64A8-5FA97E8BEB47";
	setAttr ".t" -type "double3" 0 5.907816038526053 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform40" -p "pCube174";
	rename -uid "15A7D799-6C44-9E36-9BFB-52A6ABEC3F03";
	setAttr ".v" no;
createNode mesh -n "pCubeShape174" -p "transform40";
	rename -uid "7FDC5ED9-584A-DF3C-EF5A-DB9DA9002D02";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube175";
	rename -uid "C51F0C23-3B4B-0BBB-96D3-ABA023A64564";
	setAttr ".t" -type "double3" 0 6.5451262430242299 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform39" -p "pCube175";
	rename -uid "CB0173AC-0442-1C19-5DD2-C1AB19E6BCF9";
	setAttr ".v" no;
createNode mesh -n "pCubeShape175" -p "transform39";
	rename -uid "507FC245-9346-DD9B-C2C1-E39F4993B18A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube176";
	rename -uid "6B1A0343-5B42-0B0D-E289-F197FD40602B";
	setAttr ".t" -type "double3" 0 7.1824364475224067 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform38" -p "pCube176";
	rename -uid "8D2733E5-0C42-FF96-5C60-7DAB06A213F3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape176" -p "transform38";
	rename -uid "A3FD26D7-AF4A-83F0-42F2-4C97CFA0BE8D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube177";
	rename -uid "52EE6A61-9144-5FC9-D510-5C9C1C7A66A8";
	setAttr ".t" -type "double3" 0 7.8261323119596993 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform37" -p "pCube177";
	rename -uid "13DE630A-3F4B-7170-4160-B9A23761B93B";
	setAttr ".v" no;
createNode mesh -n "pCubeShape177" -p "transform37";
	rename -uid "7601206E-CF4C-6D63-1A16-42827F62123A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube178";
	rename -uid "B558A52A-A549-54B3-84B6-0198854DB301";
	setAttr ".t" -type "double3" -2.7751002287685198 2.782071097372488 -2.7751002287685211 ;
	setAttr ".s" -type "double3" 0.38847142591309242 5.8234604877116869 0.38847142591309242 ;
createNode transform -n "transform36" -p "pCube178";
	rename -uid "B34FF291-D147-AB92-CE67-F59E35D5F5CE";
	setAttr ".v" no;
createNode mesh -n "pCubeShape178" -p "transform36";
	rename -uid "3CD7C013-124D-BB6E-8738-2FBDBD5D1F8C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".pt[2:5]" -type "float3"  0 0.39688462 0 0 0.39688462 
		0 0 0.39688462 0 0 0.39688462 0;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube179";
	rename -uid "8D5FF5C7-5A4D-E281-6B34-66B21593457D";
	setAttr ".t" -type "double3" 0 7.1824364475224067 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform35" -p "pCube179";
	rename -uid "97346377-7749-A6BF-B733-1B8752F3F01D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape179" -p "transform35";
	rename -uid "C3FC4C13-FE48-5A88-06B3-EBAA919FD58E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube180";
	rename -uid "C0EFFE89-2A47-58BB-3B5E-5692964B7D8E";
	setAttr ".t" -type "double3" 0 7.8261323119596993 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform34" -p "pCube180";
	rename -uid "8A3A307D-BA45-7E59-87A2-A589114A8155";
	setAttr ".v" no;
createNode mesh -n "pCubeShape180" -p "transform34";
	rename -uid "EF3876CE-6243-813A-EE25-A585F3A73661";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube181";
	rename -uid "3DCC8EA7-BD49-05EF-97AE-7083F383CC41";
	setAttr ".t" -type "double3" 0 5.907816038526053 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform33" -p "pCube181";
	rename -uid "318A2EEB-9849-6343-DCA9-D1B516645750";
	setAttr ".v" no;
createNode mesh -n "pCubeShape181" -p "transform33";
	rename -uid "3260BA55-C849-75BB-D9B2-3BB18D32C543";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube182";
	rename -uid "38AC9CD3-DB4A-929E-F56A-E3A2FD3561E7";
	setAttr ".t" -type "double3" 0 6.5451262430242299 -2.8214782370696163 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform32" -p "pCube182";
	rename -uid "9F53B251-8548-0E09-A937-1DBBB93EE999";
	setAttr ".v" no;
createNode mesh -n "pCubeShape182" -p "transform32";
	rename -uid "B2E66537-7D4E-E261-1ABA-45B5E8289C17";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube183";
	rename -uid "B803B617-8F4B-B202-CA4C-BF9FD8AC5ED4";
	setAttr ".t" -type "double3" 0 7.8261323119596993 2.8425402261509336 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform31" -p "pCube183";
	rename -uid "3A013AFA-D044-8850-48F7-238B2D3D8FA8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape183" -p "transform31";
	rename -uid "D2694114-FD4A-E3E2-C3DC-BEA1865E669E";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube184";
	rename -uid "1E1A47AF-EA42-F020-6E13-E4A48AAF5C44";
	setAttr ".t" -type "double3" 0 6.5451262430242299 2.8425402261509336 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform30" -p "pCube184";
	rename -uid "6A97A4A2-9C42-7D1B-6EA1-8EAE53478EF6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape184" -p "transform30";
	rename -uid "AD10C0C8-7342-8027-05B1-F4A42C28DC89";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube185";
	rename -uid "7C122C6F-9C4F-E779-2AA2-0B9DDCBE88AE";
	setAttr ".t" -type "double3" 0 7.1824364475224067 2.8425402261509336 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform29" -p "pCube185";
	rename -uid "12BE65EE-5842-94FB-315C-84ACDB4A8F14";
	setAttr ".v" no;
createNode mesh -n "pCubeShape185" -p "transform29";
	rename -uid "BDAAE31E-FB41-ED54-C202-69B0706B509D";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube186";
	rename -uid "5E2F75DB-7E40-876E-FB20-199414F4B80B";
	setAttr ".t" -type "double3" 0 5.907816038526053 2.8425402261509336 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform28" -p "pCube186";
	rename -uid "628B9DAA-6C4D-0757-06B3-2EB7A09DBFB2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape186" -p "transform28";
	rename -uid "01309113-3D47-4106-3F34-7ABEC0492AB4";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube187";
	rename -uid "01056057-3841-C791-AC9A-78846817FA00";
	setAttr ".t" -type "double3" 2.781201959715919 7.8261323119596993 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform27" -p "pCube187";
	rename -uid "2EFC4E9E-A44B-BCF6-F172-CBBC56591DB8";
	setAttr ".v" no;
createNode mesh -n "pCubeShape187" -p "transform27";
	rename -uid "9AE14CA3-DC4D-1964-87EF-3DBC93B466CE";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube188";
	rename -uid "43CF627D-014C-2B17-EA9A-2FAAE5986319";
	setAttr ".t" -type "double3" 2.781201959715919 6.5451262430242299 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform26" -p "pCube188";
	rename -uid "867D3290-7A49-83BE-1FCB-2B8346981F55";
	setAttr ".v" no;
createNode mesh -n "pCubeShape188" -p "transform26";
	rename -uid "71F3997E-5A45-17FD-70CD-2EA0FA86BDF0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube189";
	rename -uid "DC763AD6-384D-6281-1C42-1CA7ABC6DF61";
	setAttr ".t" -type "double3" 2.781201959715919 7.1824364475224067 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform25" -p "pCube189";
	rename -uid "E7B6DF5B-F147-99EA-FE81-FC9FD7BC5313";
	setAttr ".v" no;
createNode mesh -n "pCubeShape189" -p "transform25";
	rename -uid "61FF2783-7247-2657-E458-74988AF338D9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube190";
	rename -uid "0ABC4A09-B241-55A4-C716-65B80DA113A3";
	setAttr ".t" -type "double3" 2.781201959715919 5.907816038526053 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform24" -p "pCube190";
	rename -uid "B42B6D71-994E-8306-5E02-8E93673BDA95";
	setAttr ".v" no;
createNode mesh -n "pCubeShape190" -p "transform24";
	rename -uid "57384E87-5048-AD97-D391-EFA4BB02C47F";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube191";
	rename -uid "43FFD3AB-264F-9B9E-37B6-85B99FFAA4ED";
	setAttr ".t" -type "double3" -2.8792262340797463 7.8261323119596993 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform23" -p "pCube191";
	rename -uid "64CAEB26-7E45-F0CE-A226-7192ED3BC561";
	setAttr ".v" no;
createNode mesh -n "pCubeShape191" -p "transform23";
	rename -uid "14175D46-0543-E854-743E-C8BE85FB1E54";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube192";
	rename -uid "B76D14C3-F249-E374-63C3-B285113629DE";
	setAttr ".t" -type "double3" -2.8792262340797463 6.5451262430242299 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform22" -p "pCube192";
	rename -uid "A227DE67-6541-C6D6-81A0-EC9B0B21BC65";
	setAttr ".v" no;
createNode mesh -n "pCubeShape192" -p "transform22";
	rename -uid "17BFD6D2-6C41-B3CA-63F3-359AB528CDA1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube193";
	rename -uid "CFE72DD4-EE4D-353A-C59D-1C8C02452C8A";
	setAttr ".t" -type "double3" -2.8792262340797463 7.1824364475224067 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform21" -p "pCube193";
	rename -uid "22638C4E-7445-72A2-F2D9-6B9FCD9CBF3C";
	setAttr ".v" no;
createNode mesh -n "pCubeShape193" -p "transform21";
	rename -uid "74FD3534-964B-539C-B847-118D7C1B7EA9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube194";
	rename -uid "F586D990-0047-D37E-0592-1081EF66E843";
	setAttr ".t" -type "double3" -2.8792262340797463 5.907816038526053 0.11823316216747926 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 5.4319474683390334 0.61012993908686097 0.073663989170420135 ;
createNode transform -n "transform20" -p "pCube194";
	rename -uid "FA57DC4F-284A-190A-82DB-E2B4A0B70C55";
	setAttr ".v" no;
createNode mesh -n "pCubeShape194" -p "transform20";
	rename -uid "C087EDAE-9145-A356-9124-6EADA1E224A6";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube195";
	rename -uid "A25107BB-4946-5A90-F419-D58619891C7E";
	setAttr ".t" -type "double3" 0 8.3007238649158648 0 ;
	setAttr ".s" -type "double3" 6.5864437692341768 0.31622329367836749 6.5864437692341768 ;
createNode transform -n "transform19" -p "pCube195";
	rename -uid "9D059FCF-4C48-1E0D-DE32-968DFE8E9093";
	setAttr ".v" no;
createNode mesh -n "pCubeShape195" -p "transform19";
	rename -uid "E6D21425-F542-720A-A95B-01B876DD4A7B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.5 1.5 ;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 7 ".pt";
	setAttr ".pt[2]" -type "float3" 0 1.2837069 0 ;
	setAttr ".pt[3]" -type "float3" 0 1.2837071 0 ;
	setAttr ".pt[4]" -type "float3" 0 1.2837069 0 ;
	setAttr ".pt[5]" -type "float3" 0 1.2837069 0 ;
	setAttr ".pt[13]" -type "float3" 0 1.0750067 0 ;
	setAttr ".pt[15]" -type "float3" 0 0.057256661 0 ;
createNode transform -n "pCube196";
	rename -uid "31E09ECA-2A4F-04D0-AA25-7DB11D204519";
	setAttr ".t" -type "double3" 2.6915316909470683 2.2254635740307607 -4.6421837853611692 ;
	setAttr ".r" -type "double3" 42.886156459128479 0 0 ;
	setAttr ".s" -type "double3" 0.17237169216777451 1.7828346622087643 0.17237169216777451 ;
createNode transform -n "transform18" -p "pCube196";
	rename -uid "624CBDB8-0E47-C4C3-AAC8-339AFAD4CE02";
	setAttr ".v" no;
createNode mesh -n "pCubeShape196" -p "transform18";
	rename -uid "6B5F6D15-A04F-89F6-E561-6BAE85B9E5CA";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube197";
	rename -uid "EB11C8FB-9147-4CA9-701A-CAAFACC347F3";
	setAttr ".t" -type "double3" -2.70100412185475 2.2254635740307607 -4.6421837853611692 ;
	setAttr ".r" -type "double3" 42.886156459128479 0 0 ;
	setAttr ".s" -type "double3" 0.17237169216777451 1.7828346622087643 0.17237169216777451 ;
createNode transform -n "transform17" -p "pCube197";
	rename -uid "CB360E25-134D-5077-2716-35ACFA36CC72";
	setAttr ".v" no;
createNode mesh -n "pCubeShape197" -p "transform17";
	rename -uid "A7D51BD2-394F-26A9-CFD7-15AB29B55ACD";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube198";
	rename -uid "C5F16824-A542-08E7-E0F1-F8BC1149117B";
	setAttr ".t" -type "double3" 5.7384254313312066 2.636984437573251 -2.7693642473911066 ;
	setAttr ".r" -type "double3" 42.886156459128479 -90 0 ;
	setAttr ".s" -type "double3" 0.17237169216777451 1.7828346622087643 0.17237169216777451 ;
createNode transform -n "transform16" -p "pCube198";
	rename -uid "17841BF9-E145-9E34-BB3A-2A919B4D78E3";
	setAttr ".v" no;
createNode mesh -n "pCubeShape198" -p "transform16";
	rename -uid "BC252D78-3745-2A97-E2E5-C9836E3607E5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube199";
	rename -uid "ABD54C92-2A4D-87DE-6E70-BDBB88210241";
	setAttr ".t" -type "double3" 5.7384254313312066 2.636984437573251 2.7697925566000094 ;
	setAttr ".r" -type "double3" 42.886156459128479 -90 0 ;
	setAttr ".s" -type "double3" 0.17237169216777451 1.7828346622087643 0.17237169216777451 ;
createNode transform -n "transform15" -p "pCube199";
	rename -uid "86936202-4148-BBC1-6096-A58437EAEDA6";
	setAttr ".v" no;
createNode mesh -n "pCubeShape199" -p "transform15";
	rename -uid "ABF09EBD-1D48-3A0C-B134-CCAE9BD3AE36";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube200";
	rename -uid "A2CE7BDC-4C4A-4445-28DF-D9B66ABA21CA";
	setAttr ".t" -type "double3" 2.8432610259407105 6.3712828957677328 0.99302279895568946 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.2940102663078541 0.21106249847431649 ;
createNode transform -n "transform14" -p "pCube200";
	rename -uid "C5C7F565-744D-EA5C-A445-CEB83770AD95";
	setAttr ".v" no;
createNode mesh -n "pCubeShape200" -p "transform14";
	rename -uid "6DE35256-9945-1AB2-BD60-D28DD250607A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube201";
	rename -uid "1F2474D0-E441-AD60-4F92-EBABB9891889";
	setAttr ".t" -type "double3" 2.8432610259407105 6.3712828957677328 -1.0551141472371313 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.2940102663078541 0.21106249847431649 ;
createNode transform -n "transform13" -p "pCube201";
	rename -uid "7D497937-2145-CA78-3F97-AFA82395036B";
	setAttr ".v" no;
createNode mesh -n "pCubeShape201" -p "transform13";
	rename -uid "817EFC6F-094A-0125-0DD5-22AAF3A423C9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube202";
	rename -uid "F6707831-CB4E-5F4F-0169-148A254DC780";
	setAttr ".t" -type "double3" 2.8432610259407105 7.613025399280211 -0.044908341976406585 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.5168550422824274 0.21106249847431649 ;
createNode transform -n "transform12" -p "pCube202";
	rename -uid "39BC98BA-3743-CEC2-4FC7-949855801FD2";
	setAttr ".v" no;
createNode mesh -n "pCubeShape202" -p "transform12";
	rename -uid "48A6AD1E-834C-9E35-3CDB-1FBB9F8309C2";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube203";
	rename -uid "7019BC0A-8F4B-FE5F-DFD8-4CA85A970B0D";
	setAttr ".t" -type "double3" 2.8432610259407105 5.3489298980794366 -0.044908341976406585 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.5168550422824274 0.21106249847431649 ;
createNode transform -n "transform11" -p "pCube203";
	rename -uid "0BB553F1-AA46-8BB9-9231-75A177940EC4";
	setAttr ".v" no;
createNode mesh -n "pCubeShape203" -p "transform11";
	rename -uid "9461D9F6-F44F-ABB7-7EA6-D0B4C714707C";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube204";
	rename -uid "B9D470F1-0C4D-E521-3F60-A1A41BE79689";
	setAttr ".t" -type "double3" 0.010217796771313181 5.3489298980794366 -2.8821095089723756 ;
	setAttr ".r" -type "double3" 90 90 0 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.5168550422824274 0.21106249847431649 ;
createNode transform -n "transform10" -p "pCube204";
	rename -uid "1C34828A-4E4C-B26B-F48F-C3A6E046DAC1";
	setAttr ".v" no;
createNode mesh -n "pCubeShape204" -p "transform10";
	rename -uid "59F2C726-2C4B-B08F-BB8A-4CA9DC2304F0";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube205";
	rename -uid "D6B11AF2-0149-4D35-43C4-40B3147D0BE0";
	setAttr ".t" -type "double3" 0.010217796771313181 7.6502574689362142 -2.8821095089723756 ;
	setAttr ".r" -type "double3" 90 90 0 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.5168550422824274 0.21106249847431649 ;
createNode transform -n "transform9" -p "pCube205";
	rename -uid "62D7B138-F04D-5D30-8B47-DFB5F24BDE69";
	setAttr ".v" no;
createNode mesh -n "pCubeShape205" -p "transform9";
	rename -uid "6AA0ECCC-1D4B-DDA0-C5B9-13A778BB00F9";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube206";
	rename -uid "A7FF9339-F941-1546-65EA-A3B46F7A5D2C";
	setAttr ".t" -type "double3" 1.0210635637901344 6.496156789819274 -2.8543822033722277 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.2940102663078541 0.21106249847431649 ;
createNode transform -n "transform8" -p "pCube206";
	rename -uid "33A14361-974D-F529-71FD-1EB3DAE3A47D";
	setAttr ".v" no;
createNode mesh -n "pCubeShape206" -p "transform8";
	rename -uid "9B63FD40-D445-1B5D-274E-2F80C5512B82";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube207";
	rename -uid "7FA8A64A-9844-56CF-9366-CB8AD91733FE";
	setAttr ".t" -type "double3" -0.99827966368367071 6.496156789819274 -2.8543822033722277 ;
	setAttr ".s" -type "double3" 0.21106249847431649 2.2940102663078541 0.21106249847431649 ;
createNode transform -n "transform7" -p "pCube207";
	rename -uid "2E05DCCE-3D40-0629-63B8-4BB18FDF6A79";
	setAttr ".v" no;
createNode mesh -n "pCubeShape207" -p "transform7";
	rename -uid "3E65338C-7C40-F577-4D26-F0AC8640435A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube208";
	rename -uid "0862AD66-2141-C2C3-0C11-4BB0BB6F256B";
	setAttr ".t" -type "double3" 2.852158508274897 7.0620517670812069 -0.0015305105298577537 ;
	setAttr ".s" -type "double3" 0.047450972714668742 0.96149715962352278 0.12319609248560881 ;
createNode transform -n "transform6" -p "pCube208";
	rename -uid "7D85A25F-1D45-8BB7-9114-0793834E6ECB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape208" -p "transform6";
	rename -uid "D9C62443-DB47-52F6-CCDA-8E986D0910C1";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr -s 2 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode transform -n "pCube209";
	rename -uid "904B953A-4E4E-4363-B967-2B9F51ADFCF1";
	setAttr ".t" -type "double3" 0.042229245396421256 6.4617560568533223 -2.8536605698592021 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 0.044276741793109746 2.1672420373818628 2.130331295115222 ;
createNode transform -n "transform5" -p "pCube209";
	rename -uid "31E771CE-9446-1F27-8696-1084750955FB";
	setAttr ".v" no;
createNode mesh -n "pCubeShape209" -p "transform5";
	rename -uid "68D1ABD4-4143-06D7-ED5D-E287525C2B56";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube210";
	rename -uid "D735349F-6741-9ED1-560F-2696E7A50AC3";
	setAttr ".t" -type "double3" 2.852158508274897 6.5380397304656324 -0.0015305105298577537 ;
	setAttr ".r" -type "double3" 90 0 0 ;
	setAttr ".s" -type "double3" 0.047450972714668742 1.9796894914148504 0.12319609248560881 ;
createNode transform -n "transform4" -p "pCube210";
	rename -uid "4AFED390-0044-6101-C5C0-6684B3167D8E";
	setAttr ".v" no;
createNode mesh -n "pCubeShape210" -p "transform4";
	rename -uid "C923A00A-664C-08AB-75DE-76AB4CB86C8A";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube211";
	rename -uid "5935E33D-1545-CF8E-9B52-939FE8234977";
	setAttr ".t" -type "double3" 2.8239787312311933 6.4617560568533223 -0.0067126486515838302 ;
	setAttr ".s" -type "double3" 0.025768188964000262 2.1672420373818628 2.130331295115222 ;
createNode transform -n "transform3" -p "pCube211";
	rename -uid "502869CA-0A42-2716-D2D6-16A31423E883";
	setAttr ".v" no;
createNode mesh -n "pCubeShape211" -p "transform3";
	rename -uid "9126242F-4A44-CD36-3115-7D836A999476";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube212";
	rename -uid "87DDE8ED-F34F-6FD0-5280-838E8F0192F7";
	setAttr ".t" -type "double3" 0.042229245396421256 7.0700841467356321 -2.8719463175742761 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 0.068918439735686191 1.1126127387792943 0.11845663007670557 ;
createNode transform -n "transform2" -p "pCube212";
	rename -uid "121E735F-C242-647E-178B-4C8ED30C1250";
	setAttr ".v" no;
createNode mesh -n "pCubeShape212" -p "transform2";
	rename -uid "C763F5EE-074B-2648-49A5-48B734A062BC";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube213";
	rename -uid "6EEF6295-9B45-F1DA-9F60-479B4C252FCE";
	setAttr ".t" -type "double3" 0.042229245396421256 6.5102627548913929 -2.8719463175742761 ;
	setAttr ".r" -type "double3" 90 90 0 ;
	setAttr ".s" -type "double3" 0.068918439735686191 1.9224600233165503 0.11845663007670557 ;
createNode transform -n "transform1" -p "pCube213";
	rename -uid "C495FA0C-F449-7AFC-9B60-BF93EB1BB58F";
	setAttr ".v" no;
createNode mesh -n "pCubeShape213" -p "transform1";
	rename -uid "2BD1C5C4-D946-5054-83DE-589BCC1E90EB";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".iog[0].og[0].gcl" -type "componentList" 1 "f[0:5]";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0 0 1 0 0 1 1 1 0
		 2 1 2 0 3 1 3 0 4 1 4 2 0 2 1 -1 0 -1 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.5 -0.5 0.5 0.5 -0.5 0.5 -0.5 0.5 0.5 0.5 0.5 0.5
		 -0.5 0.5 -0.5 0.5 0.5 -0.5 -0.5 -0.5 -0.5 0.5 -0.5 -0.5;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "pCube214";
	rename -uid "3BF80FDC-8B46-0B20-70E6-8F84D4EA1E32";
createNode mesh -n "pCube214Shape" -p "pCube214";
	rename -uid "DAC693D5-EC42-026C-48A2-69A4193E4E01";
	setAttr -k off ".v";
	setAttr -s 12 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C82DDD23-704B-0CF1-1FD8-51B02CD60395";
	setAttr -s 8 ".lnk";
	setAttr -s 8 ".slnk";
createNode displayLayerManager -n "layerManager";
	rename -uid "2E5ADDB7-BC4E-C30C-5733-ADBC5FE5D457";
createNode displayLayer -n "defaultLayer";
	rename -uid "11A63094-FB49-753E-3E25-4AA076D66BCD";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "AF74B9AA-584C-2FE0-64D3-35A53BFE0EB6";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "57EA1844-7D46-AAD7-7EB2-D78A30AD6A07";
	setAttr ".g" yes;
createNode polyCube -n "polyCube1";
	rename -uid "0A52A201-C646-5DE8-609D-E59D59D7CF65";
	setAttr ".cuv" 1;
createNode polyCube -n "polyCube2";
	rename -uid "EB4701F8-5840-A859-BBF0-DEAE27FCE91D";
	setAttr ".cuv" 1;
createNode script -n "uiConfigurationScriptNode";
	rename -uid "779BCA69-3643-D6DE-303A-40ACDAE233E0";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n"
		+ "                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n"
		+ "                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 637\n                -height 333\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n"
		+ "            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 637\n            -height 333\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n"
		+ "                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n"
		+ "                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 637\n                -height 332\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n"
		+ "            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 637\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n"
		+ "                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n"
		+ "                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n"
		+ "                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 637\n                -height 332\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n"
		+ "            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 637\n            -height 332\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n"
		+ "                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n"
		+ "                -width 637\n                -height 333\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n"
		+ "            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 637\n            -height 333\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n"
		+ "            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n"
		+ "            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n"
		+ "                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n"
		+ "                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n"
		+ "                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n"
		+ "                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n"
		+ "                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n"
		+ "                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n"
		+ "                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n"
		+ "                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n"
		+ "                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -activeTab -1\n                -editorMode \"default\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"profilerPanel\" -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n"
		+ "                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n"
		+ "                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 4 4 \n            -bumpResolution 4 4 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 0\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 0\n            -height 0\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"quad\\\" -ps 1 50 50 -ps 2 50 50 -ps 3 50 50 -ps 4 50 50 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera top` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 333\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Side View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Side View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera side` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Front View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Front View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera front` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 637\\n    -height 332\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "51C65C30-424C-E356-81F9-05B97C0EA437";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 120 -ast 1 -aet 200 ";
	setAttr ".st" 6;
createNode polyCube -n "polyCube3";
	rename -uid "5258BE1A-B142-3F05-0FBD-9D8029086E9C";
	setAttr ".cuv" 1;
createNode polySubdFace -n "polySubdFace1";
	rename -uid "7DA12D84-1F4B-E30C-FBF8-FA8C9ADB3C5A";
	setAttr ".ics" -type "componentList" 2 "f[0:1]" "f[4]";
createNode polyCube -n "polyCube4";
	rename -uid "BA0C6CFC-2040-A055-638E-6AA275D23893";
	setAttr ".cuv" 1;
createNode polyCube -n "polyCube5";
	rename -uid "49C50E89-264D-86F2-3004-DEBD2700357C";
	setAttr ".cuv" 1;
createNode lambert -n "west_house_body";
	rename -uid "639E4A2D-C543-7DD3-5262-D289A93D1EEE";
	setAttr ".c" -type "float3" 0.37396809 0.19551384 0.095277332 ;
createNode shadingEngine -n "lambert2SG";
	rename -uid "9FC09E63-B94E-004A-0ABC-7097FB15E65A";
	setAttr ".ihi" 0;
	setAttr -s 339 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 340 ".gn";
createNode materialInfo -n "materialInfo1";
	rename -uid "BBCA2DB0-2849-60BB-622E-DFA252089211";
createNode lambert -n "west_house_posts";
	rename -uid "8559C9D6-9549-A2BC-DB00-FA9AD277477E";
	setAttr ".c" -type "float3" 0.108 0.056504261 0.02754 ;
createNode shadingEngine -n "lambert3SG";
	rename -uid "E28B03D5-3042-3849-F82B-E58E5E9D3568";
	setAttr ".ihi" 0;
	setAttr -s 31 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 31 ".gn";
createNode materialInfo -n "materialInfo2";
	rename -uid "8DC2B21A-8844-3AF1-377B-94A132B25BD1";
createNode lambert -n "west_house_roof_frame";
	rename -uid "764B4867-E546-CEBD-EE4A-5AA97B079CB6";
	setAttr ".c" -type "float3" 0.214 0.13182186 0.085599996 ;
createNode shadingEngine -n "lambert4SG";
	rename -uid "1EA20758-3941-FD3A-CF00-3AAD29C99532";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 3 ".gn";
createNode materialInfo -n "materialInfo3";
	rename -uid "B31E0C7D-CF41-8F43-832F-09A253B5A170";
createNode lambert -n "west_house_window_frame";
	rename -uid "0159B02F-7440-4120-F621-6EB2DD21A4A8";
	setAttr ".c" -type "float3" 0.108 0.056504261 0.02754 ;
createNode shadingEngine -n "lambert5SG";
	rename -uid "598D80B7-D14B-134D-F992-479CC924951C";
	setAttr ".ihi" 0;
	setAttr -s 25 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 25 ".gn";
createNode materialInfo -n "materialInfo4";
	rename -uid "8D22F408-CE4D-5E33-9BEF-1EA889E32231";
createNode lambert -n "west_house_window";
	rename -uid "BAFA5493-F64A-AC15-D373-A3BA9917C974";
	setAttr ".c" -type "float3" 0.50999999 0.61930001 0.60140002 ;
createNode shadingEngine -n "lambert6SG";
	rename -uid "28C9C0EC-534E-4D69-B9C4-E2A5D0A6D122";
	setAttr ".ihi" 0;
	setAttr -s 5 ".dsm";
	setAttr ".ro" yes;
	setAttr -s 5 ".gn";
createNode materialInfo -n "materialInfo5";
	rename -uid "F900871A-D648-54D9-2F42-299525033B33";
createNode groupId -n "groupId1";
	rename -uid "E63DE550-F543-3BB2-EA4E-B09403741E97";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts1";
	rename -uid "BDD27E05-FD46-BE99-DC5F-9CAAFFD8D6A7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[0]" "f[2:8]" "f[12:14]";
	setAttr ".irc" -type "componentList" 2 "f[1]" "f[9:11]";
createNode groupId -n "groupId2";
	rename -uid "DAA3CD08-7D49-53BF-EAC8-67AA1C8804CD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId3";
	rename -uid "74BCD2A1-B943-57D7-B197-A2A861D1477B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	rename -uid "89C10D93-EA43-DDF0-F862-26B363AB2B84";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1]" "f[9:11]";
createNode lambert -n "west_house_roof";
	rename -uid "BF0D5391-4140-0960-6D59-9394DEABF5C1";
	setAttr ".c" -type "float3" 0.214 0.14976409 0.11363401 ;
createNode shadingEngine -n "lambert7SG";
	rename -uid "70BAE316-314B-750C-DDE5-B68665AD674E";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo6";
	rename -uid "EABABF11-6A41-EF82-A2BE-AE8E7F3FAD03";
createNode nodeGraphEditorInfo -n "hyperShadePrimaryNodeEditorSavedTabsInfo";
	rename -uid "BD6B4B20-7C4F-E6D0-45B6-308D851275DB";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" -741.66663719548239 -278.57141750199497 ;
	setAttr ".tgi[0].vh" -type "double2" 705.95235290035498 290.4761789337042 ;
	setAttr -s 12 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[0].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[0].nvs" 1923;
	setAttr ".tgi[0].ni[1].x" 262.85714721679688;
	setAttr ".tgi[0].ni[1].y" -62.857143402099609;
	setAttr ".tgi[0].ni[1].nvs" 1923;
	setAttr ".tgi[0].ni[2].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[2].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[2].nvs" 1923;
	setAttr ".tgi[0].ni[3].x" 262.85714721679688;
	setAttr ".tgi[0].ni[3].y" -62.857143402099609;
	setAttr ".tgi[0].ni[3].nvs" 1923;
	setAttr ".tgi[0].ni[4].x" 582.85711669921875;
	setAttr ".tgi[0].ni[4].y" -10;
	setAttr ".tgi[0].ni[4].nvs" 1923;
	setAttr ".tgi[0].ni[5].x" 844.28570556640625;
	setAttr ".tgi[0].ni[5].y" -71.428573608398438;
	setAttr ".tgi[0].ni[5].nvs" 1923;
	setAttr ".tgi[0].ni[6].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[6].y" -10;
	setAttr ".tgi[0].ni[6].nvs" 1923;
	setAttr ".tgi[0].ni[7].x" 262.85714721679688;
	setAttr ".tgi[0].ni[7].y" -67.142860412597656;
	setAttr ".tgi[0].ni[7].nvs" 1923;
	setAttr ".tgi[0].ni[8].x" 1.4285714626312256;
	setAttr ".tgi[0].ni[8].y" -1.4285714626312256;
	setAttr ".tgi[0].ni[8].nvs" 1923;
	setAttr ".tgi[0].ni[9].x" 262.85714721679688;
	setAttr ".tgi[0].ni[9].y" -67.142860412597656;
	setAttr ".tgi[0].ni[9].nvs" 1923;
	setAttr ".tgi[0].ni[10].x" 1175.7142333984375;
	setAttr ".tgi[0].ni[10].y" -322.85714721679688;
	setAttr ".tgi[0].ni[10].nvs" 1923;
	setAttr ".tgi[0].ni[11].x" 1437.142822265625;
	setAttr ".tgi[0].ni[11].y" -380;
	setAttr ".tgi[0].ni[11].nvs" 1923;
createNode polyUnite -n "polyUnite1";
	rename -uid "090EC15F-394D-D059-628D-E2AAE7D77DB2";
	setAttr -s 199 ".ip";
	setAttr -s 199 ".im";
createNode groupId -n "groupId4";
	rename -uid "8C521A84-8146-6E47-E819-8F9F81D3F85B";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts3";
	rename -uid "827A8CCD-DC48-6F7A-7320-AE9336D23C27";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId5";
	rename -uid "2739BB29-C646-E5A3-A7F7-C9BD782E069A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId6";
	rename -uid "A8FC234C-5C4D-CDAB-D7AE-BFBCEC2E07C3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	rename -uid "7A732EC5-1F49-602A-C17C-77AE23A6AAE7";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId7";
	rename -uid "7E84A694-8E4B-D1BF-38D1-1A8EF51702E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId8";
	rename -uid "F09B9EED-BD4B-B14B-ED8B-9F91B8FB22D3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId9";
	rename -uid "1B50E563-2E40-6BB4-5956-B997E87A7ADD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId10";
	rename -uid "3C87C962-3A4E-953A-8E27-D3BC36C30466";
	setAttr ".ihi" 0;
createNode groupId -n "groupId11";
	rename -uid "05468293-804D-70E8-0744-D89D61DCD922";
	setAttr ".ihi" 0;
createNode groupId -n "groupId12";
	rename -uid "8D69A813-0D41-23CB-E26A-938A97EE94EF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId13";
	rename -uid "8BD26BAD-914E-6A3C-4CCB-A0B93DD08EAB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId14";
	rename -uid "9D0031C5-EE45-F80E-4B15-9BA64C209D06";
	setAttr ".ihi" 0;
createNode groupId -n "groupId15";
	rename -uid "F3E83B9B-6947-AC6A-511C-3998623A32F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId16";
	rename -uid "230EBAB8-8843-0370-F901-1CB55FB11EA8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId17";
	rename -uid "ADFAE27A-C040-F4A5-6D87-2FA1F85E2E3D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId18";
	rename -uid "4F9D024D-3648-2537-008B-36AA670B084C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId19";
	rename -uid "4ECCACAB-224F-993A-0AB3-EBBC220D6471";
	setAttr ".ihi" 0;
createNode groupId -n "groupId20";
	rename -uid "058548D5-B24A-ED0F-6059-74A0E5C09D4D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId21";
	rename -uid "D0863425-DB45-2CE3-36AD-3A8491665792";
	setAttr ".ihi" 0;
createNode groupId -n "groupId22";
	rename -uid "098E2DAD-D14F-ABB5-C5BC-FF83D0380D84";
	setAttr ".ihi" 0;
createNode groupId -n "groupId23";
	rename -uid "A8A74BCD-0E45-8F29-A00F-89A9510F6117";
	setAttr ".ihi" 0;
createNode groupId -n "groupId24";
	rename -uid "3BD22CC0-1E48-3FC4-6B60-7882A3CFE2E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId25";
	rename -uid "36034250-6747-1FDF-491F-65A2A815350F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId26";
	rename -uid "A8FBADC9-BB4A-FC3B-5CE2-058E74CD5A1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId27";
	rename -uid "00F9C1B8-504F-FB47-9279-3DB6A212ED5D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId28";
	rename -uid "8CCCC26B-C342-2219-3A98-CEB24F644F2E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId29";
	rename -uid "BA99F655-4347-CA77-EA48-458D5324225F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId30";
	rename -uid "43E807B2-BF4E-29D4-DFC5-F1BFC9E9F8BF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId31";
	rename -uid "AF797A9F-1B46-69CF-23ED-E599C5AD3B62";
	setAttr ".ihi" 0;
createNode groupId -n "groupId32";
	rename -uid "C9C80427-1846-BBC7-BECD-26B2B4692448";
	setAttr ".ihi" 0;
createNode groupId -n "groupId33";
	rename -uid "E5EDD712-4346-1898-4252-23BB470D8F81";
	setAttr ".ihi" 0;
createNode groupId -n "groupId34";
	rename -uid "36AF24EC-5C4A-D41C-E2C3-8FBF92DE67D5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId35";
	rename -uid "BE879A20-D74B-FD7A-3474-A9B9D358C20E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId36";
	rename -uid "BDD841E8-D441-3A64-B971-3296F18A70F3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId37";
	rename -uid "936F0FF2-0547-5AFD-FC07-60B5EE14C406";
	setAttr ".ihi" 0;
createNode groupId -n "groupId38";
	rename -uid "2098A425-734A-0958-87E0-7FB5DF912F68";
	setAttr ".ihi" 0;
createNode groupId -n "groupId39";
	rename -uid "57B3BE95-544A-B200-4867-B7B278C2C46C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId40";
	rename -uid "7AA188CC-4D45-865D-C504-D0B9CDF93542";
	setAttr ".ihi" 0;
createNode groupId -n "groupId41";
	rename -uid "A8303134-AB4F-E959-A076-DFB1DB4A6C95";
	setAttr ".ihi" 0;
createNode groupId -n "groupId42";
	rename -uid "F057DFD5-1441-3A51-0399-D49BDC760C3F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId43";
	rename -uid "9103B043-B346-2000-5CCE-089E02DA5D2C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId44";
	rename -uid "CA503689-014A-35CA-19F5-39A3FB2374CB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId45";
	rename -uid "E061CA88-CA48-04F0-4676-C08FF3BCD7B7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId46";
	rename -uid "D38F3B11-B34B-6A9F-DDDA-9F937F8875AE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId47";
	rename -uid "4727B691-094E-AD7B-FF2E-F6A50726C573";
	setAttr ".ihi" 0;
createNode groupId -n "groupId48";
	rename -uid "5FA0CFD6-8E4B-BD3E-4C05-5CBDA423F8C3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId49";
	rename -uid "BBAAF60F-6B4A-8517-AAA1-A9AE13691E69";
	setAttr ".ihi" 0;
createNode groupId -n "groupId50";
	rename -uid "6B7109BA-704D-9F26-B531-358737105B25";
	setAttr ".ihi" 0;
createNode groupId -n "groupId51";
	rename -uid "4DF9BD2C-5D4F-814B-69A3-B0B92DF20AED";
	setAttr ".ihi" 0;
createNode groupId -n "groupId52";
	rename -uid "27DE5493-B24C-8CA2-673A-14A5D5670685";
	setAttr ".ihi" 0;
createNode groupId -n "groupId53";
	rename -uid "16F9605F-104F-5D97-1F49-3FB8513D8BAC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId54";
	rename -uid "B9E2D857-CB4A-D2A3-F3FE-7EBF8598BC8A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId55";
	rename -uid "AA538886-5741-59C6-8F71-559BD681795C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId56";
	rename -uid "400F5B60-274F-6602-3BE2-87AEBBEA5069";
	setAttr ".ihi" 0;
createNode groupId -n "groupId57";
	rename -uid "E7704898-2D41-2B56-F709-C4AC3728D94E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId58";
	rename -uid "5449FB0B-5449-428A-53C4-4BB945380407";
	setAttr ".ihi" 0;
createNode groupId -n "groupId59";
	rename -uid "E9EC08D2-924E-CCAD-EF69-1B9309054F28";
	setAttr ".ihi" 0;
createNode groupId -n "groupId60";
	rename -uid "E67D372F-A24E-1B9D-E354-EFBEC2856CC4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId61";
	rename -uid "C6B291B5-4D40-2C8F-99B2-0FB39B583023";
	setAttr ".ihi" 0;
createNode groupId -n "groupId62";
	rename -uid "3CAA1873-5D45-EBF9-BCFB-23B2CBB744DA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId63";
	rename -uid "FB0F9B6D-E542-7CC4-F849-3591547BAFE0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId64";
	rename -uid "3CBE4404-F342-A769-5ACE-DBB8C94FE78C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId65";
	rename -uid "A1A7C7F6-1042-DFB6-96B5-AEBE2DAA8BDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId66";
	rename -uid "6264299E-9943-4850-2819-25A194864C91";
	setAttr ".ihi" 0;
createNode groupId -n "groupId67";
	rename -uid "1F85D337-DA4F-2D6C-5DB6-B08EA2A10E76";
	setAttr ".ihi" 0;
createNode groupId -n "groupId68";
	rename -uid "7680A800-1C4F-FF45-E0EE-F6B0096A664D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId69";
	rename -uid "8AD8A3D7-3748-BA1D-EBD0-29B921BCF43A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId70";
	rename -uid "C2A61854-7448-F64D-8D7D-F48AA2A6D9D2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId71";
	rename -uid "8BA24645-C84A-2A22-2163-D098377C8CF2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId72";
	rename -uid "4D0D1AAD-174A-2430-5358-B190E95110BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId73";
	rename -uid "B199F571-5748-6C44-17CE-A78A961CC712";
	setAttr ".ihi" 0;
createNode groupId -n "groupId74";
	rename -uid "E30392DA-AD41-0C31-AFF8-D49663F31C55";
	setAttr ".ihi" 0;
createNode groupId -n "groupId75";
	rename -uid "7D326F15-5347-3AB4-6C0B-13B50DCB84E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId76";
	rename -uid "C52970E4-544C-87A2-3DE8-DF85178BE3C1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId77";
	rename -uid "5AD7A396-2045-7B2C-468B-CBB0EC9BFE5F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId78";
	rename -uid "C86FA7A0-F54D-AEA2-E3B2-BFB55D5B1BDA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId79";
	rename -uid "2BED6264-7645-C495-E834-C3B5AC1D5B2B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId80";
	rename -uid "C6E70D50-124D-F787-3237-1AA12B23B166";
	setAttr ".ihi" 0;
createNode groupId -n "groupId81";
	rename -uid "22D5439C-844B-6DCF-FC54-2AB7F339724C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId82";
	rename -uid "CC5E42DA-7A40-51D9-5D56-6C9B897E84D8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId83";
	rename -uid "3FCEA8D7-3C41-CA89-9F66-A88D45EC7609";
	setAttr ".ihi" 0;
createNode groupId -n "groupId84";
	rename -uid "CC43D98F-8442-30FA-C52D-9AA5636B8D3A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId85";
	rename -uid "27EC4E3C-3D44-14B9-F472-80B938EB0EF2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId86";
	rename -uid "396DD6D0-504B-A13D-2AAC-82BB5874574E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId87";
	rename -uid "0953922E-8248-CCD4-76DD-759D323BD7CE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId88";
	rename -uid "9D2C5BD8-C14E-1C82-96AA-A6A58D28D2F2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId89";
	rename -uid "5E8A1D14-364E-A11A-9BCE-53BDCAB14D0B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId90";
	rename -uid "8D1199F2-7340-4E84-CFFC-B89D52C6A947";
	setAttr ".ihi" 0;
createNode groupId -n "groupId91";
	rename -uid "0647FE66-CD42-19BF-6FC6-D6B42A03EE89";
	setAttr ".ihi" 0;
createNode groupId -n "groupId92";
	rename -uid "B423DBA0-9A49-3854-9EFE-9E8BA3A6BDF6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId93";
	rename -uid "007FFE9A-3943-8F1F-17DE-28B3827209F4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId94";
	rename -uid "40E1279B-9A45-7A82-146A-97B55802C028";
	setAttr ".ihi" 0;
createNode groupId -n "groupId95";
	rename -uid "283B6098-B247-A451-0E0A-6DB2B4DDD619";
	setAttr ".ihi" 0;
createNode groupId -n "groupId96";
	rename -uid "67225DA6-7547-2B57-2686-2A9CD07EB6BE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId97";
	rename -uid "2AF954D2-E34D-13CA-D06C-6AB7D9615C5A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId98";
	rename -uid "2D5A0767-1447-287E-F9EA-689171EA77A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId99";
	rename -uid "D7430451-1448-69E5-4949-759B335C1D27";
	setAttr ".ihi" 0;
createNode groupId -n "groupId100";
	rename -uid "15DB24E6-824B-3321-A6EE-29A21EF8CEFC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId101";
	rename -uid "18C063D7-8849-0AA5-0EC8-D4AEB05B7575";
	setAttr ".ihi" 0;
createNode groupId -n "groupId102";
	rename -uid "4DAD6357-9F46-935D-EAB3-AA9C0DC43FA2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId103";
	rename -uid "89A804A4-8B4F-0BC1-EE9F-2F837B157365";
	setAttr ".ihi" 0;
createNode groupId -n "groupId104";
	rename -uid "F7870CF7-A34F-8A1A-07C6-5887CA00A82A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId105";
	rename -uid "4E40226D-CE4C-107C-C5B3-1A9F74EA78B0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId106";
	rename -uid "A61FF2F4-4149-F70A-81AA-0C971B423DE4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId107";
	rename -uid "DEAFACB9-5948-8299-A4A0-2CADDFCA0E55";
	setAttr ".ihi" 0;
createNode groupId -n "groupId108";
	rename -uid "A6DBF665-AC41-61E4-09E9-0CBCF28488CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId109";
	rename -uid "134FC1AE-EF4A-CA7B-EE03-968AFF150C5E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId110";
	rename -uid "8F98238D-3E41-F39E-DD6F-1191CCFAEA23";
	setAttr ".ihi" 0;
createNode groupId -n "groupId111";
	rename -uid "FE328124-3047-7A73-D468-4399BDBEE6CD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId112";
	rename -uid "625650BF-1F4F-A811-E460-99B6D0EF6CA1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId113";
	rename -uid "D55E76AF-E943-FFEE-BAEA-8BA4321C0F33";
	setAttr ".ihi" 0;
createNode groupId -n "groupId114";
	rename -uid "C028BD3A-7C47-65F4-92DF-4DA7E3741255";
	setAttr ".ihi" 0;
createNode groupId -n "groupId115";
	rename -uid "927C19D5-044D-E363-A47F-D48CAE061D05";
	setAttr ".ihi" 0;
createNode groupId -n "groupId116";
	rename -uid "A6F3061A-2644-D089-E47C-4F93AEAF1237";
	setAttr ".ihi" 0;
createNode groupId -n "groupId117";
	rename -uid "8C3EF800-8242-96AE-7974-4CBFC637E9D1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId118";
	rename -uid "E8C76F67-D34D-F912-4F5D-CCA775257B6F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId119";
	rename -uid "50E1F21E-1C41-5346-E586-D7BC6E13830D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId120";
	rename -uid "8526286F-8A4E-213F-4B6A-5383B013BE65";
	setAttr ".ihi" 0;
createNode groupId -n "groupId121";
	rename -uid "36E1EF3E-C040-1621-BD47-A3A20109A445";
	setAttr ".ihi" 0;
createNode groupId -n "groupId122";
	rename -uid "D3ABD4DF-E040-965A-4332-7B8519C18F07";
	setAttr ".ihi" 0;
createNode groupId -n "groupId123";
	rename -uid "C7781789-4A48-BBEA-1EB8-93BAD5FDCBCA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId124";
	rename -uid "95E377A5-244C-2DEE-68FA-71AE9D7B853D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId125";
	rename -uid "56A5C002-B84F-4455-2809-DE9E32FCFD6E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId126";
	rename -uid "1BC303F4-6040-487A-D5B2-6FB09DC8E787";
	setAttr ".ihi" 0;
createNode groupId -n "groupId127";
	rename -uid "7787200D-544F-27FD-482E-97BFDF719EBF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId128";
	rename -uid "245E08B2-0D41-AE00-DD15-E691FA56640F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId129";
	rename -uid "78030A68-C34C-4F98-54FC-3ABD54523F69";
	setAttr ".ihi" 0;
createNode groupId -n "groupId130";
	rename -uid "FB8BD622-5146-6131-28F2-F2A92153B651";
	setAttr ".ihi" 0;
createNode groupId -n "groupId131";
	rename -uid "6BC8D2B3-A449-1EC8-EA31-838E5E22E88E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId132";
	rename -uid "AB043F8C-9146-E1FC-34E0-02BCB9A4BA57";
	setAttr ".ihi" 0;
createNode groupId -n "groupId133";
	rename -uid "0DA81028-3A46-23E0-10B6-47885F97E783";
	setAttr ".ihi" 0;
createNode groupId -n "groupId134";
	rename -uid "EA40CD29-3B42-BA79-788B-1F9575ABC9CE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId135";
	rename -uid "1EC17D32-1540-B5D4-C2C7-60B097CF1E91";
	setAttr ".ihi" 0;
createNode groupId -n "groupId136";
	rename -uid "301359B5-1740-298E-F099-31810B7B0110";
	setAttr ".ihi" 0;
createNode groupId -n "groupId137";
	rename -uid "1C3F49D2-7749-3D69-CB3C-1EB30211A67B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId138";
	rename -uid "805BDA03-FF45-FE7D-A4E9-AFB57A811D20";
	setAttr ".ihi" 0;
createNode groupId -n "groupId139";
	rename -uid "065EEB4D-3849-C370-F1BD-E68AD7615EDE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId140";
	rename -uid "D6C35ABF-104B-BCBF-27A7-F7B186C11161";
	setAttr ".ihi" 0;
createNode groupId -n "groupId141";
	rename -uid "D5E6F553-734C-A5A0-2B60-3D9869057EBD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId142";
	rename -uid "30038ACA-0543-21D3-413A-C689478B4053";
	setAttr ".ihi" 0;
createNode groupId -n "groupId143";
	rename -uid "47CA1726-CF41-81BE-3FC3-788EB35B75B1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId144";
	rename -uid "B42A61D1-674B-B6AF-35B1-6B95A37B941B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId145";
	rename -uid "03AEAE9B-5A4A-8E67-DD28-76B25DAE6665";
	setAttr ".ihi" 0;
createNode groupId -n "groupId146";
	rename -uid "3CF0FAB7-4D4C-F969-187D-1883802E9823";
	setAttr ".ihi" 0;
createNode groupId -n "groupId147";
	rename -uid "1589F4E3-B942-0A13-66B1-9DBE1D309636";
	setAttr ".ihi" 0;
createNode groupId -n "groupId148";
	rename -uid "B5D2EF60-054F-809C-9744-0585DDBDA55D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId149";
	rename -uid "F95243E6-7742-D7F2-828D-FB978223CF77";
	setAttr ".ihi" 0;
createNode groupId -n "groupId150";
	rename -uid "28A45107-334F-2236-34D5-2BAF2C0433AE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId151";
	rename -uid "63B1BEA6-EB49-DFF4-C9EE-9DBD46C27B61";
	setAttr ".ihi" 0;
createNode groupId -n "groupId152";
	rename -uid "3BBA27EF-3C49-D1BD-C6D0-5FA61DF447C8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId153";
	rename -uid "B8641563-1245-5295-00A1-73861AA71F72";
	setAttr ".ihi" 0;
createNode groupId -n "groupId154";
	rename -uid "FE5D04DB-C645-26E8-7C14-67AD4A344E17";
	setAttr ".ihi" 0;
createNode groupId -n "groupId155";
	rename -uid "1FC8032D-A344-85F8-EA2F-9BA1758E2ABB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId156";
	rename -uid "021A59D4-D647-81E5-1057-BA93F31EFDC2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId157";
	rename -uid "9950DCA4-C444-A4AA-9568-B391E07A1D79";
	setAttr ".ihi" 0;
createNode groupId -n "groupId158";
	rename -uid "E8EB1273-CB44-1D20-A466-51AA00CF5836";
	setAttr ".ihi" 0;
createNode groupId -n "groupId159";
	rename -uid "C3D66F76-EC4D-1767-BC7E-8493BBADC61A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId160";
	rename -uid "E363091B-EB4D-FD9D-8602-7E88715CFF1A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId161";
	rename -uid "DB354FAE-DD48-C5C6-C825-5C8620C01316";
	setAttr ".ihi" 0;
createNode groupId -n "groupId162";
	rename -uid "64195023-7446-6B70-4479-C5BD048E6F63";
	setAttr ".ihi" 0;
createNode groupId -n "groupId163";
	rename -uid "A140DD24-1B47-E419-E3E1-E0896C20753A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId164";
	rename -uid "B9B8F1D2-E146-8B0C-25FB-23A4D8295D75";
	setAttr ".ihi" 0;
createNode groupId -n "groupId165";
	rename -uid "17C425AB-B743-0BFC-DB3B-ADAFDB4E8410";
	setAttr ".ihi" 0;
createNode groupId -n "groupId166";
	rename -uid "BBD51AAD-A74B-1DBA-6655-EAA323B0C8B7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId167";
	rename -uid "A264C02D-0C41-3607-7627-E6963512237D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId168";
	rename -uid "6301EDAD-7349-5496-F608-9FB5EEB7BD9F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId169";
	rename -uid "CC9CFF2B-9B4D-F9F0-A642-41A4D52FF67A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId170";
	rename -uid "BF6508D7-2042-ECE7-F3F0-A488E19335DC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId171";
	rename -uid "01FCA517-DF44-7FDB-7FED-2480A245E092";
	setAttr ".ihi" 0;
createNode groupId -n "groupId172";
	rename -uid "149E5225-5B42-80F9-454B-8BA3BBEF8DEB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId173";
	rename -uid "F25CD815-E646-FA4C-BDB7-21A528915AE4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId174";
	rename -uid "6D5205A9-DE4A-E096-55B1-32BF56BE9EAE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId175";
	rename -uid "FCC9FA80-7C4C-B2C1-9FE6-EB9F39F2D32F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId176";
	rename -uid "F8E73BFF-A54E-E25D-F3B4-388218F31C9C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId177";
	rename -uid "A9EB2417-FC4A-5254-EB2A-8E92CC7507EB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId178";
	rename -uid "90AC3A1F-FC4B-241B-BBA0-22AC14893E8A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId179";
	rename -uid "D04C3D6A-C34E-9EEE-6500-A5B30D852533";
	setAttr ".ihi" 0;
createNode groupId -n "groupId180";
	rename -uid "5BF16ACE-F248-13E5-D0D1-6DB2764EF5D8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId181";
	rename -uid "608ABE4E-9F4D-B1B1-A242-B7974A8017E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId182";
	rename -uid "81D54CC5-E141-E19A-9B85-FEA78F253BDB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId183";
	rename -uid "3692A4B4-F543-B526-1ACB-CBB8A2EA0805";
	setAttr ".ihi" 0;
createNode groupId -n "groupId184";
	rename -uid "7E9BD697-674C-0F43-A280-86848EE0F7B7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId185";
	rename -uid "4FA6317F-A045-8DF4-AAD9-CE89ED8D6D98";
	setAttr ".ihi" 0;
createNode groupId -n "groupId186";
	rename -uid "42B7D804-9E4E-9F21-7534-F5B8B20E7244";
	setAttr ".ihi" 0;
createNode groupId -n "groupId187";
	rename -uid "1FC761E1-BD42-5F80-82BD-29A5EF287F3C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId188";
	rename -uid "A1EBF0D0-E74E-B04E-09A8-C48DD23156EA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId189";
	rename -uid "2F1DFEC5-0142-F37D-6CF5-489BDB772827";
	setAttr ".ihi" 0;
createNode groupId -n "groupId190";
	rename -uid "D58F58A2-4045-AC93-FC72-9C8F67A8035F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId191";
	rename -uid "A05B58A8-834D-8603-DC6C-1298C5C9FD6D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId192";
	rename -uid "404A59A7-9B4C-7B84-731A-17900C1FCB02";
	setAttr ".ihi" 0;
createNode groupId -n "groupId193";
	rename -uid "F21C76A5-A14A-FEE8-89C3-F7B1322D350C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId194";
	rename -uid "3DA93F8A-624E-DD48-212A-229C15E8F2E0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId195";
	rename -uid "6487CDFE-754D-15FE-99B4-239861A8B8F2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId196";
	rename -uid "297C70BB-5445-4CC6-1B17-6E9A866E1ED9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId197";
	rename -uid "35294D76-234D-9D98-1CA4-D980F1DF85E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId198";
	rename -uid "88430A72-7E42-48AF-E61D-0A8131AE874C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId199";
	rename -uid "ACBCFF6C-5E4E-1AEE-4117-DCBB3E7188F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId200";
	rename -uid "06A87B32-4546-D159-F0DC-39A1A3C1374E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId201";
	rename -uid "EDC440FB-624A-C25C-0140-96AD6428E216";
	setAttr ".ihi" 0;
createNode groupId -n "groupId202";
	rename -uid "1B3F26C1-D341-2961-9373-BB84134BD2BF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId203";
	rename -uid "B1CDBA40-4B4A-BF11-0BFE-569CA0D38300";
	setAttr ".ihi" 0;
createNode groupId -n "groupId204";
	rename -uid "B4E8DAFC-6F43-772F-9395-619AD19F158E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId205";
	rename -uid "BCD1663C-884B-CBB9-CD3B-66A8D8D7D2BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId206";
	rename -uid "8458F73B-C241-F329-A9AE-0284B00F11DD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId207";
	rename -uid "8393385E-BB49-F63C-F8CE-E495DA13C9D5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId208";
	rename -uid "2C2D6FE0-1E4A-67AF-A4F5-148B83835E56";
	setAttr ".ihi" 0;
createNode groupId -n "groupId209";
	rename -uid "DEC81C5F-F845-05BC-9D00-6CBFE42D6B97";
	setAttr ".ihi" 0;
createNode groupId -n "groupId210";
	rename -uid "A088FE1D-344B-D19B-DB1F-81A604094C6B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId211";
	rename -uid "E31712B5-8E4F-1071-643D-9E9DA1BCBBE3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId212";
	rename -uid "C0EFA47C-F34F-A15E-B40F-3B876602634F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId213";
	rename -uid "3F928FD0-8649-4DD3-36A0-AA867845FB58";
	setAttr ".ihi" 0;
createNode groupId -n "groupId214";
	rename -uid "E0CC6FB5-9E4C-4AF9-1E11-27A17E3159B5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId215";
	rename -uid "9E1A1A31-CB4F-13DC-BBC2-04921FF2888F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId216";
	rename -uid "38AC9AD2-A946-0123-40F3-E69918417935";
	setAttr ".ihi" 0;
createNode groupId -n "groupId217";
	rename -uid "0F853AB8-834E-4067-0C6A-CCB456F4F2D4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId218";
	rename -uid "9E3A4765-5446-F541-E538-019AB3DC1E3B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId219";
	rename -uid "E18E0053-D842-44B2-3775-4B87106D3C74";
	setAttr ".ihi" 0;
createNode groupId -n "groupId220";
	rename -uid "863C5C1E-154B-1B4F-6E55-E79BBAC000A9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId221";
	rename -uid "E1BFAE45-EC41-2355-2946-D29AB76DE64E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId222";
	rename -uid "DE3B8FD9-C249-AD28-88A3-4BB25B62B07C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId223";
	rename -uid "37D15AAB-7842-3723-8FA3-F7851C1700CE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId224";
	rename -uid "5CDBEBE8-3B41-4051-330F-488D128DB9B0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId225";
	rename -uid "3ADF7B6D-6F48-5058-0CD6-98AEC5DF0326";
	setAttr ".ihi" 0;
createNode groupId -n "groupId226";
	rename -uid "013C4613-C240-8647-31DE-328EADAA6748";
	setAttr ".ihi" 0;
createNode groupId -n "groupId227";
	rename -uid "701F1574-A942-7C60-1E6C-CFB76005AB0C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId228";
	rename -uid "57C95740-F041-0D0A-3DEB-E394BEE7ED96";
	setAttr ".ihi" 0;
createNode groupId -n "groupId229";
	rename -uid "5554AED9-3047-26B8-E2A9-F3972305F9FA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId230";
	rename -uid "5913247A-204E-A279-D198-DB85128D00D5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId231";
	rename -uid "3E11DDA7-2E4D-C2DC-F88B-CEBF6C91C1B3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId232";
	rename -uid "061F29DF-2B47-02EE-0921-408789E8406C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId233";
	rename -uid "25E2F92D-624C-6B5A-027A-108F3A008036";
	setAttr ".ihi" 0;
createNode groupId -n "groupId234";
	rename -uid "89097828-F941-63A4-9CD2-7BA1B90BA55A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId235";
	rename -uid "E7687492-EE48-047A-D03C-0BB5DE735FA4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId236";
	rename -uid "DD7AB979-D544-EF77-ECB4-C9886B19489A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId237";
	rename -uid "D5677684-9A42-3F0C-2129-E1AF33897A00";
	setAttr ".ihi" 0;
createNode groupId -n "groupId238";
	rename -uid "4E75F858-FA44-8A99-CB36-3996034F69E5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId239";
	rename -uid "66BB1FE6-014A-3BCF-952F-E68A2E95DEA0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId240";
	rename -uid "DEAB9667-824F-75B5-2BF8-04B1F50E7C95";
	setAttr ".ihi" 0;
createNode groupId -n "groupId241";
	rename -uid "F5B60018-974C-B362-D149-C09FFDC3A39E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId242";
	rename -uid "52C25FA4-624D-91EF-A309-948F8922C969";
	setAttr ".ihi" 0;
createNode groupId -n "groupId243";
	rename -uid "2E3CFC30-1C4A-C9A0-98AA-00B31D291B8D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId244";
	rename -uid "FA08B2AB-4A4E-7239-77CC-CC929C6C6DBA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId245";
	rename -uid "157E50C7-A741-7B62-36D5-3EB3124C5730";
	setAttr ".ihi" 0;
createNode groupId -n "groupId246";
	rename -uid "276B0A22-4D42-E501-BFFE-619E861B28E9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId247";
	rename -uid "97EB5576-AA46-58EB-A563-889A0A4CF6E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId248";
	rename -uid "742F1E80-9444-9C70-57BD-B68EDB5DDD0D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId249";
	rename -uid "1A87A9F1-E847-7419-CB2C-50900F45F5BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId250";
	rename -uid "50C05578-4148-C23D-697D-DC99B4914364";
	setAttr ".ihi" 0;
createNode groupId -n "groupId251";
	rename -uid "241A8314-E647-6A4E-B341-5F9586372B69";
	setAttr ".ihi" 0;
createNode groupId -n "groupId252";
	rename -uid "D991F73C-CC4D-1C37-849B-CA8912C27D15";
	setAttr ".ihi" 0;
createNode groupId -n "groupId253";
	rename -uid "52058029-8940-CB03-FAC3-E6A573F563CC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId254";
	rename -uid "139C2364-3143-A68D-D13D-C6AF7ABCC3EC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId255";
	rename -uid "77EBC581-F847-864C-0E64-039920FD91DD";
	setAttr ".ihi" 0;
createNode groupId -n "groupId256";
	rename -uid "3D8330EE-7243-BD39-9946-F2BA1D039F94";
	setAttr ".ihi" 0;
createNode groupId -n "groupId257";
	rename -uid "338D47E4-3E4E-C1AE-C1B6-C085DB1ADF0C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId258";
	rename -uid "8CC6D925-8B45-E533-6BB2-B3A0D1D08787";
	setAttr ".ihi" 0;
createNode groupId -n "groupId259";
	rename -uid "63B0B45B-D242-C507-6B7D-1DACB865E921";
	setAttr ".ihi" 0;
createNode groupId -n "groupId260";
	rename -uid "2DD0E3DB-F447-1B84-22CC-639EF8C39653";
	setAttr ".ihi" 0;
createNode groupId -n "groupId261";
	rename -uid "AA2E8B96-6E49-202F-2F92-BE820F46BAB6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId262";
	rename -uid "7FA31D6C-2846-F4AA-82D5-AB9EB11DCED8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId263";
	rename -uid "6FBA3528-1745-78B4-E733-61AED869F3E6";
	setAttr ".ihi" 0;
createNode groupId -n "groupId264";
	rename -uid "2169053E-5C40-AFE1-6CA1-C4B560C65136";
	setAttr ".ihi" 0;
createNode groupId -n "groupId265";
	rename -uid "60C6C107-E442-DD6C-739A-398E297D6EAF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId266";
	rename -uid "D2312571-4E44-BCF1-6B21-93BB7133592D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId267";
	rename -uid "67118216-F04F-9B34-0BCC-B09805B2F193";
	setAttr ".ihi" 0;
createNode groupId -n "groupId268";
	rename -uid "FC76E3BA-BE4B-0975-5B84-DC913E245179";
	setAttr ".ihi" 0;
createNode groupId -n "groupId269";
	rename -uid "48C896AC-F34C-AF4F-6C30-D29BCDD7AA8F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId270";
	rename -uid "40C3E6F2-B141-AFE5-FB99-29B982622B99";
	setAttr ".ihi" 0;
createNode groupId -n "groupId271";
	rename -uid "831E856B-6141-A8C6-3ACC-80ABA520CBAC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId272";
	rename -uid "0703D4FD-F242-52FD-2770-4B8BF0F61C8A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId273";
	rename -uid "E820EF52-714B-830F-113F-068EC047446B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId274";
	rename -uid "D2002B12-1242-2033-463B-96ACE4A9F2D4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId275";
	rename -uid "3A9A9630-8C49-2EF0-8DEF-479C097298B0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId276";
	rename -uid "955249AC-A547-61F7-6CDF-B3A262DD8181";
	setAttr ".ihi" 0;
createNode groupId -n "groupId277";
	rename -uid "638B2B78-4B41-3065-1131-FB9D45F0F033";
	setAttr ".ihi" 0;
createNode groupId -n "groupId278";
	rename -uid "17EAC1D9-1F45-ED3C-F02C-A186347286E5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId279";
	rename -uid "D5647E20-E843-439A-DC17-9C9A428B1189";
	setAttr ".ihi" 0;
createNode groupId -n "groupId280";
	rename -uid "137A2EE8-104F-BD68-45F3-54BF5F2A7004";
	setAttr ".ihi" 0;
createNode groupId -n "groupId281";
	rename -uid "D450A8B5-A64F-E409-D6B2-49B47236CE9E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId282";
	rename -uid "1154651D-734C-FFFD-1DD4-A1AD2A873F1E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId283";
	rename -uid "EAA4453C-944C-0A89-CDE5-BBA70DF7D6D4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId284";
	rename -uid "FEF3C284-844B-0EC7-DA44-A991A7C0DB59";
	setAttr ".ihi" 0;
createNode groupId -n "groupId285";
	rename -uid "2CD1B329-A748-26CA-1B3A-A5A9717D9E14";
	setAttr ".ihi" 0;
createNode groupId -n "groupId286";
	rename -uid "1EE0C26D-994F-3659-384D-DB80DF1CB6AC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId287";
	rename -uid "449D8986-5C47-C73D-6496-E4AD2C67DB13";
	setAttr ".ihi" 0;
createNode groupId -n "groupId288";
	rename -uid "1F5C26C2-3E45-2F34-760C-D785850BE96E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId289";
	rename -uid "7216D9A3-E949-37F9-8EBC-96B417B98895";
	setAttr ".ihi" 0;
createNode groupId -n "groupId290";
	rename -uid "0ABF7EC6-AA4D-5856-D0BB-A28EE659A06E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId291";
	rename -uid "B66B5C28-3B49-7D18-D4EA-FBB761353B2A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId292";
	rename -uid "17A0D9BC-5944-045C-0F42-0EA730EA50EF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId293";
	rename -uid "54E18928-3845-F06B-69B7-07872980AB6D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId294";
	rename -uid "FD528EF4-CE49-00F8-046C-9A814F65E94E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId295";
	rename -uid "ADB0B643-8E43-49FB-873B-D091DDE7AB90";
	setAttr ".ihi" 0;
createNode groupId -n "groupId296";
	rename -uid "175BDB0B-864A-C3C3-EC47-32A0D059D888";
	setAttr ".ihi" 0;
createNode groupId -n "groupId297";
	rename -uid "59730D47-6E4B-3A76-E66E-0F8B7383FD50";
	setAttr ".ihi" 0;
createNode groupId -n "groupId298";
	rename -uid "4BA97492-DE49-5C37-250A-43B8FBD0F46F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId299";
	rename -uid "75D73966-FD4B-35BE-C1F1-61A309E8C898";
	setAttr ".ihi" 0;
createNode groupId -n "groupId300";
	rename -uid "32B54D9E-7F4E-AB60-8DE3-03A606E3F3E2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId301";
	rename -uid "8F78AD60-2646-6098-BF4D-AB829093C871";
	setAttr ".ihi" 0;
createNode groupId -n "groupId302";
	rename -uid "9778797A-F24F-5921-CCFA-E98934CF43A1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId303";
	rename -uid "DF5E121F-D547-D76A-F032-B8BC2607FEFB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId304";
	rename -uid "21B93EE5-3047-1D49-F68D-5B8E3C45445D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId305";
	rename -uid "F96726F3-3B41-67C3-7937-9F83B243D6BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId306";
	rename -uid "13EE1E68-BF44-DC4F-3E95-819A20116BBE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId307";
	rename -uid "11F8C5D3-5B4E-3CEB-74C7-8B8BBEB2DDB4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId308";
	rename -uid "B5BBC411-3048-F27A-D157-1EA814290C6A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId309";
	rename -uid "78F0C105-B44B-BD1B-1B99-96885BEFD370";
	setAttr ".ihi" 0;
createNode groupId -n "groupId310";
	rename -uid "9AFD7171-C747-FD17-30F6-7B81CB798A31";
	setAttr ".ihi" 0;
createNode groupId -n "groupId311";
	rename -uid "B70BF269-774C-5F10-5DA1-6191842F701D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId312";
	rename -uid "41D44F97-CE4B-837B-8BC1-9094294BACF4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId313";
	rename -uid "A9906950-5B4A-6A5B-8B46-1F963AD63092";
	setAttr ".ihi" 0;
createNode groupId -n "groupId314";
	rename -uid "8FD07D7C-1548-7AF4-EE5C-FEB5D22A306D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId315";
	rename -uid "32BEE376-EE4D-6219-3E2C-4FBA21AC222F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId316";
	rename -uid "B9B60941-8A48-EA17-9D08-7BAD00C3C434";
	setAttr ".ihi" 0;
createNode groupId -n "groupId317";
	rename -uid "B8E22ED2-E54E-EF1C-03AF-66883AE328EB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId318";
	rename -uid "660693DE-CD4E-91AA-683F-B4B78994EA42";
	setAttr ".ihi" 0;
createNode groupId -n "groupId319";
	rename -uid "ECB14FC7-B74B-5F83-552F-EAAF72755B98";
	setAttr ".ihi" 0;
createNode groupId -n "groupId320";
	rename -uid "A3DFC345-214C-875D-3D15-B2ABF89D7D1F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId321";
	rename -uid "33C2DC1F-D645-598E-462B-3FAC68E4843A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId322";
	rename -uid "742CB864-0545-671A-4C56-AB9A790736B0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId323";
	rename -uid "9C2F8ABF-0444-C479-EED0-068E9A67D487";
	setAttr ".ihi" 0;
createNode groupId -n "groupId324";
	rename -uid "3AC14BC5-5C41-F501-1760-4FB879910549";
	setAttr ".ihi" 0;
createNode groupId -n "groupId325";
	rename -uid "F3BC3771-604E-1EC1-0707-E3A41353B2A3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId326";
	rename -uid "5F736FE2-6A4D-7A21-C700-45B03A4F5EE9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId327";
	rename -uid "9D01D5F7-274E-2A81-CD00-719DA37243F1";
	setAttr ".ihi" 0;
createNode groupId -n "groupId328";
	rename -uid "07F389BE-D447-996F-A96A-CCA627F6A91D";
	setAttr ".ihi" 0;
createNode groupId -n "groupId329";
	rename -uid "67328210-8845-E439-A431-A7ABE1A2E106";
	setAttr ".ihi" 0;
createNode groupId -n "groupId330";
	rename -uid "15E1AB7E-5546-939E-43C3-F9A2B0D75CE0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId331";
	rename -uid "116F4EB0-104C-0315-140E-6F861BB4070A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId332";
	rename -uid "6DC0E19B-CB48-9FE6-1217-83A00EB3E213";
	setAttr ".ihi" 0;
createNode groupId -n "groupId333";
	rename -uid "5FA42C64-7149-72DA-8912-36B04401F27B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId334";
	rename -uid "8F33ACCF-114E-906B-2C52-2289F5EA8D3C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId335";
	rename -uid "B90EED07-2B4C-A27A-9806-328A8F42F7AC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId336";
	rename -uid "DF22CA4E-004D-B015-8A8C-47A290C055F4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId337";
	rename -uid "251D0922-1143-5371-7157-C5B0063D7385";
	setAttr ".ihi" 0;
createNode groupId -n "groupId338";
	rename -uid "0C5A5C5D-6747-9C9D-8DD5-5894E39D8FA3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId339";
	rename -uid "57E81030-CC40-DD67-BB37-8A9A9FB3F332";
	setAttr ".ihi" 0;
createNode groupId -n "groupId340";
	rename -uid "DD108B03-3146-C19E-2D26-769C30DBF75C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId341";
	rename -uid "BFED4576-7946-E056-8BA8-DBB3980DEFB4";
	setAttr ".ihi" 0;
createNode groupId -n "groupId342";
	rename -uid "CBA6DEF4-004A-B201-4187-CA83EDBC534F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId343";
	rename -uid "7FB5F585-F444-8805-1990-8093077435EE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId344";
	rename -uid "6EA4B8AC-6742-0AE5-8F93-A3992F1A811F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId345";
	rename -uid "5A3DD704-E249-62F5-EC76-A19B00C5451F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId346";
	rename -uid "A4E7CA36-3846-B7C1-B5F0-96A8D7AAD42C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId347";
	rename -uid "EB9B0F7E-D94B-2F73-69E7-A6BC7E7991C5";
	setAttr ".ihi" 0;
createNode groupId -n "groupId348";
	rename -uid "F8BE24FA-1E4C-3A8B-39AE-0F8D5A3AD413";
	setAttr ".ihi" 0;
createNode groupId -n "groupId349";
	rename -uid "754C232D-9044-C1A9-3161-EC89999DB8D2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId350";
	rename -uid "B16FE521-F14F-FDD9-F552-E0B9F9966DC9";
	setAttr ".ihi" 0;
createNode groupId -n "groupId351";
	rename -uid "8F3A4EC9-2A4B-CE4C-228F-6391BF522727";
	setAttr ".ihi" 0;
createNode groupId -n "groupId352";
	rename -uid "155AAED9-E449-86DC-DAE7-A78D66B38379";
	setAttr ".ihi" 0;
createNode groupId -n "groupId353";
	rename -uid "22427A1D-9E41-8D24-6141-268BB975B324";
	setAttr ".ihi" 0;
createNode groupId -n "groupId354";
	rename -uid "3EE79C18-824D-D9BB-5E73-11B64C5347D2";
	setAttr ".ihi" 0;
createNode groupId -n "groupId355";
	rename -uid "881CB96F-D148-B381-B504-90ACED1ADE54";
	setAttr ".ihi" 0;
createNode groupId -n "groupId356";
	rename -uid "EA454111-464F-958C-41BF-4390DE008076";
	setAttr ".ihi" 0;
createNode groupId -n "groupId357";
	rename -uid "A6872E35-C94F-D432-F2AB-17A2DE16244A";
	setAttr ".ihi" 0;
createNode groupId -n "groupId358";
	rename -uid "61407BC5-BE4B-4C64-E845-21B7D0D43844";
	setAttr ".ihi" 0;
createNode groupId -n "groupId359";
	rename -uid "B68B3E4B-DE49-7CCC-4360-AF9D2486FCBB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId360";
	rename -uid "1C02A7B8-E64F-EA97-54DE-0DA669A79DF8";
	setAttr ".ihi" 0;
createNode groupId -n "groupId361";
	rename -uid "14398FE0-7F44-3ADC-90F4-FE90D903B0FE";
	setAttr ".ihi" 0;
createNode groupId -n "groupId362";
	rename -uid "CE7CC8C8-DA47-9951-0A0F-DBAFD262C8D3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId363";
	rename -uid "93F71E63-C441-A237-16DC-1F81948546F7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId364";
	rename -uid "3D15C75D-5249-EC43-5862-3EA5652AA038";
	setAttr ".ihi" 0;
createNode groupId -n "groupId365";
	rename -uid "022E8661-B74A-D3D5-4044-6CB84600AD88";
	setAttr ".ihi" 0;
createNode groupId -n "groupId366";
	rename -uid "5A2967C3-8040-96D9-4FFF-CCA40B95B3ED";
	setAttr ".ihi" 0;
createNode groupId -n "groupId367";
	rename -uid "83124487-F84D-E33B-48CA-FA9F1CE3C85B";
	setAttr ".ihi" 0;
createNode groupId -n "groupId368";
	rename -uid "1521B0DD-EE41-FEBE-B662-D997921F5DAB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId369";
	rename -uid "A322F69D-AB43-8A76-5301-AE8FDBC0CF65";
	setAttr ".ihi" 0;
createNode groupId -n "groupId370";
	rename -uid "86206291-8A49-A8C0-E60F-189C6E55447C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId371";
	rename -uid "6407F340-7244-FE2D-B094-F58755D480BA";
	setAttr ".ihi" 0;
createNode groupId -n "groupId372";
	rename -uid "BA570A71-6546-1348-41CB-8F85F7D637F0";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts5";
	rename -uid "68ADCF23-E74D-768E-B152-ABACE72BD58E";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId373";
	rename -uid "1ED2221B-8A42-36B3-D2CE-A28043E90036";
	setAttr ".ihi" 0;
createNode groupId -n "groupId374";
	rename -uid "BAB1DAC2-EE4D-6B88-48ED-0CB420E44A17";
	setAttr ".ihi" 0;
createNode groupId -n "groupId375";
	rename -uid "3B834DC4-6A48-38B0-B3F8-49864C4684DF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId376";
	rename -uid "25C6F9BC-C54B-21FA-82BE-FFA62B47DD61";
	setAttr ".ihi" 0;
createNode groupId -n "groupId377";
	rename -uid "8983216D-8145-B305-93DE-BCB38B08A75E";
	setAttr ".ihi" 0;
createNode groupId -n "groupId378";
	rename -uid "B008B387-F14D-0925-A6E2-A69339D6261C";
	setAttr ".ihi" 0;
createNode groupId -n "groupId379";
	rename -uid "CC66AEBE-DB4C-F83B-A815-6ABC83877F97";
	setAttr ".ihi" 0;
createNode groupId -n "groupId380";
	rename -uid "8F168F40-EC47-0152-30F4-C4BF1014ACF7";
	setAttr ".ihi" 0;
createNode groupId -n "groupId381";
	rename -uid "5D8FC25F-EE4C-E28E-6CFE-34BEFC4CCE02";
	setAttr ".ihi" 0;
createNode groupId -n "groupId382";
	rename -uid "2B27D31E-2A47-3CF9-AEB4-828EBC834D85";
	setAttr ".ihi" 0;
createNode groupId -n "groupId383";
	rename -uid "7DDEB30B-0642-BD45-D1F4-9082AFC7F7E3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId384";
	rename -uid "4CE18706-4F4A-CCD3-76F1-B7A58C14EF71";
	setAttr ".ihi" 0;
createNode groupId -n "groupId385";
	rename -uid "AB138E0A-DE47-8636-C9B4-91AAE0AA6963";
	setAttr ".ihi" 0;
createNode groupId -n "groupId386";
	rename -uid "FA918498-7449-C14C-CF13-34B53A992521";
	setAttr ".ihi" 0;
createNode groupId -n "groupId387";
	rename -uid "85E72C0D-B24D-336A-BDCA-B2A061291735";
	setAttr ".ihi" 0;
createNode groupId -n "groupId388";
	rename -uid "B456C3FA-D543-D8C8-CBA4-11AA987780DC";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	rename -uid "FD1525B3-184A-06CA-DE7E-AF8014812387";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "f[0:5]";
createNode groupId -n "groupId389";
	rename -uid "45EBD50F-444F-2E21-0630-F7A1BE055EF3";
	setAttr ".ihi" 0;
createNode groupId -n "groupId390";
	rename -uid "3148B8E3-F840-7803-3B1A-849CF1370ADF";
	setAttr ".ihi" 0;
createNode groupId -n "groupId391";
	rename -uid "010613E5-EA4D-7A4D-ED0A-11A4548C8243";
	setAttr ".ihi" 0;
createNode groupId -n "groupId392";
	rename -uid "5BA5C115-4747-1BD9-A4D4-D79705116618";
	setAttr ".ihi" 0;
createNode groupId -n "groupId393";
	rename -uid "710A3DBC-4A4F-35A1-881B-77AF1537F13F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId394";
	rename -uid "90038702-2F4A-C063-F156-59851E1DD5F0";
	setAttr ".ihi" 0;
createNode groupId -n "groupId395";
	rename -uid "E4AE90BB-B248-50AF-FC01-86B50320A588";
	setAttr ".ihi" 0;
createNode groupId -n "groupId396";
	rename -uid "1B8A1CDA-6446-6871-227F-85A500D340DC";
	setAttr ".ihi" 0;
createNode groupId -n "groupId397";
	rename -uid "44CAF4B2-7D41-4518-CD8A-0B9EF0F830BB";
	setAttr ".ihi" 0;
createNode groupId -n "groupId398";
	rename -uid "18FE28C3-3D46-E0ED-1080-AEAFDD1DB34F";
	setAttr ".ihi" 0;
createNode groupId -n "groupId399";
	rename -uid "5DF144F6-D64B-C812-EEE6-359D6C724522";
	setAttr ".ihi" 0;
createNode groupId -n "groupId400";
	rename -uid "774C265B-074A-1C68-A705-16940F006B41";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts7";
	rename -uid "83D55526-BD42-B139-4710-8EA725711B42";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[0:5]" "f[24:821]" "f[846:905]" "f[912:941]" "f[954:977]" "f[984:1079]";
createNode groupId -n "groupId401";
	rename -uid "148C7007-8843-E53E-FC04-5EAA4EAD8215";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	rename -uid "938AED68-D44F-BECD-E801-65896DBC2D1D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 6 "f[6:23]" "f[822:845]" "f[906:911]" "f[942:953]" "f[978:983]" "f[1095:1118]";
createNode groupId -n "groupId402";
	rename -uid "7FFC6ECD-8846-72EE-FBA4-989BF4F7E65C";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts9";
	rename -uid "261835CC-AA42-4B58-9DC1-DF885B707990";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[1080]" "f[1082:1088]" "f[1092:1094]";
createNode groupId -n "groupId403";
	rename -uid "6C4945C9-F049-65C5-275C-E19F63DD9404";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts10";
	rename -uid "03561127-974E-B454-EAEA-358EA56D3D59";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1081]" "f[1089:1091]";
createNode groupId -n "groupId404";
	rename -uid "6FF9481C-0A45-1634-BA2D-38B43210FB9D";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts11";
	rename -uid "D4DF2582-F649-9A69-37C0-3992EDE6683B";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 3 "f[1119:1172]" "f[1179:1184]" "f[1191:1202]";
createNode groupId -n "groupId405";
	rename -uid "3727C359-BD47-5ECE-AD43-1AABB230CEF3";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts12";
	rename -uid "DB566CE7-6C49-B5E4-06DB-7E93A919C90D";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 2 "f[1173:1178]" "f[1185:1190]";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -s 8 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 10 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
connectAttr "groupId4.id" "pCubeShape1.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape1.iog.og[0].gco";
connectAttr "groupParts3.og" "pCubeShape1.i";
connectAttr "groupId5.id" "pCubeShape1.ciog.cog[0].cgid";
connectAttr "groupId6.id" "pCubeShape2.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape2.iog.og[0].gco";
connectAttr "groupParts4.og" "pCubeShape2.i";
connectAttr "groupId7.id" "pCubeShape2.ciog.cog[0].cgid";
connectAttr "groupId8.id" "pCubeShape3.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape3.iog.og[0].gco";
connectAttr "groupId9.id" "pCubeShape3.ciog.cog[0].cgid";
connectAttr "groupId10.id" "pCubeShape5.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape5.iog.og[0].gco";
connectAttr "groupId11.id" "pCubeShape5.ciog.cog[0].cgid";
connectAttr "groupId12.id" "pCubeShape8.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape8.iog.og[0].gco";
connectAttr "groupId13.id" "pCubeShape8.ciog.cog[0].cgid";
connectAttr "groupId14.id" "pCubeShape9.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape9.iog.og[0].gco";
connectAttr "groupId15.id" "pCubeShape9.ciog.cog[0].cgid";
connectAttr "groupId16.id" "pCubeShape10.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape10.iog.og[0].gco";
connectAttr "groupId17.id" "pCubeShape10.ciog.cog[0].cgid";
connectAttr "groupId18.id" "pCubeShape11.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape11.iog.og[0].gco";
connectAttr "groupId19.id" "pCubeShape11.ciog.cog[0].cgid";
connectAttr "groupId20.id" "pCubeShape12.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape12.iog.og[0].gco";
connectAttr "groupId21.id" "pCubeShape12.ciog.cog[0].cgid";
connectAttr "groupId22.id" "pCubeShape13.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape13.iog.og[0].gco";
connectAttr "groupId23.id" "pCubeShape13.ciog.cog[0].cgid";
connectAttr "groupId24.id" "pCubeShape14.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape14.iog.og[0].gco";
connectAttr "groupId25.id" "pCubeShape14.ciog.cog[0].cgid";
connectAttr "groupId26.id" "pCubeShape15.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape15.iog.og[0].gco";
connectAttr "groupId27.id" "pCubeShape15.ciog.cog[0].cgid";
connectAttr "groupId28.id" "pCubeShape16.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape16.iog.og[0].gco";
connectAttr "groupId29.id" "pCubeShape16.ciog.cog[0].cgid";
connectAttr "groupId30.id" "pCubeShape18.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape18.iog.og[0].gco";
connectAttr "groupId31.id" "pCubeShape18.ciog.cog[0].cgid";
connectAttr "groupId32.id" "pCubeShape19.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape19.iog.og[0].gco";
connectAttr "groupId33.id" "pCubeShape19.ciog.cog[0].cgid";
connectAttr "groupId34.id" "pCubeShape20.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape20.iog.og[0].gco";
connectAttr "groupId35.id" "pCubeShape20.ciog.cog[0].cgid";
connectAttr "groupId36.id" "pCubeShape21.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape21.iog.og[0].gco";
connectAttr "groupId37.id" "pCubeShape21.ciog.cog[0].cgid";
connectAttr "groupId38.id" "pCubeShape22.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape22.iog.og[0].gco";
connectAttr "groupId39.id" "pCubeShape22.ciog.cog[0].cgid";
connectAttr "groupId40.id" "pCubeShape23.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape23.iog.og[0].gco";
connectAttr "groupId41.id" "pCubeShape23.ciog.cog[0].cgid";
connectAttr "groupId42.id" "pCubeShape24.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape24.iog.og[0].gco";
connectAttr "groupId43.id" "pCubeShape24.ciog.cog[0].cgid";
connectAttr "groupId44.id" "pCubeShape25.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape25.iog.og[0].gco";
connectAttr "groupId45.id" "pCubeShape25.ciog.cog[0].cgid";
connectAttr "groupId46.id" "pCubeShape26.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape26.iog.og[0].gco";
connectAttr "groupId47.id" "pCubeShape26.ciog.cog[0].cgid";
connectAttr "groupId48.id" "pCubeShape27.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape27.iog.og[0].gco";
connectAttr "groupId49.id" "pCubeShape27.ciog.cog[0].cgid";
connectAttr "groupId50.id" "pCubeShape28.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape28.iog.og[0].gco";
connectAttr "groupId51.id" "pCubeShape28.ciog.cog[0].cgid";
connectAttr "groupId52.id" "pCubeShape29.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape29.iog.og[0].gco";
connectAttr "groupId53.id" "pCubeShape29.ciog.cog[0].cgid";
connectAttr "groupId54.id" "pCubeShape30.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape30.iog.og[0].gco";
connectAttr "groupId55.id" "pCubeShape30.ciog.cog[0].cgid";
connectAttr "groupId56.id" "pCubeShape31.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape31.iog.og[0].gco";
connectAttr "groupId57.id" "pCubeShape31.ciog.cog[0].cgid";
connectAttr "groupId58.id" "pCubeShape32.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape32.iog.og[0].gco";
connectAttr "groupId59.id" "pCubeShape32.ciog.cog[0].cgid";
connectAttr "groupId60.id" "pCubeShape33.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape33.iog.og[0].gco";
connectAttr "groupId61.id" "pCubeShape33.ciog.cog[0].cgid";
connectAttr "groupId62.id" "pCubeShape34.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape34.iog.og[0].gco";
connectAttr "groupId63.id" "pCubeShape34.ciog.cog[0].cgid";
connectAttr "groupId64.id" "pCubeShape35.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape35.iog.og[0].gco";
connectAttr "groupId65.id" "pCubeShape35.ciog.cog[0].cgid";
connectAttr "groupId66.id" "pCubeShape36.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape36.iog.og[0].gco";
connectAttr "groupId67.id" "pCubeShape36.ciog.cog[0].cgid";
connectAttr "groupId68.id" "pCubeShape37.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape37.iog.og[0].gco";
connectAttr "groupId69.id" "pCubeShape37.ciog.cog[0].cgid";
connectAttr "groupId70.id" "pCubeShape38.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape38.iog.og[0].gco";
connectAttr "groupId71.id" "pCubeShape38.ciog.cog[0].cgid";
connectAttr "groupId72.id" "pCubeShape39.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape39.iog.og[0].gco";
connectAttr "groupId73.id" "pCubeShape39.ciog.cog[0].cgid";
connectAttr "groupId74.id" "pCubeShape40.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape40.iog.og[0].gco";
connectAttr "groupId75.id" "pCubeShape40.ciog.cog[0].cgid";
connectAttr "groupId76.id" "pCubeShape41.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape41.iog.og[0].gco";
connectAttr "groupId77.id" "pCubeShape41.ciog.cog[0].cgid";
connectAttr "groupId78.id" "pCubeShape42.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape42.iog.og[0].gco";
connectAttr "groupId79.id" "pCubeShape42.ciog.cog[0].cgid";
connectAttr "groupId80.id" "pCubeShape43.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape43.iog.og[0].gco";
connectAttr "groupId81.id" "pCubeShape43.ciog.cog[0].cgid";
connectAttr "groupId82.id" "pCubeShape45.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape45.iog.og[0].gco";
connectAttr "groupId83.id" "pCubeShape45.ciog.cog[0].cgid";
connectAttr "groupId84.id" "pCubeShape46.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape46.iog.og[0].gco";
connectAttr "groupId85.id" "pCubeShape46.ciog.cog[0].cgid";
connectAttr "groupId86.id" "pCubeShape47.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape47.iog.og[0].gco";
connectAttr "groupId87.id" "pCubeShape47.ciog.cog[0].cgid";
connectAttr "groupId88.id" "pCubeShape48.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape48.iog.og[0].gco";
connectAttr "groupId89.id" "pCubeShape48.ciog.cog[0].cgid";
connectAttr "groupId90.id" "pCubeShape49.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape49.iog.og[0].gco";
connectAttr "groupId91.id" "pCubeShape49.ciog.cog[0].cgid";
connectAttr "groupId92.id" "pCubeShape50.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape50.iog.og[0].gco";
connectAttr "groupId93.id" "pCubeShape50.ciog.cog[0].cgid";
connectAttr "groupId94.id" "pCubeShape51.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape51.iog.og[0].gco";
connectAttr "groupId95.id" "pCubeShape51.ciog.cog[0].cgid";
connectAttr "groupId96.id" "pCubeShape52.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape52.iog.og[0].gco";
connectAttr "groupId97.id" "pCubeShape52.ciog.cog[0].cgid";
connectAttr "groupId98.id" "pCubeShape53.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape53.iog.og[0].gco";
connectAttr "groupId99.id" "pCubeShape53.ciog.cog[0].cgid";
connectAttr "groupId100.id" "pCubeShape54.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape54.iog.og[0].gco";
connectAttr "groupId101.id" "pCubeShape54.ciog.cog[0].cgid";
connectAttr "groupId102.id" "pCubeShape55.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape55.iog.og[0].gco";
connectAttr "groupId103.id" "pCubeShape55.ciog.cog[0].cgid";
connectAttr "groupId104.id" "pCubeShape56.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape56.iog.og[0].gco";
connectAttr "groupId105.id" "pCubeShape56.ciog.cog[0].cgid";
connectAttr "groupId106.id" "pCubeShape57.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape57.iog.og[0].gco";
connectAttr "groupId107.id" "pCubeShape57.ciog.cog[0].cgid";
connectAttr "groupId108.id" "pCubeShape58.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape58.iog.og[0].gco";
connectAttr "groupId109.id" "pCubeShape58.ciog.cog[0].cgid";
connectAttr "groupId110.id" "pCubeShape59.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape59.iog.og[0].gco";
connectAttr "groupId111.id" "pCubeShape59.ciog.cog[0].cgid";
connectAttr "groupId112.id" "pCubeShape60.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape60.iog.og[0].gco";
connectAttr "groupId113.id" "pCubeShape60.ciog.cog[0].cgid";
connectAttr "groupId114.id" "pCubeShape61.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape61.iog.og[0].gco";
connectAttr "groupId115.id" "pCubeShape61.ciog.cog[0].cgid";
connectAttr "groupId116.id" "pCubeShape62.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape62.iog.og[0].gco";
connectAttr "groupId117.id" "pCubeShape62.ciog.cog[0].cgid";
connectAttr "groupId118.id" "pCubeShape63.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape63.iog.og[0].gco";
connectAttr "groupId119.id" "pCubeShape63.ciog.cog[0].cgid";
connectAttr "groupId120.id" "pCubeShape64.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape64.iog.og[0].gco";
connectAttr "groupId121.id" "pCubeShape64.ciog.cog[0].cgid";
connectAttr "groupId122.id" "pCubeShape65.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape65.iog.og[0].gco";
connectAttr "groupId123.id" "pCubeShape65.ciog.cog[0].cgid";
connectAttr "groupId124.id" "pCubeShape66.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape66.iog.og[0].gco";
connectAttr "groupId125.id" "pCubeShape66.ciog.cog[0].cgid";
connectAttr "groupId126.id" "pCubeShape67.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape67.iog.og[0].gco";
connectAttr "groupId127.id" "pCubeShape67.ciog.cog[0].cgid";
connectAttr "groupId128.id" "pCubeShape68.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape68.iog.og[0].gco";
connectAttr "groupId129.id" "pCubeShape68.ciog.cog[0].cgid";
connectAttr "groupId130.id" "pCubeShape69.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape69.iog.og[0].gco";
connectAttr "groupId131.id" "pCubeShape69.ciog.cog[0].cgid";
connectAttr "groupId132.id" "pCubeShape70.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape70.iog.og[0].gco";
connectAttr "groupId133.id" "pCubeShape70.ciog.cog[0].cgid";
connectAttr "groupId134.id" "pCubeShape71.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape71.iog.og[0].gco";
connectAttr "groupId135.id" "pCubeShape71.ciog.cog[0].cgid";
connectAttr "groupId136.id" "pCubeShape72.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape72.iog.og[0].gco";
connectAttr "groupId137.id" "pCubeShape72.ciog.cog[0].cgid";
connectAttr "groupId138.id" "pCubeShape73.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape73.iog.og[0].gco";
connectAttr "groupId139.id" "pCubeShape73.ciog.cog[0].cgid";
connectAttr "groupId140.id" "pCubeShape74.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape74.iog.og[0].gco";
connectAttr "groupId141.id" "pCubeShape74.ciog.cog[0].cgid";
connectAttr "groupId142.id" "pCubeShape75.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape75.iog.og[0].gco";
connectAttr "groupId143.id" "pCubeShape75.ciog.cog[0].cgid";
connectAttr "groupId144.id" "pCubeShape76.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape76.iog.og[0].gco";
connectAttr "groupId145.id" "pCubeShape76.ciog.cog[0].cgid";
connectAttr "groupId146.id" "pCubeShape77.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape77.iog.og[0].gco";
connectAttr "groupId147.id" "pCubeShape77.ciog.cog[0].cgid";
connectAttr "groupId148.id" "pCubeShape78.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape78.iog.og[0].gco";
connectAttr "groupId149.id" "pCubeShape78.ciog.cog[0].cgid";
connectAttr "groupId150.id" "pCubeShape79.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape79.iog.og[0].gco";
connectAttr "groupId151.id" "pCubeShape79.ciog.cog[0].cgid";
connectAttr "groupId152.id" "pCubeShape81.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape81.iog.og[0].gco";
connectAttr "groupId153.id" "pCubeShape81.ciog.cog[0].cgid";
connectAttr "groupId154.id" "pCubeShape82.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape82.iog.og[0].gco";
connectAttr "groupId155.id" "pCubeShape82.ciog.cog[0].cgid";
connectAttr "groupId156.id" "pCubeShape83.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape83.iog.og[0].gco";
connectAttr "groupId157.id" "pCubeShape83.ciog.cog[0].cgid";
connectAttr "groupId158.id" "pCubeShape84.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape84.iog.og[0].gco";
connectAttr "groupId159.id" "pCubeShape84.ciog.cog[0].cgid";
connectAttr "groupId160.id" "pCubeShape85.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape85.iog.og[0].gco";
connectAttr "groupId161.id" "pCubeShape85.ciog.cog[0].cgid";
connectAttr "groupId162.id" "pCubeShape86.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape86.iog.og[0].gco";
connectAttr "groupId163.id" "pCubeShape86.ciog.cog[0].cgid";
connectAttr "groupId164.id" "pCubeShape87.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape87.iog.og[0].gco";
connectAttr "groupId165.id" "pCubeShape87.ciog.cog[0].cgid";
connectAttr "groupId166.id" "pCubeShape88.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape88.iog.og[0].gco";
connectAttr "groupId167.id" "pCubeShape88.ciog.cog[0].cgid";
connectAttr "groupId168.id" "pCubeShape90.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape90.iog.og[0].gco";
connectAttr "groupId169.id" "pCubeShape90.ciog.cog[0].cgid";
connectAttr "groupId170.id" "pCubeShape91.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape91.iog.og[0].gco";
connectAttr "groupId171.id" "pCubeShape91.ciog.cog[0].cgid";
connectAttr "groupId172.id" "pCubeShape92.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape92.iog.og[0].gco";
connectAttr "groupId173.id" "pCubeShape92.ciog.cog[0].cgid";
connectAttr "groupId174.id" "pCubeShape93.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape93.iog.og[0].gco";
connectAttr "groupId175.id" "pCubeShape93.ciog.cog[0].cgid";
connectAttr "groupId176.id" "pCubeShape94.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape94.iog.og[0].gco";
connectAttr "groupId177.id" "pCubeShape94.ciog.cog[0].cgid";
connectAttr "groupId178.id" "pCubeShape95.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape95.iog.og[0].gco";
connectAttr "groupId179.id" "pCubeShape95.ciog.cog[0].cgid";
connectAttr "groupId180.id" "pCubeShape96.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape96.iog.og[0].gco";
connectAttr "groupId181.id" "pCubeShape96.ciog.cog[0].cgid";
connectAttr "groupId182.id" "pCubeShape97.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape97.iog.og[0].gco";
connectAttr "groupId183.id" "pCubeShape97.ciog.cog[0].cgid";
connectAttr "groupId184.id" "pCubeShape99.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape99.iog.og[0].gco";
connectAttr "groupId185.id" "pCubeShape99.ciog.cog[0].cgid";
connectAttr "groupId186.id" "pCubeShape100.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape100.iog.og[0].gco";
connectAttr "groupId187.id" "pCubeShape100.ciog.cog[0].cgid";
connectAttr "groupId188.id" "pCubeShape101.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape101.iog.og[0].gco";
connectAttr "groupId189.id" "pCubeShape101.ciog.cog[0].cgid";
connectAttr "groupId190.id" "pCubeShape102.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape102.iog.og[0].gco";
connectAttr "groupId191.id" "pCubeShape102.ciog.cog[0].cgid";
connectAttr "groupId192.id" "pCubeShape103.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape103.iog.og[0].gco";
connectAttr "groupId193.id" "pCubeShape103.ciog.cog[0].cgid";
connectAttr "groupId194.id" "pCubeShape104.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape104.iog.og[0].gco";
connectAttr "groupId195.id" "pCubeShape104.ciog.cog[0].cgid";
connectAttr "groupId196.id" "pCubeShape105.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape105.iog.og[0].gco";
connectAttr "groupId197.id" "pCubeShape105.ciog.cog[0].cgid";
connectAttr "groupId198.id" "pCubeShape106.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape106.iog.og[0].gco";
connectAttr "groupId199.id" "pCubeShape106.ciog.cog[0].cgid";
connectAttr "groupId200.id" "pCubeShape107.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape107.iog.og[0].gco";
connectAttr "groupId201.id" "pCubeShape107.ciog.cog[0].cgid";
connectAttr "groupId202.id" "pCubeShape108.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape108.iog.og[0].gco";
connectAttr "groupId203.id" "pCubeShape108.ciog.cog[0].cgid";
connectAttr "groupId204.id" "pCubeShape109.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape109.iog.og[0].gco";
connectAttr "groupId205.id" "pCubeShape109.ciog.cog[0].cgid";
connectAttr "groupId206.id" "pCubeShape110.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape110.iog.og[0].gco";
connectAttr "groupId207.id" "pCubeShape110.ciog.cog[0].cgid";
connectAttr "groupId208.id" "pCubeShape111.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape111.iog.og[0].gco";
connectAttr "groupId209.id" "pCubeShape111.ciog.cog[0].cgid";
connectAttr "groupId210.id" "pCubeShape112.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape112.iog.og[0].gco";
connectAttr "groupId211.id" "pCubeShape112.ciog.cog[0].cgid";
connectAttr "groupId212.id" "pCubeShape113.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape113.iog.og[0].gco";
connectAttr "groupId213.id" "pCubeShape113.ciog.cog[0].cgid";
connectAttr "groupId214.id" "pCubeShape114.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape114.iog.og[0].gco";
connectAttr "groupId215.id" "pCubeShape114.ciog.cog[0].cgid";
connectAttr "groupId216.id" "pCubeShape115.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape115.iog.og[0].gco";
connectAttr "groupId217.id" "pCubeShape115.ciog.cog[0].cgid";
connectAttr "groupId218.id" "pCubeShape118.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape118.iog.og[0].gco";
connectAttr "groupId219.id" "pCubeShape118.ciog.cog[0].cgid";
connectAttr "groupId220.id" "pCubeShape119.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape119.iog.og[0].gco";
connectAttr "groupId221.id" "pCubeShape119.ciog.cog[0].cgid";
connectAttr "groupId222.id" "pCubeShape120.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape120.iog.og[0].gco";
connectAttr "groupId223.id" "pCubeShape120.ciog.cog[0].cgid";
connectAttr "groupId224.id" "pCubeShape121.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape121.iog.og[0].gco";
connectAttr "groupId225.id" "pCubeShape121.ciog.cog[0].cgid";
connectAttr "groupId226.id" "pCubeShape122.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape122.iog.og[0].gco";
connectAttr "groupId227.id" "pCubeShape122.ciog.cog[0].cgid";
connectAttr "groupId228.id" "pCubeShape123.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape123.iog.og[0].gco";
connectAttr "groupId229.id" "pCubeShape123.ciog.cog[0].cgid";
connectAttr "groupId230.id" "pCubeShape124.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape124.iog.og[0].gco";
connectAttr "groupId231.id" "pCubeShape124.ciog.cog[0].cgid";
connectAttr "groupId232.id" "pCubeShape127.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape127.iog.og[0].gco";
connectAttr "groupId233.id" "pCubeShape127.ciog.cog[0].cgid";
connectAttr "groupId234.id" "pCubeShape128.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape128.iog.og[0].gco";
connectAttr "groupId235.id" "pCubeShape128.ciog.cog[0].cgid";
connectAttr "groupId236.id" "pCubeShape129.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape129.iog.og[0].gco";
connectAttr "groupId237.id" "pCubeShape129.ciog.cog[0].cgid";
connectAttr "groupId238.id" "pCubeShape130.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape130.iog.og[0].gco";
connectAttr "groupId239.id" "pCubeShape130.ciog.cog[0].cgid";
connectAttr "groupId240.id" "pCubeShape131.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape131.iog.og[0].gco";
connectAttr "groupId241.id" "pCubeShape131.ciog.cog[0].cgid";
connectAttr "groupId242.id" "pCubeShape132.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape132.iog.og[0].gco";
connectAttr "groupId243.id" "pCubeShape132.ciog.cog[0].cgid";
connectAttr "groupId244.id" "pCubeShape133.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape133.iog.og[0].gco";
connectAttr "groupId245.id" "pCubeShape133.ciog.cog[0].cgid";
connectAttr "groupId246.id" "pCubeShape136.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape136.iog.og[0].gco";
connectAttr "groupId247.id" "pCubeShape136.ciog.cog[0].cgid";
connectAttr "groupId248.id" "pCubeShape137.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape137.iog.og[0].gco";
connectAttr "groupId249.id" "pCubeShape137.ciog.cog[0].cgid";
connectAttr "groupId250.id" "pCubeShape138.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape138.iog.og[0].gco";
connectAttr "groupId251.id" "pCubeShape138.ciog.cog[0].cgid";
connectAttr "groupId252.id" "pCubeShape139.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape139.iog.og[0].gco";
connectAttr "groupId253.id" "pCubeShape139.ciog.cog[0].cgid";
connectAttr "groupId254.id" "pCubeShape140.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape140.iog.og[0].gco";
connectAttr "groupId255.id" "pCubeShape140.ciog.cog[0].cgid";
connectAttr "groupId256.id" "pCubeShape141.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape141.iog.og[0].gco";
connectAttr "groupId257.id" "pCubeShape141.ciog.cog[0].cgid";
connectAttr "groupId258.id" "pCubeShape142.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape142.iog.og[0].gco";
connectAttr "groupId259.id" "pCubeShape142.ciog.cog[0].cgid";
connectAttr "groupId260.id" "pCubeShape143.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape143.iog.og[0].gco";
connectAttr "groupId261.id" "pCubeShape143.ciog.cog[0].cgid";
connectAttr "groupId262.id" "pCubeShape144.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape144.iog.og[0].gco";
connectAttr "groupId263.id" "pCubeShape144.ciog.cog[0].cgid";
connectAttr "groupId264.id" "pCubeShape145.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape145.iog.og[0].gco";
connectAttr "groupId265.id" "pCubeShape145.ciog.cog[0].cgid";
connectAttr "groupId266.id" "pCubeShape146.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape146.iog.og[0].gco";
connectAttr "groupId267.id" "pCubeShape146.ciog.cog[0].cgid";
connectAttr "groupId268.id" "pCubeShape147.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape147.iog.og[0].gco";
connectAttr "groupId269.id" "pCubeShape147.ciog.cog[0].cgid";
connectAttr "groupId270.id" "pCubeShape148.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape148.iog.og[0].gco";
connectAttr "groupId271.id" "pCubeShape148.ciog.cog[0].cgid";
connectAttr "groupId272.id" "pCubeShape149.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape149.iog.og[0].gco";
connectAttr "groupId273.id" "pCubeShape149.ciog.cog[0].cgid";
connectAttr "groupId274.id" "pCubeShape150.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape150.iog.og[0].gco";
connectAttr "groupId275.id" "pCubeShape150.ciog.cog[0].cgid";
connectAttr "groupId276.id" "pCubeShape151.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape151.iog.og[0].gco";
connectAttr "groupId277.id" "pCubeShape151.ciog.cog[0].cgid";
connectAttr "groupId278.id" "pCubeShape152.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape152.iog.og[0].gco";
connectAttr "groupId279.id" "pCubeShape152.ciog.cog[0].cgid";
connectAttr "groupId280.id" "pCubeShape153.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape153.iog.og[0].gco";
connectAttr "groupId281.id" "pCubeShape153.ciog.cog[0].cgid";
connectAttr "groupId282.id" "pCubeShape154.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape154.iog.og[0].gco";
connectAttr "groupId283.id" "pCubeShape154.ciog.cog[0].cgid";
connectAttr "groupId284.id" "pCubeShape155.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape155.iog.og[0].gco";
connectAttr "groupId285.id" "pCubeShape155.ciog.cog[0].cgid";
connectAttr "groupId286.id" "pCubeShape156.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape156.iog.og[0].gco";
connectAttr "groupId287.id" "pCubeShape156.ciog.cog[0].cgid";
connectAttr "groupId288.id" "pCubeShape157.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape157.iog.og[0].gco";
connectAttr "groupId289.id" "pCubeShape157.ciog.cog[0].cgid";
connectAttr "groupId290.id" "pCubeShape158.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape158.iog.og[0].gco";
connectAttr "groupId291.id" "pCubeShape158.ciog.cog[0].cgid";
connectAttr "groupId292.id" "pCubeShape159.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape159.iog.og[0].gco";
connectAttr "groupId293.id" "pCubeShape159.ciog.cog[0].cgid";
connectAttr "groupId294.id" "pCubeShape160.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape160.iog.og[0].gco";
connectAttr "groupId295.id" "pCubeShape160.ciog.cog[0].cgid";
connectAttr "groupId296.id" "pCubeShape161.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape161.iog.og[0].gco";
connectAttr "groupId297.id" "pCubeShape161.ciog.cog[0].cgid";
connectAttr "groupId298.id" "pCubeShape162.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape162.iog.og[0].gco";
connectAttr "groupId299.id" "pCubeShape162.ciog.cog[0].cgid";
connectAttr "groupId300.id" "pCubeShape163.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape163.iog.og[0].gco";
connectAttr "groupId301.id" "pCubeShape163.ciog.cog[0].cgid";
connectAttr "groupId302.id" "pCubeShape164.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape164.iog.og[0].gco";
connectAttr "groupId303.id" "pCubeShape164.ciog.cog[0].cgid";
connectAttr "groupId304.id" "pCubeShape165.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape165.iog.og[0].gco";
connectAttr "groupId305.id" "pCubeShape165.ciog.cog[0].cgid";
connectAttr "groupId306.id" "pCubeShape166.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape166.iog.og[0].gco";
connectAttr "groupId307.id" "pCubeShape166.ciog.cog[0].cgid";
connectAttr "groupId308.id" "pCubeShape167.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape167.iog.og[0].gco";
connectAttr "groupId309.id" "pCubeShape167.ciog.cog[0].cgid";
connectAttr "groupId310.id" "pCubeShape168.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape168.iog.og[0].gco";
connectAttr "groupId311.id" "pCubeShape168.ciog.cog[0].cgid";
connectAttr "groupId312.id" "pCubeShape169.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape169.iog.og[0].gco";
connectAttr "groupId313.id" "pCubeShape169.ciog.cog[0].cgid";
connectAttr "groupId314.id" "pCubeShape170.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape170.iog.og[0].gco";
connectAttr "groupId315.id" "pCubeShape170.ciog.cog[0].cgid";
connectAttr "groupId316.id" "pCubeShape171.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape171.iog.og[0].gco";
connectAttr "groupId317.id" "pCubeShape171.ciog.cog[0].cgid";
connectAttr "groupId318.id" "pCubeShape172.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape172.iog.og[0].gco";
connectAttr "groupId319.id" "pCubeShape172.ciog.cog[0].cgid";
connectAttr "groupId320.id" "pCubeShape173.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape173.iog.og[0].gco";
connectAttr "groupId321.id" "pCubeShape173.ciog.cog[0].cgid";
connectAttr "groupId322.id" "pCubeShape174.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape174.iog.og[0].gco";
connectAttr "groupId323.id" "pCubeShape174.ciog.cog[0].cgid";
connectAttr "groupId324.id" "pCubeShape175.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape175.iog.og[0].gco";
connectAttr "groupId325.id" "pCubeShape175.ciog.cog[0].cgid";
connectAttr "groupId326.id" "pCubeShape176.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape176.iog.og[0].gco";
connectAttr "groupId327.id" "pCubeShape176.ciog.cog[0].cgid";
connectAttr "groupId328.id" "pCubeShape177.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape177.iog.og[0].gco";
connectAttr "groupId329.id" "pCubeShape177.ciog.cog[0].cgid";
connectAttr "groupId330.id" "pCubeShape178.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape178.iog.og[0].gco";
connectAttr "groupId331.id" "pCubeShape178.ciog.cog[0].cgid";
connectAttr "groupId332.id" "pCubeShape179.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape179.iog.og[0].gco";
connectAttr "groupId333.id" "pCubeShape179.ciog.cog[0].cgid";
connectAttr "groupId334.id" "pCubeShape180.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape180.iog.og[0].gco";
connectAttr "groupId335.id" "pCubeShape180.ciog.cog[0].cgid";
connectAttr "groupId336.id" "pCubeShape181.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape181.iog.og[0].gco";
connectAttr "groupId337.id" "pCubeShape181.ciog.cog[0].cgid";
connectAttr "groupId338.id" "pCubeShape182.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape182.iog.og[0].gco";
connectAttr "groupId339.id" "pCubeShape182.ciog.cog[0].cgid";
connectAttr "groupId340.id" "pCubeShape183.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape183.iog.og[0].gco";
connectAttr "groupId341.id" "pCubeShape183.ciog.cog[0].cgid";
connectAttr "groupId342.id" "pCubeShape184.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape184.iog.og[0].gco";
connectAttr "groupId343.id" "pCubeShape184.ciog.cog[0].cgid";
connectAttr "groupId344.id" "pCubeShape185.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape185.iog.og[0].gco";
connectAttr "groupId345.id" "pCubeShape185.ciog.cog[0].cgid";
connectAttr "groupId346.id" "pCubeShape186.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape186.iog.og[0].gco";
connectAttr "groupId347.id" "pCubeShape186.ciog.cog[0].cgid";
connectAttr "groupId348.id" "pCubeShape187.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape187.iog.og[0].gco";
connectAttr "groupId349.id" "pCubeShape187.ciog.cog[0].cgid";
connectAttr "groupId350.id" "pCubeShape188.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape188.iog.og[0].gco";
connectAttr "groupId351.id" "pCubeShape188.ciog.cog[0].cgid";
connectAttr "groupId352.id" "pCubeShape189.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape189.iog.og[0].gco";
connectAttr "groupId353.id" "pCubeShape189.ciog.cog[0].cgid";
connectAttr "groupId354.id" "pCubeShape190.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape190.iog.og[0].gco";
connectAttr "groupId355.id" "pCubeShape190.ciog.cog[0].cgid";
connectAttr "groupId356.id" "pCubeShape191.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape191.iog.og[0].gco";
connectAttr "groupId357.id" "pCubeShape191.ciog.cog[0].cgid";
connectAttr "groupId358.id" "pCubeShape192.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape192.iog.og[0].gco";
connectAttr "groupId359.id" "pCubeShape192.ciog.cog[0].cgid";
connectAttr "groupId360.id" "pCubeShape193.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape193.iog.og[0].gco";
connectAttr "groupId361.id" "pCubeShape193.ciog.cog[0].cgid";
connectAttr "groupId362.id" "pCubeShape194.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCubeShape194.iog.og[0].gco";
connectAttr "groupId363.id" "pCubeShape194.ciog.cog[0].cgid";
connectAttr "groupId1.id" "pCubeShape195.iog.og[0].gid";
connectAttr "lambert4SG.mwc" "pCubeShape195.iog.og[0].gco";
connectAttr "groupId3.id" "pCubeShape195.iog.og[1].gid";
connectAttr "lambert7SG.mwc" "pCubeShape195.iog.og[1].gco";
connectAttr "groupParts2.og" "pCubeShape195.i";
connectAttr "groupId2.id" "pCubeShape195.ciog.cog[0].cgid";
connectAttr "groupId364.id" "pCubeShape196.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape196.iog.og[0].gco";
connectAttr "groupId365.id" "pCubeShape196.ciog.cog[0].cgid";
connectAttr "groupId366.id" "pCubeShape197.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape197.iog.og[0].gco";
connectAttr "groupId367.id" "pCubeShape197.ciog.cog[0].cgid";
connectAttr "groupId368.id" "pCubeShape198.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape198.iog.og[0].gco";
connectAttr "groupId369.id" "pCubeShape198.ciog.cog[0].cgid";
connectAttr "groupId370.id" "pCubeShape199.iog.og[0].gid";
connectAttr "lambert3SG.mwc" "pCubeShape199.iog.og[0].gco";
connectAttr "groupId371.id" "pCubeShape199.ciog.cog[0].cgid";
connectAttr "groupId372.id" "pCubeShape200.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape200.iog.og[0].gco";
connectAttr "groupParts5.og" "pCubeShape200.i";
connectAttr "groupId373.id" "pCubeShape200.ciog.cog[0].cgid";
connectAttr "groupId374.id" "pCubeShape201.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape201.iog.og[0].gco";
connectAttr "groupId375.id" "pCubeShape201.ciog.cog[0].cgid";
connectAttr "groupId376.id" "pCubeShape202.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape202.iog.og[0].gco";
connectAttr "groupId377.id" "pCubeShape202.ciog.cog[0].cgid";
connectAttr "groupId378.id" "pCubeShape203.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape203.iog.og[0].gco";
connectAttr "groupId379.id" "pCubeShape203.ciog.cog[0].cgid";
connectAttr "groupId380.id" "pCubeShape204.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape204.iog.og[0].gco";
connectAttr "groupId381.id" "pCubeShape204.ciog.cog[0].cgid";
connectAttr "groupId382.id" "pCubeShape205.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape205.iog.og[0].gco";
connectAttr "groupId383.id" "pCubeShape205.ciog.cog[0].cgid";
connectAttr "groupId384.id" "pCubeShape206.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape206.iog.og[0].gco";
connectAttr "groupId385.id" "pCubeShape206.ciog.cog[0].cgid";
connectAttr "groupId386.id" "pCubeShape207.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape207.iog.og[0].gco";
connectAttr "groupId387.id" "pCubeShape207.ciog.cog[0].cgid";
connectAttr "groupId388.id" "pCubeShape208.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape208.iog.og[0].gco";
connectAttr "groupParts6.og" "pCubeShape208.i";
connectAttr "groupId389.id" "pCubeShape208.ciog.cog[0].cgid";
connectAttr "groupId390.id" "pCubeShape209.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "pCubeShape209.iog.og[0].gco";
connectAttr "groupId391.id" "pCubeShape209.ciog.cog[0].cgid";
connectAttr "groupId392.id" "pCubeShape210.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape210.iog.og[0].gco";
connectAttr "groupId393.id" "pCubeShape210.ciog.cog[0].cgid";
connectAttr "groupId394.id" "pCubeShape211.iog.og[0].gid";
connectAttr "lambert6SG.mwc" "pCubeShape211.iog.og[0].gco";
connectAttr "groupId395.id" "pCubeShape211.ciog.cog[0].cgid";
connectAttr "groupId396.id" "pCubeShape212.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape212.iog.og[0].gco";
connectAttr "groupId397.id" "pCubeShape212.ciog.cog[0].cgid";
connectAttr "groupId398.id" "pCubeShape213.iog.og[0].gid";
connectAttr "lambert5SG.mwc" "pCubeShape213.iog.og[0].gco";
connectAttr "groupId399.id" "pCubeShape213.ciog.cog[0].cgid";
connectAttr "groupParts12.og" "pCube214Shape.i";
connectAttr "groupId400.id" "pCube214Shape.iog.og[0].gid";
connectAttr "lambert2SG.mwc" "pCube214Shape.iog.og[0].gco";
connectAttr "groupId401.id" "pCube214Shape.iog.og[1].gid";
connectAttr "lambert3SG.mwc" "pCube214Shape.iog.og[1].gco";
connectAttr "groupId402.id" "pCube214Shape.iog.og[2].gid";
connectAttr "lambert4SG.mwc" "pCube214Shape.iog.og[2].gco";
connectAttr "groupId403.id" "pCube214Shape.iog.og[3].gid";
connectAttr "lambert7SG.mwc" "pCube214Shape.iog.og[3].gco";
connectAttr "groupId404.id" "pCube214Shape.iog.og[4].gid";
connectAttr "lambert5SG.mwc" "pCube214Shape.iog.og[4].gco";
connectAttr "groupId405.id" "pCube214Shape.iog.og[5].gid";
connectAttr "lambert6SG.mwc" "pCube214Shape.iog.og[5].gco";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert5SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert6SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert7SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "polyCube3.out" "polySubdFace1.ip";
connectAttr "west_house_body.oc" "lambert2SG.ss";
connectAttr "pCubeShape1.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape1.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape8.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape8.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape9.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape9.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape10.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape10.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape11.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape11.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape12.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape12.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape13.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape13.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape14.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape14.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape15.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape15.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape16.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape16.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape18.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape18.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape19.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape19.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape20.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape20.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape21.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape21.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape22.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape22.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape23.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape23.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape24.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape24.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape25.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape25.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape26.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape26.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape27.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape27.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape28.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape28.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape29.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape29.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape30.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape30.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape31.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape31.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape32.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape32.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape33.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape33.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape34.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape34.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape35.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape35.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape36.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape36.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape37.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape37.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape38.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape38.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape39.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape39.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape40.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape40.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape41.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape41.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape42.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape42.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape43.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape43.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape45.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape45.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape46.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape46.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape47.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape47.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape48.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape48.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape49.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape49.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape50.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape50.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape51.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape51.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape52.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape52.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape53.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape53.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape54.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape54.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape55.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape55.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape56.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape56.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape57.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape57.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape58.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape58.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape59.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape59.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape60.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape60.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape61.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape61.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape62.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape62.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape63.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape63.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape64.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape64.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape65.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape65.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape66.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape66.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape67.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape67.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape68.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape68.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape69.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape69.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape70.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape70.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape71.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape71.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape72.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape72.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape73.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape73.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape74.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape74.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape75.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape75.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape76.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape76.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape77.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape77.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape78.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape78.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape79.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape79.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape81.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape81.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape82.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape82.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape83.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape83.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape84.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape84.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape85.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape85.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape86.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape86.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape87.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape87.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape88.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape88.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape90.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape90.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape91.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape91.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape92.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape92.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape93.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape93.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape94.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape94.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape95.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape95.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape96.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape96.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape97.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape97.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape99.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape99.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape100.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape100.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape101.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape101.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape102.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape102.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape103.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape103.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape104.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape104.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape105.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape105.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape106.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape106.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape107.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape107.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape108.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape108.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape109.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape109.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape110.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape110.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape111.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape111.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape112.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape112.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape113.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape113.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape114.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape114.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape115.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape115.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape118.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape118.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape119.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape119.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape120.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape120.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape121.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape121.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape122.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape122.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape123.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape123.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape124.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape124.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape127.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape127.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape128.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape128.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape129.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape129.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape130.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape130.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape131.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape131.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape132.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape132.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape133.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape133.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape136.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape136.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape137.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape137.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape138.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape138.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape139.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape139.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape140.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape140.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape141.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape141.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape142.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape142.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape143.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape143.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape144.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape144.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape145.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape145.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape146.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape146.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape147.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape147.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape148.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape148.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape149.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape149.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape150.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape150.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape151.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape151.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape156.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape156.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape157.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape157.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape158.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape158.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape159.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape159.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape160.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape160.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape161.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape161.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape162.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape162.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape163.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape163.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape164.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape164.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape165.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape165.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape167.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape167.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape168.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape168.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape169.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape169.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape170.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape170.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape171.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape171.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape174.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape174.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape175.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape175.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape176.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape176.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape177.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape177.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape179.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape179.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape180.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape180.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape181.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape181.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape182.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape182.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape183.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape183.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape184.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape184.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape185.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape185.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape186.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape186.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape187.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape187.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape188.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape188.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape189.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape189.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape190.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape190.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape191.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape191.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape192.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape192.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape193.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape193.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape194.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "pCubeShape194.ciog.cog[0]" "lambert2SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[0]" "lambert2SG.dsm" -na;
connectAttr "groupId3.msg" "lambert2SG.gn" -na;
connectAttr "groupId4.msg" "lambert2SG.gn" -na;
connectAttr "groupId5.msg" "lambert2SG.gn" -na;
connectAttr "groupId12.msg" "lambert2SG.gn" -na;
connectAttr "groupId13.msg" "lambert2SG.gn" -na;
connectAttr "groupId14.msg" "lambert2SG.gn" -na;
connectAttr "groupId15.msg" "lambert2SG.gn" -na;
connectAttr "groupId16.msg" "lambert2SG.gn" -na;
connectAttr "groupId17.msg" "lambert2SG.gn" -na;
connectAttr "groupId18.msg" "lambert2SG.gn" -na;
connectAttr "groupId19.msg" "lambert2SG.gn" -na;
connectAttr "groupId20.msg" "lambert2SG.gn" -na;
connectAttr "groupId21.msg" "lambert2SG.gn" -na;
connectAttr "groupId22.msg" "lambert2SG.gn" -na;
connectAttr "groupId23.msg" "lambert2SG.gn" -na;
connectAttr "groupId24.msg" "lambert2SG.gn" -na;
connectAttr "groupId25.msg" "lambert2SG.gn" -na;
connectAttr "groupId26.msg" "lambert2SG.gn" -na;
connectAttr "groupId27.msg" "lambert2SG.gn" -na;
connectAttr "groupId28.msg" "lambert2SG.gn" -na;
connectAttr "groupId29.msg" "lambert2SG.gn" -na;
connectAttr "groupId30.msg" "lambert2SG.gn" -na;
connectAttr "groupId31.msg" "lambert2SG.gn" -na;
connectAttr "groupId32.msg" "lambert2SG.gn" -na;
connectAttr "groupId33.msg" "lambert2SG.gn" -na;
connectAttr "groupId34.msg" "lambert2SG.gn" -na;
connectAttr "groupId35.msg" "lambert2SG.gn" -na;
connectAttr "groupId36.msg" "lambert2SG.gn" -na;
connectAttr "groupId37.msg" "lambert2SG.gn" -na;
connectAttr "groupId38.msg" "lambert2SG.gn" -na;
connectAttr "groupId39.msg" "lambert2SG.gn" -na;
connectAttr "groupId40.msg" "lambert2SG.gn" -na;
connectAttr "groupId41.msg" "lambert2SG.gn" -na;
connectAttr "groupId42.msg" "lambert2SG.gn" -na;
connectAttr "groupId43.msg" "lambert2SG.gn" -na;
connectAttr "groupId44.msg" "lambert2SG.gn" -na;
connectAttr "groupId45.msg" "lambert2SG.gn" -na;
connectAttr "groupId46.msg" "lambert2SG.gn" -na;
connectAttr "groupId47.msg" "lambert2SG.gn" -na;
connectAttr "groupId48.msg" "lambert2SG.gn" -na;
connectAttr "groupId49.msg" "lambert2SG.gn" -na;
connectAttr "groupId50.msg" "lambert2SG.gn" -na;
connectAttr "groupId51.msg" "lambert2SG.gn" -na;
connectAttr "groupId52.msg" "lambert2SG.gn" -na;
connectAttr "groupId53.msg" "lambert2SG.gn" -na;
connectAttr "groupId54.msg" "lambert2SG.gn" -na;
connectAttr "groupId55.msg" "lambert2SG.gn" -na;
connectAttr "groupId56.msg" "lambert2SG.gn" -na;
connectAttr "groupId57.msg" "lambert2SG.gn" -na;
connectAttr "groupId58.msg" "lambert2SG.gn" -na;
connectAttr "groupId59.msg" "lambert2SG.gn" -na;
connectAttr "groupId60.msg" "lambert2SG.gn" -na;
connectAttr "groupId61.msg" "lambert2SG.gn" -na;
connectAttr "groupId62.msg" "lambert2SG.gn" -na;
connectAttr "groupId63.msg" "lambert2SG.gn" -na;
connectAttr "groupId64.msg" "lambert2SG.gn" -na;
connectAttr "groupId65.msg" "lambert2SG.gn" -na;
connectAttr "groupId66.msg" "lambert2SG.gn" -na;
connectAttr "groupId67.msg" "lambert2SG.gn" -na;
connectAttr "groupId68.msg" "lambert2SG.gn" -na;
connectAttr "groupId69.msg" "lambert2SG.gn" -na;
connectAttr "groupId70.msg" "lambert2SG.gn" -na;
connectAttr "groupId71.msg" "lambert2SG.gn" -na;
connectAttr "groupId72.msg" "lambert2SG.gn" -na;
connectAttr "groupId73.msg" "lambert2SG.gn" -na;
connectAttr "groupId74.msg" "lambert2SG.gn" -na;
connectAttr "groupId75.msg" "lambert2SG.gn" -na;
connectAttr "groupId76.msg" "lambert2SG.gn" -na;
connectAttr "groupId77.msg" "lambert2SG.gn" -na;
connectAttr "groupId78.msg" "lambert2SG.gn" -na;
connectAttr "groupId79.msg" "lambert2SG.gn" -na;
connectAttr "groupId80.msg" "lambert2SG.gn" -na;
connectAttr "groupId81.msg" "lambert2SG.gn" -na;
connectAttr "groupId82.msg" "lambert2SG.gn" -na;
connectAttr "groupId83.msg" "lambert2SG.gn" -na;
connectAttr "groupId84.msg" "lambert2SG.gn" -na;
connectAttr "groupId85.msg" "lambert2SG.gn" -na;
connectAttr "groupId86.msg" "lambert2SG.gn" -na;
connectAttr "groupId87.msg" "lambert2SG.gn" -na;
connectAttr "groupId88.msg" "lambert2SG.gn" -na;
connectAttr "groupId89.msg" "lambert2SG.gn" -na;
connectAttr "groupId90.msg" "lambert2SG.gn" -na;
connectAttr "groupId91.msg" "lambert2SG.gn" -na;
connectAttr "groupId92.msg" "lambert2SG.gn" -na;
connectAttr "groupId93.msg" "lambert2SG.gn" -na;
connectAttr "groupId94.msg" "lambert2SG.gn" -na;
connectAttr "groupId95.msg" "lambert2SG.gn" -na;
connectAttr "groupId96.msg" "lambert2SG.gn" -na;
connectAttr "groupId97.msg" "lambert2SG.gn" -na;
connectAttr "groupId98.msg" "lambert2SG.gn" -na;
connectAttr "groupId99.msg" "lambert2SG.gn" -na;
connectAttr "groupId100.msg" "lambert2SG.gn" -na;
connectAttr "groupId101.msg" "lambert2SG.gn" -na;
connectAttr "groupId102.msg" "lambert2SG.gn" -na;
connectAttr "groupId103.msg" "lambert2SG.gn" -na;
connectAttr "groupId104.msg" "lambert2SG.gn" -na;
connectAttr "groupId105.msg" "lambert2SG.gn" -na;
connectAttr "groupId106.msg" "lambert2SG.gn" -na;
connectAttr "groupId107.msg" "lambert2SG.gn" -na;
connectAttr "groupId108.msg" "lambert2SG.gn" -na;
connectAttr "groupId109.msg" "lambert2SG.gn" -na;
connectAttr "groupId110.msg" "lambert2SG.gn" -na;
connectAttr "groupId111.msg" "lambert2SG.gn" -na;
connectAttr "groupId112.msg" "lambert2SG.gn" -na;
connectAttr "groupId113.msg" "lambert2SG.gn" -na;
connectAttr "groupId114.msg" "lambert2SG.gn" -na;
connectAttr "groupId115.msg" "lambert2SG.gn" -na;
connectAttr "groupId116.msg" "lambert2SG.gn" -na;
connectAttr "groupId117.msg" "lambert2SG.gn" -na;
connectAttr "groupId118.msg" "lambert2SG.gn" -na;
connectAttr "groupId119.msg" "lambert2SG.gn" -na;
connectAttr "groupId120.msg" "lambert2SG.gn" -na;
connectAttr "groupId121.msg" "lambert2SG.gn" -na;
connectAttr "groupId122.msg" "lambert2SG.gn" -na;
connectAttr "groupId123.msg" "lambert2SG.gn" -na;
connectAttr "groupId124.msg" "lambert2SG.gn" -na;
connectAttr "groupId125.msg" "lambert2SG.gn" -na;
connectAttr "groupId126.msg" "lambert2SG.gn" -na;
connectAttr "groupId127.msg" "lambert2SG.gn" -na;
connectAttr "groupId128.msg" "lambert2SG.gn" -na;
connectAttr "groupId129.msg" "lambert2SG.gn" -na;
connectAttr "groupId130.msg" "lambert2SG.gn" -na;
connectAttr "groupId131.msg" "lambert2SG.gn" -na;
connectAttr "groupId132.msg" "lambert2SG.gn" -na;
connectAttr "groupId133.msg" "lambert2SG.gn" -na;
connectAttr "groupId134.msg" "lambert2SG.gn" -na;
connectAttr "groupId135.msg" "lambert2SG.gn" -na;
connectAttr "groupId136.msg" "lambert2SG.gn" -na;
connectAttr "groupId137.msg" "lambert2SG.gn" -na;
connectAttr "groupId138.msg" "lambert2SG.gn" -na;
connectAttr "groupId139.msg" "lambert2SG.gn" -na;
connectAttr "groupId140.msg" "lambert2SG.gn" -na;
connectAttr "groupId141.msg" "lambert2SG.gn" -na;
connectAttr "groupId142.msg" "lambert2SG.gn" -na;
connectAttr "groupId143.msg" "lambert2SG.gn" -na;
connectAttr "groupId144.msg" "lambert2SG.gn" -na;
connectAttr "groupId145.msg" "lambert2SG.gn" -na;
connectAttr "groupId146.msg" "lambert2SG.gn" -na;
connectAttr "groupId147.msg" "lambert2SG.gn" -na;
connectAttr "groupId148.msg" "lambert2SG.gn" -na;
connectAttr "groupId149.msg" "lambert2SG.gn" -na;
connectAttr "groupId150.msg" "lambert2SG.gn" -na;
connectAttr "groupId151.msg" "lambert2SG.gn" -na;
connectAttr "groupId152.msg" "lambert2SG.gn" -na;
connectAttr "groupId153.msg" "lambert2SG.gn" -na;
connectAttr "groupId154.msg" "lambert2SG.gn" -na;
connectAttr "groupId155.msg" "lambert2SG.gn" -na;
connectAttr "groupId156.msg" "lambert2SG.gn" -na;
connectAttr "groupId157.msg" "lambert2SG.gn" -na;
connectAttr "groupId158.msg" "lambert2SG.gn" -na;
connectAttr "groupId159.msg" "lambert2SG.gn" -na;
connectAttr "groupId160.msg" "lambert2SG.gn" -na;
connectAttr "groupId161.msg" "lambert2SG.gn" -na;
connectAttr "groupId162.msg" "lambert2SG.gn" -na;
connectAttr "groupId163.msg" "lambert2SG.gn" -na;
connectAttr "groupId164.msg" "lambert2SG.gn" -na;
connectAttr "groupId165.msg" "lambert2SG.gn" -na;
connectAttr "groupId166.msg" "lambert2SG.gn" -na;
connectAttr "groupId167.msg" "lambert2SG.gn" -na;
connectAttr "groupId168.msg" "lambert2SG.gn" -na;
connectAttr "groupId169.msg" "lambert2SG.gn" -na;
connectAttr "groupId170.msg" "lambert2SG.gn" -na;
connectAttr "groupId171.msg" "lambert2SG.gn" -na;
connectAttr "groupId172.msg" "lambert2SG.gn" -na;
connectAttr "groupId173.msg" "lambert2SG.gn" -na;
connectAttr "groupId174.msg" "lambert2SG.gn" -na;
connectAttr "groupId175.msg" "lambert2SG.gn" -na;
connectAttr "groupId176.msg" "lambert2SG.gn" -na;
connectAttr "groupId177.msg" "lambert2SG.gn" -na;
connectAttr "groupId178.msg" "lambert2SG.gn" -na;
connectAttr "groupId179.msg" "lambert2SG.gn" -na;
connectAttr "groupId180.msg" "lambert2SG.gn" -na;
connectAttr "groupId181.msg" "lambert2SG.gn" -na;
connectAttr "groupId182.msg" "lambert2SG.gn" -na;
connectAttr "groupId183.msg" "lambert2SG.gn" -na;
connectAttr "groupId184.msg" "lambert2SG.gn" -na;
connectAttr "groupId185.msg" "lambert2SG.gn" -na;
connectAttr "groupId186.msg" "lambert2SG.gn" -na;
connectAttr "groupId187.msg" "lambert2SG.gn" -na;
connectAttr "groupId188.msg" "lambert2SG.gn" -na;
connectAttr "groupId189.msg" "lambert2SG.gn" -na;
connectAttr "groupId190.msg" "lambert2SG.gn" -na;
connectAttr "groupId191.msg" "lambert2SG.gn" -na;
connectAttr "groupId192.msg" "lambert2SG.gn" -na;
connectAttr "groupId193.msg" "lambert2SG.gn" -na;
connectAttr "groupId194.msg" "lambert2SG.gn" -na;
connectAttr "groupId195.msg" "lambert2SG.gn" -na;
connectAttr "groupId196.msg" "lambert2SG.gn" -na;
connectAttr "groupId197.msg" "lambert2SG.gn" -na;
connectAttr "groupId198.msg" "lambert2SG.gn" -na;
connectAttr "groupId199.msg" "lambert2SG.gn" -na;
connectAttr "groupId200.msg" "lambert2SG.gn" -na;
connectAttr "groupId201.msg" "lambert2SG.gn" -na;
connectAttr "groupId202.msg" "lambert2SG.gn" -na;
connectAttr "groupId203.msg" "lambert2SG.gn" -na;
connectAttr "groupId204.msg" "lambert2SG.gn" -na;
connectAttr "groupId205.msg" "lambert2SG.gn" -na;
connectAttr "groupId206.msg" "lambert2SG.gn" -na;
connectAttr "groupId207.msg" "lambert2SG.gn" -na;
connectAttr "groupId208.msg" "lambert2SG.gn" -na;
connectAttr "groupId209.msg" "lambert2SG.gn" -na;
connectAttr "groupId210.msg" "lambert2SG.gn" -na;
connectAttr "groupId211.msg" "lambert2SG.gn" -na;
connectAttr "groupId212.msg" "lambert2SG.gn" -na;
connectAttr "groupId213.msg" "lambert2SG.gn" -na;
connectAttr "groupId214.msg" "lambert2SG.gn" -na;
connectAttr "groupId215.msg" "lambert2SG.gn" -na;
connectAttr "groupId216.msg" "lambert2SG.gn" -na;
connectAttr "groupId217.msg" "lambert2SG.gn" -na;
connectAttr "groupId218.msg" "lambert2SG.gn" -na;
connectAttr "groupId219.msg" "lambert2SG.gn" -na;
connectAttr "groupId220.msg" "lambert2SG.gn" -na;
connectAttr "groupId221.msg" "lambert2SG.gn" -na;
connectAttr "groupId222.msg" "lambert2SG.gn" -na;
connectAttr "groupId223.msg" "lambert2SG.gn" -na;
connectAttr "groupId224.msg" "lambert2SG.gn" -na;
connectAttr "groupId225.msg" "lambert2SG.gn" -na;
connectAttr "groupId226.msg" "lambert2SG.gn" -na;
connectAttr "groupId227.msg" "lambert2SG.gn" -na;
connectAttr "groupId228.msg" "lambert2SG.gn" -na;
connectAttr "groupId229.msg" "lambert2SG.gn" -na;
connectAttr "groupId230.msg" "lambert2SG.gn" -na;
connectAttr "groupId231.msg" "lambert2SG.gn" -na;
connectAttr "groupId232.msg" "lambert2SG.gn" -na;
connectAttr "groupId233.msg" "lambert2SG.gn" -na;
connectAttr "groupId234.msg" "lambert2SG.gn" -na;
connectAttr "groupId235.msg" "lambert2SG.gn" -na;
connectAttr "groupId236.msg" "lambert2SG.gn" -na;
connectAttr "groupId237.msg" "lambert2SG.gn" -na;
connectAttr "groupId238.msg" "lambert2SG.gn" -na;
connectAttr "groupId239.msg" "lambert2SG.gn" -na;
connectAttr "groupId240.msg" "lambert2SG.gn" -na;
connectAttr "groupId241.msg" "lambert2SG.gn" -na;
connectAttr "groupId242.msg" "lambert2SG.gn" -na;
connectAttr "groupId243.msg" "lambert2SG.gn" -na;
connectAttr "groupId244.msg" "lambert2SG.gn" -na;
connectAttr "groupId245.msg" "lambert2SG.gn" -na;
connectAttr "groupId246.msg" "lambert2SG.gn" -na;
connectAttr "groupId247.msg" "lambert2SG.gn" -na;
connectAttr "groupId248.msg" "lambert2SG.gn" -na;
connectAttr "groupId249.msg" "lambert2SG.gn" -na;
connectAttr "groupId250.msg" "lambert2SG.gn" -na;
connectAttr "groupId251.msg" "lambert2SG.gn" -na;
connectAttr "groupId252.msg" "lambert2SG.gn" -na;
connectAttr "groupId253.msg" "lambert2SG.gn" -na;
connectAttr "groupId254.msg" "lambert2SG.gn" -na;
connectAttr "groupId255.msg" "lambert2SG.gn" -na;
connectAttr "groupId256.msg" "lambert2SG.gn" -na;
connectAttr "groupId257.msg" "lambert2SG.gn" -na;
connectAttr "groupId258.msg" "lambert2SG.gn" -na;
connectAttr "groupId259.msg" "lambert2SG.gn" -na;
connectAttr "groupId260.msg" "lambert2SG.gn" -na;
connectAttr "groupId261.msg" "lambert2SG.gn" -na;
connectAttr "groupId262.msg" "lambert2SG.gn" -na;
connectAttr "groupId263.msg" "lambert2SG.gn" -na;
connectAttr "groupId264.msg" "lambert2SG.gn" -na;
connectAttr "groupId265.msg" "lambert2SG.gn" -na;
connectAttr "groupId266.msg" "lambert2SG.gn" -na;
connectAttr "groupId267.msg" "lambert2SG.gn" -na;
connectAttr "groupId268.msg" "lambert2SG.gn" -na;
connectAttr "groupId269.msg" "lambert2SG.gn" -na;
connectAttr "groupId270.msg" "lambert2SG.gn" -na;
connectAttr "groupId271.msg" "lambert2SG.gn" -na;
connectAttr "groupId272.msg" "lambert2SG.gn" -na;
connectAttr "groupId273.msg" "lambert2SG.gn" -na;
connectAttr "groupId274.msg" "lambert2SG.gn" -na;
connectAttr "groupId275.msg" "lambert2SG.gn" -na;
connectAttr "groupId276.msg" "lambert2SG.gn" -na;
connectAttr "groupId277.msg" "lambert2SG.gn" -na;
connectAttr "groupId286.msg" "lambert2SG.gn" -na;
connectAttr "groupId287.msg" "lambert2SG.gn" -na;
connectAttr "groupId288.msg" "lambert2SG.gn" -na;
connectAttr "groupId289.msg" "lambert2SG.gn" -na;
connectAttr "groupId290.msg" "lambert2SG.gn" -na;
connectAttr "groupId291.msg" "lambert2SG.gn" -na;
connectAttr "groupId292.msg" "lambert2SG.gn" -na;
connectAttr "groupId293.msg" "lambert2SG.gn" -na;
connectAttr "groupId294.msg" "lambert2SG.gn" -na;
connectAttr "groupId295.msg" "lambert2SG.gn" -na;
connectAttr "groupId296.msg" "lambert2SG.gn" -na;
connectAttr "groupId297.msg" "lambert2SG.gn" -na;
connectAttr "groupId298.msg" "lambert2SG.gn" -na;
connectAttr "groupId299.msg" "lambert2SG.gn" -na;
connectAttr "groupId300.msg" "lambert2SG.gn" -na;
connectAttr "groupId301.msg" "lambert2SG.gn" -na;
connectAttr "groupId302.msg" "lambert2SG.gn" -na;
connectAttr "groupId303.msg" "lambert2SG.gn" -na;
connectAttr "groupId304.msg" "lambert2SG.gn" -na;
connectAttr "groupId305.msg" "lambert2SG.gn" -na;
connectAttr "groupId308.msg" "lambert2SG.gn" -na;
connectAttr "groupId309.msg" "lambert2SG.gn" -na;
connectAttr "groupId310.msg" "lambert2SG.gn" -na;
connectAttr "groupId311.msg" "lambert2SG.gn" -na;
connectAttr "groupId312.msg" "lambert2SG.gn" -na;
connectAttr "groupId313.msg" "lambert2SG.gn" -na;
connectAttr "groupId314.msg" "lambert2SG.gn" -na;
connectAttr "groupId315.msg" "lambert2SG.gn" -na;
connectAttr "groupId316.msg" "lambert2SG.gn" -na;
connectAttr "groupId317.msg" "lambert2SG.gn" -na;
connectAttr "groupId322.msg" "lambert2SG.gn" -na;
connectAttr "groupId323.msg" "lambert2SG.gn" -na;
connectAttr "groupId324.msg" "lambert2SG.gn" -na;
connectAttr "groupId325.msg" "lambert2SG.gn" -na;
connectAttr "groupId326.msg" "lambert2SG.gn" -na;
connectAttr "groupId327.msg" "lambert2SG.gn" -na;
connectAttr "groupId328.msg" "lambert2SG.gn" -na;
connectAttr "groupId329.msg" "lambert2SG.gn" -na;
connectAttr "groupId332.msg" "lambert2SG.gn" -na;
connectAttr "groupId333.msg" "lambert2SG.gn" -na;
connectAttr "groupId334.msg" "lambert2SG.gn" -na;
connectAttr "groupId335.msg" "lambert2SG.gn" -na;
connectAttr "groupId336.msg" "lambert2SG.gn" -na;
connectAttr "groupId337.msg" "lambert2SG.gn" -na;
connectAttr "groupId338.msg" "lambert2SG.gn" -na;
connectAttr "groupId339.msg" "lambert2SG.gn" -na;
connectAttr "groupId340.msg" "lambert2SG.gn" -na;
connectAttr "groupId341.msg" "lambert2SG.gn" -na;
connectAttr "groupId342.msg" "lambert2SG.gn" -na;
connectAttr "groupId343.msg" "lambert2SG.gn" -na;
connectAttr "groupId344.msg" "lambert2SG.gn" -na;
connectAttr "groupId345.msg" "lambert2SG.gn" -na;
connectAttr "groupId346.msg" "lambert2SG.gn" -na;
connectAttr "groupId347.msg" "lambert2SG.gn" -na;
connectAttr "groupId348.msg" "lambert2SG.gn" -na;
connectAttr "groupId349.msg" "lambert2SG.gn" -na;
connectAttr "groupId350.msg" "lambert2SG.gn" -na;
connectAttr "groupId351.msg" "lambert2SG.gn" -na;
connectAttr "groupId352.msg" "lambert2SG.gn" -na;
connectAttr "groupId353.msg" "lambert2SG.gn" -na;
connectAttr "groupId354.msg" "lambert2SG.gn" -na;
connectAttr "groupId355.msg" "lambert2SG.gn" -na;
connectAttr "groupId356.msg" "lambert2SG.gn" -na;
connectAttr "groupId357.msg" "lambert2SG.gn" -na;
connectAttr "groupId358.msg" "lambert2SG.gn" -na;
connectAttr "groupId359.msg" "lambert2SG.gn" -na;
connectAttr "groupId360.msg" "lambert2SG.gn" -na;
connectAttr "groupId361.msg" "lambert2SG.gn" -na;
connectAttr "groupId362.msg" "lambert2SG.gn" -na;
connectAttr "groupId363.msg" "lambert2SG.gn" -na;
connectAttr "groupId400.msg" "lambert2SG.gn" -na;
connectAttr "lambert2SG.msg" "materialInfo1.sg";
connectAttr "west_house_body.msg" "materialInfo1.m";
connectAttr "west_house_posts.oc" "lambert3SG.ss";
connectAttr "pCubeShape2.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape2.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape3.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape5.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape5.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape152.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape152.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape153.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape153.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape154.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape154.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape155.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape155.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape166.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape166.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape172.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape172.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape173.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape173.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape178.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape178.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape196.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape196.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape197.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape197.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape198.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape198.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape199.iog.og[0]" "lambert3SG.dsm" -na;
connectAttr "pCubeShape199.ciog.cog[0]" "lambert3SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[1]" "lambert3SG.dsm" -na;
connectAttr "groupId6.msg" "lambert3SG.gn" -na;
connectAttr "groupId7.msg" "lambert3SG.gn" -na;
connectAttr "groupId8.msg" "lambert3SG.gn" -na;
connectAttr "groupId9.msg" "lambert3SG.gn" -na;
connectAttr "groupId10.msg" "lambert3SG.gn" -na;
connectAttr "groupId11.msg" "lambert3SG.gn" -na;
connectAttr "groupId278.msg" "lambert3SG.gn" -na;
connectAttr "groupId279.msg" "lambert3SG.gn" -na;
connectAttr "groupId280.msg" "lambert3SG.gn" -na;
connectAttr "groupId281.msg" "lambert3SG.gn" -na;
connectAttr "groupId282.msg" "lambert3SG.gn" -na;
connectAttr "groupId283.msg" "lambert3SG.gn" -na;
connectAttr "groupId284.msg" "lambert3SG.gn" -na;
connectAttr "groupId285.msg" "lambert3SG.gn" -na;
connectAttr "groupId306.msg" "lambert3SG.gn" -na;
connectAttr "groupId307.msg" "lambert3SG.gn" -na;
connectAttr "groupId318.msg" "lambert3SG.gn" -na;
connectAttr "groupId319.msg" "lambert3SG.gn" -na;
connectAttr "groupId320.msg" "lambert3SG.gn" -na;
connectAttr "groupId321.msg" "lambert3SG.gn" -na;
connectAttr "groupId330.msg" "lambert3SG.gn" -na;
connectAttr "groupId331.msg" "lambert3SG.gn" -na;
connectAttr "groupId364.msg" "lambert3SG.gn" -na;
connectAttr "groupId365.msg" "lambert3SG.gn" -na;
connectAttr "groupId366.msg" "lambert3SG.gn" -na;
connectAttr "groupId367.msg" "lambert3SG.gn" -na;
connectAttr "groupId368.msg" "lambert3SG.gn" -na;
connectAttr "groupId369.msg" "lambert3SG.gn" -na;
connectAttr "groupId370.msg" "lambert3SG.gn" -na;
connectAttr "groupId371.msg" "lambert3SG.gn" -na;
connectAttr "groupId401.msg" "lambert3SG.gn" -na;
connectAttr "lambert3SG.msg" "materialInfo2.sg";
connectAttr "west_house_posts.msg" "materialInfo2.m";
connectAttr "west_house_roof_frame.oc" "lambert4SG.ss";
connectAttr "pCubeShape195.iog.og[0]" "lambert4SG.dsm" -na;
connectAttr "pCubeShape195.ciog.cog[0]" "lambert4SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[2]" "lambert4SG.dsm" -na;
connectAttr "groupId1.msg" "lambert4SG.gn" -na;
connectAttr "groupId2.msg" "lambert4SG.gn" -na;
connectAttr "groupId402.msg" "lambert4SG.gn" -na;
connectAttr "lambert4SG.msg" "materialInfo3.sg";
connectAttr "west_house_roof_frame.msg" "materialInfo3.m";
connectAttr "west_house_window_frame.oc" "lambert5SG.ss";
connectAttr "pCubeShape200.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape200.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape201.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape201.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape202.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape202.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape203.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape203.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape204.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape204.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape205.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape205.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape206.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape206.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape207.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape207.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape208.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape208.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape210.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape210.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape212.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape212.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape213.iog.og[0]" "lambert5SG.dsm" -na;
connectAttr "pCubeShape213.ciog.cog[0]" "lambert5SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[4]" "lambert5SG.dsm" -na;
connectAttr "groupId372.msg" "lambert5SG.gn" -na;
connectAttr "groupId373.msg" "lambert5SG.gn" -na;
connectAttr "groupId374.msg" "lambert5SG.gn" -na;
connectAttr "groupId375.msg" "lambert5SG.gn" -na;
connectAttr "groupId376.msg" "lambert5SG.gn" -na;
connectAttr "groupId377.msg" "lambert5SG.gn" -na;
connectAttr "groupId378.msg" "lambert5SG.gn" -na;
connectAttr "groupId379.msg" "lambert5SG.gn" -na;
connectAttr "groupId380.msg" "lambert5SG.gn" -na;
connectAttr "groupId381.msg" "lambert5SG.gn" -na;
connectAttr "groupId382.msg" "lambert5SG.gn" -na;
connectAttr "groupId383.msg" "lambert5SG.gn" -na;
connectAttr "groupId384.msg" "lambert5SG.gn" -na;
connectAttr "groupId385.msg" "lambert5SG.gn" -na;
connectAttr "groupId386.msg" "lambert5SG.gn" -na;
connectAttr "groupId387.msg" "lambert5SG.gn" -na;
connectAttr "groupId388.msg" "lambert5SG.gn" -na;
connectAttr "groupId389.msg" "lambert5SG.gn" -na;
connectAttr "groupId392.msg" "lambert5SG.gn" -na;
connectAttr "groupId393.msg" "lambert5SG.gn" -na;
connectAttr "groupId396.msg" "lambert5SG.gn" -na;
connectAttr "groupId397.msg" "lambert5SG.gn" -na;
connectAttr "groupId398.msg" "lambert5SG.gn" -na;
connectAttr "groupId399.msg" "lambert5SG.gn" -na;
connectAttr "groupId404.msg" "lambert5SG.gn" -na;
connectAttr "lambert5SG.msg" "materialInfo4.sg";
connectAttr "west_house_window_frame.msg" "materialInfo4.m";
connectAttr "west_house_window.oc" "lambert6SG.ss";
connectAttr "pCubeShape209.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "pCubeShape209.ciog.cog[0]" "lambert6SG.dsm" -na;
connectAttr "pCubeShape211.iog.og[0]" "lambert6SG.dsm" -na;
connectAttr "pCubeShape211.ciog.cog[0]" "lambert6SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[5]" "lambert6SG.dsm" -na;
connectAttr "groupId390.msg" "lambert6SG.gn" -na;
connectAttr "groupId391.msg" "lambert6SG.gn" -na;
connectAttr "groupId394.msg" "lambert6SG.gn" -na;
connectAttr "groupId395.msg" "lambert6SG.gn" -na;
connectAttr "groupId405.msg" "lambert6SG.gn" -na;
connectAttr "lambert6SG.msg" "materialInfo5.sg";
connectAttr "west_house_window.msg" "materialInfo5.m";
connectAttr "polySubdFace1.out" "groupParts1.ig";
connectAttr "groupId1.id" "groupParts1.gi";
connectAttr "groupParts1.og" "groupParts2.ig";
connectAttr "groupId3.id" "groupParts2.gi";
connectAttr "west_house_roof.oc" "lambert7SG.ss";
connectAttr "pCubeShape195.iog.og[1]" "lambert7SG.dsm" -na;
connectAttr "pCube214Shape.iog.og[3]" "lambert7SG.dsm" -na;
connectAttr "groupId403.msg" "lambert7SG.gn" -na;
connectAttr "lambert7SG.msg" "materialInfo6.sg";
connectAttr "west_house_roof.msg" "materialInfo6.m";
connectAttr "west_house_body.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[0].dn"
		;
connectAttr "lambert2SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[1].dn"
		;
connectAttr "west_house_posts.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[2].dn"
		;
connectAttr "lambert3SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[3].dn"
		;
connectAttr "west_house_roof_frame.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[4].dn"
		;
connectAttr "lambert4SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[5].dn"
		;
connectAttr "west_house_window_frame.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[6].dn"
		;
connectAttr "lambert5SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[7].dn"
		;
connectAttr "west_house_window.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[8].dn"
		;
connectAttr "lambert6SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[9].dn"
		;
connectAttr "west_house_roof.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[10].dn"
		;
connectAttr "lambert7SG.msg" "hyperShadePrimaryNodeEditorSavedTabsInfo.tgi[0].ni[11].dn"
		;
connectAttr "pCubeShape1.o" "polyUnite1.ip[0]";
connectAttr "pCubeShape2.o" "polyUnite1.ip[1]";
connectAttr "pCubeShape3.o" "polyUnite1.ip[2]";
connectAttr "pCubeShape5.o" "polyUnite1.ip[3]";
connectAttr "pCubeShape8.o" "polyUnite1.ip[4]";
connectAttr "pCubeShape9.o" "polyUnite1.ip[5]";
connectAttr "pCubeShape10.o" "polyUnite1.ip[6]";
connectAttr "pCubeShape11.o" "polyUnite1.ip[7]";
connectAttr "pCubeShape12.o" "polyUnite1.ip[8]";
connectAttr "pCubeShape13.o" "polyUnite1.ip[9]";
connectAttr "pCubeShape14.o" "polyUnite1.ip[10]";
connectAttr "pCubeShape15.o" "polyUnite1.ip[11]";
connectAttr "pCubeShape16.o" "polyUnite1.ip[12]";
connectAttr "pCubeShape18.o" "polyUnite1.ip[13]";
connectAttr "pCubeShape19.o" "polyUnite1.ip[14]";
connectAttr "pCubeShape20.o" "polyUnite1.ip[15]";
connectAttr "pCubeShape21.o" "polyUnite1.ip[16]";
connectAttr "pCubeShape22.o" "polyUnite1.ip[17]";
connectAttr "pCubeShape23.o" "polyUnite1.ip[18]";
connectAttr "pCubeShape24.o" "polyUnite1.ip[19]";
connectAttr "pCubeShape25.o" "polyUnite1.ip[20]";
connectAttr "pCubeShape26.o" "polyUnite1.ip[21]";
connectAttr "pCubeShape27.o" "polyUnite1.ip[22]";
connectAttr "pCubeShape28.o" "polyUnite1.ip[23]";
connectAttr "pCubeShape29.o" "polyUnite1.ip[24]";
connectAttr "pCubeShape30.o" "polyUnite1.ip[25]";
connectAttr "pCubeShape31.o" "polyUnite1.ip[26]";
connectAttr "pCubeShape32.o" "polyUnite1.ip[27]";
connectAttr "pCubeShape33.o" "polyUnite1.ip[28]";
connectAttr "pCubeShape34.o" "polyUnite1.ip[29]";
connectAttr "pCubeShape35.o" "polyUnite1.ip[30]";
connectAttr "pCubeShape36.o" "polyUnite1.ip[31]";
connectAttr "pCubeShape37.o" "polyUnite1.ip[32]";
connectAttr "pCubeShape38.o" "polyUnite1.ip[33]";
connectAttr "pCubeShape39.o" "polyUnite1.ip[34]";
connectAttr "pCubeShape40.o" "polyUnite1.ip[35]";
connectAttr "pCubeShape41.o" "polyUnite1.ip[36]";
connectAttr "pCubeShape42.o" "polyUnite1.ip[37]";
connectAttr "pCubeShape43.o" "polyUnite1.ip[38]";
connectAttr "pCubeShape45.o" "polyUnite1.ip[39]";
connectAttr "pCubeShape46.o" "polyUnite1.ip[40]";
connectAttr "pCubeShape47.o" "polyUnite1.ip[41]";
connectAttr "pCubeShape48.o" "polyUnite1.ip[42]";
connectAttr "pCubeShape49.o" "polyUnite1.ip[43]";
connectAttr "pCubeShape50.o" "polyUnite1.ip[44]";
connectAttr "pCubeShape51.o" "polyUnite1.ip[45]";
connectAttr "pCubeShape52.o" "polyUnite1.ip[46]";
connectAttr "pCubeShape53.o" "polyUnite1.ip[47]";
connectAttr "pCubeShape54.o" "polyUnite1.ip[48]";
connectAttr "pCubeShape55.o" "polyUnite1.ip[49]";
connectAttr "pCubeShape56.o" "polyUnite1.ip[50]";
connectAttr "pCubeShape57.o" "polyUnite1.ip[51]";
connectAttr "pCubeShape58.o" "polyUnite1.ip[52]";
connectAttr "pCubeShape59.o" "polyUnite1.ip[53]";
connectAttr "pCubeShape60.o" "polyUnite1.ip[54]";
connectAttr "pCubeShape61.o" "polyUnite1.ip[55]";
connectAttr "pCubeShape62.o" "polyUnite1.ip[56]";
connectAttr "pCubeShape63.o" "polyUnite1.ip[57]";
connectAttr "pCubeShape64.o" "polyUnite1.ip[58]";
connectAttr "pCubeShape65.o" "polyUnite1.ip[59]";
connectAttr "pCubeShape66.o" "polyUnite1.ip[60]";
connectAttr "pCubeShape67.o" "polyUnite1.ip[61]";
connectAttr "pCubeShape68.o" "polyUnite1.ip[62]";
connectAttr "pCubeShape69.o" "polyUnite1.ip[63]";
connectAttr "pCubeShape70.o" "polyUnite1.ip[64]";
connectAttr "pCubeShape71.o" "polyUnite1.ip[65]";
connectAttr "pCubeShape72.o" "polyUnite1.ip[66]";
connectAttr "pCubeShape73.o" "polyUnite1.ip[67]";
connectAttr "pCubeShape74.o" "polyUnite1.ip[68]";
connectAttr "pCubeShape75.o" "polyUnite1.ip[69]";
connectAttr "pCubeShape76.o" "polyUnite1.ip[70]";
connectAttr "pCubeShape77.o" "polyUnite1.ip[71]";
connectAttr "pCubeShape78.o" "polyUnite1.ip[72]";
connectAttr "pCubeShape79.o" "polyUnite1.ip[73]";
connectAttr "pCubeShape81.o" "polyUnite1.ip[74]";
connectAttr "pCubeShape82.o" "polyUnite1.ip[75]";
connectAttr "pCubeShape83.o" "polyUnite1.ip[76]";
connectAttr "pCubeShape84.o" "polyUnite1.ip[77]";
connectAttr "pCubeShape85.o" "polyUnite1.ip[78]";
connectAttr "pCubeShape86.o" "polyUnite1.ip[79]";
connectAttr "pCubeShape87.o" "polyUnite1.ip[80]";
connectAttr "pCubeShape88.o" "polyUnite1.ip[81]";
connectAttr "pCubeShape90.o" "polyUnite1.ip[82]";
connectAttr "pCubeShape91.o" "polyUnite1.ip[83]";
connectAttr "pCubeShape92.o" "polyUnite1.ip[84]";
connectAttr "pCubeShape93.o" "polyUnite1.ip[85]";
connectAttr "pCubeShape94.o" "polyUnite1.ip[86]";
connectAttr "pCubeShape95.o" "polyUnite1.ip[87]";
connectAttr "pCubeShape96.o" "polyUnite1.ip[88]";
connectAttr "pCubeShape97.o" "polyUnite1.ip[89]";
connectAttr "pCubeShape99.o" "polyUnite1.ip[90]";
connectAttr "pCubeShape100.o" "polyUnite1.ip[91]";
connectAttr "pCubeShape101.o" "polyUnite1.ip[92]";
connectAttr "pCubeShape102.o" "polyUnite1.ip[93]";
connectAttr "pCubeShape103.o" "polyUnite1.ip[94]";
connectAttr "pCubeShape104.o" "polyUnite1.ip[95]";
connectAttr "pCubeShape105.o" "polyUnite1.ip[96]";
connectAttr "pCubeShape106.o" "polyUnite1.ip[97]";
connectAttr "pCubeShape107.o" "polyUnite1.ip[98]";
connectAttr "pCubeShape108.o" "polyUnite1.ip[99]";
connectAttr "pCubeShape109.o" "polyUnite1.ip[100]";
connectAttr "pCubeShape110.o" "polyUnite1.ip[101]";
connectAttr "pCubeShape111.o" "polyUnite1.ip[102]";
connectAttr "pCubeShape112.o" "polyUnite1.ip[103]";
connectAttr "pCubeShape113.o" "polyUnite1.ip[104]";
connectAttr "pCubeShape114.o" "polyUnite1.ip[105]";
connectAttr "pCubeShape115.o" "polyUnite1.ip[106]";
connectAttr "pCubeShape118.o" "polyUnite1.ip[107]";
connectAttr "pCubeShape119.o" "polyUnite1.ip[108]";
connectAttr "pCubeShape120.o" "polyUnite1.ip[109]";
connectAttr "pCubeShape121.o" "polyUnite1.ip[110]";
connectAttr "pCubeShape122.o" "polyUnite1.ip[111]";
connectAttr "pCubeShape123.o" "polyUnite1.ip[112]";
connectAttr "pCubeShape124.o" "polyUnite1.ip[113]";
connectAttr "pCubeShape127.o" "polyUnite1.ip[114]";
connectAttr "pCubeShape128.o" "polyUnite1.ip[115]";
connectAttr "pCubeShape129.o" "polyUnite1.ip[116]";
connectAttr "pCubeShape130.o" "polyUnite1.ip[117]";
connectAttr "pCubeShape131.o" "polyUnite1.ip[118]";
connectAttr "pCubeShape132.o" "polyUnite1.ip[119]";
connectAttr "pCubeShape133.o" "polyUnite1.ip[120]";
connectAttr "pCubeShape136.o" "polyUnite1.ip[121]";
connectAttr "pCubeShape137.o" "polyUnite1.ip[122]";
connectAttr "pCubeShape138.o" "polyUnite1.ip[123]";
connectAttr "pCubeShape139.o" "polyUnite1.ip[124]";
connectAttr "pCubeShape140.o" "polyUnite1.ip[125]";
connectAttr "pCubeShape141.o" "polyUnite1.ip[126]";
connectAttr "pCubeShape142.o" "polyUnite1.ip[127]";
connectAttr "pCubeShape143.o" "polyUnite1.ip[128]";
connectAttr "pCubeShape144.o" "polyUnite1.ip[129]";
connectAttr "pCubeShape145.o" "polyUnite1.ip[130]";
connectAttr "pCubeShape146.o" "polyUnite1.ip[131]";
connectAttr "pCubeShape147.o" "polyUnite1.ip[132]";
connectAttr "pCubeShape148.o" "polyUnite1.ip[133]";
connectAttr "pCubeShape149.o" "polyUnite1.ip[134]";
connectAttr "pCubeShape150.o" "polyUnite1.ip[135]";
connectAttr "pCubeShape151.o" "polyUnite1.ip[136]";
connectAttr "pCubeShape152.o" "polyUnite1.ip[137]";
connectAttr "pCubeShape153.o" "polyUnite1.ip[138]";
connectAttr "pCubeShape154.o" "polyUnite1.ip[139]";
connectAttr "pCubeShape155.o" "polyUnite1.ip[140]";
connectAttr "pCubeShape156.o" "polyUnite1.ip[141]";
connectAttr "pCubeShape157.o" "polyUnite1.ip[142]";
connectAttr "pCubeShape158.o" "polyUnite1.ip[143]";
connectAttr "pCubeShape159.o" "polyUnite1.ip[144]";
connectAttr "pCubeShape160.o" "polyUnite1.ip[145]";
connectAttr "pCubeShape161.o" "polyUnite1.ip[146]";
connectAttr "pCubeShape162.o" "polyUnite1.ip[147]";
connectAttr "pCubeShape163.o" "polyUnite1.ip[148]";
connectAttr "pCubeShape164.o" "polyUnite1.ip[149]";
connectAttr "pCubeShape165.o" "polyUnite1.ip[150]";
connectAttr "pCubeShape166.o" "polyUnite1.ip[151]";
connectAttr "pCubeShape167.o" "polyUnite1.ip[152]";
connectAttr "pCubeShape168.o" "polyUnite1.ip[153]";
connectAttr "pCubeShape169.o" "polyUnite1.ip[154]";
connectAttr "pCubeShape170.o" "polyUnite1.ip[155]";
connectAttr "pCubeShape171.o" "polyUnite1.ip[156]";
connectAttr "pCubeShape172.o" "polyUnite1.ip[157]";
connectAttr "pCubeShape173.o" "polyUnite1.ip[158]";
connectAttr "pCubeShape174.o" "polyUnite1.ip[159]";
connectAttr "pCubeShape175.o" "polyUnite1.ip[160]";
connectAttr "pCubeShape176.o" "polyUnite1.ip[161]";
connectAttr "pCubeShape177.o" "polyUnite1.ip[162]";
connectAttr "pCubeShape178.o" "polyUnite1.ip[163]";
connectAttr "pCubeShape179.o" "polyUnite1.ip[164]";
connectAttr "pCubeShape180.o" "polyUnite1.ip[165]";
connectAttr "pCubeShape181.o" "polyUnite1.ip[166]";
connectAttr "pCubeShape182.o" "polyUnite1.ip[167]";
connectAttr "pCubeShape183.o" "polyUnite1.ip[168]";
connectAttr "pCubeShape184.o" "polyUnite1.ip[169]";
connectAttr "pCubeShape185.o" "polyUnite1.ip[170]";
connectAttr "pCubeShape186.o" "polyUnite1.ip[171]";
connectAttr "pCubeShape187.o" "polyUnite1.ip[172]";
connectAttr "pCubeShape188.o" "polyUnite1.ip[173]";
connectAttr "pCubeShape189.o" "polyUnite1.ip[174]";
connectAttr "pCubeShape190.o" "polyUnite1.ip[175]";
connectAttr "pCubeShape191.o" "polyUnite1.ip[176]";
connectAttr "pCubeShape192.o" "polyUnite1.ip[177]";
connectAttr "pCubeShape193.o" "polyUnite1.ip[178]";
connectAttr "pCubeShape194.o" "polyUnite1.ip[179]";
connectAttr "pCubeShape195.o" "polyUnite1.ip[180]";
connectAttr "pCubeShape196.o" "polyUnite1.ip[181]";
connectAttr "pCubeShape197.o" "polyUnite1.ip[182]";
connectAttr "pCubeShape198.o" "polyUnite1.ip[183]";
connectAttr "pCubeShape199.o" "polyUnite1.ip[184]";
connectAttr "pCubeShape200.o" "polyUnite1.ip[185]";
connectAttr "pCubeShape201.o" "polyUnite1.ip[186]";
connectAttr "pCubeShape202.o" "polyUnite1.ip[187]";
connectAttr "pCubeShape203.o" "polyUnite1.ip[188]";
connectAttr "pCubeShape204.o" "polyUnite1.ip[189]";
connectAttr "pCubeShape205.o" "polyUnite1.ip[190]";
connectAttr "pCubeShape206.o" "polyUnite1.ip[191]";
connectAttr "pCubeShape207.o" "polyUnite1.ip[192]";
connectAttr "pCubeShape208.o" "polyUnite1.ip[193]";
connectAttr "pCubeShape209.o" "polyUnite1.ip[194]";
connectAttr "pCubeShape210.o" "polyUnite1.ip[195]";
connectAttr "pCubeShape211.o" "polyUnite1.ip[196]";
connectAttr "pCubeShape212.o" "polyUnite1.ip[197]";
connectAttr "pCubeShape213.o" "polyUnite1.ip[198]";
connectAttr "pCubeShape1.wm" "polyUnite1.im[0]";
connectAttr "pCubeShape2.wm" "polyUnite1.im[1]";
connectAttr "pCubeShape3.wm" "polyUnite1.im[2]";
connectAttr "pCubeShape5.wm" "polyUnite1.im[3]";
connectAttr "pCubeShape8.wm" "polyUnite1.im[4]";
connectAttr "pCubeShape9.wm" "polyUnite1.im[5]";
connectAttr "pCubeShape10.wm" "polyUnite1.im[6]";
connectAttr "pCubeShape11.wm" "polyUnite1.im[7]";
connectAttr "pCubeShape12.wm" "polyUnite1.im[8]";
connectAttr "pCubeShape13.wm" "polyUnite1.im[9]";
connectAttr "pCubeShape14.wm" "polyUnite1.im[10]";
connectAttr "pCubeShape15.wm" "polyUnite1.im[11]";
connectAttr "pCubeShape16.wm" "polyUnite1.im[12]";
connectAttr "pCubeShape18.wm" "polyUnite1.im[13]";
connectAttr "pCubeShape19.wm" "polyUnite1.im[14]";
connectAttr "pCubeShape20.wm" "polyUnite1.im[15]";
connectAttr "pCubeShape21.wm" "polyUnite1.im[16]";
connectAttr "pCubeShape22.wm" "polyUnite1.im[17]";
connectAttr "pCubeShape23.wm" "polyUnite1.im[18]";
connectAttr "pCubeShape24.wm" "polyUnite1.im[19]";
connectAttr "pCubeShape25.wm" "polyUnite1.im[20]";
connectAttr "pCubeShape26.wm" "polyUnite1.im[21]";
connectAttr "pCubeShape27.wm" "polyUnite1.im[22]";
connectAttr "pCubeShape28.wm" "polyUnite1.im[23]";
connectAttr "pCubeShape29.wm" "polyUnite1.im[24]";
connectAttr "pCubeShape30.wm" "polyUnite1.im[25]";
connectAttr "pCubeShape31.wm" "polyUnite1.im[26]";
connectAttr "pCubeShape32.wm" "polyUnite1.im[27]";
connectAttr "pCubeShape33.wm" "polyUnite1.im[28]";
connectAttr "pCubeShape34.wm" "polyUnite1.im[29]";
connectAttr "pCubeShape35.wm" "polyUnite1.im[30]";
connectAttr "pCubeShape36.wm" "polyUnite1.im[31]";
connectAttr "pCubeShape37.wm" "polyUnite1.im[32]";
connectAttr "pCubeShape38.wm" "polyUnite1.im[33]";
connectAttr "pCubeShape39.wm" "polyUnite1.im[34]";
connectAttr "pCubeShape40.wm" "polyUnite1.im[35]";
connectAttr "pCubeShape41.wm" "polyUnite1.im[36]";
connectAttr "pCubeShape42.wm" "polyUnite1.im[37]";
connectAttr "pCubeShape43.wm" "polyUnite1.im[38]";
connectAttr "pCubeShape45.wm" "polyUnite1.im[39]";
connectAttr "pCubeShape46.wm" "polyUnite1.im[40]";
connectAttr "pCubeShape47.wm" "polyUnite1.im[41]";
connectAttr "pCubeShape48.wm" "polyUnite1.im[42]";
connectAttr "pCubeShape49.wm" "polyUnite1.im[43]";
connectAttr "pCubeShape50.wm" "polyUnite1.im[44]";
connectAttr "pCubeShape51.wm" "polyUnite1.im[45]";
connectAttr "pCubeShape52.wm" "polyUnite1.im[46]";
connectAttr "pCubeShape53.wm" "polyUnite1.im[47]";
connectAttr "pCubeShape54.wm" "polyUnite1.im[48]";
connectAttr "pCubeShape55.wm" "polyUnite1.im[49]";
connectAttr "pCubeShape56.wm" "polyUnite1.im[50]";
connectAttr "pCubeShape57.wm" "polyUnite1.im[51]";
connectAttr "pCubeShape58.wm" "polyUnite1.im[52]";
connectAttr "pCubeShape59.wm" "polyUnite1.im[53]";
connectAttr "pCubeShape60.wm" "polyUnite1.im[54]";
connectAttr "pCubeShape61.wm" "polyUnite1.im[55]";
connectAttr "pCubeShape62.wm" "polyUnite1.im[56]";
connectAttr "pCubeShape63.wm" "polyUnite1.im[57]";
connectAttr "pCubeShape64.wm" "polyUnite1.im[58]";
connectAttr "pCubeShape65.wm" "polyUnite1.im[59]";
connectAttr "pCubeShape66.wm" "polyUnite1.im[60]";
connectAttr "pCubeShape67.wm" "polyUnite1.im[61]";
connectAttr "pCubeShape68.wm" "polyUnite1.im[62]";
connectAttr "pCubeShape69.wm" "polyUnite1.im[63]";
connectAttr "pCubeShape70.wm" "polyUnite1.im[64]";
connectAttr "pCubeShape71.wm" "polyUnite1.im[65]";
connectAttr "pCubeShape72.wm" "polyUnite1.im[66]";
connectAttr "pCubeShape73.wm" "polyUnite1.im[67]";
connectAttr "pCubeShape74.wm" "polyUnite1.im[68]";
connectAttr "pCubeShape75.wm" "polyUnite1.im[69]";
connectAttr "pCubeShape76.wm" "polyUnite1.im[70]";
connectAttr "pCubeShape77.wm" "polyUnite1.im[71]";
connectAttr "pCubeShape78.wm" "polyUnite1.im[72]";
connectAttr "pCubeShape79.wm" "polyUnite1.im[73]";
connectAttr "pCubeShape81.wm" "polyUnite1.im[74]";
connectAttr "pCubeShape82.wm" "polyUnite1.im[75]";
connectAttr "pCubeShape83.wm" "polyUnite1.im[76]";
connectAttr "pCubeShape84.wm" "polyUnite1.im[77]";
connectAttr "pCubeShape85.wm" "polyUnite1.im[78]";
connectAttr "pCubeShape86.wm" "polyUnite1.im[79]";
connectAttr "pCubeShape87.wm" "polyUnite1.im[80]";
connectAttr "pCubeShape88.wm" "polyUnite1.im[81]";
connectAttr "pCubeShape90.wm" "polyUnite1.im[82]";
connectAttr "pCubeShape91.wm" "polyUnite1.im[83]";
connectAttr "pCubeShape92.wm" "polyUnite1.im[84]";
connectAttr "pCubeShape93.wm" "polyUnite1.im[85]";
connectAttr "pCubeShape94.wm" "polyUnite1.im[86]";
connectAttr "pCubeShape95.wm" "polyUnite1.im[87]";
connectAttr "pCubeShape96.wm" "polyUnite1.im[88]";
connectAttr "pCubeShape97.wm" "polyUnite1.im[89]";
connectAttr "pCubeShape99.wm" "polyUnite1.im[90]";
connectAttr "pCubeShape100.wm" "polyUnite1.im[91]";
connectAttr "pCubeShape101.wm" "polyUnite1.im[92]";
connectAttr "pCubeShape102.wm" "polyUnite1.im[93]";
connectAttr "pCubeShape103.wm" "polyUnite1.im[94]";
connectAttr "pCubeShape104.wm" "polyUnite1.im[95]";
connectAttr "pCubeShape105.wm" "polyUnite1.im[96]";
connectAttr "pCubeShape106.wm" "polyUnite1.im[97]";
connectAttr "pCubeShape107.wm" "polyUnite1.im[98]";
connectAttr "pCubeShape108.wm" "polyUnite1.im[99]";
connectAttr "pCubeShape109.wm" "polyUnite1.im[100]";
connectAttr "pCubeShape110.wm" "polyUnite1.im[101]";
connectAttr "pCubeShape111.wm" "polyUnite1.im[102]";
connectAttr "pCubeShape112.wm" "polyUnite1.im[103]";
connectAttr "pCubeShape113.wm" "polyUnite1.im[104]";
connectAttr "pCubeShape114.wm" "polyUnite1.im[105]";
connectAttr "pCubeShape115.wm" "polyUnite1.im[106]";
connectAttr "pCubeShape118.wm" "polyUnite1.im[107]";
connectAttr "pCubeShape119.wm" "polyUnite1.im[108]";
connectAttr "pCubeShape120.wm" "polyUnite1.im[109]";
connectAttr "pCubeShape121.wm" "polyUnite1.im[110]";
connectAttr "pCubeShape122.wm" "polyUnite1.im[111]";
connectAttr "pCubeShape123.wm" "polyUnite1.im[112]";
connectAttr "pCubeShape124.wm" "polyUnite1.im[113]";
connectAttr "pCubeShape127.wm" "polyUnite1.im[114]";
connectAttr "pCubeShape128.wm" "polyUnite1.im[115]";
connectAttr "pCubeShape129.wm" "polyUnite1.im[116]";
connectAttr "pCubeShape130.wm" "polyUnite1.im[117]";
connectAttr "pCubeShape131.wm" "polyUnite1.im[118]";
connectAttr "pCubeShape132.wm" "polyUnite1.im[119]";
connectAttr "pCubeShape133.wm" "polyUnite1.im[120]";
connectAttr "pCubeShape136.wm" "polyUnite1.im[121]";
connectAttr "pCubeShape137.wm" "polyUnite1.im[122]";
connectAttr "pCubeShape138.wm" "polyUnite1.im[123]";
connectAttr "pCubeShape139.wm" "polyUnite1.im[124]";
connectAttr "pCubeShape140.wm" "polyUnite1.im[125]";
connectAttr "pCubeShape141.wm" "polyUnite1.im[126]";
connectAttr "pCubeShape142.wm" "polyUnite1.im[127]";
connectAttr "pCubeShape143.wm" "polyUnite1.im[128]";
connectAttr "pCubeShape144.wm" "polyUnite1.im[129]";
connectAttr "pCubeShape145.wm" "polyUnite1.im[130]";
connectAttr "pCubeShape146.wm" "polyUnite1.im[131]";
connectAttr "pCubeShape147.wm" "polyUnite1.im[132]";
connectAttr "pCubeShape148.wm" "polyUnite1.im[133]";
connectAttr "pCubeShape149.wm" "polyUnite1.im[134]";
connectAttr "pCubeShape150.wm" "polyUnite1.im[135]";
connectAttr "pCubeShape151.wm" "polyUnite1.im[136]";
connectAttr "pCubeShape152.wm" "polyUnite1.im[137]";
connectAttr "pCubeShape153.wm" "polyUnite1.im[138]";
connectAttr "pCubeShape154.wm" "polyUnite1.im[139]";
connectAttr "pCubeShape155.wm" "polyUnite1.im[140]";
connectAttr "pCubeShape156.wm" "polyUnite1.im[141]";
connectAttr "pCubeShape157.wm" "polyUnite1.im[142]";
connectAttr "pCubeShape158.wm" "polyUnite1.im[143]";
connectAttr "pCubeShape159.wm" "polyUnite1.im[144]";
connectAttr "pCubeShape160.wm" "polyUnite1.im[145]";
connectAttr "pCubeShape161.wm" "polyUnite1.im[146]";
connectAttr "pCubeShape162.wm" "polyUnite1.im[147]";
connectAttr "pCubeShape163.wm" "polyUnite1.im[148]";
connectAttr "pCubeShape164.wm" "polyUnite1.im[149]";
connectAttr "pCubeShape165.wm" "polyUnite1.im[150]";
connectAttr "pCubeShape166.wm" "polyUnite1.im[151]";
connectAttr "pCubeShape167.wm" "polyUnite1.im[152]";
connectAttr "pCubeShape168.wm" "polyUnite1.im[153]";
connectAttr "pCubeShape169.wm" "polyUnite1.im[154]";
connectAttr "pCubeShape170.wm" "polyUnite1.im[155]";
connectAttr "pCubeShape171.wm" "polyUnite1.im[156]";
connectAttr "pCubeShape172.wm" "polyUnite1.im[157]";
connectAttr "pCubeShape173.wm" "polyUnite1.im[158]";
connectAttr "pCubeShape174.wm" "polyUnite1.im[159]";
connectAttr "pCubeShape175.wm" "polyUnite1.im[160]";
connectAttr "pCubeShape176.wm" "polyUnite1.im[161]";
connectAttr "pCubeShape177.wm" "polyUnite1.im[162]";
connectAttr "pCubeShape178.wm" "polyUnite1.im[163]";
connectAttr "pCubeShape179.wm" "polyUnite1.im[164]";
connectAttr "pCubeShape180.wm" "polyUnite1.im[165]";
connectAttr "pCubeShape181.wm" "polyUnite1.im[166]";
connectAttr "pCubeShape182.wm" "polyUnite1.im[167]";
connectAttr "pCubeShape183.wm" "polyUnite1.im[168]";
connectAttr "pCubeShape184.wm" "polyUnite1.im[169]";
connectAttr "pCubeShape185.wm" "polyUnite1.im[170]";
connectAttr "pCubeShape186.wm" "polyUnite1.im[171]";
connectAttr "pCubeShape187.wm" "polyUnite1.im[172]";
connectAttr "pCubeShape188.wm" "polyUnite1.im[173]";
connectAttr "pCubeShape189.wm" "polyUnite1.im[174]";
connectAttr "pCubeShape190.wm" "polyUnite1.im[175]";
connectAttr "pCubeShape191.wm" "polyUnite1.im[176]";
connectAttr "pCubeShape192.wm" "polyUnite1.im[177]";
connectAttr "pCubeShape193.wm" "polyUnite1.im[178]";
connectAttr "pCubeShape194.wm" "polyUnite1.im[179]";
connectAttr "pCubeShape195.wm" "polyUnite1.im[180]";
connectAttr "pCubeShape196.wm" "polyUnite1.im[181]";
connectAttr "pCubeShape197.wm" "polyUnite1.im[182]";
connectAttr "pCubeShape198.wm" "polyUnite1.im[183]";
connectAttr "pCubeShape199.wm" "polyUnite1.im[184]";
connectAttr "pCubeShape200.wm" "polyUnite1.im[185]";
connectAttr "pCubeShape201.wm" "polyUnite1.im[186]";
connectAttr "pCubeShape202.wm" "polyUnite1.im[187]";
connectAttr "pCubeShape203.wm" "polyUnite1.im[188]";
connectAttr "pCubeShape204.wm" "polyUnite1.im[189]";
connectAttr "pCubeShape205.wm" "polyUnite1.im[190]";
connectAttr "pCubeShape206.wm" "polyUnite1.im[191]";
connectAttr "pCubeShape207.wm" "polyUnite1.im[192]";
connectAttr "pCubeShape208.wm" "polyUnite1.im[193]";
connectAttr "pCubeShape209.wm" "polyUnite1.im[194]";
connectAttr "pCubeShape210.wm" "polyUnite1.im[195]";
connectAttr "pCubeShape211.wm" "polyUnite1.im[196]";
connectAttr "pCubeShape212.wm" "polyUnite1.im[197]";
connectAttr "pCubeShape213.wm" "polyUnite1.im[198]";
connectAttr "polyCube1.out" "groupParts3.ig";
connectAttr "groupId4.id" "groupParts3.gi";
connectAttr "polyCube2.out" "groupParts4.ig";
connectAttr "groupId6.id" "groupParts4.gi";
connectAttr "polyCube4.out" "groupParts5.ig";
connectAttr "groupId372.id" "groupParts5.gi";
connectAttr "polyCube5.out" "groupParts6.ig";
connectAttr "groupId388.id" "groupParts6.gi";
connectAttr "polyUnite1.out" "groupParts7.ig";
connectAttr "groupId400.id" "groupParts7.gi";
connectAttr "groupParts7.og" "groupParts8.ig";
connectAttr "groupId401.id" "groupParts8.gi";
connectAttr "groupParts8.og" "groupParts9.ig";
connectAttr "groupId402.id" "groupParts9.gi";
connectAttr "groupParts9.og" "groupParts10.ig";
connectAttr "groupId403.id" "groupParts10.gi";
connectAttr "groupParts10.og" "groupParts11.ig";
connectAttr "groupId404.id" "groupParts11.gi";
connectAttr "groupParts11.og" "groupParts12.ig";
connectAttr "groupId405.id" "groupParts12.gi";
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "lambert4SG.pa" ":renderPartition.st" -na;
connectAttr "lambert5SG.pa" ":renderPartition.st" -na;
connectAttr "lambert6SG.pa" ":renderPartition.st" -na;
connectAttr "lambert7SG.pa" ":renderPartition.st" -na;
connectAttr "west_house_body.msg" ":defaultShaderList1.s" -na;
connectAttr "west_house_posts.msg" ":defaultShaderList1.s" -na;
connectAttr "west_house_roof_frame.msg" ":defaultShaderList1.s" -na;
connectAttr "west_house_window_frame.msg" ":defaultShaderList1.s" -na;
connectAttr "west_house_window.msg" ":defaultShaderList1.s" -na;
connectAttr "west_house_roof.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of western_house.ma
