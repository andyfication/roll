This is my repo for my final year project in Games Programming.

Supervisor: Phoenix Perry

Project Name: Roll

Roll aims to combine classic components of a video game with the incredible science of physical computing.
By infusing the two, Roll aspires to bridge the gap between playing on a digital screen with the analog 
world to create a more fulfilling and immersive game.


The folder contains the followings:

Blender_Assets ----  all the game models made in Blender.

Maya_Assets ---- all the game models made in Maya.

Final Repot ---- all the documentation and references about the project

Useful Materials ---- all materials used for the project

Roll ---- main Unity project

Roll_Exe_Apps ---- executable game files





